---
title: "Desperation by Stephen King (1996)"
slug: "desperation"
author: "Paulo Pereira"
date: 2024-10-13T17:00:00+01:00
lastmod: 2024-10-13T17:00:00+01:00
cover: "/posts/books/desperation.jpg"
description: "Finished “Desperation” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - 7/10 Books
  - Audiobook
---

Finished “Desperation”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [January 1, 1996](/book-publication-year/1990s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/30155190-desperation)
>
> Something is terribly wrong in Desperation, Nevada -- a small mining town just off Route 50 with a played out open pit copper mine. The streets are wind swept and deserted; animals have the run of the town and something horrific is brewing in the now abandoned mine pit. You won't have a good day in Desperation.
> 
> En route to Lake Tahoe for a much anticipated vacation, the Carver family is arrested for blowing out all four tires on their camper. Collie Entragian is the arresting officer, the self-made sheriff of a town called Desperation, Nevada, and the quintessential bad cop.
> 
> "You have the right to remain silent", the big cop said in his robot's voice. "If you do not choose to remain silent, anything you say may be used against you in a court of law. You have the right to an attorney. I'm going to kill you. If you cannot afford an attorney, one will be provided for you. Do you understand your rights as I have explained them to you?"
> 
> Unbeknownst to the Carvers, Entragian regularly sniffs out passerbys on this stretch of road, and in fact has done in nearly every resident of his hometown. He can also change form and summon the help of creepy creatures, including scorpions, snakes and spiders.
> 
> Though the family seems doomed, an unlikely hero emerges --11-year-old David Carver--who finds his own way to get around the Law.