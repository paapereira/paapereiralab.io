---
title: "Not Till We Are Lost (Bobiverse Book 5) by Dennis E. Taylor (2024)"
slug: "not-till-we-are-lost-bobiverse-book-5"
author: "Paulo Pereira"
date: 2024-11-24T15:00:00+00:00
lastmod: 2024-11-24T15:00:00+00:00
cover: "/posts/books/not-till-we-are-lost-bobiverse-book-5.jpg"
description: "Finished “Not Till We Are Lost (Bobiverse Book 5)” by Dennis E. Taylor."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2024
book authors:
  - Dennis E. Taylor
book series:
  - Bobiverse Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Dennis E. Taylor
  - Bobiverse Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Not Till We Are Lost”.
* Author: [Dennis E. Taylor](/book-authors/dennis-e.-taylor)
* First Published: [September 5, 2024](/book-publication-year/2024)
* Series: [Bobiverse](/book-series/bobiverse-series) Book 5
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/159116833-not-till-we-are-lost)
>
> The Bobiverse is a different place in the aftermath of the Starfleet War, and the days of the Bobs gathering in one big happy moot are far behind. There’s anti-Bob sentiment on multiple planets, the Skippies playing with an AI time bomb, and multiple Bobs just wanting to get away from it all.
> 
> But it all pales compared to what Icarus and Daedalus discover on their 26,000-year journey to the center of the galaxy. Sure, it could settle the Fermi Paradox for good (and what Bob doesn’t want to solve a mystery of the universe?). But it also reveals a threat to the galaxy greater than anything the Bobs could have imagined.
> 
> Just another average day in the Bobiverse.