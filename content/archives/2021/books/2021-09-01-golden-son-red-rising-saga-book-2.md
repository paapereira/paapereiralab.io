---
title: "Golden Son (Red Rising Saga Book 2) by Pierce Brown (2015)"
slug: "golden-son-red-rising-saga-book-2"
author: "Paulo Pereira"
date: 2021-09-01T21:00:00+01:00
lastmod: 2021-09-01T21:00:00+01:00
cover: "/posts/books/golden-son-red-rising-saga-book-2.jpg"
description: "Finished “Golden Son (Red Rising Saga Book 2)” by Pierce Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - Pierce Brown
book series:
  - Red Rising Saga Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Pierce Brown
  - Red Rising Saga Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
---

Finished “Golden Son”.
* Author: [Pierce Brown](/book-authors/pierce-brown)
* First Published: [January 6th 2015](/book-publication-year/2015)
* Series: [Red Rising Saga](/book-series/red-rising-saga-series) Book 2
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/21425079-golden-son)
>
> Golden Son continues the stunning saga of Darrow, a rebel forged by tragedy, battling to lead his oppressed people to freedom.
> 
> As a Red, Darrow grew up working the mines deep beneath the surface of Mars, enduring backbreaking labor while dreaming of the better future he was building for his descendants. But the Society he faithfully served was built on lies. Darrow’s kind have been betrayed and denied by their elitist masters, the Golds—and their only path to liberation is revolution. And so Darrow sacrifices himself in the name of the greater good for which Eo, his true love and inspiration, laid down her own life. He becomes a Gold, infiltrating their privileged realm so that he can destroy it from within.
> 
> A lamb among wolves in a cruel world, Darrow finds friendship, respect, and even love—but also the wrath of powerful rivals. To wage and win the war that will change humankind’s destiny, Darrow must confront the treachery arrayed against him, overcome his all-too-human desire for retribution—and strive not for violent revolt but a hopeful rebirth. Though the road ahead is fraught with danger and deceit, Darrow must choose to follow Eo’s principles of love and justice to free his people.
> 
> He must live for more.