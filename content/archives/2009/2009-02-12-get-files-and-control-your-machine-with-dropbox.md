---
title: "Get files and control your machine with Dropbox"
slug: "get-files-and-control-your-machine-with-dropbox"
author: "Paulo Pereira"
date: 2009-02-12T21:21:00+00:00
lastmod: 2009-02-12T21:21:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Dropbox
---

After talking with a friend about how he was using [Dropbox](https://www.getdropbox.com/) to add new torrent files in is home computer, from work, I got excited with the potential of the idea.

Dropbox, is a multiplatform tool that allows you to sync and share files online across computers. If you save a file in your home computer, when you turn on your computer at work, for example, that file will be  automatically synced if you have Dropbox installed. It also have a web  interface, so you don't even need to install it.

If you don't know it, definitely [check it out](https://www.getdropbox.com/tour).

## Control your machine with Dropbox

What I will show, is a way to control your home computer from, for example, your workplace, using Dropbox.

Here's two scenarios:

1. You need a document you have at home, but it is not in your Dropbox share
2. You want to download the latest Ubuntu torrent so when you get home the download is finished

Both this scenarios have the same solution: use a combination of Dropbox, crontab, shell scripts and a config file.

## Dropbox

First of all you need of course Dropbox. Even without this post it is extremely cool and useful :)

In this example the Dropbox location is /home/user/Dropbox/.

## Shell Script

Create a shell script in your favorite location. In this example it will be /home/user/.

```bash
touch /home/user/dropscript.sh
chmod 755 /home/user/dropscript.sh
```

For now I'll do a simple example, but I'll post some improvements at a later time.

Add the following to the script:

```text
cat '/home/user/Dropbox/dropconfig'
```

## Configuration File

In your Dropbox location create a configuration file. This is the file you will be editing from your workplace in order to get things done.

I want this to be a more structured file, but for now I'll show a simple example.

```bash
touch /home/user/Dropbox/dropconfig
```

Add the following to the file, accordingly to the scenario:

**Scenario 1**

```bash
cp /home/user/Documents/myfile /home/user/Dropbox/
```

**Scenario 2**

```bash
cd /home/user/Downloads
wget ftp://ftp.rnl.ist.utl.pt/ubuntu-releases/intrepid/ubuntu-8.10-desktop-i386.iso
```

## Crontab

Have crontab run your script. In this example every minute.

```bash
crontab -e
```

Add the following to crontab:

```text
* * * * * '/home/user/dropscript.sh'
```

## Conclusion

The showed example will execute the instructions in your configuration file every minute of every day. If you are at work and you need a file or want to shutdown your machine, just edit the configuration file  available at your Dropbox, and wait until crontab executes your command. It's that simple.

Of course there are many solutions to scenarios like this. Better and more elegant, sure. Still, I think this is a pretty cool solution.

**Advantages**

- Free solution
- No variable IP troubles

**Disadvantages**

- Security - you could do virtually everything that can be executed in a Terminal

Comment await and share the things you are doing using Dropbox :-)