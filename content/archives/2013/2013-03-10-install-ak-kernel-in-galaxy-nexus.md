---
title: "Install Ak Kernel in Galaxy Nexus"
slug: "install-ak-kernel-in-galaxy-nexus"
author: "Paulo Pereira"
date: 2013-03-10T22:06:00+00:00
lastmod: 2013-03-10T22:06:00+00:00
draft: false
toc: true
categories:
  - Android
tags:
  - Android
  - Galaxy Nexus
  - Ak Kernel
---

I started using Ak Kernel after installing Xylon, as I talked about in my previous [post](/posts/install-xylon-in-galaxy-nexus/).

![Ak Kernel](/posts/2013/2013-03-10-install-ak-kernel-in-galaxy-nexus/ak.png)

I will show how to install [Ak Kernel](http://forum.xda-developers.com/showthread.php?t=1883298) coming from another kernel an how to simply update Ak Kernel when a new version is released.

You can also read the kernel FAQ [here](http://forum.xda-developers.com/showthread.php?t=2157193).

## Ak Kernel

[Ak Kernel](http://forum.xda-developers.com/showthread.php?t=1883298) comes in two flavors:

- Purity edition, the stable version
- Cylon edition, a experimental branch

## Install Ak Kernel coming from another kernel

If you are installing Ak Kernel for the first time in your ROM, follow this steps:

- Clear /cache (wipe cache partition)
- Wipe Dalvik Cache (advanced > wipe dalvik cache)
- Format /system (mounts and storage > format /system)
- Flash your ROM again (install zip from sdcard > choose zip from sdcard > ...)
- Install the kernel (install zip from sdcard > choose zip from sdcard > 0 > 0.ROOT > AK.731.CYLON.zip)
- Fix permissions (advanced > fix permissions)
- Reboot (reboot system now)

You won't lose your data by doing this! Only the /system partition is formated, your /data partition won't be touched.

Still always backup first!

## Updating Ak Kernel

When a new update is available just flash the new kernel. It will wipe the cache and dalvik cache for you.

- install zip from sdcard > choose zip from sdcard > 0 > 0.ROOT > AK.731.CYLON.zip

## Purity vs. Cylon

When changing between the two editions I recommend you to proceed as coming from another kernel, as showed.

## Ak Kernel settings

To change Ak Kernel setting use [Trickster MOD Kernel Settings](https://play.google.com/store/apps/details?id=com.bigeyes0x0.trickstermod). Don't forget to donate to the developer by purchasing the premium [key](https://play.google.com/store/apps/details?id=com.bigeyes0x0.trickstermod.premium).

I'm still playing with the setting and will post my config when I find one I like.