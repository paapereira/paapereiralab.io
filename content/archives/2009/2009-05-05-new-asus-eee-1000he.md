---
title: "New Asus Eee 1000HE"
slug: "new-asus-eee-1000he"
author: "Paulo Pereira"
date: 2009-05-05T19:59:00+01:00
lastmod: 2009-05-05T19:59:00+01:00
draft: false
toc: true
categories:
  - General
tags:
  - General
  - Asus Eee 1000HE
aliases:
  - /posts/new-asus-eee-1000he/
---

Last week I bought a new [Asus Eee 1000HE](http://eeepc.asus.com/global/product1000he.html). Who can resist a 10% off the price? :-)

The main feature in this new netbook is, of course, the 9.5 hours of battery life. Amazing!

It came with Windows XP. The next thing was installing Linux. I chose [Ubuntu 9.04 Netbook Remix](http://www.ubuntu.com/getubuntu/download-netbook).

Here's some pics I've manage to take with my phone.

![eee1](/posts/2009/2009-05-05-new-asus-eee-1000he/eee1.jpg)

![eee2](/posts/2009/2009-05-05-new-asus-eee-1000he/eee2.jpg)

![eee3](/posts/2009/2009-05-05-new-asus-eee-1000he/eee3.jpg)

![eee4](/posts/2009/2009-05-05-new-asus-eee-1000he/eee4.jpg)