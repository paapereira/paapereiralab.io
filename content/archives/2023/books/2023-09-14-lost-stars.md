---
title: "Lost Stars by Claudia Gray (2015)"
slug: "lost-stars"
author: "Paulo Pereira"
date: 2023-09-14T21:00:00+01:00
lastmod: 2023-09-14T21:00:00+01:00
cover: "/posts/books/lost-stars.jpg"
description: "Finished “Lost Stars” by Claudia Gray."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - Claudia Gray
book series:
  - Star Wars Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Claudia Gray
  - Star Wars Universe
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/lost-stars
---

Finished “Lost Stars”.
* Author: [Claudia Gray](/book-authors/claudia-gray)
* First Published: [September 4, 2015](/book-publication-year/2015)
* Series: [Star Wars Universe](/book-series/star-wars-universe)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25654388-lost-stars)
>
> This thrilling Young Adult novel gives readers a macro view of some of the most important events in the Star Wars universe, from the rise of the Rebellion to the fall of the Empire. Readers will experience these major moments through the eyes of two childhood friends--Ciena Ree and Thane Kyrell--who have grown up to become an Imperial officer and a Rebel pilot. Now on opposite sides of the war, will these two star-crossed lovers reunite, or will duty tear them--and the galaxy--apart? Star Wars: Lost Stars also includes all-new post- Star Wars: Return of the Jedi content, as well as hints and clues about the upcoming film Star Wars: The Force Awakens, making this a must-read for all Star Wars fans.