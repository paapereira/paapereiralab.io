---
title: "Books I have read"
slug: "reading-list"
draft: false
toc: true
---

## About

I have been reading for a long time. 30+ years of books read. 

I only stared organizing the books I read around 2013 though. So, this list is much incomplete. In time I will try to remember books read from before 2013. Should I go back in time...

I am only listing books, including ebooks and audiobooks. Comics and graphic novels are not included.

For each book I am keeping the book format (B-physical book, E-ebook, A-audiobook), length, reading date, rating and genre:
- FC-Fiction
- NC-Nonfiction
- SF-Science Fiction
- FY-Fantasy
- HR-Horror
- TH-Thriller
- MY-Mystery
- SC-Science
- SH-Self Help
- PR-Productivity
- BI-Biography
- HI-History
- BU-Business
- PY-Psychology
- PH-Philosophy
- FI-Finance
- HU-Humor
- CL-Classic
- AD-Adventure
- RPG-Role Playing Game
- SS-Short Stories
- CR-Crime

**==Highlighted==** are me favorite books.

## 1980s, 1990s and up to 2012

This section I got from memory and from my resistent childhood library. I do not remember reading dates and the list is lacking, incomplete and not in a reading order.

I will add to it if and when I remember more.

1. **Uma Aventura na Cidade** (Uma Aventura #1) by Ana Maria Magalhães and Isabel Alçada (1982) [B|FC,AD]
2. **Uma Aventura nas Férias de Natal** (Uma Aventura #2) by Ana Maria Magalhães and Isabel Alçada (1983) [B|FC,AD]
3. **Uma Aventura na Falésia** (Uma Aventura #3) by Ana Maria Magalhães and Isabel Alçada (1983) [B|FC,AD]
4. **Uma Aventura em Viagem** (Uma Aventura #4) by Ana Maria Magalhães and Isabel Alçada (1983) [B|FC,AD]
5. **Uma Aventura no Bosque** (Uma Aventura #5) by Ana Maria Magalhães and Isabel Alçada (1983) [B|FC,AD]
6. **Uma Aventura entre Douro e Minho** (Uma Aventura #6) by Ana Maria Magalhães and Isabel Alçada (1983) [B|FC,AD]
7. **Uma Aventura Alarmante** (Uma Aventura #7) by Ana Maria Magalhães and Isabel Alçada (1984) [B|FC,AD]
8. **Uma Aventura na Escola** (Uma Aventura #8) by Ana Maria Magalhães and Isabel Alçada (1984) [B|FC,AD]
9. **Uma Aventura no Ribatejo** (Uma Aventura #9) by Ana Maria Magalhães and Isabel Alçada (1985) [B|FC,AD]
10. **Uma Aventura em Evoramonte** (Uma Aventura #10) by Ana Maria Magalhães and Isabel Alçada (1984) [B|FC,AD]
11. **Uma Aventura na Mina** (Uma Aventura #11) by Ana Maria Magalhães and Isabel Alçada (1985) [B|FC,AD]
12. **Uma Aventura no Algarve** (Uma Aventura #12) by Ana Maria Magalhães and Isabel Alçada (1985) [B|FC,AD]
13. **Uma Aventura no Porto** (Uma Aventura #13) by Ana Maria Magalhães and Isabel Alçada (1985) [B|FC,AD]
14. **Uma Aventura no Estádio** (Uma Aventura #14) by Ana Maria Magalhães and Isabel Alçada (1985) [B|FC,AD]
15. **Uma Aventura na Terra e no Mar** (Uma Aventura #15) by Ana Maria Magalhães and Isabel Alçada (1986) [B|FC,AD]
16. **Uma Aventura debaixo da Terra** (Uma Aventura #16) by Ana Maria Magalhães and Isabel Alçada (1986) [B|FC,AD]
17. **Uma Aventura no Supermercado** (Uma Aventura #17) by Ana Maria Magalhães and Isabel Alçada (1986) [B|FC,AD]
18. **Uma Aventura Musical** (Uma Aventura #18) by Ana Maria Magalhães and Isabel Alçada (1987) [B|FC,AD]
19. **Uma Aventura nas Férias da Páscoa** (Uma Aventura #19) by Ana Maria Magalhães and Isabel Alçada (1987) [B|FC,AD]
20. **Uma Aventura no Teatro** (Uma Aventura #20) by Ana Maria Magalhães and Isabel Alçada (1988) [B|FC,AD]
21. **Uma Aventura no Deserto** (Uma Aventura #21) by Ana Maria Magalhães and Isabel Alçada (1988) [B|FC,AD]
22. **Uma Aventura em Lisboa** (Uma Aventura #22) by Ana Maria Magalhães and Isabel Alçada (1988) [B|FC,AD]
23. **Uma Aventura nas Férias Grandes** (Uma Aventura #23) by Ana Maria Magalhães and Isabel Alçada (1988) [B|FC,AD]
24. **Uma Aventura no Carnaval** (Uma Aventura #24) by Ana Maria Magalhães and Isabel Alçada (1989) [B|FC,AD]
25. **Uma Aventura nas Ilhas de Cabo Verde** (Uma Aventura #25) by Ana Maria Magalhães and Isabel Alçada (1990) [B|FC,AD]
26. **Uma Aventura no Palácio da Pena** (Uma Aventura #26) by Ana Maria Magalhães and Isabel Alçada (1990) [B|FC,AD]
27. **Uma Aventura no Inverno** (Uma Aventura #27) by Ana Maria Magalhães and Isabel Alçada (1990) [B|FC,AD]
28. **Uma Aventura em França** (Uma Aventura #28) by Ana Maria Magalhães and Isabel Alçada (1991) [B|FC,AD]
29. **Uma Aventura Fantástica** (Uma Aventura #29) by Ana Maria Magalhães and Isabel Alçada (1991) [B|FC,AD]
30. **Uma Aventura no Verão** (Uma Aventura #30) by Ana Maria Magalhães and Isabel Alçada (1991) [B|FC,AD]
31. **Uma Aventura nos Açores** (Uma Aventura #31) by Ana Maria Magalhães and Isabel Alçada (1993) [B|FC,AD]
32. **Uma Aventura na Serra da Estrela** (Uma Aventura #32) by Ana Maria Magalhães and Isabel Alçada (1993) [B|FC,AD]
33. **Uma Aventura na Praia** (Uma Aventura #33) by Ana Maria Magalhães and Isabel Alçada (1994) [B|FC,AD]
34. **Uma Aventura Perigosa** (Uma Aventura #34) by Ana Maria Magalhães and Isabel Alçada (1994) [B|FC,AD]
35. **Uma Aventura em Macau** (Uma Aventura #35) by Ana Maria Magalhães and Isabel Alçada (1995) [B|FC,AD]
36. **Uma Aventura na Biblioteca** (Uma Aventura #36) by Ana Maria Magalhães and Isabel Alçada (1996) [B|FC,AD]
37. **Uma Aventura em Espanha** (Uma Aventura #37) by Ana Maria Magalhães and Isabel Alçada (1996) [B|FC,AD]
38. **Uma Aventura na Casa Assombrada** (Uma Aventura #38) by Ana Maria Magalhães and Isabel Alçada (1997) [B|FC,AD]
39. **Uma Aventura na Televisão** (Uma Aventura #39) by Ana Maria Magalhães and Isabel Alçada (1998) [B|FC,AD]
40. **Uma Aventura no Egipto** (Uma Aventura #40) by Ana Maria Magalhães and Isabel Alçada (1999) [B|FC,AD]
41. **Uma Viagem ao Tempo dos Castelos** (Viagens no Tempo #1) by Ana Maria Magalhães and Isabel Alçada (1985) [B|FC,AD]
42. **Uma Visita à Corte do Rei D. Dinis** (Viagens no Tempo #2) by Ana Maria Magalhães and Isabel Alçada (1986) [B|FC,AD]
43. **O Ano Da Peste Negra** (Viagens no Tempo #3) by Ana Maria Magalhães and Isabel Alçada (1986) [B|FC,AD]
44. **Uma Ilha de Sonho** (Viagens no Tempo #4) by Ana Maria Magalhães and Isabel Alçada (1987) [B|FC,AD]
45. **A Terra Será Redonda?** (Viagens no Tempo #5) by Ana Maria Magalhães and Isabel Alçada (1988) [B|FC,AD]
46. **Um Cheirinho de Canela** (Viagens no Tempo #6) by Ana Maria Magalhães and Isabel Alçada (1988) [B|FC,AD]
47. **O Dia do Terramoto** (Viagens no Tempo #7) by Ana Maria Magalhães and Isabel Alçada (1989) [B|FC,AD]
48. **Mistérios da Flandres** (Viagens no Tempo #8) by Ana Maria Magalhães and Isabel Alçada (1990) [B|FC,AD]
49. **O sabor da liberdade** (Viagens no Tempo #9) by Ana Maria Magalhães and Isabel Alçada (1991) [B|FC,AD]
50. **Brasil! Brasil!** (Viagens no Tempo #10) by Ana Maria Magalhães and Isabel Alçada (1992) [B|FC,AD]
51. **Um trono para dois irmãos** (Viagens no Tempo #11) by Ana Maria Magalhães and Isabel Alçada (1993) [B|FC,AD]
52. **Mataram o rei!** (Viagens no Tempo #12) by Ana Maria Magalhães and Isabel Alçada (1994) [B|FC,AD]
53. **Tufão nos Mares da China** (Viagens no Tempo #13) by Ana Maria Magalhães and Isabel Alçada (1997) [B|FC,AD]
54. **No Coração da África Misteriosa** (Viagens no Tempo #14) by Ana Maria Magalhães and Isabel Alçada (1998) [B|FC,AD]
55. **O Feiticeiro da Montanha de Fogo** (Aventuras Fantásticas #1) by Steve Jackson and Ian Livingstone (1982) [B|FY,RPG]
56. **A Floresta da Morte** (Aventuras Fantásticas #2) by Ian Livingstone (1983) [B|FY,RPG]
57. **A Cidadela do Caos** (Aventuras Fantásticas #3) by Steve Jackson (1983) [B|FY,RPG]
58. **A Nave Perdida** (Aventuras Fantásticas #4) by Steve Jackson (1983) [B|FY,RPG]
59. **A Cidade dos Ladrões** (Aventuras Fantásticas #5) by Ian Livingstone (1983) [B|FY,RPG]
60. **A Masmorra Infernal** (Aventuras Fantásticas #6) by Ian Livingstone (1984) [B|FY,RPG]
61. **A Ilha do Rei Lagarto** (Aventuras Fantásticas #7) by Ian Livingstone (1984) [B|FY,RPG]
62. **O Pântano do Escorpião** (Aventuras Fantásticas #8) by Steve Jackson (1984) [B|FY,RPG]
63. **A Feiticeira das Neves** (Aventuras Fantásticas #9) by Ian Livingstone (1984) [B|FY,RPG]
64. **A Mansão Diabólica** (Aventuras Fantásticas #10) by Steve Jackson (1982) [B|FY,RPG]
65. **O Talismã da Morte** (Aventuras Fantásticas #11) by Steve Jackson and Ian Livingstone (1984) [B|FY,RPG]
66. **O Assassino do Espaço** (Aventuras Fantásticas #12) by Steve Jackson and Ian Livingstone (1985) [B|FY,RPG]
67. **O Templo do Terror** (Aventuras Fantásticas #13) by Ian Livingstone (1985) [B|FY,RPG]
68. **O Planeta Rebelde** (Aventuras Fantásticas #14) by Steve Jackson and Ian Livingstone (1986) [B|FY,RPG]
69. **Demónios das Profundezas** (Aventuras Fantásticas #15) by Steve Jackson and Ian Livingstone (1986) [B|FY,RPG]
70. **A Espada do Samurai** (Aventuras Fantásticas #16) by Steve Jackson and Ian Livingstone (1986) [B|FY,RPG]
71. **O Desafio dos Campeões** (Aventuras Fantásticas #17) by Ian Livingstone (1986) [B|FY,RPG]
72. **Os Círculos de Kether** (Aventuras Fantásticas #18) by Steve Jackson and Ian Livingstone (1985) [B|FY,RPG]
73. **Máscaras de Destruição** (Aventuras Fantásticas #19) by Steve Jackson and Ian Livingstone (1985) [B|FY,RPG]
74. **Comando Robot** (Aventuras Fantásticas #20) by Steve Jackson and Ian Livingstone (1985) [B|FY,RPG]
75. **O Castelo dos Pesadelos** (Aventuras Fantásticas #21) by Steve Jackson and Ian Livingstone (1987) [B|FY,RPG]
76. **A Cripta da Feitiçaria** (Aventuras Fantásticas #22) by Steve Jackson and Ian Livingstone (1987) [B|FY,RPG]
77. **O Viajante das Estrelas** (Aventuras Fantásticas #23) by Steve Jackson and Ian Livingstone (1987) [B|FY,RPG]
78. **Abismos do Mal** (Aventuras Fantásticas #24) by Steve Jackson and Ian Livingstone (1987) [B|FY,RPG]
79. **Encontro com o M.E.D.O.** (Aventuras Fantásticas #25) by Steve Jackson and Ian Livingstone (1985) [B|FY,RPG]
80. **A Arma de Telak** (Aventuras Fantásticas #26) by Steve Jackson and Ian Livingstone (1988) [B|FY,RPG]
81. **Cavaleiro do Céu** (Aventuras Fantásticas #27) by Steve Jackson and Ian Livingstone (1988) [B|FY,RPG]
82. **O Ladrão de Espíritos** (Aventuras Fantásticas #28) by Steve Jackson and Ian Livingstone (1988) [B|FY,RPG]
83. **A Maldição do Vampiro** (Aventuras Fantásticas #29) by Steve Jackson and Ian Livingstone (1989) [B|FY,RPG]
84. **Torre de devastação** (Aventuras Fantásticas #30) by Steve Jackson and Ian Livingstone (1991) [B|FY,RPG]
85. **Maré Vermelha** (Aventuras Fantásticas #31) by Steve Jackson and Ian Livingstone (1992) [B|FY,RPG]
86. **Escravos do Abismo** (Aventuras Fantásticas #32) by Steve Jackson and Ian Livingstone (1988) [B|FY,RPG]
87. **Exércitos da Morte** (Aventuras Fantásticas #33) by Ian Livingstone (1988) [B|FY,RPG]
88. **A Lenda dos Cavaleiros das Trevas** (Aventuras Fantásticas #34) by Steve Jackson and Ian Livingstone (1991) [B|FY,RPG]
89. **Regresso à Montanha de Fogo** (Aventuras Fantásticas #35) by Ian Livingstone (1992) [B|FY,RPG]
90. **O Senhor do Caos** (Aventuras Fantásticas #36) by Steve Jackson and Ian Livingstone (1990) [B|FY,RPG]
91. **Resgate em Arion** (Aventuras Fantásticas #37) by Steve Jackson and Ian Livingstone (1994) [B|FY,RPG]
92. **A Maldição da Múmia** (Aventuras Fantásticas #38) by Steve Jackson and Ian Livingstone (1995) [B|FY,RPG]
93. **O Olhar do Dragão** (Triângulo Jota #1) by Álvaro Magalhães (1989) [B|FC,AD]
94. **Sete Dias e Sete Noites** (Triângulo Jota #2) by Álvaro Magalhães (1989) [B|FC,AD]
95. **Corre Michael! Corre!** (Triângulo Jota #3) by Álvaro Magalhães (1989) [B|FC,AD]
96. **A Rapariga dos Anúncios** (Triângulo Jota #4) by Álvaro Magalhães (1990) [B|FC,AD]
97. **Ao Serviço de Sua Majestade** (Triângulo Jota #5) by Álvaro Magalhães (1990) [B|FC,AD]
98. **O Vampiro do Dente de Ouro** (Triângulo Jota #6) by Álvaro Magalhães (1991) [B|FC,AD]
99. **O Beijo da Serpente** (Triângulo Jota #7) by Álvaro Magalhães (1992) [B|FC,AD]
100. **Guardado No Coração - 1ª Parte** (Triângulo Jota #8) by Álvaro Magalhães (1993) [B|FC,AD]
101. **Guardado No Coração - 2ª Parte** (Triângulo Jota #9) by Álvaro Magalhães (1994) [B|FC,AD]
102. **A Rosa do Egipto** (Triângulo Jota #10) by Álvaro Magalhães (1995) [B|FC,AD]
103. **O Assassino Leitor** (Triângulo Jota #11) by Álvaro Magalhães (1997) [B|FC,AD]
104. **Pelos Teus Lindos Olhos** (Triângulo Jota #12) by Álvaro Magalhães (1997) [B|FC,AD]
105. **O Rei Lagarto** (Triângulo Jota #13) by Álvaro Magalhães (1998) [B|FC,AD]
106. **O clube das Chaves entra em acção** (O Clube das Chaves #1) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1989) [B|FC,AD]
107. **O Clube das Chaves dá tempo ao tempo** (O Clube das Chaves #2) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1990) [B|FC,AD]
108. **O Clube das Chaves toca a 4 mãos** (O Clube das Chaves #3) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1990) [B|FC,AD]
109. **O Clube das Chaves põe tudo em pratos limpos** (O Clube das Chaves #4) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1991) [B|FC,AD]
110. **O Clube das Chaves descobre uma estrela** (O Clube das Chaves #5) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1991) [B|FC,AD]
111. **O Clube das Chaves soma e segue** (O Clube das Chaves #6) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1991) [B|FC,AD]
112. **O Clube das Chaves sobe ao pódium** (O Clube das Chaves #7) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1991) [B|FC,AD]
113. **O Clube das Chaves -ganha terreno** (O Clube das Chaves #8) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1992) [B|FC,AD]
114. **O Clube das Chaves cumpre a missão** (O Clube das Chaves #9) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1992) [B|FC,AD]
115. **O Clube das Chaves na crista da onda** (O Clube das Chaves #10) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1992) [B|FC,AD]
116. **O Clube das Chaves no trilho dourado** (O Clube das Chaves #11) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1993) [B|FC,AD]
117. **O Clube das Chaves tem carta branca** (O Clube das Chaves #12) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1993) [B|FC,AD]
118. **O Clube das Chaves e a nova ordem** (O Clube das Chaves #13) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1994) [B|FC,AD]
119. **O Clube das Chaves tira a prova real** (O Clube das Chaves #14) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1994) [B|FC,AD]
120. **O Clube das Chaves preso por um fio** (O Clube das Chaves #15) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1995) [B|FC,AD]
121. **O Clube das Chaves entre barreiras** (O Clube das Chaves #16) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1995) [B|FC,AD]
122. **O Clube das Chaves regressa à república** (O Clube das Chaves #17) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1996) [B|FC,AD]
123. **O Clube das Chaves caça a pantera** (O Clube das Chaves #18) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1996) [B|FC,AD]
124. **O Clube das Chaves e os animais desaparecidos** (O Clube das Chaves #19) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1997) [B|FC,AD]
125. **O Clube das Chaves mergulha no oceano** (O Clube das Chaves #20) by Maria Teresa Maia Gonzalez and Maria do Rosário Pedreira (1998) [B|FC,AD]
126. **Os Cinco na Ilha do Tesouro** (Os Cinco #1) by Enid Blyton (1942) [B|FC,AD]
127. **Nova Aventura de os Cinco** (Os Cinco #2) by Enid Blyton (1942) [B|FC,AD]
128. **Os Cinco voltam à Ilha** (Os Cinco #3) by Enid Blyton (1944) [B|FC,AD]
129. **Os Cinco e os Contrabandistas** (Os Cinco #4) by Enid Blyton (1945) [B|FC,AD]
130. **Os Cinco e o Circo** (Os Cinco #5) by Enid Blyton (1946) [B|FC,AD]
131. **Os Cinco Salvaram o Tio** (Os Cinco #6) by Enid Blyton (1942) [B|FC,AD]
132. **Os Cinco e o Comboio Fantasma** (Os Cinco #7) by Enid Blyton (1948) [B|FC,AD]
133. **Os Cinco na Casa do Mocho** (Os Cinco #8) by Enid Blyton (1949) [B|FC,AD]
134. **Os Cinco e a Ciganita** (Os Cinco #9) by Enid Blyton (1950) [B|FC,AD]
135. **Os Cinco No Lago Negro** (Os Cinco #10) by Enid Blyton (1951) [B|FC,AD]
136. **Os Cinco No Castelo da Bela-Vista** (Os Cinco #11) by Enid Blyton (1952) [B|FC,AD]
137. **Os Cinco na Torre do Farol** (Os Cinco #12) by Enid Blyton (1953) [B|FC,AD]
138. **Os Cinco na Planície Misteriosa** (Os Cinco #13) by Enid Blyton (1953) [B|FC,AD]
139. **Os Cinco e os Raptores** (Os Cinco #14) by Enid Blyton (1955) [B|FC,AD]
140. **Os Cinco na Casa em Ruínas** (Os Cinco #15) by Enid Blyton (1956) [B|FC,AD]
141. **Os Cinco e os Aviadores** (Os Cinco #16) by Enid Blyton (1957) [B|FC,AD]
142. **Os Cinco nas Montanhas de Gales** (Os Cinco #17) by Enid Blyton (1958) [B|FC,AD]
143. **Os Cinco na Quinta Finniston** (Os Cinco #18) by Enid Blyton (1960) [B|FC,AD]
144. **O Clube dos Sete** (Os Sete #1) by Enid Blyton (1949) [B|FC,AD]
145. **A Primeira Aventura dos Sete** (Os Sete #2) by Enid Blyton (1950) [B|FC,AD]
146. **Os Sete e a Marca Vermelha** (Os Sete #3) by Enid Blyton (1951) [B|FC,AD]
147. **Os Sete e os Seus Rivais** (Os Sete #4) by Enid Blyton (1952) [B|FC,AD]
148. **Os Sete e os Cães Roubados** (Os Sete #5) by Enid Blyton (1953) [B|FC,AD]
149. **Bravo, Valentes Sete** (Os Sete #6) by Enid Blyton (1954) [B|FC,AD]
150. **Os Sete Levam a Melhor** (Os Sete #7) by Enid Blyton (1955) [B|FC,AD]
151. **Três Vivas aos Sete** (Os Sete #8) by Enid Blyton (1956) [B|FC,AD]
152. **O Mistério dos Sete** (Os Sete #9) by Enid Blyton (1957) [B|FC,AD]
153. **Os Sete e o Violino Roubado** (Os Sete #10) by Enid Blyton (1958) [B|FC,AD]
154. **Bem-vindos à Casa da Morte** (Arrepios #1) by R.L. Stine (1992) [B|FC,HR]
155. **A Cave do Terror** (Arrepios #2) by R.L. Stine (1992) [B|FC,HR]
156. **Sangue de monstro** (Arrepios #3) by R.L. Stine (1992) [B|FC,HR]
157. **O Sorriso da Morte** (Arrepios #4) by R.L. Stine (1992) [B|FC,HR]
158. **Um Dia no País dos Horrores** (Arrepios #5) by R.L. Stine (1994) [B|FC,HR]
159. **O Lobisomem do Pântano** (Arrepios #6) by R.L. Stine (1993) [B|FC,HR]
160. **A Noite do Boneco Vivo** (Arrepios #7) by R.L. Stine (1993) [B|FC,HR]
161. **Terror na Biblioteca** (Arrepios #8) by R.L. Stine (1993) [B|FC,HR]
162. **A Máscara Maldita** (Arrepios #9) by R.L. Stine (1993) [B|FC,HR]
163. **Pânico no Acampamento** (Arrepios #10) by R.L. Stine (1993) [B|FC,HR]
164. **O Fantasma da Casa ao Lado** (Arrepios #11) by R.L. Stine (1993) [B|FC,HR]
165. **O Feitiço do Relógio de Cuco** (Arrepios #12) by R.L. Stine (1995) [B|FC,HR]
166. **Uma Aventura Peluda** (Arrepios #14) by R.L. Stine (1994) [B|FC,HR]
167. **O Fantasma do Auditório** (Arrepios #15) by R.L. Stine (1994) [B|FC,HR]
168. **A Maldição da Múmia** (Arrepios #16) by R.L. Stine (1993) [B|FC,HR]
169. **Um Espelho do Outro Mundo** (Arrepios #17) by R.L. Stine (1993) [B|FC,HR]
170. **Os desejos podem matar!** (Arrepios #18) by R.L. Stine (1993) [B|FC,HR]
171. **A Face Do Terror** (Arrepios #19) by R.L. Stine (1994) [B|FC,HR]
172. **Perigo nas Profundezas** (Arrepios #20) by R.L. Stine (1994) [B|FC,HR]
173. **O Espantalho da Meia-Noite** (Arrepios #21) by R.L. Stine (1994) [B|FC,HR]
174. **Sangue de Monstro II** (Arrepios #22) by R.L. Stine (1994) [B|FC,HR]
175. **Mutação Fatal** (Arrepios #23) by R.L. Stine (1994) [B|FC,HR]
176. **O Piano Assassino** (Arrepios #24) by R.L. Stine (1993) [B|FC,HR]
177. **A Vingança das Minhocas** (Arrepios #25) by R.L. Stine (1994) [B|FC,HR]
178. **A Praia Assombrada** (Arrepios #26) by R.L. Stine (1994) [B|FC,HR]
179. **A Sonâmbula** (A Estrada do Terror #1) by R.L. Stine (1990) [B|FC,HR]
180. **Fim-de-Semana Alucinante** (A Estrada do Terror #2) by R.L. Stine (1989) [B|FC,HR]
181. **O Número Errado** (A Estrada do Terror #3) by R.L. Stine (1990) [B|FC,HR]
182. **A Nova Aluna** (A Estrada do Terror #4) by R.L. Stine (1989) [B|FC,HR]
183. **Desaparecidos** (A Estrada do Terror #5) by R.L. Stine (1990) [B|FC,HR]
184. **A Festa-Surpresa** (A Estrada do Terror #6) by R.L. Stine (1989) [B|FC,HR]
185. **O Jardim Secreto** by Frances Hodgson Burnett (1911) [B|FC,CL]
186. **Tom Brown no Colégio** by Thomas Hughes (1857) [B|FC,CL]
187. **A Pousada do Anjo da Guarda** by Condessa de Ségur (1863) [B|FC,CL]
188. **As Meninas Exemplares** by Condessa de Ségur (1858) [B|FC,CL]
189. **Um Bom Diabrete** by Condessa de Ségur (1865) [B|FC,CL]
190. **Memórias de um Burro** by Condessa de Ségur (1860) [B|FC,CL]
191. **O General Dourakine** by Condessa de Ségur (1863) [B|FC,CL]
192. **O Príncipe Desaparecido** by Frances Hodgson Burnett (1915) [B|FC,CL]
193. **Mulherzinhas** by Louisa May Alcott (1868) [B|FC,CL]
194. **Viagem ao Centro da Terra** by Jules Verne (1864) [B|SF,CL]
195. **20000 Léguas Submarinas** by Jules Verne (1869) [B|SF,CL]
196. **Da Terra à Lua** by Jules Verne (1865) [B|SF,CL]
197. **A Lua de Joana** by Maria Teresa Maia Gonzalez (1995) [B|FC|7/10]
198. **==O Conde de Monte Cristo==** by Alexandre Dumas (1844) [B|FC,CL]
199. **A Ilha do Tesouro** by Robert Louis Stevenson (1882) [B|FC,CL,AD]
200. **==Drácula==** by Bram Stoker (1897) [B|FC,CL,HR]
201. **O Cavaleiro da Dinamarca** by Sophia de Mello Breyner Andresen (1964) [B|FC,CL]
202. **Os Maias** by Eça de Queirós (1888) [B|FC,CL] (almost finished)
203. **Aparição** by Vergílio Ferreira (1959) [B|FC,CL|6/10]
204. **O Codex 632** (Tomás Noronha #1) by José Rodrigues dos Santos (2005) [B|FC,TH|7/10]
205. **A Fórmula de Deus** (Tomás Noronha #2) by José Rodrigues dos Santos (2006) [B|FC,TH|7/10]
206. **A Essência do Mal** (James Bond) by Sebastian Faulks (2008) [B|FC,TH|6/10]
207. **O Perfume: História de um Assassino** by Patrick Süskind (1985) [B|FC,HR|7/10]
208. **As Vinhas da Ira** by John Steinbeck (1939) [B|FC,CL|8/10]
209. **==O Pequeno Príncipe==** by Antoine de Saint-Exupéry (1943) [B|FC,CL|9/10]
210. **==A Irmandade do Anel==** (O Senhor dos Anés #1) by J.R.R. Tolkien (1954) [B|FC,FY,CL|10/10]
211. **==As Duas Torres==** (O Senhor dos Anés #2) by J.R.R. Tolkien (1954) [B|FC,FY,CL|10/10]
212. **==O Regresso do Rei==** (O Senhor dos Anés #3) by J.R.R. Tolkien (1955) [B|FC,FY,CL|10/10]
213. **==A Guerra dos Tronos==** (As Crónicas de Gelo e Fogo #1) by George R.R. Martin (1996) [B|FC,FY|10/10]
214. **==The Hunger Games==** (The Hunger Games #1) by Suzanne Collins (2008) [A,11h11m|FC,SF|9/10]
215. **==A Máquina do Tempo==** by H.G. Wells (1895) [E|FC,SF,CL|10/10]

## 2013

This year I read 10 books.

216. **==The Girl with the Dragon Tattoo==** (Millennium #1) by Stieg Larsson (2005) [A,18h47m|FC,MY|2013-01-01,9/10]
217. **The Zombie Survival Guide** by Max Brooks (2003) [A,8h39m|FC,HR]; 2013-02-24; 5/10
218. 
219. 
220. 
221. 
222. 
223. 
224. 
225. 

## 2014

This year I read 16 books.

226. 
227. 
228. 
229. 
230. 
231. 
232. 
233. 
234. 
235. 
236. 
237. 
238. 
239. 
240. 
241. 

## 2015

This year I read 18 books.

242. 
243. 
244. 
245. 
246. 
247. 
248. 
249. 
250. 
251. 
252. 
253. 
254. 
255. 
256. 
257. 
258. 
259. 

## 2016

This year I read 29 books.

260. 
261. 
262. 
263. 
264. 
265. 
266. 
267. 
268. 
269. 
270. 
271. 
272. 
273. 
274. **Ghost in the Wires** by Kevin Mitnick (2011) [A,13h59m|NF,BI|2016-07-15,6/10]
275. **Annihilation** (Star Force #7) by B.V. Larson (2013) [A,12h50m|FC,SF|2016-07-29,7/10]
276. **Storm Assault** (Star Force #8) by B.V. Larson (2013) [A,11h50m|FC,SF|2016-08-16,7/10]
277. **In the Plex** by Steven Levy (2011) [A,19h46m|NF,BU|2016-09-09,5/10]
278. **The Dead Sun** (Star Force #9) by B.V. Larson (2013) [A,12h27m|FC,SF|2016-09-22,7/10]
279. **The Girl on the Train** by Paula Hawkins (2015) [A,10h58m|FC,MY,TH|2016-09-28,7/10]
280. **Deep Work** by Cal Newport (2016) [A,7h44m|NF,SH|2016-10-07,8/10]
281. **Outcast** (Star Force #10) by B.V. Larson and David VanDyke (2014) [A,13h45m|FC,SF|2016-10-24,7/10]
282. **Journey to the Center of the Earth** by Jules Verne (1864) [A,8h20m|FC,SF,CL|2016-11-05,5/10]
283. **Exile** (Star Force #11) by B.V. Larson and David VanDyke (2014) [A,12h55m|FC,SF|2016-11-22,7/10]
284. **Bye Bye Banks?** by James Haycock (2015) [B,130p|NF,FI|2016-11-29,5/10]
285. **Demon Star** (Star Force #12) by B.V. Larson and David VanDyke (2015) [A,12h22m|FC,SF|2016-12-13,7/10]
286. **The Most Wonderful Tales of the Year** (2016) [A,1h47m|NF|2016-12-15,3/10]
287. **Remote** by David Heinemeier Hansson and Jason Fried (2013) [A,3h22m|NF,BU|2016-12-16,7/10]
288. **The Fold** (Threshold #2) by Peter Clines (2015) [A,10h52m|FC,SF,HR|2016-12-31,6/10]

## 2017

This year I read 25 books.

289. **Always Looking Up** by Michael J. Fox (2008) [A,4h32m|NF,BI|2017-01-08,4/10]
290. **Essentialism** by Greg McKeown (2011) [A,6h14m|NF,SH|2017-01-16,8/10]
291. **Starfire** by B.V. Larson (2014) [A,13h01m|FC,SF|2017-02-04,5/10]
292. **The Life-Changing Magic of Tidying Up** (Magic Cleaning #1) by Marie Kondō (2011) [A,4h50m|NF,SH|2017-02-11,7/10]
293. **I'm Feeling Lucky** by Douglas Edwards (2011) [A,16h14m|NF,BI|2017-03-02,5/10]
294. **Steve Jobs** by Walter Isaacson (2011) [A,25h03m|NF,BI|2017-04-02,7/10]
295. **Dark Matter** by Blake Crouch (2016) [A,10h08m|FC,SF|2017-04-13,8/10]
296. **The Complete Stories of Sherlock Holmes, Volume 2** by Arthur Conan Doyle (2010) [A,27h50m|FC,MY,CL|2017-04-23,7/10]
297. **The Autobiography of Black Hawk** by Black Hawk (1833) [A,3h33m|NF,HI|2017-04-29,3/10]
298. **The Circle** by Dave Eggers (2013) [A,13h42m|FC,SF|2017-05-13,7/10]
299. **The Dead Zone** by Stephen King (1979) [A,16h12m|FC,HR|2017-06-03,8/10]
300. **Steel World** (Undying Mercenaries #1) by B.V. Larson (2013) [A,12h39m|FC,SF|2017-06-18,7/10]
301. **Area 51** (Area 51 #1) by Robert Doherty (1997) [A,10h14m|FC,SF|2017-07-04,5/10]
302. **Dust World** (Undying Mercenaries #2) by B.V. Larson (2014) [A,13h02m|FC,SF|2017-07-22,7/10]
303. **Radical Candor: Be a Kick-Ass Boss Without Losing Your Humanity** by Kim Malone Scott (2017) [A,9h23m|NF,BU|2017-08-05,7/10]
304. **The Gunslinger** (The Dark Tower #1) by Stephen King (1982) [A,7h20m|FC,FY|2017-08-16,8/10]
305. **Tech World** (Undying Mercenaries #3) by B.V. Larson (2014) [A,13h13m|FC,SF|2017-09-03,7/10]
306. **Originals: How Non-Conformists Move the World** by Adam M. Grant (2016) [A,10h01m|NF,BU|2017-09-19,7/10]
307. **Machine World** (Undying Mercenaries #4) by B.V. Larson (2015) [A,13h18m|FC,SF|2017-10-05,7/10]
308. **Will Save the Galaxy for Food** (Jacques McKeown #1) by Yahtzee Croshaw (2017) [A,10h20m|FC,SF|2017-10-20,5/10]
309. **Death World** (Undying Mercenaries #5) by B.V. Larson (2015) [A,13h19m|FC,SF|2017-11-05,7/10]
310. **The Butterfly Effect** by Jon Ronson (2017) [A,3h25m|NF|2017-11-12,3/10]
311. **Christine** by Stephen King (1983) [A,19h35m|FC,HR|2017-12-02,7/10]
312. **==Sapiens: A Brief History of Humankind==** by Yuval Noah Harari (2011) [A,15h17m|NF,HI,SC|2017-12-20,9/10]
313. **Gather 'Round the Sound** by Paulo Coelho, Yvonne Morrison, Charles Dickens (2017) [A,1h12m|FC|2017-12-21,4/10]

## 2018

This year I read 46 books.

314. **==1984==** by George Orwell (1949) [A,11h22m|FC,SF,CL|2018-01-01,9/10]
315. **Home World** (Undying Mercenaries #6) by B.V. Larson (2016) [A,12h55m|FC,SF|2018-01-14,7/10]
316. **Tribes: We Need You to Lead Us** by Seth Godin (2008) [A,3h42m|NF,BU|2018-01-18,5/10]
317. **Alien: Covenant Origins** by Alan Dean Foster (2017) [A,8h31m|FC,SF|2018-01-26,5/10]
318. **The Girl in the Spider's Web** (Millennium #4) by David Lagercrantz (2015) [A,16h46m|FC,MY|2018-02-11,7/10]
319. **So Good They Can't Ignore You** by Cal Newport (2012) [A,6h28m|NF,BU|2018-02-18,7/10]
320. **Artemis** by Andy Weir (2017) [A,8h57m|FC,SF|2018-02-26,7/10]
321. **The Dispatcher** (The Dispatcher #1) by John Scalzi (2016) [A,2h18m|FC,SF|2018-02-28,5/10]
322. **Origin** (Robert Langdon #5) by Dan Brown (2017) [A,18h10m|FC,TH,MY|2018-03-17,7/10]
323. **Homo Deus: A Brief History of Tomorrow** by Yuval Noah Harari (2016) [A,14h54m|NF,HI,SC|2018-03-30,7/10]
324. **Astrophysics for People in a Hurry** by Neil deGrasse Tyson (2017) [A,3h41m|NF,SC|2018-04-01,7/10]
325. **Brave New World** by Aldous Huxley (1932) [A,8h00m|FC,SF,CL|2018-04-08,7/10]
326. **Goodbye, Things: The New Japanese Minimalism** by Fumio Sasaki (2015) [A,4h32m|NF,SH|2018-04-12,5/10]
327. **Damocles** by S.G. Redling (2013) [A,10h33m|FC,SF|2018-04-22,5/10]
328. **Rogue World** (Undying Mercenaries #7) by B.V. Larson (2017) [A,12h39m|FC,SF|2018-05-04,7/10]
329. **The Girl Who Takes an Eye for an Eye** (Millennium #5) by David Lagercrantz (2017) [A,13h32m|FC,MY|2018-05-18,6/10]
330. **Alien: Covenant** by Alan Dean Foster (2017) [A,8h58m|FC,SF|2018-05-27,6/10]
331. **Endurance: Shackleton's Incredible Voyage** by Alfred Lansing (1959) [A,10h20m|NF,HI|2018-06-05,5/10]
332. **I Am Legend** by Richard Matheson (1954) [A,5h20m|FC,SF,HR|2018-06-10,8/10]
333. **Blood World** (Undying Mercenaries #8) by B.V. Larson (2017) [A,12h31m|FC,SF|2018-06-25,7/10]
334. **The Good Girl** by Mary Kubica (2014) [A,10h38m|FC,MY|2018-07-08,5/10]
335. **Alien** by Alan Dean Foster (1979) [A,9h03m|FC,SF|2018-07-18,7/10]
336. **West Cork** by Sam Bungey and Jennifer Forde (2018) [A,7h50m|NF,CR|2018-07-27,5/10]
337. **Meditations** by Marcus Aurelius (0180) [A,5h11m|NF,PH,CL|2018-07-29,5/10]
338. **Invisible Man** by Ralph Ellison (1952) [A,18h36m|FC,CL|2018-08-12,5/10]
339. **Dark World** (Undying Mercenaries #9) by B.V. Larson (2018) [A,13h28m|FC,SF|2018-08-23,7/10]
340. **==The Hobbit==** by J.R.R. Tolkien (1937) [A,11h05m|FC,FY,CL|2018-09-02,9/10]
341. **Alien: Out of the Shadows** by Tim Lebbon (2013) [A,4h28m|FC,SF|2018-09-07,5/10]
342. **Island** by Aldous Huxley (1962) [A,11h27m|FC,SF,CL|2018-09-16,6/10]
343. **==Cosmos==** by Carl Sagan (1980) [A,14h31m|NF,SC|2018-09-29,9/10]
344. **==The Time Machine==** by H.G. Wells (1895) [A,3h22m|FC,SF,CL|2018-10-02,10/10]
345. **==The Name of the Wind==** (The Kingkiller Chronicle #1) by Patrick Rothfuss (2007) [A,27h55m|FC,FY|2018-10-24,9/10]
346. **The Playground** by Ray Bradbury (1953) [A,44m|FC|2018-10-25,7/10]
347. **Everything All at Once** by Bill Nye (2017) [A,12h35m|NF,BI,SC|2018-11-04,5/10]
348. **==Pet Sematary==** by Stephen King (1983) [A,15h41m|FC,HR|2018-11-17,9/10]
349. **Alien: River of Pain** by Christopher Golden (2014) [A,4h52m|FC,SF|2018-11-21,5/10]
350. **All Systems Red** (The Murderbot Diaries #1) by Martha Wells (2017) [A,3h17m|FC,SF|2018-11-25,7/10]
351. **Deliverance** by James Dickey (1970) [A,7h31m|FC|2018-12-02,5/10]
352. **Light Falls** by Brian Greene (2016) [A,2h24m|NF,SC|2018-12-05,7/10]
353. **Paradox Bound** by Peter Clines (2017) [A,12h31m|FC,SF|2018-12-15,7/10]
354. **The Complete Stories of Sherlock Holmes, Volume 3** by Arthur Conan Doyle (2010) [A,22h35m|FC,MY,CL|2018-12-16,7/10]
355. **The Christmas Hirelings** by Mary Elizabeth Braddon (1894) [A,3h53m|FC|2018-12-19,5/10]
356. **Jingle Bell Pop** by John Seabrook (2018) [A,1h14m|NF|2018-12-20,3/10]
357. **The Night Before Christmas** by Clement C. Moore (1823) [A,4m|FC,CL|2018-12-20,5/10]
358. **Blade Runner** (Blade Runner #1) by Philip K. Dick (1968) [A,9h12m|FC,SF|2018-12-28,8/10]
359. **The Adventure of the Blue Carbuncle** by Arthur Conan Doyle (1892) [A,46m|FC,MY,CL|2018-12-29,5/10]

## 2019

This year I read 61 books.

360. **Superintelligence: Paths, Dangers, Strategies** by Nick Bostrom (2014) [A,13h29m|NF,SC|2019-01-09,6/10]
361. **The B-Team** (The Human Division #1) by John Scalzi (2013) [A,2h20m|FC,SF|2019-01-11,5/10]
362. **The Art of Invisibility** by Kevin Mitnick (2017) [A,9h17m|NF,SC|2019-01-18,7/10]
363. **Walk the Plank** (The Human Division #2) by John Scalzi (2013) [A,39m|FC,SF|2019-01-18,5/10]
364. **Bad Blood: Secrets and Lies in a Silicon Valley Startup** by John Carreyrou (2018) [A,11h37m|NF,BU|2019-01-27,7/10]
365. **We Only Need the Heads** (The Human Division #3) by John Scalzi (2013) [A,1h06m|FC,SF|2019-01-27,5/10]
366. **Autonomous** by Annalee Newitz (2017) [A,10h27m|FC,SF|2019-02-05,7/10]
367. **A Voice in the Wilderness** (The Human Division #4) by John Scalzi (2013) [A,49m|FC,SF|2019-02-06,5/10]
368. **I, Robot** (Robot Series #0.1) by Isaac Asimov (1950) [A,8h20m|FC,SF|2019-02-12,7/10]
369. **Tales from the Clarke** (The Human Division #5) by John Scalzi (2013) [A,59m|FC,SF|2019-02-13,5/10]
370. **Altered Carbon** (Takeshi Kovacs #1) by Richard K. Morgan (2002) [A,17h14m|FC,SF|2019-02-24,7/10]
371. **The Back Channel** (The Human Division #6) by John Scalzi (2013) [A,47m|FC,SF|2019-02-25,5/10]
372. **Hitchhiker's Guide to the Galaxy** (Hitchhiker's Guide to the Galaxy #1) by Douglas Adams (1979) [A,5h51m|FC,SF|2019-03-03,8/10]
373. **==Children of Time==** (Children of Time #1) by Adrian Tchaikovsky (2015) [A,16h31m|FC,SF|2019-03-16,9/10]
374. **The Dog King** (The Human Division #7) by John Scalzi (2013) [A,58m|FC,SF|2019-03-17,5/10]
375. **Flatland** by Edwin A. Abbott (1884) [A,4h03m|FC,SF|2019-03-19,5/10]
376. **==Ender's Game==** (Ender's Saga #1) by Orson Scott Card (1985) [A,11h57m|FC,SF|2019-03-28,10/10]
377. **The Sound of Rebellion** (The Human Division #8) by John Scalzi (2013) [A,41m|FC,SF|2019-03-28,5/10]
378. **Just Six Numbers** by Martin J. Rees (1999) [A,3h20m|NF,SC|2019-03-31,5/10]
379. **Storm World** (Undying Mercenaries #10) by B.V. Larson (2018) [A,16h06m|FC,SF|2019-04-12,7/10]
380. **21 Lessons for the 21st Century** by Yuval Noah Harari (2018) [A,11h41m|NF,HI|2019-04-22,7/10]
381. **Starship Pandora** by B.V. Larson (2018) [A,4h23m|FC,SF|2019-04-24,5/10]
382. **The Stars My Destination** by Alfred Bester (1955) [A,8h27m|FC,SF|2019-05-01,6/10]
383. **The Observers** (The Human Division #9) by John Scalzi (2013) [A,1h09m|FC,SF|2019-05-02,5/10]
384. **Girls & Boys** by Dennis Kelly (2018) [A,1h46m|FC|2019-05-05,3/10]
385. **This Must Be the Place** (The Human Division #10) by John Scalzi (2013) [A,44m|FC,SF|2019-05-12,5/10]
386. **==We Are Legion (We Are Bob)==** (Bobiverse #1) by Dennis E. Taylor (2016) [A,9h56m|FC,SF|2019-05-12,9/10]
387. **Hi Bob!** by Bob Newhart (2018) [A,3h35m|FC,HU|2019-05-14,3/10]
388. **Broken Angels** (Takeshi Kovacs #2) by Richard K. Morgan (2003) [A,15h40m|FC,SF|2019-05-26,5/10]
389. **A Problem of Proportion** (The Human Division #11) by John Scalzi (2013) [A,1h03m|FC,SF|2019-05-27,5/10]
390. **The Invisible Man** by H.G. Wells (1897) [A,4h36m|FC,SF,CL|2019-05-31,5/10]
391. **The Gentle Art of Cracking Heads** (The Human Division #12) by John Scalzi (2013) [A,38m|FC,SF|2019-06-01,5/10]
392. **Foundation** (Foundation #1) by Isaac Asimov (1951) [A,8h37m|FC,SF,CL|2019-06-08,8/10]
393. **Earth Below, Sky Above** (The Human Division #13) by John Scalzi (2013) [A,2h00m|FC,SF|2019-06-09,5/10]
394. **The Martian** (The Martian #1) by Andy Weir (2012) [A,10h53m|FC,SF|2019-06-18,8/10]
395. **Lullaby** by Jonathan Maberry (2018) [A,37m|FC,HR|2019-06-19,4/10]
396. **The War of the Worlds** by H.G. Wells (1898) [A,6h50m|FC,SF,CL|2019-06-24,7/10]
397. **Out of My Mind** by Alan Arkin (2018) [A,2h17m|NF,BI|2019-06-26,3/10]
398. **The Future of Humanity** by Michio Kaku (2018) [A,12h22m|NF,SC|2019-07-05,7/10]
399. **Alien: Sea of Sorrows** by James A. Moore (2014) [A,5h07m|FC,SF|2019-07-08,5/10]
400. **Twain's Feast** by Nick Offerman (2018) [A,4h27m|NF,HI|2019-07-10,4/10]
401. **==The Talisman==** by Stephen King and Peter Straub (1984) [A,28h00m|FC,HR,FY|2019-08-01,9/10]
402. **Around the World in 80 Days** by Jules Verne (1873) [A,6h49m|FC,CL|2019-08-07,5/10]
403. **Power Moves** by Adam M. Grant (2019) [A,3h04m|NF,BU|2019-08-09,5/10]
404. **Fahrenheit 451** by Ray Bradbury (1953) [A,5h01m|FC,SF,CL|2019-08-12,8/10]
405. **Origin Story** by David Christian (2018) [A,12h23m|NF,HI,SC|2019-08-19,5/10]
406. **The Island of Doctor Moreau** by H.G. Wells (1896) [A,4h52m|FC,SC,CL|2019-08-24,5/10]
407. **No Country for Old Men** by Cormac McCarthy (2005) [A,7h29m|FC,TH,MY|2019-08-29,6/10]
408. **Norse Mythology** by Neil Gaiman (2017) [A,6h30m|FC,FY|2019-09-03,7/10]
409. **14** (Threshold #1) by Peter Clines (2012) [A,12h34m|FC,SF|2019-09-12,8/10]
410. **Armor World** (Undying Mercenaries #11) by B.V. Larson (2019) [A,11h15m|FC,SF|2019-09-22,7/10]
411. **==The Three-Body Problem==** (Remembrance of Earth's Past #1) by Liu Cixin (2008) [A,14h37m|FC,SF|2019-10-04,9/10]
412. **Victorian Secrets** by John Woolf and Nick Baker (2018) [A,7h33m|NF,HI|2019-10-09,4/10]
413. **For We Are Many** (Bobiverse #2) by Dennis E. Taylor (2017) [A,8h59m|FC,SF|2019-10-16,7/10]
414. **Dodge & Twist** by Tony Lee (2011) [A,4h56m|FC|2019-10-21,4/10]
415. **All These Worlds** (Bobiverse #3) by Dennis E. Taylor (2017) [A,7h56m|FC,SF|2019-10-28,7/10]
416. **Life 3.0** by Max Tegmark (2017) [A,13h29m|NF,SC|2019-11-11,6/10]
417. **Recursion** by Blake Crouch (2019) [A,10h47m|FC,SF|2019-11-21,8/10]
418. **The Dark Forest** (Remembrance of Earth's Past #2) by Liu Cixin (2008) [A,23h04m|FC,SF|2019-12-11,8/10]
419. **Clone World** (Undying Mercenaries #12) by B.V. Larson (2019) [A,11h44m|FC,SF|2019-12-20,7/10]
420. **The Demon Next Door** by Bryan Burrough (2019) [A,2h45m|NF,CR|2019-12-23,4/10]

## 2020

This year I read 51 books.

421. **Death's End** (Remembrance of Earth's Past #3) by Liu Cixin (2010) [A,29h11m|FC,SF|2020-01-21,8/10]
422. **The Man on the Mountaintop** by Susan Trott (2017) [A,5h45m|FC|2020-01-26,4/10]
423. **Annihilation** (Southern Reach #1) by Jeff VanderMeer (2014) [A,6h00m|FC,SF|2020-01-31,6/10]
424. **Killer by Nature** by Jan Smith (2017) [A,4h32m|NF,TR,MY|2020-02-03,4/10]
425. **Permanent Record** by Edward Snowden (2019) [A,11h31m|NF,BI|2020-02-12,7/10]
426. **Aliens** by Alan Dean Foster (1986) [A,9h48m|FC,SF|2020-02-19,7/10]
427. **Junk** by Les Bohem (2019) [A,10h37m|FC,SF|2020-02-25,5/10]
428. **Elizabeth II: Life of a Monarch** by Ruth Cowen (2017) [A,3h47m|NF,BI,HI|2020-02-28,4/10]
429. **==2001: A Space Odyssey==** (Space Odyssey #1) by Arthur C. Clarke (1968) [A,6h42m|FC,SF,CL|2020-03-03,9/10]
430. **Children of Ruin** (Children of Time #2) by Adrian Tchaikovsky (2019) [A,15h25m|FC,SF|2020-03-17,6/10]
431. **Remote** by David Heinemeier Hansson and Jason Fried (2013) [A,3h22m|NF,BU|2020-03-20,7/10]
432. **The 3-Day Effect** by Florence Williams (2018) [A,3h01m|NF|2020-03-22,4/10]
433. **Spaceman** by Mike Massimino (2016) [A,10h54m|NF,SC,BI|2020-03-31,7/10]
434. **Alien: The Cold Forge** by Alex White (2018) [A,11h52m|FC,SF|2020-04-09,7/10]
435. **Glass World** (Undying Mercenaries #13) by B.V. Larson (2020) [A,12h07m|FC,SF|2020-04-19,7/10]
436. **A Grown-Up Guide to Dinosaurs** by Ben Garrod (2019) [A,2h43m|NF,SC|2020-04-21,5/10]
437. **Einstein's Relativity and the Quantum Revolution** by Richard Wolfson (2000) [A,12h17m|NF,SC|2020-04-30,7/10]
438. **Stan Lee's Alliances: A Trick of Light** by Stan Lee (2019) [A,11h48m|FC,SF|2020-05-10,5/10]
439. **The Man Who Knew the Way to the Moon** by Todd Zwillich (2019) [A,3h33m|NF,HI|2020-05-11,5/10]
440. **==The Eye of the World==** (The Wheel of Time #1) by Robert Jordan (1990) [A,29h57m|FC,FY|2020-06-04,9/10]
441. **The Darkwater Bride** by Marty Ross (2018) [A,6h45m|FC,MY,HR|2020-06-09,5/10]
442. **==Contact==** by Carl Sagan (1985) [A,14h45m|FC,SF|2020-06-21,9/10]
443. **Reverse Transmission** by Param Anand Singh (2018) [A,2h32m|FC,SF|2020-06-22,3/10]
444. **Level Five** (Killday #1) by William Ledbetter (2018) [A,11h34m|FC,SF|2020-07-01,7/10]
445. **Wally Roux, Quantum Mechanic** by Nick Carr (2019) [A,1h55m|FC,SF|2020-07-03,3/10]
446. **Starship Troopers** by Robert A. Heinlein (1959) [A,9h52m|FC,SF|2020-07-12,6/10]
447. **Understanding Complexity** by Scott E. Page (2009) [A,6h04m|NF,SC|2020-07-16,5/10]
448. **==Interview with the Vampire==** (The Vampire Chronicles #1) by Anne Rice (1976) [A,14h25m|FC,HR,FY|2020-07-26,9/10]
449. **Body of Proof** by Darrell Brown and Sophie Ellis (2019) [A,5h09m|NF|2020-07-30,5/10]
450. **Dead Moon** (Threshold #3) by Peter Clines (2019) [A,11h23m|FC,SF,HR|2020-08-09,6/10]
451. **The Burnout Generation** by Anne Helen Petersen (2019) [A,1h48m|NF|2020-08-09,5/10]
452. **Lock In** (Lock In #1) by John Scalzi (2014) [A,9h57m|FC,SF|2020-08-16,7/10]
453. **Carmilla** by Joseph Sheridan Le Fanu (1872) [A,2h22m|FC,HR,CL|2020-08-18,5/10]
454. **Space Force** by Jeremy Robinson (2018) [A,9h34m|FC,SF|2020-08-25,5/10]
455. **How We'll Live on Mars** by Stephen Petranek (2014) [A,2h14m|NF,SC|2020-08-27,5/10]
456. **Head On** (Lock In #2) by John Scalzi (2018) [A,7h37m|FC,SF|2020-09-01,7/10]
457. **The Sentient Machine: The Coming Age of Artificial Intelligence** by Amir Husain (2017) [A,5h45m|NF,SC|2020-09-06,5/10]
458. **Carnival Row: Tangle in the Dark** by Stephanie K. Smith (2019) [A,3h08m|FC,FY|2020-09-07,3/10]
459. **The Girl Who Lived Twice** (Millennium #6) by David Lagercrantz (2019) [A,11h47m|FC,MY|2020-09-18,6/10]
460. **Bedtime Stories for Cynics** by Dave Hill (2017) [A,1h42m|FC,HU|2020-09-20,4/10]
461. **Terminus** (Threshold #4) by Peter Clines (2020) [A,11h21m|FC,SF,HR|2020-09-27,8/10]
462. **The Minimalist Way** by Erica Layne (2019) [A,4h00m|NF,SH|2020-09-29,5/10]
463. **==It==** by Stephen King (1986) [A,44h56m|FC,HR|2020-10-25,10/10]
464. **More Bedtime Stories for Cynics** by Dave Hill (2019) [A,3h25m|FC,HU|2020-10-26,4/10]
465. **Heaven's River** (Bobiverse #4) by Dennis E. Taylor (2020) [A,16h57m|FC,SF|2020-11-08,7/10]
466. **Alien III: An Audible Original Drama** by William Gibson (2019) [A,2h16m|FC,SF|2020-11-09,4/10]
467. **The Wise Man's Fear** (The Kingkiller Chronicle #2) by Patrick Rothfuss (2011) [A,42h55m|FC,FY|2020-12-08,8/10]
468. **The Power of Self-Compassion** by Laurie J. Cameron (2020) [A,4h15m|NF,SH|2020-12-10,5/10]
469. **Ready Player Two** (Ready Player One #2) by Ernest Cline (2020) [A,13h46m|FC,SF|2020-12-20,5/10]
470. **Artificial Condition** (The Murderbot Diaries #2) by Martha Wells (2018) [A,3h21m|FC,SF|2020-12-22,6/10]
471. **Edge World** (Undying Mercenaries #14) by B.V. Larson (2020) [A,16h15m|FC,SF|2020-12-31,7/10]

## 2021

This year I read 65 books.

472. **Rogue Protocol** (The Murderbot Diaries #3) by Martha Wells (2018) [A,3h46m|FC,SF|2021-01-03,6/10]
473. **Cut and Run** by Ben Acker and Ben Blacker (2020) [A,2h44m|FC|2021-01-07,5/10]
474. **Awkward** by Ty Tashiro (2017) [A,6h57m|NF,PY|2021-01-17,5/10]
475. **Exit Strategy** (The Murderbot Diaries #4) by Martha Wells (2018) [A,3h46m|FC,SF|2021-01-21,6/10]
476. **Gardens of the Moon** (Malazan Book of the Fallen #1) by Steven Erikson (1999) [E,567p|FC,FY|2021-01-23,9/10]
477. **The Adventures of Tom Sawyer** (Adventures of Tom and Huck #1) by Mark Twain (1876) [A,7h52m,E,257p|FC,CL|2021-01-27,7/10]
478. **Everybody Lies** by Seth Stephens-Davidowitz (2017) [A,7h40m|NF,SC|2021-02-01,7/10]
479. **Just Do It** by Donald R. Katz (1994) [A,14h01m|NF,BI,BU|2021-02-17,5/10]
480. **Alien 3** by Alan Dean Foster (1992) [A,7h31m|FC,SF|2021-02-26,5/10]
481. **Deadhouse Gates** (Malazan Book of the Fallen #2) by Steven Erikson (2000) [E,690p|FC,FY|2021-03-09,9/10]
482. **The Andromeda Strain** (Andromeda #1) by Michael Crichton (1969) [E,235p|FC,SF|2021-03-15,7/10]
483. **Bruce Lee: A Life** by Matthew Polly (2018) [A,19h06m|NF,BI|2021-03-16,7/10]
484. **The Slow Regard of Silent Things** (The Kingkiller Chronicle #2.5) by Patrick Rothfuss (2014) [E,93p|FC,FY|2021-03-19,6/10]
485. **The Terminal Man** by Michael Crichton (1972) [A,6h31m|FC,SF|2021-03-22,6/10]
486. **We** by Yevgeny Zamyatin (1920) [E,208p|FC,SF|2021-03-29,6/10]
487. **Different Seasons** by Stephen King (1982) [A,19h49m|FC,HR,SS|2021-04-12,8/10]
488. **The Great Train Robbery** by Michael Crichton (1975) [A,8h39m|FC|2021-04-24,5/10]
489. **Malcolm and Me** by Ishmael Reed (2020) [A,1h39m|NF|2021-04-25,3/10]
490. **The Science of Sci-Fi: From Warp Speed to Interstellar Travel** by Erin Macdonald (2019) [A,3h59m|NF,SC|2021-05-01,5/10]
491. **Eaters of the Dead** by Michael Crichton (1976) [A,5h19m|FC|2021-05-06,5/10]
492. **Memories of Ice** (Malazan Book of the Fallen #3) by Steven Erikson (2001) [E,913p|FC,FY|2021-05-10,9/10]
493. **Alien Resurrection** by A.C. Crispin (1997) [A,9h46m|FC,SF|2021-05-16,5/10]
494. **Network Effect** (The Murderbot Diaries #5) by Martha Wells (2020) [E,303p|FC|2021-05-18,6/10]
495. **Congo** by Michael Crichton (1980) [E,319p|FC|2021-05-23,7/10]
496. **Project Hail Mary** by Andy Weir (2021) [A,16h10m|FC,SF|2021-06-03,8/10]
497. **Alone with the Stars** by David R. Gillham (2020) [A,2h10m|FC|2021-06-06,4/10]
498. **==House of Chains==** (Malazan Book of the Fallen #4) by Steven Erikson (2002) [E,761p|FC,FY|2021-06-14,9/10]
499. **The Eyes of the Dragon** by Stephen King (1987) [A,10h23m|FC,FY|2021-06-15,5/10]
500. **Caffeine: How Caffeine Created the Modern World** by Michael Pollan (2020) [A,2h02m|NF,HI,SC|2021-06-19,5/10]
501. **Kings of the Wyld** (The Band #1) by Nicholas Eames (2017) [E,460p|FC,FY|2021-06-27,7/10]
502. **==Sphere==** by Michael Crichton (1987) [E,341p|FC,SF|2021-07-03,9/10]
503. **==Red Rising==** (Red Rising Saga #1) by Pierce Brown (2014) [E,375p|FC,SF|2021-07-10,10/10]
504. **The Vampire Lestat** (The Vampire Chronicles #2) by Anne Rice (1985) [A,21h40m|FC,HR,FY|2021-07-12,7/10]
505. **==The Forever War==** (The Forever War #1) by Joe Haldeman (1974) [E,281p|FC,SF|2021-07-15,10/10]
506. **Forward: Stories of Tomorrow** by Blake Crouch, Veronica Roth, N.K. Jemisin, Amor Towles, Paul Tremblay and Andy Weir (2019) [A,8h24m|FC,SF,SS|2021-07-23,5/10]
507. **The Messengers** by Lindsay Joelle (2020) [A,1h20m|FC,SF|2021-07-24,5/10]
508. **The Space Race** by Colin Brake, Patrick Chapman, Richard Hollingham, Richard Kurti, Sue Nelson, Helen Quigley and Andrew Mark Sewell (2019) [A,9h12m|NF,SC|2021-08-02,5/10]
509. **==Midnight Tides==** (Malazan Book of the Fallen #5) by Steven Erikson (2004) [E,688p|FC,FY|2021-08-05,9/10]
510. **==Jurassic Park==** (Jurassic Park #1) by Michael Crichton (1990) [E,401p|FC,SF|2021-08-11,9/10]
511. **Green World** (Undying Mercenaries #15) by B.V. Larson (2021) [A,12h54m|FC,SF|2021-08-15,7/10]
512. **The Real Sherlock** by Lucinda Hawksley (2019) [A,2h05m|NF,BI|2021-08-20,5/10]
513. **==Dune==** (Dune #1) by Frank Herbert (1965) [E,615p|FC,SF|2021-08-23,10/10]
514. **How to Build Meaningful Relationships through Conversations** by Carol Ann Lloyd (2020) [A,5h06m|NF,SH|2021-08-25,5/10]
515. **Fugitive Telemetry** (The Murderbot Diaries #6) by Martha Wells (2021) [A,4h24m|FC,SF|2021-08-29,6/10]
516. **==Golden Son==** (Red Rising Saga #2) by Pierce Brown (2015) [E,435p|FC,SF|2021-09-01,10/10]
517. **==Forever Peace==** (The Forever War #2) by Joe Haldeman (1997) [E,274p|FC,SF|2021-09-07,10/10]
518. **Night Shift** by Stephen King (1978) [E,321p|FC,HR,SS|2021-09-14,7/10]
519. **The Queen of the Damned** (The Vampire Chronicles #3) by Anne Rice (1988) [A,20h03m|FC,HR,FY|2021-09-20,6/10]
520. **Thicker Than Water** by Tyler Shultz (2020) [A,3h37m|FC,BI,SC|2021-09-25,5/10]
521. **Rising Sun** by Michael Crichton (1992) [A,11h45m|FC,TH|2021-10-09,7/10]
522. **Murder by Other Means** (The Dispatcher #2) by John Scalzi (2020) [A,3h33m|FC,SF|2021-10-11,5/10]
523. **==The Bonehunters==** (Malazan Book of the Fallen #6) by Steven Erikson (2006) [E,906p|FC,FY|2021-10-14,9/10]
524. **Thinner** by Stephen King writing as Richard Bachman (1984) [A,10h07m|FC,HR|2021-10-21,7/10]
525. **Dune Messiah** (Dune #2) by Frank Herbert (1969) [E,240p|FC,SF|2021-10-22,8/10]
526. **Biography of Resistance** by Muhammad H. Zaman (2020) [A,8h03m|NF,SC|2021-10-29,5/10]
527. **Mars: Our Future on the Red Planet** by Leonard David (2016) [A,3h27m|NF,SC|2021-10-31,5/10]
528. **==Morning Star==** (Red Rising Saga #3) by Pierce Brown (2016) [E,622p|FC,SF|2021-11-02,10/10]
529. **==Forever Free==** (The Forever War #3) by Joe Haldeman (1999) [E,270p|FC,SF|2021-11-09,10/10]
530. **Misery** by Stephen King (1987) [A,12h11m|FC,HR|2021-11-14,7/10]
531. **==Childhood's End==** by Arthur C. Clarke (1953) [E,210p|FC,SF|2021-11-15,9/10]
532. **Joy at Work** (Magic Cleaning #3) by Marie Kondō and Scott Sonenshein (2020) [A,5h19m|NF,SH|2021-11-21,5/10]
533. **Death by Black Hole** by Neil deGrasse Tyson (2006) [A,12h04m|NF,SC|2021-12-04,5/10]
534. **==Reaper's Gale==** (Malazan Book of the Fallen #7) by Steven Erikson (2007) [E,944p|FC,FY|2021-12-08,9/10]
535. **The Long Walk** by Stephen King writing as Richard Bachman (1979) [A,10h44m|FC,HR|2021-12-14,5/10]
536. **Children of Dune** (Dune #3) by Frank Herbert (1976) [E,470p|FC,SF|2021-12-20,8/10]

## 2022

This year I read 55 books.

537. **Ice World** (Undying Mercenaries #16) by B.V. Larson (2021) [A,13h00m|FC,SF|2022-01-01,7/10]
538. **Iron Gold** (Red Rising Saga #4) by Pierce Brown (2018) [E,598p|FC,SF|2022-01-03,10/10]
539. **Origins: Fourteen Billion Years of Cosmic Evolution** by Neil deGrasse Tyson and Donald Goldsmith (2004) [A,8h39m|NF,SC|2022-01-09,5/10]
540. **Roadwork** by Stephen King writing as Richard Bachman (1981) [A,9h37m|FC,HR|2022-01-19,7/10]
541. **The Art of War** by Sun Tzu (-500) [A,1h07m|NF,CL|2022-01-20,5/10]
542. **==The Dragonbone Chair==** (Memory, Sorrow, and Thorn # 1) by Tad Williams (1988) [E,796p|FC,FY|2022-01-28,10/10]
543. **The Gospel According to Jesus Christ** by José Saramago (1991) [A,13h01m|FC|2022-02-01,6/10]
544. **The Running Man** by Stephen King writing as Richard Bachman (1982) [A,7h42m|FC,HR|2022-02-09,7/10]
545. **Blindness** (Blindness #1) by José Saramago (1995) [A,12h33m|FC|2022-02-21,7/10]
546. **==Toll the Hounds==** (Malazan Book of the Fallen #8) by Steven Erikson (2008) [E,973p|FC,FY|2022-02-26,9/10]
547. **Starship Liberator** (Galactic Liberation #1) by David VanDyke and B.V. Larson (2016) [A,16h04m|FC,SF|2022-03-11,5/10]
548. **==Stone of Farewell==** (Memory, Sorrow, and Thorn #2) by Tad Williams (1990) [E,706p|FC,FY|2022-03-16,10/10]
549. **Disclosure** by Michael Crichton (1994) [E,397p|FC|2022-03-20,7/10]
550. **Galileo and the Science Deniers** by Mario Livio (2020) [A,8h47m|NF,SC|2022-03-20,5/10]
551. **God Emperor of Dune** (Dune #4) by Frank Herbert (1981) [E,441p|FC,SF|2022-03-29,8/10]
552. **==Dark Age==** (Red Rising Saga #5) by Pierce Brown (2019) [E,834p|FC,SF|2022-04-28,10/10]
553. **The Tommyknockers** by Stephen King (1987) [A,27h43m|FC,HR|2022-05-05,7/10]
554. **City World** (Undying Mercenaries #17) by B.V. Larson (2022) [A,12h58m|FC,SF|2022-05-22,7/10]
555. **Andrea Vernon and the Corporation for UltraHuman Protection** (Andrea Vernon #1) by Alexander C. Kane (2017) [A,8h50m|FC,FY|2022-06-05,5/10]
556. **==To Green Angel Tower==** (Memory, Sorrow, and Thorn #3) by Tad Williams (1993) [E,1333p|FC,FY|2022-06-18,10/10]
557. **Battleship Indomitable** (Galactic Liberation #2) by David VanDyke and B.V. Larson (2017) [A,16h17m|FC,SF|2022-06-19,5/10]
558. **Cain** by José Saramago (2009) [A,5h21m|FC|2022-06-26,7/10]
559. **Heretics of Dune** (Dune #5) by Frank Herbert (1984) [E,482p|FC,SF|2022-07-06,8/10]
560. **The Dark Half** by Stephen King (1989) [A,15h14m|FC,HR|2022-07-10,8/10]
561. **==The Lost World==** (Jurassic Park #2) by Michael Crichton (1995) [E,400p|FC,SF|2022-07-16,9/10]
562. **==The Handmaid's Tale==** (The Handmaid's Tale #1) by Margaret Atwood (1985) [E,298p|FC,SF|2022-07-25,9/10]
563. **Flagship Victory** (Galactic Liberation #3) by David VanDyke and B.V. Larson (2018) [A,16h20m|FC,SF|2022-07-31,5/10]
564. **Storm Front** (The Dresden Files #1) by Jim Butcher (2000) [E,240p|FC,FY|2022-08-01,7/10]
565. **The Man in the High Castle** by Philip K. Dick (1962) [E,232p|FC,SF|2022-08-05,7/10]
566. **==Upgrade==** by Blake Crouch (2022) [A,9h47m|FC,SF|2022-08-07,9/10]
567. **Rich Dad Poor Dad** (Rich Dad #1) by Robert T. Kiyosaki (1997) [A,6h09m|NF,SH|2022-08-14,7/10]
568. **Stock Investing For Beginners** by John Roberts (2017) [E,89p|NF,SH|2022-08-16,7/10]
569. **Chapterhouse: Dune** (Dune #6) by Frank Herbert (1985) [E,454p|FC,SF|2022-08-19,8/10]
570. **Dividend Investing for Beginners** by G.R. Tiberius (2021) [E,158p|NF,SH|2022-08-22,7/10]
571. **Fool Moon** (The Dresden Files #2) by Jim Butcher (2001) [E,277p|FC,FY|2022-08-28,7/10]
572. **Airframe** by Michael Crichton (1996) [E,371p|FC,TH|2022-09-02,7/10]
573. **==NOS4A2==** by Joe Hill (2013) [E,620p|FC,HR|2022-09-09,8/10]
574. **Four Past Midnight** by Stephen King (1990) [A,29h39m|FC,HR,SS|2022-09-11,8/10]
575. **==Cage of Souls==** by Adrian Tchaikovsky (2019) [E,500p|FC,SF|2022-09-30,9/10]
576. **Welcome to Dead House** (Goosebumps #1) by R.L. Stine (1992) [E,103p|FC,HR|2022-10-02,6/10]
577. **Hive War** (Galactic Liberation #4) by David VanDyke and B.V. Larson (2018) [A,15h31m|FC,SF|2022-10-09,5/10]
578. **==Let the Right One In==** by John Ajvide Lindqvist (2004) [E,401p|FC,HR|2022-10-12,9/10]
579. **All the Names** by José Saramago (1997) [A,8h59m|FC|2022-10-16,7/10]
580. **Born a Crime** by Trevor Noah (2016) [A,8h44m|NF,BI|2022-10-23,7/10]
581. **Grave Peril** (The Dresden Files #3) by Jim Butcher (2001) [E,323p|FC,FY|2022-10-23,7/10]
582. **The One Thing** by Gary Keller and Jay Papasan (2012) [A,5h24m|NF,SH|2022-10-30,7/10]
583. **==Revelation Space==** (Revelation Space #1) by Alastair Reynolds (2000) [E,583p|FC,SF|2022-11-18,9/10]
584. **Needful Things** by Stephen King (1991) [A,25h11m|FC,HR|2022-11-27,8/10]
585. **==The First Fifteen Lives of Harry August==** by Claire North (2014) [E,420p|FC,SF|2022-11-29,8/10]
586. **==Dogs of War==** (Dogs of War #1) by Adrian Tchaikovsky (2017) [E,250p|FC,SF|2022-12-08,9/10]
587. **Straker's Breakers** (Galactic Liberation #5) by David VanDyke and B.V. Larson (2019) [A,11h8m|FC,SF|2022-12-11,6/10]
588. **Timeline** by Michael Crichton (1999) [E,461p|FC,SF|2022-12-20,7/10]
589. **Summer Knight** (The Dresden Files #4) by Jim Butcher (2002) [E,310p|FC,FY|2022-12-23,7/10]
590. **Roadside Picnic** by Arkady Strugatsky and Boris Strugatsky (1972) [E,163p|FC,SF|2022-12-26,8/10]
591. **Sky World** (Undying Mercenaries #18) by B.V. Larson (2022) [A,14h11m|FC,SF|2022-12-31,7/10]

## 2023

This year I read 64 books.

592. **Skeleton Crew** by Stephen King (1985) [A,22h29m|FC,HR,SS|2023-01-15,7/10]
593. **==Redemption Ark==** (Revelation Space #2) by Alastair Reynolds (2002) [E,672p|FC,SF|2023-01-16,9/10]
594. **The Testaments** (The Handmaid's Tale #2) by Margaret Atwood (2019) [E,457p|FC,SF|2023-01-26,7/10]
595. **The Signal and the Noise** by Nate Silver (2012) [A,16h21m|NF,SC,BU|2023-02-05,7/10]
596. **==Boy's Life==** by Robert R. McCammon (1991) [E,504p|FC,FY|2023-02-10,10/10]
597. **==Bear Head==** (Dogs of War #2) by Adrian Tchaikovsky (2021) [E,294p|FC,SF|2023-02-22,9/10]
598. **Hell's Reach** (Galactic Liberation #6) by David VanDyke and B.V. Larson (2019) [A,12h51m|FC,SF|2023-02-26,6/10]
599. **The Secret Life of Walter Mitty** by James Thurber (1939) [A,15m|FC,CL,SS|2023-02-26,5/10]
600. **Death Masks** (The Dresden Files #5) by Jim Butcher (2003) [E,293p|FC,FY|2023-03-05,7/10]
601. **Gerald's Game** by Stephen King (1992) [A,13h34m|FC,HR|2023-03-12,8/10]
602. **Prey** by Michael Crichton (2002) [E,318p|FC,SF|2023-03-17,8/10]
603. **Until the End of Time** by Brian Greene (2020) [A,14h36m|NF,SC|2023-03-26,7/10]
604. **Star Runner** (Star Runner #1) by B.V. Larson (2020) [A,9h26m|FC,SF|2023-04-02,6/10]
605. **==Absolution Gap==** (Revelation Space #3) by Alastair Reynolds (2020) [E,790p|FC,SF|2023-04-12,9/10]
606. **Dolores Claiborne** by Stephen King (1992) [A,9h15m|FC,TH|2023-04-23,7/10]
607. **==Life of Pi==** by Yann Martel (2001) [E,359p|FC|2023-04-26,9/10]
608. **Dragons of Eden** by Carl Sagan (1977); g[A,6h41m|NF,SC|2023-04-30,7/10]
609. **State of Fear** by Michael Crichton (2004) [A,18h19m|FC,TH|2023-05-14,7/10]
610. **==The Final Empire==** (The Mistborn Saga #1) by Brandon Sanderson (2006) [E|FC,FY|2023-05-22,9/10]
611. **Jungle World** (Undying Mercenaries #19) by B.V. Larson (2023) [A,13h25m|FC,SF|2023-05-28,7/10]
612. **Elder Race** by Adrian Tchaikovsky (2021) [E,140p|FC,SF|2023-05-29,7/10]
613. **Digital Minimalism** by Cal Newport (2019) [A,6h59m|NF,SH|2023-06-04,7/10]
614. **Baal** by Robert R. McCammon (1978) [E,250p|FC,HR|2023-06-07,7/10]
615. **Blood Rites** (The Dresden Files #6) by Jim Butcher (2004) [E,349p|FC,FY|2023-06-19,7/10]
616. **The Tooth Fairy** by Graham Joyce (1996) [E,300p|FC,FY,HR|2023-06-27,8/10]
617. **Insomnia** by Stephen King (1994) [A,25h39m|FC,FY|2023-07-02,7/10]
618. **Fire Fight** (Star Runner #2) by B.V. Larson (2021) [A,10h16m|FC,SF|2023-07-09,6/10]
619. **How to Avoid a Climate Disaster** by Bill Gates (2021) [A,7h11m|NF,SC|2023-07-16,7/10]
620. **==Chasm City==** (Revelation Space #0.5) by Alastair Reynolds (2001) [E,631p|FC,SF|2023-07-25,9/10]
621. **Next** by Michael Crichton (2006) [E,421p|FC,SF|2023-08-01,7/10]
622. **==Building a Second Brain==** by Tiago Forte (2022) [E,244p|NF,SH,PR|2023-08-05,9/10]
623. **==The PARA Method==** by Tiago Forte (2023) [E,158p|NF,SH,PR|2023-08-19,9/10]
624. **Hearts in Atlantis** by Stephen King (1999) [A,20h9m|FC,FY,SS|2023-08-20,7/10]
625. **==The Well of Ascension==** (The Mistborn Saga #2) by Brandon Sanderson (2007) [E|FC,FY|2023-08-27,9/10]
626. **How It Unfolds** (The Far Reaches #1) by James S.A. Corey (2023) [E,27p|FC,SF|2023-08-28,7/10]
627. **Void** (The Far Reaches #2) by Veronica Roth (2023) [E,35p|FC,SF|2023-08-29,7/10]
628. **Androids and Aliens** (Star Runner #3) by B.V. Larson (2022) [A,9h17m|FC,SF|2023-09-03,6/10]
629. **The Private Life of Elder Things** by Adrian Tchaikovsky, Adam Gauntlett and Keris McDonald (2016) [E,208p|FC,HR,FY,SS|2023-09-05,6/10]
630. **A World Without Email** by Cal Newport (2021) [A,9h16m|NF,SH,PR|2023-09-10,8/10]
631. **==Lost Stars==** by Claudia Gray (2015) [E,318p|FC,SF|2023-09-14,9/10]
632. **Falling Bodies** (The Far Reaches #3) by Rebecca Roanhorse (2023) [E,26p|FC,SF|2023-09-15,6/10]
633. **The Long Game** (The Far Reaches #4) by Ann Leckie (2023) [E,27p|FC,SF|2023-09-16,7/10]
634. **Level Six** (Killday #2) by William Ledbetter (2020) [A,9h12m|FC,SF|2023-09-17,7/10]
635. **==Light Bringer==** (Red Rising Saga #6) by Pierce Brown (2023) [E,837p|FC,SF|2023-10-07,10/10]
636. **Stay Out of the Basement** (Goosebumps #2) by R.L. Stine (1992) [E,116p|FC,HR|2023-10-10,6/10]
637. **Elon Musk** by Walter Isaacson (2023) [A,20h27m|NF,BI|2023-10-15,7/10]
638. **Heart-Shaped Box** by Joe Hill (2007) [E,359p|FC,HR|2023-10-20,7/10]
639. **Monster Blood** (Goosebumps #3) by R.L. Stine (1992) [E,104p|FC,HR|2023-10-22,6/10]
640. **The Ocean at the End of the Lane** by Neil Gaiman (2013) [A,5h48m|FC,FY|2023-10-22,7/10]
641. **Horrorstör** by Grady Hendrix (2014) [E,181p|FC,HR|2023-10-28,7/10]
642. **Say Cheese and Die!** (Goosebumps #4) by R.L. Stine (1992) [E,110p|FC,HR|2023-10-29,6/10]
643. **The Halloween** Tree by Ray Bradbury (1972) [E,121p|FC,HR,FY|2023-11-01,7/10]
644. **Just Out of Jupiter's Reach** (The Far Reaches #5) by Nnedi Okorafor (2023) [E,33p|FC,SF|2023-11-01,7/10]
645. **Slow Time Between the Stars** (The Far Reaches #6) by John Scalzi (2023) [E,21p|FC,SF|2023-11-02,7/10]
646. **Rose Madder** by Stephen King (1995) [A,17h22m|FC,HR,FY|2023-11-05,7/10]
647. **Dead Beat** (The Dresden Files #7) by Jim Butcher (2005) [E,401p|FC,FY|2023-11-17,7/10]
648. **Red Company: First Strike!** (Red Company #1) by B.V. Larson (2023) [A,9h30m|FC,SF|2023-11-19,6/10]
649. **Bethany's Sin** by Robert R. McCammon (1980) [E,321p|FC,HR|2023-11-28,7/10]
650. **Parallel Worlds** by Michio Kaku (2013) [A,14h50m|NF,SC|2023-12-03,7/10]
651. **Diamond Dogs, Turquoise Days** (Revelation Space #1.5) by Alastair Reynolds (2003) [E,200p|FC,SF|2023-12-04,8/10]
652. **Crystal World** (Undying Mercenaries #20) by B.V. Larson (2023) [A,13h55m|FC,SF|2023-12-17,7/10]
653. **Who Goes There?** by John W. Campbell Jr. (1938) [A,2h35m|SC,HR|2023-12-17,7/10]
654. **==Wool==** (Silo #1) by Hugh Howey (2012) [E,493p|FC,SF|2023-12-20,8/10]
655. **Pirate Latitudes** by Michael Crichton (2009) [E,263p|FC|2023-12-22,7/10]

## 2024

This year I read 64 books.

656. **==The Hero of Ages==** (The Mistborn Saga #3) by Brandon Sanderson (2008) [E,844p|FC,FY|2024-01-10,9/10]
657. **Accessory to War** by Neil deGrasse Tyson and Avis Lang (2018) [A,18h39m|NF,SC|2024-01-14,7/10]
658. **A Call to Arms** (The Damned #1) by Alan Dean Foster (1991) [E,332p|FC,SF|2024-01-23,7/10]
659. **Precious Little Things** (Made Things #0.5) by Adrian Tchaikovsky (2017) [E,25p|FC,FY,SS|2024-01-23,7/10]
660. **The Regulators** by Stephen King writing as Richard Bachman (1996) [A,12h11m|FC,HR|2024-01-28,7/10]
661. **Star Wars Episode IV: A New Hope** by George Lucas (1976) [E,317p|FC,SF|2024-01-31,8/10]
662. **Red Company: Discovery** (Red Company #2) by B.V. Larson (2023) [A,9h31m|FC,SF|2024-02-11,6/10]
663. **Proven Guilty** (The Dresden Files #8) by Jim Butcher (2006) [E,417p|FC,FY|2024-02-12,7/10]
664. **Tau Zero** by Poul Anderson (1970) [E,200p|FC,SF|2024-02-16,7/10]
665. **The Night Boat** by Robert R. McCammon (1980) [E,258p|FC,HR|2024-02-24,7/10]
666. **The Sandman: Act I** by Neil Gaiman and Dirk Maggs (2020) [A,10h54m|FC,FY|2024-02-25,6/10]
667. **The Universe in a Nutshell** by Stephen Hawking (2001) [A,3h27m|NF,SC|2024-03-03,7/10]
668. **Micro** by Michael Crichton with Richard Preston (2011) [E,365p|FC,SF|2024-03-07,7/10]
669. **==The Green Mile==** by Stephen King (1996) [A,13h52m|FC,FY,HR|2024-03-17,10/10]
670. **Supernova Era** by Liu Cixin (2003) [E,355p|FC,SF|2024-03-19,7/10]
671. **Cycle of the Werewolf** by Stephen King (1983) [E,112p|FC,HR|2024-03-20,7/10]
672. **==Galactic North==** (Revelation Space #3.5) by Alastair Reynolds (2006) [E,313p|FC,SF|2024-03-31,8/10]
673. **Made Things** (Made Things #1) by Adrian Tchaikovsky (2019) [E,122p|FC,FY|2024-04-05,6/10]
674. **==Shift==** (Silo #2) by Hugh Howey (2013) [E,484p|FC,SF|2024-04-19,9/10]
675. **Total Recall** by Arnold Schwarzenegger (2012) [A,23h21m|NF,BI|2024-04-20,7/10]
676. **Minority Report and Other Stories** by Philip K. Dick (2001) [A,5h33m|FC,SF|2024-04-28,8/10]
677. **The Compound Effect** by Darren Hardy (2010) [A,4h44m|NF,SH|2024-05-05,7/10]
678. **==Warbreaker==** by Brandon Sanderson (2009) [E,739p|FC,FY|2024-05-11,9/10]
679. **Red Company: Contact** (Red Company #3) by B.V. Larson (2023) [A,9h57m|FC,SF|2024-05-12,6/10]
680. **Nightflyers & Other Stories** by George R.R. Martin (1985) [E,236p|FC,SF,SS|2024-05-18,8/10]
681. **Super Mario: How Nintendo Conquered America** by Jeff Ryan (2011) [A,8h20m|NF,BU,HI|2024-05-19,6/10]
682. **The False Mirror** (The Damned #2) by Alan Dean Foster (1992) [E,275p|FC,SF|2024-05-26,7/10]
683. **Harry Potter and the Sorcerer's Stone** (Harry Potter #1) by J.K. Rowling (1997) [E,244p|FC,FY|2024-06-01,8/10]
684. **Star Wars Episode V: The Empire Strikes Back** by Donald F. Glut (1980) [E,285p|FC,SF|2024-06-06,7/10]
685. **White Night** (The Dresden Files #9) by Jim Butcher (2007) [E,439p|FC,FY|2024-06-13,7/10]
686. **The Showman** by Simon Shuster (2024) [A,14h27m|NF,BI|2024-06-16,7/10]
687. **Stranger in a Strange Land** by Robert A. Heinlein (1961) [E,459p|FC,SF|2024-06-28,8/10]
688. **Dragon Teeth** by Michael Crichton (2017) [E,241p|FC|2024-07-03,7/10]
689. **The Sandman: Act II** by Neil Gaiman and Dirk Maggs (2021) [A,13h48m|FC,FY|2024-07-07,6/10]
690. **They Thirst** by Robert R. McCammon (1981) [E,589p|FC,HR|2024-07-21,8/10]
691. **==Dust==** (Silo #3) by Hugh Howey (2013) [E,484p|FC,SF|2024-07-31,8/10]
692. **==Lord of the Flies==** by William Golding (1954) [E,207p|FC,CL|2024-08-05,9/10]
693. **Silo Stories** by Hugh Howey [E,54p|FC,SF|2024-08-06,7/10]
694. **The Perfect Son** by Freida McFadden (2019) [E,308p|FC,TH|2024-08-08,8/10]
695. **==Inhibitor Phase==** (Revelation Space #4) by Alastair Reynolds (2021) [E,533p|FC,SF|2024-08-14,10/10]
696. **==The Godfather==** (The Godfather #1) by Mario Puzo (1969) [E,438p|FC,CL|2024-08-24,10/10]
697. **Throne World** (Undying Mercenaries #21) by B.V. Larson (2024) [A,12h30m|FC,SF|2024-08-25,7/10]
698. **Rocannon's World** (Hainish Cycle) by Ursula K. Le Guin (1966) [E,113p|FC,SF,FY|2024-08-28,7/10]
699. **The Expert System's Brother** (Expert System #1) by Adrian Tchaikovsky (2018) [E,107p|FC,SF|2024-08-31,7/10]
700. **Snow Crash** by Neal Stephenson (1992) [E,489p|FC,SF|2024-09-16,7/10]
701. **Tress of the Emerald Sea** by Brandon Sanderson (2023) [E,365p|FC,FY|2024-10-02,8/10]
702. **The New Girl** (Fear Street #1) by R.L. Stine (1989) [E,122p|FC,HR|2024-10-04,6/10]
703. **The Surrogate Mother** by Freida McFadden (2018) [E,244p|FC,TH|2024-10-07,8/10]
704. **Desperation** by Stephen King (1996) [A,21h15m|FC,HR|2024-10-13,7/10]
705. **My Best Friend's Exorcism** by Grady Hendrix (2016) [E,303p|FC,HR|2024-10-14,7/10]
706. **The Curse of the Mummy's Tomb** (Goosebumps #5) by R.L. Stine (1993) [E,110p|FC,HR|2024-10-16,6/10]
707. **Horns** by Joe Hill (2009) [E,417p|FC,HR|2024-10-26,8/10]
708. **I Found Puppets Living In My Apartment Walls** (I Found Horror Series) by Ben Farthing (2023) [E,117p|FC,HR|2024-10-28,6/10]
709. **The Surprise Party** (Fear Street #2) by R.L. Stine (1989) [E,123p|FC,HR|2024-11-01,6/10]
710. **The Spoils of War** (The Damned #3) by Alan Dean Foster (1993) [E,264p|FC,SF|2024-11-07,7/10]
711. **Harry Potter and the Chamber of Secrets** (Harry Potter #2) by J.K. Rowling (1998) [E,246p|FC,FY|2024-11-15,8/10]
712. **Hawksbill Station** by Robert Silverberg (1967) [E,165p|FC,SF|2024-11-19,8/10]
713. **Not Till We Are Lost** (Bobiverse #5) by Dennis E. Taylor (2024) [A,11h41m|FC,SF|2024-11-24,7/10]
714. **Small Favor** (The Dresden Files #10) by Jim Butcher (2008) [E,391p|FC,FY|2024-11-28,7/10]
715. **Star Wars Episode VI: Return of the Jedi** by James Kahn (1983) [E,282p|FC,FY|2024-12-03,7/10]
716. **The Subtle Art of Not Giving a F*ck** by Mark Manson (2016) [E,156p|NF,SH|2024-12-07,7/10]
717. **Way Station** by Clifford D. Simak (1963) [E,199p|FC,SF|2024-12-14,8/10]
718. **Mystery Walk** by Robert R. McCammon (1983) [E,449p|FC,HR|2024-12-23,7/10]
719. **The Gift** by Freida McFadden (2022) [E,48p|FC,TH,SS|2024-12-23,6/10]

## 2025

720. **==Aurora Rising==** (Prefect Dreyfus Emergency #1) by Alastair Reynolds (2007) [E,506p|FC,SF|2025-01-02,9/10]
721. **The Sicilian** (The Godfather #2) by Mario Puzo (1984) [E,338p|FC,CL|2025-01-12,8/10]
722. **The Andromeda Evolution** (Andromeda #2) by Daniel H. Wilson (2019) [E,326p|FC,SF|2025-01-20,7/10]
723. **Nexus: A Brief History of Information Networks from the Stone Age to AI** by Yuval Noah Harari (2024) [A,17h28m|NF,HI,SC|2025-01-22,8/10]
724. **Planet of Exile** (Hainish Cycle) by Ursula K. Le Guin (1966) [E,107p|FC,SF,FY|2025-01-25,7/10]
725. **We Can Build You** by Philip K. Dick (1972) [E,199p|FC,SF|2025-01-31,6/10]
726. **A Coming of Age** by Timothy Zahn (1984) [E,274p|FC,SF|2025-02-08,7/10]
727. **Red Company: Invasion** (Red Company #4) by B.V. Larson (2024) [A,9h20m|FC,SF|2025-02-09,6/10]
728. **Dead Med** by Freida McFadden (2024) [E,384p|FC,TH|2025-02-14,7/10]
729. **The Richest Man in Babylon** by George S. Clason (1926) [A,3h47m|NF,FI,SH|2025-02-09,6/10]
730. **The Expert System’s Champion** (Expert System #2) by Adrian Tchaikovsky (2021) [E,125p|FC,SF|2025-02-18,6/10]