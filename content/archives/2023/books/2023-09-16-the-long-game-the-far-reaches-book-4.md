---
title: "The Long Game (The Far Reaches Book 4) by Ann Leckie (2023)"
slug: "the-long-game-the-far-reaches-book-4"
author: "Paulo Pereira"
date: 2023-09-16T21:00:00+01:00
lastmod: 2023-09-16T21:00:00+01:00
cover: "/posts/books/the-long-game-the-far-reaches-book-4.jpg"
description: "Finished “The Long Game (The Far Reaches Book 4)” by Ann Leckie."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - Ann Leckie
book series:
  - The Far Reaches Series
book genres:
  - Fiction
  - Science Fiction
  - Short Stories
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Ann Leckie
  - The Far Reaches Series
  - Fiction
  - Science Fiction
  - Short Stories
  - 7/10 Books
  - Ebook
---

Finished “The Long Game”.
* Author: [Ann Leckie](/book-authors/ann-leckie)
* First Published: [June 27, 2023](/book-publication-year/2023)
* Series: [The Far Reaches](/book-series/the-far-reaches-series) Book 4
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/151908306-the-long-game)
>
> An inquisitive life-form finds there’s more to existence than they ever dreamed in an imaginative short story by New York Times bestselling and Hugo and Nebula Award–winning author Ann Leckie. On a far-off colony, humans tower over the local species who grow the plants they need. Narr keeps the workers in line—someone has to. But when Narr learns just how short-lived their species is, the little alien embarks on a big to find out why their people die and how to stop it. Stubborn and hopeful, Narr has a plan for the locals, for humans, and for the future.