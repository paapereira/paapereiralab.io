---
title: "Alternatives for WhatsApp (#dump-whatsapp)"
slug: "alternative-whatsapp"
author: "Paulo Pereira"
date: 2021-01-10T14:31:28+00:00
lastmod: 2021-01-10T14:31:28+00:00
cover: "/posts/2021/2021-01-10-alternative-whatsapp/conversations.png"
description: "Looking for alternatives for WhatsApp?\n

My recommendation: use [Signal](https://www.signal.org/) or (my personal favorite) [XMPP](/posts/xmpp) and specifically [Conversations](https://conversations.im/) or [Quicksy](https://quicksy.im/)."
draft: false
toc: false
categories:
  - General
tags:
  - General
  - Signal
  - XMPP
  - Jabber
  - WhatsApp
aliases:
  - /posts/2021/01/alternative_whatsapp/
---

Looking for alternatives for WhatsApp following recent [news](https://www.firstpost.com/tech/news-analysis/whatsapp-privacy-policy-update-concerned-about-privacy-users-look-at-alternative-apps-like-signal-telegram-9183071.html) and [\#dump-whatsapp](https://twitter.com/search?q=dump%20WhatsApp)? News that should not be strange, but...

My recommendation: use [Signal](https://www.signal.org/) or (my personal favorite) [XMPP](/posts/xmpp) and specifically [Conversations](https://conversations.im/) or [Quicksy](https://quicksy.im/).
