---
title: "The Long Walk by Stephen King writing as Richard Bachman (1979)"
slug: "the-long-walk"
author: "Paulo Pereira"
date: 2021-12-14T22:00:00+00:00
lastmod: 2021-12-14T22:00:00+00:00
cover: "/posts/books/the-long-walk.jpg"
description: "Finished “The Long Walk” by Stephen King writing as Richard Bachman."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Stephen King
  - Richard Bachman
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Stephen King
  - Richard Bachman
  - Stephen King Novels
  - Fiction
  - Horror
  - 5/10 Books
  - Audiobook
---

Finished “The Long Walk”.
* Author: [Stephen King](/book-authors/stephen-king) writing as [Richard Bachman](/book-authors/richard-bachman)
* First Published: [July 1979](/book-publication-year/1970s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28930119-the-long-walk)
>
> On the first day of May, 100 teenage boys meet for a race known as "The Long Walk." If you break the rules, you get three warnings. If you exceed your limit, what happens is absolutely terrifying.