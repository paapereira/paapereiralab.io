---
title: "Way Station by Clifford D. Simak (1963)"
slug: "way-station"
author: "Paulo Pereira"
date: 2024-12-14T22:00:00+00:00
lastmod: 2024-12-14T22:00:00+00:00
cover: "/posts/books/way-station.jpg"
description: "Finished “Way Station” by Clifford D. Simak."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Clifford D. Simak
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Clifford D. Simak
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
aliases:
  - /posts/books/way-station
---

Finished “Way Station”.
* Author: [Clifford D. Simak](/book-authors/clifford-d.-simak)
* First Published: [June 1, 1963](/book-publication-year/1960s)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35006946-way-station)
>
> Enoch Wallace is an ageless hermit, striding across his untended farm as he has done for over a century, still carrying the gun with which he had served in the Civil War. But what his neighbors must never know is that, inside his unchanging house, he meets with a host of unimaginable friends from the farthest stars.
> 
> More than a hundred years before, an alien named Ulysses had recruited Enoch as the keeper of Earth's only galactic transfer station. Now, as Enoch studies the progress of Earth and tends the tanks where the aliens appear, the charts he made indicate his world is doomed to destruction. His alien friends can only offer help that seems worse than the dreaded disaster. Then he discovers the horror that lies across the galaxy...