---
title: "1984 by George Orwell (1949)"
slug: "1984"
author: "Paulo Pereira"
date: 2018-01-01T21:57:00+00:00
lastmod: 2018-01-01T21:57:00+00:00
cover: "/posts/books/1984.jpg"
description: "Finished “1984” by George Orwell."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1949
book authors:
  - George Orwell
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
  - Classics
book scores:
  - 9/10 Books
tags:
  - Book Log
  - George Orwell
  - Fiction
  - Science Fiction
  - Classics
  - 9/10 Books
  - Audiobook
aliases:
  - /posts/books/1984
---

Finished “1984”.
* Author: [George Orwell](/book-authors/george-orwell)
* First Published: [June 8th 1949](/book-publication-year/1949)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28441377-1984)
>
> Among the seminal texts of the 20th century, Nineteen Eighty-Four is a rare work that grows more haunting as its futuristic purgatory becomes more real. Published in 1949, the book offers political satirist George Orwell's nightmare vision of a totalitarian, bureaucratic world and one poor stiff's attempt to find individuality. The brilliance of the novel is Orwell's prescience of modern life--the ubiquity of television, the distortion of the language--and his ability to construct such a thorough version of hell. Required reading for students since it was published, it ranks among the most terrifying novels ever written.