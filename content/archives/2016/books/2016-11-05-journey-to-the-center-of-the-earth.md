---
title: "Journey to the Center of the Earth by Jules Verne (1864)"
slug: "journey-to-the-center-of-the-earth"
author: "Paulo Pereira"
date: 2016-11-05T23:19:00+00:00
lastmod: 2016-11-05T23:19:00+00:00
cover: "/posts/books/journey-to-the-center-of-the-earth.jpg"
description: "Finished “Journey to the Center of the Earth” by Jules Verne."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - Jules Verne
book series:
  -
book genres:
  - Fiction
  - Science Fiction
  - Classics
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Jules Verne
  - Fiction
  - Science Fiction
  - Classics
  - 5/10 Books
  - Audiobook
---

Finished “Journey to the Center of the Earth”.
* Author: [Jules Verne](/book-authors/jules-verne)
* First Published: [November 25th 1864](/book-publication-year/1800s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13530422-journey-to-the-center-of-the-earth)
>
> Journey to the Center of the Earth is one of literature’s earliest works of science fiction. It vividly animates a fantastical subterranean world as an intrepid crew, led by the eccentric Otto Lidenbrock, traverses the planet’s core and its various bizarre obstacles: giant mushrooms and insects, a herd of mastodons, prehistoric humans, a treacherous pit of magma, and more.