---
title: "The Halloween Tree by Ray Bradbury (1972)"
slug: "the-halloween-tree"
author: "Paulo Pereira"
date: 2023-11-01T16:00:00+00:00
lastmod: 2023-11-01T16:00:00+00:00
cover: "/posts/books/the-halloween-tree.jpg"
description: "Finished “The Halloween Tree” by Ray Bradbury."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Ray Bradbury
book series:
  - 
book genres:
  - Fiction
  - Horror
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Ray Bradbury
  - Fiction
  - Horror
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “The Halloween Tree: A Scary Halloween Book”.
* Author: [Ray Bradbury](/book-authors/ray-bradbury)
* First Published: [January 1, 1972](/book-publication-year/1970s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25344096-the-halloween-tree)
>
> A fast-moving, eerie...tale set on Halloween night. Eight costumed boys running to meet their friend Pipkin at the haunted house outside town encounter instead the huge and cadaverous Mr. Moundshroud. As Pipkin scrambles to join them, he is swept away by a dark Something, and Moundshroud leads the boys on the tail of a kite through time and space to search the past for their friend and the meaning of Halloween.