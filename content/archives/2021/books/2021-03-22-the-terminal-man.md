---
title: "The Terminal Man by Michael Crichton (1972)"
slug: "the-terminal-man"
author: "Paulo Pereira"
date: 2021-03-22T22:00:00+00:00
lastmod: 2021-03-22T22:00:00+00:00
cover: "/posts/books/the-terminal-man.jpg"
description: "Finished “The Terminal Man” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Michael Crichton
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
aliases:
  - /posts/books/the-terminal-man
---

Finished “The Terminal Man”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [March 1st 1972](/book-publication-year/1970s)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/46132375-the-terminal-man)
>
> Harry Benson is prone to violent, uncontrollable seizures and is under police guard after attacking two people.
>
> Dr. Roger McPherson, head of the prestigious Neuropsychiatric Research Unit at University Hospital in Los Angeles, is convinced he can cure Benson through a procedure called Stage Three. During this highly specialized experimental surgery, electrodes will be place in the patient's brain, sending monitored, soothing pulses to its pleasure canyons.
>
> Though the operation is a success, there is an unforseen development. Benson learns how to control the pulses and is increasing their frequency. He escapes -- a homicidal maniac loose in the city -- and nothing will stop his murderous rampages or impede his deadly agenda. . .