---
title: "Straker's Breakers (Galactic Liberation Book 5) by David VanDyke and B.V. Larson (2019)"
slug: "strakers-breakers-galactic-liberation-book-5"
author: "Paulo Pereira"
date: 2022-12-11T18:00:00+00:00
lastmod: 2022-12-11T18:00:00+00:00
cover: "/posts/books/strakers-breakers-galactic-liberation-book-5.jpg"
description: "Finished “Straker's Breakers (Galactic Liberation Book 5)” by David VanDyke and B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - David VanDyke
  - B.V. Larson
book series:
  - Galactic Liberation Series
book genres:
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - David VanDyke
  - B.V. Larson
  - Galactic Liberation Series
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Straker's Breakers”.
* Author: [David VanDyke](/book-authors/david-vandyke) and [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [May 1, 2019](/book-publication-year/2019)
* Series: [Galactic Liberation](/book-series/galactic-liberation-series) Book 5
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/46248650-straker-s-breakers)
>
> It's been five years since the end of the Hive Wars. Straker has handed over the burden of governing the Earthan Republic to civil authorities. 
> 
> This should be a happy time, as Straker and his most loyal soldiers retire to a planet and step off the galactic stage. Unfortunately, authoritarian forces within the Earthan Republic are still simmering with rage at his victory. Rumors reach Straker of a growing new Republic Fleet manned by state loyalists. Planets begin to fall under their influence and go dark. Even in retirement, they see Straker's Breakers as a threat. 
> 
> Fearing he may have to move again in the name of freedom and the people, Straker begins to train and plan. Will they dare to come for him? His battlesuiters have formed families and planted roots on Culloden. What of their children and stories of neighboring worlds reverting to dictatorships?   
> 
> Straker's Breakers prepare to fight again, alone and without support.