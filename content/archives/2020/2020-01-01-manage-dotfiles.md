---
title: "Manage dotfiles using a git bare repository"
slug: "manage-dotfiles"
author: "Paulo Pereira"
date: 2020-01-01T18:33:15+00:00
lastmod: 2020-01-01T18:33:15+00:00
description: "I've been searching the best way to manage my dotfiles using git."
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Git
  - Keybase
aliases:
  - /posts/manage-dotfiles/
---

I've been searching the best way to manage my dotfiles using git.

After seeing this [video](https://www.youtube.com/watch?v=tBoLDpTWVOM) and checking this [tutorial](https://www.atlassian.com/git/tutorials/dotfiles) I decided to give it a go.

I will be using [keybase](https://keybase.io/) as my (private) git repository.

## Create and setup repository

* Create a repository in keybase using the application
* Create a bare repository

```bash
mkdir -p $HOME/.dotfiles
git init --bare $HOME/.dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
dotfiles config --local status.showUntrackedFiles no
echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'" >> $HOME/.zshrc
```

## Adding files

* Add a first file to test if everything is OK

```bash
dotfiles status
dotfiles add .vimrc
dotfiles commit -m "Add vimrc"
dotfiles remote add origin keybase://private/your_username/dotfiles
dotfiles push -u origin master
```

* Every time you want to add a new file

```bash
dotfiles add .bashrc
dotfiles commit -m "Add bashrc"
dotfiles add .zshrc
dotfiles commit -m "Add zshrc"
dotfiles push
```

## Dotfiles in a new system

* In a new system, you can get your dotfiles by:

```bash
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
echo ".dotfiles" >> .gitignore
git clone --bare keybase://private/your_username/dotfiles $HOME/.dotfiles
dotfiles config --local status.showUntrackedFiles no
mkdir -p $HOME/.dotfiles-backup
dotfiles checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
else
  echo "Backing up pre-existing dot files.";
  dotfiles checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} $HOME/.dotfiles-backup/{}
fi;
dotfiles checkout
```

* Then, every time you want to refresh your dotfiles

```bash
dotfiles pull
```