---
title: "Dead Beat (The Dresden Files Book 7) by Jim Butcher (2005)"
slug: "dead-beat-the-dresden-files-book-7"
author: "Paulo Pereira"
date: 2023-11-17T22:00:00+00:00
lastmod: 2023-11-17T22:00:00+00:00
cover: "/posts/books/dead-beat-the-dresden-files-book-7.jpg"
description: "Finished “Dead Beat (The Dresden Files Book 7)” by Jim Butcher."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2005
book authors:
  - Jim Butcher
book series:
  - The Dresden Files Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jim Butcher
  - The Dresden Files Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Dead Beat”.
* Author: [Jim Butcher](/book-authors/jim-butcher)
* First Published: [May 3, 2005](/book-publication-year/2005)
* Series: [The Dresden Files](/book-series/the-dresden-files-series) Book 7
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6567483-dead-beat)
>
> Paranormal investigations are Harry Dresden’s business and Chicago is his beat, as he tries to bring law and order to a world of wizards and monsters that exists alongside everyday life. And though most inhabitants of the Windy City don’t believe in magic, the Special Investigations Department of the Chicago PD knows better.   Karrin Murphy is the head of S. I. and Harry’s good friend. So when a killer vampire threatens to destroy Murphy’s reputation unless Harry does her bidding, he has no choice. The vampire wants the Word of Kemmler (whatever that is) and all the power that comes with it. Now, Harry is in a race against time—and six merciless necromancers—to find the Word before Chicago experiences a Halloween night to wake the dead...