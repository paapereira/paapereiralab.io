---
title: "Monster Blood (Goosebumps Book 3) by R.L. Stine (1992)"
slug: "monster-blood-goosebumps-book-3"
author: "Paulo Pereira"
date: 2023-10-22T21:00:00+01:00
lastmod: 2023-10-22T21:00:00+01:00
cover: "/posts/books/monster-blood-goosebumps-book-3.jpg"
description: "Finished “Monster Blood (Goosebumps Book 3)” by R.L. Stine."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - R.L. Stine
book series:
  - Goosebumps Series
book genres:
  - Fiction
  - Horror
book scores:
  - 6/10 Books
tags:
  - Book Log
  - R.L. Stine
  - Goosebumps Series
  - Fiction
  - Horror
  - 6/10 Books
  - Ebook
---

Finished “Monster Blood”.
* Author: [R.L. Stine](/book-authors/r.l.-stine)
* First Published: [September 1, 1992](/book-publication-year/1990s)
* Series: [Goosebumps](/book-series/goosebumps-series) Book 3
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12585458-monster-blood)
>
> While staying with his weird great-aunt Kathryn, Evan visits an eerie old toy store and buys a dusty can of Monster Blood. It's fun to play with at first. And Evan's dog, Trigger, likes it so much, he eats some. But then Evan notices something weird about the slimy green ooze. It keeps growing. And growing. And growing.And all that growing has given the Monster Blood a monstrous appetite. . . .