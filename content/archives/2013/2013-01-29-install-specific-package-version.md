---
title: "Install a specific package version"
slug: "install-specific-package-version"
author: "Paulo Pereira"
date: 2013-01-29T21:11:00+00:00
lastmod: 2013-01-29T21:11:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 12.10
  - Quantal Quetzal
  - APT
  - git
---

Yesterday, after a partial upgrade in my Ubuntu 12.10 installation, [git](http://git-scm.com/) was uninstalled.

After the upgrade I tried to re-install it and got the following error:

```bash
sudo apt-get install git
```
```text
Reading package lists... Done
Building dependency tree   
Reading state information... Done
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:
The following packages have unmet dependencies:
**git : Depends: git-man (< 1:1.8.0.3-.) but 1:1.8.1.1-1~ppa0~quantal2 is to be installed**
E: Unable to correct problems, you have held broken packages.
```

Basically git depended on git-man version 1.8.0.3, but the APT package  handling utility was trying to install the newer version 1.8.1.1.

But how to install a specific version?

After some digging I found this [tip](http://www.howtogeek.com/117929/how-to-downgrade-packages-on-ubuntu/).

I prefer the terminal option.

- Query the APT cache about the packages versions avaiable:
```bash
sudo apt-cache showpkg git-man
```
```text
Package: git-man
Versions:
**1:1.8.1.1-1~ppa0~quantal2** (/var/lib/apt/lists/ppa.launchpad.net_git-core_ppa_ubuntu_dists_quantal_main_binary-amd64_Packages) (/var/lib/dpkg/status)
 Description Language:
         File: /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_quantal_main_binary-amd64_Packages
         MD5: 0c79f507738c0cb72351c8ae551ee47d
 Description Language: en
         File: /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_quantal_main_i18n_Translation-en
         MD5: 0c79f507738c0cb72351c8ae551ee47d
**1:1.8.0.3-0ppa1~quantal1** (/var/lib/apt/lists/ppa.launchpad.net_git-core_ppa_ubuntu_dists_quantal_main_binary-amd64_Packages)
 Description Language:
         File: /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_quantal_main_binary-amd64_Packages
         MD5: 0c79f507738c0cb72351c8ae551ee47d
 Description Language: en
         File: /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_quantal_main_i18n_Translation-en
         MD5: 0c79f507738c0cb72351c8ae551ee47d
(...)
```
- Install the desired version
```bash
sudo apt-get install git-man=1:1.8.0.3-0ppa1~quantal1
```
- Install git normally
```bash
sudo apt-get install git
```