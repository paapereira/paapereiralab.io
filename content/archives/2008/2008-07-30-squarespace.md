---
title: "Squarespace"
slug: "squarespace"
author: "Paulo Pereira"
date: 2008-07-30T19:26:00+01:00
lastmod: 2008-07-30T19:26:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
aliases:
  - /posts/squarespace/
---

[Squarespace](http://www.squarespace.com/) is an amazing publishing service for building websites and creating blogs.

Check out the [video](http://www.squarespace.com/) and prepare to be amazed.

As Kevin Rose said:

> "... it's like [Typepad](http://www.typepad.com/) and [Wordpress](http://wordpress.com/) on crack."

It's a [paid](http://www.squarespace.com/pricing/) service, but it seems to me it's worth every penny.

I didn't try it yet, but there's a [free](https://www.squarespace.com/signup/), no credit card needed, 14 days trial period.

If you've tried please leave your thoughts :)
