---
title: "Head On (Lock In Book 2) by John Scalzi (2018)"
slug: "head-on-lock-in-book-2"
author: "Paulo Pereira"
date: 2020-09-01T22:54:00+01:00
lastmod: 2020-09-01T22:54:00+01:00
cover: "/posts/books/head-on-lock-in-book-2.png"
description: "Finished “Head On (Lock In Book 2)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - John Scalzi
book series:
  - Lock In Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - John Scalzi
  - Lock In Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Head On”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [April 17th 2018](/book-publication-year/2018)
* Series: [Lock In](/book-series/lock-in-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/41071335-head-on)
>
> John Scalzi returns with Head On, the standalone follow-up to the New York Times bestselling and critically acclaimed Lock In. Chilling near-future SF with the thrills of a gritty cop procedural, Head On brings Scalzi's trademark snappy dialogue and technological speculation to the future world of sports.
>
> Hilketa is a frenetic and violent pastime where players attack each other with swords and hammers. The main goal of the game: obtain your opponent’s head and carry it through the goalposts. With flesh and bone bodies, a sport like this would be impossible. But all the players are “threeps,” robot-like bodies controlled by people with Haden’s Syndrome, so anything goes. No one gets hurt, but the brutality is real and the crowds love it.
>
> Until a star athlete drops dead on the playing field.
>
> Is it an accident or murder? FBI Agents and Haden-related crime investigators, Chris Shane and Leslie Vann, are called in to uncover the truth―and in doing so travel to the darker side of the fast-growing sport of Hilketa, where fortunes are made or lost, and where players and owners do whatever it takes to win, on and off the field.