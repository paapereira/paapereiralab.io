---
title: "Hardy is out"
slug: "hardy-is-out"
author: "Paulo Pereira"
date: 2008-04-24T19:36:00+01:00
lastmod: 2008-04-24T19:36:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

Here it his. [Ubuntu 8.04 LTS](http://www.ubuntu.com/news/ubuntu-8.04-lts-desktop), codename Hardy Heron, is out.

This is the second Long Time Support (LTS) release from Ubuntu and it will be supported for 3 years (instead of the normal 18 months).

For those who don't want to install a new OS every 6 to 18 months this is a great chance to do a fresh install and keep your desktop up and running, with full support, for a long time.

If you have Ubuntu 7.10 it's easy to upgrade to Ubuntu 8.04. Basically you just need to update your system using Update Manager (check the list of links bellow).

Check your Ubuntu version:

```bash
lsb_release -a
```

I will upgrade my laptop and will be back soon will feedback.

Meanwhile here's some useful links:

- [Ubuntu Release Notes](http://www.ubuntu.com/getubuntu/releasenotes/804overview)
- [Upgrading Ubuntu from 7.10 to 8.04 LTS](http://www.ubuntu.com/getubuntu/upgrading)
- [Ubuntu Forum announcement (with more links)](http://ubuntuforums.org/showthread.php?t=764856)