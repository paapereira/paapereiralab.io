---
title: "Firefox Guinness World Record"
slug: "firefox-guinness-world-record"
author: "Paulo Pereira"
date: 2008-06-01T23:29:00+01:00
lastmod: 2008-06-01T23:29:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
  - Firefox
aliases:
  - /posts/firefox-guinness-world-record/
---

Help [Firefox](http://www.spreadfirefox.com/en-US/worldrecord/) get the Guinness World Record for the most software downloads in 24 hours.

All you have to do is download Firefox 3 within the 24 hour after is release.

The launch day will be [announced soon](http://www.spreadfirefox.com/en-US/worldrecord/), so bookmark [this link](http://www.spreadfirefox.com/en-US/worldrecord/).

You can also [get involved](http://www.spreadfirefox.com/en-US/worldrecord/getinvolved), and spread this record attempt.