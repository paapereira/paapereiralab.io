---
title: "Double Dexter (Dexter Book 6) by Jeff Lindsay (2011)"
slug: "double-dexter-dexter-book-6"
author: "Paulo Pereira"
date: 2015-04-07T23:33:00+01:00
lastmod: 2015-04-07T23:33:00+01:00
cover: "/posts/books/double-dexter-dexter-book-6.jpg"
description: "Finished “Double Dexter (Dexter Book 6)” by Jeff Lindsay."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Jeff Lindsay
book series:
  - Dexter Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jeff Lindsay
  - Dexter Series
  - Fiction
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “Double Dexter”.
* Author: [Jeff Lindsay](/book-authors/jeff-lindsay)
* First Published: [October 11th 2011](/book-publication-year/2011)
* Series: [Dexter](/book-series/dexter-series) Book 6
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13600593-double-dexter)
>
> A witness. Such a simple concept - and yet for Dexter Morgan, the perfectly well-disguised monster, the possibility of a witness is unthinkable. But when Dexter is on a very private, very satisfying excursion one evening with a wretchedly deserving playmate, the unthinkable happens: someone sees him.
>
> Dexter is not at all pleased. As an upstanding blood-spatter analyst for the Miami Police Department, he has always managed to keep the darker side of his life out of the spotlight...the fun part, where he finds truly bad people - murderers who have escaped the reach of the justice system - and quietly gives them his very special attentions. But now that he's been seen and identified by his witness, Dexter must launch himself into a different kind of hunt.
>
> Making matters worse, a brutal cop killer is targeting Miami's police detectives, leaving behind bodies that are battered beyond recognition...and completely bloodless. As the department grows more fearful of the psychotic killer in their midst, Dexter must handle his own crisis and come to terms with the fact that his witness is not only circling him but determined to expose him. Dexter is being followed, manipulated, and mimicked...leading him to realize that no one likes to have a double, especially when his double's goal is to kill him.
>
> Double Dexter is complex and suspenseful and, as always, raucously entertaining...full of smart thrills and dark laughs that move effortlessly from a keen portrayal of Miami to Key West and beyond. Like the previous five bestselling novels in the Dexter series, this one is alive with the witty, macabre originality that has propelled Jeff Lindsay to international success.