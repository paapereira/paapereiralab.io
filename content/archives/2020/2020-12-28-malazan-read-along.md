---
title: "Malazan Book of the Fallen Read Along"
slug: "malazan-read-along"
author: "Paulo Pereira"
date: 2020-12-28T18:23:20+00:00
lastmod: 2020-12-28T18:23:20+00:00
description: "I’ll be reading “[Gardens of the Moon](https://www.goodreads.com/book/show/8134944)” by Steven Erikson, the first book of the [Malazan Book of the Fallen](https://www.goodreads.com/series/43493-malazan-book-of-the-fallen) series, beginning next year."
draft: false
toc: false
categories:
  - Books
tags:
  - Books
aliases:
  - /posts/malazan-read-along/
---

I’ll be reading “[Gardens of the Moon](https://www.goodreads.com/book/show/8134944)” by Steven Erikson, the first book of the [Malazan Book of the Fallen](https://www.goodreads.com/series/43493-malazan-book-of-the-fallen) series, beginning next year.

I’ll be participating in [Mike's Book Reviews](https://www.youtube.com/channel/UCm_DGonIiSrLBGLz0ProiWA) community read along.

You can find out more in his [Malazan: Channel Read Along Schedule & FAQ](https://www.youtube.com/watch?v=uG7xMR7Fxwk) video.

This is a 2 year read along marathon. Don’t know if I’m gonna go to the end, but at least the first book is bought and ready to go.

![2020 in Books](/posts/2020/2020-12-28-malazan-read-along/malazan-read-along.jpg)