---
title: "Seafile Installation Notes in Arch Linux"
slug: "seafile-installation"
author: "Paulo Pereira"
date: 2017-05-28T18:23:43+01:00
lastmod: 2017-05-28T18:23:43+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Seafile
---

These are my notes about installing [Seafile](https://www.seafile.com/) in my Arch Linux home server.

*Note:* I have reviewed and added to these notes several times.

I'm using:
* Seafile
* A NUC with Arch Linux as my home server
* A WD My Cloud NAS

## Setup a NAS share

- Create a new share `seafile`

- Create a new directory in the server

```bash
sudo mkdir /home/nas/seafile
sudo chmod 750 /home/nas/seafile
sudo chown seafile:seafile /home/nas/seafile
```

- Setup a mountpoint in the server

```bash
sudo vi /etc/fstab
```
```text
nas:/nfs/seafile        /home/nas/seafile       nfs  users,noauto,x-systemd.automount,x-systemd.device-timeout=10,timeo=14,soft,intr,noatime,file_mode=0640,dir_mode=0750 0 0
```

- Set NO_ROOT_SWASH in the WD My Cloud NFS exports configuration (when I restart my NAS I have to do this again...)

```bash
ssh -oHostKeyAlgorithms=+ssh-dss sshd@192.168.0.99
```
```bash
vi /etc/exports
```
```text
"/nfs/seafile" *(rw,no_root_squash,sync,no_wdelay,insecure_locks,insecure,no_subtree_check,anonuid=501,anongid=1000)
```
```bash
exportfs -r
```

## Server installation


### Pre-requisites

```bash
sudo pacman -S python-imaging mysql-python

mkdir /tmp/cower
cd /tmp/cower
cower -d libsearpc
cd /tmp/cower/libsearpc
makepkg -sri
cower -d libsepol
cd /tmp/cower/libsepol
makepkg -sri
cower -d libselinux
cd /tmp/cower/libselinux
makepkg -sri
```

### Download Seafile

https://www.seafile.com/en/download/

```bash
sudo useradd -m -r -d /srv/seafile -s /usr/bin/nologin seafile

sudo -u seafile -s /bin/sh

mkdir seafile/seafile-data
chown -R seafile:users seafile/seafile-data

echo $HOME

cd $HOME
wget https://bintray.com/artifact/download/seafile-org/seafile/seafile-server_6.0.9_x86-64.tar.gz

mkdir seafile
mv seafile-server_* seafile
cd seafile
tar -xzf seafile-server_*
mkdir installed
mv seafile-server_* installed
```

### MySQL setup for Seafile

```bash
sudo -u seafile -s /bin/sh

cd $HOME/seafile/seafile-server-latest
./setup-seafile-mysql.sh

./seafile.sh start
./seahub.sh start
./seahub.sh stop
./seafile.sh stop
```

### Move to NAS

```bash
cd $HOME/seafile
mv seafile-data /home/nas/seafile/
ln -s /home/nas/seafile/seafile-data $HOME/seafile/seafile-data
```

### Email configuration

```bash
sudo -u seafile -s /bin/sh

cd $HOME/seafile/conf
vi seahub_settings.py
```
```text
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.mail.com'
EMAIL_HOST_USER = 'your_user@mail.com'
EMAIL_HOST_PASSWORD = 'XXXXXXXXX'
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = "your_user@mail.com"
SERVER_EMAIL = "your_user@mail.com"
```

### Apache configuration

```bash
cd /etc/httpd/conf
sudo vi httpd.conf
```
```text
Include conf/extra/httpd-vhosts.conf
LoadModule rewrite_module modules/mod_rewrite.so
LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so
LoadModule proxy_http_module modules/mod_proxy_http.so
```
```bash
sudo vi extra/httpd-vhosts.conf
```
```text
<VirtualHost *:80>
	ServerName my_domain.ddns.net
	DocumentRoot /etc/httpd
	Alias /media  /srv/seafile/seafile/seafile-server-latest/seahub/media

	RewriteEngine On

	<Location /media>
		Require all granted
	</Location>

	#
	# seafile fileserver
	#
	ProxyPass /seafhttp http://127.0.0.1:8082
	ProxyPassReverse /seafhttp http://127.0.0.1:8082
	RewriteRule ^/seafhttp - [QSA,L]

	#
	# WebDAV
	# We use http proxy, since SeafDAV is incompatible with FCGI proxy in Apache 2.4.
	#
	ProxyPass /seafdav http://127.0.0.1:8080/seafdav
	ProxyPassReverse /seafdav http://127.0.0.1:8080/seafdav
    
	#
	# seahub
	#
	SetEnvIf Request_URI . proxy-fcgi-pathinfo=unescape
	SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
	ProxyPass / fcgi://127.0.0.1:8000/
</VirtualHost>
```
```bash
sudo -u seafile -s /bin/sh
cd $HOME/seafile/conf
vi ccnet.conf
```
```text
SERVICE_URL = http://my_domain.ddns.net
```
```bash
vi seahub_settings.py
```
```text
FILE_SERVER_ROOT = 'http://my_domain.ddns.net/seafile/seafhttp'
```
```bash
vi seafdav.conf
```
```text
[WEBDAV]
enabled = true 
port = 8080
fastcgi = false
share_name = /seafdav
```
```bash
exit
sudo systemctl restart httpd
sudo -u seafile -s /bin/sh
cd /srv/seafile/seafile/seafile-server-latest
./seafile.sh restart
./seahub.sh stop
./seahub.sh start-fastcgi
```

### Create systemd services

```bash
cat << EOF | sudo tee /etc/systemd/system/seafile.service
[Unit]
Description=Seafile server
After=network.target mysqld.service

[Service]
Type=oneshot
ExecStart=/srv/seafile/seafile/seafile-server-latest/seafile.sh start
ExecStop=/srv/seafile/seafile/seafile-server-latest/seafile.sh stop
RemainAfterExit=yes
User=seafile
Group=nogroup

[Install]
WantedBy=multi-user.target
EOF
```
```bash
sudo systemctl start seafile
sudo systemctl status seafile
sudo systemctl enable seafile
```
```bash
cat << EOF | sudo tee /etc/systemd/system/seahub.service
[Unit]
Description=Seahub server
After=network.target mysqld.service seafile.service

[Service]
Type=oneshot
ExecStart=/srv/seafile/seafile/seafile-server-latest/seahub.sh start
ExecStop=/srv/seafile/seafile/seafile-server-latest/seahub.sh stop
RemainAfterExit=yes
User=seafile
Group=nogroup

[Install]
WantedBy=multi-user.target
EOF
```
```bash
sudo systemctl start seahub
sudo systemctl status seahub
sudo systemctl enable seahub
```

### Setup Apache with SSL

```bash
cd /etc/httpd/conf

sudo openssl genrsa -out privkey_seafile.pem 2048
sudo openssl req -new -x509 -key privkey_seafile.pem -out cacert_seafile.pem -days 1095
```
```bash
sudo vi httpd.conf
```
```text
Include conf/extra/httpd-vhosts.conf
LoadModule ssl_module modules/mod_ssl.so
LoadModule log_config_module modules/mod_log_config.so
```
```bash
sudo vi extra/httpd-vhosts.conf
```
```text
<VirtualHost *:443>
  ServerName my_domain.ddns.net
  DocumentRoot /etc/httpd

  SSLEngine On
  SSLCertificateFile /etc/httpd/conf/cacert_seafile.pem
  SSLCertificateKeyFile /etc/httpd/conf/privkey_seafile.pem

  Alias /media  /srv/seafile/seafile/seafile-server-latest/seahub/media

  <Location /media>
	Require all granted
  </Location>

  RewriteEngine On

  #
  # seafile fileserver
  #
  ProxyPass /seafhttp http://127.0.0.1:8082
  ProxyPassReverse /seafhttp http://127.0.0.1:8082
  RewriteRule ^/seafhttp - [QSA,L]

  #
  # WebDAV
  # We use http proxy, since SeafDAV is incompatible with FCGI proxy in Apache 2.4.
  #
  ProxyPass /seafdav http://127.0.0.1:8080/seafdav
  ProxyPassReverse /seafdav http://127.0.0.1:8080/seafdav
  
  #
  # seahub
  #
  # BEFORE 6.3
  #SetEnvIf Request_URI . proxy-fcgi-pathinfo=unescape
  #SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
  #ProxyPass / fcgi://127.0.0.1:8000/
  # AFTER 6.3
  SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
  ProxyPass / http://127.0.0.1:8000/
  ProxyPassReverse / http://127.0.0.1:8000/
</VirtualHost>
```
```bash
sudo -u seafile -s /bin/sh
cd $HOME/seafile/conf
vi ccnet.conf
```
```text
SERVICE_URL = https://my_domain.ddns.net
```
```bash
vi seahub_settings.py
```
```text
FILE_SERVER_ROOT = 'https://my_domain.ddns.net/seafhttp'
```
```bash
vi seafdav.conf
```
```text
[WEBDAV]
enabled = true 
port = 8080
fastcgi = false
share_name = /seafdav
```
```bash
exit
sudo systemctl restart httpd

sudo -u seafile -s /bin/sh
cd /srv/seafile/seafile/seafile-server-latest
./seafile.sh restart
./seahub.sh stop
./seahub.sh start-fastcgi
```

#### SSL Certificate using certbot

https://manual.seafile.com/deploy/https_with_apache.html

https://certbot.eff.org/lets-encrypt/arch-apache

```bash
sudo pacman -S certbot-apache

cd /etc/httpd/conf
sudo vi extra/httpd-vhosts.conf
```
```text
(...)
<VirtualHost *:80>
	ServerAdmin admin@example.com
	ServerName my_domain.ddns.net
	ServerAlias my_domain.ddns.net
	DocumentRoot /etc/httpd
</VirtualHost>
(...)
```
```bash
sudo certbot --apache
sudo certbot renew
sudo systemctl restart httpd
```

### Systemd (SSL version)

```bash
cat << EOF | sudo tee /etc/systemd/system/seafile.service
[Unit]
Description=Seafile server
After=network.target mysqld.service

[Service]
Type=oneshot
ExecStart=/srv/seafile/seafile/seafile-server-latest/seafile.sh start
ExecStop=/srv/seafile/seafile/seafile-server-latest/seafile.sh stop
RemainAfterExit=yes
User=seafile
Group=nogroup

[Install]
WantedBy=multi-user.target
EOF
```
```bash
sudo systemctl start seafile
sudo systemctl status seafile
sudo systemctl enable seafile
```
```bash
cat << EOF | sudo tee /etc/systemd/system/seahub.service
[Unit]
Description=Seahub server
After=network.target mysqld.service seafile.service

[Service]
Type=oneshot
ExecStart=/srv/seafile/seafile/seafile-server-latest/seahub.sh start-fastcgi
ExecStop=/srv/seafile/seafile/seafile-server-latest/seahub.sh stop
RemainAfterExit=yes
User=seafile
Group=nogroup

[Install]
WantedBy=multi-user.target
EOF
```
```bash
sudo systemctl daemon-reload
sudo systemctl start seahub
sudo systemctl status seahub
sudo systemctl enable seahub
```

### seafile.conf

```bash
sudo -u seafile -s /bin/sh
cd $HOME/seafile
vi conf/seafile.conf
```
```text
[fileserver]
port = 8082

[database]
type = mysql
host = 127.0.0.1
port = 3306
user = seafile
password = ***********
db_name = seafile-db
connection_charset = utf8

[history]
# Number of days to keep version history
keep_days = 30

[library_trash]
# How often trashed libraries are scanned for removal, default 1 day.
scan_days = 1
# How many days to keep trashed libraries, default 30 days.
expire_days = 30
```

## Desktop Installation

```bash
mkdir /tmp/cower
cd /tmp/cower

cower -d libsearpc
cd /tmp/cower/libsearpc
makepkg -sri
cower -d ccnet-server
cd /tmp/cower/ccnet-server
makepkg -sri
cower -d ccnet
cd /tmp/cower/ccnet
makepkg -sri
cower -d seafile
cd /tmp/cower/seafile
makepkg -sri
cower -d seafile-client
cd /tmp/cower/seafile-client
makepkg -sri
```

## Migrate to Pro version

https://customer.seafile.com/downloads/

https://download.seafile.com/d/XXXXXXXXX/?p=/pro

```bash
sudo pacman -S python2-memcached python2-ldap python-urllib3 poppler python-boto jre7-openjdk

sudo -u seafile -s /bin/sh
cd $HOME
wget "https://download.seafile.com/d/XXXXXXXXX/files/?p=/pro/seafile-pro-server_6.0.13_x86-64.tar.gz&dl=1" -O seafile-pro-server_6.0.13_x86-64.tar.gz

mv seafile-pro-server_* seafile
cd seafile
tar -xzf seafile-pro-server_*
mv seafile-pro-server_*.tar.gz installed

cd seafile-pro-server-6.0.13/

vi pro/pro.py 
```
```text
#!/usr/bin/env python2
```
```bash
./pro/pro.py setup --migrate
exit
sudo systemctl stop seahub
sudo systemctl stop seafile
sudo systemctl start seafile
sudo systemctl start seahub
```

## Two Factor Authentication

- Activate in the admin account
- Enable in each user Settings

## Web Resumable File Upload

```bash
sudo -u seafile -s /bin/sh
cd $HOME/seafile/conf
```
```bash
vi seahub_settings.py
```
```text
ENABLE_RESUMABLE_FILEUPLOAD = True
```
```bash
exit
sudo systemctl stop seahub
sudo systemctl stop seafile
sudo systemctl start seafile
sudo systemctl start seahub
```

## Office integration

```bash
sudo pacman -S libreoffice

sudo systemctl stop seahub
sudo systemctl stop seafile
sudo systemctl start seafile
sudo systemctl start seahub
```

## conf/seafevents.conf

```bash
sudo -u seafile -s /bin/sh
cd $HOME/seafile/conf
```
```bash
vi seafevents.conf
```
```text
[DATABASE]
type = mysql
host = 127.0.0.1
port = 3306
username = seafile
password = *****************
name = seahub-db

[Audit]
enable = true

[INDEX FILES]
enabled = true
interval = 10m

## If true, indexes the contents of office/pdf files while updating search index
## Note: If you change this option from "false" to "true", then you need to clear the search index and update the index again. See the FAQ for details.
index_office_pdf = true

## AFTER 6.3
highlight = fvh

[OFFICE CONVERTER]
enabled = true
workers = 1

## how many pages are allowed to be previewed online. Default is 50 pages
max-pages = 50

## the max size of documents allowed to be previewed online, in MB. Default is 10 MB
## Previewing a large file (for example >30M) online is likely going to freeze the browser.
max-size = 10

[SEAHUB EMAIL]
enabled = true

## interval of sending Seahub email. Can be s(seconds), m(minutes), h(hours), d(days)
interval = 30m
```
```bash
exit
sudo systemctl stop seahub
sudo systemctl stop seafile
sudo systemctl start seafile
sudo systemctl start seahub
```

## Cleanup and Maintenance

```bash
sudo -u seafile -s /bin/sh
cd $HOME/seafile/seafile-server-latest
```

- Garbage collection

```bash
./seaf-gc.sh --dry-run
./seaf-gc.sh
```

- Check integrity

```bash
./seaf-fsck.sh
./seaf-fsck.sh --repair
```

- Clean Database

```bash
./seahub.sh clearsessions
exit
mysql -u seafile -p;
use seahub-db;
DELETE FROM Event WHERE to_days(now()) - to_days(timestamp) > 90;
```

## Minor version upgrades (like from 5.0.x to 5.1.y)

https://customer.seafile.com/downloads/

https://download.seafile.com/d/XXXXXXXXX/?p=/pro

```bash
sudo systemctl stop seahub
sudo systemctl stop seafile

sudo -u seafile -s /bin/sh
cd $HOME
wget "https://download.seafile.com/d/XXXXXXXXX/files/?p=/pro/seafile-pro-server_6.1.1_x86-64.tar.gz&dl=1" -O seafile-pro-server_6.1.1_x86-64.tar.gz

mv seafile-pro-server_* seafile
cd seafile
tar -xzf seafile-pro-server_*
mv seafile-pro-server_*.tar.gz installed

cd seafile-pro-server-6.1.1/
ls upgrade/upgrade_*
upgrade/upgrade_6.0_6.1.sh

exit
sudo systemctl start seafile
sudo systemctl start seahub
```

## Maintenance version upgrades (like from 5.1.2 to 5.1.3)

https://customer.seafile.com/downloads/

https://download.seafile.com/d/XXXXXXXXX/?p=/pro

```bash
sudo systemctl stop seahub
sudo systemctl stop seafile

sudo -u seafile -s /bin/sh
cd $HOME
wget "https://download.seafile.com/d/XXXXXXXXX/files/?p=/pro/seafile-pro-server_6.1.4_x86-64.tar.gz&dl=1" -O seafile-pro-server_6.1.4_x86-64.tar.gz

mv seafile-pro-server_* seafile
cd seafile
tar -xzf seafile-pro-server_*
mv seafile-pro-server_*.tar.gz installed

cd seafile-pro-server-6.1.4/
upgrade/minor-upgrade.sh

exit
sudo systemctl start seafile
sudo systemctl start seahub
```

If you get this error starting: `ccnet-server: symbol lookup error: /usr/lib/libnssutil3.so: undefined symbol: PR_GetEnvSecure`

```bash
sudo -u seafile -s /bin/sh
cd $HOME/seafile/seafile-pro-server-6.1.7/seafile/lib
mv libnspr4.so libnspr4.so.old
```

## Major version upgrades (like from 6 to 7)

https://download.seafile.com/published/support/server-upgrade/pro-7.0.md

```bash
java -version
	java version "1.7.0_171"
sudo pacman -S jre8-openjdk
archlinux-java get
	java-7-openjdk/jre
sudo archlinux-java set java-8-openjdk/jre
java -version
	openjdk version "1.8.0_212"
archlinux-java get
	java-8-openjdk/jre

sudo systemctl stop seahub
sudo systemctl stop seafile

sudo -u seafile -s /bin/sh
cd $HOME
wget "https://download.seafile.com/d/XXXXXXXXX/files/?p=/pro/seafile-pro-server_7.0.3_x86-64.tar.gz&dl=1" -O seafile-pro-server_7.0.3_x86-64.tar.gz

mv seafile-pro-server_* seafile
cd seafile
tar -xzf seafile-pro-server_*
mv seafile-pro-server_*.tar.gz installed

cd seafile-pro-server-7.0.3
upgrade/upgrade_6.3_7.0.sh
```
```bash
cd $HOME/seafile/conf
vi seahub_settings.py
```
```text
ENCRYPTED_LIBRARY_VERSION = 3
```
```bash
vi seafevents.conf
```
```text
[FILE HISTORY]
enabled = true
suffix = md,txt,ppt,pptx,doc,docx,xls,xlsx
```
```bash
exit
sudo systemctl start seafile
sudo systemctl start seahub
```