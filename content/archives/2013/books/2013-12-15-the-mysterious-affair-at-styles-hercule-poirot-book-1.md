---
title: "The Mysterious Affair at Styles (Hercule Poirot Book 1) by Agatha Christie (1920)"
slug: "the-mysterious-affair-at-styles-hercule-poirot-book-1"
author: "Paulo Pereira"
date: 2013-12-15T23:50:00+00:00
lastmod: 2013-12-15T23:50:00+00:00
cover: "/posts/books/the-mysterious-affair-at-styles-hercule-poirot-book-1.jpg"
description: "Finished “The Mysterious Affair at Styles (Hercule Poirot Book 1)” by Agatha Christie."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1920s
book authors:
  - Agatha Christie
book series:
  - Hercule Poirot Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Agatha Christie
  - Hercule Poirot Series
  - Fiction
  - Mystery
  - 5/10 Books
  - Audiobook
---

Finished “The Mysterious Affair at Styles”.
* Author: [Agatha Christie](/book-authors/agatha-christie)
* First Published: [October 1920](/book-publication-year/1920s)
* Series: [Hercule Poirot](/book-series/hercule-poirot-series) Book 1
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43556123-the-mysterious-affair-at-styles)
>
> Agatha Christie's debut novel was the first to feature Hercule Poirot, her famously eccentric Belgian detective.
>
> A refugee of the Great War, Poirot is settling in England near Styles Court, the country estate of his wealthy benefactor, the elderly Emily Inglethorp. When Emily is poisoned and the authorities are baffled, Poirot puts his prodigious sleuthing skills to work. Suspects are plentiful, including the victim’s much younger husband, her resentful stepsons, her longtime hired companion, a young family friend working as a nurse, and a London specialist on poisons who just happens to be visiting the nearby village.
>
> All of them have secrets they are desperate to keep, but none can outwit Poirot as he navigates the ingenious red herrings and plot twists that earned Agatha Christie her well-deserved reputation as the queen of mystery.