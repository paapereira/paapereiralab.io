---
title: "What Would Google Do? by Jeff Jarvis (2009)"
slug: "what-would-google-do"
author: "Paulo Pereira"
date: 2013-06-16T23:56:00+01:00
lastmod: 2013-06-16T23:56:00+01:00
cover: "/posts/books/what-would-google-do.jpg"
description: "Finished “What Would Google Do?” by Jeff Jarvis."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2009
book authors:
  - Jeff Jarvis
book series:
  - 
book genres:
  - Business
  - Nonfiction
  - Technology
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Jeff Jarvis
  - Business
  - Nonfiction
  - Technology
  - 5/10 Books
  - Audiobook
---

Finished “What Would Google Do?”.
* Author: [Jeff Jarvis](/book-authors/jeff-jarvis)
* First Published: [2009](/book-publication-year/2009)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/9886576-what-would-google-do)
>
> A bold and vital book that asks and answers the most urgent question of today: What Would Google Do?
>
> In a book that's one part prophecy, one part thought experiment, one part manifesto, and one part survival manual, internet impresario and blogging pioneer Jeff Jarvis reverse-engineers Google—the fastest-growing company in history—to discover forty clear and straightforward rules to manage and live by. At the same time, he illuminates the new worldview of the internet generation: how it challenges and destroys, but also opens up vast new opportunities. His findings are counterintuitive, imaginative, practical, and above all visionary, giving readers a glimpse of how everyone and everything—from corporations to governments, nations to individuals—must evolve in the Google era.
>
> Along the way, he looks under the hood of a car designed by its drivers, ponders a worldwide university where the students design their curriculum, envisions an airline fueled by a social network, imagines the open-source restaurant, and examines a series of industries and institutions that will soon benefit from this book's central question.
>
> The result is an astonishing, mind-opening book that, in the end, is not about Google. It's about you.