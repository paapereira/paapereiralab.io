---
title: "Terminus (Threshold Book 4) by Peter Clines (2020)"
slug: "terminus-threshold-book-4"
author: "Paulo Pereira"
date: 2020-09-27T21:59:00+01:00
lastmod: 2020-09-27T21:59:00+01:00
cover: "/posts/books/terminus-threshold-book-4.jpg"
description: "Finished “Terminus (Threshold Book 4)” by Peter Clines."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Peter Clines
book series:
  - Threshold Series
book genres:
  - Fiction
  - Science Fiction
  - Horror
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Peter Clines
  - Threshold Series
  - Fiction
  - Science Fiction
  - Horror
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/terminus-threshold-book-4
---

Finished “Terminus”.
* Author: [Peter Clines](/book-authors/peter-clines)
* First Published: [January 30th 2020](/book-publication-year/2020)
* Series: [Threshold](/book-series/threshold-series) Book 4
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/49122653-terminus)
>
> Murdoch’s past has finally come crashing down on him. His former girlfriend. His Family. He’s been happily avoiding them for ages, trying to live something close to a normal life. But now he’s been drawn back into another one of their ludicrous attempts to bring about the end of all things.
>
> Chase has spent the past year just trying to get away. Trying to escape the memories that won’t stop following him, the moment when his life collapsed. He’s traveled around the world trying to stay ahead of it all, but those final moments may be catching up with him at last.
>
> Anne is tired of living in the past. She’s finally looking to the future and embracing her destiny. She’s going to lead the Family forward on their greatest, final crusade to destroy the hated Machine of their long-time adversary.
>
> Their paths will intersect in the middle of nowhere, on an uncharted island where the walls of reality are thin... and an apocalyptic threat is tearing its way through.