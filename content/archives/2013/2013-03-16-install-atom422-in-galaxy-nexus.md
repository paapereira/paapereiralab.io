---
title: "Install atom422 in Galaxy Nexus"
slug: "install-atom422-in-galaxy-nexus"
author: "Paulo Pereira"
date: 2013-03-16T21:31:00+00:00
lastmod: 2013-03-16T21:31:00+00:00
draft: false
toc: true
categories:
  - Android
tags:
  - Android
  - Galaxy Nexus
  - atom422
  - Ak Kernel
---

I've been using [atom422](http://forum.xda-developers.com/showthread.php?t=1969162) ROM with [Ak Kernel](/tags/ak-kernel) for a week now, and I must say this is the best ROM/kernel combo I had in my Galaxy Nexus. And it's underclocked!

I will be using atom422 v9 and Ak Kernel 731.

## Backing up your device

- Backup using [Titanium Backup](https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup)
- Backup [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher) settings
- Backup SMS using [SMS Backup & Restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore)
- Reboot into recovery (I have ClockworkMod Recovery v6.0.2.3)
- Backup (backup and restore > backup)
- Go into "install zip from sdcard > choose zip from sdcard" just  to guarantee that ClockworkMod Recovery is accessing the storage
- Plug the phone into a computer (I'm using Ubuntu 12.10, and have already android-sdk)
- Go into your android-sdk tools folder
  ```bash
  cd Android/android-sdk-linux_86/tools/
  ```
- Go into adb shell and list the directories in your "sdcard" to decide what to backup
  ```bash
  sudo ./adb shell
  ls /data/media/0
  exit
  ```
- Pull all the directories you want to backup
  ```bash
  sudo ./adb pull /data/media/0/examplefolder sdcard/examplefolder
  ```
- The previous example will create a sdcard folder in your tools folder with the data folder pulled from your phone
- Change the owner of the pulled folders
  ```bash
  sudo chown -R youruser:youruser sdcard
  ```

You can also backup using FTP for example. I normally use [WellFTP Server](https://play.google.com/store/apps/details?id=com.gsapp.wifitransport).

Don't forget to backup at least your "clockwork" and "TitaniumBackup" folders!

Note: clockwork backups are located in "/data/media/clockworkmod" and  not in "/data/media/0/clockworkmod", at least at my system.

Check in what folder you have the following folders: "backup" and "blobs". That will be the correct clockwork backup folder.

## Wiping your device

- Format /data and /cache (wipe data/factory reset)
- Format /system (mounts and storage > format /system)
- Format /sdcard (mounts and storage > format /sdcard)
- Wipe Dalvik Cache (advanced > wipe dalvik cache)

After these steps, your device, including the "sdcard" is cleaned without ROM or files.

## Installing atom422

I will be installing atom422 v9 (ATOM422_maguro_v9), based on Android 4.2.2, Google Apps (gapps-jb-20130301-signed), a productivity pack  provided for this ROM (Email/exchange, thinkfree document software,  video editor) and Ak Kernel Cylon edition, version 731.

- Create a new folder in your "sdcard"
  ```bash
  sudo ./adb shell
  cd /data/media
  mkdir 0/0.ROOT/atom422
  exit
  ```
- Push the ROM, Google Apps, productivity pack and the kernel into your phone
  ```bash
  sudo ./adb push ~/ATOM422_maguro_v9.zip /data/media/0/0.ROOT/atom422
  sudo ./adb push ~/gapps-jb-20130301-signed.zip /data/media/0/0.ROOT/atom422
  sudo ./adb push ~/Productivity.zip /data/media/0/0.ROOT/atom422
  sudo ./adb push ~/AK.731.CYLON.zip /data/media/0/0.ROOT/atom422
  ```
- Install the ROM (install zip from sdcard > choose zip from sdcard > 0 > 0.ROOT > atom422 > ATOM422_maguro_v9.zip)
- Install the Google Apps (install zip from sdcard > choose zip  from sdcard > 0 > 0.ROOT  > atom422 > gapps-jb-20130301-signed.zip)
- Install the productivity pack (install zip from sdcard > choose  zip from sdcard > 0 > 0.ROOT > atom422 > Productivity.zip)
- Install the kernel (install zip from sdcard > choose zip from sdcard > 0 > 0.ROOT > atom422 > AK.731.CYLON.zip)
- Reboot (reboot system now)

## Restoring your "sdcard"

- Reboot into Recovery
- Push your backed up folders into your "sdcard"
  ```bash
  sudo ./adb push sdcard/examplefolder /data/media/0
  ```
- Reboot (reboot system now)

When restoring I tend to use [WellFTP Server](https://play.google.com/store/apps/details?id=com.gsapp.wifitransport).

## Restoring your apps

- Restore apps using [Titanium Backup](https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup)
- Restore [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher) settings
- Restore SMS using [SMS Backup & Restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore)

## Ak Kernel settings

To change Ak Kernel setting use [Trickster MOD Kernel Settings](https://play.google.com/store/apps/details?id=com.bigeyes0x0.trickstermod). Don't forget to donate to the developer by purchasing the premium [key](https://play.google.com/store/apps/details?id=com.bigeyes0x0.trickstermod.premium).

I'm using the settings showed in this [post](http://forum.xda-developers.com/showpost.php?p=38030897&postcount=13473) by Nephilim. Yes, it's underclocked but smooth as never before.