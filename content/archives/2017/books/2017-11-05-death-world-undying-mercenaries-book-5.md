---
title: "Death World (Undying Mercenaries Book 5) by B.V. Larson (2015)"
slug: "death-world-undying-mercenaries-book-5"
author: "Paulo Pereira"
date: 2017-11-05T22:30:00+00:00
lastmod: 2017-11-05T22:30:00+00:00
cover: "/posts/books/death-world-undying-mercenaries-book-5.jpg"
description: "Finished “Death World (Undying Mercenaries Book 5)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Death World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [March 27th 2015](/book-publication-year/2015)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 5
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31117168-death-world)
>
> Unknown aliens attack Earth. Their planet is uncharted, mysteriously having avoided detection for centuries. It's a world packed with the most vicious aliens humanity has yet to encounter. James McGill has discovered: Death World.
>
> In the fifth book of the Undying Mercenaries series, the war comes home, and aliens strike a devastating blow. Bent on revenge, Legion Varus chases the raiders to the stars and discovers a growing alien menace. A cancerous species has invaded our region of the galaxy and must be dealt with. McGill learns why the Cephalopod Kingdom has yet to attack Earth and what's happening behind the scenes in the Core Worlds. Throughout, he upholds his unique sense of right, wrong, and honor. Death World is a military science fiction novel by best-selling author B. V. Larson.