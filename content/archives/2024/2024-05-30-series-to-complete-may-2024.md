---
title: "Books Series to complete buying"
slug: "series-to-complete-may-2024"
author: "Paulo Pereira"
date: 2024-05-30T12:30:00+01:00
lastmod: 2024-05-30T12:30:00+01:00
description: "Short list of series I'm yet to complete buying."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Wishlist
---

December update for my short list of series to complete buying.

* Mars Trilogy by Kim Stanley Robinson
* The Sun Eater by Christopher Ruocchio
* Culture by Iain M. Banks
* New Crobuzon by China Mieville
* The Long Earth by Terry Pratchett and Stephen Baxter
* The Last Kingdom by Bernard Cornwell
* The Legend of Drizzt by R. A. Salvatore
* Sookie Stackhouse by Charlaine Harris
* The Matthew Corbett Novels by Robert McCammon
* Marsbound by Joe Haldeman
* The Rampart Trilogy by M. R. Carey
* The Southern Reach Trilogy by Jeff VanderMeer
* Wanderers by Chuck Wendig
* Zones of Thought (Qeng Ho) by Vernor Vinge
* Star Wars Universe books
* Doctor Who