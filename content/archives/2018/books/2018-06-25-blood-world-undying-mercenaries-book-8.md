---
title: "Blood World (Undying Mercenaries Book 8) by B.V. Larson (2017)"
slug: "blood-world-undying-mercenaries-book-8"
author: "Paulo Pereira"
date: 2018-06-25T22:07:00+01:00
lastmod: 2018-06-25T22:07:00+01:00
cover: "/posts/books/blood-world-undying-mercenaries-book-8.jpg"
description: "Finished “Blood World (Undying Mercenaries Book 8)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Blood World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [2017](/book-publication-year/2017)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 8
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/38768761-blood-world)
>
> A dirty deal was struck. Humanity was allowed to keep three hundred rebellious worlds. In return, we declared war on a powerful enemy from beyond the frontier.
>
> A frantic build-up of forces has begun, but the task is hopeless. Seeking allies, Earth’s legions are sent to BLOOD WORLD. A planet on the fringe of known space, where the people only respect masters of combat.
>
> Earth’s Legions must impress them, but other alien powers have been invited to join the contest. The prize consists of billions of loyal troops—Earth must win.
>
> Fighting and dying and fighting again, the struggle is half-mad—but so is James McGill.
>
> BLOOD WORLD is the eighth book in the Undying Mercenaries Series.