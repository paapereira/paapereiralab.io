---
title: "Create an usb recovery disk for booting b3 server"
slug: "create-usb-recovery-disk-for-booting-b3"
author: "Paulo Pereira"
date: 2011-05-01T16:08:00+01:00
lastmod: 2011-05-01T16:08:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
aliases:
  - /posts/create-usb-recovery-disk-for-booting-b3/
---

I can't access my [B3 Server](http://www.excito.com/) and I don't know why... :( I was away for a week and in between I couldn't access my server.

Now, after locally rebooting it, still no joy...

I will show how to create an usb recovery disk to boot the B3 Server and how to backup your B3 storage to your local computer.
After this you can do a full recovery, formating and reinstalling your B3 Server.

## Creating the usb recovery disk

- You'll need an empty usb stick (*it will be formatted*)

- Find out the device reference of your usb stick (*check the LABEL in the output*)
	- or use gparted (*sudo apt-get gparted*) to find out 

```bash
sudo blkid
```

- Remove all existing partitions from the usb stick
	- I used gparted to remove all partitions

- Create a new primary partition using the *cfdisk* interface to:
	- create a new primary partition
  - make it bootable
  - change it to type W95 FAT32

```bash
sudo cfdisk /dev/sdc
```

- Create a new vfat partition

```bash
sudo mkfs.vfat /dev/sdc1
```

- Download the latest B3 install image from http://update.excito.net/install/latest/b3/

- - Extract the zip file to the usb stick

```bash
zip b3-install-2.2.1.zip /media/usb_disk
```

- Change the bubba.cfg file in the *install* folder using you favorite text editor
	- change *DO_INSTALL=1* to *DO_INSTALL=0* 
  - **this is very important in order to enter the recovery mode and not the reinstall mode...**

```bash
gedit /media/usb_disk/install/bubba.cfg
```

## Boot the B3 Server into recovery

- Remove all external drives connect to the b3 server
- Unplug the power cord
- Connect the b3 server WAN connector to a DHCP server (like your router)
- Put the usb recovery disk in one of the usb ports
- While pressing the b3 power button connect the power cord
- Keep the power button pressed for like a minute (important)
- Check in your DHCP server the b3 IP address (example: 100.10.10.10)
- Connect via ssh to the b3 server as *root*
	- the password is *excito*

```bash
ssh root@100.10.10.10
```

## Mount the B3 root disk and the B3 storage disk

- Mount the B3 root disk

```bash
mount /dev/sda1 /mnt
```

- The contents of the disk can be accessed in */mnt*

```bash
ls /mnt
```

- The B3 storage disk is a Logical Volume in */dev/sda2*

- Check the existing volume groups

```bash
vgscan
```

- Activate the volume group 

```bash
vgchange -a y
```

- Mount the B3 storage disk (after unmounting the root disk)

```bash
cd /
umount /mnt
mount /dev/bubba/storage /mnt/
```

- The contents of the disk can be accessed in */mnt*

```bash
ls /mnt
```

## Backup the content of the B3 Server disks

- Example: backup the home folders
- Check the available folders

```bash
ls /mnt
```

- Copy the content of the folders via ssh (example: copy B3 Server folder *b3userfolder* to your *userfolder* for your *localuser* at your local IP address)

```bash
cd /mnt
scp -r b3userfolder/ localuser@100.10.10.22:/home/localuser/b3-bck
```

- How to copy everything

```bash
scp -r */ localuser@100.10.10.22:/home/localuser/b3-bck
```

*References:*

- *[Excito Wiki](http://wiki.excito.org/wiki/index.php/Rescue_System)*