---
title: "Falling Bodies (The Far Reaches Book 3) by Rebecca Roanhorse (2023)"
slug: "falling-bodies-the-far-reaches-book-3"
author: "Paulo Pereira"
date: 2023-09-15T21:00:00+01:00
lastmod: 2023-09-15T21:00:00+01:00
cover: "/posts/books/falling-bodies-the-far-reaches-book-3.jpg"
description: "Finished “Falling Bodies (The Far Reaches Book 3)” by Rebecca Roanhorse."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - Rebecca Roanhorse
book series:
  - The Far Reaches Series
book genres:
  - Fiction
  - Science Fiction
  - Short Stories
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Rebecca Roanhorse
  - The Far Reaches Series
  - Fiction
  - Science Fiction
  - Short Stories
  - 6/10 Books
  - Ebook
---

Finished “Falling Bodies”.
* Author: [Rebecca Roanhorse](/book-authors/rebecca-roanhorse)
* First Published: [June 27, 2023](/book-publication-year/2023)
* Series: [The Far Reaches](/book-series/the-far-reaches-series) Book 3
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/151908270-falling-bodies)
>
> A young man caught between two disparate worlds searches for his place in the universe in a wrenching short story by New York Times bestselling author Rebecca Roanhorse.
> 
> Light-years from home, it’s Ira’s second chance. Just another anonymous student at a space station university. Not the orphan whose Earther heritage was erased. Not some social experiment put on display by his adoptive father. Not the criminal recruited by the human rebels. But when Ira’s loyalties clash once again, two wars break one on the ground and one within himself. Which will Ira stand with? Which will take him down?