---
title: "Junk by Les Bohem (2019)"
slug: "junk"
author: "Paulo Pereira"
date: 2020-02-25T20:49:00+00:00
lastmod: 2020-02-25T20:49:00+00:00
cover: "/posts/books/junk.jpg"
description: "Finished “Junk” by Les Bohem."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Les Bohem
book series:
  -
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Les Bohem
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Junk”.
* Author: [Les Bohem](/book-authors/les-bohem)
* First Published: [February 28th 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44148094-junk)
>
> Set in present-day Los Angeles, Junk explores an epic conspiracy, one at work for thousands of years that involves total takeover of the planet Earth by aliens. In the wild, souped-up vision of Les Bohem - the acclaimed, Emmy-winning writer of the Steven Spielberg miniseries, Taken - the world is at the end stage of long-range plot that involves a gigantic genetic-engineering project. The aliens who have invaded us have no planet. No spaceship is coming. Instead, a small advance force comes, breeds, and dies - thus becoming an anomaly in our DNA that can’t be explained. In Junk, seers for centuries have had visions that turned out to be messages from our alien DNA. The time for the takeover is now. The aliens are ready, and they are starting to bloom.