---
title: "B3 Server 2.3.1 Update"
slug: "b3-server-231-update"
author: "Paulo Pereira"
date: 2011-11-05T22:27:00+00:00
lastmod: 2011-11-05T22:27:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
---

Minor update released [last week](http://forum.excito.net/viewtopic.php?f=1&t=3408).

## Upgrading to 2.3.1

- Become root

```bash
su
```

- Do an upgrade

```bash
apt-get update
apt-get upgrade
```

- Upgrade b3 backend

```bash
apt-get install bubba-backend
```

You can confirm the software version going into b3 web administration page, then Settings and Software update.

![B3 Server](/posts/2011/2011-11-05-b3-server-231-update/b3server.png)

## Official Excito forum notes

A major part of this update is a new software update service. When updating the update service, strange things can happen, so:

```text
* If the update doesn't go all the way but hangs just before it should 
 be finished (99 or 100%) this is normal. This is in the transition to 
 the new updater logic. Just wait a few minutes and then reload the 
 page.
*** After the update - restart your B3.
* The updater might look like it hangs, it doesn't'! Please don't reload 
 the page, be patient, it starts again.
```