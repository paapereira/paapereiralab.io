---
title: "Updating AOKP in Galaxy Nexus"
slug: "updating-aokp-in-galaxy-nexus"
author: "Paulo Pereira"
date: 2013-02-11T22:48:00+00:00
lastmod: 2013-02-11T22:48:00+00:00
draft: false
toc: true
categories:
  - Android
tags:
  - Android
  - AOKP
  - Galaxy Nexus
aliases:
  - /posts/updating-aokp-in-galaxy-nexus/
---

[AOKP build 3](http://aokp.co/release-jb-mr1-build-3/) is out, and after showing how to [install AOKP from scratch](/posts/install-aokp-in-galaxy-nexus/), this is how I usually upgrade a ROM in my Galaxy Nexus.

![aokp_maguro_jb-mr1_build-3](/posts/2013/2013-02-11-updating-aokp-in-galaxy-nexus/aokp.png)

I only use this procedure between builds of the same ROM, and typically  when the base Android version is the same (in this case 4.2.1).

## Backing up your device

When upgrading I usually only backup with Titanium Backup and within  ClockworkMod Recovery. Usually I don't copy the backups to my desktop.

- Backup using [Titanium Backup](https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup)
- Backup [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher) settings
- Backup SMS using [SMS Backup & Restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore)
- Reboot into recovery (I have ClockworkMod Recovery v6.0.2.3)
- Backup (backup and restore > backup)

## Clearing cache and /system

Before installing I usually clear cache and format the /system  partition. Depending on the ROM or the changes between versions,  formatting the /system partition may not be necessary. Still I usually  do it.

- Clear /cache (wipe cache partition)
- Wipe Dalvik Cache (advanced > wipe dalvik cache)
- Format /system (mounts and storage > format /system)

After these steps, your apps data and sdcard files are safe. Only the system files are gone.
If something goes wrong you can always restore in recovery (backup and restore > restore).

## Installing the ROM update

Download [AOKP JB-MR1 Build 3](http://aokp.co/release-jb-mr1-build-3/) for the Galaxy Nexus (maguro) including the Google Apps package.

- Push the ROM and Google Apps into your phone
```bash
sudo ./adb push ~/aokp_maguro_jb-mr1_build-3.zip /data/media/0/0.ROOT/AOKP
sudo ./adb push ~/gapps-jb-20121212-signed.zip /data/media/0/0.ROOT/AOKP
```
- I will also install [franco.Kernel](/posts/favapps-francokernel-updater/)
```bash
sudo ./adb push ~/franco.Kernel-r364.zip /data/media/0/0.ROOT/AOKP
```
- Install the ROM (install zip from sdcard > choose zip from sdcard > 0 > 0.ROOT > AOKP > aokp_maguro_jb-mr1_build-3.zip)
- Install the Google Apps (install zip from sdcard > choose zip  from sdcard > 0 > 0.ROOT > AOKP  > gapps-jb-20121212-signed.zip)
- Reboot (reboot system now)
- Reboot into Recovery
- Wipe Cache (wipe cache partition)
- Wipe Dalvik Cache (advanced > wipe dalvik cache)
- Install franco.Kernel (install zip from sdcard > choose zip from  sdcard > 0 > 0.ROOT > AOKP > franco.Kernel-r364.zip)
- Reboot (reboot system now)

Enjoy!