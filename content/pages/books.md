---
title: "Books"
slug: "books"
cover: "/pages/library-banner.jpg"
toc: true
aliases:
  - /pages/books
---

Reading is one my favorite things to do since I was little.

For the last years I've been reading ebooks in a [Kindle Paperwhite](/tags/kindle-paperwhite) and listening to books using [Audible](https://www.audible.com/) or [audiobookshelf](https://www.audiobookshelf.org/).

## Books

* Follow my [Book Log](/tags/book-log/) and [Schedule](/tags/books-schedule/)
* You can browse by [Authors](/book-authors), [Series](/book-series), [Genres](/book-genres), [Publication Year](/book-publication-year) or [Book Rating](/book-ratings)
* Check my [reading list](/reading-list) from the 80s until today, my [Book Hauls](/tags/book-haul), my [Wishlist](/tags/books-wishlist) and my [Year in Books](/tags/year-in-books)
* If your curious on how I rate my books, check my [posts](/tags/books-rating/) about it.

## Goodreads

* [Follow me](https://www.goodreads.com/paapereira)
* Browse my [Book Shelfs](https://www.goodreads.com/review/list/16853893?shelf=)
* Follow my [Reading Challenges](https://www.goodreads.com/user_challenges/14834701)
* Check my [Statistics](https://www.goodreads.com/review/stats/16853893-paulo-pereira)
