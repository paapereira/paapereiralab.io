---
title: "Dividend Investing for Beginners by G.R. Tiberius (2021)"
slug: "dividend-investing-for-beginners"
author: "Paulo Pereira"
date: 2022-08-22T21:00:00+01:00
lastmod: 2022-08-22T21:00:00+01:00
cover: "/posts/books/dividend-investing-for-beginners.jpg"
description: "Finished “Dividend Investing for Beginners” by G.R. Tiberius."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - G.R. Tiberius
book series:
  - 
book genres:
  - Nonfiction
  - Self Help
book scores:
  - 7/10 Books
tags:
  - Book Log
  - G.R. Tiberius
  - Nonfiction
  - Self Help
  - 7/10 Books
  - Ebook
---

Finished “Dividend Investing for Beginners: Build your Dividend Strategy, Buy Dividend Stocks Easily, and Achieve Lifelong Passive Income”.
* Author: [G.R. Tiberius](/book-authors/g.r.-tiberius)
* First Published: [July 20, 2021](/book-publication-year/2021)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/58640579-dividend-investing-for-beginners)