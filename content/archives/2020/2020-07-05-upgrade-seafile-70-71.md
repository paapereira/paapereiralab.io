---
title: "Upgrading Seafile from 7.0.x to 7.1.x"
slug: "upgrade-seafile-70-71"
author: "Paulo Pereira"
date: 2020-07-05T14:24:44+01:00
lastmod: 2020-07-05T14:24:44+01:00
cover: "/posts/2020/2020-07-05-upgrade-seafile-70-71/seafile.png"
description: "I’ve waiting to upgrade to [Seafile](https://www.seafile.com/) 7.1 because I keep my installation manually and I wasn't finding the *x86-64.tar.gz option to download.
After asking in the [forums](https://forum.seafile.com/t/seafile-server-7-1-4-is-ready/11910/8), I discovered it's just a change of name.\n

Don't forget to read the [release notes](https://download.seafile.com/published/seafile-manual/upgrade/upgrade_notes_for_7.1.x.md) from Seafile."
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Seafile
---

I’ve waiting to upgrade to [Seafile](https://www.seafile.com/) 7.1 because I keep my installation manually and I wasn't finding the \*x86-64.tar.gz option to download.
After asking in the [forums](https://forum.seafile.com/t/seafile-server-7-1-4-is-ready/11910/8), I discovered it's just a change of name.

Don't forget to read the [release notes](https://download.seafile.com/published/seafile-manual/upgrade/upgrade_notes_for_7.1.x.md) from Seafile.

## Before upgrading

Reading the [release notes](https://download.seafile.com/published/seafile-manual/upgrade/upgrade_notes_for_7.1.x.md), this version needs Python 3 and a set of modules.

### Python 3

```bash
yay -S python python-setuptools python-pip

sudo pip3 install --timeout=3600 wheel Pillow pylibmc captcha jinja2 \
	sqlalchemy django-pylibmc django-simple-captcha python3-ldap \
	django-recaptcha django-ranged-response
```

### Memcached

The Memcached configuration also changed in this version. I wasn't using Memcached, so I took the opportunity to set it up.

- Installation

```bash
yay -S memcached libmemcached
```

- Review the cache size to 128MB

```bash
sudo systemctl edit memcached.service --full
```
```text
Environment=CACHESIZE=128
```

- Enable and start the service

```bash
sudo systemctl enable memcached
sudo systemctl start memcached
sudo systemctl status memcached
```

- Configure seahub settings

```bash
vi conf/seahub_settings.py
```
```text
CACHES = {
	'default': {
		'BACKEND': 'django_pylibmc.memcached.PyLibMCCache',
		'LOCATION': '127.0.0.1:11211',
	},
	'locmem': {
		'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
	},
}
```

## Upgrading

I started by stopping the services:

```bash
sudo systemctl stop seahub
sudo systemctl stop seafile
```

Then I downloaded the latest version (I use the pro version) and executed the upgrade script.

```bash
sudo -u seafile -s /bin/sh

cd /srv/seafile
wget "https://download.seafile.com/d/xxxxxxx/files/?p=/pro/seafile-pro-server_7.1.5_x86-64_CentOS.tar.gz&dl=1" -O seafile-pro-server_7.1.5_x86-64_CentOS.tar.gz
tar -xzf seafile-pro-server_*

cd seafile-pro-server-7.1.5/
upgrade/upgrade_7.0_7.1.sh
```

## Upgrade failed

The upgrade script was failing with syntax errors. I thought it was something to do with the Python 3 migration, but after some digging around I found out that the problem was in the fact that my mysql password had a '%' 😐

I changed the password, reviewed the configuration files to update de password, re-executed the upgrade script and... success.

```bash
mysql -u root -p

use mysql;
SET PASSWORD FOR 'seafile_user'@'localhost' = PASSWORD('newpass');
```

Configuration files to review the mysql password:
* seafile.conf
* seafevents.conf
* seahub_settings.py
* ccnet.conf

## Clean Seahub cache

```bash
rm -rf /tmp/seahub_cache/
sudo systemctl restart memcached
```

## Review seafile webdav configuration

```bash
cd /etc/httpd/conf
sudo vi extra/httpd-vhosts.conf
```
```text
<Location /seafdav>
  ProxyPass "http://127.0.0.1:8080/seafdav"
</Location>
```

## Starting the services

```bash
sudo systemctl start seafile
sudo systemctl start seahub
```

And... seahub didn't start.

After much digging around I found out that the problem was in some python modules.

Some tips om how I debugged the problem:
* Looked in the [forums](https://forum.seafile.com/)
* Looked in the Seafile logs
* Started seahub manually to see the output

To start seahub mannualy change the daemon option in the gunicorn.conf.py file:
```bash
sudo systemctl stop seafile
vi conf/gunicorn.conf.py
```
```text
daemon = False
```
```bash
seafile-server-latest/seahub.sh start
```

I had to manually remove 2 python modules and re-installed them via pip:

```bash
sudo mv /usr/lib/python3.8/site-packages/django_pylibmc* /tmp
sudo mv /usr/lib/python3.8/site-packages/django_simple_captcha-0.5.12.dist-info /tmp
sudo pip3 install --timeout=3600 django-pylibmc django-simple-captcha
```

Don't forget to change the daemon config again:
```bash
vi conf/gunicorn.conf.py
```
```text
daemon = True
```

And voilá:

```bash
sudo systemctl start seafile
sudo systemctl start seahub
```