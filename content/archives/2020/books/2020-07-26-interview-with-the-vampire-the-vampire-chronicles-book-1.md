---
title: "Interview with the Vampire (The Vampire Chronicles Book 1) by Anne Rice (1976)"
slug: "interview-with-the-vampire-the-vampire-chronicles-book-1"
author: "Paulo Pereira"
date: 2020-07-26T18:16:00+01:00
lastmod: 2020-07-26T18:16:00+01:00
cover: "/posts/books/interview-with-the-vampire-the-vampire-chronicles-book-1.png"
description: "Finished “Interview with the Vampire (The Vampire Chronicles Book 1)” by Anne Rice."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Anne Rice
book series:
  - The Vampire Chronicles Series
book genres:
  - Fiction
  - Horror
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Anne Rice
  - The Vampire Chronicles Series
  - Fiction
  - Horror
  - Fantasy
  - 9/10 Books
  - Audiobook
aliases:
  - /posts/books/interview-with-the-vampire-the-vampire-chronicles-book-1
---

Finished “Interview with the Vampire”.
* Author: [Anne Rice](/book-authors/anne-rice)
* First Published: [April 12th 1976](/book-publication-year/1970s)
* Series: [The Vampire Chronicles](/book-series/the-vampire-chronicles-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/26252764-interview-with-the-vampire)
>
> In a darkened room a young man sits telling the macabre and eerie story of his life - the story of a vampire, gifted with eternal life, cursed with an exquisite craving for human blood. Anne Rice's compulsively readable novel is arguably the most celebrated work of vampire fiction since Bram Stoker's Dracula was published in 1897. As the Washington Post said on its first publication, it is a 'thrilling, strikingly original work of the imagination . . . sometimes horrible, sometimes beautiful, always unforgettable'.