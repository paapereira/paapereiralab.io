---
title: "Pines (Wayward Pines Book 1) by Blake Crouch (2012)"
slug: "pines-wayward-pines-book-1"
author: "Paulo Pereira"
date: 2015-12-09T23:05:00+00:00
lastmod: 2015-12-09T23:05:00+00:00
cover: "/posts/books/pines-wayward-pines-book-1.jpg"
description: "Finished “Pines (Wayward Pines Book 1)” by Blake Crouch."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - Blake Crouch
book series:
  - Wayward Pines Series
book genres:
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Blake Crouch
  - Wayward Pines Series
  - Science Fiction
  - 8/10 Books
  - Audiobook
---

Finished “Pines”.
* Author: [Blake Crouch](/book-authors/blake-crouch)
* First Published: [August 21st 2012](/book-publication-year/2012)
* Series: [Wayward Pines](/book-series/wayward-pines-series) Book 1
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25862841-pines)
>
> Wayward Pines, Idaho, is quintessential small-town America - or so it seems. Secret Service agent Ethan Burke arrives in search of two missing federal agents, yet soon is facing much more than he bargained for. After a violent accident lands him in the hospital, Ethan comes to with no ID and no cell phone. The medical staff seems friendly enough, but something feels...off. As the days pass, Ethan's investigation into his colleagues' disappearance turns up more questions than answers. Why can't he make contact with his family in the outside world? Why doesn't anyone believe he is who he says he is? And what's the purpose of the electrified fences encircling the town? Are they keeping the residents in? Or something else out? Each step toward the truth takes Ethan further from the world he knows, until he must face the horrifying possibility that he may never leave Wayward Pines alive...