---
title: "Firefox Widgets"
slug: "firefox-widgets"
author: "Paulo Pereira"
date: 2008-04-17T21:58:00+01:00
lastmod: 2008-04-17T21:58:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Firefox
---

Firefox widgets (buttons, radio buttons, drop down menus, text fields and checkboxes) in Ubuntu don't look so good. I don't like it. And I'm not the only one.
So, we can change the images and CSS code for the widgets so Firefox will look really nice :)

Here's a tip from [Ubuntu Forums](http://ubuntuforums.org/showthread.php?t=369596).

## Steps to make Firefox widgets look good

- Download the Firefox Widget Installer
  
- available at the end of the first post ([direct link](http://ubuntuforums.org/attachment.php?attachmentid=40110&d=1186519785))
  
- Run the installer
  - unpack the archive and run the *graphic_installer*
  - install in the Firefox directory (default is */usr/lib/firefox*)

- Make this the default look, even when Firefox is updated

  - to make the default look

  ```bash
  sudo dpkg-divert --add /usr/lib/firefox/res/forms.css
  ```

  - to undo the previous

  ```bash
  sudo dpkg-divert --remove /usr/lib/firefox/res/forms.css
  ```