---
title: "List of favorite applications, sites and other things"
slug: "favorites"
draft: false
toc: true
aliases:
  - /pages/favorites
---

## Hardware

* [NUC6i5SYH](https://ark.intel.com/content/www/us/en/ark/products/89190/intel-nuc-kit-nuc6i5syh.html) is my main desktop machine
* [NUC7i3BNH](/posts/2020/05/new-nuc7i3bnh/) is my home server
* [Surface Go](/posts/install-arch-linux-surface-go) is my personal laptop
* [Vultr](/tags/vultr) as my cloud provider

In my own hardware I'm running [Arch Linux](https://www.archlinux.org/).

## Operating System

* [Arch Linux](https://www.archlinux.org/) as my daily driver in my desktop, home server and laptop
* [Debian 10](https://www.debian.org/) in [Vultr](/tags/vultr)

## Window Manager, Desktop Environments and Android Launchers

* [leftwm](https://github.com/leftwm/leftwm) is my window manager of choice and daily driver #linux
* [i3-gaps](https://en.wikipedia.org/wiki/I3_(window_manager)) was my daily driver before leftwm #linux
* [Xfce](https://en.wikipedia.org/wiki/Xfce) if I some day went back to a desktop environment #linux
* [Olauncher](https://play.google.com/store/apps/details?id=app.olauncher&hl=en_US&gl=US) #android

## Browsers

* [LibreWolf](https://librewolf-community.gitlab.io/) is currently my default browser ➡ [Read more](/tags/librewolf) #linux
* [Firefox](https://www.mozilla.org/en-US/firefox/) is my backup browser #linux #android
* [Chromium](https://www.chromium.org/) if I need for some reason a Chrome based browser #linux

## Email

* [Thunderbird](https://en.wikipedia.org/wiki/Mozilla_Thunderbird) #linux
* [Evolution](https://en.wikipedia.org/wiki/GNOME_Evolution) #linux
* [neomutt](https://neomutt.org/) #linux
* [Proton Mail](https://proton.me/) #service

## File managers

* [Nemo](https://github.com/linuxmint/nemo) #linux
* [Thunar](https://docs.xfce.org/xfce/thunar/start) #linux

## Password management

* [pass](https://www.passwordstore.org/) ➡ [Read more](/posts/pass) #linux #windows
* [Password Store for Android](https://play.google.com/store/apps/details?id=dev.msfjarvis.aps&hl=en_US&gl=US) #android

## Package manager

* [paru](https://github.com/morganamilo/paru) ➡ [Read more](/tags/paru) #linux

## Messaging

* [SimpleX Chat](/posts/simplex-chat) #linux #android
* [Gajim](https://gajim.org/) #linux
* [Pidgin](https://www.pidgin.im/) #linux
* [Conversations](https://play.google.com/store/apps/details?id=eu.siacs.conversations) #android

## RSS reads

* [Thunderbird](https://en.wikipedia.org/wiki/Mozilla_Thunderbird) #linux
* [Newsboat](https://newsboat.org/) #linux

## Notes, tasks and knowledge management

* [VSCodium](https://vscodium.com/) (with plugins) #linux #windows
* [Zim Wiki](https://zim-wiki.org/index.html) #linux #windows
* [Zettel Notes](https://play.google.com/store/apps/details?id=org.eu.thedoc.zettelnotes) #android

## Podcasts

* [gpodder](https://gpodder.net/) #linux
* [AntennaPod](https://play.google.com/store/apps/details?id=de.danoeh.antennapod&hl=en_US&gl=US) #android

## Backups

* [Borg Backup](https://www.borgbackup.org/) ➡ [Read more](/tags/borgbackup) #service
* [Vorta Backup Client](https://github.com/borgbase/vorta) ➡ [Read more](/tags/vorta) #linux
* [borgbase.com](https://www.borgbase.com/) ➡ [Read more](/tags/borgbase)

## Books and comics

* [Calibre](https://calibre-ebook.com/) for ebooks #linux
* [YACReader](https://www.yacreader.com/) for comics #linux

## Personal finances

* [HomeBank](http://homebank.free.fr/en/index.php) #linux

## Syncronisation

* [Seafile](https://www.seafile.com/en/home/) ➡ [Read more](/tags/seafile) #linux #android
* [Syncthing](https://syncthing.net/) #linux #android