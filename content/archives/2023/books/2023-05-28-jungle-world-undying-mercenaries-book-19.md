---
title: "Jungle World (Undying Mercenaries Book 19) by B.V. Larson (2023)"
slug: "jungle-world-undying-mercenaries-book-19"
author: "Paulo Pereira"
date: 2023-05-28T16:00:00+01:00
lastmod: 2023-05-28T16:00:00+01:00
cover: "/posts/books/jungle-world-undying-mercenaries-book-19.jpg"
description: "Finished “Jungle World (Undying Mercenaries Book 19)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Jungle World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [January 20, 2023](/book-publication-year/2023)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 19
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/75256227-jungle-world)
>
> A new threat arrives from the rim of the galaxy. From the colorless depths of space, unknown invaders strike worlds across the Frontier Zone. Rigel is a thousand lightyears farther from the Core Worlds than Earth, so they suffer the first attacks.
> 
> The mood at Central is jubilant. Our greatest rival is falling—now is the time to strike while they’re weak! Legion Varus is deployed to Jungle World, a vital planet deep in Rigellian territory. Our foes, fighting on two fronts, are driven back with ease.
> 
> But one man questions the wisdom of this move. Should Earth expand with greed, aiding new monsters from the Galactic Rim? That lone voice of reason comes from James McGill.