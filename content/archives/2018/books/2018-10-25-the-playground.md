---
title: "The Playground by Ray Bradbury (1953)"
slug: "the-playground"
author: "Paulo Pereira"
date: 2018-10-25T21:51:00+01:00
lastmod: 2018-10-25T21:51:00+01:00
cover: "/posts/books/the-playground.jpg"
description: "Finished “The Playground” by Ray Bradbury."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1950s
book authors:
  - Ray Bradbury
book series:
  - 
book genres:
  - Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Ray Bradbury
  - Fiction
  - 7/10 Books
  - Audiobook
---

Finished “The Playground”.
* Author: [Ray Bradbury](/book-authors/ray-bradbury)
* First Published: [1953](/book-publication-year/1950s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/23296980-the-playground)
>
> "The Playground" was part of the first hardcover edition of Ray Bradbury's legendary work Fahrenheit 451, published in 1953. In the story, Charles Underhill is a widower who will do anything to protect his young son Jim from the horrors of the playground - a playground which he and the boy pass by daily and the tumult of which, the activity, brings back to Charles the anguish of his own childhood. The playground, like childhood itself, is a nightmare of torment and vulnerability; Charles fears his sensitive son will be destroyed there just as he almost was so many years ago.
> Underhill's sister, Carol, who has moved in to help raise the young boy after his mother passed away, feels differently. The playground, she believes, is preparation for life, Jim will survive the experience she feels, and he will be the better for it and more equipped to deal with the rigor and obligation of adult existence.
> Underhill is caught between his own fear and his sister's invocation of reason and feels paralyzed. A mysterious boy calls out to him from the playground, and seems to know all too well why Underhill is there and what the source of his agony really is. A mysterious Manager also lurks and to whom the strange boy directs Underhill. An agreement can be made perhaps - this is what the boy tells Underhill. Perhaps Jim can be spared the playground, but of course, a substitute must be found.