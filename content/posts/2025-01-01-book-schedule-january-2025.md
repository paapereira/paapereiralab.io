---
title: "Books Schedule update"
slug: "book-schedule-january-2025"
author: "Paulo Pereira"
date: 2025-01-01T14:30:00+00:00
lastmod: 2025-01-01T14:30:00+00:00
description: "January Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my January TBR (To Be Read) update for the next months.

* **[Read]** Star Wars: Episode VI: Return of the Jedi by James Kahn
* **[Read]** The Subtle Art of Not Giving a F*ck by Mark Manson
* **[Read]** Way Station by Clifford D. Simak
* **[Read]** Mystery Walk by Robert R. McCammon
* **[Reading]** Aurora Rising (Prefect Dreyfus Emergency #1) by Alastair Reynolds
* The Sicilian (The Godfather #2) by Mario Puzo
* The Andromeda Evolution (Andromeda #2) by Daniel H. Wilson & Michael Crichton
* Planet of Exile (Hainish Cycle) by Ursula K. le Guin
* We Can Build You by Philip K. Dick
* A Coming of Age by Timothy Zahn
* The Expert System's Champion (The Expert System's Brother #2) by Adrian Tchaikovsky
* The Way of Kings (The Stormlight Archive #1) by Brandon Sanderson
* Dead Med (Prescription: Murder #1) by Freida McFadden
* The Bullet Journal Method by Ryder Carroll
* Harry Potter and the Prisoner of Azkaban (Harry Potter #3) by J. K. Rowling
* Turn Coat (The Dresden Files #11) by Jim Butcher
* Resurrection, Inc. by Kevin J. Anderson
* Revan (Star Wars: The Old Republic #1) by Drew Karpyshyn
* Usher's Passing by Robert R. McCammon
* Memorial do Convento by José Saramago

I also have always an audiobook going too:

* **[Reading]** Nexus by Yuval Noah Harari
* Breakfast at Tiffany's by Truman Capote
* Red Company: Invasion (Red Company #4) by B. V. Larson
* The Richest Man in Babylon by George S. Clason
* Impact Winter #1 by Travis Beacham
* A History of Video Games by Jeremy Parish
* The Sandman: Act III by Neil Gaiman
* Bag of Bones by Stephen King
