---
title: "The Eyes of the Dragon by Stephen King (1987)"
slug: "the-eyes-of-the-dragon"
author: "Paulo Pereira"
date: 2021-06-15T21:00:00+01:00
lastmod: 2021-06-15T21:00:00+01:00
cover: "/posts/books/the-eyes-of-the-dragon.jpg"
description: "Finished “The Eyes of the Dragon” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Fantasy
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Fantasy
  - 5/10 Books
  - Audiobook
---

Finished “The Eyes of the Dragon”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [February 2nd 1987](/book-publication-year/1980s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/29121348-the-eyes-of-the-dragon)
>
> A kingdom is in turmoil after old King Roland dies and his worthy successor, Prince Peter, is imprisoned by the evil Flagg and his pawn, young Prince Thomas. But Flagg's evil plot is not perfect, for he knows naught of Thomas's terrible secret - or Prince Peter's daring plan to escape to claim what is rightfully his. A tale of archetypal heroes and sweeping adventures, of dragons and princes and evil wizards, here is epic fantasy as only Stephen King could envision it. Stephen King has taken the classic fairy tale and transformed it into a masterpiece of fiction for the ages.