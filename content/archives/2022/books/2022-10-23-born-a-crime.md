---
title: "Born a Crime by Trevor Noah (2016)"
slug: "born-a-crime"
author: "Paulo Pereira"
date: 2022-10-23T17:00:00+01:00
lastmod: 2022-10-23T17:00:00+01:00
cover: "/posts/books/born-a-crime.jpg"
description: "Finished “Born a Crime” by Trevor Noah."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - Trevor Noah
book series:
  - 
book genres:
  - Nonfiction
  - Biography
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Trevor Noah
  - Nonfiction
  - Biography
  - 7/10 Books
  - Audiobook
---

Finished “Born a Crime: Stories from a South African Childhood”.
* Author: [Trevor Noah](/book-authors/trevor-noah)
* First Published: [November 15, 2016](/book-publication-year/2016)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31948742-born-a-crime)
>
> Trevor Noah is one of the comedy world’s brightest new voices, a light-footed but sharp-minded observer of the absurdities of politics, race, and identity, sharing jokes and insights drawn from the wealth of experience acquired in his relatively young life. As host of The Daily Show with Trevor Noah, he provides viewers in America and around the globe with their nightly dose of biting satire, but here Noah turns his focus inward, giving readers a deeply personal, heartfelt, and humorous look at the world that shaped him.
> 
> Noah was born a crime, the son of a white Swiss father and a black Xhosa mother, at a time when such a union was punishable by five years in prison. Living proof of his parents’ indiscretion, Trevor was kept mostly indoors for the first years of his life, bound by the extreme and often absurd measures his mother took to hide him from a government that could, at any moment, take him away. Finally liberated by the end of South Africa’s white rule, Trevor and his mother set forth on a grand adventure, living openly and freely and embracing the opportunities won by a centuries-long struggle.