---
title: "We Are Legion (We Are Bob) (Bobiverse Book 1) by Dennis E. Taylor (2016)"
slug: "we-are-legion-we-are-bob-bobiverse-book-1"
author: "Paulo Pereira"
date: 2019-05-12T19:56:00+01:00
lastmod: 2019-05-12T19:56:00+01:00
cover: "/posts/books/we-are-legion-we-are-bob-bobiverse-book-1.jpg"
description: "Finished “We Are Legion (We Are Bob) (Bobiverse Book 1)” by Dennis E. Taylor."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - Dennis E. Taylor
book series:
  - Bobiverse Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Dennis E. Taylor
  - Bobiverse Series
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Audiobook
---

Finished “We Are Legion (We Are Bob)”.
* Author: [Dennis E. Taylor](/book-authors/dennis-e.-taylor)
* First Published: [September 20th 2016](/book-publication-year/2016)
* Series: [Bobiverse](/book-series/bobiverse-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/32603222-we-are-legion-we-are-bob)
>
> Bob Johansson has just sold his software company and is looking forward to a life of leisure. There are places to go, books to read, and movies to watch. So it's a little unfair when he gets himself killed crossing the street.
>
> Bob wakes up a century later to find that corpsicles have been declared to be without rights, and he is now the property of the state. He has been uploaded into computer hardware and is slated to be the controlling AI in an interstellar probe looking for habitable planets. The stakes are high: no less than the first claim to entire worlds. If he declines the honor, he'll be switched off, and they'll try again with someone else. If he accepts, he becomes a prime target. There are at least three other countries trying to get their own probes launched first, and they play dirty.
>
> The safest place for Bob is in space, heading away from Earth at top speed. Or so he thinks. Because the universe is full of nasties, and trespassers make them mad – very mad.