---
title: "Upgrading to Hardy"
slug: "upgrading-to-hardy"
author: "Paulo Pereira"
date: 2008-04-25T00:56:00+01:00
lastmod: 2008-04-25T00:56:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
aliases:
  - /posts/upgrading-to-hardy/
---

And I'm set. I'm now officially running Ubuntu 8.04 LTS.

The recommended (and easier) way to [upgrade](http://www.ubuntu.com/getubuntu/upgrading) from Ubuntu 7.10 is to use Update Manager. But the servers are clotted (April 25th)...
I tried it two times and it just isn't possible at this moment (yes, I want Hardy).

So I downloaded the [alternate version of Ubuntu](http://www.ubuntu.com/getubuntu/download) (check bellow the "Start Download" button).
This version isn't a live cd, and it can be used to upgrade Ubuntu.

I installed (Applications > Add/Remove) [Gmount-iso](http://www.ubuntugeek.com/easy-way-of-mountunmount-iso-images-in-ubuntu.html). This way I could use the iso file without having to burn a cd.

To make sure cd image isn't corrupted:

- In the Terminal

```bash
md5sum ubuntu-8.04-alternate-i386.iso
```

- You should get the correct hash for the iso you're checking

```text
166991d61e7c79a452b604f0d25d07f9 *ubuntu-8.04-alternate-i386.iso
```

- Check the hash values [here](https://help.ubuntu.com/community/UbuntuHashes)

After an hour I got the alternate version of Ubuntu 8.04, mounted the iso file using Gmount-iso and, in the Terminal:

```bash
gksu "sh /cdrom/cdromupgrade"
```

And off we go. Upgrading to 8.04.

After some time the upgrade was complete and I rebooted the machine. Check bellow the aftermath:

- Several packages were marked to removal at the beginning of the upgrade. My Ubuntu 7.10 installation came from Ubuntu Ultimate Edition. I think that's one of the reasons. I gave it a little look and kept going. If something is missing I will install it later.
- The installer asked 3 or 4 times about replacing configuration files, like smb.conf. It allowed to see the differences between the current file and the new one, and I could choose to keep or replace the files. I backed up the ones I wanted and replaced them all.
- Restarted.
- After the restart the Update Manager wanted to do a partial upgrade. It seems the alternate cd didn't cover all packages. More packages where marked for removal.
- After the successful partial upgrade, I'm did another one.
- I then restarted again.
- I ran the Update Manager manually and check for updated and there aren't none. I was finished. It was a long run.

## Issues

- **Issue 1:** I was missing some software I wanted to get back. Just went do Applications > Add/Remove and that was it.

  - gVim (installed)
  - vlc player (installed)
  - VirtualBox kernel driver (to install)
  - [LAMP Server](/posts/apache-php-and-mysql/) (installed - check the link for details)

- **Issue 2:** All my preferences seems to be ok, except the mouse pointer speed (System > Preferences > Mouse) (solved)

- **Issue 3:** in Places > Network I see my Workgroup, but no computers (in this case a Windows machine). Still the Windows machine is accessing Linux shared folders (that I had to share again). (to solve)

  - If I use directly the IP I have no problems, but even in Places > Connect to Server I'm having no luck.
  - To pass this "problem" I created a Launcher (Location) in the Desktop to the machine (*smb://100.000.0.0/*).

- **Issue 4:** The version of Firefox that came with Ubuntu is the 3.0 beta 5. Unfortunately some of my favorite add-ons aren't supported (just yet), so I'm gonna have to install Firefox 2.0 (to do).

- **Issue 5:** The time server (ntp) I had defined was gone. I had it again. (solved)

```text
ntp02.oal.ul.pt
```

- **Issue 6:** I had some custom code in grub. I have to check it out tomorrow. (to check)

- **Issue 7:** Check if VirtualBox is running. (to check)