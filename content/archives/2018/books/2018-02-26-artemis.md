---
title: "Artemis by Andy Weir (2017)"
slug: "artemis"
author: "Paulo Pereira"
date: 2018-02-26T21:59:00+00:00
lastmod: 2018-02-26T21:59:00+00:00
cover: "/posts/books/artemis.jpg"
description: "Finished “Artemis” by Andy Weir."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Andy Weir
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Andy Weir
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/artemis
---

Finished “Artemis”.
* Author: [Andy Weir](/book-authors/andy-weir)
* First Published: [November 14th 2017](/book-publication-year/2017)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/37536257-artemis)
>
> Jazz Bashara is a criminal. Well, sort of. Life on Artemis, the first and only city on the moon, is tough if you're not a rich tourist or an eccentric billionaire. So smuggling in the occasional harmless bit of contraband barely counts, right? Not when you've got debts to pay and your job as a porter barely covers the rent.
>
> Everything changes when Jazz sees the chance to commit the perfect crime, with a reward too lucrative to turn down. But pulling off the impossible is just the start of her problems, as she learns that she's stepped square into a conspiracy for control of Artemis itself - and that now her only chance at survival lies in a gambit even riskier than the first.