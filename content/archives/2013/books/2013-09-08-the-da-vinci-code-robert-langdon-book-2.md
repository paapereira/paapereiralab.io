---
title: "The Da Vinci Code (Robert Langdon Book 2) by Dan Brown (2003)"
slug: "the-da-vinci-code-robert-langdon-book-2"
author: "Paulo Pereira"
date: 2013-09-08T23:58:00+01:00
lastmod: 2013-09-08T23:58:00+01:00
cover: "/posts/books/the-da-vinci-code-robert-langdon-book-2.jpg"
description: "Finished “The Da Vinci Code (Robert Langdon Book 2)” by Dan Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2003
book authors:
  - Dan Brown
book series:
  - Robert Langdon Series
book genres:
  - Fiction
  - Thriller
  - Mystery
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Dan Brown
  - Robert Langdon Series
  - Fiction
  - Thriller
  - Mystery
  - 9/10 Books
  - Audiobook
---

Finished “The Da Vinci Code”.
* Author: [Dan Brown](/book-authors/dan-brown)
* First Published: [March 18th 2003](/book-publication-year/2003)
* Series: [Robert Langdon](/book-series/robert-langdon-series) Book 2
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17213413-the-da-vinci-code)
>
> While in Paris on business, Harvard symbologist Robert Langdon receives an urgent late-night phone call: the elderly curator of the Louvre has been murdered inside the museum. Near the body, police have found a baffling cipher. While working to solve the enigmatic riddle, Langdon is stunned to discover it leads to a trail of clues hidden in the works of Da Vinci, clues visible for all to see, yet ingeniously disguised by the painter.
>
> Langdon joins forces with a gifted French cryptologist, Sophie Neveu, and learns the late curator was involved in the Priory of Sion, an actual secret society whose members included Sir Isaac Newton, Botticelli, Victor Hugo, and Da Vinci, among others.
>
> In a breathless race through Paris, London, and beyond, Langdon and Neveu match wits with a faceless powerbroker who seems to anticipate their every move. Unless Langdon and Neveu can decipher the labyrinthine puzzle in time, the Priory's ancient secret, and an explosive historical truth, will be lost forever.