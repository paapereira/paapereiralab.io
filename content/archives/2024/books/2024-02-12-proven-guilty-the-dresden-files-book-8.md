---
title: "Proven Guilty (The Dresden Files Book 8) by Jim Butcher (2006)"
slug: "proven-guilty-the-dresden-files-book-8"
author: "Paulo Pereira"
date: 2024-02-12T18:00:00+00:00
lastmod: 2024-02-12T18:00:00+00:00
cover: "/posts/books/proven-guilty-the-dresden-files-book-8.jpg"
description: "Finished “Proven Guilty (The Dresden Files Book 8)” by Jim Butcher."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2006
book authors:
  - Jim Butcher
book series:
  - The Dresden Files Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jim Butcher
  - The Dresden Files Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Proven Guilty”.
* Author: [Jim Butcher](/book-authors/jim-butcher)
* First Published: [February 1, 2006](/book-publication-year/2006)
* Series: [The Dresden Files](/book-series/the-dresden-files-series) Book 8
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/5986832-proven-guilty)
>
> Harry Dresden is the only wizard in the Chicago phone book. He consults from the police department on those so-called "unusual" cases. He's even been on talk shows. So there's no love lost between Harry and the White Council of Wizards, who find him a little brash and undisciplined. But now war with the vampires has thinned the ranks of wizards, and the White Council needs Harry, like it or not. He's drafted as a Warden, and assigned to look into rumors of black magic in the Windy City.
> 
> And if that wasn't enough, another problem arrives in the form of the tattooed and pierced daughter of an old friend, all grown-up and already in trouble. Her boyfriend was the only one in a room where an old man was attacked, but in spite of this, he insists he didn't do it. What looks like a supernatural assault straight out of a horror film turns out to be...well, something quite close to that, as Harry discovers that malevolent entities that feed on fear are loose in Chicago. All in a day's work for a wizard, his faithful dog, and a talking skull named Bob...