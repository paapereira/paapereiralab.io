---
title: "Victorian Secrets by John Woolf and Nick Baker (2018)"
slug: "victorian-secrets"
author: "Paulo Pereira"
date: 2019-10-09T22:16:00+01:00
lastmod: 2019-10-09T22:16:00+01:00
cover: "/posts/books/victorian-secrets.jpg"
description: "Finished “Stephen Fry's Victorian Secrets” by John Woolf and Nick Baker."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - John Woolf
  - Nick Baker
book series:
  - 
book genres:
  - Nonfiction
  - History
book scores:
  - 4/10 Books
tags:
  - Book Log
  - John Woolf
  - Nick Baker
  - Nonfiction
  - History
  - 4/10 Books
  - Audiobook
---

Finished “Stephen Fry's Victorian Secrets”.
* Author: [John Woolf](/book-authors/john-woolf) and [Nick Baker](/book-authors/nick-baker)
* First Published: [2018](/book-publication-year/2018)
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53105626-stephen-fry-s-victorian-secrets)
>
> Step right up, step right up and don’t be shy—welcome to Victorian Secrets. Over 12 fascinating episodes, Stephen Fry explores the weird and worrying ways of Victorian Britain through true accounts delving deep into a period of time we think we know, to discover an altogether darker reality.