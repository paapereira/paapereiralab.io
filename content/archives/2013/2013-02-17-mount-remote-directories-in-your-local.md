---
title: "Mount remote directories in your local machine"
slug: "mount-remote-directories-in-your-local"
author: "Paulo Pereira"
date: 2013-02-17T16:57:00+00:00
lastmod: 2013-02-17T16:57:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
  - GitHub
  - sshfsmount
aliases:
  - /posts/mount-remote-directories-in-your-local/
---

I have my Music and Video directories in my B3 Server. To access them locally I mount them using [sshfs](http://fuse.sourceforge.net/sshfs.html).

In order to not think about it, I created a script that runs at boot time to take care of that for me.

The script is available at my [github](https://github.com/paapereira) account in a repository called [sshfsmount](https://github.com/paapereira/sshfsmount).

![github](/posts/2013/2013-02-17-mount-remote-directories-in-your-local/github.png)

## Installing sshfs

First of all you need to install sshfs:

```bash
sudo apt-get install sshfs
```

## Get the code

Get the code from my [sshfsmount](https://github.com/paapereira/sshfsmount) github repository:

```bash
cd ~/Downloads
wget -O sshfsmount.zip https://github.com/paapereira/sshfsmount/archive/master.zip
unzip sshfsmount.zip
chmod +x sshfsmount-master/sshfsmount.sh
```

## Configure the mountpoints

Change the mountpoints configuration file to add your directories:

```bash
vi sshfsmount-master/config/sshfstab
```

## Running

To mount all the directories in you configuration file:

```bash
sshfsmount-master/sshfsmount.sh mount
```

To re-mount all the directories in you configuration file:

```bash
sshfsmount-master/sshfsmount.sh re-mount
```

## Running at startup

Go to the Startup Applications and add a new Startup Program:

![Mount your remote directories at startup](/posts/2013/2013-02-17-mount-remote-directories-in-your-local/mount.png)