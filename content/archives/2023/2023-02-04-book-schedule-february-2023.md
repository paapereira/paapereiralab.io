---
title: "Books Schedule update"
slug: "book-schedule-february-2023"
author: "Paulo Pereira"
date: 2023-02-04T13:00:00+00:00
lastmod: 2023-02-04T13:00:00+00:00
description: "February Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my February TBR (To Be Read) update for the next months.

* **[Read]** Redemption Ark (Revelation Space 2) by Alastair Reynolds
* **[Read]** The Testaments (The Handmaid's Tale 2) by Margaret Atwood
* **[Reading]** Boy's Life by Robert R. McCammon
* Bear Head (Dogs of War 2) by Adrian Tchaikovsky
* Death Masks (The Dresden Files 5) by Jim Butcher
* Prey by Michael Crichton
* Absolution Gap (Revelation Space 3) by Alastair Reynolds
* Life of Pi by Yann Martel
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson

I also have always an audiobook going too:

* **[Read]** Skeleton Crew by Stephen King
* **[Reading]** The Signal and the Noise by Nate Silver
* Hell's Reach (Galactic Liberation 6) by David Vandyke & B. V. Larson
* The Secret Life of Walter Mitty by James Thurber
* Gerald's Game by Stephen King
* Until the End of Time by Brian Greene