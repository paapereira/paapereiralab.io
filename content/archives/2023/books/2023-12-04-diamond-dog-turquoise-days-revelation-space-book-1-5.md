---
title: "Diamond Dogs, Turquoise Days (Revelation Space Book 1.5) by Alastair Reynolds (2003)"
slug: "diamond-dog-turquoise-days-revelation-space-book-1-5"
author: "Paulo Pereira"
date: 2023-12-04T21:00:00+00:00
lastmod: 2023-12-04T21:00:00+00:00
cover: "/posts/books/diamond-dog-turquoise-days-revelation-space-book-1-5.jpg"
description: "Finished “Diamond Dogs, Turquoise Days (Revelation Space Book 1.5)” by Alastair Reynolds."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2003
book authors:
  - Alastair Reynolds
book series:
  - Revelation Space Series
  - Revelation Space Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Alastair Reynolds
  - Revelation Space Series
  - Revelation Space Universe
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “Diamond Dogs, Turquoise Days”.
* Author: [Alastair Reynolds](/book-authors/alastair-reynolds)
* First Published: [February 1, 2003](/book-publication-year/2003)
* Series: [Revelation Space](/book-series/revelation-space-series) Book 1.5
* [Revelation Space Universe](/book-series/revelation-space-universe)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/49093654-diamond-dogs-turquoise-days)
>
> "Diamond Dogs"The planet Golgotha -- supposedly lifeless -- resides in a remote star system, far from those inhabited by human colonists. It is home to an enigmatic machine-like structure called the Blood Spire, which has already brutally and systematically claimed the lives of one starship crew that attempted to uncover its secrets. But nothing will deter Richard Swift from exploring this object of alien origin...
> 
> "Turquoise Days"In the seas of Turquoise live the Pattern Jugglers, the amorphous, aquatic organisms capable of preserving the memories of any human swimmer who joins their collective consciousness. Naqi Okpik devoted her life to studying these creatures -- and paid a high price for swimming among them. Now, she may be the only hope for the survival of the species -- and of every person living on Turquoise...