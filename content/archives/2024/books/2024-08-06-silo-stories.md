---
title: "Silo Stories by Hugh Howey"
slug: "silo-stories"
author: "Paulo Pereira"
date: 2024-08-06T21:00:00+01:00
lastmod: 2024-08-06T21:00:00+01:00
cover: "/posts/books/silo-stories.jpg"
description: "Finished “Silo Stories” by Hugh Howey."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 
book authors:
  - Hugh Howey
book series:
  - Silo Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Hugh Howey
  - Silo Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “Silo Stories”.
* Author: [Hugh Howey](/book-authors/hugh-howey)
* Series: [Silo](/book-series/silo-series)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/58802560-silo-stories)
