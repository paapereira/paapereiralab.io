---
title: "The Power of Self-Compassion by Laurie J. Cameron (2020)"
slug: "the-power-of-self-compassion"
author: "Paulo Pereira"
date: 2020-12-10T22:10:00+00:00
lastmod: 2020-12-10T22:10:00+00:00
cover: "/posts/books/the-power-of-self-compassion.jpg"
description: "Finished “The Power of Self-Compassion” by Laurie J. Cameron."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Laurie J. Cameron
book series:
  - 
book genres:
  - Nonfiction
  - Self Help
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Laurie J. Cameron
  - Nonfiction
  - Self Help
  - 5/10 Books
  - Audiobook
---

Finished “The Power of Self-Compassion”.
* Author: [Laurie J. Cameron](/book-authors/laurie-j.-cameron)
* First Published: [January 2nd 2020](/book-publication-year/2020)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/50031244-the-power-of-self-compassion)
>
> There’s no denying that life can be difficult. Simply being human means experiencing emotional and physical pain. None of us escapes dealing with pain, failure, and setbacks. But learning how to practice mindful self-compassion can be life-changing. Self-compassion comes from the understanding that every human being suffers, that we all want to be happy, and that this commonality connects us with everyone else.
>
> Over the last decade, there has been an international explosion of research on mindfulness and self-compassion. Contemplative practices are being integrated with science and psychology. Researchers are sharing information on the power of inner-directed compassion and its beneficial effects on mental well-being, growth, motivation, relationships, and physical health.
>
> Join expert Laurie Cameron to discover tools—including meditations, exercises, journaling, and in-the-moment practices—that will help you evoke mindfulness and self-compassion in your everyday life, in a way that it becomes your natural response—your new set of habits. As you adopt these practices, you’ll start to see a shift in how you work with stressful life events, as well as how you connect with the shared human experience of loss, challenge, disappointment, failure, and setbacks.