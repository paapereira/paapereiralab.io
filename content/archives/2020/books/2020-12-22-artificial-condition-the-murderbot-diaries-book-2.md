---
title: "Artificial Condition (The Murderbot Diaries Book 2) by Martha Wells (2018)"
slug: "artificial-condition-the-murderbot-diaries-book-2"
author: "Paulo Pereira"
date: 2020-12-22T22:22:00+00:00
lastmod: 2020-12-22T22:22:00+00:00
cover: "/posts/books/artificial-condition-the-murderbot-diaries-book-2.jpg"
description: "Finished “Artificial Condition (The Murderbot Diaries Book 2)” by Martha Wells."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Martha Wells
book series:
  - The Murderbot Diaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Martha Wells
  - The Murderbot Diaries Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Artificial Condition”.
* Author: [Martha Wells](/book-authors/martha-wells)
* First Published: [May 8th 2018](/book-publication-year/2018)
* Series: [The Murderbot Diaries](/book-series/the-murderbot-diaries-series) Book 2
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/39908325-artificial-condition)
>
> It has a dark past – one in which a number of humans were killed. A past that caused it to christen itself “Murderbot”. But it has only vague memories of the massacre that spawned that title, and it wants to know more.
>
> Teaming up with a Research Transport vessel named ART (you don’t want to know what the “A” stands for), Murderbot heads to the mining facility where it went rogue.
>
> What it discovers will forever change the way it thinks…