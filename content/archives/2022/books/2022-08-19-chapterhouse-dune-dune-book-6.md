---
title: "Chapterhouse: Dune (Dune Book 6) by Frank Herbert (1985)"
slug: "chapterhouse-dune-dune-book-6"
author: "Paulo Pereira"
date: 2022-08-19T21:00:00+01:00
lastmod: 2022-08-19T21:00:00+01:00
cover: "/posts/books/chapterhouse-dune-dune-book-6.jpg"
description: "Finished Chapterhouse: Dune (Dune Book 6)” by Frank Herbert."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Frank Herbert
book series:
  - Dune Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Frank Herbert
  - Dune Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “Chapterhouse: Dune”.
* Author: [Frank Herbert](/book-authors/frank-herbert)
* First Published: [April 1, 1985](/book-publication-year/1980s)
* Series: [Dune](/book-series/dune-series) Book 6
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/55001208-chapterhouse)
>
> The desert planet Arrakis, called Dune, has been destroyed. The remnants of the Old Empire have been consumed by the violent matriarchal cult known as the Honored Matres. Only one faction remains a viable threat to their total conquest—the Bene Gesserit, heirs to Dune’s power.
> 
> Under the leadership of Mother Superior Darwi Odrade, the Bene Gesserit have colonized a green world on the planet Chapterhouse and are turning it into a desert, mile by scorched mile. And once they’ve mastered breeding sandworms, the Sisterhood will control the production of the greatest commodity in the known galaxy—the spice melange. But their true weapon remains a man who has lived countless lifetimes—a man who served under the God Emperor Paul Muad’Dib....