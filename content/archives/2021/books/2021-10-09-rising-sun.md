---
title: "Rising Sun by Michael Crichton (1992)"
slug: "rising-sun"
author: "Paulo Pereira"
date: 2021-10-09T21:00:00+01:00
lastmod: 2021-10-09T21:00:00+01:00
cover: "/posts/books/rising-sun.jpg"
description: "Finished “Rising Sun” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Michael Crichton
book series:
  - 
book genres:
  - Fiction
  - Thriller
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Fiction
  - Thriller
  - 7/10 Books
  - Audiobook
---

Finished “Rising Sun”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [January 27th 1992](/book-publication-year/1990s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34534728-rising-sun)
>
> A riveting thriller of corporate intrigue and cutthroat competition between American and Japanese business interests.
> 
> On the forty-fifth floor of the Nakamoto tower in downtown Los Angeles - the new American headquarters of the immense Japanese conglomerate - a grand opening celebration is in full swing.
> 
> On the forty-sixth floor, in an empty conference room, the corpse of a beautiful young woman is discovered.
> 
> The investigation immediately becomes a headlong chase through a twisting maze of industrial intrigue, a no-holds-barred conflict in which control of a vital American technology is the fiercely coveted prize - and in which the Japanese saying "business is war" takes on a terrifying reality.