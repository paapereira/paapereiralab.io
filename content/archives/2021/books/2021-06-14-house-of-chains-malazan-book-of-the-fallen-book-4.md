---
title: "House of Chains (Malazan Book of the Fallen Book 4) by Steven Erikson (2002)"
slug: "house-of-chains-malazan-book-of-the-fallen-book-4"
author: "Paulo Pereira"
date: 2021-06-14T21:00:00+01:00
lastmod: 2021-06-14T21:00:00+01:00
cover: "/posts/books/house-of-chains-malazan-book-of-the-fallen-book-4.jpg"
description: "Finished “House of Chains (Malazan Book of the Fallen Book 4)” by Steven Erikson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2002
book authors:
  - Steven Erikson
book series:
  - Malazan Book of the Fallen Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Steven Erikson
  - Malazan Book of the Fallen Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
---

Finished “House of Chains”.
* Author: [Steven Erikson](/book-authors/steven-erikson)
* First Published: [December 2002](/book-publication-year/2002)
* Series: [Malazan Book of the Fallen](/book-series/malazan-book-of-the-fallen-series) Book 4
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8177008-house-of-chains)
>
> In Northern Genabackis, a raiding party of savage tribal warriors descends from the mountains into the southern flatlands. Their intention is to wreak havoc amongst the despised lowlanders, but for the one named Karsa Orlong it marks the beginning of what will prove to be an extraordinary destiny.
>
> Some years later, it is the aftermath of the Chain of Dogs. Tavore, the Adjunct to the Empress, has arrived in the last remaining Malazan stronghold of Seven Cities. New to command, she must hone twelve thousand soldiers, mostly raw recruits but for a handful of veterans of Coltaine's legendary march, into a force capable of challenging the massed hordes of Sha'ik's Whirlwind who lie in wait in the heart of the Holy Desert.
>
> But waiting is never easy. The seer's warlords are locked into a power struggle that threatens the very soul of the rebellion, while Sha'ik herself suffers, haunted by the knowledge of her nemesis: her own sister, Tavore.
>
> And so begins this awesome new chapter in Steven Erikson's acclaimed Malazan Book of the Fallen . . . 