---
title: "Small Favor (The Dresden Files Book 10) by Jim Butcher (2008)"
slug: "small-favor-the-dresden-files-book-10"
author: "Paulo Pereira"
date: 2024-11-28T22:00:00+00:00
lastmod: 2024-11-28T22:00:00+00:00
cover: "/posts/books/small-favor-the-dresden-files-book-10.jpg"
description: "Finished “Small Favor (The Dresden Files Book 10)” by Jim Butcher."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2008
book authors:
  - Jim Butcher
book series:
  - The Dresden Files Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jim Butcher
  - The Dresden Files Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Small Favor”.
* Author: [Jim Butcher](/book-authors/jim-butcher)
* First Published: [April 1, 2008](/book-publication-year/2008)
* Series: [The Dresden Files](/book-series/the-dresden-files-series) Book 10
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8123311-small-favor)
>
> Harry’s life finally seems to be calming down. The White Council’s war with the vampiric Red Court is easing up, no one’s tried to kill him lately, and his eager apprentice is starting to learn real magic. For once, the future looks fairly bright. But the past casts one hell of a long shadow.
> 
> Mab, monarch of the Sidhe Winter Court, calls in an old favor from Harry. Just one small favor he can’t refuse...one that will trap Harry Dresden between a nightmarish foe and an equally deadly ally, and one that will strain his skills—and loyalties—to their very limits.
> 
> And everything was going so well for once...