---
title: "The Dark Half by Stephen King (1989)"
slug: "the-dark-half"
author: "Paulo Pereira"
date: 2022-07-10T17:35:00+01:00
lastmod: 2022-07-10T17:35:00+01:00
cover: "/posts/books/the-dark-half.jpg"
description: "Finished “The Dark Half” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - 8/10 Books
  - Audiobook
---

Finished “The Dark Half”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [October 20th 1989](/book-publication-year/1980s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53333618-the-dark-half)
>
> Thad Beaumont would like to say he is innocent. He'd like to say he has nothing to do with the series of monstrous murders that keep coming closer to his home. But how can Thad disown the ultimate embodiment of evil that goes by the name he gave it-and signs its crimes with Thad's bloody fingerprints?