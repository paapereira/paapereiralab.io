---
title: "The Gentle Art of Cracking Heads (The Human Division Book 12) by John Scalzi (2013)"
slug: "the-gentle-art-of-cracking-heads-the-human-division-book-12"
author: "Paulo Pereira"
date: 2019-06-01T22:51:00+01:00
lastmod: 2019-06-01T22:51:00+01:00
cover: "/posts/books/the-gentle-art-of-cracking-heads-the-human-division-book-12.png"
description: "Finished “The Gentle Art of Cracking Heads (The Human Division Book 12)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Gentle Art of Cracking Heads”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [April 2nd 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 12
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17727159-the-gentle-art-of-cracking-heads)
>
> United States Diplomat Danielle Lowen was there when one of her fellow diplomats committed an unthinkable act that had consequences for the entire planet. Now she’s trying to figure out how it happened before it can happen again. Putting the puzzle pieces together could solve the mystery - or it could threaten her own life.