---
title: "Fire Fight (Star Runner Book 2) by B.V. Larson (2021)"
slug: "fire-fight-star-runner-book-2"
author: "Paulo Pereira"
date: 2023-07-09T17:00:00+01:00
lastmod: 2023-07-09T17:00:00+01:00
cover: "/posts/books/fire-fight-star-runner-book-2.jpg"
description: "Finished “Fire Fight (Star Runner Book 2)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - B.V. Larson
book series:
  - Star Runner Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Runner Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished Fire Fight”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [April 27, 2021](/book-publication-year/2021)
* Series: [Star Runner](/book-series/star-runner-series) Book 2
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/58466745-fire-fight)
>
> The colonies of the Faustian Chain have gone dark. No star freighters have come from the star cluster for decades.
> 
> Captain Bill Gorman, a smuggler on the Fringe, is one of the few who know the truth. He’s personally battled the alien invaders who’ve overrun the human colonies of the Chain. He and his crew are faced with a choice: will they fight the invaders, or will they run?