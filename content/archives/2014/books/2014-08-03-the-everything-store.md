---
title: "The Everything Store by Brad Stone (2013)"
slug: "the-everything-store"
author: "Paulo Pereira"
date: 2014-08-03T22:41:00+01:00
lastmod: 2014-08-03T22:41:00+01:00
cover: "/posts/books/the-everything-store.jpg"
description: "Finished “The Everything Store: Jeff Bezos and the Age of Amazon” by Brad Stone."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - Brad Stone
book series:
  - 
book genres:
  - Nonfiction
  - Biography
  - Business
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Brad Stone
  - Nonfiction
  - Biography
  - Business
  - 7/10 Books
  - Audiobook
---

Finished “The Everything Store: Jeff Bezos and the Age of Amazon”.
* Author: [Brad Stone](/book-authors/brad-stone)
* First Published: [2013](/book-publication-year/2013)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/21825796-the-everything-store)
>
> The definitive story of Amazon.com, one of the most successful companies in the world, and of its driven, brilliant founder, Jeff Bezos. Amazon.com started off delivering books through the mail. But its visionary founder, Jeff Bezos, wasn't content with being a bookseller. He wanted Amazon to become the everything store, offering limitless selection and seductive convenience at disruptively low prices. To do so, he developed a corporate culture of relentless ambition and secrecy that's never been cracked. Until now. Brad Stone enjoyed unprecedented access to current and former Amazon employees and Bezos family members, giving readers the first in-depth, fly-on-the-wall account of life at Amazon. Compared to tech's other elite innovators--Jobs, Gates, Zuckerberg--Bezos is a private man. But he stands out for his restless pursuit of new markets, leading Amazon into risky new ventures like the Kindle and cloud computing, and transforming retail in the same way Henry Ford revolutionized manufacturing. THE EVERYTHING STORE will be the revealing, definitive biography of the company that placed one of the first and largest bets on the Internet and forever changed the way we shop and read.