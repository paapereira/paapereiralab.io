---
title: "Install Spotify in Ubuntu"
slug: "install-spotify-in-ubuntu"
author: "Paulo Pereira"
date: 2013-02-12T16:49:00+00:00
lastmod: 2013-02-12T16:49:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 12.10
  - Quantal Quetzal
  - Spotify
---

[Spotify](http://www.spotify.com/pt/) is finally available in Portugal!

![Spotify](/posts/2013/2013-02-12-install-spotify-in-ubuntu/spotify.jpg)

Here's how you install the preview build of Spotify for Linux in Ubuntu.

## Installation

- Add the following into your Software Sources:
```bash
deb http://repository.spotify.com stable non-free
```
![Software Sources](/posts/2013/2013-02-12-install-spotify-in-ubuntu/software-sources.png)
- Add Spotify public key into your trusted software provider keys;
```bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 94558F59
```
- Update apt-get
```bash
sudo apt-get update
```
- Install Spotify
```bash
sudo apt-get install spotify-client
```
![spotify-client](/posts/2013/2013-02-12-install-spotify-in-ubuntu/client.png)

Enjoy!