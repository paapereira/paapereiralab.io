---
title: "Light Falls by Brian Greene (2016)"
slug: "light-falls"
author: "Paulo Pereira"
date: 2018-12-05T22:05:00+00:00
lastmod: 2018-12-05T22:05:00+00:00
cover: "/posts/books/light-falls.jpg"
description: "Finished “Light Falls: Space, Time, and an Obsession of Einstein” by Brian Greene."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - Brian Greene
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Brian Greene
  - Nonfiction
  - Science
  - 7/10 Books
  - Audiobook
---

Finished “Light Falls: Space, Time, and an Obsession of Einstein”.
* Author: [Brian Greene](/book-authors/brian-greene)
* First Published: [October 25th 2016](/book-publication-year/2016)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/33552647-light-falls)
>
> Best-selling author, superstar physicist, and cofounder of the World Science Festival Brian Greene (The Elegant Universe, The Fabric of the Cosmos) and an ensemble cast led by award-winning actor Paul Rudd (Ant-Man) perform this dramatic story tracing Albert Einstein's discovery of the general theory of relativity. Featuring an original score by composer Jeff Beal (House of Cards, Pollock), Einstein’s electrifying journey toward his greatest achievement is brought vividly to life.
> The theatrical version of Light Falls was first performed at the World Science Festival in New York City.