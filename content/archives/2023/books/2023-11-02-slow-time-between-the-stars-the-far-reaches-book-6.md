---
title: "Slow Time Between the Stars (The Far Reaches Book 6) by John Scalzi (2023)"
slug: "slow-time-between-the-stars-the-far-reaches-book-6"
author: "Paulo Pereira"
date: 2023-11-02T21:00:00+00:00
lastmod: 2023-11-02T21:00:00+00:00
cover: "/posts/books/slow-time-between-the-stars-the-far-reaches-book-6.jpg"
description: "Finished “Slow Time Between the Stars (The Far Reaches Book 6)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - John Scalzi
book series:
  - The Far Reaches Series
book genres:
  - Fiction
  - Science Fiction
  - Short Stories
book scores:
  - 7/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Far Reaches Series
  - Fiction
  - Science Fiction
  - Short Stories
  - 7/10 Books
  - Ebook
---

Finished “Slow Time Between the Stars”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [June 27, 2023](/book-publication-year/2023)
* Series: [The Far Reaches](/book-series/the-far-reaches-series) Book 6
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/151908304-slow-time-between-the-stars)
>
> An artificial intelligence on a star-spanning mission explores the farthest horizons of human potential—and its own purpose—in a mind-bending short story by New York Times bestselling author John Scalzi. Equipped with the entirety of human knowledge, a sentient ship is launched on a last-ditch journey to find a new home for civilization. Trillions of miles. Tens of thousands of years. In the space between, the AI has plenty of time to think about life, the vastness of the universe, everything it was meant to do, and—with a perspective created but not limited by humans—what it should do.