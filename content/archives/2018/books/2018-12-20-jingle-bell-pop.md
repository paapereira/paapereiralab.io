---
title: "Jingle Bell Pop by John Seabrook (2018)"
slug: "jingle-bell-pop"
author: "Paulo Pereira"
date: 2018-12-20T20:21:00+00:00
lastmod: 2018-12-20T20:21:00+00:00
cover: "/posts/books/jingle-bell-pop.jpg"
description: "Finished “Jingle Bell Pop: How to Make A Holiday Hit” by John Seabrook."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - John Seabrook
book series:
  - 
book genres:
  - Nonfiction
book scores:
  - 3/10 Books
tags:
  - Book Log
  - John Seabrook
  - Nonfiction
  - 3/10 Books
  - Audiobook
---

Finished “Jingle Bell Pop: How to Make A Holiday Hit”.
* Author: [John Seabrook](/book-authors/john-seabrook)
* First Published: [December 6th 2018](/book-publication-year/2018)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43169851-jingle-bell-pop)
>
> It seems like every year, the holiday season arrives earlier and earlier. Before Halloween’s ghosts and ghouls have even had their chance to come out of the shadows, sleigh bells and ribbons begin to materialize at the mall and towering tinsel-tinged trees appear in our living rooms. But the most telltale sign of the arrival of yuletide festivities is the unceasingly merry melody of the seasonal songbook, from "Silent Night" to "Santa Baby." Love them or loathe them, these holiday earworms are here to stay. But how do these songs endure for decades? And why are there so few contemporary Christmas carols?In this holly jolly Audible Original, New York Times bestselling author and New Yorker columnist John Seabrook uncovers the mysteries of the holiday music machine, exploring how these hits were made and why they’ve dominated the soundwaves each and every winter. From the mid-century reign of songwriter Johnny Marks ("Rudolph the Red-Nosed Reindeer") to the manufactured musical nostalgia of modern holiday hits like "All I Want for Christmas Is You," Seabrook, alongside a cast of singers, songwriters, and producers, reveals the untold stories behind the songs that have us rockin’ around the Christmas tree year after year.