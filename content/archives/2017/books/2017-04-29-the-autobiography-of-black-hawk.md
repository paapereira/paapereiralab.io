---
title: "The Autobiography of Black Hawk by Black Hawk (1833)"
slug: "the-autobiography-of-black-hawk"
author: "Paulo Pereira"
date: 2017-04-29T00:51:00+01:00
lastmod: 2017-04-29T00:51:00+01:00
cover: "/posts/books/the-autobiography-of-black-hawk.jpg"
description: "Finished “The Autobiography of Black Hawk” by Black Hawk."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - Black Hawk
book series:
  - 
book genres:
  - Nonfiction
  - History
book scores:
  - 3/10 Books
tags:
  - Book Log
  - Black Hawk
  - Nonfiction
  - History
  - 3/10 Books
  - Audiobook
---

Finished “The Autobiography of Black Hawk”.
* Author: [Black Hawk](/book-authors/black-hawk)
* First Published: [1833](/book-publication-year/1800s)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/16176251-the-autobiography-of-black-hawk-unabridged)
>
> This story is told in the words of a tragic figure in American history: a hook-nosed, hollow-cheeked old Sauk warrior who lived under four flags while the Mississippi Valley was being wrested from his people.
>
> The author is Black Hawk himself - once pursued by an army whose members included Captain Abraham Lincoln and Lieutenant Jefferson Davis. Perhaps no Indian ever saw so much of American expansion or fought harder to prevent that expansion from driving his people to exile and death. He knew Zebulon Pike, William Clark, Henry Schoolcraft, George Catlin, Winfield Scott, and such figures in American government as President Andrew Jackson and Secretary of State Lewis Cass. He knew Chicago when it was a cluster of log houses around a fort, and he was in St. Louis the day the American flag went up and the French flag came down. He saw crowds gather to cheer him in Philadelphia, Baltimore, and New York - and to stone the driver of his carriage in Albany - during a fantastic tour sponsored by the government. And at last he dies in 1838, bitter in the knowledge that he had led men, women, and children of his tribe to slaughter on the banks of the Mississippi.
>
> After his capture at the end of the Black Hawk War, he was imprisoned for a time and then released to live in the territory that is now Iowa. He dictated his autobiography to a government interpreter, Antoine LeClaire, and the story was put into written form by J. B. Patterson, a young Illinois newspaperman. Since its first appearance in 1833, the autobiography has become known as an American classic.