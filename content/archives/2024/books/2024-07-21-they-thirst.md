---
title: "They Thirst by Robert R. McCammon (1981)"
slug: "they-thirst"
author: "Paulo Pereira"
date: 2024-07-21T16:00:00+01:00
lastmod: 2024-07-21T16:00:00+01:00
cover: "/posts/books/they-thirst.jpg"
description: "Finished “They Thirst” by Robert R. McCammon."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Robert R. McCammon
book series:
  - 
book genres:
  - Fiction
  - Horror
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Robert R. McCammon
  - Fiction
  - Horror
  - 8/10 Books
  - Ebook
---

Finished “They Thirst”.
* Author: [Robert R. McCammon](/book-authors/robert-r.-mccammon)
* First Published: [January 1, 1981](/book-publication-year/1980s)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/15847330-they-thirst)
>
> The Kronsteen castle, a gothic monstrosity, looms over Los Angeles. Built during Hollywood’s golden age for a long-dead screen idol with a taste for the macabre, it stands as a decaying reminder of the past. Since the owner’s murder, no living thing has ever again taken up residence. But it isn’t abandoned. Prince Conrad Vulkan, Hungarian master of the vampires, as old as the centuries, calls it home. His plan is to replace all humankind with his kind. And he’s starting with the psychotic dregs of society in the City of Angels.
> 
> The number of victims is growing night after night, and so is Vulkan’s legion of the dead. As a glittering city bleeds into a necropolis, a band of vampire hunters takes action: an avenging young boy who saw his parents devoured; a television star whose lover has an affinity for the supernatural; a dying priest chosen by God to defend the world; a female reporter investigating a rash of cemetery desecrations; and LAPD homicide detective Andy Palatazin, an immigrant who survived a vampire attack in his native Hungary when he was child and has been hunting evil across the globe for decades.
> 
> Palatazin knows that to stop the Prince of Darkness, one must invade his nest. He knows it’s also a suicide mission. But it’s the only way to save the city—and the world—from vampire domination.