---
title: "New Blog Template"
slug: "new-blog-template"
author: "Paulo Pereira"
date: 2009-05-13T21:52:00+01:00
lastmod: 2009-05-13T21:52:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
---

I'm renewing the template on the blog. I'm still working on it, still... what do you think so far?

Before:

![before](/posts/2009/2009-05-13-new-blog-template/template-before.png)

After:

![after](/posts/2009/2009-05-13-new-blog-template/template-after.png)