---
title: "More Bedtime Stories for Cynics by Dave Hill (2019)"
slug: "more-bedtime-stories-for-cynics-bedtime-stories-for-cynics-book-2"
author: "Paulo Pereira"
date: 2020-10-26T22:31:00+00:00
lastmod: 2020-10-26T22:31:00+00:00
cover: "/posts/books/more-bedtime-stories-for-cynics-bedtime-stories-for-cynics-book-2.jpg"
description: "Finished “More Bedtime Stories for Cynics” by Dave Hill."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Dave Hill
book series:
  - Bedtime Stories for Cynics Series
book genres:
  - Fiction
  - Humor
book scores:
  - 4/10 Books
tags:
  - Book Log
  - Dave Hill
  - Bedtime Stories for Cynics Series
  - Fiction
  - Humor
  - 4/10 Books
  - Audiobook
---

Finished “More Bedtime Stories for Cynics”.
* Author: [Dave Hill](/book-authors/dave-hill)
* First Published: [May 2nd 2019](/book-publication-year/2019)
* Series: [Bedtime Stories for Cynics](/book-series/bedtime-stories-for-cynics-series) Book 2
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/52948271-more-bedtime-stories-for-cynics)
>
> Nick Offerman and his posse of high-profile guests present this series of 12 short stories written in the style of classic kid’s tales, but with a decidedly adult approach.
>
> If children's literature is any guide, we should all be able to magically fall asleep simply by saying goodnight to the things we can see from our beds. But any adult knows that our work anxieties and shameful memories would rather stay up all night and chat. That’s where Offerman and Co. come in—with clever and occasionally downright dark parodies of the classic kids genre. What really happened after Snow White died, from the perspective of the one medically trained dwarf? A naive wizard professor reports back from the trenches of an underprivileged school of magic. A middle-aged man is haunted by the voices of his own aging body. The stories will make you laugh, cry and probably squirm a little.