---
title: "Hitchhiker's Guide to the Galaxy (Hitchhiker's Guide to the Galaxy Book 1) by Douglas Adams (1979)"
slug: "hitchhikers-guide-to-the-galaxy-hitchhikers-guide-to-the-galaxy-book-1"
author: "Paulo Pereira"
date: 2019-03-03T22:27:00+00:00
lastmod: 2019-03-03T22:27:00+00:00
cover: "/posts/books/hitchhikers-guide-to-the-galaxy-hitchhikers-guide-to-the-galaxy-book-1.jpg"
description: "Finished “Hitchhiker's Guide to the Galaxy (Hitchhiker's Guide to the Galaxy Book 1)” by Douglas Adams."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Douglas Adams
book series:
  - Hitchhikers Guide to the Galaxy Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Douglas Adams
  - Hitchhikers Guide to the Galaxy Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/hitchhikers-guide-to-the-galaxy-hitchhikers-guide-to-the-galaxy-book-1
---

Finished “Hitchhiker's Guide to the Galaxy”.
* Author: [Douglas Adams](/book-authors/douglas-adams)
* First Published: [October 12th 1979](/book-publication-year/1970s)
* Series: [Hitchhiker's Guide to the Galaxy](/book-series/hitchhikers-guide-to-the-galaxy-series) Book 1
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/386162.The_Hitchhiker_s_Guide_to_the_Galaxy)
>
> Seconds before the Earth is demolished to make way for a galactic freeway, Arthur Dent is plucked off the planet by his friend Ford Prefect, a researcher for the revised edition of The Hitchhiker's Guide to the Galaxy who, for the last fifteen years, has been posing as an out-of-work actor.
>
> Together this dynamic pair begin a journey through space aided by quotes from The Hitchhiker's Guide ("A towel is about the most massively useful thing an interstellar hitchhiker can have") and a galaxy-full of fellow travelers: Zaphod Beeblebrox—the two-headed, three-armed ex-hippie and totally out-to-lunch president of the galaxy; Trillian, Zaphod's girlfriend (formally Tricia McMillan), whom Arthur tried to pick up at a cocktail party once upon a time zone; Marvin, a paranoid, brilliant, and chronically depressed robot; Veet Voojagig, a former graduate student who is obsessed with the disappearance of all the ballpoint pens he bought over the years.