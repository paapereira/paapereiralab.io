---
title: "Radical Candor: Be a Kick-Ass Boss Without Losing Your Humanity by Kim Malone Scott (2017)"
slug: "radical-candor"
author: "Paulo Pereira"
date: 2017-08-05T22:59:00+01:00
lastmod: 2017-08-05T22:59:00+01:00
cover: "/posts/books/radical-candor.jpg"
description: "Finished “Radical Candor: Be a Kick-Ass Boss Without Losing Your Humanity” by Kim Malone Scott."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Kim Malone Scott
book series:
  - 
book genres:
  - Nonfiction
  - Business
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Kim Malone Scott
  - Nonfiction
  - Business
  - 7/10 Books
  - Audiobook
---

Finished “Radical Candor: Be a Kick-Ass Boss Without Losing Your Humanity”.
* Author: [Kim Malone Scott](/book-authors/kim-malone-scott)
* First Published: [March 14th 2017](/book-publication-year/2017)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/32809138-radical-candor)
>
> From the time we learn to speak, we’re told that if you don’t have anything nice to say, don’t say anything at all. While this advice may work for everyday life, it is, as Kim Scott has seen, a disaster when adopted by managers.
>
> Scott earned her stripes as a highly successful manager at Google and then decamped to Apple, where she developed a class on optimal management. She has earned growing fame in recent years with her vital new approach to effective management, the “radical candor” method.
>
> Radical candor is the sweet spot between managers who are obnoxiously aggressive on one side and ruinously empathetic on the other. It’s about providing guidance, which involves a mix of praise as well as criticism—delivered to produce better results and help employees achieve.
>
> Great bosses have strong relationships with their employees, and Scott has identified three simple principles for building better relationships with your employees: make it personal, get (sh)it done, and understand why it matters.
>
> Radical Candor offers a guide to those bewildered or exhausted by management, written for bosses and those who manage bosses. Taken from years of the author’s experience, and distilled clearly giving actionable lessons to the reader; it shows managers how to be successful while retaining their humanity, finding meaning in their job, and creating an environment where people both love their work and their colleagues.