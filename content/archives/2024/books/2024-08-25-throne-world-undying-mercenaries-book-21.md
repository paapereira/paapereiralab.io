---
title: "Throne World (Undying Mercenaries Book 21) by B.V. Larson (2024)"
slug: "throne-world-undying-mercenaries-book-21"
author: "Paulo Pereira"
date: 2024-08-25T12:30:00+01:00
lastmod: 2024-08-25T12:30:00+01:00
cover: "/posts/books/throne-world-undying-mercenaries-book-21.jpg"
description: "Finished “Throne World (Undying Mercenaries Book 21)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2024
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Throne World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [April 27, 2024](/book-publication-year/2024)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 21
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/210457441-throne-world)
>
> The Galaxy is ruled by a single Empire—but the Imperial Throne is empty. The machine races demand a turn at the job, but the organics refuse to allow it. For more than a century, the Galactics have battled for supreme power. War and instability reign in place of peace, with every species spread over a hundred billion stars suffering as the Empire decays.