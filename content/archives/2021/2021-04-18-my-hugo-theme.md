---
title: "Creating my own Hugo Simple List theme"
slug: "my-hugo-theme"
author: "Paulo Pereira"
date: 2021-04-18T13:56:33+01:00
lastmod: 2021-04-18T13:56:33+01:00
description: "Check my own **Simple List** theme on [GitLab](https://gitlab.com/paapereira/hugo-theme-simple-list)."
draft: false
toc: true
categories:
  - General
tags:
  - Hugo
  - GitLab
---

I've been using the [terminal](https://github.com/panr/hugo-theme-terminal) theme, but I wanted to learn more about [Hugo](https://gohugo.io/) and how to create themes. Also I've been wanting to change to a more minimal theme.

I came across the [hugo-classic](https://github.com/goodroot/hugo-classic) theme, and while trying to customize it I decided to give it a go, fork it, and make it my own. I also used the [terminal](https://github.com/panr/hugo-theme-terminal) theme as inspiration.

This is my notes on how I did it and how it turned up. You can find my **Simple List** theme on [GitLab](https://gitlab.com/paapereira/hugo-theme-simple-list).

## Creating the theme

This is a very quick list of steps I took to create my theme.

1. Created a new project on GitLab

https://gitlab.com/paapereira/hugo-theme-simple-list

2. Git clone in my local machine

~~~bash
cd www
git clone git@gitlab.com:paapereira/hugo-theme-simple-list.git
~~~

3. Copied the [hugo-classic](https://github.com/goodroot/hugo-classic) theme I add cloned to my new theme folder

4. Started changing the theme to my hearts content

5. Pushed my new theme to GitLab

~~~bash
cd hugo-theme-simple-list
git add .
git commit -m "New Simple List theme for Hugo.io"
git push -u origin master
~~~

Next I talk a little about some the changes I made.

### theme.toml

This is where the theme information is set. Don't forget to add the original theme information.

https://gitlab.com/paapereira/hugo-theme-simple-list/-/blob/master/theme.toml

### README.md

I kept the base structure and the README file. Reviewed the instructions to match my testing of creating a new site.

https://gitlab.com/paapereira/hugo-theme-simple-list/-/blob/master/README.md

### static/css/style.css

For the site customization I kept only the style.css file.

I tried to simplify it and added some tweaks based of the [terminal](https://github.com/panr/hugo-theme-terminal) theme. I will probably continue to add some tweaks, specially while I learn more about some css 'stuff'.

https://gitlab.com/paapereira/hugo-theme-simple-list/-/blob/master/static/css/style.css

### layouts/

This is where the .html files are kept to define how the site is to be generated.

I tried to simplify it, reducing the number of files and adapting it to my preference. I also added a 'rss.xml' file to add support to the 'description' property in the post files.

Still a lot to learn about Hugo, so I will probably keep on making more changes.

https://gitlab.com/paapereira/hugo-theme-simple-list/-/tree/master/layouts

### exampleSite/

I kept the base examples, but reviewed the 'config.toml' file as to be a good base for all the features this theme have.

https://gitlab.com/paapereira/hugo-theme-simple-list/-/tree/master/exampleSite

## Replacing my current theme

Although is not required, I removed my old theme before adding the new one.

Remove the submodule for the old theme from the `.gitmodules`file:

~~~bash
cd mysite
vi .gitmodules
git add .gitmodules
~~~

Also remove the old theme from the `.git/config`file:

~~~bash
vi .git/config
~~~

Unstage and remove the old theme:

~~~bash
git rm --cached themes/terminal
rm -rf .git/modules/themes/terminal
git commit -m "Removed theme"
rm -rf themes/terminal
~~~

Add the new theme:

~~~bash
git submodule add https://gitlab.com/paapereira/hugo-theme-simple-list themes/simple-list
git add .
git commit -m "New theme"
git push
~~~

## Conclusion

And that was it. In the meanwhile I added some new tweaks and have some others to do and to fix.

Here's the before/after screenshots.

### Before screenshot

![old](/posts/2021/2021-04-18-my-hugo-theme/old-template.png)

### After screenshot

![new](/posts/2021/2021-04-18-my-hugo-theme/new-template.png)