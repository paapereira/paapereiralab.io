---
title: "Network Effect (The Murderbot Diaries Book 5) by Martha Wells (2020)"
slug: "network-effect-the-murderbot-diaries-book-5"
author: "Paulo Pereira"
date: 2021-05-18T21:00:00+01:00
lastmod: 2021-05-18T21:00:00+01:00
cover: "/posts/books/network-effect-the-murderbot-diaries-book-5.jpg"
description: "Finished “Network Effect (The Murderbot Diaries Book 5)” by Martha Wells."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Martha Wells
book series:
  - The Murderbot Diaries Series
book genres:
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Martha Wells
  - The Murderbot Diaries Series
  - Science Fiction
  - 6/10 Books
  - Ebook
---

Finished “Network Effect”.
* Author: [Martha Wells](/book-authors/martha-wells)
* First Published: [May 5th 2020](/book-publication-year/2020)
* Series: [The Murderbot Diaries](/book-series/the-murderbot-diaries-series) Book 5
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/52680842-network-effect)
>
> You know that feeling when you’re at work, and you’ve had enough of people, and then the boss walks in with yet another job that needs to be done right this second or the world will end, but all you want to do is go home and binge your favorite shows? And you're a sentient murder machine programmed for destruction? Congratulations, you're Murderbot.
>
> Come for the pew-pew space battles, stay for the most relatable A.I. you’ll read this century.
>
> —
>
> I’m usually alone in my head, and that’s where 90 plus percent of my problems are.
>
> When Murderbot's human associates (not friends, never friends) are captured and another not-friend from its past requires urgent assistance, Murderbot must choose between inertia and drastic action.
>
> Drastic action it is, then.