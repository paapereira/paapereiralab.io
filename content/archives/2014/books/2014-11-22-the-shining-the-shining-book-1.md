---
title: "The Shining (The Shining Book 1) by Stephen King (1977)"
slug: "the-shining-the-shining-book-1"
author: "Paulo Pereira"
date: 2014-11-22T23:31:00+00:00
lastmod: 2014-11-22T23:31:00+00:00
cover: "/posts/books/the-shining-the-shining-book-1.jpg"
description: "Finished “The Shining (The Shining Book 1)” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Stephen King
book series:
  - The Shining Series
  - Stephen King Novels
book genres:
  - Horror
  - Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Stephen King
  - The Shining Series
  - Stephen King Novels
  - Horror
  - Fiction
  - 8/10 Books
  - Audiobook
---

Finished “The Shining”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [January 28th 1977](/book-publication-year/1970s)
* Series: [The Shining](/book-series/the-shining-series) Book 1, [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/16065967-the-shining)
>
> Jack Torrance's new job at the Overlook Hotel is the perfect chance for a fresh start. As the off-season caretaker at the atmospheric old hotel, he'll have plenty of time to spend reconnecting with his family and working on his writing. But as the harsh winter weather sets in, the idyllic location feels ever more remote . . . and more sinister. And the only one to notice the strange and terrible forces gathering around the Overlook is Danny Torrance, a uniquely gifted five-year-old.