---
title: "Dearly Devoted Dexter (Dexter Book 2) by Jeff Lindsay (2005)"
slug: "dearly-devoted-dexter-dexter-book-2"
author: "Paulo Pereira"
date: 2014-10-29T23:13:00+00:00
lastmod: 2014-10-29T23:13:00+00:00
cover: "/posts/books/dearly-devoted-dexter-dexter-book-2.jpg"
description: "Finished “Dearly Devoted Dexter (Dexter Book 2)” by Jeff Lindsay."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2005
book authors:
  - Jeff Lindsay
book series:
  - Dexter Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jeff Lindsay
  - Dexter Series
  - Fiction
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “Dearly Devoted Dexter”.
* Author: [Jeff Lindsay](/book-authors/jeff-lindsay)
* First Published: [March 15th 2005](/book-publication-year/2005)
* Series: [Dexter](/book-series/dexter-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13155708-dearly-devoted-dexter)
>
> He's a charming monster... A macabre hero... A serial killler who only kills bad people.
>
> Dexter Morgan has been under considerable pressure. It's just not easy being an ethical serial killer - especially while trying to avoid the unshakable suspicions of the dangerous Sergeant Doakes (who believes Dexter is a homicidal maniac...which, of course, he is). In an attempt to throw Doakes off his trail, Dexter has had to slip deep into his foolproof disguise. While not working as a blood-spatter analyst for the Miami Police Department, he now spends nearly all his time with his cheerful girlfriend, Rita, and her two children, sipping light beer and slowly becoming the world's first serial couch potato. But how long can Dexter play Kick the Can instead of Slice the Slasher? How long before his Dark Passenger forces him to drop the charade and let his inner monster run free?
>
> In trying times, opportunity knocks. A particularly nasty psychopath is cutting a trail through Miami - a man whose twisted technique leaves even Dexter speechless. As Dexter's dark appetite is revived, his sister, Deborah (a newly minted, tough-as-nails Miami detective), is drawn headlong into the case. It quickly becomes clear that it will take a monster to catch a monster - but it isn't until his archnemesis is abducted that Dex can finally throw himself into the search for a new plaything. Unless, of course, his plaything finds him first...
>
> With the incredible wit and freshness that drew widespread acclaim to Darkly Dreaming Dexter, Jeff Lindsay now takes Dexter Morgan to a new level of macabre appeal and gives us one of the most original, colorful narrators in years.