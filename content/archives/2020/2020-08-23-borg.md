---
title: "Remote backups using BorgBackup, Vorta and BorgBase"
slug: "borg"
author: "Paulo Pereira"
date: 2020-08-23T20:11:14+01:00
lastmod: 2020-08-23T20:11:14+01:00
description: "I’ve been using a mix of my own scripts with tar/gpg and [duplicity](/tags/duplicity) to manage my backups (local and remote).\n

For some time now duplicity has been my choice, but lately it seems that every time a backup is interrupted it doesn’t restart at the archive it was at, but it starts from the beginning."
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - BorgBackup
  - BorgBase
  - Vorta
aliases:
  - /posts/borg/
---

I’ve been using a mix of my own scripts with tar/gpg and [duplicity](/tags/duplicity) to manage my backups (local and remote).

For some time now duplicity has been my choice, but lately it seems that every time a backup is interrupted it doesn’t restart at the archive it was at, but it starts from the beginning.

Since I have many GB to backup, every time duplicity performs a full backup this is a problem because I have to leave my desktop on for many hours. Also the fans start doing overtime and my little [nuc](/tags/nuc) starts heating up.

Looking around for the re-start issue I found out about [BorgBackup](https://www.borgbackup.org/) and [BorgBase](https://www.borgbase.com/).

Quoting from the official site, [BorgBackup](https://www.borgbackup.org/) (short: Borg) gives you:

- Space efficient storage of backups.    
- Secure, authenticated encryption.    
- Compression: LZ4, zlib, LZMA, zstd (since borg 1.1.4).    
- Mountable backups with FUSE.    
- Easy installation on multiple platforms: Linux, macOS, BSD, ...    
- Free software (BSD license).    
- Backed by a large and active open source community.

[BorgBase](https://www.borgbase.com/) is a Hosting service for Borg Repositories.

So I decided to give it a try.

## Installing

I used the [Vorta Backup Client](https://github.com/borgbase/vorta) to manage my repositories. BorgBackup is installed as a dependency.

```bash
yay -S vorta
```

## BorgBase

[BorgBase](https://www.borgbase.com/) gives you 10GB free to test the service. You can create 2 repositories with the free account.

You have to go to BorgBase > Account > SSH Keys and add the public key for the key you will be using. I used the Vorta client to create a new key.

I then created a 2 repositories. One for daily backups and another for weekly backups.

You a great Setup page where you can choose your repository and the commands to initiate the repository are there to be copied.

As I’m using the Vorta client, I’ll be doing the setup there.

## Vorta Client Setup

I created two profiles in Vorta for the daily and weekly backups.

In the Repository option I chose to create a new remote repository and used the url for the BorgBase repository (you can copy the url from the Repositories page).

I also chose the new SSH Key I had created and chose the LZ4 compression.

I then chose the folders and files to backup and the exclude patterns to ignore what I don’t need to backup.

## Conclusion

I'm still looking around, but I will probably pay for an account and to have more space for my backups.