---
title: "Cleaning your system from outdated software"
slug: "cleaning-your-system-from-outdated-software"
author: "Paulo Pereira"
date: 2008-07-22T21:06:00+01:00
lastmod: 2008-07-22T21:06:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
aliases:
  - /posts/cleaning-your-system-from-outdated-software/
---

As you install, uninstall and update applications there are some garbage left behind.

## Autoremove

When you run *autoremove*, packages that have been installed but are no longer in use, are removed.

```bash
sudo apt-get autoremove 
```

## Autoclean

*Autoclean* removes .deb files from packages no longer installed on your system.

```bash
sudo apt-get autoclean 
```

Do you have other cleaning tips? Send me an email or post a comment.