---
title: "The Vampire Lestat (The Vampire Chronicles Book 2) by Anne Rice (1985)"
slug: "the-vampire-lestat-the-vampire-chronicles-book-2"
author: "Paulo Pereira"
date: 2021-07-12T21:00:00+01:00
lastmod: 2021-07-12T21:00:00+01:00
cover: "/posts/books/the-vampire-lestat-the-vampire-chronicles-book-2.jpg"
description: "Finished “The Vampire Lestat (The Vampire Chronicles Book 2)” by Anne Rice."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Anne Rice
book series:
  - The Vampire Chronicles Series
book genres:
  - Fiction
  - Horror
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Anne Rice
  - The Vampire Chronicles Series
  - Fiction
  - Horror
  - Fantasy
  - 7/10 Books
  - Audiobook
---

Finished “The Vampire Lestat”.
* Author: [Anne Rice](/book-authors/anne-rice)
* First Published: [1985](/book-publication-year/1980s)
* Series: [The Vampire Chronicles](/book-series/the-vampire-chronicles-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53952676-the-vampire-lestat)
>
> Once an aristocrat in the heady days of pre-revolutionary France, now Lestat is a rockstar in the demonic, shimmering 1980s. He rushes through the centuries in search of others like him, seeking answers to the mystery of his terrifying exsitence. His story, the second volume in Anne Rice's best-selling Vampire Chronicles, is mesmerizing, passionate, and thrilling.