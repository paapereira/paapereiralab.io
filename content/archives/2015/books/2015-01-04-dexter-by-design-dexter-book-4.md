---
title: "Dexter By Design (Dexter Book 4) by Jeff Lindsay (2008)"
slug: "dexter-by-design-dexter-book-4"
author: "Paulo Pereira"
date: 2015-01-04T23:28:00+00:00
lastmod: 2015-01-04T23:28:00+00:00
cover: "/posts/books/dexter-by-design-dexter-book-4.jpg"
description: "Finished “Dexter By Design (Dexter Book 4)” by Jeff Lindsay."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2008
book authors:
  - Jeff Lindsay
book series:
  - Dexter Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jeff Lindsay
  - Dexter Series
  - Fiction
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “Dexter By Design”.
* Author: [Jeff Lindsay](/book-authors/jeff-lindsay)
* First Published: [2008](/book-publication-year/2008)
* Series: [Dexter](/book-series/dexter-series) Book 4
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13381931-dexter-by-design)
>
> The macabre, witty New York Times bestselling series (and inspiration for the #1 Showtime series, Dexter) continues as our darkly lovable killer matches wits with a sadistic artiste--who is creating bizarre murder tableaux of his own all over Miami.
> After his surprisingly glorious honeymoon in Paris, life is almost normal for Dexter Morgan. Married life seems to agree with him: he’s devoted to his bride, his stomach is full, and his homicidal hobbies are nicely under control. But old habits die hard--and Dexter’s work as a blood spatter analyst never fails to offer new temptations that appeal to his offbeat sense of justice...and his Dark Passenger still waits to hunt with him in the moonlight.
>
> The discovery of a corpse (artfully displayed as a sunbather relaxing on a Miami beach chair) naturally piques Dexter’s curiosity and Miami’s finest realize they've got a terrifying new serial killer on the loose. And Dexter, of course, is back in business.