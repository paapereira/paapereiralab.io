---
title: "Add a new partition to your system"
slug: "add-new-partition-to-your-system"
author: "Paulo Pereira"
date: 2009-06-11T18:26:00+01:00
lastmod: 2009-06-11T18:26:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - GParted
---

Imagine you buy a new hard drive. Here's a few easy steps to create a  partition in that drive, and a mount point, so you can have that  partition available to you at boot time.

If you have an easy way to add a new partition in your system, let me know.

I usually use [GParted](http://gparted.sourceforge.net/) to create the partition.

## Add a partition

Just follow the following screenshots to create a partition in GParted.  Make sure you choose a Label for your partition. In this example is *Storage*.

![new](/posts/2009/2009-06-11-add-new-partition-to-your-system/gparted-new.png)

![add](/posts/2009/2009-06-11-add-new-partition-to-your-system/gparted-add.png)

![created](/posts/2009/2009-06-11-add-new-partition-to-your-system/gparted-created.png)

## Create a mount point to the new partition

Now that you have a new partition created, you need to create a mount  point. Basically a mount point maps a partition to a directory.

In this example we will map the partition Storage to the location */media/storage*.

- Find out the UUID (Unique Unit ID) of the device

```bash
sudo vol_id -u /dev/sdb1
```

- Create a folder for your new mount point

```bash
sudo mkdir /media/storage
```

- Open */etc/fstab* with gedit (or your favourite editor)

```bash
sudo gedit /etc/fstab
```

- Add the mount point using the UUID you got before, save the file and exit gedit

```text
UUID=ad02cb4e-f9cf-491a-b661-d96ed2c456be /media/storage ext3 relatime 0 2
```

- Add permissions to the mount point to *youruser*

```bash
sudo chown -R youruser:youruser /media/storage
sudo chmod -R 755 /media/storage
```

- Reboot and confirm that */media/storage* allows you to create your files and folders and has the size of the new partition

![mountpoint](/posts/2009/2009-06-11-add-new-partition-to-your-system/mountpoint.png)