---
title: "Upgrading Seafile from 7.1.x to 8.0.x"
slug: "upgrade-seafile-80-90"
author: "Paulo Pereira"
date: 2022-01-01T14:17:46+00:00
lastmod: 2022-01-01T14:17:46+00:00
cover: "/posts/2022/2022-01-01-upgrade-seafile-80-90/seafile.png"
description: "The new 9.0 [Seafile](https://www.seafile.com/) version is out. Time to upgrade. I'm currently running 8.0.16.

Don't forget to read the [release notes](https://manual.seafile.com/upgrade/upgrade_notes_for_9.0.x/) from Seafile."
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Seafile
---

The new 9.0 [Seafile](https://www.seafile.com/) version is out. Time to upgrade. I'm currently running 8.0.16.

Don't forget to read the [release notes](https://manual.seafile.com/upgrade/upgrade_notes_for_9.0.x/) from Seafile.

## Before upgrading

Reading the [release notes](https://manual.seafile.com/upgrade/upgrade_notes_for_9.0.x/), this version needs python modules. I also upgraded `pip`

```bash
sudo /usr/bin/python -m pip install --upgrade pip

sudo /usr/bin/python -m pip install pycryptodome==3.12.0
sudo /usr/bin/python -m pip install cffi==1.14.0
```

I had `cffi` 1.15 in my system, I had to downgrade:

```bash
sudo pacman -U /var/cache/pacman/pkg/python-cffi-1.14.6-2-x86_64.pkg.tar.zst
```

ElasticSearch now needs to be installed and managed individually.

As I currently have the search functionality [disabled](/posts/log4j-seafile) I skipped this for now.

## Upgrading

I started by stopping the services:

```bash
sudo systemctl stop seahub
sudo systemctl stop seafile
```

Then I downloaded the latest version (I use the pro version) and executed the upgrade script.

```bash
sudo -u seafile_user -s /bin/sh

cd /srv/seafile
wget "https://download.seafile.com/d/xxxxxxx/files/?p=/pro/seafile-pro-server_9.0.3_x86-64_CentOS.tar.gz&dl=1" -O seafile-pro-server_9.0.3_x86-64_CentOS.tar.gz

tar -xzf seafile-pro-server_*.gz

cd seafile-pro-server-8.0.0/
upgrade/upgrade_8.0_9.0.sh
```

To use the new golang file-server I add the `use_go_fileserver` option in `seafile.conf`:

```bash
vi conf/seafile.conf
```

```text
[fileserver]
use_go_fileserver = true
```

## Starting the services

```bash
sudo systemctl start seafile
sudo systemctl start seahub
```

See how to start seahub not as a daemon to see errors in this [post](/posts/2021-12-19-fixing-seafile).

I had to do this to find out I had to downgrade `cffi`.