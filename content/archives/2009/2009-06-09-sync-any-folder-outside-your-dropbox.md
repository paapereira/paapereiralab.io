---
title: "Sync any folder outside your Dropbox folder"
slug: "sync-any-folder-outside-your-dropbox"
author: "Paulo Pereira"
date: 2009-06-09T13:24:00+01:00
lastmod: 2009-06-09T13:24:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Dropbox
---

If you don't know [Dropbox](https://www.getdropbox.com/referrals/NTExNjI2Nzk), try it. Basically you can sync your files between computers and OS's.

You define in each computer you install Dropbox a "Dropbox folder" (it  doesn't need to be the same in every computer) and every file and folder inside the folder will be synced.

## Example - Computer A - Linux

```text
/home/youruser/Dropbox/folderA/
/home/youruser/Dropbox/fileA.txt
```

## Example - Computer B - Windows

```text
C:\My Dropbox\folderA\
C:\My Dropbox\fileA.txt
```

In this examples, the folder *folderA* and every file and folder inside, and the file *fileA.txt*, are synced between both computers.

Only the files and folders inside your "Dropbox folder" are synced, but  if you create a symbolic link to a file or folder outside the "Dropbox  folder" it will also be synced.

## Create a symbolic link

```bash
ln -s '/storage/outsidefolder/fileB.txt' '/home/youruser/Dropbox/insidefolder'
ln -s '/storage/outsidefolder/folderB' '/home/youruser/Dropbox/insidefolder'
```

The *fileB.txt* and the *folderB* will be synced into your "Dropbox folder" and synced into the the "Dropbox folder" in Computer B.

Check how to create symbolic links in Windows [here](http://lifehacker.com/5154698/sync-files-and-folders-outside-your-my-dropbox-folder).

## Problem

If you try this between two similar Linux computers, the files in the second computer will not be created in the same outside folder. It will stay in the "Dropbox folder" of that second computer.

fileB.txt in Computer A: /storage/outsidefolder/fileB.txt

fileB.txt in Computer B: /home/youruser/Dropbox/insidefolder/fileB.txt

If you know a way to do this please let me know by commenting in this post or by sending me an [email](https://www.blogger.com/contact/) :-)