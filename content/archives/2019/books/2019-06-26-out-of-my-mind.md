---
title: "Out of My Mind by Alan Arkin (2018)"
slug: "out-of-my-mind"
author: "Paulo Pereira"
date: 2019-06-26T22:24:00+01:00
lastmod: 2019-06-26T22:24:00+01:00
cover: "/posts/books/out-of-my-mind.jpg"
description: "Finished “Out of My Mind” by Alan Arkin."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Alan Arkin
book series:
  - 
book genres:
  - Nonfiction
  - Biography
book scores:
  - 3/10 Books
tags:
  - Book Log
  - Alan Arkin
  - Nonfiction
  - Biography
  - 3/10 Books
  - Audiobook
---

Finished “Out of My Mind”.
* Author: [Alan Arkin](/book-authors/alan-arkin)
* First Published: [December 6th 2018](/book-publication-year/2018)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43169404-out-of-my-mind)
>
> Alan Arkin, one of the most beloved and accomplished actors of our time, reveals a side of himself not often shown on stage or screen.
>
> Like many teenagers, 16-year-old Alan Arkin had it all figured out. Then came young adulthood, and with it a wave of doubt so strong it caused him to question everything he thought he knew about himself and the world. Ever skeptical and full of questions, Arkin embarked on a spiritual journey to find something—anything—to believe in. An existential crisis in his 30s led him to the study of Eastern philosophy. Soon he began opening himself to the possibility that there was more to life than what he had simply seen, heard, or been taught.
>
> In this Audible Original "mini-memoir," the 84-year-old actor shares his powerful spiritual experiences, from his brush with reincarnation to the benefits of meditation. In a gruff, earthy voice that sounds more suited to a New York cabbie than a spiritual guide, he shows us that wisdom can come from the most unexpected places and teachers. Out of My Mind is a candid, relatable, and delightfully irreverent take on how one man went searching for meaning and ended up discovering himself.