---
title: "Iron Gold (Red Rising Saga Book 4) by Pierce Brown (2018)"
slug: "iron-gold-red-rising-saga-book-4"
author: "Paulo Pereira"
date: 2022-01-03T22:00:00+00:00
lastmod: 2022-01-03T22:00:00+00:00
cover: "/posts/books/iron-gold-red-rising-saga-book-4.jpg"
description: "Finished “Iron Gold (Red Rising Saga Book 4)” by Pierce Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Pierce Brown
book series:
  - Red Rising Saga Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Pierce Brown
  - Red Rising Saga Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
---

Finished “Iron Gold”.
* Author: [Pierce Brown](/book-authors/pierce-brown)
* First Published: [January 16th 2018](/book-publication-year/2018)
* Series: [Red Rising Saga](/book-series/red-rising-saga-series) Book 4
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/33361943-iron-gold)
>
> They call him father, liberator, warlord, Reaper. But he feels a boy as he falls toward the pale blue planet, his armor red, his army vast, his heart heavy. It is the tenth year of war and the thirty-second of his life.
> 
> A decade ago, Darrow was the hero of the revolution he believed would break the chains of the Society. But the Rising has shattered everything: Instead of peace and freedom, it has brought endless war. Now he must risk everything he has fought for on one last desperate mission. Darrow still believes he can save everyone, but can he save himself?
> 
> And throughout the worlds, other destinies entwine with Darrow’s to change his fate forever:
> 
> A young Red girl flees tragedy in her refugee camp and achieves for herself a new life she could never have imagined.
> 
> An ex-soldier broken by grief is forced to steal the most valuable thing in the galaxy—or pay with his life.
> 
> And Lysander au Lune, the heir in exile to the sovereign, wanders the stars with his mentor, Cassius, haunted by the loss of the world that Darrow transformed, and dreaming of what will rise from its ashes.
> 
> Red Rising was the story of the end of one universe, and Iron Gold is the story of the creation of a new one. Witness the beginning of a stunning new saga of tragedy and triumph from masterly New York Times bestselling author Pierce Brown.