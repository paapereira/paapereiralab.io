---
title: "Steel World (Undying Mercenaries Book 1) by B.V. Larson (2013)"
slug: "steel-world-undying-mercenaries-book-1"
author: "Paulo Pereira"
date: 2017-06-18T22:26:00+01:00
lastmod: 2017-06-18T22:26:00+01:00
cover: "/posts/books/steel-world-undying-mercenaries-book-1.jpg"
description: "Finished “Steel World (Undying Mercenaries Book 1)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Steel World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [November 4th 2013](/book-publication-year/2013)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/20409734-steel-world)
>
> In the 20th century Earth sent probes, transmissions, and welcoming messages to the stars. Unfortunately, someone noticed. The Galactics arrived with their battle fleet in 2052. Rather than being exterminated under a barrage of hell-burners, Earth joined their vast Empire. Swearing allegiance to our distant alien overlords wasn't the only requirement for survival. We also had to have something of value to trade, something that neighboring planets would pay their hard-earned credits to buy. As most of the local worlds were too civilized to have a proper army, the only valuable service Earth could provide came in the form of soldiers…someone had to do their dirty work for them, their fighting and dying.
>
> I, James McGill, was born in 2099 on the fringe of the galaxy. When Hegemony Financial denied my loan applications, I was kicked out of the university and I turned to the stars. My first campaign involved the invasion of a mineral-rich planet called Cancri-9, better known as Steel World. The attack didn't go well, and now Earth has entered a grim struggle for survival. Humanity's mercenary legions go to war in Steel World, best-selling author B. V. Larson's latest science fiction novel.