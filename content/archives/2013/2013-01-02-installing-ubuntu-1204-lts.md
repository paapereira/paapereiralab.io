---
title: "Installing Ubuntu 12.04 LTS"
slug: "installing-ubuntu-1204-lts"
author: "Paulo Pereira"
date: 2013-01-02T16:08:00+00:00
lastmod: 2013-01-02T16:08:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 12.04 LTS
  - Precise Pangolin
aliases:
  - /posts/installing-ubuntu-1204-lts/
---

Although Ubuntu 12.10 had already came out, since [12.04](http://releases.ubuntu.com/precise/) is a [LTS](https://wiki.ubuntu.com/LTS) release and I didn't post my installation guide at the time, I decided to document my take on it.

![Ubuntu 12.04 LTS](/posts/2013/2013-01-02-installing-ubuntu-1204-lts/ubuntu.png)

Also, I had bought a new computer at the time (Asus M5A99X board) and had to consider EFI and my new SSD drive.

## Hardware considerations

Comparing this install with my [previous ones](/tags/linux), I had two new things to consider: a [EFI](http://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface) motherboard and a SSD drive.

After my first attempt at installing Ubuntu in my new system, I was having the following problem when booting:

> GRUB: “invalid arch independent ELF magic” after install on SSD

After much research and new install attempts (including a Linux Mint) I found two solutions:

- Re-install GRUB (from a live cd)
```bash
sudo mount /dev/sda1 /mnt
sudo grub-install --root-directory=/mnt /dev/sda
```
- Create a /boot partition for EFI

I decided to go with the second option. More on this later.

## Preparing for installation

- Backup your data!
- [Download](http://releases.ubuntu.com/precise/) Ubuntu. In my case was the 64bit version (ubuntu-12.04-desktop-amd64.iso).
- Check the [hash](https://help.ubuntu.com/community/UbuntuHashes) of the downloaded file:
```bash
md5sum ubuntu-12.04-desktop-amd64.iso
```
- Compare the output with the Ubuntu [hash list](https://help.ubuntu.com/community/UbuntuHashes):
```text
128f0c16f4734c420b0185a492d92e52 ubuntu-12.04-desktop-amd64.iso
```
- Create a CD or a bootable USB stick. I used the "Startup Disk Creator" in Ubuntu. More information [here](http://www.ubuntu.com/download/desktop).
- Reboot into your CD or bootable USB stick.
- Start the installation!

Installing Ubuntu it's pretty straightforward this days, so I'll focus only on the most important parts, given my system.

## Partitions

As I have a EFI motherboard a created the following partition in my SSD and HDD drives.

```text
/dev/sda
  /dev/sda1    fat16    /boot/efi        500 MB      -- primary, flags: boot
  /dev/sda2    ext4    /             55,41 GB      -- primary
/dev/sdb
  /dev/sdb1    swap                 3,81 GB      -- primary
  /dev/sda5    ext4    /home       927,70 GB      -- extended
```

## Performance boosts

After installing Ubuntu, and given the characteristics of a [SSD](http://en.wikipedia.org/wiki/Solid-state_drive) drive I did the following tweaks in /etc/fstab:

- Open /etc/fstab as root
```bash
gksu gedit /etc/fstab
```
- Change the / mounting options to noatime
```text
UUID=caf2c98d-5796-4187-992d-ae0f1e48d678   /   ext4   noatime,discard,data=ordered,errors=remount-ro 0    1
```
- Mount the /tmp partition in RAM
```text
tmpfs /tmp tmpfs nodev,nosuid,noatime,mode=1777 0 0
```

I also changed the IO scheduler to noop:

- Open /etc/rc.local as root
```bash
gksu gedit /etc/rc.local
```
- Add the following to change the IO scheduler
```bash
echo noop > /sys/block/sda/queue/scheduler
```

And changed he swap priority:
```bash
$ gksu gedit /etc/sysctl.conf
	vm.swappiness=10
```
Some links with more information:

http://www.zdnet.com/blog/perlow/geek-sheet-a-tweakers-guide-to-solid-state-drives-ssds-and-linux/9190

http://askubuntu.com/questions/86433/ubuntu-11-10-on-modern-ssd

http://www.howtogeek.com/62761/how-to-tweak-your-ssd-in-ubuntu-for-better-performance/

http://askubuntu.com/questions/19376/installing-ubuntu-on-a-ssd

http://askubuntu.com/questions/1400/optimizing-the-os-for-ssds

https://wiki.ubuntu.com/MagicFab/SSDchecklist

http://askubuntu.com/questions/18903/how-to-enable-trim

http://ubuntued.info/diminua-a-prioridade-da-swap-do-ubuntu-para-ter-melhor-performance

## Extra software installed and other changes**

- Tomboy (always the first thing I install to get my notes)

```bash
sudo apt-get install tomboy
```

- Add Tomboy to the Startup Applications (Command: tomboy)

- [Terminator](/posts/favapps-terminator/) (my favorite terminal)

```bash
sudo apt-get install terminator
```

- Change DHCP for manual so I can use my B3 Server as a DNS server

- Disable "Require my password when waking from suspend" option (System Settings > Brightness and Lock)

- Install NVIDIA drivers (System Settings > Additional Drivers)

- Select the Canonical Partners PPA and Independent PPA in Software Sources

- Update the system to the latest releases

```bash
sudo apt-get update
sudo apt-get upgrade
```

- SSH and SSH File System for my [B3 Server](/tags/b3-server) mounts

```bash
sudo apt-get install sshfs openssh-server
```

- Samba

```bash
sudo apt-get install samba
```

- Gigolo

```bash
sudo apt-get install gigolo
```

- Chrome

Download:

[http://www.google.com/chrome/intl/en/eula_beta.html?dl=beta_amd64_deb](https://www.google.com/chrome/intl/en/eula_beta.html?dl=beta_amd64_deb)

Install:

```bash
sudo dpkg -i Downloads/google-chrome/google-chrome-beta_current_amd64.deb
```

I had this errors:

> dpkg: dependency problems prevent configuration of google-chrome-beta:
> 
>  google-chrome-beta depends on libxss1; however:
>  
>  Package libxss1 is not installed.
>  
>  google-chrome-beta depends on libcurl3; however:
>  
>  Package libcurl3 is not installed.
>  
> dpkg: error processing google-chrome-beta (--install):
> 
>  dependency problems - leaving unconfigured 

Resolving dependencies:

```bash
sudo apt-get -f install 
```

Google Talk Plugin:

```bash
sudo dpkg -i Downloads/google-chrome/google-talkplugin_current_amd64.deb
```

Removing Firefox:

```bash
sudo apt-get remove --purge firefox
```

- Multimedia

```bash
sudo apt-get install libxine1-ffmpeg gxine mencoder mpeg2dec  vorbis-tools id3v2 mpg321 mpg123 libflac++6 ffmpeg totem-mozilla icedax  tagtool easytag id3tool lame nautilus-script-audio-convert libmad0  libjpeg-progs flac faac faad sox ffmpeg2theora libmpeg2-4 uudeview flac  libmpeg3-1 mpeg3-utils mpegdemux liba52-0.7.4-dev libquicktime2
sudo apt-get install gstreamer0.10-ffmpeg gstreamer0.10-fluendo-mp3  gstreamer0.10-gnonlin gstreamer0.10-sdl  gstreamer0.10-plugins-bad-multiverse gstreamer0.10-schroedinger  gstreamer0.10-plugins-ugly totem-plugins-extra
sudo apt-get install gstreamer-dbus-media-service gstreamer-tools ubuntu-restricted-extras
sudo apt-get install ttf-mscorefonts-installer
sudo apt-get install libdvdcss2 libdvdnav4
sudo apt-get install libdvdread4
sudo /usr/share/doc/libdvdread4/./install-css.sh
sudo apt-get install regionset
sudo regionset
```

- VLC

```bash
sudo apt-get install vlc vlc-plugin-pulse vlc-plugin-notify
```

- Banshee

```bash
sudo apt-add-repository ppa:banshee-team/ppa
sudo apt-get update
sudo apt-get install banshee banshee-extension-lyrics
```

- CompizConfig Settings Manager

```bash
sudo apt-get install compizconfig-settings-manager
```

- MyUnity

```bash
sudo apt-get install myunity
```

- Ubuntu Tweak

```bash
sudo add-apt-repository ppa:tualatrix/next
sudo apt-get update
sudo apt-get install ubuntu-tweak
```

- Java (no more Sun Java...)

```bash
sudo apt-get install openjdk-7-jre
```

- HP Linux Imaging and Printing GUI

```bash
sudo apt-get install hplip-gui
```

- Dropbox

```bash
rm -r ~/.dropbox-dist
sudo apt-get install nautilus-dropbox
dropbox start -i
```

- GParted

```bash
sudo apt-get install gparted ntfs-3g
```

- GCStar

```bash
sudo apt-get install gcstar
```

- Gaupol

```bash
sudo apt-get install gaupol
```

- Portuguese Aspell

```bash
sudo apt-get install aspell-pt-pt
```

- GParted

```bash
sudo apt-get install gparted
```

- Compression tools support

```bash
sudo apt-get install unace rar unrar p7zip zip unzip sharutils uudeview mpack lha arj cabextract file-roller
```

- Libre Office

```bash
sudo add-apt-repository ppa:libreoffice/ppa
sudo apt-get update
sudo apt-get dist-upgrade
```

- Gimp

```bash
sudo apt-get install gimp gimp-data gimp-plugin-registry gimp-data-extras
```

- Gconf Editor

```bash
sudo apt-get install gconf-editor
```

- XChat

```bash
sudo apt-get install xchat xchat-indicator
```

- Menu Editor

```bash
sudo apt-get install lo-menubar
```

- Everpad

```bash
sudo add-apt-repository ppa:nvbn-rm/ppa
sudo apt-get update
sudo apt-get install everpad
```

## Mounting Chrome cache in memory

I created the following script to keep the Chrome cache in RAM:

- Create the script:

```bash
vi ~/bin/chrome_tmp_cache.sh
```

- Add the following:

```bash
#!/bin/bash
if test -d /tmp/chrome; then
  exit 0
else
  rm -r ~/.cache/google-chrome
  mkdir /tmp/chrome
  ln -s /tmp/chrome ~/.cache/google-chrome
fi
```

- Change the script permissions:

```bash
chmod +x bin/chrome_tmp_cache.sh
```

- Add the script to the Startup Applications:

```text
Name: Chrome Cache
Command: /home/<user>/bin/chrome_tmp_cache.sh
Comment: Move Chrome cache to /tmp
```

## Useful Links

[Ubuntu](http://www.ubuntu.com/)
[Ubuntu Forums](http://ubuntuforums.org/)
[OMG! Ubuntu! "10 Things To Do After Installing Ubuntu 12.04"](http://www.omgubuntu.co.uk/2012/04/10-things-to-do-after-installing-ubuntu-12-04)