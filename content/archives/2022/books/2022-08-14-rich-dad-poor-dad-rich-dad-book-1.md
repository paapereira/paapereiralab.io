---
title: "Rich Dad Poor Dad (Rich Dad Book 1) by Robert T. Kiyosaki (1997)"
slug: "rich-dad-poor-dad-rich-dad-book-1"
author: "Paulo Pereira"
date: 2022-08-14T15:30:00+01:00
lastmod: 2022-08-14T15:30:00+01:00
cover: "/posts/books/rich-dad-poor-dad-rich-dad-book-1.jpg"
description: "Finished “Rich Dad Poor Dad (Rich Dad Book 1)” by Robert T. Kiyosaki."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Robert T. Kiyosaki
book series:
  - Rich Dad Series
book genres:
  - Nonfiction
  - Self Help
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Robert T. Kiyosaki
  - Rich Dad Series
  - Nonfiction
  - Self Help
  - 7/10 Books
  - Audiobook
---

Finished “Rich Dad Poor Dad”.
* Author: [Robert T. Kiyosaki](/book-authors/robert-t.-kiyosaki)
* First Published: [January 1, 1997](/book-publication-year/1990s)
* Series: [Rich Dad](/book-series/rich-dad-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/20918792-rich-dad-poor-dad)
>
> Rich Dad Poor Dad will Explode the myth that you need to earn a high income to become rich
> 
> Challenge the belief that your house is an asset
> 
> Show parents why they can't rely on the school system to teach their kids about money
> 
> Define once and for all an asset and a liability
> 
> Teach you what to teach your kids about money for their future financial success
> 
> Robert Kiyosaki has challenged and changed the way tens of millions of people around the world think about money. With perspectives that often contradict conventional wisdom, Robert has earned a reputation for straight talk, irreverence, and courage. He is regarded worldwide as a passionate advocate for financial education.
> 
> The main reason people struggle financially is because they have spent years in school but learned nothing about money. The result is that people learn to work for money but never learn to have money work for them. Robert Kiyosaki