---
title: "The Wise Man's Fear (The Kingkiller Chronicle Book 2) by Patrick Rothfuss (2011)"
slug: "the-wise-mans-fear-the-kingkiller-chronicle-book-2"
author: "Paulo Pereira"
date: 2020-12-08T15:09:00+00:00
lastmod: 2020-12-08T15:09:00+00:00
cover: "/posts/books/the-wise-mans-fear-the-kingkiller-chronicle-book-2.jpg"
description: "Finished “The Wise Man's Fear (The Kingkiller Chronicle Book 2)” by Patrick Rothfuss."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Patrick Rothfuss
book series:
  - The Kingkiller Chronicle Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Patrick Rothfuss
  - The Kingkiller Chronicle Series
  - Fiction
  - Fantasy
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/the-wise-mans-fear-the-kingkiller-chronicle-book-2
---

Finished “The Wise Man's Fear”.
* Author: [Patrick Rothfuss](/book-authors/patrick-rothfuss)
* First Published: [March 1st 2011](/book-publication-year/2011)
* Series: [The Kingkiller Chronicle](/book-series/the-kingkiller-chronicle-series) Book 2
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/15700595-the-wise-man-s-fear)
>
> In The Wise Man's Fear, Kvothe searches for answers, attempting to uncover the truth about the mysterious Amyr, the Chandrian, and the death of his parents. Along the way, Kvothe is put on trial by the legendary Adem mercenaries, forced to reclaim the honor of his family, and travels into the Fae realm. There he meets Felurian, the faerie woman no man can resist, and who no man has ever survived...until Kvothe.
>
> Now, Kvothe takes his first steps on the path of the hero and learns how difficult life can be when a man becomes a legend in his own time.