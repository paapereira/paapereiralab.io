---
title: "Skeleton Crew by Stephen King (1985)"
slug: "skeleton-crew"
author: "Paulo Pereira"
date: 2023-01-15T19:40:00+00:00
lastmod: 2023-01-15T19:40:00+00:00
cover: "/posts/books/skeleton-crew.jpg"
description: "Finished “Skeleton Crew” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
  - Short Stories
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - Short Stories
  - 7/10 Books
  - Audiobook
---

Finished “Skeleton Crew”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [June 21, 1985](/book-publication-year/1980s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34724175-skeleton-crew)
>
> In a bumper collection of truly chilling tales, we meet Gramma - who only wanted to hug little George, even after she was dead; The Raft - a primeval sea creature with an insatiable appetite; The Monkey - an innocent-looking toy with sinister powers; the unspeakable horror of The Mist. And there is a gruesome host of other stories, each with the distinctive blend of unimaginable terror and realism that typifies King's writing.