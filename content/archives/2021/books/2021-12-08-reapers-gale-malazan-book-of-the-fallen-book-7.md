---
title: "Reaper's Gale (Malazan Book of the Fallen Book 7) by Steven Erikson (2007)"
slug: "reapers-gale-malazan-book-of-the-fallen-book-7"
author: "Paulo Pereira"
date: 2021-12-08T08:00:00+00:00
lastmod: 2021-12-08T08:00:00+00:00
cover: "/posts/books/reapers-gale-malazan-book-of-the-fallen-book-7.jpg"
description: "Finished “Reaper's Gale (Malazan Book of the Fallen Book 7)” by Steven Erikson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2007
book authors:
  - Steven Erikson
book series:
  - Malazan Book of the Fallen Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Steven Erikson
  - Malazan Book of the Fallen Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
---

Finished “Reaper's Gale”.
* Author: [Steven Erikson](/book-authors/steven-erikson)
* First Published: [May 7th 2007](/book-publication-year/2007)
* Series: [Malazan Book of the Fallen](/book-series/malazan-book-of-the-fallen-series) Book 7
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8623112-reaper-s-gale)
>
> A truly epic fantasy series that has confirmed its author as one of the most original and exciting genre storytellers in years.
> 
> Erikson’s ‘Malazan Book of the Fallen’ has been recognised the world-over by writers, critics and fans alike — in a recent review of The Bonehunters, the sixth chapter in this remarkable tale, the UK’s Interzone magazine hailed it ‘a masterpiece’ and ‘the benchmark for all future works in the field’, while the hugely influential genre website, Ottawa-based SF Site, declared ‘this series has clearly established itself as the most significant work of epic fantasy since Donaldson’s Chronicles of Thomas Covenant’.
> 
> Now comes Reaper’s Gale — the seventh Tale of the Malazan Book of the Fallen — and neither Erikson nor the excitement are showing any sign of letting up. Mauled and now cut adrift by the Malazan Empire, Tavore and her now infamous 14th army have landed on the coast of a strange, unknown continent and find themselves facing an even more dangerous enemy: the Tiste Edur, a nightmarish empire pledged to serve the Crippled God…
> 
> A brutal, harrowing novel of war, intrigue and dark, uncontrollable magic, this is fantasy at its most imaginative and storytelling at its most thrilling.