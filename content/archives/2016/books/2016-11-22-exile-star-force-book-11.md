---
title: "Exile (Star Force Book 11) by B.V. Larson and David VanDyke (2014)"
slug: "exile-star-force-book-11"
author: "Paulo Pereira"
date: 2016-11-22T22:44:00+00:00
lastmod: 2016-11-22T22:44:00+00:00
cover: "/posts/books/exile-star-force-book-11.jpg"
description: "Finished “Exile (Star Force Book 11)” by B.V. Larson and David VanDyke."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - B.V. Larson
  - David VanDyke
book series:
  - Star Force Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - David VanDyke
  - Star Force Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Exile”.
* Author: [B.V. Larson](/book-authors/b.v.-larson) and [David VanDyke](/book-authors/david-vandyke)
* First Published: [September 20th 2014](/book-publication-year/2014)
* Series: [Star Force](/book-series/star-force-series) Book 11
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25596113-exile)
>
> Lost and cut off from Earth, the crew of the starship Valiant seeks a way home.
>
> In the eleventh book of the Star Force series Cody Riggs collides with the overwhelming power of the Ancients. They fly incomprehensible ships and consider humans to be curiosities.
>
> Caught between ghosts of the past and demons of the present, Cody is abandoned by everyone. His girl, his crew, and even his ships change sides, exiling him in the cold depths of space.
>
> As is the strength of any Riggs family member, Cody manages to climb back into the game, upsetting the plans of his enemies and his allies alike. The only question is whether or not he’ll live long enough to regret it.
>
> EXILE is a full length novel of science fiction by bestselling authors B. V. Larson and David VanDyke. The first book telling Cody Riggs' story is titled OUTCAST. If you've never read any of the Star Force novels, SWARM was the beginning of it all.