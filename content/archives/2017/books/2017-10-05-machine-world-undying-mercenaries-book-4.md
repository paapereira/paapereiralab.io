---
title: "Machine World (Undying Mercenaries Book 4) by B.V. Larson (2015)"
slug: "machine-world-undying-mercenaries-book-4"
author: "Paulo Pereira"
date: 2017-10-05T21:41:00+01:00
lastmod: 2017-10-05T21:41:00+01:00
cover: "/posts/books/machine-world-undying-mercenaries-book-4.jpg"
description: "Finished “Machine World (Undying Mercenaries Book 4)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Machine World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [January 9th 2015](/book-publication-year/2015)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 4
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28206764-machine-world)
>
> The Galactics arrived with their Battle Fleet in 2052. Rather than being exterminated under a barrage of hell-burners, Earth joined a vast Empire that spanned the Milky Way. Our only worthwhile trade goods are our infamous mercenary legions, elite troops we sell to the highest alien bidder.
>
> In the fourth book of the series, James McGill is up for promotion. Not everyone is happy about that, and McGill must prove he’s worth his stripes. Deployed to a strange alien planet outside the boundaries of the Galactic Empire, he’s caught up in warfare and political intrigue. Earth expands, the Cephalopod Kingdom launches ships to stop us, and a grand conspiracy emerges among the upper ranks of the Hegemony military.
>
> In MACHINE WORLD, McGill faces an entirely new kind of alien life, Galactic prosecution, and thousands of relentless squid troopers. He lives and dies in the falling ashes of the Empire, a man of unique honor at the dawn of humanity’s resurgence.