---
title: "Extinction (Star Force Book 2) by B.V. Larson (2011)"
slug: "extinction-star-force-book-2"
author: "Paulo Pereira"
date: 2016-03-03T23:23:00+00:00
lastmod: 2016-03-03T23:23:00+00:00
cover: "/posts/books/extinction-star-force-book-2.jpg"
description: "Finished “Extinction (Star Force Book 2)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - B.V. Larson
book series:
  - Star Force Series
book genres:
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Force Series
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Extinction”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [March 28th 2011](/book-publication-year/2011)
* Series: [Star Force](/book-series/star-force-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/11019965-extinction)
>
> Earth's Star Force marines invade an alien world!
>
> In the second book of the Star Force series, Kyle Riggs has another bad year. The Nano ships have a new mission-one that sentences their pilots to death. Meanwhile, the governments of Earth want to steal Star Force's Nano technology for their own. Worst of all, Earth has made a promise to the Macros, and the machines are coming to collect.
>
> EXTINCTION is the story of Earth's entry into an interstellar war between living creatures and machines. To buy the peace, we've signed up with the machines.... EXTINCTION is an 112,000 word science fiction novel.