---
title: "Upgrading to Ubuntu 13.04"
slug: "upgrading-to-ubuntu-1304"
author: "Paulo Pereira"
date: 2013-04-27T21:57:00+01:00
lastmod: 2013-04-27T21:57:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 13.04
  - Raring Ringtail
---

It's that time of the year! Well, the first one... A new Ubuntu version comes out every April and October.

[Ubuntu 13.04](http://www.ubuntu.com/), Raring Ringtail, has been released.

Time to do a direct upgrade from my 12.10 installation.

![Ubuntu 13.04](/posts/2013/2013-04-27-upgrading-to-ubuntu-1304/ubuntu.png)

The upgrade process was quite simple. I upgraded some packages normally  using Update Manager, and when everything was up to date:

![Software Updater](/posts/2013/2013-04-27-upgrading-to-ubuntu-1304/software-center.png)

After rebooting, the only thing I had to do was going to "Software & Updates" and in the "Other Software" tab re-add my 3rd party PPAs.

Unity seems faster and everything is running smooth.