---
title: "Sky World (Undying Mercenaries Book 18) by B.V. Larson (2022)"
slug: "sky-world-undying-mercenaries-book-18"
author: "Paulo Pereira"
date: 2022-12-31T17:50:00+00:00
lastmod: 2022-12-31T17:50:00+00:00
cover: "/posts/books/sky-world-undying-mercenaries-book-18.jpg"
description: "Finished “Sky World (Undying Mercenaries Book 18)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2022
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Sky World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [December 12, 2022](/book-publication-year/2022)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 18
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/63833653-sky-world)
>
> Earth is locked in a dangerous arms race with Rigel. McGill’s efforts at diplomacy are disastrous. Events spin out of control, and interstellar warfare consumes the outer provinces of the Empire. Great fleets are gathered, and troops are hurried aboard transports. Legion Varus is launched toward a secret target world.
> 
> McGill leads an early strike against Rigel’s defenses, but his efforts are foiled at every turn. Who could be behind these betrayals? Could his own government be plotting his demise?