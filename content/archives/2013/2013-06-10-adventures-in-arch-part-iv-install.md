---
title: "Adventures in Arch (Part IV): Install extra programs"
slug: "adventures-in-arch-part-iv-install"
author: "Paulo Pereira"
date: 2013-06-10T18:47:00+01:00
lastmod: 2013-06-15T18:47:00+01:00
draft: false
toc: no
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
---

In this part I will show the extra programs I installed.

I will keep updating this post in the next weeks, to add more programs installed.

![Arch Linux](/posts/2013/2013-06-10-adventures-in-arch-part-iv-install/arch-linux.png)

This is the list of programs I have installed:

- Chrome (download from [AUR](https://aur.archlinux.org/packages/google-chrome-beta/))

```bash
mkdir builds
cd builds
tar -xzf google-chrome-beta.tar.gz
cd google-chrome-beta
makepkg -s
sudo pacman -U google-chrome-beta-28.0.1500.36-1-x86_64.pkg.tar.xz
```

- GNOME Tweak Tool

```bash
sudo pacman -S gnome-tweak-tool
```

- Tomboy

```bash
sudo pacman -S tomboy
```

To get my notes I recovered the following folders from my /home backup:

> ~/.config/tomboy/
> 
> ~/.local/share/tomboy/

- rsync

```bash
sudo pacman -S rsync
```

- Gimp

```bash
sudo pacman -S gimp
```

- Terminator

```bash
sudo pacman -S terminator
```

To get my Terminator settings I recovered the following folder from my /home backup:

> ~/.config/terminator

- SSH

```bash
sudo pacman -S sshfs
sudo pacman -S openssh
```

To get my ssh keys back I recovered the following folders from my /home backup:

> ~/.ssh
> 
> ~/.gnupg

To mount my server shares I use a [script](/posts/mount-remote-directories-in-your-local/). Here's how to make it run at boot time:

```bash
$ nano /home/youruser/.config/autostart/server_sshfsmount.desktop.desktop
```

```text
[Desktop Entry]
Type=Application
Exec=/home/youruser/bin/sshfsmount/sshfsmount.sh mount
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_US]=server_sshfsmount.desktop
Name=Mount server Shares
Comment[en_US]=Mount server Shares
Comment=Mount server Shares
```

- Samba

```bash
sudo pacman -S samba
```

- Multimedia (more info [here](https://wiki.archlinux.org/index.php/Codecs))

```bash
sudo pacman -S a52dec
sudo pacman -S faac
sudo pacman -S faad2
sudo pacman -S flac
sudo pacman -S jasper
sudo pacman -S lame
sudo pacman -S libdca
sudo pacman -S libdv
sudo pacman -S libmad
sudo pacman -S libmpeg2
sudo pacman -S libtheora
sudo pacman -S libvorbis
sudo pacman -S libxv
sudo pacman -S opus
sudo pacman -S wavpack
sudo pacman -S x264
sudo pacman -S xvidcore
sudo pacman -S gstreamer0.10-plugins
sudo pacman -S xine-lib
sudo pacman -S gnome-vfs
sudo pacman -S imagemagick
sudo pacman -S openexr
sudo pacman -S ffmpeg
sudo pacman -S gst-libav
```

Download fluendo from [AUR](https://aur.archlinux.org/packages/gstreamer0.10-fluendo/).

```bash
cd builds
tar -xzf gstreamer0.10-fluendo.tar.gz
cd gstreamer0.10-fluendo
makepkg -s
sudo pacman -U gstreamer0.10-fluendo-0.10.72-2-x86_64.pkg.tar.xz
```

- VLC

```bash
sudo pacman -S vlc
```

- Banshee

```bash
sudo pacman -S banshee
```

To get my Banshee settings I recovered the following folder from my /home backup:

> ~/.config/banshee-1

- GDM Setup (download from [here](https://aur.archlinux.org/packages/gdm3setup-utils/) and [here](https://aur.archlinux.org/packages/gdm3setup/))

```bash
cd builds
tar -xzf gdm3setup-utils.tar.gz
cd gdm3setup-utils
makepkg -s
sudo pacman -U gdm3setup-utils-20130207-1-any.pkg.tar.xz
cd ..
tar -xzf gdm3setup-utils.tar.gz
cd gdm3setup
makepkg -s
sudo pacman -U gdm3setup-20130207-1-any.pkg.tar.xz
```

- Filezilla

```bash
sudo pacman -S filezilla
```

To get my Filezilla settings I recovered the following folder from my /home backup:

> ~/.filezilla

- HP printer support

```bash
sudo pacman -S hplip
sudo pacman -S sane cups cups-pdf cups-filters rpcbind \
	python2-pyqt ghostscript ghostscript psutils
```

Also download [this](https://aur.archlinux.org/packages/hplip-plugin/), [this](https://aur.archlinux.org/packages/foomatic-db-foo2zjs/) and [this](https://aur.archlinux.org/packages/foo2zjs/) from AUR:

```bash
cd builds
tar -xzf hplip-plugin.tar.gz
cd hplip-plugin
makepkg -s
sudo pacman -U hplip-plugin-3.13.5-1-x86_64.pkg.tar.xz
cd ..
tar -xzf foomatic-db-foo2zjs.tar.gz
cd foomatic-db-foo2zjs
makepkg -s
sudo pacman -U foomatic-db-foo2zjs-1\:4.0.17_20130518-1-x86_64.pkg.tar.xz
cd ..
tar -xzf foo2zjs.tar.gz
cd foo2zjs
makepkg -s
sudo pacman -U foo2zjs-20130530-3-x86_64.pkg.tar.xz
```

Now for installing the printer. Mine is connected to my router.

```bash
sudo groupadd lpadmin
sudo usermod -aG lpadmin youruser
sudo gpasswd -a youruser lp
sudo gpasswd -a youruser sys
sudo nano /etc/cups/cups-files.conf
```

Uncomment:

> Group lp

Add lpadmin:

> SystemGroup sys root lpadmin

```bash
sudo systemctl restart cups.service
sudo systemctl enable avahi-daemon.service
```

Re-login.

```bash
hp-toolbox
```

In the Device Discovery choose the Network option and in the Advanced Options add your printer IP in Manual Discovery.

- Scanning

Download from AUR the following:

https://aur.archlinux.org/packages/perl-gtk2-ex-simple-list/

https://aur.archlinux.org/packages/perl-config-general/

https://aur.archlinux.org/packages/perl-font-ttf/

https://aur.archlinux.org/packages/perl-pdf-api2/

https://aur.archlinux.org/packages/perl-goo-canvas/

https://aur.archlinux.org/packages/perl-gtk2-imageview/

https://aur.archlinux.org/packages/perl-sane/

https://aur.archlinux.org/packages/perl-b-keywords/

https://aur.archlinux.org/packages/perl-devel-stacktrace/

https://aur.archlinux.org/packages/perl-exception-class/

https://aur.archlinux.org/packages/perl-pod-spell/

https://aur.archlinux.org/packages/perl-task-weaken/

https://aur.archlinux.org/packages/perl-test-object/

https://aur.archlinux.org/packages/perl-test-subcalls/

https://aur.archlinux.org/packages/perl-hook-lexwrap/

https://aur.archlinux.org/packages/perl-ppi/

https://aur.archlinux.org/packages/perl-readonly/

https://aur.archlinux.org/packages/perl-ppix-regexp/

https://aur.archlinux.org/packages/perl-readonly-xs/

https://aur.archlinux.org/packages/perl-ppix-utilities/

https://aur.archlinux.org/packages/perl-string-format/

https://aur.archlinux.org/packages/perl-critic/

https://aur.archlinux.org/packages/perl-test-critic/

https://aur.archlinux.org/packages/perl-set-intspan/

https://aur.archlinux.org/packages/perl-proc-processtable/

https://aur.archlinux.org/packages/gscan2pdf/

Repeat for each package:

```bash
tar -xzf perl-gtk2-ex-simple-list.tar.gz
cd perl-gtk2-ex-simple-list
makepkg -s
sudo pacman -U perl-gtk2-ex-simple-list-0.50-2-any.pkg.tar.xz
sudo pacman -S gocr
sudo pacman -S simple-scan
```

- MS Fonts (download from [AUR](https://aur.archlinux.org/packages/ttf-ms-fonts/))

```bash
cd builds
tar -xzf ttf-ms-fonts.tar.gz
cd ttf-ms-fonts
makepkg -s
sudo pacman -U ttf-ms-fonts-2.0-10-any.pkg.tar.xz
```

- Dropbox

```bash
sudo pacman -S dropbox
```

Also download nautilus support from [AUR](https://aur.archlinux.org/packages/nautilus-dropbox/).

```bash
cd builds
tar -xzf nautilus-dropbox.tar.gz
cd nautilus-dropbox
makepkg -s
sudo pacman -U nautilus-dropbox-1.6.0-1-x86_64.pkg.tar.xz
```

Also installed [this](https://extensions.gnome.org/extension/495/topicons/) GNOME extension.

- Gparted

```bash
sudo pacman -S gparted
```

- GCStar

```bash
sudo pacman -S gcstar
```

To get my collections I recovered the following folder from my /home backup:

> ~/.config/gcstar
> 
> ~/.local/share/gcstar

- Gaupol

```bash
sudo pacman -S gaupol
```

- Aspell PT (download from [AUR](https://aur.archlinux.org/packages/aspell-pt-ao/))

```bash
cd builds
tar -xzf aspell-pt-ao.tar.gz
cd aspell-pt-ao
makepkg -s
sudo pacman -U aspell-pt-ao-20121014-1-x86_64.pkg.tar.xz
```

- Libreoffice

```bash
sudo pacman -S libreoffice
```

- Mysql Workbench (download from [AUR](https://aur.archlinux.org/packages/mysql-workbench-gpl/))

```bash
cd builds
tar -xzf mysql-workbench-gpl.tar.gz
cd mysql-workbench-gpl
makepkg -s
sudo pacman -U mysql-workbench-gpl-5.2.47-1-x86_64.pkg.tar.xz
```

To get my connections I recovered the following folder from my /home backup:

> ~/.mysql

- Google Music Manager (download from [AUR](https://aur.archlinux.org/packages/google-musicmanager/))

```bash
cd builds
tar -xzf google-musicmanager.tar.gz
cd google-musicmanager
makepkg -s
sudo pacman -U google-musicmanager-1.0.65.1341_r0-1-x86_64.pkg.tar.xz
```

- SyncBackup (download from [AUR](https://aur.archlinux.org/packages/syncbackup/))

```bash
cd builds
tar -xzf syncbackup.tar.gz
cd syncbackup
makepkg -s
sudo pacman -U syncbackup-1.1.4-1-x86_64.pkg.tar.xz
```

- youtube-dl

```bash
sudo pacman -S youtube-dl
```

- Duplicity

```bash
sudo pacman -S duplicity 
```

- Nuvola Player (download from [AUR](https://aur.archlinux.org/packages.php?ID=58166), also needs [this](https://aur.archlinux.org/packages/scour/) and [this](https://aur.archlinux.org/packages/gnome-shell-extension-mediaplayer-git/))

```bash
cd builds
tar -xzf scour.tar.gz
cd scour
makepkg -s
sudo pacman -U scour-0.26-1-any.pkg.tar.xz
cd ..
tar -xzf gnome-shell-extension-mediaplayer-git.tar.gz
cd gnome-shell-extension-mediaplayer-git
makepkg -s
sudo pacman -U gnome-shell-extension-mediaplayer-git-388-1-any.pkg.tar.xz
cd ..
tar -xzf nuvolaplayer.tar.gz
cd nuvolaplayer
makepkg -s
sudo pacman -U nuvolaplayer-2.0.3-3-x86_64.pkg.tar.xz
```

Check this [post](/posts/google-music/) for more information about using Nuvola Player with Google Play Music.

- Wine and PlayOnLinux

Activate multilib, by uncommenting the following lines in pacman.conf:

```bash
sudo nano /etc/pacman.conf
```

```text
[multilib]
Include = /etc/pacman.d/mirrorlist
```

```bash
sudo pacman -Syy
sudo pacman -S wine
sudo pacman -S playonlinux
sudo pacman -S lib32-giflib lib32-libpng lib32-libldap \
	lib32-gnutls lib32-lcms lib32-libxml2 lib32-mpg123 openal \
	lib32-openal lib32-v4l-utils lib32-libpulse lib32-alsa-plugins \
	lib32-alsa-lib lib32-libjpeg-turbo lib32-libxcomposite \
	lib32-libxinerama lib32-ncurses libcl lib32-libcl oss \
	lib32-sdl lib32-jack lib32-libsamplerate lib32-speex
```

Check this [post](/posts/install-evernote-in-ubuntu/) on how to install Evernote under Wine.

- Archey

```bash
yaourt -S archey
```