---
title: "Book Haul for January 2022"
slug: "book-haul-2022-01"
author: "Paulo Pereira"
date: 2022-02-01T20:20:00+00:00
lastmod: 2022-02-01T20:20:00+00:00
cover: "/posts/book-hauls/2022-01-book-haul.png"
description: "January book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

January book haul.