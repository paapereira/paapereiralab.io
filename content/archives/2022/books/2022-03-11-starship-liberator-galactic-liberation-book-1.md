---
title: "Starship Liberator (Galactic Liberation Book 1) by David VanDyke and B.V. Larson (2016)"
slug: "starship-liberator-galactic-liberation-book-1"
author: "Paulo Pereira"
date: 2022-03-11T22:00:00+00:00
lastmod: 2022-03-11T22:00:00+00:00
cover: "/posts/books/starship-liberator-galactic-liberation-book-1.jpg"
description: "Finished “Starship Liberator (Galactic Liberation Book 1)” by David VanDyke and B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - David VanDyke
  - B.V. Larson
book series:
  - Galactic Liberation Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - David VanDyke
  - B.V. Larson
  - Galactic Liberation Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Starship Liberator”.
* Author: [David VanDyke](/book-authors/david-vandyke) and [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [December 28th 2016](/book-publication-year/2016)
* Series: [Galactic Liberation](/book-series/galactic-liberation-series) Book 1
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36170204-starship-liberator)
>
> The Hundred Worlds have withstood invasion by the relentless Hok for decades. The human worlds are strong, but the Hok have the resources of a thousand planets behind them, and their fleets attack in endless waves.
> 
> The long war has transformed the Hundred Worlds into heavily fortified star systems. Their economies are geared for military output, and they raise specialized soldiers to save our species. Assault Captain Derek Straker is one such man among many. Genetically sculpted to drive a mech-suit as if he wears a second skin, he must find a path to victory.
> 
> It's a battle in which he'll never admit defeat, but not even Straker knows the dark truth behind this titanic struggle. With Lieutenant Carla Engels piloting, Starship Liberator explores enemy territory in search of answers.