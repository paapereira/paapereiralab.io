---
title: "Books Schedule update"
slug: "book-schedule-november-2024"
author: "Paulo Pereira"
date: 2024-11-01T16:10:00+00:00
lastmod: 2024-11-01T16:10:00+00:00
description: "November Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my November TBR (To Be Read) update for the next months.

* **[Read]** The New Girl (Fear Street #1) by R. L. Stine
* **[Read]** The Surrogate Mother by Freida McFadden
* **[Read]** My Best Friend's Exorcism by Grady Hendrix
* **[Read]** The Curse of the Mummy's Tomb (Goosebumps #5) by R. L. Stine
* **[Read]** Horns by Joe Hill
* **[Read]** I Found Puppets Living in My Apartment Walls by Ben Farthing
* **[Read]** The Surprise Party (Fear Street #2) by R. L. Stine
* **[Reading]** The Spoils of War (The Damned #3) by Alan Dean Foster 
* Harry Potter and the Chamber of Secrets (Harry Potter #2) by J. K. Rowling
* Hawksbill Station by Robert Silverberg
* Small Favor (The Dresden Files #10) by Jim Butcher
* Star Wars: Episode VI: Return of the Jedi by James Kahn
* The Subtle Art of Not Giving a F*ck by Mark Manson
* The Andromeda Evolution (Andromeda #2) by Daniel H. Wilson & Michael Crichton
* Mystery Walk by Robert R. McCammon
* Way Station by Clifford D. Simak
* Aurora Rising (Prefect Dreyfus Emergency #1) by Alastair Reynolds
* The Sicilian (The Godfather #2) by Mario Puzo
* Planet of Exile (Hainish Cycle) by Ursula K. le Guin
* We Can Build You by Philip K. Dick
* A Coming of Age by Timothy Zahn
* The Expert System's Champion (The Expert System's Brother #2) by Adrian Tchaikovsky
* The Way of Kings (The Stormlight Archive #1) by Brandon Sanderson
* Dead Med (Prescription: Murder #1) by Freida McFadden
* The Bullet Journal Method by Ryder Carroll

I also have always an audiobook going too:

* **[Read]** Desperation by Stephen King
* **[Reading]** Not Till We Are Lost (Bobiverse #5) by Dennis E. Taylor
* Nexus by Yuval Noah Harari
* Breakfast at Tiffany's by Truman Capote
* Red Company: Invasion (Red Company #4) by B. V. Larson
* The Richest Man in Babylon by George S. Clason
* Impact Winter #1 by Travis Beacham
* A History of Video Games by Jeremy Parish
* The Sandman: Act III by Neil Gaiman
* Bag of Bones by Stephen King
