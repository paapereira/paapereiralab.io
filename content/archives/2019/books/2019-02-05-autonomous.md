---
title: "Autonomous by Annalee Newitz (2017)"
slug: "autonomous"
author: "Paulo Pereira"
date: 2019-02-05T22:13:00+00:00
lastmod: 2019-02-05T22:13:00+00:00
cover: "/posts/books/autonomous.jpg"
description: "Finished “Autonomous” by Annalee Newitz."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Annalee Newitz
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Annalee Newitz
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/autonomous
---

Finished “Autonomous”.
* Author: [Annalee Newitz](/book-authors/annalee-newitz)
* First Published: [September 19th 2017](/book-publication-year/2017)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35796064-autonomous)
>
> The highly anticipated science fiction debut from the founder of io9!Earth, 2144. Jack is an anti-patent scientist turned drug pirate, traversing the world in a submarine as a pharmaceutical Robin Hood, fabricating cheap scrips for poor people who cant otherwise afford them. But her latest drug hack has left a trail of lethal overdoses as people become addicted to their work, doing repetitive tasks until they become unsafe or insane.Hot on her trail, an unlikely pair: Eliasz, a brooding military agent, and his robotic partner, Paladin. As they race to stop information about the sinister origins of Jacks drug from getting out, they begin to form an uncommonly close bond that neither of them fully understand.And underlying it all is one fundamental question: Is freedom possible in a culture where everything, even people, can be owned?