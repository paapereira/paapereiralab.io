---
title: "Bandwidth"
slug: "bandwidth"
author: "Paulo Pereira"
date: 2009-06-09T13:15:00+01:00
lastmod: 2009-06-09T13:15:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
---

![bandwidth](/posts/2009/2009-06-09-bandwidth/bandwidth.png)

Here's my cable connection bandwith. It's a 8Mb/512Kb connection, so the results aren't bad.

You can check your connection [here](http://speedtest.net/).