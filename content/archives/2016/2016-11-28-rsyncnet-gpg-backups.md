---
title: "GPG encrypted backups to rsync.net"
slug: "rsyncnet-gpg-backups"
author: "Paulo Pereira"
date: 2016-11-28T21:02:25+00:00
lastmod: 2016-11-28T21:02:25+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - rsync.net
  - gpg
---

I use [rsync.net](https://rsync.net) for my cloud/offline backups.

Here’s how to do gpg encrypted backups.

- Mount rsync.net using `sshfs` and create a backup folder 

```bash
mkdir -p /home/your_user/rsync.net/
sshfs 99999@server.rsync.net: /home/your_user/rsync.net/
mkdir -p /home/your_user/rsync.net/backups/
```

- Create and encript a directory

```bash
tar --xattrs -czpvf /tmp/2016-11-28_my_dir.tgz -C /home/your_user/my_dir .
gpg --cipher-algo aes256 -c -o /home/your_user/rsync.net/backups/2016-11-28_my_dir.tgz.gpg /tmp/2016-11-28_my_dir.tgz
```

- Restore and decrypt a directory

```bash
mkdir -p /tmp/extract
cd /tmp/extract
gpg -o- /tmp/2016-11-28_my_dir.tgz.gpg | tar zxvf -
```

- Unmount rsync.net

```bash
fusermount -u /home/your_user/rsync.net/
```