---
title: "Books Schedule update"
slug: "book-schedule-july-2023"
author: "Paulo Pereira"
date: 2023-07-01T17:30:00+01:00
lastmod: 2023-07-01T17:30:00+01:00
description: "July Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my July TBR (To Be Read) update for the next months.

* **[Read]** The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* **[Read]** Elder Race by Adrian Tchaikovsky
* **[Read]** Baal by Robert R. McCammon
* **[Read]** Blood Rites (The Dresden Files 6) by Jim Butcher
* **[Read]** The Tooth Fairy by Graham Joyce
* **[Reading]** Chasm City by Alastair Reynolds
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson
* How It Unfolds (Far Reaches 1) by James S. A. Corey
* The Private Life of Elder Things by Adrian Tchaikovsky & Keris McDonald & Adam Gauntlett
* Bethany's Sin by Robert R. McCammon
* Dead Beat (The Dresden Files 7) by Jim Butcher

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson

I also have always an audiobook going too:

* **[Read]** State of Fear by Michael Crichton
* **[Read]** Jungle World (Undying Mercenaries Book 19) by B.V. Larson
* **[Read]** Digital Minimalism by Cal Newport
* **[Reading]** Insomnia by Stephen King
* Fire Fight (Star Runner 2) by B. V. Larson
* How to Avoid a Climate Disaster by Bill Gates
* Hearts in Atlantis by Stephen King
* Androids and Aliens (Star Runner 3) by B. V. Larson