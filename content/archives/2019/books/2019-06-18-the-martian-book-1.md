---
title: "The Martian (The Martian Book 1) by Andy Weir (2012)"
slug: "the-martian-book-1"
author: "Paulo Pereira"
date: 2019-06-18T22:46:00+01:00
lastmod: 2019-06-18T22:46:00+01:00
cover: "/posts/books/the-martian-book-1.jpg"
description: "Finished “The Martian (The Martian Book 1)” by Andy Weir."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - Andy Weir
book series:
  - The Martian Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Andy Weir
  - The Martian Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/the-martian-book-1
---

Finished “The Martian”.
* Author: [Andy Weir](/book-authors/andy-weir)
* First Published: [September 27th 2012](/book-publication-year/2012)
* Series: [The Martian](/book-series/the-martian-series) Book 1
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/26462167-the-martian)
>
> Six days ago, astronaut Mark Watney became one of the first people to walk on Mars.
>
> Now, he's sure he'll be the first person to die there.
>
> After a dust storm nearly kills him & forces his crew to evacuate while thinking him dead, Mark finds himself stranded & completely alone with no way to even signal Earth that he’s alive—& even if he could get word out, his supplies would be gone long before a rescue could arrive.
>
> Chances are, though, he won't have time to starve to death. The damaged machinery, unforgiving environment or plain-old "human error" are much more likely to kill him first.
>
> But Mark isn't ready to give up yet. Drawing on his ingenuity, his engineering skills—& a relentless, dogged refusal to quit—he steadfastly confronts one seemingly insurmountable obstacle after the next. Will his resourcefulness be enough to overcome the impossible odds against him?