---
title: "All Systems Red (The Murderbot Diaries Book 1) by Martha Wells (2017)"
slug: "all-systems-red-the-murderbot-diaries-book-1"
author: "Paulo Pereira"
date: 2018-11-25T18:06:00+00:00
lastmod: 2018-11-25T18:06:00+00:00
cover: "/posts/books/all-systems-red-the-murderbot-diaries-book-1.jpg"
description: "Finished “All Systems Red (The Murderbot Diaries Book 1)” by Martha Wells."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Martha Wells
book series:
  - The Murderbot Diaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Martha Wells
  - The Murderbot Diaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “All Systems Red”.
* Author: [Martha Wells](/book-authors/martha-wells)
* First Published: [May 2nd 2017](/book-publication-year/2017)
* Series: [The Murderbot Diaries](/book-series/the-murderbot-diaries-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/37433980-all-systems-red)
>
> In a corporate-dominated spacefaring future, planetary missions must be approved and supplied by the Company. Exploratory teams are accompanied by Company-supplied security androids, for their own safety.
>
> But in a society where contracts are awarded to the lowest bidder, safety isn’t a primary concern.
>
> On a distant planet, a team of scientists are conducting surface tests, shadowed by their Company-supplied ‘droid — a self-aware SecUnit that has hacked its own governor module, and refers to itself (though never out loud) as “Murderbot.” Scornful of humans, all it really wants is to be left alone long enough to figure out who it is.
>
> But when a neighboring mission goes dark, it's up to the scientists and their Murderbot to get to the truth.