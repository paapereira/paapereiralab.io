---
title: "NFS file sharing"
slug: "nfs-file-sharing"
author: "Paulo Pereira"
date: 2008-05-01T17:59:00+01:00
lastmod: 2008-05-01T17:59:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
aliases:
  - /posts/nfs-file-sharing/
---

Here's how you share files between two Linux machines, using NFS.

## Installation

- Go to the Synaptic Package Manager
- Find `nfs-kernel-server` and install

## Configuration in machine 1 (where the shared folder is)

- Define the shares you want to provide

```bash
sudo vi /etc/exports
```

- Add your shares to the file

```text
/home/mach1user/Public *(ro,sync)
/home/mach1user/Documents 100.000.00.01(ro,sync,no_subtree_check)
/home/mach1user/Share 100.000.00.01/20(rw,sync,no_subtree_check)
```

- Make your changes be known

```bash
sudo exportfs -a
```

## Configuration in machine 2 (where the shared folder are going to be accessed)

- Create a folder, where the share will be mounted

```bash
mkdir /home/mach2user/HomeNetwork/Public
mkdir /home/mach2user/HomeNetwork/Documents
mkdir /home/mach2user/HomeNetwork/Share
```

- Mount the shares (the IP is from machine 1)

```bash
sudo mount 100.000.0.05:/home/mach1user/Public /home/mach2user/HomeNetwork/Public
sudo mount 100.000.0.05:/home/mach1user/Documents /home/mach2user/HomeNetwork/Documents
sudo mount 100.000.0.05:/home/mach1user/Share /home/mach2user/HomeNetwork/Share
```

- Make the shares available after rebooting

```bash
sudo vi /etc/fstab
```

- Add to the file (the IP is from machine 1)

```text
100.000.0.05:/home/mach1user/Public /home/mach2user/HomeNetwork/Public nfs ro,hard,intr 0 0
100.000.0.05:/home/mach1user/Documents /home/mach2user/HomeNetwork/Documents nfs ro,hard,intr 0 0
100.000.0.05:/home/mach1user/Share /home/mach2user/HomeNetwork/Share nfs ro,hard,intr 0 0
```

And that should do it.