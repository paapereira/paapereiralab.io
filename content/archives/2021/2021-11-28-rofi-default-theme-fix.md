---
title: "Fix theme option after upgrading rofi to 1.7.1"
slug: "rofi-default-theme-fix"
author: "Paulo Pereira"
date: 2021-11-28T11:59:57+00:00
lastmod: 2021-11-28T11:59:57+00:00
description: "Fix theme option after upgrading rofi to 1.7.1."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - rofi
---

Following the latest [1.7.0 rofi update](/posts/fix-theme-rofi/), the latest 1.7.1 version all "broke" my theming.

Basically the `theme` option changed.

To fix I just add to my `~/.config/rofi/config.rasi` file:

Before:

```bash
configuration {
  (...)
  theme: "~/.config/rofi/themes/dmenu.rasi";
  (...)
}
```

After:

```bash
configuration {
  (...)
}

@theme "~/.config/rofi/themes/dmenu.rasi"
```