---
title: "Creating a subversion server in B3 Server"
slug: "creating-subversion-server-in-b3-server"
author: "Paulo Pereira"
date: 2012-06-10T00:37:00+01:00
lastmod: 2012-06-10T00:37:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
  - subversion
---

In order to keep a version control of my home projects, I decided to set up a subversion server in my B3 Server.

I will be using subversion an Apache.

## B3 Server Installation

- Become root
```bash
su
```
- Install subversion and support for Apache
```bash
apt-get install subversion libapache2-svn
```
- Create a base directory for your subversion projects
```bash
mkdir /var/svn
```
- Give permissions for Apache
```bash
chown -R www-data:www-data /var/svn
```
- Configure Apache svn module
```bash
vi /etc/apache2/mods-available/dav_svn.conf
```
- Here's my dav_svn.conf. Be sure to alter SVNParentPath to your subversion projects base directory.
```text
 <Location /svn>

  # Uncomment this to enable the repository
  DAV svn

  SVNParentPath /var/svn
  SVNListParentPath on
  AuthType Basic
  AuthName "Subversion Repository"
  AuthUserFile /etc/apache2/dav_svn.passwd

  # The following three lines allow anonymous read, but make
  # committers authenticate themselves.  It requires the 'authz_user'
  # module (enable it with 'a2enmod').
  #<LimitExcept GET PROPFIND OPTIONS REPORT>
    Require valid-user
    SSLRequireSSL
  #</LimitExcept>

</Location>
```
- Restart Apache
```bash
/etc/init.d/apache2 restart
```
- Create users for subversion
```bash
htpasswd -cm /etc/apache2/dav_svn.passwd *yourusername*
```
-  Create your projects with the right permissions
```bash
svnadmin create /var/svn/yourproject
chown -R www-data:www-data /var/svn/yourproject
```
- To remove projects
```bash
cd /var/svn
rm yourproject -R
```

## Client Installation

- Install subversion
```bash
sudo apt-get install subversion
```
- Connect and sync your projects. This will create a new directory *yourproject* in your home folder.
```bash
svn co https://b3server/svn/yourproject
```
- Optionally you can install a graphical tool like [rabbitvcs](http://rabbitvcs.org/), with nautilus integration
```bash
sudo add-apt-repository ppa:rabbitvcs/ppa
sudo apt-get update
sudo apt-get install rabbitvcs-nautilus3
killall nautilus
```

You can check out the [excito wiki](http://wiki.excito.org/wiki/index.php/Tutorials_and_How-tos/Install_Subversion_with_Apache) for further details.