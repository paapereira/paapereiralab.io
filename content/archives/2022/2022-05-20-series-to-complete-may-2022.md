---
title: "Books Series to complete buying"
slug: "series-to-complete-may-2022"
author: "Paulo Pereira"
date: 2022-05-20T15:18:01+01:00
lastmod: 2022-05-20T15:18:01+01:00
description: "Short list of series I'm yet to complete buying."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Wishlist
---

Just a short to be at hand list of series I'm yet to complete buying. Some I'm currently reading it, others still getting them.

It's not a complete list, but it's meant to help me quickly decide in a birthday/christmas situation.

* Malazan Book of the Fallen by Steven Erikson
* The Book of the New Sun by Gene Wolfe
* The Mistborn Saga by Brandon Sanderson
* The Wheel of Time by Robert Jordan
* Ender's Saga by Orson Scott Card
* The Witcher by Andrzej Sapkowski
* A Song of Ice and Fire by George R. R. Martin
* The Expanse by James S. A. Corey
* Culture by Iain M. Banks
* The First Law Trilogy by Joe Abercrombie
* The Legend of Drizzt by R. A. Salvatore
* Old Man's War by John Scalzi
* The Dark Tower by Stephen King
* The Hitchhiker's Guide to the Galaxy by Douglas Adams
* The Faithful and the Fallen by John Gwynne