---
title: "Cut and Run: A Light-Hearted Dark Comedy by Ben Acker and Ben Blacker (2020)"
slug: "cut-and-run"
author: "Paulo Pereira"
date: 2021-01-07T22:00:00+00:00
lastmod: 2021-01-07T22:00:00+00:00
cover: "/posts/books/cut-and-run.jpg"
description: "Finished “Cut and Run: A Light-Hearted Dark Comedy” by Ben Acker and Ben Blacker."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Ben Acker
  - Ben Blacker
book series:
  - 
book genres:
  - Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Ben Acker
  - Ben Blacker
  - Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Cut and Run: A Light-Hearted Dark Comedy”.
* Author: [Ben Acker](/book-authors/ben-acker) and [Ben Blacker](/book-authors/ben-blacker)
* First Published: [November 17th 2020](/book-publication-year/2020)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/55336871-cut-and-run)
>
> Samantha Dugan and Abe Lally are best friends. They steal kidneys. They have their reasons. 
>
> Samantha is a professional seductress and con artist with a heart of gold. If she talked about her work, she'd tell you she only pilfers organs for medical research and that she only takes one kidney, and she only steals those kidneys from dishonest people. It's hell on her social life. 
>
> Abe is the doctor and Sam's partner in crime. He isn't much of a criminal. Except for the kidney stealing. But he's using that money to fund his research that he anticipates will be able to cure diabetes. So, all for a good cause...? You decide. Of course, it's hell on his social life.