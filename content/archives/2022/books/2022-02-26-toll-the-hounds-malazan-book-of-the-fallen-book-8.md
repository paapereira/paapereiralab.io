---
title: "Toll the Hounds (Malazan Book of the Fallen Book 8) by Steven Erikson (2008)"
slug: "toll-the-hounds-malazan-book-of-the-fallen-book-8"
author: "Paulo Pereira"
date: 2022-02-26T14:00:00+00:00
lastmod: 2022-02-26T14:00:00+00:00
cover: "/posts/books/toll-the-hounds-malazan-book-of-the-fallen-book-8.jpg"
description: "Finished “Toll the Hounds (Malazan Book of the Fallen Book 8)” by Steven Erikson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2008
book authors:
  - Steven Erikson
book series:
  - Malazan Book of the Fallen Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Steven Erikson
  - Malazan Book of the Fallen Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
---

Finished “Toll the Hounds”.
* Author: [Steven Erikson](/book-authors/steven-erikson)
* First Published: [June 30th 2008](/book-publication-year/2008)
* Series: [Malazan Book of the Fallen](/book-series/malazan-book-of-the-fallen-series) Book 8
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8177049-toll-the-hounds)
>
> In Darujhistan, the city of blue fire, it is said that love and death shall arrive dancing. It is summer and the heat is oppressive, but for the small round man in the faded red waistcoat, discomfiture is not just because of the sun. All is not well. Dire portents plague his nights and haunt the city streets like fiends of shadow. Assassins skulk in alleyways, but the quarry has turned and the hunters become the hunted.
> 
> Hidden hands pluck the strings of tyranny like a fell chorus. While the bards sing their tragic tales, somewhere in the distance can be heard the baying of Hounds...And in the distant city of Black Coral, where rules Anomander Rake, Son of Darkness, ancient crimes awaken, intent on revenge. It seems Love and Death are indeed about to arrive...hand in hand, dancing.