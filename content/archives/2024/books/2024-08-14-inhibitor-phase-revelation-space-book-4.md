---
title: "Inhibitor Phase (Revelation Space Book 4) by Alastair Reynolds (2021)"
slug: "inhibitor-phase-revelation-space-book-4"
author: "Paulo Pereira"
date: 2024-08-14T15:30:00+01:00
lastmod: 2024-08-14T15:30:00+01:00
cover: "/posts/books/inhibitor-phase-revelation-space-book-4.jpg"
description: "Finished “Inhibitor Phase (Revelation Space Book 4)” by Alastair Reynolds."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - Alastair Reynolds
book series:
  - Revelation Space Series
  - Revelation Space Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Alastair Reynolds
  - Revelation Space Series
  - Revelation Space Universe
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
aliases:
  - /posts/books/inhibitor-phase-revelation-space-book-4
---

Finished “Inhibitor Phase”.
* Author: [Alastair Reynolds](/book-authors/alastair-reynolds)
* First Published: [October 12, 2021](/book-publication-year/2021)
* Series: [Revelation Space](/book-series/revelation-space-series) Book 4
* [Revelation Space Universe](/book-series/revelation-space-universe)
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/55954564-inhibitor-phase)
>
> For thirty years a tiny band of humans has been sheltering in the caverns of an airless, crater-pocked world called Michaelmas. Beyond their solar system lie the ruins of human interstellar civilization, stalked by a ruthless, infinitely patient cybernetic entity determined to root out the last few bands of survivors. One man has guided the people of Michaelmas through the hardest of times, and given them hope against the Miguel de Ruyter.When a lone human ship blunders into their system, and threatens to lead the wolves to Michaelmas, de Ruyter embarks on a desperate, near-suicide mission to prevent catastrophe. But an encounter with a refugee from the ship—the enigmatic woman who calls herself only Glass—leads to de Ruyter's world being turned upside down.