---
title: "Aurora Rising (Prefect Dreyfus Emergency Book 1) by Alastair Reynolds (2007)"
slug: "aurora-rising-prefect-dreyfus-emergency-book-1"
author: "Paulo Pereira"
date: 2025-01-02T22:00:00+00:00
lastmod: 2025-01-02T22:00:00+00:00
cover: "/posts/books/aurora-rising-prefect-dreyfus-emergency-book-1.jpg"
description: "Finished “Aurora Rising (Prefect Dreyfus Emergency Book 1)” by Alastair Reynolds."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2007
book authors:
  - Alastair Reynolds
book series:
  - Prefect Dreyfus Emergency Series
  - Revelation Space Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Alastair Reynolds
  - Prefect Dreyfus Emergency Series
  - Revelation Space Universe
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/aurora-rising-prefect-dreyfus-emergency-book-1
---

Finished “Aurora Rising”.
* Author: [Alastair Reynolds](/book-authors/alastair-reynolds)
* First Published: [April 12, 2007](/book-publication-year/2007)
* Series: [Prefect Dreyfus Emergency](/book-series/prefect-dreyfus-emergency-series) Book 1
* [Revelation Space Universe](/book-series/revelation-space-universe)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/50329893-aurora-rising)
>
> Tom Dreyfus is a Prefect, a law enforcement officer with the Panoply. His beat is the Glitter Band, that vast swirl of space habitats orbiting the planet Yellowstone, the teeming hub of a human interstellar empire spanning many worlds. His current case: investigating a murderous attack against one of the habitats that leaves nine hundred people dead. But his investigation uncovers something far more serious than mass slaughter-a covert plot by an enigmatic entity who seeks nothing less than total control of the Glitter Band.