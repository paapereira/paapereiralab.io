---
title: "Project Hail Mary by Andy Weir (2021)"
slug: "project-hail-mary"
author: "Paulo Pereira"
date: 2021-06-03T16:00:00+01:00
lastmod: 2021-06-03T16:00:00+01:00
cover: "/posts/books/project-hail-mary.jpg"
description: "Finished “Project Hail Mary” by Andy Weir."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - Andy Weir
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Andy Weir
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/project-hail-mary
---

Finished “Project Hail Mary”.
* Author: [Andy Weir](/book-authors/andy-weir)
* First Published: [May 4th 2021](/book-publication-year/2021)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/56049167-project-hail-mary)
>
> Ryland Grace is the sole survivor on a desperate, last-chance mission - and if he fails, humanity and the earth itself will perish.
>
> Except that right now, he doesn't know that. He can't even remember his own name, let alone the nature of his assignment or how to complete it.
>
> All he knows is that he's been asleep for a very, very long time. And he's just been awakened to find himself millions of miles from home, with nothing but two corpses for company.
>
> His crew-mates dead, his memories fuzzily returning, he realizes that an impossible task now confronts him. Alone on this tiny ship that's been cobbled together by every government and space agency on the planet and hurled into the depths of space, it's up to him to conquer an extinction-level threat to our species.
>
> And thanks to an unexpected ally, he just might have a chance.
>
> Part scientific mystery, part dazzling interstellar journey, 'PROJECT HAIL MARY is a tale of discovery, speculation, and survival to rival 'The Martian' - while taking us to places it never dreamed of going.