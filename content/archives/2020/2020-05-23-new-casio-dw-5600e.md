---
title: "⌚ New Casio G-Shock DW5600-E"
slug: "new-casio-dw-5600e"
author: "Paulo Pereira"
date: 2020-05-23T18:05:55+01:00
lastmod: 2020-05-23T18:05:55+01:00
cover: "/posts/2020/2020-05-23-new-casio-dw-5600e/g-shock-dw-5600e.png"
description: "As an early birthday present I got the best space proved, back from ‘83, [Casio G-Shock DW-5600E](https://www.casio.com/products/watches/g-shock/dw5600e-1v)."
draft: false
toc: false
categories:
  - General
tags:
  - Casio
---

As an early birthday present I got the best space proved, back from ‘83, [Casio G-Shock DW-5600E](https://www.casio.com/products/watches/g-shock/dw5600e-1v).

I wanted a G-Shock since I was a kid. I used a Casio F-91W when I was younger until I got a cellphone in early 2000s. After that my clock was my phone, from my first “dumb” phone the the latest smart-phones and “smart”-watches.

After buying some time ago a F-91W, remembering my 80s and 90s, I now got a super present :-)
