---
title: "Minority Report and Other Stories by Philip K. Dick (2001)"
slug: "minority-report-and-other-stories"
author: "Paulo Pereira"
date: 2024-04-28T15:00:00+01:00
lastmod: 2024-04-28T15:00:00+01:00
cover: "/posts/books/minority-report-and-other-stories.jpg"
description: "Finished “Minority Report and Other Stories” by Philip K. Dick."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2001
book authors:
  - Philip K. Dick
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Philip K. Dick
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Audiobook
---

Finished “Minority Report and Other Stories”.
* Author: [Philip K. Dick](/book-authors/philip-k.-dick)
* First Published: [January 1, 2001](/book-publication-year/2001)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34535832-minority-report-and-other-stories)
>
> •The Minority Report: a special unit that employs those with the power of precognition to prevent crimes proves itself less than reliable...
> •We Can Remember It For You Wholesale: an everyguy's yearning for more exciting "memories" places him in a danger he never could have imagined (basis of the feature film Total Recall)...
> •Paycheck: a mechanic who has no memory of the previous two years of his life finds that a bag of seemingly worthless and unrelated objects can actually unlock the secret of his recent past -- and insure that he has a future...
> •Second Variety: the UN's technological advances to win a global war veer out of control, threatening to destroy all of humankind (basis of the movie Screamers)...
> •The Eyes Have It: a whimsical, laugh-out-loud play on the words of the title.