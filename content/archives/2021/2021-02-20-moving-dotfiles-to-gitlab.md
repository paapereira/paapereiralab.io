---
title: "Moving my dotfiles repository to Gitlab"
slug: "moving-dotfiles-to-gitlab"
author: "Paulo Pereira"
date: 2021-02-20T17:19:31+00:00
lastmod: 2021-02-20T17:19:31+00:00
description: "I've been using [Keybase](https://keybase.io/) to [manage my dotfiles](/posts/manage-dotfiles/) with git. Do give it a read to learn how I'm doing it.\n

This is just how I moved the git server-side from Keybase to [Gitlab](https://gitlab.com/)."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Gitlab
---

I've been using [Keybase](https://keybase.io/) to [manage my dotfiles](/posts/manage-dotfiles/) with git. Do give it a read to learn how I'm doing it.

This is just how I moved the git server-side from Keybase to [Gitlab](https://gitlab.com/).

First of all create a new empty repository in Gitlab. I've created a 'dotfiles' repository.

```bash
cd ~
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
dotfiles fetch origin
dotfiles remote add new-origin git@gitlab.com:your_username/dotfiles.git
dotfiles push --all new-origin
dotfiles remote rm origin
dotfiles remote rename new-origin origin
dotfiles add your_dotfile
dotfiles commit -m "Updated configs"
dotfiles push --set-upstream origin master
```