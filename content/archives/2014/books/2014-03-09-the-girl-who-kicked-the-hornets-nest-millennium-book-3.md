---
title: "The Girl Who Kicked the Hornet's Nest (Millennium Book 3) by Stieg Larsson (2007)"
slug: "the-girl-who-kicked-the-hornets-nest-millennium-book-3"
author: "Paulo Pereira"
date: 2014-03-09T23:38:00+00:00
lastmod: 2014-03-09T23:38:00+00:00
cover: "/posts/books/the-girl-who-kicked-the-hornets-nest-millennium-book-3.jpg"
description: "Finished “The Girl Who Kicked the Hornet's Nest (Millennium Book 3)” by Stieg Larsson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2007
book authors:
  - Stieg Larsson
book series:
  - Millennium Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stieg Larsson
  - Millennium Series
  - Fiction
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “The Girl Who Kicked the Hornet's Nest”.
* Author: [Stieg Larsson](/book-authors/stieg-larsson)
* First Published: [May 2007](/book-publication-year/2007)
* Series: [Millennium](/book-series/millennium-series) Book 3
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/7046116-the-girl-who-kicked-the-hornet-s-nest)
>
> Lisbeth Salander lies in Intensive Care with a bullet lodged in her head. She will face trial for three murders and one attempted murder on her release.
>
> With the help of journalist Mikael Blomkvist, Salander must not only prove her innocence, but identify and denounce the corrupt politicians that have allowed the vulnerable to become victims of abuse and violence. Salander is now ready to fight back.