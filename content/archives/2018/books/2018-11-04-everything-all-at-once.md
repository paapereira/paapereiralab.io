---
title: "Everything All at Once by Bill Nye (2017)"
slug: "everything-all-at-once"
author: "Paulo Pereira"
date: 2018-11-04T22:03:00+00:00
lastmod: 2018-11-04T22:03:00+00:00
cover: "/posts/books/everything-all-at-once.jpg"
description: "Finished “Everything All at Once: How to Unleash Your Inner Nerd, Tap into Radical Curiosity and Solve Any Problem” by Bill Nye."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Bill Nye
book series:
  - 
book genres:
  - Nonfiction
  - Biography
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Bill Nye
  - Nonfiction
  - Biography
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “Everything All at Once: How to Unleash Your Inner Nerd, Tap into Radical Curiosity and Solve Any Problem”.
* Author: [Bill Nye](/book-authors/bill-nye)
* First Published: [July 11th 2017](/book-publication-year/2017)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/38047509-everything-all-at-once)
>
> Bill Nye has been the public face of science for more than 20 years. In Everything All At Once, the New York Times bestselling author issues a call to arms meant to rouse everyone to become the change they want to see in the world. Whether addressing global warming, social change, or personal success, there are certain strategies that always get results: looking at the world with radical curiosity, being driven by a desire for a better future, and being willing to take the actions needed to make change a reality.
>
> With his signature humor and storytelling, Bill shares how he has developed specific techniques for what he calls the "everything all at once" lifestyle, from his childhood scouting adventures and his career at aerospace companies to the development of his famous Science Guy television show. This approach requires the nerd mindset: a way of thinking that leaves no stone unturned. It's about learning to solve problems through exploration, trial and error, and sheer creativity. Problem solving is a skill that anyone can harness to create change, and Bill Nye is here to show us how to sort out problems, recognize solutions, and join him in changing the world.