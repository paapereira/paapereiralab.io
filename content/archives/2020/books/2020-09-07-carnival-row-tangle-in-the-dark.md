---
title: "Carnival Row: Tangle in the Dark by Stephanie K. Smith (2019)"
slug: "carnival-row-tangle-in-the-dark"
author: "Paulo Pereira"
date: 2020-09-07T22:16:00+01:00
lastmod: 2020-09-07T22:16:00+01:00
cover: "/posts/books/carnival-row-tangle-in-the-dark.png"
description: "Finished “Carnival Row: Tangle in the Dark” by Stephanie K. Smith."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Stephanie K. Smith
book series:
  - 
book genres:
  - Fiction
  - Fantasy
book scores:
  - 3/10 Books
tags:
  - Book Log
  - Stephanie K. Smith
  - Fiction
  - Fantasy
  - 3/10 Books
  - Audiobook
---

Finished “Carnival Row: Tangle in the Dark”.
* Author: [Stephanie K. Smith](/book-authors/stephanie-k.-smith)
* First Published: [October 3rd 2019](/book-publication-year/2019)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/49438946-carnival-row)
>
> Tourmaline Larou lives an idyllic life of learning by day and partying by night. An aspiring poet, her future promises nothing less than brilliance.
>
> Then, Vignette Stonemoss walks through the door, and Tourmaline’s world is upended.
>
> As she struggles to understand the effect this stranger has on her, Tourmaline and her fellow fae face a looming threat from the human world. War is on the horizon, and their very existence is at stake.
>
> And Tourmaline will discover whether love will save her - or destroy her.
>
> Tangle in the Dark is set in the world of the Amazon Original series Carnival Row, created by Travis Beacham and René Echevarria. Carnival Row is available now only on Amazon Prime Video.