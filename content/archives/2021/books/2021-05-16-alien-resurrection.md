---
title: "Alien Resurrection by A.C. Crispin (1997)"
slug: "alien-resurrection"
author: "Paulo Pereira"
date: 2021-05-16T21:00:00+01:00
lastmod: 2021-05-16T21:00:00+01:00
cover: "/posts/books/alien-resurrection.jpg"
description: "Finished “Alien Resurrection” by A.C. Crispin."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - A.C. Crispin
book series:
  - Alien Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - A.C. Crispin
  - Alien Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Alien Resurrection”.
* Author: [A.C. Crispin](/book-authors/a.c.-crispin)
* First Published: [January 1st 1997](/book-publication-year/1990s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28163484-alien-resurrection)
>
> At the farthest reaches of the solar system, aboard orbiting space station Auriga, the unthinkable has happened. Ripley awakes. Her last memory is of her own fiery death on the prison colony Fiorina 361. And yet she is alive. Her questions grow as she notices her body stronger and fiercer than she ever remembers. And she confronts the most terrifying threat of all--that she is not alone in her salvation from death's grasp. To combat the incalculable alien menace, she teams up with a renegade band of space smugglers.