---
title: "Red Company: Contact (Red Company Book 3) by B.V. Larson (2023)"
slug: "red-company-contact-red-company-book-3"
author: "Paulo Pereira"
date: 2024-05-12T15:00:00+01:00
lastmod: 2024-05-12T15:00:00+01:00
cover: "/posts/books/red-company-contact-red-company-book-3.jpg"
description: "Finished “Red Company: Contact (Red Company Book 3)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - B.V. Larson
book series:
  - Red Company Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Red Company Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Red Company: Contact”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [June 30, 2023](/book-publication-year/2023)
* Series: [Red Company](/book-series/red-company-series) Book 3
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/141237065-contact)
>
> Everyone wants a piece of the newly discovered alien tech, and they’re willing to fight over it. As tensions rise between Earth’s various factions, and the mining vessel Borag is refitted for war. She becomes a key player in a brewing conflict that threatens to shatter the fragile balance of power in the Solar System.
> 
> Sergeant Devin Starn of Red Company, the marine attachment stationed aboard Borag , heads into deep space once again. Starn and his squad must navigate a treacherous web of deceit and hidden agendas, defending their ship from increasingly powerful enemies.