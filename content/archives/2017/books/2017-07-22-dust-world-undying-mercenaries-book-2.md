---
title: "Dust World (Undying Mercenaries Book 2) by B.V. Larson (2014)"
slug: "dust-world-undying-mercenaries-book-2"
author: "Paulo Pereira"
date: 2017-07-22T22:43:00+01:00
lastmod: 2017-07-22T22:43:00+01:00
cover: "/posts/books/dust-world-undying-mercenaries-book-2.jpg"
description: "Finished “Dust World (Undying Mercenaries Book 2)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Dust World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [March 23rd 2014](/book-publication-year/2014)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/22603197-dust-world)
>
> The Galactics arrived with their Battle fleet in 2052. Rather than being exterminated under a barrage of hell-burners, Earth joined a vast Empire that spans the Milky Way. Our only worthwhile trade goods are our infamous mercenary legions, elite troops we sell to the highest alien bidder.
>
> In 2122 a lost colony expedition contacts Earth, surprising our government. Colonization is against Galactic Law, and Legion Varus is dispatched to the system to handle the situation. Earth gave them sealed orders, but Earth is 35 lightyears away. The Legion commanders have a secret plan of their own. And then there's James McGill, who was never too good at listening to authority in the first place....
>
> In Dust World, book two of the Undying Mercenaries Series, McGill is promoted to Specialist and sent to a frontier planet outside the Empire. Earth's status within the Empire will never be the same.