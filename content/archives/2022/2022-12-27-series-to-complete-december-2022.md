---
title: "Books Series to complete buying"
slug: "series-to-complete-december-2022"
author: "Paulo Pereira"
date: 2022-12-27T14:00:00+00:00
lastmod: 2022-12-27T14:00:00+00:00
description: "Short list of series I'm yet to complete buying."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Wishlist
---

December update for my short list of series to complete buying.

* The Expanse short stories by James S. A. Corey
* The Dark Tower by Stephen King
* Red Rising by Pierce Brown
* The Hitchhiker's Guide to the Galaxy by Douglas Adams
* Terra Ignota by Ada Palmer
* The Final Architecture by Adrian Tchaikovsky
* Mistborn by Brandon Sanderson
* Culture by Iain M. Banks
* The Commonwealth Saga by Peter F. Hamilton
* The Night's Dawn by Peter F. Hamilton
* The Legend of Drizzt by R. A. Salvatore
* The Elric Saga by Michael Moorcock