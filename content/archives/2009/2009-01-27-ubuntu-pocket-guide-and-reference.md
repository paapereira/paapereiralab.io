---
title: "Ubuntu Pocket Guide and Reference"
slug: "ubuntu-pocket-guide-and-reference"
author: "Paulo Pereira"
date: 2009-01-27T19:42:00+00:00
lastmod: 2009-01-27T19:42:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

Great book about Ubuntu. Ideal if you are new to Linux and Ubuntu.

http://www.ubuntupocketguide.com/

You can [download](http://www.ubuntupocketguide.com/download.html) the PDF version for free or [buy it](http://www.amazon.com/gp/product/1440478295?ie=UTF8&tag=beginningubun-20&link_code=as3&camp=211189&creative=373489&creativeASIN=1440478295) online.

![ubuntu-badge](/posts/2009/2009-01-27-ubuntu-pocket-guide-and-reference/ubuntu-badge-big.gif)