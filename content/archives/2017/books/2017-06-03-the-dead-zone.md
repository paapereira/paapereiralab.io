---
title: "The Dead Zone by Stephen King (1979)"
slug: "the-dead-zone"
author: "Paulo Pereira"
date: 2017-06-03T22:31:00+00:00
lastmod: 2017-06-03T22:31:00+00:00
cover: "/posts/books/the-dead-zone.jpg"
description: "Finished “The Dead Zone” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - 8/10 Books
  - Audiobook
---

Finished “The Dead Zone”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [August 30th 1979](/book-publication-year/1970s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34960542-the-dead-zone)
>
> Never before on audio! A number-one national best seller about a man who wakes up from a five-year coma able to see people's futures and the terrible fate awaiting mankind in The Dead Zone - a "compulsive page-turner".
>
> Johnny Smith awakens from a five-year coma after his car accident and discovers that he can see people's futures and pasts when he touches them. Many consider his talent a gift; Johnny feels cursed. His fiancée married another man during his coma, and people clamor for him to solve their problems.
>
> When Johnny has a disturbing vision after he shakes the hand of an ambitious and amoral politician, he must decide if he should take drastic action to change the future. The Dead Zone is a "faultlessly paced...continuously engrossing" (Los Angeles Times) novel of second sight.