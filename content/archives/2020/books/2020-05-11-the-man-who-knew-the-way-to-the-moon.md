---
title: "The Man Who Knew the Way to the Moon by Todd Zwillich (2019)"
slug: "the-man-who-knew-the-way-to-the-moon"
author: "Paulo Pereira"
date: 2020-05-11T22:52:00+01:00
lastmod: 2020-05-11T22:52:00+01:00
cover: "/posts/books/the-man-who-knew-the-way-to-the-moon.png"
description: "Finished “The Man Who Knew the Way to the Moon” by Todd Zwillich."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Todd Zwillich
book series:
  - 
book genres:
  - Nonfiction
  - History
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Todd Zwillich
  - Nonfiction
  - History
  - 5/10 Books
  - Audiobook
---

Finished “The Man Who Knew the Way to the Moon”.
* Author: [Todd Zwillich](/book-authors/todd-zwillich)
* First Published: [July 4th 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/46805255-the-man-who-knew-the-way-to-the-moon)
>
> The story of John C. Houbolt, an unsung hero of Apollo 11 and the man who showed NASA how to put America on the moon.
>
> Without John C. Houbolt, a junior engineer at NASA, Apollo 11 would never have made it to the moon.
>
> Top NASA engineers on the project, including Werner Von Braun, strongly advocated for a single, huge spacecraft to travel to the moon, land, and return to Earth. It's the scenario used in 1950s cartoons and horror movies about traveling to outer space.
>
> Houbolt had another idea: Lunar Orbit Rendezvous. LOR would link two spacecraft in orbit while the crafts were travelling at 17,000 miles per hour. His plan was ridiculed and considered unthinkable. But this junior engineer was irrepressible. He stood by his concept, fired off memos to executives, and argued that LOR was the only way to success.
>
> For the 50th anniversary of Apollo 11, hear the untold story of the man who helped fulfill Kennedy’s challenge to reach the moon and begin exploring the final frontier.