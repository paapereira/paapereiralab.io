---
title: "Books Schedule update"
slug: "book-schedule-december-2023"
author: "Paulo Pereira"
date: 2023-12-01T13:15:00+00:00
lastmod: 2023-12-01T13:15:00+00:00
description: "December Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my December TBR (To Be Read) update for the next months.

* **[Read]** The Halloween Tree by Ray Bradbury
* **[Read]** Just Out of Jupiter's Reach (Far Reaches 5) by Nnedi Okorafor
* **[Read]** Slow Time Between the Stars (Far Reaches 6) by John Scalzi
* **[Read]** Dead Beat (The Dresden Files 7) by Jim Butcher
* **[Read]** Bethany's Sin by Robert R. McCammon
* **[Reading]** Diamond Dogs, Turquoise Days by Alastair Reynolds
* Wool (Silo 1) by Hugh Howey
* Pirate Latitudes by Michael Crichton
* The Hero of Ages (The Mistborn Saga 3) by Brandon Sanderson
* A Call to Arms (The Damned 1) by Alan Dean Foster
* Precious Little Things by Adrian Tchaikovsky
* Star Wars Episode IV: A New Hope by George Lucas
* Proven Guilty (The Dresden Files 8) by Jim Butcher
* The Night Boat by Robert R. McCammon
* Tau Zero by Poul Anderson
* Supernova Era by Cixin Liu

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson

I also have always an audiobook going too:

* **[Read]** Rose Madder by Stephen King
* **[Read]** Red Company: First Strike! (Red Company 1) by B. V. Larson
* **[Reading]** Parallel Worlds by Michio Kaku
* Crystal World (Undying Mercenaries 20) by B. V. Larson
* Who Goes There? by John W. Campbell Jr.
* Accessory to War by Neil Degrasse Tyson
* The Regulators by Stephen King/Richard Bachman
* Red Company: Discovery (Red Company 2) by B. V. Larson