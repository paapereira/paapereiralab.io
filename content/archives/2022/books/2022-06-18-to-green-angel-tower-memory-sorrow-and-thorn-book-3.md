---
title: "To Green Angel Tower (Memory, Sorrow, and Thorn Book 3) by Tad Williams (1993)"
slug: "to-green-angel-tower-memory-sorrow-and-thorn-book-3"
author: "Paulo Pereira"
date: 2022-06-18T18:30:00+01:00
lastmod: 2022-06-18T18:30:00+01:00
cover: "/posts/books/to-green-angel-tower-memory-sorrow-and-thorn-book-3.jpg"
description: "Finished “To Green Angel Tower (Memory, Sorrow, and Thorn Book 3)” by Tad Williams."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Tad Williams
book series:
  - Memory, Sorrow, and Thorn Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Tad Williams
  - Memory, Sorrow, and Thorn Series
  - Fiction
  - Fantasy
  - 10/10 Books
  - Ebook
---

Finished “To Green Angel Tower”.
* Author: [Tad Williams](/book-authors/tad-williams)
* First Published: [March 1993](/book-publication-year/1990s)
* Series: [Memory, Sorrow, and Thorn](/book-series/memory-sorrow-and-thorn-series) Book 3
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44141054-to-green-angel-tower)
>
> The evil minions of the undead Sithi Storm King are beginning their final preparations for the kingdom-shattering culmination of their dark sorceries, drawing King Elias ever deeper into their nightmarish, spell-spun world.
> 
> As the Storm King’s power grows and the boundaries of time begin to blur, the loyal allies of Prince Josua struggle to rally their forces at the Stone of Farewell. There, too, Simon and the surviving members of the League of the Scroll have gathered for a desperate attempt to unravel mysteries from the forgotten past.
> 
> For if the League can reclaim these age-old secrets of magic long-buried beneath the dusts of time, they may be able to reveal to Josua and his army the only means of striking down the unslayable foe....