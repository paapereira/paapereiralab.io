---
title: "Upgrading to Ubuntu 11.10"
slug: "upgrading-to-ubuntu-1110"
author: "Paulo Pereira"
date: 2011-10-15T23:06:00+01:00
lastmod: 2011-10-15T23:06:00+01:00
draft: false
toc: true
categories:
  - Apps
tags:
  - Linux
  - Ubuntu
  - Ubuntu 11.10
  - Oneiric Ocelot
aliases:
  - /posts/upgrading-to-ubuntu-1110/
---

[Ubuntu 11.10](http://www.ubuntu.com/download) came out last Thursday. Codename: Oneiric Ocelot.

I will be upgrading using the "Alternate CD/DVD upgrade".
This way I don't depend on Ubuntu servers during the upgrade.

## Upgrading

![Ubuntu 11.10](/posts/2011/2011-10-15-upgrading-to-ubuntu-1110/ubuntu.png)

- Backup, backup!
- Be sure you have the latest updates prior to this new release:

```bash
sudo apt-get update
sudo apt-get upgrade
```

- [Download](http://www.ubuntu.com/download/ubuntu/alternative-download) the alternate iso image. In my case was the 64bit version (*ubuntu-11.10-alternate-amd64.iso*).
- Check the [hash](https://help.ubuntu.com/community/UbuntuHashes) of the downloaded file:

```bash
md5sum ubuntu-11.10-alternate-amd64.iso
```

- Compare the output with the Ubuntu [hash list](https://help.ubuntu.com/community/UbuntuHashes):

> 5e427f31e6b10315ada74094e8d5d483  ubuntu-11.10-alternate-amd64.iso

- Mount the iso file as a cdrom:

```bash
sudo mkdir -p /media/cdrom
sudo mount -o loop ~/Desktop/ubuntu-11.10-alternate-amd64.iso /media/cdrom
```

- Run the installer:

```bash
gksu "sh /media/cdrom/cdromupgrade"
```

You can find more on how to upgrade to Oneiric in [Ubuntu website](https://help.ubuntu.com/community/OneiricUpgrades).

## Problems upgrading

In both my machine and my girlfriend's the upgrade failed. Something to do with [LightDM](http://www.freedesktop.org/wiki/Software/LightDM).

I believe the problem was the LightDM installation we both did in Ubuntu 11.04 that never worked.

Basically I can't get past the boot process and Ubuntu won't start.

I could have tried and solve this, but I just did a fresh install instead. New post coming.