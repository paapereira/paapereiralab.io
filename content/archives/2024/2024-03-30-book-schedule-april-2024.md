---
title: "Books Schedule update"
slug: "book-schedule-april-2024"
author: "Paulo Pereira"
date: 2024-03-30T14:00:00+00:00
lastmod: 2024-03-30T14:00:00+00:00
description: "April Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my April TBR (To Be Read) update for the next months.

* **[Read]** Micro by Michael Crichton
* **[Read]** Supernova Era by Cixin Liu
* **[Read]** Cycle of the Werewolf by Stephen King
* **[Reading]** Galactic North by Alastair Reynolds
* Made Things by Adrian Tchaikovsky
* Shift (Silo #2) by Hugh Howey
* Warbreaker by Brandon Sanderson
* Nightflyers & Other Stories by George R. R. Martin
* The False Mirror (The Damned #2) by Alan Dean Foster
* Star Wars: Episode V: The Empire Strikes Back by Donald F. Glut
* Harry Potter and the Sorcerer's Stone (Harry Potter #1) by J. K. Rowling
* Stranger in a Strange Land by Robert A. Heinlein
* White Night (The Dresden Files #9) by Jim Butcher
* Dragon Teeth by Michael Crichton
* They Thirst by Robert R. McCammon
* Snow Crash by Neal Stephenson
* Inhibitor Phase (Revelation Space #4) by Alastair Reynolds
* Lord of the Flies by William Golding
* Rocannon's World (Hainish Cycle) by Ursula K. le Guin

I also have always an audiobook going too:

* **[Read]** The Universe in a Nutshell by Stephen Hawking
* **[Read]** The Green Mile by Stephen King
* **[Reading]** Total Recall: My Unbelievably True Life Story by Arnold Schwarzenegger
* Minority Report and Other Stories by Philip K. Dick
* The Compound Effect by Darren Hardy
* Red Company: Contact (Red Company #3) by B. V. Larson
* Super Mario: How Nintendo Conquered America by Jeff Ryan
* The Showman by Simon Shuster
* The Sandman: Act II by Neil Gaiman
* Breakfast at Tiffany's by Truman Capote
