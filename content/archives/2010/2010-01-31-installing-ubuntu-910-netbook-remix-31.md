---
title: "Installing Ubuntu 9.10 Netbook Remix (Asus Eee 1000HE)"
slug: "installing-ubuntu-910-netbook-remix_31"
author: "Paulo Pereira"
date: 2010-01-31T22:33:00+00:00
lastmod: 2010-01-31T22:33:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 9.10
  - Karmic Koala
  - Netbook Remix
  - Asus Eee 1000HE
---

After installing Karmic in my [desktop](/posts/installing-ubuntu-910-karmic-koala-on-the-desktop/) and [laptop](/posts/installing-ubuntu-910-karmic-koala-64-bits-hp-pavilion-dv5-1/), its time for my Asus Eee 1000HE.

The eee don't have an optical drive, so we need to install it using an usb drive.

I will keep the dual booting with Windows XP and Ubuntu. The Eee is my only Windows machine right now.

## Download and "Burn" a USB Drive

- Go to Ubuntu Netbook Remix [Download Page](http://www.ubuntu.com/getubuntu/download-netbook) and download Ubuntu. 
- Check the downloaded file hash. The output must be the same as you see in the [Ubuntu Hashes Page](https://help.ubuntu.com/community/UbuntuHashes).

```bash
md5sum ubuntu-9.10-netbook-remix-i386.img
```

- [Create a bootable usb drive from the downloaded image](https://help.ubuntu.com/community/Installation/FromImgFiles). I used the USB Startup Disk Creator (*System > Administration > USB Startup Disk Creator*) in my karmic desktop.

## Install from usb

It's easy. Boot from the usb drive (check the BIOS) and install like any other Ubuntu release.

This is a [tested machine](https://wiki.ubuntu.com/HardwareSupport/Machines/Netbooks).

## Hard Drive Partition Layout

```text
/dev/sda
   /dev/sda1    ntfs    WinXP     20 GB    -- primary
   /dev/sda2    ext3   /        9.54 GB    -- primary
   /dev/sda3   swap          1.91 GB    -- extended
   /dev/sda4                            -- extended
   /dev/sda5   ext3     /home      9.54 GB
   /dev/sda6    ntfs    tunes      40 GB
   /dev/sda7   ext3   /storage   9.54 GB
```

## Post-Install: Software Sources

Added the following:

```text
deb http://ppa.launchpad.net/do-core/ppa/ubuntu jaunty main
deb http://linux.getdropbox.com/ubuntu jaunty main
```

## Post-install: Extra Software

- Dropbox (`sudo apt-get install nautilus-dropbox`)
- Gnome-Do (`sudo apt-get install gnome-do`)
- [Eee-Control](http://greg.geekmind.org/eee-control/) (can't get it to work!)
- Powertop (`sudo apt-get install powertop`)
- [Firefox Xmarks add-on](http://www.xmarks.com/)
- Gnucash (`sudo apt-get install gnucash`)
- Aspell pt-pt (`sudo apt-get install aspell-pt-pt`)
- Pidgin
- VLC
- RAR Support (`sudo apt-get install rar unrar`)

Everything worked out of the box, including wireless :-)

If you have more tips about using or optimizing the eee please contact me or post a comment :-)