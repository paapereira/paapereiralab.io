---
title: "Red Rising (Red Rising Saga Book 1) by Pierce Brown (2014)"
slug: "red-rising-red-rising-saga-book-1"
author: "Paulo Pereira"
date: 2021-07-10T21:00:00+01:00
lastmod: 2021-07-10T21:00:00+01:00
cover: "/posts/books/red-rising-red-rising-saga-book-1.jpg"
description: "Finished “Red Rising (Red Rising Saga Book 1)” by Pierce Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - Pierce Brown
book series:
  - Red Rising Saga Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Pierce Brown
  - Red Rising Saga Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
aliases:
  - /posts/books/red-rising-red-rising-saga-book-1
---

Finished “Red Rising”.
* Author: [Pierce Brown](/book-authors/pierce-brown)
* First Published: [January 28th 2014](/book-publication-year/2014)
* Series: [Red Rising Saga](/book-series/red-rising-saga-series) Book 1
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34873380-red-rising)
>
> Darrow is a Red, a member of the lowest caste in the color-coded society of the future. Like his fellow Reds, he works all day, believing that he and his people are making the surface of Mars livable for future generations.
>
> Yet he spends his life willingly, knowing that his blood and sweat will one day result in a better world for his children.
>
> But Darrow and his kind have been betrayed. Soon he discovers that humanity already reached the surface generations ago. Vast cities and sprawling parks spread across the planet. Darrow—and Reds like him—are nothing more than slaves to a decadent ruling class.
>
> Inspired by a longing for justice, and driven by the memory of lost love, Darrow sacrifices everything to infiltrate the legendary Institute, a proving ground for the dominant Gold caste, where the next generation of humanity's overlords struggle for power. He will be forced to compete for his life and the very future of civilization against the best and most brutal of Society's ruling class. There, he will stop at nothing to bring down his enemies... even if it means he has to become one of them to do so.