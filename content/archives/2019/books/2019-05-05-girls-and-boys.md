---
title: "Girls & Boys by Dennis Kelly (2018)"
slug: "girls-and-boys"
author: "Paulo Pereira"
date: 2019-05-05T22:26:00+01:00
lastmod: 2019-05-05T22:26:00+01:00
cover: "/posts/books/girls-and-boys.jpg"
description: "Finished “Girls and Boys” by Dennis Kelly."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Dennis Kelly
book series:
  - 
book genres:
  - Fiction
book scores:
  - 3/10 Books
tags:
  - Book Log
  - Dennis Kelly
  - Fiction
  - 3/10 Books
  - Audiobook
---

Finished “Girls & Boys”.
* Author: [Dennis Kelly](/book-authors/dennis-kelly)
* First Published: [2018](/book-publication-year/2018)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40817977-girls-boys)
>
> They met at an airport and fell for each other. But in time, they would meet their fate as it all falls apart.
>
> A pulse-pounding new play from Tony Award-winning® playwright Dennis Kelly takes you on a journey that is at once hilarious, gripping, and heartbreaking. This world-premiere production starring Carey Mulligan (The Great Gatsby, An Education) is available exclusively on Audible after a celebrated run at the Royal Court Theatre in London and off-Broadway at the Minetta Lane Theatre.