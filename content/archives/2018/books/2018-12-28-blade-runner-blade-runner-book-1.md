---
title: "Blade Runner (Blade Runner Book 1) by Philip K. Dick (1968)"
slug: "blade-runner-blade-runner-book-1"
author: "Paulo Pereira"
date: 2018-12-28T23:04:00+00:00
lastmod: 2018-12-28T23:04:00+00:00
cover: "/posts/books/blade-runner-blade-runner-book-1.jpg"
description: "Finished “Blade Runner (Blade Runner Book 1)” by Philip K. Dick."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Philip K. Dick
book series:
  - Blade Runner Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Philip K. Dick
  - Blade Runner Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/blade-runner-blade-runner-book-1
---

Finished “Blade Runner (aka Do Androids Dream of Electric Sheep?)”.
* Author: [Philip K. Dick](/book-authors/philip-k.-dick)
* First Published: [1968](/book-publication-year/1960s)
* Series: [Blade Runner](/book-series/blade-runner-series) Book 1
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/26004332-blade-runner)
>
> A final, apocalyptic, world war has killed millions, driving entire species into extinction and sending the majority of mankind off-planet. Those who remain, venerate all remaining examples of life, and owning an animal of your own is both a symbol of status and a necessity. For those who can't afford an authentic animal, companies build incredibly realistic simulacrae: horses, birds, cats, sheep . . . even humans.