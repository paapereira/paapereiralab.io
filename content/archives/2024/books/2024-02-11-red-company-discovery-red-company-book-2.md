---
title: "Red Company: Discovery (Red Company Book 2) by B.V. Larson (2023)"
slug: "red-company-discovery-red-company-book-2"
author: "Paulo Pereira"
date: 2024-02-11T18:00:00+00:00
lastmod: 2024-02-11T18:00:00+00:00
cover: "/posts/books/red-company-discovery-red-company-book-2.jpg"
description: "Finished “Red Company: Discovery (Red Company Book 2)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - B.V. Larson
book series:
  - Red Company Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Red Company Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Red Company: Discovery”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [June 25, 2023](/book-publication-year/2023)
* Series: [Red Company](/book-series/red-company-series) Book 2
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/181082313-red-company)
>
> There’s a land-rush on exploiting alien tech. Pirates, claim-jumpers, cyborgs, and bureaucrats all must be defeated to keep Borag flying.
> 
> Corporal Devin Starn is working his way up through the ranks of Red Company. Aboard Borag , a mining ship based out of Mars City, Starn discovers more about the aliens who preceded us. Their ruins and strange technologies deepen the mystery of their ancient downfall.