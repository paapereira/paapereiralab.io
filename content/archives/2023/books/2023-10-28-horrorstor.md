---
title: "Horrorstör by Grady Hendrix (2014)"
slug: "horrorstor"
author: "Paulo Pereira"
date: 2023-10-28T21:00:00+01:00
lastmod: 2023-10-28T21:00:00+01:00
cover: "/posts/books/horrorstor.jpg"
description: "Finished “Horrorstör” by Grady Hendrix."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - Grady Hendrix
book series:
  - 
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Grady Hendrix
  - Fiction
  - Horror
  - 7/10 Books
  - Ebook
---

Finished “Horrorstör”.
* Author: [Grady Hendrix](/book-authors/grady-hendrix)
* First Published: [September 23, 2014](/book-publication-year/2014)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/22729262-horrorst-r)
>
> Something strange is happening at the Orsk furniture superstore in Cleveland, Ohio. Every morning, employees arrive to find broken Kjerring bookshelves, shattered Glans water goblets, and smashed Liripip wardrobes. Sales are down, security cameras reveal nothing, and store managers are panicking.
> 
> To unravel the mystery, three employees volunteer to work a nine-hour dusk-till-dawn shift. In the dead of the night, they’ll patrol the empty showroom floor, investigate strange sights and sounds, and encounter horrors that defy the imagination.