---
title: "Let the Right One In by John Ajvide Lindqvist (2004)"
slug: "let-the-right-one-in"
author: "Paulo Pereira"
date: 2022-10-12T19:00:00+01:00
lastmod: 2022-10-12T19:00:00+01:00
cover: "/posts/books/let-the-right-one-in.jpg"
description: "Finished “Let the Right One In” by John Ajvide Lindqvist."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2004
book authors:
  - John Ajvide Lindqvist
book series:
  - 
book genres:
  - Fiction
  - Horror
book scores:
  - 9/10 Books
tags:
  - Book Log
  - John Ajvide Lindqvist
  - Fiction
  - Horror
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/let-the-right-one-in
---

Finished “Let the Right One In”.
* Author: [John Ajvide Lindqvist](/book-authors/john-ajvide-lindqvist)
* First Published: [May 2004](/book-publication-year/2004)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/20751037-let-the-right-one-in)
>
> It is autumn 1981 when inconceivable horror comes to Blackeberg, a suburb in Sweden. The body of a teenager is found, emptied of blood, the murder rumored to be part of a ritual killing. Twelve-year-old Oskar is personally hoping that revenge has come at long last---revenge for the bullying he endures at school, day after day.
> 
> But the murder is not the most important thing on his mind. A new girl has moved in next door---a girl who has never seen a Rubik's Cube before, but who can solve it at once. There is something wrong with her, though, something odd. And she only comes out at night. . . .Sweeping top honors at film festivals all over the globe, director Tomas Alfredsson's film of Let the Right One In has received the same kind of spectacular raves that have been lavished on the book. American and Swedish readers of vampire fiction will be thrilled!