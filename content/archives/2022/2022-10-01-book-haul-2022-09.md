---
title: "Book Haul for September 2022"
slug: "book-haul-2022-09"
author: "Paulo Pereira"
date: 2022-10-01T18:30:00+01:00
lastmod: 2022-10-01T18:30:00+01:00
cover: "/posts/book-hauls/2022-09-book-haul.png"
description: "September book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

September book haul.