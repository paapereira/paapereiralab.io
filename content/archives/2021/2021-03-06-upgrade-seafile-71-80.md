---
title: "Upgrading Seafile from 7.1.x to 8.0.x"
slug: "upgrade-seafile-71-80"
author: "Paulo Pereira"
date: 2021-03-06T14:08:33+00:00
lastmod: 2021-03-06T14:08:33+00:00
cover: "/posts/2021/2021-03-06-upgrade-seafile-71-80/seafile.png"
description: "The new 8.0.0 [Seafile](https://www.seafile.com/) version is out. Time to upgrade. I'm currently running 7.1.14.\n

Don't forget to read the [release notes](https://manual.seafile.com/upgrade/upgrade_notes_for_8.0.x/) from Seafile."
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Seafile
---

The new 8.0.0 [Seafile](https://www.seafile.com/) version is out. Time to upgrade. I'm currently running 7.1.14.

Don't forget to read the [release notes](https://manual.seafile.com/upgrade/upgrade_notes_for_8.0.x/) from Seafile.

## Before upgrading

Reading the [release notes](https://manual.seafile.com/upgrade/upgrade_notes_for_8.0.x/), this version needs the `libmysqlclient-dev` (I'm using `mariadb-libs`) package and new python modules.

I just upgraded `pip`, installed the new python modules and upgraded others.

```bash
sudo /usr/bin/python -m pip install --upgrade pip

sudo pip3 install --timeout=3600 future mysqlclient

sudo pip3 install --timeout=3600 Pillow pylibmc captcha jinja2 \
	sqlalchemy django-pylibmc django-simple-captcha python3-ldap
```

## Upgrading

I started by stopping the services:

```bash
sudo systemctl stop seahub
sudo systemctl stop seafile
```

Then I downloaded the latest version (I use the pro version) and executed the upgrade script.

```bash
sudo -u seafile -s /bin/sh

cd /srv/seafile
wget "https://download.seafile.com/d/xxxxxxx/files/?p=/pro/seafile-pro-server_8.0.0_x86-64_CentOS.tar.gz&dl=1" -O seafile-pro-server_8.0.0_x86-64_CentOS.tar.gz
tar -xzf seafile-pro-server_*

cd seafile-pro-server-8.0.0/
upgrade/upgrade_7.1_8.0.sh
```

## Starting the services

```bash
sudo systemctl start seafile
sudo systemctl start seahub
```