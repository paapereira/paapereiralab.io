---
title: "Children of Dune (Dune Book 3) by Frank Herbert (1976)"
slug: "children-of-dune-dune-book-3"
author: "Paulo Pereira"
date: 2021-12-20T09:00:00+00:00
lastmod: 2021-12-20T09:00:00+00:00
cover: "/posts/books/children-of-dune-dune-book-3.jpg"
description: "Finished Children of Dune (Dune Book 3)” by Frank Herbert."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Frank Herbert
book series:
  - Dune Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Frank Herbert
  - Dune Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “Children of Dune”.
* Author: [Frank Herbert](/book-authors/frank-herbert)
* First Published: [April 21st 1976](/book-publication-year/1970s)
* Series: [Dune](/book-series/dune-series) Book 3
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8117884-children-of-dune)
>
> The Children of Dune are twin siblings Leto and Ghanima Atreides, whose father, the Emperor Paul Muad’Dib, disappeared in the desert wastelands of Arrakis nine years ago. Like their father, the twins possess supernormal abilities—making them valuable to their manipulative aunt Alia, who rules the Empire in the name of House Atreides.
> 
> Facing treason and rebellion on two fronts, Alia’s rule is not absolute. The displaced House Corrino is plotting to regain the throne while the fanatical Fremen are being provoked into open revolt by the enigmatic figure known only as The Preacher. Alia believes that by obtaining the secrets of the twins’ prophetic visions, she can maintain control over her dynasty.
> 
> But Leto and Ghanima have their own plans for their visions—and their destinies...