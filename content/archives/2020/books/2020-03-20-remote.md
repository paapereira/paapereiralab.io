---
title: "[re-read] Remote by David Heinemeier Hansson and Jason Fried (2013)"
slug: "remote"
author: "Paulo Pereira"
date: 2020-03-20T23:02:00+00:00
lastmod: 2020-03-20T23:02:00+00:00
cover: "/posts/books/remote.jpg"
description: "Finished “Remote: Office Not Required” by David Heinemeier Hansson and Jason Fried."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - David Heinemeier Hansson
  - Jason Fried
book series:
  -
book genres:
  - Nonfiction
  - Business
book scores:
  - 7/10 Books
tags:
  - Book Log
  - David Heinemeier Hansson
  - Jason Fried
  - Nonfiction
  - Business
  - 7/10 Books
  - Audiobook
---

Finished “Remote: Office Not Required”. This is my [second](/posts/remote-office-not-required/) read of this book.
* Author: [David Heinemeier Hansson](/book-authors/david-heinemeier-hansson) and [Jason Fried](/book-authors/jason-fried)
* First Published: [October 29th 2013](/book-publication-year/2013)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17316682-remote)
>
> The “work from home” phenomenon is thoroughly explored in this illuminating new book from bestselling 37signals founders Fried and Hansson, who point to the surging trend of employees working from home (and anywhere else) and explain the challenges and unexpected benefits.  Most important, they show why – with a few controversial exceptions such as Yahoo -- more businesses will want to promote this new model of getting things done.
>
> The Industrial Revolution's "under one roof" model of conducting work is steadily declining owing to technology that is rapidly creating virtual workspaces and allowing workers to provide their vital contribution without physically clustering together.  Today, the new paradigm is "move work to the workers, rather than workers to the workplace."  According to Reuters, one in five global workers telecommutes frequently and nearly ten percent work from home every day. Moms in particular will welcome this trend.  A full 60% wish they had a flexible work option. But companies see advantages too in the way remote work increases their talent pool, reduces turnover, lessens their real estate footprint, and improves the ability to conduct business across multiple time zones, to name just a few advantages.  In Remote, inconoclastic authors Fried and Hansson will convince readers that letting all or part of work teams function remotely is a great idea--and they're going to show precisely how a remote work setup can be accomplished.
>