---
title: "Syncing Windows Mobile 5/6 with Synce (Ubuntu 8.10 version)"
slug: "syncing-windows-mobile-56-with-synce-update"
author: "Paulo Pereira"
date: 2008-12-10T21:10:00+00:00
lastmod: 2008-12-10T21:10:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 8.10
  - Synce
  - Qtek 9100
aliases:
  - /posts/syncing-windows-mobile-56-with-synce-update/
---

One of my "[to achieve](/posts/ubuntu-in-my-desktop-status/)" points was syncing my calendar and contact information from my Windows Mobile 5.0 Smartphone ([Qtek 9100](http://www.gsmarena.com/qtek_9100-1257.php)).

This post is an update from my previous [one](/posts/syncing-windows-mobile-56-with-synce/), after my Ubuntu 8.10 upgrade. Please refer to that first post if you're using previous versions of Ubuntu. I upgraded Ubuntu and Synce, so if you are installing it from scratch and you're having problems please let me know. I'll try and help you.

I will show how I'm syncing my phone with [Evolution](http://www.gnome.org/projects/evolution/) using [Synce](http://www.synce.org/) in Ubuntu 8.10.

This is based in [Synce wiki](http://www.synce.org/moin/SynceWithUbuntu) and [Synce mailing lists](http://sourceforge.net/mailarchive/message.php?msg_id=77f21d490805200515r65f82998ka148724b6e29b880%40mail.gmail.com).

Note: save this post if you're using Gnome Network Manager, because you may stay without Internet after installing the core libraries. It's very easy to fix though. In Ubuntu 8.10 I don't have this problem.

## Disclaimer

**This is not an official Synce guide.** This is my personal eXPerience using a single device, with a single WM flavor and a single OS.

If you are starting to play around with Synce, this post could help you if you're using the same WM and Linux OS's, but you should always check [Synce official wiki](http://www.synce.org/) for updates.

If you are having problems, the [Synce mailing lists](http://sourceforge.net/mail/?group_id=30550) are probably the best place to go right now. The people are friendly and will help you solving your problems. You could also send me an email. If I can, I'll help, if not, I'll guide you in the right direction.

## My setup

This is my current environment:

- [Qtek 9100](http://www.gsmarena.com/qtek_9100-1257.php) device running Windows Mobile 5.0 (WM5)
- Ubuntu 8.10
- [Evolution](http://www.gnome.org/projects/evolution/) as my Personal Information Manager

## Adding repositories

- Go to System > Administration > Software Sources
- Add the following Third Party software

```text
deb http://ppa.launchpad.net/synce/ubuntu intrepid main
```

- Close and Reload, as asked, your sources

## Core libraries

- Get the core libraries

```bash
sudo apt-get install synce-hal librra0-tools librapi2-tools
```

- Connect your device and run

```bash
synce-pls
```

You should see a list of files on your device. If so you have a working connection to your device!

## Password protected device

When running `synce-pls`, if you have the following error, your device is password protected. If so, install [SynCE-GNOME](http://www.synce.org/moin/SynceTools/SynceGnome) or [SynCE-KPM](http://www.synce.org/moin/SynceTools/SynceKpm) to provide a password prompt on device connect.

I didn't try this because my device isn't password protected...

```text
WARNING **: synce_info_from_odccm: Failed to get a connection for :
Not authenticated, you need to call !ProvidePassword with the
correct password. pls: Could not find configuration at path
'(Default)'
```

## Gnome Network Manager

If you're running Gnome Network Manager you don't have Internet by now because your phone is now the new default network connection.

Follow this and you should be fine:

- Check what ethernet device was given to your phone (in my case is rndis0)

```bash
ifconfig -a | grep 80:00:60:0f:e8:00 | cut -d " " -f 1
```

- Open /etc/network/interfaces

```bash
sudo vi /etc/network/interfaces
```

- Add the following to the file (replace *rndis0* with the result from the first command)

```text
iface rndis0 inet dhcp
```

- This will make Gnome Network Manager ignore the interface
- Restart your network

```bash
sudo /etc/init.d/networking restart
```

## No devices are connected to odccm

If you get an error that no devices are connected to odccm you should probably blacklist ipaq module:

- Open /etc/modprobe.d/blacklist

```bash
sudo vi /etc/modprobe.d/blacklist
```

- Add the following to the file

```text
blacklist ipaq
```

- After that you can remove any currently active ipaq modules

```bash
sudo rmmod ipaq
```

## Syncing with OpenSync (needed packages)

In order to sync your device you need to use OpenSync.

```bash
sudo apt-get install multisync-tools opensync-plugin-evolution opensync-plugin-synce
```

## Sync-engine

Sync-engine must be running in order to sync your phone. In Ubuntu the Engine should start automatically when you connect your device.

- Download the config.xml file needed by sync-engine

```bash
cd ~/.synce/
wget http://synce.svn.sf.net/svnroot/synce/trunk/sync-engine/config/config.xml
```

Now the [tip](http://sourceforge.net/mailarchive/message.php?msg_id=77f21d490805200515r65f82998ka148724b6e29b880%40mail.gmail.com). The current config.xml has the *AutoSyncCommand* and the *Disable* commented. Uncommented, leaving it disabled.

- You can download my own

```bash
cd ~/.synce/
wget http://lofspot.net/synce/config.xml
```

- Start sync-engine if needed

```bash
synce-sync-engine
```

- Connect your phone via usb. You shouldn't get any errors.

## Create a partnership

You have to create a partnership between device and computer using one of two methods:

1. Via command line 
2. Using synce-kpm (graphical and recommend)

(1) You can then use the command

```bash
synce-create-partnership "Linux desktop" "Contacts,Calendar"
```

The "Linux desktop" string can be any string of 20 characters or less. The available items for synchronization are:

* Contacts
* Calendar
* Tasks
* Files

When specifying items to sync, they must be separated by commas. You must also surround the list in double quotes and the string must not contain any whitespace.

(2) Or install [synce-kpm](http://www.guidodiepen.nl/)

```bash
sudo apt-get install synce-kpm
```

Now run `synce-kpm`

```bash
synce-kpm
```

Go to Partnership manager. You probably have already partnerships created if you ever synced your phone under Windows.

I chose to start by deleting my existing partnerships and start from scratch, but, as pointed out by [Guido Diepen](http://lof-ubuntu-xp.lofspot.net/2008/06/04/syncing-windows-mobile-56-with-synce/#comment-21) if your device runs WM6 this will erase all your contacts/tasks/calendar. So do not delete your partnership if you're running WM6 and your want to keep your data in the device.

In my case, having WM5, I've deleted every partnerships and created a new one. I chose to sync Calendar, Contacts and Tasks.

## synce-opensync-plugin

Check the list of the available plugins:

```bash
msynctool --listplugins
```

You should have synce-opensync-plugin.

If you don't see the above plugin in the output of `msynctool`, then download the [plugin](http://synce.svn.sf.net/svnroot/synce/trunk/sync-engine/plugins/synce-opensync-plugin-2x.py) and copy this file to /usr/lib/opensync/python-plugins.

I already had the plugin.

## Create OpenSync group

A group between SynCE and your chosen PIM application must be initiated.

I'm using Evolution (evo2-sync).

```bash
msynctool --addgroup synce-sync
msynctool --addmember synce-sync synce-opensync-plugin
msynctool --addmember synce-sync evo2-sync 
```

## Syncing your device (finally)

- Start sync-engine if needed and connect your device

```bash
synce-sync-engine
```

- Start synce-kpm if you want

```bash
synce-kpm
```

- Sync with OpenSync

```bash
msynctool --sync synce-sync
```

This could take some time the first time, so be patient. You can check your progress in synce-kpm.

After the msynctool command ends completely, check Evolution :-)

Important: keep your device on, so the synchronization isn't interrupted.

In Ubuntu 8.10 I only need to call the msynctool after connecting the device and be sure the firewall isn't causing trouble. Very easy.

## Firewall

If you have a firewall active, and you're having troubles syncing your device try to disable it.

## Inconsistencies while syncing

While syncing kepp an eye out in you console. I had some inconsistencies in my data (contacts) than I had to resolve by selecting witch version of a contact I would like to keep.

Please, feel free to post a comment or email me at lof-ubuntu-xp [(at)] lofspot.net.

This is just a "this is how I did it" post. I probably won't be able to help you, especially in another distribution/version or phone, still feel free to share your experience.