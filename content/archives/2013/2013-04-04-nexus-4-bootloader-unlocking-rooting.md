---
title: "Nexus 4 bootloader unlocking, rooting and installing a new ROM"
slug: "nexus-4-bootloader-unlocking-rooting"
author: "Paulo Pereira"
date: 2013-04-04T00:17:00+01:00
lastmod: 2013-04-04T00:17:00+01:00
draft: false
toc: true
categories:
  - Android
tags:
  - Android
  - Nexus 4
  - CarbonROM
---

I just got a Nexus 4 :)

![Nexus 4 (mako)](/posts/2013/2013-04-04-nexus-4-bootloader-unlocking-rooting/nexus_4.png)

Time to unlock the bootloader, install a new recovery, get root and install a new ROM.

## Android SDK

Download the Android SDK from [here](http://developer.android.com/sdk/index.html#ExistingIDE) and uncompress the zip file. I will be using the Linux version.

```bash
mkdir ~/Android
cd ~/Android
unzip adt-bundle-linux-x86_64-20130219.zip
```

## USB Debugging

In Android go to Settings>About>Build Number and click seven times to unlock the Developer Options.
Then go to Settings>Developer Options and check USB Debugging.

## Unlocking the bootloader

Power Off the device.

Press Volume Down + Power to enter the bootloader.
You should see a screen with a green android with his chest exposed.
The Lock State should be Locked.

Unlock the bootloader:

```bash
cd ~/Android/adt-bundle-linux-x86_64-20130219/sdk/platform-tools
sudo ./fastboot oem unlock
```

Say Yes in the phone.

The Lock State should now say Unlocked.

## Install the Clockwork Recovery

Download the Clockwork Recovery for the Nexus 4 [here](http://www.clockworkmod.com/rommanager).

Flash the recovery into the device, still in the bootloader screen:

```bash
sudo ./fastboot flash recovery recovery-clockwork-6.0.3.0-mako.img
```

Now, navigate to "Recovery Mode" with the volume keys and use the power button to select.
You should see the Clockwork Recovery menu.

## Getting Root

Download the SuperUser app [here](http://download.chainfire.eu/282/SuperSU/).

Go into "install zip from sdcard > choose zip from sdcard" just to  guarantee that ClockworkMod Recovery is accessing the storage.

Push the SuperUser app to the device:

```bash
sudo ./adb push CWM-SuperSU-v0.98.zip /data/media
```

Reboot (reboot system now).

Your phone is now unlocked and with root. You can check is the app SuperSu is in your apps drawer.

## Prepare your SIM card

The Nexus 4 uses a microSIM card and I have a regular SIM card.

I saw this [video](https://www.youtube.com/watch?v=qquug1GuNLg) and used this [template](http://www.airportal.de/nanosim/How_to_cut_Mini_and_Micro_SIM_to_Nano_SIM.pdf) to help me cut the SIM card into a microSIM card.

## Backup original ROM

Now that you have the Clockwork Recovery installed, you can backup the  original ROM. It's a good idea especially if you want to install a new  custom ROM.

Power Off the device.

Press Volume Down + Power to enter the bootloader.

Navigate to "Recovery Mode" with the volume keys and use the power button to select.

In the recovery go to "backup and restore > backup".

Copy the backup to your computer:

```bash
sudo ./adb pull /data/media/clockworkmod sdcard/clockworkmod_bcks
sudo chown -R yourusername:yourusername sdcard
```

## Install a custom ROM

I will install the [CarbonROM](http://goo.im/devs/carbon) for the Nexus 4 (mako).
The [Google Apps](http://goo.im/gapps/) are also needed.

Wipe your device:

- Format /data and /cache (wipe data/factory reset)
- Format /system (mounts and storage > format /system)
- Format /sdcard (mounts and storage > format /sdcard)
- Wipe Dalvik Cache (advanced > wipe dalvik cache)

After these steps, your device, including the "sdcard" is cleaned without ROM or files.

Create a new folder in the device:

```bash
sudo ./adb shell
cd /data/media/0
mkdir 0.ROOT
mkdir 0.ROOT/CarbonRom
exit
```

Push the ROM and the Google Apps into the device:

```bash
sudo ./adb push CARBON-JB-EXP-03APR2013-031909-mako.zip /data/media/0/0.ROOT/CarbonRom
sudo ./adb push gapps-jb-20130301-signed.zip /data/media/0/0.ROOT/CarbonRom
```

Install the ROM and the Google Apps:

- Install the ROM (install zip from sdcard > choose zip from sdcard > 0 > 0.ROOT  > CarbonRom > CARBON-JB-EXP-03APR2013-031909-mako.zip
- Install the Google Apps (install zip from sdcard > choose zip  from sdcard > 0 > 0.ROOT > CarbonRom >  gapps-jb-20130301-signed.zip)
- Reboot (reboot system now)

Main source: http://forum.xda-developers.com/showthread.php?t=2018179