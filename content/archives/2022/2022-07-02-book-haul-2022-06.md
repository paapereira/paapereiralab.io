---
title: "Book Haul for June 2022 + Birthday Haul"
slug: "book-haul-2022-06"
author: "Paulo Pereira"
date: 2022-07-02T13:32:02+01:00
lastmod: 2022-07-02T13:32:02+01:00
cover: "/posts/book-hauls/2022-06-book-haul.png"
description: "June + Birthday book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

June book haul + Birthday Haul.