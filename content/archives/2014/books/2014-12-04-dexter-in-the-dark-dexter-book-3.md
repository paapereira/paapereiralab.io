---
title: "Dexter in the Dark (Dexter Book 3) by Jeff Lindsay (2006)"
slug: "dexter-in-the-dark-dexter-book-3"
author: "Paulo Pereira"
date: 2014-12-04T23:45:00+00:00
lastmod: 2014-12-04T23:45:00+00:00
cover: "/posts/books/dexter-in-the-dark-dexter-book-3.jpg"
description: "Finished “Dexter in the Dark (Dexter Book 3)” by Jeff Lindsay."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2006
book authors:
  - Jeff Lindsay
book series:
  - Dexter Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Jeff Lindsay
  - Dexter Series
  - Fiction
  - Mystery
  - 6/10 Books
  - Audiobook
---

Finished “Dexter in the Dark”.
* Author: [Jeff Lindsay](/book-authors/jeff-lindsay)
* First Published: [2006](/book-publication-year/2006)
* Series: [Dexter](/book-series/dexter-series) Book 3
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/23279899-dexter-in-the-dark)
>
> In his work as a Miami crime scene investigator, Dexter Morgan is accustomed to seeing evil deeds...particularly because, on occasion, he rather enjoys committing them himself. Guided by his Dark Passenger (the reptilian voice inside him), he lives his outwardly normal life adhering to one simple rule: He kills only very bad people. Dexter slides through life undetected, working as a blood spatter analyst for the Miami Police Department, helping his fiancée raise her two adorable (if somewhat...unique) children, and always planning his next jaunt as Dexter the Dark Avenger under the light of the full moon.
>
> But then everything changes. Dexter is called to a crime scene that seems routine: a gruesome double homicide at the university campus, which Dexter would normally investigate with gusto, before enjoying a savory lunch. And yet this scene feels terribly wrong. Dexter's Dark Passenger senses something it recognizes, something utterly chilling, and the Passenger - mastermind of Dexter's homicidal prowess - promptly goes into hiding.
>
> With his Passenger on the run, Dexter is left to face this case all alone - not to mention his demanding sister (Sergeant Deborah), his frantic fiancée (Rita), and the most frightening wedding caterer ever to plan a menu. Equally unsettling, Dexter begins to realize that something very dark and very powerful has its sights set on him. Dexter is left in the dark, but he must summon his sharpest investigative instincts not only to pursue his enemy, but to locate and truly understand his Dark Passenger. To find him, Dexter has to research the questions he's never dared ask: Who is the Dark Passenger, and where does he come from? It is nothing less than a search for Dexter's own dark soul...fueled by a steady supply of fresh doughnuts.
>
> Macabre, ironic, and wonderfully entertaining, Dexter in the Dark goes deeper into the psyche of one of the freshest protagonists in recent fiction. Jeff Lindsay's glorious creativity is on full display in his most accomplished novel yet.