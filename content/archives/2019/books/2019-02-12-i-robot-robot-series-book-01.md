---
title: "I, Robot (Robot Series Book 0.1) by Isaac Asimov (1950)"
slug: "i-robot-robot-series-book-01"
author: "Paulo Pereira"
date: 2019-02-12T22:49:00+00:00
lastmod: 2019-02-12T22:49:00+00:00
cover: "/posts/books/i-robot-robot-series-book-01.jpg"
description: "Finished “I, Robot (Robot Series Book 0.1)” by Isaac Asimov."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1950s
book authors:
  - Isaac Asimov
book series:
  - Robot Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Isaac Asimov
  - Robot Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/i-robot-robot-series-book-01
---

Finished “I, Robot”.
* Author: [Isaac Asimov](/book-authors/isaac-asimov)
* First Published: [1950](/book-publication-year/1950s)
* Series: [Robot Series](/book-series/robot-series) Book 0.1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/41033118-i-robot)
>
> The three laws of Robotics:
> 1) A robot may not injure a human being or, through inaction, allow a human being to come to harm.
> 2) A robot must obey orders given to it by human beings except where such orders would conflict with the First Law.
> 3) A robot must protect its own existence as long as such protection does not conflict with the First or Second Law.
>
> With these three, simple directives, Isaac Asimov changed our perception of robots forever when he formulated the laws governing their behavior. In I, Robot, Asimov chronicles the development of the robot through a series of interlinked stories: from its primitive origins in the present to its ultimate perfection in the not-so-distant future--a future in which humanity itself may be rendered obsolete.
>
> Here are stories of robots gone mad, of mind-read robots, and robots with a sense of humor. Of robot politicians, and robots who secretly run the world--all told with the dramatic blend of science fact and science fiction that has become Asimov's trademark.