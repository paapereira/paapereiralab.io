---
title: "The Name of the Wind (The Kingkiller Chronicle Book 1) by Patrick Rothfuss (2007)"
slug: "the-name-of-the-wind-the-kingkiller-chronicle-book-1"
author: "Paulo Pereira"
date: 2018-10-24T21:44:00+01:00
lastmod: 2018-10-24T21:44:00+01:00
cover: "/posts/books/the-name-of-the-wind-the-kingkiller-chronicle-book-1.jpg"
description: "Finished “The Name of the Wind (The Kingkiller Chronicle Book 1)” by Patrick Rothfuss."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2007
book authors:
  - Patrick Rothfuss
book series:
  - The Kingkiller Chronicle Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Patrick Rothfuss
  - The Kingkiller Chronicle Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Audiobook
---

Finished “The Name of the Wind”.
* Author: [Patrick Rothfuss](/book-authors/patrick-rothfuss)
* First Published: [March 27th 2007](/book-publication-year/2007)
* Series: [The Kingkiller Chronicle](/book-series/the-kingkiller-chronicle-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/22571137-the-name-of-the-wind)
>
> "My name is Kvothe. I have stolen princesses back from sleeping barrow kings. I have burned down the town of Trebon. I have spent the night with Felurian and left with both my sanity and my life. I was expelled from the University at a younger age than most people are allowed in. I tread paths by moonlight that others fear to speak of during the day. I have talked to Gods, loved women and written songs that make the minstrels weep. You may have heard of me."
>
> So begins a tale unequaled in fantasy literature - the story of a hero told in his own voice. It is a tale of sorrow, a tale of survival, a tale of one man's search for meaning in his universe, and how that search, and the indomitable will that drove it, gave birth to a legend.