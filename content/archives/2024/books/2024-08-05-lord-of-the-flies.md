---
title: "Lord of the Flies by William Golding (1954)"
slug: "lord-of-the-flies"
author: "Paulo Pereira"
date: 2024-08-05T21:00:00+01:00
lastmod: 2024-08-05T21:00:00+01:00
cover: "/posts/books/lord-of-the-flies.jpg"
description: "Finished “Lord of the Flies” by William Golding."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1950s
book authors:
  - William Golding
book series:
  - 
book genres:
  - Fiction
  - Classics
book scores:
  - 9/10 Books
tags:
  - Book Log
  - William Golding
  - Fiction
  - Classics
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/lord-of-the-flies
---

Finished “Lord of the Flies”.
* Author: [William Golding](/book-authors/william-golding)
* First Published: [September 17, 1954](/book-publication-year/1950s)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31133566-lord-of-the-flies)
>
> As provocative today as when it was first published in 1954, Lord of the Flies continues to ignite passionate debate with its startling, brutal portrait of human nature. William Golding’s compelling story about a group of very ordinary boys marooned on a coral island has been labeled a parable, an allegory, a myth, a morality tale, a parody, a political treatise, and even a vision of the apocalypse. But above all, it has earned its place as one of the indisputable classics of the twentieth century for readers of any age.