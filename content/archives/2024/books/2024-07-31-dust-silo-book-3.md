---
title: "Dust (Silo Book 3) by Hugh Howey (2013)"
slug: "dust-silo-book-3"
author: "Paulo Pereira"
date: 2024-07-31T15:00:00+01:00
lastmod: 2024-07-31T15:00:00+01:00
cover: "/posts/books/dust-silo-book-3.jpg"
description: "Finished “Dust (Silo Book 3)” by Hugh Howey."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - Hugh Howey
book series:
  - Silo Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Hugh Howey
  - Silo Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “Dust”.
* Author: [Hugh Howey](/book-authors/hugh-howey)
* First Published: [August 17, 2013](/book-publication-year/2013)
* Series: [Silo](/book-series/silo-series) Book 3
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17855756-dust)
>
> In a time when secrets and lies were the foundations of life, someone has discovered the truth. And they are going to tell.
> 
> Jules knows what her predecessors created. She knows they are the reason life has to be lived in this way.
> 
> And she won't stand for it.
> 
> But Jules no longer has supporters. And there is far more to fear than the toxic world beyond her walls.
> 
> A poison is growing from within Silo 18.
> 
> One that cannot be stopped.
> 
> Unless Silo 1 step in.