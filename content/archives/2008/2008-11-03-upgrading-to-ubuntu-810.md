---
title: "Upgrading to Ubuntu 8.10 The Intrepid Ibex"
slug: "upgrading-to-ubuntu-810"
author: "Paulo Pereira"
date: 2008-11-03T22:03:00+00:00
lastmod: 2008-11-03T22:03:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 8.10
  - Intrepid Ibex
aliases:
  - /posts/upgrading-to-ubuntu-810/
---

Last week Ubuntu 8.10, aka The Intrepid Ibex, was [released](http://www.ubuntu.com/getubuntu/download).

Time to updgrade :-) First in my [laptop](/posts/hp-pavilion-dv5-1020ep-ubuntu-64-bits/).

I will be the using the 64 bit alternate cd to upgrade. This means that the upgrade will have a cd image as his source, and not the Internet. This way it's much faster.

## Before you start

- Check for updates using Update Manager
- Backup your data
- Check the [Release Notes](http://www.ubuntu.com/getubuntu/releasenotes/810)
- [Download](http://www.ubuntu.com/getubuntu/download) Ubuntu (I recommend using [Torrents](http://www.ubuntu.com/getubuntu/downloadmirrors#bt))
- Check the file hash (example: `md5sum ubuntu-8.10-alternate-amd64.iso`)
- Be sure to upgrade from Ubuntu 8.04.
- Use [Gmount-iso](http://www.ubuntugeek.com/easy-way-of-mountunmount-iso-images-in-ubuntu.html) to mount the iso file. This way cd could be mounted without having to burn a cd.

In the Terminal, go to the cd mount point (example: */media/cdrom*) and type:

```bash
gksu "sh /cdrom/cdromupgrade"
```

And now we wait...

During the upgrade there may be some questions about replacing some configuration files. I replaced them all with the new versions.

After restarting and reactivating my Software Sources there were more updates to do. This is because Ubuntu deactivates other software sources and because I made the upgrade from the alternate cd.

## Issues

- **Non-Issue 1:** It really seams faster :-)
- **Non-Issue 2:** Install OpenOffice 3.0 by adding the following to your Software Sources (System > Administration > Software Sources) and do a normal Update

```text
deb http://ppa.launchpad.net/openoffice-pkgs/ubuntu intrepid main
deb-src http://ppa.launchpad.net/openoffice-pkgs/ubuntu intrepid main
```

- **Issue 1:**

I was missing some software I wanted to get back

- gVim
- gnuCash
- Mail Notification
- [LAMP Server](/posts/apache-php-and-mysql/) (*check the link for details*)

- **Issue 2:** Reactivate Software Sources (System > Administration > Software Sources) updating them to Intrepid.

- **Issue 3:** Add to re-install EnvyNG-Qt and enable the ATI driver

```bash
sudo apt-get install envyng-qt
```

- **Issue 4:** Scrolling in Firefox was extremely slow. I reinstalled the ATI driver and it's fixed now.

```bash
sudo apt-get remove --purge fglrx*
sudo apt-get remove --purge xserver-xorg-video-ati xserver-xorg-video-radeon
sudo apt-get install xserver-xorg-video-ati
sudo dpkg-reconfigure -phigh xserver-xorg
sudo dpkg-reconfigure -phigh xserver-xorg
```

- **Issue 5:** The SC Card Reader only works if the card is in the slot at boot up time.