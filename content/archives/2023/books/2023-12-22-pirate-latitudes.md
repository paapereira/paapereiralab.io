---
title: "Pirate Latitudes by Michael Crichton (2009)"
slug: "pirate-latitudes"
author: "Paulo Pereira"
date: 2023-12-22T16:00:00+00:00
lastmod: 2023-12-22T16:00:00+00:00
cover: "/posts/books/pirate-latitudes.jpg"
description: "Finished “Pirate Latitudes” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2009
book authors:
  - Michael Crichton
book series:
  - 
book genres:
  - Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Fiction
  - 7/10 Books
  - Ebook
---

Finished “Pirate Latitudes”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [November 24, 2009](/book-publication-year/2009)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8153333-pirate-latitudes)
>
> The Caribbean, 1665. A remote colony of the English Crown, the island of Jamaica holds out against the vast supremacy of the Spanish empire. Port Royal, its capital, is a cutthroat town of taverns, grog shops, and bawdy houses. 
> 
> In this steamy climate there's a living to be made, a living that can end swiftly by disease - or by dagger. For Captain Charles Hunter, gold in Spanish hands is gold for the taking, and the law of the land rests with those ruthless enough to make it. 
> 
> Word in port is that the galleon El Trinidad , fresh from New Spain, is awaiting repairs in a nearby harbor. Heavily fortified, the impregnable harbor is guarded by the bloodthirsty Cazalla, a favorite commander of the Spanish king himself. With backing from a powerful ally, Hunter assembles a crew of ruffians to infiltrate the enemy outpost and commandeer El Trinidad , along with its fortune in Spanish gold. The raid is as perilous as the bloodiest tales of island legend, and Hunter will lose more than one man before he even sets foot on foreign shores, where dense jungle and the firepower of Spanish infantry stand between him and the treasure. . . . 