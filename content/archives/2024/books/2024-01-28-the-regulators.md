---
title: "The Regulators by Stephen King writing as Richard Bachman (1996)"
slug: "the-regulators"
author: "Paulo Pereira"
date: 2024-01-28T19:00:00+00:00
lastmod: 2024-01-28T19:00:00+00:00
cover: "/posts/books/the-regulators.jpg"
description: "Finished “The Regulators” by Stephen King writing as Richard Bachman."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Stephen King
  - Richard Bachman
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Richard Bachman
  - Stephen King Novels
  - Fiction
  - Horror
  - 7/10 Books
  - Audiobook
---

Finished “The Regulators”.
* Author: [Stephen King](/book-authors/stephen-king) writing as [Richard Bachman](/book-authors/richard-bachman)
* First Published: [January 1, 1996](/book-publication-year/1990s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/30237730-the-regulators)
>
> The battle against evil has begun in this "devishly entertaining" (Publishers Weekly) story of a suburban neighborhood in the grip of surreal terror - a number-one national best seller from Stephen King writing as Richard Bachman.
> 
> Peaceful suburbia on Poplar Street in Wentworth, Ohio, takes a turn for the ugly when four vans containing armed "regulators" terrorize the street's residents, cold-bloodedly killing anyone foolish enough to step outside their homes. Houses mysteriously transform into log cabins, and the street now ends in what looks like a child's hand-drawn Western landscape. Masterminding this sudden onslaught is the evil creature Tak, who has taken over the body of an autistic eight-year-old boy, Seth Garin.