---
title: "The Sandman: Act II by Neil Gaiman and Dirk Maggs (2021)"
slug: "the-sandman-act-2-book-2"
author: "Paulo Pereira"
date: 2024-07-07T15:00:00+01:00
lastmod: 2024-07-07T15:00:00+01:00
cover: "/posts/books/the-sandman-act-2-book-2.jpg"
description: "Finished “The Sandman: Act II” by Neil Gaiman and Dirk Maggs."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - Neil Gaiman
  - Dirk Maggs
book series:
  - Sandman Audible Original Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Neil Gaiman
  - Dirk Maggs
  - Sandman Audible Original Series
  - Fiction
  - Fantasy
  - 6/10 Books
  - Audiobook
---

Finished “The Sandman: Act II”.
* Author: [Neil Gaiman](/book-authors/neil-gaiman) and [Dirk Maggs](/book-authors/dirk-maggs)
* First Published: [September 22, 2021](/book-publication-year/2021)
* Series: [Sandman Audible Original](/book-series/sandman-audible-original-series) Book 2
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/58730467-the-sandman)
>
> Enter the Dreaming once again as the blockbuster audio adaptation of "the greatest epic in the history of comic books" continues in The Sandman: Act II. James McAvoy returns to voice Morpheus, the Lord of Dreams, in this sequel to the #1 New York Times audio best-seller. Journey into a world of myths, imagination, and terror based on the best-selling DC comic books and graphic novels written by Neil Gaiman (returning as the Narrator), and lose yourself in another groundbreaking, immersive drama adapted and directed by the award-winning audio master Dirk Maggs.
> 
> In the absolutely packed Act II, the dark fantasy resumes and the Sandman expands into the French Revolution, Ancient Rome, 19th-Century San Francisco, 8th-century Baghdad, and beyond. New and familiar characters abound, voiced by a bright mix of performers, including Kat Dennings, Regé-Jean Page, Emma Corrin, Michael Sheen, Kristen Schaal, Brian Cox, John Lithgow, Jeffrey Wright, and so many more, including fan-favorite narrators Simon Vance and Ray Porter.
> 
> Just close your eyes and listen again as the greatest epic continues.