---
title: "The Future of Humanity by Michio Kaku (2018)"
slug: "the-future-of-humanity"
author: "Paulo Pereira"
date: 2019-07-05T22:21:00+01:00
lastmod: 2019-07-05T22:21:00+01:00
cover: "/posts/books/the-future-of-humanity.jpg"
description: "Finished “The Future of Humanity: Terraforming Mars, Interstellar Travel, Immortality, and Our Destiny Beyond” by Michio Kaku."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Michio Kaku
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Michio Kaku
  - Nonfiction
  - Science
  - 7/10 Books
  - Audiobook
---

Finished “The Future of Humanity: Terraforming Mars, Interstellar Travel, Immortality, and Our Destiny Beyond”.
* Author: [Michio Kaku](/book-authors/michio-kaku)
* First Published: [2018](/book-publication-year/2018)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/42757107-the-future-of-humanity)
>
> Human civilization is on the verge of spreading beyond Earth. More than a possibility, it is becoming a necessity: whether our hand is forced by climate change and resource depletion or whether future catastrophes compel us to abandon Earth, one day we will make our homes among the stars.
>
> World-renowned physicist and futurist Michio Kaku explores in rich, accessible detail how humanity might gradually develop a sustainable civilization in outer space. With his trademark storytelling verve, Kaku shows us how science fiction is becoming reality: mind-boggling developments in robotics, nanotechnology, and biotechnology could enable us to build habitable cities on Mars; nearby stars might be reached by microscopic spaceships sailing through space on laser beams; and technology might one day allow us to transcend our physical bodies entirely.
>
> With irrepressible enthusiasm and wonder, Dr. Kaku takes readers on a fascinating journey to a future in which humanity could finally fulfil its long-awaited destiny among the stars - and perhaps even achieve immortality.