---
title: "A Problem of Proportion (The Human Division Book 11) by John Scalzi (2013)"
slug: "problem-of-proportion-the-human-division-book-11"
author: "Paulo Pereira"
date: 2019-05-27T23:12:00+01:00
lastmod: 2019-05-27T23:12:00+01:00
cover: "/posts/books/problem-of-proportion-the-human-division-book-11.png"
description: "Finished “A Problem of Proportion (The Human Division Book 11)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “A Problem of Proportion”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [March 26th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 11
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17695236-a-problem-of-proportion)
>
> A secret backdoor meeting between Ambassador Ode Abumwe and the Conclaves Hafte Sorvalh turns out to be less than secret as both of their ships are attacked. Its a surprise to both teams but its the identity of the attacker that is the real surprise, and suggests a threat to both humanity and The Conclave.