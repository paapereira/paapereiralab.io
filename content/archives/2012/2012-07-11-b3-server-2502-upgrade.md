---
title: "B3 Server 2.5.0.2 Upgrade"
slug: "b3-server-2502-upgrade"
author: "Paulo Pereira"
date: 2012-07-11T22:19:00+01:00
lastmod: 2012-07-11T22:19:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
---

New B3 Server [version](http://forum.excito.net/viewtopic.php?f=1&t=3947) is out, with minor updates.

## Upgrade to 2.5.0.2

- Connect to your b3 server
```bash
ssh user@b3server
```
- Become root
```bash
su
```
- Upgrade instructions
```bash
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade
```

You can confirm the software version going into b3 web administration page, then Settings and Software update.

![b3 server](/posts/2012/2012-07-11-b3-server-2502-upgrade/b3server.png)

## Official Excito Forum Notes

*Fixed another backup regression.*