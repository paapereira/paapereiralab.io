---
title: "Death's End (Remembrance of Earth's Past Book 3) by Liu Cixin (2010)"
slug: "deaths-end-remembrance-of-earths-past-book-3"
author: "Paulo Pereira"
date: 2020-01-21T23:18:00+00:00
lastmod: 2020-01-21T23:18:00+00:00
cover: "/posts/books/deaths-end-remembrance-of-earths-past-book-3.png"
description: "Finished “Death's End (Remembrance of Earth's Past Book 3)” by Liu Cixin."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2010
book authors:
  - Liu Cixin
book series:
  - Remembrance of Earth's Past Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Liu Cixin
  - Remembrance of Earth's Past Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Audiobook
---

Finished “Death's End”.
* Author: [Liu Cixin](/book-authors/liu-cixin)
* First Published: [2010](/book-publication-year/2010)
* Series: [Remembrance of Earth's Past](/book-series/remembrance-of-earths-past-series) Book 3
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/32500791-death-s-end)
>
> Half a century after the Doomsday Battle, the uneasy balance of Dark Forest Deterrence keeps the Trisolaran invaders at bay. Earth enjoys unprecedented prosperity due to the infusion of Trisolaran knowledge. With human science advancing daily and the Trisolarans adopting Earth culture, it seems that the two civilizations will soon be able to co-exist peacefully as equals without the terrible threat of mutually assured annihilation. But the peace has also made humanity complacent.
>
> Cheng Xin, an aerospace engineer from the early twenty-first century, awakens from hibernation in this new age. She brings with her knowledge of a long-forgotten program dating from the beginning of the Trisolar Crisis, and her very presence may upset the delicate balance between two worlds. Will humanity reach for the stars or die in its cradle?
>