---
title: "Meditations by Marcus Aurelius (0180)"
slug: "meditations"
author: "Paulo Pereira"
date: 2018-07-29T20:49:00+01:00
lastmod: 2018-07-29T20:49:00+01:00
cover: "/posts/books/meditations.jpg"
description: "Finished “Meditations” by Marcus Aurelius."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 0180
book authors:
  - Marcus Aurelius
book series:
  - 
book genres:
  - Nonfiction
  - Philosophy
  - Classics
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Marcus Aurelius
  - Nonfiction
  - Philosophy
  - Classics
  - 5/10 Books
  - Audiobook
---

Finished “Meditations”.
* Author: [Marcus Aurelius](/book-authors/marcus-aurelius)
* First Published: [0180](/book-publication-year/0180)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28634442-meditations)
>
> Written in Greek, without any intention of publication, by the only Roman emperor who was also a philosopher, the Meditations of Marcus Aurelius (AD 121-180) offer a remarkable series of challenging spiritual reflections and exercises developed as the emperor struggled to understand himself and make sense of the universe.
>
> Ranging from doubt and despair to conviction and exaltation, they cover such diverse topics as the nature of moral virtue, human rationality, divine providence, and Marcus' own emotions.
>
> But while the Meditations were composed to provide personal consolation and encouragement, in developing his beliefs Marcus Aurelius also created one of the greatest of all works of philosophy: a timeless collection of extended meditations and short aphorisms that has been consulted and admired by statesmen, thinkers and readers through the centuries.