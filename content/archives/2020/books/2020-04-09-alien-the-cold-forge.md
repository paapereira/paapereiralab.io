---
title: "Alien: The Cold Forge by Alex White (2018)"
slug: "alien-the-cold-forge"
author: "Paulo Pereira"
date: 2020-04-09T23:27:00+01:00
lastmod: 2020-04-09T23:27:00+01:00
cover: "/posts/books/alien-the-cold-forge.jpg"
description: "Finished “Alien: The Cold Forge” by Alex White."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Alex White
book series:
  - Alien Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Alex White
  - Alien Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/alien-the-cold-forge
---

Finished “Alien: The Cold Forge”.
* Author: [Alex White](/book-authors/alex-white)
* First Published: [April 24th 2018](/book-publication-year/2018)
* Series: [Alien Series](/book-series/alien-series)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43526396-alien)
>
> A dramatic new Alien novel, as Weyland-Yutani seeks to recover from the failure of Hadley's Hope, and successfully weaponize the Xenomorphs.
>
> With the failure of the Hadley's Hope, Weyland-Yutani has suffered a devastating defeat--the loss of the Aliens. Yet there's a reason the company rose to the top, and they have a redundancy already in place. Remote station RB-323 abruptly becomes their greatest hope for weaponizing the Xenomorph, but there's a spy aboard--someone who doesn't necessarily act in the company's best interests. If discovered, this person may have no choice but to destroy RB-323... and everyone on board. That is, if the Xenomorphs don't do the job first.
>