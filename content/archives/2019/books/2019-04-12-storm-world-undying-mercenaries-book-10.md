---
title: "Storm World (Undying Mercenaries Book 10) by B.V. Larson (2018)"
slug: "storm-world-undying-mercenaries-book-10"
author: "Paulo Pereira"
date: 2019-04-12T23:20:00+01:00
lastmod: 2019-04-12T23:20:00+01:00
cover: "/posts/books/storm-world-undying-mercenaries-book-10.jpg"
description: "Finished “Storm World (Undying Mercenaries Book 10)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Storm World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [2018](/book-publication-year/2018)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 10
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43525584-storm-world)
>
> James McGill is sent to the Core Worlds!
>
> In an unprecedented first, Earth sends a message to Mogwa Prime. Unfortunately, the messenger is not met with enthusiasm. Misunderstandings soon threaten all of Humanity. Forced to prove Earth can serve the Empire better than any rival, McGill does his best.
>
> Eager to prove our worth to the Galactics, the frontier war between Rigel and Earth is expanded to Storm World. Circled by six moons, the planet is ravaged by wild storms and tides. Battles are fought in raging hurricanes, and death stalks the soldiers on both sides.
>
> McGill grimly fights and dies in the mud until the job gets done, but will it be enough to satisfy the angry Mogwa?
>
> STORM WORLD, the tenth novel in the Undying Mercenaries Series, is the longest and most thrilling book yet.