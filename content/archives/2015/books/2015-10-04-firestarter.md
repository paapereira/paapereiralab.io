---
title: "Firestarter by Stephen King (1980)"
slug: "firestarter"
author: "Paulo Pereira"
date: 2015-10-04T22:46:00+01:00
lastmod: 2015-10-04T22:46:00+01:00
cover: "/posts/books/firestarter.jpg"
description: "Finished “Firestarter” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Horror
  - Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Horror
  - Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Firestarter”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [September 29th 1980](/book-publication-year/1980s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/20330984-firestarter)
>
> First, a man and a woman are subjects of a top-secret government experiment designed to produce extraordinary psychic powers.
>
> Then, they are married and have a child. A daughter.
>
> Early on the daughter shows signs of a wild and horrifying force growing within her. Desperately, her parents try to train her to keep that force in check, to "act normal."
>
> Now the government wants its brainchild back - for its own insane ends.