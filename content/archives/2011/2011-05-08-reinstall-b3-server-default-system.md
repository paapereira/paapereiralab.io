---
title: "Reinstall B3 Server default system"
slug: "reinstall-b3-server-default-system"
author: "Paulo Pereira"
date: 2011-05-08T12:19:00+01:00
lastmod: 2011-05-08T12:19:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
---

[B3 Server](http://www.excito.com/bubba/products/technical-specifications.html) comes with a custom Debian (Squeeze) install.

In order to reinstall the all system you will need an USB stick and the latest install image from http://update.excito.net/install/latest/b3/.

## Creating the usb recovery disk

- You'll need an empty usb stick (*it will be formatted*)
- Find out the device reference of your usb stick (check the LABEL in the output)

```bash
sudo blkid
```

- or use gparted (*sudo apt-get gparted*) to find out 

- Remove all existing partitions from the usb stick

	- I used *[gparted](http://gparted.sourceforge.net/)* to remove all partitions

- Create a new primary partition using the *cfdisk* interface to:

	- create a new primary partition
  - make it bootable
  - change it to type W95 FAT32

```bash
sudo cfdisk /dev/sdc
```

![cfdisk](/posts/2011/2011-05-08-reinstall-b3-server-default-system/cfdisk-b3-usb.png)

- Create a new vfat partition

```bash
sudo mkfs.vfat /dev/sdc1
```

- Download the latest B3 install image from http://update.excito.net/install/latest/b3/
- Extract the zip file to the usb stick

```bash
zip b3-install-2.2.1.zip /media/usb_disk
```

## Boot the B3 Server into reinstall mode

- Remove all external drives connect to the B3 Server
- Unplug the power cord
- Connect the b3 server WAN connector to a DHCP server (like your router)
- Put the usb recovery disk in one of the usb ports
- While pressing the B3 power button connect the power cord
- Keep the power button pressed for like a minute (important)
- Check in your DHCP server the B3 IP address, so you'll know ist's connected
- The B3 LED will start green, then purple and finally blue
- Check if you have access at http://b3.local (change your network cable accordingly your setup)

*References:*

- [*B3 User Manual*](http://download.excito.net/web/B3/B3-UM-eng.pdf)