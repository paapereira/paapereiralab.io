---
title: "The Bonehunters (Malazan Book of the Fallen Book 6) by Steven Erikson (2006)"
slug: "the-bonehunters-malazan-book-of-the-fallen-book-6"
author: "Paulo Pereira"
date: 2021-10-14T20:00:00+01:00
lastmod: 2021-10-14T20:00:00+01:00
cover: "/posts/books/the-bonehunters-malazan-book-of-the-fallen-book-6.jpg"
description: "Finished “The Bonehunters (Malazan Book of the Fallen Book 6)” by Steven Erikson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2006
book authors:
  - Steven Erikson
book series:
  - Malazan Book of the Fallen Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Steven Erikson
  - Malazan Book of the Fallen Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
---

Finished “The Bonehunters”.
* Author: [Steven Erikson](/book-authors/steven-erikson)
* First Published: [April 4th 2006](/book-publication-year/2006)
* Series: [Malazan Book of the Fallen](/book-series/malazan-book-of-the-fallen-series) Book 6
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/19746283-the-bonehunters)
>
> The Seven Cities Rebellion has been crushed. Sha'ik is dead. One last rebel force remains, holed up in the city of Y'Ghatan and under the fanatical command of Leoman of the Flails. The prospect of laying siege to this ancient fortress makes the battle-weary Malaz 14th Army uneasy. For it was here that the Empire's greatest champion Dassem Ultor was slain and a tide of Malazan blood spilled. A place of foreboding, its smell is of death. But elsewhere, agents of a far greater conflict have made their opening moves. The Crippled God has been granted a place in the pantheon, a schism threatens and sides must be chosen. Whatever each god decides, the ground-rules have changed, irrevocably, terrifyingly and the first blood spilled will be in the mortal world. A world in which a host of characters, familiar and new, including Heboric Ghost Hands, the possessed Apsalar, Cutter, once a thief now a killer, the warrior Karsa Orlong and the two ancient wanderers Icarium and Mappo, each searching for such a fate as they might fashion with their own hands, guided by their own will. If only the gods would leave them alone. But now that knives have been unsheathed, the gods are disinclined to be kind. There shall be war, war in the heavens.And the prize? Nothing less than existence itself...
> 
> Here is the stunning new chapter in Steven Erikson magnificent 'Malazan Book of the Fallen' - hailed an epic of the imagination and acknowledged as a fantasy classic in the making.