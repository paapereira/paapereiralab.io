---
title: "The Complete Stories of Sherlock Holmes, Volume 1 by Arthur Conan Doyle (2009)"
slug: "the-complete-stories-of-sherlock-holmes-1"
author: "Paulo Pereira"
date: 2015-11-15T22:44:00+00:00
lastmod: 2015-11-15T22:44:00+00:00
cover: "/posts/books/the-complete-stories-of-sherlock-holmes-1.jpg"
description: "Finished “The Complete Stories of Sherlock Holmes, Volume 1” by Arthur Conan Doyle."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2009
book authors:
  - Arthur Conan Doyle
book series:
  - The Complete Stories of Sherlock Holmes
book genres:
  - Fiction
  - Classics
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Arthur Conan Doyle
  - The Complete Stories of Sherlock Holmes
  - Fiction
  - Classics
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “The Complete Stories of Sherlock Holmes Book 1”.
* Author: [Arthur Conan Doyle](/book-authors/arthur-conan-doyle)
* First Published: [October 15th 2009](/book-publication-year/2009)
* Series: [The Complete Stories of Sherlock Holmes](/book-series/the-complete-stories-of-sherlock-holmes) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/10087767-the-complete-stories-of-sherlock-holmes-volume-1)
>
> First appearing in print in 1890, the character of Sherlock Holmes has now become synonymous worldwide with the concept of a super sleuth. His creator, Conan Doyle, imbued his detective hero with intellectual power, acute observational abilities, a penchant for deductive reasoning and a highly educated use of forensic skills. Indeed, Doyle created the first fictional private detective who used what we now recognize as modern scientific investigative techniques.
>
> Doyle ended up writing four novels and 56 short stories featuring Holmes and his companion, Dr. Watson. All but four are told in the first person by Watson, two by Holmes, and two are written in the third person. Together, this series of beautifully written Victorian literature has sold more copies than any other books in the English language, with the exceptions of the Bible and Shakespeare.
>
> Volume 1 in this series consists of two novels, A Study in Scarlet and The Sign of Four, followed by a collection of short stories entitled The Adventures of Sherlock Holmes.