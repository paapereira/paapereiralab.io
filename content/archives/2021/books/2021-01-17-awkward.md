---
title: "Awkward by Ty Tashiro (2017)"
slug: "awkward"
author: "Paulo Pereira"
date: 2021-01-17T18:00:00+00:00
lastmod: 2021-01-17T18:00:00+00:00
cover: "/posts/books/awkward.jpg"
description: "Finished “Awkward: The Science of Why We're Socially Awkward and Why That's Awesome” by Ty Tashiro."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Ty Tashiro
book series:
  - 
book genres:
  - Nonfiction
  - Psychology
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Ty Tashiro
  - Nonfiction
  - Psychology
  - 5/10 Books
  - Audiobook
---

Finished “Awkward: The Science of Why We're Socially Awkward and Why That's Awesome”.
* Author: [Ty Tashiro](/book-authors/ty-tashiro)
* First Published: [April 25th 2017](/book-publication-year/2017)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/50356007-awkward)
>
> In the vein of Quiet and The Geeks Shall Inherit the Earth comes this illuminating look at what it means to be awkward—and how the same traits that make us socially anxious and cause embarrassing faux pas also provide the seeds for extraordinary success.
>
> As humans, we all need to belong. While modern social life can make even the best of us feel gawky, for roughly one in five of us, navigating its challenges is consistently overwhelming—an ongoing maze without an exit. Often unable to grasp social cues or master the skills and grace necessary for smooth interaction, we feel out of sync with those around us. Though individuals may recognize their awkward disposition, they rarely understand why they are like this—which makes it hard for them to know how to adjust their behavior.
>
> Psychologist and interpersonal relationship expert Ty Tashiro knows what it’s like to be awkward. Growing up, he could do math in his head and memorize the earned run averages of every National League starting pitcher. But he couldn’t pour liquids without spilling and habitually forgot to bring his glove to Little League games. In Awkward, he unpacks decades of research into human intelligence, neuroscience, personality, and sociology to help us better understand this widely shared trait. He explores its nature vs. nurture origins, considers how the awkward view the world, and delivers a welcome counterintuitive message: the same characteristics that make people socially clumsy can be harnessed to produce remarkable achievements.
>
> Interweaving the latest research with personal tales and real world examples, Awkward offers reassurance and provides valuable insights into how we can embrace our personal quirks and unique talents to harness our awesome potential—and more comfortably navigate our complex world.