---
title: "Andrea Vernon and the Corporation for UltraHuman Protection (Andrea Vernon Book 1) by Alexander C. Kane (2017)"
slug: "andrea-vernon-and-the-corporation-for-ultrahuman-protection-andrea-vernon-book-1"
author: "Paulo Pereira"
date: 2022-06-05T12:00:00+01:00
lastmod: 2022-06-05T12:00:00+01:00
cover: "/posts/books/andrea-vernon-and-the-corporation-for-ultrahuman-protection-andrea-vernon-book-1.jpg"
description: "Finished “Andrea Vernon and the Corporation for UltraHuman Protection (Andrea Vernon Book 1)” by Alexander C. Kane."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Alexander C. Kane
book series:
  - Andrea Vernon Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Alexander C. Kane
  - Andrea Vernon Series
  - Fiction
  - Fantasy
  - 5/10 Books
  - Audiobook
---

Finished “Andrea Vernon and the Corporation for UltraHuman Protection”.
* Author: [Alexander C. Kane](/book-authors/alexander-c.-kane)
* First Published: [August 22nd 2017](/book-publication-year/2017)
* Series: [Andrea Vernon](/book-series/andrea-vernon-series) Book 1
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40860147-andrea-vernon-and-the-corporation-for-ultrahuman-protection)
>
> Andrea Vernon always thought she would spend her life living in Paris writing thought-provoking historical novels all day and sipping wine on the Seine all night. But the reality is she's drowning in debt, has no prospects, and is forced to move back to Queens, where her parents remind her daily that they are very interested in grandchildren.
> 
> Then, one morning, she is kidnapped, interviewed, and hired as an administrative assistant by the Corporation for UltraHuman Protection. Superheroes for hire, using their powers for good. What could possibly go wrong?
> 
> Lots.
> 
> Her coworkers may be able to shoot lightning out of their hands or have skin made of diamonds, but they refuse to learn how to use the company's database. She has a swell hook-up buddy relationship with The Big Axe, but he's pushing to go exclusive. Then there's the small matter of a giant alien space egg hovering over Yankee Stadium, threatening civilization as we know it.
> 
> Will Andrea find contentment in office drudgery? Can she make a life together with a guy who's eight feet tall and never puts down his axe? And will she ever figure out how her boss likes her coffee?