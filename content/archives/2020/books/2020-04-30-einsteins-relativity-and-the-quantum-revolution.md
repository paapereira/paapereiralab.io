---
title: "Einstein's Relativity and the Quantum Revolution by Richard Wolfson (2000)"
slug: "einsteins-relativity-and-the-quantum-revolution"
author: "Paulo Pereira"
date: 2020-04-30T22:57:00+01:00
lastmod: 2020-04-30T22:57:00+01:00
cover: "/posts/books/einsteins-relativity-and-the-quantum-revolution.png"
description: "Finished “Einstein's Relativity and the Quantum Revolution: Modern Physics for Non-Scientists” by Richard Wolfson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2000
book authors:
  - Richard Wolfson
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Richard Wolfson
  - Nonfiction
  - Science
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/einsteins-relativity-and-the-quantum-revolution
---

Finished “Einstein's Relativity and the Quantum Revolution: Modern Physics for Non-Scientists”.
* Author: [Richard Wolfson](/book-authors/richard-wolfson)
* First Published: [2000](/book-publication-year/2000)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40540283-einstein-s-relativity-and-the-quantum-revolution)
>
> "It doesn't take an Einstein to understand modern physics," says Professor Wolfson at the outset of these twenty-four lectures on what may be the most important subjects in the universe: relativity and quantum physics. Both have reputations for complexity. But the basic ideas behind them are, in fact, simple and comprehensible by anyone. These dynamic and illuminating lectures begin with a brief overview of theories of physical reality starting with Aristotle and culminating in Newtonian or "classical" physics. After that, you'll follow along as Professor Wolfson outlines the logic that led to Einstein's profound theory of special relativity and the simple yet far-reaching insight on which it rests. With that insight in mind, you'll move on to consider Einstein's theory of general relativity and its interpretation of gravitation in terms of the curvature of space and time.
>
> From there, you'll embark on a dazzling exploration of how inquiry into matter at the atomic and subatomic scales led to quandaries that are resolved-or at least clarified-by quantum mechanics, a vision of physical reality so profound and so at odds with our experience that it nearly defies language.
>
> By bringing relativity and quantum mechanics into the same picture, you'll chart the development of fascinating hypotheses about the origin, development, and possible futures of the entire universe, as well as the possibility that physics can produce a "theory of everything" to account for all aspects of the physical world. But the goal throughout these lectures remains the same: to present the key ideas of modern physics in a way that makes them clear to the interested layperson.