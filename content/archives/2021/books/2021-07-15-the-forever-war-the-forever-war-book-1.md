---
title: "The Forever War (The Forever War Book 1) by Joe Haldeman (1974)"
slug: "the-forever-war-the-forever-war-book-1"
author: "Paulo Pereira"
date: 2021-07-15T20:30:00+01:00
lastmod: 2021-07-15T20:30:00+01:00
cover: "/posts/books/the-forever-war-the-forever-war-book-1.jpg"
description: "Finished “The Forever War (The Forever War Book 1)” by Joe Haldeman."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Joe Haldeman
book series:
  - The Forever War Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Joe Haldeman
  - The Forever War Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
aliases:
  - /posts/books/the-forever-war-the-forever-war-book-1
---

Finished “The Forever War”.
* Author: [Joe Haldeman](/book-authors/joe-haldeman)
* First Published: [December 1974](/book-publication-year/1970s)
* Series: [The Forever War](/book-series/the-forever-war-series) Book 1
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35281659-the-forever-war)
>
> The Earth's leaders have drawn a line in the interstellar sand—despite the fact that the fierce alien enemy that they would oppose is inscrutable, unconquerable, and very far away. A reluctant conscript drafted into an elite Military unit, Private William Mandella has been propelled through space and time to fight in the distant thousand-year conflict; to perform his duties without rancor and even rise up through military ranks. Pvt. Mandella is willing to do whatever it takes to survive the ordeal and return home. But "home" may be even more terrifying than battle, because, thanks to the time dilation caused by space travel, Mandella is aging months while the Earth he left behind is aging centuries.