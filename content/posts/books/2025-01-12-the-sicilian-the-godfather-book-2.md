---
title: "The Sicilian (The Godfather Book 2) by Mario Puzo (1984)"
slug: "the-sicilian-the-godfather-book-2"
author: "Paulo Pereira"
date: 2025-01-12T14:00:00+00:00
lastmod: 2025-01-12T14:00:00+00:00
cover: "/posts/books/the-sicilian-the-godfather-book-2.jpg"
description: "Finished The Sicilian (The Godfather Book 2)” by Mario Puzo."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Mario Puzo
book series:
  - The Godfather Series
book genres:
  - Fiction
  - Classics
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Mario Puzo
  - The Godfather Series
  - Fiction
  - Classics
  - 8/10 Books
  - Ebook
---

Finished “The Sicilian”.
* Author: [Mario Puzo](/book-authors/mario-puzo)
* First Published: [November 1, 1984](/book-publication-year/1980s)
* Series: [The Godfather](/book-series/the-godfather-series) Book 2
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6656593-the-sicilian)
>
> The year is 1950. Michael Corleone is nearing the end of his exile in Sicily. The Godfather has commanded Michael to bring a young Sicilian bandit named Salvatore Guiliano back with him to America. But Guiliano is a man entwined in a bloody web of violence and vendettas. In Sicily, Guiliano is a modern day Robin Hood who has defied corruption--and defied the Cosa Nostra. Now, in the land of mist-shrouded mountains and ancient ruins, Michael Corleone's fate is entwined with the dangerous legend of Salvatore Guiliano: warrior, lover, and the ultimate Siciliano.