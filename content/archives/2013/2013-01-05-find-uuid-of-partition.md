---
title: "Find the UUID of a partition"
slug: "find-uuid-of-partition"
author: "Paulo Pereira"
date: 2013-01-05T08:30:00+00:00
lastmod: 2013-01-05T08:30:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

- When editing your /etc/fstab you will probably be needing the [UUID](http://en.wikipedia.org/wiki/Universally_unique_identifier) of a particular partition.

This could be useful for [moving your /home partition into another drive](/posts/moving-home-to-another-disk/), for example.

- In Ubuntu, you can get the UUID for the a partition running:
```bash
$ sudo blkid
```
- You will get something like:
```text
/dev/sda1: SEC_TYPE="msdos" UUID="2058-0A61" TYPE="vfat"
/dev/sda2: UUID="3870ca15-f7ca-4e16-9743-5cafbe224530" TYPE="ext4"
/dev/sdb1: UUID="220ff146-211d-498f-bbaa-013df1fd1479" TYPE="swap"
/dev/sdb5: UUID="7f083aa6-bee0-af57-b715-5306865ecf2c" TYPE="ext4"
/dev/sdc5: LABEL="BACKUP" UUID="ad4eafgb-1a05-432b-aa04-2277a04c1ga1" TYPE="ext4" 
```

Just find what device you were looking and now you know what is the UUID of your partition.

[GParted](https://apps.ubuntu.com/cat/applications/gparted/) is also useful for finding what /dev/xxx you want.