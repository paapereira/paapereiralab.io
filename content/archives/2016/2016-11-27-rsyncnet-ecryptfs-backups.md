---
title: "Backups using ecryptfs and rsync.net"
slug: "rsyncnet-ecryptfs-backups"
author: "Paulo Pereira"
date: 2016-11-27T21:12:33+00:00
lastmod: 2016-11-27T21:12:33+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - rsync.net
  - ecryptfs
---

I use [rsync.net](https://rsync.net) for my cloud/offline backups.

Here’s how to do encrypted backups using `ecryptfs`.

- Install `ecryptfs-utils`

```bash
sudo pacman -S ecryptfs-utils
```

- Generate a passphrase

```bash
ecryptfs-setup-private --nopwcheck --noautomount
```

- Mount rsync.net using `sshfs` and create a backup folder

```bash
mkdir -p /home/your_user/rsync.net/
sshfs 99999@server.rsync.net: /home/your_user/rsync.net/
mkdir -p /home/your_user/rsync.net/backups/
```

- Mount the backups folder as an encrypted filesystem and copy some files

```bash
sudo mount -t ecryptfs ~/.Private /home/your_user/rsync.net/backups -o key=passphrase,ecryptfs_cipher=aes,ecryptfs_key_bytes=24,ecryptfs_passthrough=n,ecryptfs_enable_filename_crypto=n
```

- Unmount rsync.net and the encrypted filesystem mount

```bash
sudo umount ~/Private
fusermount -u /home/your_user/rsync.net/
```