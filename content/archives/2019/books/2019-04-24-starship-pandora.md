---
title: "Starship Pandora by B.V. Larson (2018)"
slug: "starship-pandora"
author: "Paulo Pereira"
date: 2019-04-24T22:43:00+01:00
lastmod: 2019-04-24T22:43:00+01:00
cover: "/posts/books/starship-pandora.jpg"
description: "Finished “Starship Pandora” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - B.V. Larson
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Starship Pandora”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [2018](/book-publication-year/2018)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43510818-starship-pandora)
>
> The mysterious alien race known as the Ancients vanished before Earth’s last ice age - but not before building interconnected rings that form a highway system across the galaxy. Humanity has used these rings for years but has never figured out how to build its own, or even how to alter the connection pattern.
>
> Now, the Imperial starship Pandora is on a mission to discover and explore new interstellar connection points. While on Venus, an unexpected guest arrives - Marvin, a rogue robot who puts his strange mind to work on reconnecting the rings into a new pattern. And everything goes haywire. Grotesque aliens come through the ring and overrun Pandora’s base camp. The good news is the aliens can be killed. The bad news is the aliens keep coming, and no one knows how to turn off the ring that leads directly to their home world.
>
> Even if the aliens are defeated - there’s still Marvin to contend with.