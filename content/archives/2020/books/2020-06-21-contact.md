---
title: "Contact by Carl Sagan (1985)"
slug: "contact"
author: "Paulo Pereira"
date: 2020-06-21T22:43:00+01:00
lastmod: 2020-06-21T22:43:00+01:00
cover: "/posts/books/contact.png"
description: "Finished “Contact” by Carl Sagan."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Carl Sagan
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Carl Sagan
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Audiobook
aliases:
  - /posts/books/contact
---

Finished “Contact”.
* Author: [Carl Sagan](/book-authors/carl-sagan)
* First Published: [September 1985](/book-publication-year/1980s)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17226485-contact)
>
> The future is here...in an adventure of cosmic dimension. In December, 1999, a multinational team journeys out to the stars, to the most awesome encounter in human history. Who - or what - is out there? In Cosmos, Carl Sagan explained the universe. In Contact, he predicts its future - and our own.