---
title: "Sun Virtualbox"
slug: "sun-virtualbox"
author: "Paulo Pereira"
date: 2008-07-30T23:03:00+01:00
lastmod: 2008-07-30T23:03:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Virtualbox
aliases:
  - /posts/sun-virtualbox/
---

I have [Virtualbox OSE](/posts/windows-under-linux/) (Open Source Edition) installed. I use it basically to use Office, when needed.

Currently I only restart to Windows to sync my iPod Touch, and that's because Virtualbox OSE doesn't support USB.

But there's a version, provided by Sun, that supports USB devices.

## Uninstall Virtualbox OSE

If you already have Virtualbox installed go to Applications > Add/Remove... and look for *virtualbox* to uninstall your current version.

I also went to System > Administration > Synaptic Package Manager and also looked for *virtualbox* to remove every trace of it.

## Install Sun Virtualbox (currently v1.6.2 v1.6.4)

Go to [Virtualbox download site](http://www.virtualbox.org/wiki/Downloads) and go to Binaries (all platforms).

Select your Platform (Ubuntu 8.04 x86 in my case), agree to the license and continue to the download.

After the download is complete start the installer (or double click the .deb file):

```bash
sudo dpkg -i virtualbox_1.6.2-31466_Ubuntu_hardy_i386.deb
```

And it's that easy.

## Virtualbox Users

- Go to System > Administration > Users and Groups
- Unlock the Settings and go to Manage Groups
- Find the *vboxusers* group and click Properties
- Select your *username* in the Group Members list 

## USB Support

By default the usb support is disabled. To enable it do the following:

```bash
sudo gedit /etc/init.d/mountdevsubfs.sh
```

Now look for:

```text
#
# Magic to make /proc/bus/usb work
#
#mkdir -p /dev/bus/usb/.usbfs
#domount usbfs "" /dev/bus/usb/.usbfs -obusmode=0700,devmode=0600,listmode=0644
#ln -s .usbfs/devices /dev/bus/usb/devices
#mount --rbind /dev/bus/usb /proc/bus/usb
```

And replace with (remove # from the last 4 lines):

```text
#
# Magic to make /proc/bus/usb work
#
mkdir -p /dev/bus/usb/.usbfs
domount usbfs "" /dev/bus/usb/.usbfs -obusmode=0700,devmode=0600,listmode=0644
ln -s .usbfs/devices /dev/bus/usb/devices
mount --rbind /dev/bus/usb /proc/bus/usb
```

Restart your machine.

## Update Virtualbox to a new version

When a new version is available you just need to download it and double click the .deb file. It's that easy.

## Last tips

Sun Virtualbox is now available at Applications > System Tools > Sun xVM VirtualBox.

In my case, at the end of the installation, Virtualbox found my previous virtual machine (Windows XP) and upgraded it to the new version, allowing me to use that virtual machine.

Because of this, every configuration was set and I only had to activate the USB support in my virtual machine.

Check my [Virtualbox OSE](/posts/windows-under-linux/) post and see how to:

- Create a new Virtual Machine
- Share folders with the Virtual Machine (install Guest Additions for Windows)
- VirtualBox running modes

Note: if you had Guest Additions for Windows in your old virtual machine, uninstall it and install the new 1.6.2 version.