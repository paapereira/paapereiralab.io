---
title: "The Real Sherlock by Lucinda Hawksley (2019)"
slug: "the-real-sherlock"
author: "Paulo Pereira"
date: 2021-08-20T21:00:00+01:00
lastmod: 2021-08-20T21:00:00+01:00
cover: "/posts/books/the-real-sherlock.jpg"
description: "Finished “The Real Sherlock” by Lucinda Hawksley."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Lucinda Hawksley
book series:
  - 
book genres:
  - Nonfiction
  - Biography
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Lucinda Hawksley
  - Nonfiction
  - Biography
  - 5/10 Books
  - Audiobook
---

Finished “The Real Sherlock”.
* Author: [Lucinda Hawksley](/book-authors/lucinda-hawksley)
* First Published: [May 23rd 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44435083-the-real-sherlock)
>
> Sherlock Holmes is the most portrayed fictional character of all time, and he has been reimagined by actors, playwrights and directors over centuries - but who is the creator behind the detective?
>
> Conan Doyle’s own life was often stranger than fiction, and his most famous characters’ stories and personalities bear more than a passing resemblance to his own life and his closest friends.
>
> Biographer and broadcaster Lucinda Hawksley gains unprecedented access to a treasure trove of Doyle’s never-before-seen personal letters and diaries. This is a chance for Sherlock fans to see their detective hero and his creator as they’ve never seen them before. Through interviews with Doyle aficionados, academics, actors and family members, we explore Doyle’s travels and sailing adventures across the globe, his pioneering work as a doctor, his life in the Freemasons and his fights against miscarriages of justice. As well as his many triumphs, we will also explore the challenges he faced, from the death of his first wife and son to the initial rejection he faced as an author.
>
> We will also look beyond Sherlock to Doyle’s other great works including his fantasy and science fiction novels and hear how one of his most famous, The Lost World, part-inspired Michael Crichton’s book of the same name, which became the successful Jurassic Park film franchise.