---
title: "The Time Machine by H.G. Wells (1895)"
slug: "the-time-machine"
author: "Paulo Pereira"
date: 2018-10-02T21:51:00+01:00
lastmod: 2018-10-02T21:51:00+01:00
cover: "/posts/books/the-time-machine.jpg"
description: "Finished “The Time Machine” by H.G. Wells."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - H.G. Wells
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
  - Classics
book scores:
  - 10/10 Books
tags:
  - Book Log
  - H.G. Wells
  - Fiction
  - Science Fiction
  - Classics
  - 10/10 Books
  - Audiobook
aliases:
  - /posts/books/the-time-machine
---

Finished “The Time Machine”.
* Author: [H.G. Wells](/book-authors/h.g.-wells)
* First Published: [May 7th 1895](/book-publication-year/1800s)
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40784019-the-time-machine)
>
> So begins the Time Traveller’s astonishing firsthand account of his journey 800,000 years beyond his own era—and the story that launched H.G. Wells’s successful career and earned him his reputation as the father of science fiction. With a speculative leap that still fires the imagination, Wells sends his brave explorer to face a future burdened with our greatest hopes...and our darkest fears. A pull of the Time Machine’s lever propels him to the age of a slowly dying Earth.  There he discovers two bizarre races—the ethereal Eloi and the subterranean Morlocks—who not only symbolize the duality of human nature, but offer a terrifying portrait of the men of tomorrow as well.  Published in 1895, this masterpiece of invention captivated readers on the threshold of a new century. Thanks to Wells’s expert storytelling and provocative insight, The Time Machine will continue to enthrall readers for generations to come.