---
title: "Wally Roux, Quantum Mechanic by Nick Carr (2019)"
slug: "wally-roux-quantum-mechanic"
author: "Paulo Pereira"
date: 2020-07-03T22:03:00+01:00
lastmod: 2020-07-03T22:03:00+01:00
cover: "/posts/books/wally-roux-quantum-mechanic.png"
description: "Finished “Wally Roux, Quantum Mechanic” by Nick Carr."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Nick Carr
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 3/10 Books
tags:
  - Book Log
  - Nick Carr
  - Fiction
  - Science Fiction
  - 3/10 Books
  - Audiobook
---

Finished “Wally Roux, Quantum Mechanic”.
* Author: [Nick Carr](/book-authors/nick-carr)
* First Published: [August 1st 2019](/book-publication-year/2019)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/47503394-wally-roux-quantum-mechanic)
>
> A teenage genius with a big imagination, Wally just moved to Savannah, GA from Maine with his mom, who adopted Wally when he was an infant. In this charming and sweet solo show, Wally investigates a hiccup in the spacetime fabric of his neighborhood which is causing a number of strange events and occurrences. Part science fiction and part coming-of-age story, Wally Roux charts one boy's journey of self-discovery and identity.