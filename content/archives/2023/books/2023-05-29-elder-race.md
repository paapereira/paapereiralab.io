---
title: "Elder Race by Adrian Tchaikovsky (2021)"
slug: "elder-race"
author: "Paulo Pereira"
date: 2023-05-29T21:00:00+01:00
lastmod: 2023-05-29T21:00:00+01:00
cover: "/posts/books/elder-race.jpg"
description: "Finished “Elder Race” by Adrian Tchaikovsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - Adrian Tchaikovsky
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “Elder Race”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
* First Published: [November 16, 2021](/book-publication-year/2021)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/56815367-elder-race)
>
> A junior anthropologist on a distant planet must help the locals he has sworn to study to save a planet from an unbeatable foe.
> 
> Lynesse is the lowly Fourth Daughter of the queen, and always getting in the way.
> 
> But a demon is terrorizing the land, and now she’s an adult (albeit barely) with responsibilities (she tells herself). Although she still gets in the way, she understands that the only way to save her people is to invoke the pact between her family and the Elder sorcerer who has inhabited the local tower for as long as her people have lived here (though none in living memory has approached it).
> 
> But Elder Nyr isn’t a sorcerer, and he is forbidden to help, and his knowledge of science tells him the threat cannot possibly be a demon…