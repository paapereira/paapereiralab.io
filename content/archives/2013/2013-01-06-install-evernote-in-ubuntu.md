---
title: "Install Evernote in Ubuntu"
slug: "install-evernote-in-ubuntu"
author: "Paulo Pereira"
date: 2013-01-06T22:14:00+00:00
lastmod: 2013-01-06T22:14:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Evernote
  - PlayOnLinux
  - Wine
aliases:
  - /posts/install-evernote-in-ubuntu/
---

[Evernote](http://www.evernote.com/) doesn't have a Linux client :( (not cool)

![Evernote](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote.png)

Still, it is a great service and I use it every day both for personal  and professional ends. I use it in my work Windows based laptop and in [my Android devices](/tags/android).

I use [Everpad](http://www.omgubuntu.co.uk/2012/09/use-evernote-in-ubuntu-with-everpad) and it's a really great alternative to a proper client and it as Unity integration. Great!

Still a client is very handy, and with [Wine](http://www.winehq.org/) you can use the Windows client in your Linux machine.

## The setup

I will be using [PlayOnLinux](http://www.playonlinux.com/en/) to install Evernote. Basically PlayOnLinux is a great front-end to install Windows applications and games using Wine.

![PlayOnLinux](/posts/2013/2013-01-06-install-evernote-in-ubuntu/playonlinux.png)

For this post, and for getting screenshots I'm using a virtual machine with Ubuntu 12.04 LTS and PlayOnLinux 4.0.14.
I had some problems in my Ubuntu 12.10 installation with PlayOnLinux 4.1.8. More on this at the end of the post.

I'm also using Evernote 4.6.0.7670.

## Installing PlayOnLinux

You can download PlayOnLinux from their [website](http://www.playonlinux.com/en/download.html) (at this moment the latest release is 4.1.8) or you can install it using the following command:

```bash
sudo apt-get install playonlinux
```

This will install PlayOnLinux 4.0.14.

When you open PlayOnLinux for the first time it will create a default wine drive. Just click Next.

After refreshing you will notice that on the bottom there's a note  saying that there is a new version of PlayOnLinux. Ignore it for now.
If you still want to install it, go to their [website](http://www.playonlinux.com/en/download.html) and install  If you have problems ending the Evernote installation check at the end  of this post how you can manually finish the installation.

## Installing Evernote

First download Evernote from their [website](http://evernote.com/download/get.php?file=Win).

Then open PlayOnLinux, and go to Configure.

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote1.png)
*Select "Configure" to create a new virtual drive*

Click on the New button to create a new virtual drive called "Evernote". 

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote2.png)
*Select "New"*

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote3.png)
*You now have a specific virtual drive for Evernote*

After that click on the Install button in the main screen, and select "Install a non-listed program" in the bottom.

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote4.png)
*Select "Install a non-listed program"*

Select the options as the following images.

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote5.png)
*Select the "Edit or update an existing application" option*

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote6.png)
*Check the "Show virtual drives option" and select "Evernote"*

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote7.png)
*Click Next*

Select your downloaded Evernote installation file.

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote8.png)
*Click "Browse" and find your Evernote installation file*

Install Evernote as you would in a Windows environment.

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote9.png)
*Install Evernote as in Windows*

You can sign in into your Evernote account (or create a new one).

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote10.png)
*Evernote*

Close Evernote and in the PlayOnLinux screen select the Evernote.exe file and name it Evernote. This will create a shortcut.

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote11.png)
*Select "Evernote.exe" to create a shortcut*

That's it. Evernote is installed.

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote12.png)
*Evernote is installed*

A shortcut to open Evernote is created in the Desktop. Keep it there or move it to another place.
You can also drag it into the Unity bar.

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote13.png)
*Drag the shortcut to the Unity bar*

Now you can easily use Evernote as if it was a Linux application.

![Configure](/posts/2013/2013-01-06-install-evernote-in-ubuntu/evernote14.png)
*Evernote launched from the Unity bar*

## Problems in PlayOnLinux 4.1.8

In Ubuntu 12.04 LTS and using PlayOnLinux 4.0.14 everything works fine.

In my actual setup (Ubuntu 12.10 and PlayOnLinux 4.1.8), before the step where we choose the "Evernote.exe" file to create a shortcut,  PlayOnLinux seems to stop. It never shows me the screen to choose the  exe file.
My only choice is to close the screen and Evernote doesn't appear on  PlayOnLinux main screen. Still, apart from the shortcut, Evernote was  installed!

Here's how I solved it:

- Go into PlayOnLinux shortcut folder
```bash
cd ~/.PlayOnLinux/shortcuts
```
- Create a new file
```bash
vi Evernote
```
- Add the following (replacing *youruser* with your username)
```text
#!/bin/bash
[ "$PLAYONLINUX" = "" ] && exit 0
source "$PLAYONLINUX/lib/sources"
export WINEPREFIX="/home/youruser/.PlayOnLinux/wineprefix/Evernote"
export WINEDEBUG="-all"
cd "/home/youruser/.PlayOnLinux/wineprefix/Evernote/drive_c/./Program Files/Evernote/Evernote"
POL_Wine Evernote.exe "$@"
```
- Now Evernote appears on PlayOnLinux, but the icon is wrong
- Go into Configure, select your Evernote virtual drive and click on  "Make a new shortcut from this virtual drive". This option launch the  screen where you choose "Evernote.exe" and you can follow the next steps as showed before.