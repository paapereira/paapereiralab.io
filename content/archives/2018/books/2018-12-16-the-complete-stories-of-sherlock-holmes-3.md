---
title: "The Complete Stories of Sherlock Holmes, Volume 3 by Arthur Conan Doyle (2010)"
slug: "the-complete-stories-of-sherlock-holmes-3"
author: "Paulo Pereira"
date: 2018-12-16T20:46:00+00:00
lastmod: 2018-12-16T20:46:00+00:00
cover: "/posts/books/the-complete-stories-of-sherlock-holmes-3.jpg"
description: "Finished “The Complete Stories of Sherlock Holmes, Volume 3” by Arthur Conan Doyle."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2010
book authors:
  - Arthur Conan Doyle
book series:
  - The Complete Stories of Sherlock Holmes
book genres:
  - Fiction
  - Mystery
  - Classics
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Arthur Conan Doyle
  - The Complete Stories of Sherlock Holmes
  - Fiction
  - Mystery
  - Classics
  - 7/10 Books
  - Audiobook
---

Finished “The Complete Stories of Sherlock Holmes Book 3”.
* Author: [Arthur Conan Doyle](/book-authors/arthur-conan-doyle)
* First Published: [2010](/book-publication-year/2010)
* Series: [The Complete Stories of Sherlock Holmes](/book-series/the-complete-stories-of-sherlock-holmes) Book 3
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13626136-the-complete-stories-of-sherlock-holmes-volume-3)
>
> Arthur Conan Doyle never wasted time in getting his stories moving. His plots are always direct and refreshingly lucid, and the narrative has a velocity that sweeps you along right to the end. This was no doubt a large part of his immense worldwide success. Not surprisingly, each time he tried to end the series, his fans would howl in protest. But, as he says in the preface to his last collection of Sherlock Holmes stories, all good things must come to an end. And so it is with this series, as we have now arrived at the end of the Sherlock Holmes tales, Conan Doyle's most magnificent creation.
>
> This last volume contains one novel, The Valley of Fear, and two collections of short stories: His Last Bow and The Case Book of Sherlock Holmes.