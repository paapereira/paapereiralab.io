---
title: "Ubuntu Certificate of Authenticity"
slug: "ubuntu-certificate-of-authenticity"
author: "Paulo Pereira"
date: 2009-02-27T13:25:00+00:00
lastmod: 2009-02-27T13:25:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

[Get your copy](http://ubuntuforums.org/showthread.php?p=3629848)... today!

![Ubuntu Certificate of Authenticity](/posts/2009/2009-02-27-ubuntu-certificate-of-authenticity/ubuntucert.png)