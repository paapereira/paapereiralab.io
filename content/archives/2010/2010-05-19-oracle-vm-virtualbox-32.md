---
title: "Oracle VM VirtualBox 3.2"
slug: "oracle-vm-virtualbox-32"
author: "Paulo Pereira"
date: 2010-05-19T22:44:00+01:00
lastmod: 2010-05-19T22:44:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 10.04
  - Lucid Lynx
  - VirtualBox
---

After the acquisition of Sun Microsystems by Oracle Corporation VirtualBox is now called Oracle VM VirtualBox.

If you have a previous version already installed you need to manually install the new one. This is the 3.2.0 version.

## Install

- Go to System > Administration > Software Sources and Third-Party Software.
- Add the following sources for Lucid (check your distribution [here](http://www.virtualbox.org/wiki/Linux_Downloads))
- Download and register Oracle authentication key

```bash
wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | sudo apt-key add -
```

- Be sure your source list is updated

```bash
sudo apt-get update
```

- Be sure you have [dkms](http://linux.dell.com/projects.shtml) installed (so Virtualbox host kernel is automatically updated as you update your system kernel)

```bash
sudo apt-get install dkms
```

- Install Virtualbox 3.2

```bash
sudo apt-get install virtualbox-3.2
```

- Logoff and login again
- Start Virtualbox at Applications > System Tools > Sun VirtualBox
- Don't forget to install/reinstall the Guest Additions in all your  virtual machines (after starting the virtual machine go to Devices >  Install Guest Additions)