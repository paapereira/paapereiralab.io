---
title: "Free Software in Portugal with SAPO"
slug: "free-software-in-portugal-with-sapo"
author: "Paulo Pereira"
date: 2008-07-23T21:04:00+01:00
lastmod: 2008-07-23T21:04:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
---

[SAPO](http://www.sapo.pt/), a [portuguese](http://en.wikipedia.org/wiki/Portugal) ISP, and for whom might interess, hosts Free Software (*Software Livre*, in portuguese).

The *[Software Livre](http://softwarelivre.sapo.pt/)* project is still in an early stage, but I think that it's a great initiative.

A list of [hosted projects](http://softwarelivre.sapo.pt/geral/wiki/ListaDeProjectos) and an english [Service Catalog](http://developers.sapo.pt/developers/wiki/Catalog_EN) for Developers are available.

Currently there is a list of [possible projects](http://softwarelivre.sapo.pt/geral/wiki/SummerbitsOrientadores2008) within the scope of SAPO Summerbits 2008.

The *Software Livre* platform is 100% free software and it's running Debian GNU/Linux with trac, mailman, svn, etc.

The following entities are supporting this project:

[ANSOL - Associação Nacional para o Software Livre](http://www.ansol.org/) (National Free Software Association)

[Caixa Mágica - Distribuição de Linux Portuguesa](http://www.caixamagica.pt/) (Linux Portuguese Distribution)

[DRI - Consultoria Informática](http://www.dri.pt/) (An Information Technology Companie)