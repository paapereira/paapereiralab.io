---
title: "The Testaments (The Handmaid's Tale Book 2) by Margaret Atwood (2019)"
slug: "the-testaments-the-handmaids-tale-book-2"
author: "Paulo Pereira"
date: 2023-01-26T20:00:00+00:00
lastmod: 2023-01-26T20:00:00+00:00
cover: "/posts/books/the-testaments-the-handmaids-tale-book-2.jpg"
description: "Finished “The Testaments (The Handmaid's Tale Book 2)” by Margaret Atwood."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Margaret Atwood
book series:
  - The Handmaids Tale Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Margaret Atwood
  - The Handmaids Tale Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “The Testaments”.
* Author: [Margaret Atwood](/book-authors/margaret-atwood)
* First Published: [September 10, 2019](/book-publication-year/2019)
* Series: [The Handmaid's Tale](/book-series/the-handmaids-tale-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/55263876-the-testaments)
>
> More than fifteen years after the events of The Handmaid’s Tale, the theocratic regime of the Republic of Gilead maintains its grip on power, but there are signs it is beginning to rot from within. At this crucial moment, the lives of three radically different women converge, with potentially explosive results.
> 
> Two have grown up as part of the first generation to come of age in the new order. The testimonies of these two young women are joined by a third voice: a woman who wields power through the ruthless accumulation and deployment of secrets.