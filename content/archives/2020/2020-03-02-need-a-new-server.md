---
title: "🖥️ My CPU Fan is starting to die - I need a new server"
slug: "need-a-new-server"
author: "Paulo Pereira"
date: 2020-03-02T22:11:10+01:00
lastmod: 2020-03-02T22:11:10+01:00
cover: "/posts/2020/2020-03-02-need-a-new-server/intel-nuc7i3bnh.jpg"
description: "So, my NUC server CPU fan is starting to go.\n

It has been making a lot of noise. I already tried to clean it and turn off the server overnight. It’s better, but it’s starting to die."
draft: false
toc: false
categories:
  - Hardware
tags:
  - Linux
  - NUC
---

So, my NUC server CPU fan is starting to go.

It has been making a lot of noise. I already tried to clean it and turn off the server overnight. It’s better, but it’s starting to die.

Time to upgrade.

Basically I will still go with a NUC i3 with 8GB of RAM. It’s enough for my needs.
