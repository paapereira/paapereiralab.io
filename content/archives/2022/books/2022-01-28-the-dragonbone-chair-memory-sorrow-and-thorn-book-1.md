---
title: "The Dragonbone Chair (Memory, Sorrow, and Thorn Book 1) by Tad Williams (1988)"
slug: "the-dragonbone-chair-memory-sorrow-and-thorn-book-1"
author: "Paulo Pereira"
date: 2022-01-28T19:30:00+00:00
lastmod: 2022-01-28T19:30:00+00:00
cover: "/posts/books/the-dragonbone-chair-memory-sorrow-and-thorn-book-1.jpg"
description: "Finished “The Dragonbone Chair (Memory, Sorrow, and Thorn Book 1)” by Tad Williams."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Tad Williams
book series:
  - Memory, Sorrow, and Thorn Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Tad Williams
  - Memory, Sorrow, and Thorn Series
  - Fiction
  - Fantasy
  - 10/10 Books
  - Ebook
---

Finished “The Dragonbone Chair”.
* Author: [Tad Williams](/book-authors/tad-williams)
* First Published: [October 25th 1988](/book-publication-year/1980s)
* Series: [Memory, Sorrow, and Thorn](/book-series/memory-sorrow-and-thorn-series) Book 1
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8242507-the-dragonbone-chair)
>
> With The Dragonbone Chair, Tad Williams introduced readers to the incredible fantasy world of Osten Ard. His beloved, internationally bestselling series Memory, Sorrow, and Thorn inspired a generation of modern fantasy writers, including George R.R. Martin, Patrick Rothfuss, and Christopher Paolini, and defined Tad Williams as one of the most important fantasy writers of our time.