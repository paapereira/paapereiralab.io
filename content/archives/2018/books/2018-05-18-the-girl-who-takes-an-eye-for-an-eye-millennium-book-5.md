---
title: "The Girl Who Takes an Eye for an Eye (Millennium Book 5) by David Lagercrantz (2017)"
slug: "the-girl-who-takes-an-eye-for-an-eye-millennium-book-5"
author: "Paulo Pereira"
date: 2018-05-18T22:40:00+01:00
lastmod: 2018-05-18T22:40:00+01:00
cover: "/posts/books/the-girl-who-takes-an-eye-for-an-eye-millennium-book-5.jpg"
description: "Finished “The Girl Who Takes an Eye for an Eye (Millennium Book 5)” by David Lagercrantz."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - David Lagercrantz
book series:
  - Millennium Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 6/10 Books
tags:
  - Book Log
  - David Lagercrantz
  - Millennium Series
  - Fiction
  - Mystery
  - 6/10 Books
  - Audiobook
---

Finished “The Girl Who Takes an Eye for an Eye”.
* Author: [David Lagercrantz](/book-authors/david-lagercrantz)
* First Published: [September 7th 2017](/book-publication-year/2017)
* Series: [Millennium](/book-series/millennium-series) Book 5
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36249257-the-girl-who-takes-an-eye-for-an-eye)
>
> The girl with the dragon tattoo is not given to forgiveness.
>
> Lisbeth Salander has been forged by a brutal childhood and horrific abuse. And repeated attempts on her life.
>
> The ink embedded in her skin is a constant reminder of her pledge to fight against the injustice she finds on every side.
>
> Confinement to the secure unit of a women's prison is intended as a punishment. Instead, Lisbeth finds herself in relative safety. Flodberga is a failing prison, effectively controlled by the inmates, and for a computer hacker of her exceptional gifts there are no boundaries.
>
> Mikael Blomkvist makes the long trip to visit every week - and receives a lead to follow for his pains, one that could provide an important exposé for Millennium: Salander tells him to check out Leo Mannheimer, a seemingly reputable stockbroker from Stockholm, somehow connected to the long-ago death of a child psychologist - and to the psychiatric unit where Lisbeth was an involuntary patient as a child.
>
> Lisbeth knows she is coming closer to solving the mysteries of her early life, and even within the confines of the prison, she feels the deadly influence exerted by her twin sister. She cannot stand by as racial and religious conflicts run unchecked amongst the community around her, manipulated by criminal forces far beyond the prison walls.
>
> Salander will stand up for what she believes in. She will find out the truth. Whatever the cost...
>
> The tension, power and unstoppable force of The Girl Who Takes an Eye for an Eye are inspired by Stieg Larsson's Millennium trilogy, as Salander and Blomkvist continue the fight for justice that has thrilled millions of readers across the world.