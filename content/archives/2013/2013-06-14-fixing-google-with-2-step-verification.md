---
title: "Fixing Google with 2-Step Verification Online Account in Gnome 3.8"
slug: "fixing-google-with-2-step-verification"
author: "Paulo Pereira"
date: 2013-06-14T19:03:00+01:00
lastmod: 2013-06-14T19:03:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - GNOME
---

In my [Arch](/tags/arch-linux) adventures I added my Google account in the Online Accounts.

![Settings > Online Accounts](/posts/2013/2013-06-14-fixing-google-with-2-step-verification/settings.png)

Having the 2-Step Verification activated, after I logged in with my  password, and entered the token, the account the credentials expired.

After trying again, same thing.

I'm in Arch with GNOME 3.8.3.

So, after some googling I found a solution in the [Arch forum](https://bbs.archlinux.org/viewtopic.php?id=152973).

- Generate a Google Application-specific password;
- Go to Passwords and Keys (seahorse);
- Find your Google credentials (something like "GOA google credentials...");
- Right click > Properties;
- Click on Show password;
- Replace your password with the application-specific password you generated:

> {'authorization_code': (......), 'password': <'**YOUR_APPLIC_SPECIFIC_PASS**'>}

That's it! You calendar and email should be synced in Evolution.