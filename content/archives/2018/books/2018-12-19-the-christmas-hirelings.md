---
title: "The Christmas Hirelings by Mary Elizabeth Braddon (1894)"
slug: "the-christmas-hirelings"
author: "Paulo Pereira"
date: 2018-12-19T22:40:00+00:00
lastmod: 2018-12-19T22:40:00+00:00
cover: "/posts/books/the-christmas-hirelings.jpg"
description: "Finished “The Christmas Hirelings” by Mary Elizabeth Braddon."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - Mary Elizabeth Braddon
book series:
  - 
book genres:
  - Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Mary Elizabeth Braddon
  - Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Christmas Hirelings”.
* Author: [Mary Elizabeth Braddon](/book-authors/mary-elizabeth-braddon)
* First Published: [1894](/book-publication-year/1800s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43199023-the-christmas-hirelings)
>
> A heartwarming festive tale of redemption and cheer, The Christmas Hirelings is the story of a lonely widower who reluctantly invites a group of children to his manor for Christmas. It showcases the benefits of opening your doors and hearts during the holiday season.
>
> First published in 1893, The Christmas Hirelings is a delightful short tale of compassion and redemption. With his sole surviving daughter Sybil estranged, and his wife passed away, there is a deficit of Christmas cheer in the near-vacant country mansion of Sir John Penlyon. In an attempt to improve the festive mood, Sir John's good friend Mr Danby suggests that three children be 'hired' over the Christmas period in the hope of brightening the mood. After overcoming his initial reluctance, Sir John soon opens the doors of Penlyon Castle to Lassie, Laddie and little Moppet. Their arrival does more than signal the coming of Christmas however, as Sir John finds himself re-evaluating his past actions.