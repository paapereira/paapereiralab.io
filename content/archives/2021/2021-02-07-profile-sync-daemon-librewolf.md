---
title: "Using profile-sync-daemon in LibreWolf"
slug: "profile-sync-daemon-librewolf"
author: "Paulo Pereira"
date: 2021-02-07T14:14:08+00:00
lastmod: 2021-02-07T14:14:08+00:00
description: "I talked about [LibreWolf](/posts/librewolf/) last month, and how I made [profile-sync-daemon](https://wiki.archlinux.org/index.php/profile-sync-daemon) work.\n

The way I did made Firefox stop working, so this is the better way."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - LibreWolf
aliases:
  - /posts/profile-sync-daemon-librewolf/
---

I talked about [LibreWolf](/posts/librewolf/) last month, and how I made [profile-sync-daemon](https://wiki.archlinux.org/index.php/profile-sync-daemon) work.

The way I did made Firefox stop working, so this is the better way.

Fixing what I did and set it correctly:

```bash
sudo mv /usr/share/psd/browsers/firefox_bck /usr/share/psd/browsers/firefox
sudo cp /usr/share/psd/browsers/firefox /usr/share/psd/browsers/librewolf
```

Replacing `.mozilla/firefox` with `.librewolf` in `/usr/share/psd/browsers/librewolf` and restart `psd`.

```bash
sudo vi /usr/share/psd/browsers/librewolf
systemctl --user stop psd.service
systemctl --user start psd.service

psd p
```