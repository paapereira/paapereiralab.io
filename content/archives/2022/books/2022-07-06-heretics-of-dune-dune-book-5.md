---
title: "Heretics of Dune (Dune Book 5) by Frank Herbert (1984)"
slug: "heretics-of-dune-dune-book-5"
author: "Paulo Pereira"
date: 2022-07-06T21:00:00+01:00
lastmod: 2022-07-06T21:00:00+01:00
cover: "/posts/books/heretics-of-dune-dune-book-5.jpg"
description: "Finished Heretics of Dune (Dune Book 5)” by Frank Herbert."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Frank Herbert
book series:
  - Dune Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Frank Herbert
  - Dune Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “Heretics of Dune”.
* Author: [Frank Herbert](/book-authors/frank-herbert)
* First Published: [April 1st 1984](/book-publication-year/1980s)
* Series: [Dune](/book-series/dune-series) Book 5
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/55712125-heretics-of-dune)
>
> Leto Atreides, the God Emperor of Dune, is dead. In the fifteen hundred years since his passing, the Empire has fallen into ruin. The great Scattering saw millions abandon the crumbling civilization and spread out beyond the reaches of known space. The planet Arrakis—now called Rakis—has reverted to its desert climate, and its great sandworms are dying.
> 
> Now the Lost Ones are returning home in pursuit of power. And as these factions vie for control over the remnants of the Empire, a girl named Sheeana rises to prominence in the wastelands of Rakis, sending religious fervor throughout the galaxy. For she possesses the abilities of the Fremen sandriders—fulfilling a prophecy foretold by the late God Emperor....