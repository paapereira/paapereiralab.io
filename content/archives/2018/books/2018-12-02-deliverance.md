---
title: "Deliverance by James Dickey (1970)"
slug: "deliverance"
author: "Paulo Pereira"
date: 2018-12-02T18:06:00+00:00
lastmod: 2018-12-02T18:06:00+00:00
cover: "/posts/books/deliverance.jpg"
description: "Finished “Deliverance” by James Dickey."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - James Dickey
book series:
  - 
book genres:
  - Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - James Dickey
  - Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Deliverance”.
* Author: [James Dickey](/book-authors/james-dickey)
* First Published: [1970](/book-publication-year/1970s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17409290-deliverance)
>
> The setting is the Georgia wilderness, where the state's most remote white-water river awaits. In the thundering froth of that river, in its echoing stone canyons, four men on a canoe trip discover a freedom and exhilaration beyond compare. And then, in a moment of horror, the adventure turns into a struggle for survival as one man becomes a human hunter who is offered his own harrowing deliverance.