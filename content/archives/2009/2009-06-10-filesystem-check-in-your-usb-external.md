---
title: "Filesystem check in your USB external hard drives"
slug: "filesystem-check-in-your-usb-external"
author: "Paulo Pereira"
date: 2009-06-10T13:28:00+01:00
lastmod: 2009-06-10T13:28:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

Typically, every number of mounts, your partitions are automatically checked (`e2fsck`) at boot time. This will check and repair your hard drives.

But, if you have external hard drives connected only when needed, this automatic checks aren't performed.

I recommend you to check your hard drive after like 30 mounts. See what  is the maximum mount count in your internal system hard drive and use  that count as reference.

I will list a number of useful commands to gather information about your drives and to check and repair them.

If you have suggestions, please let me know.

## Checking your partitions

Use the command tree to list all your partition and find out witch device you want to check.

- Install tree

```bash
sudo apt-get install tree
```

- List all drives

```bash
tree /dev/disk
```

- List drives by label (probably more easy)

```bash
sudo ls -l /dev/disk/by-label
```

## Maximum mount count

Use the maximum mount count in your internal system hard drive and use that count as reference to regullary check your external hard drives.

```bash
sudo tune2fs -l /dev/sda5 | grep "Maximum mount count"
```

## Check Filesystem

- Make sure the filesystem is unmounted

```bash
sudo umount /dev/sda5
```

- Check and repair filesystem

```bash
sudo e2fsck -C0 -f -p -v /dev/sda5
```

- Check for bad blocks (this will take a while in big drives)

```bash
sudo badblocks -sv /dev/sdb1 >> badblocks.report
```

- See the report created

```bash
cat badblocks.report
```

## smartmontools

Use the command `smartmontools` to print all SMART information about the disk (if your disk have smart capabilities).

- Install smartmontools

```bash
sudo apt-get install smartmontools
```

- Print SMART information about the disk

```bash
sudo smartctl -a /dev/sda
```

Please comment this post with more useful tips or send me an [email](http://paapereira.squarespace.com/contact/).