---
title: "Alien: Covenant by Alan Dean Foster (2017)"
slug: "alien-covenant"
author: "Paulo Pereira"
date: 2018-05-27T18:02:00+01:00
lastmod: 2018-05-27T18:02:00+01:00
cover: "/posts/books/alien-covenant.jpg"
description: "Finished “Alien: Covenant” by Alan Dean Foster."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Alan Dean Foster
book series:
  - Alien Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Alan Dean Foster
  - Alien Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Alien: Covenant”.
* Author: [Alan Dean Foster](/book-authors/alan-dean-foster)
* First Published: [May 23rd 2017](/book-publication-year/2017)
* Series: [Alien Series](/book-series/alien-series)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/33541226-alien)
>
> Ridley Scott returns to the universe he created, with Alien: Covenant, a new chapter in his groundbreaking Alien adventure. The crew of the colony ship Covenant, bound for a remote planet on the far side of the galaxy, discovers what they think is an uncharted paradise. But it is actually a dark, dangerous world.
>
> When they uncover a threat beyond their imaginations, they must attempt a harrowing escape.
>
> Acclaimed author Alan Dean Foster also returns to the universe he first encountered with the official novelization of the original Alien film. Alien: Covenant is the pivotal adventure that preceded that seminal film, and leads to the events that will yield one of the most terrifying sagas of all time.