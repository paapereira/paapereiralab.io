---
title: "Seafile log4j vulnerability fix"
slug: "log4j-seafile"
author: "Paulo Pereira"
date: 2021-12-30T19:57:31+00:00
lastmod: 2021-12-30T19:57:31+00:00
description: "Similarly to the beginning of the year, I had a problem with Seafile. It was crashing and I couldn't start it with systemd.\n

Here's my notes/steps/fixes for the issue."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Seafile
aliases:
  - /posts/log4j-seafile
---

There is a recent log4j vulnerability (CVE-2021-44228) affecting a ton or services and servers.

Specifically for Seafile, you can read this [post](https://forum.seafile.com/t/security-advisory-seafile-servers-vulnerability-to-log4shell-log4j-vulnerability-cve-2021-44228/15590) in the forum to know more about this issue and how to fix it.

Here are my notes about it.

## Scan for vulnerable log4j classes

```bash
sudo -u seafile_user -s /bin/sh
mkdir /tmp/log4jscan/
cd /tmp/log4jscan/
wget https://github.com/lunasec-io/lunasec/releases/download/v1.3.1-log4shell/log4shell_1.3.1-log4shell_Linux_x86_64
chmod +x log4shell_1.3.1-log4shell_Linux_x86_64
./log4shell_1.3.1-log4shell_Linux_x86_64 scan $HOME/seafile/seafile-server-latest/
```

You will see something like this:

```text
7:31PM ??? Identified vulnerable path 
	cve: CVE-2021-44228 
	fileName: org/apache/logging/log4j/core/lookup/JndiLookup.class 
	hash: 0f038a1e0aa0aff76d66d1440c88a2b35a3d023ad8b2e3bac8e25a3208499f7e 
	path: /srv/seafile/seafile/seafile-server-latest/pro/elasticsearch/lib/log4j-core-2.11.1.jar 
	severity: 10.0 
	versionInfo: "2.10.0, 2.11.0, 2.11.1, 2.11.2, 2.9.0, 2.9.1"
```

You should also check if there is any strange ssh key in your `authorized_keys` file:

```bash
cat /${HOME}/.ssh/authorized_keys
```

And check if are any strange user connecting to your machine:

```bash
last -30 -i
```

Search for this string the the elasticsearch logs:

```bash
grep -i "jndi" $HOME/seafile/logs/elasticsearch*
```

## Mitigating the issue

This vulnerability does not affect you if you disable the full text search functionality. In my case I just disabled this and will wait for a "final" fix.

The the forum post to see how to mitigate the issue and keep the full text search functionality active.

```bash
vi $HOME/seafile/conf/seafevents.conf
```

```text
[INDEX FILES]
## must be "true" to enable search
enabled = false
```

```bash
sudo systemctl restart seahub
sudo systemctl restart seafile
```