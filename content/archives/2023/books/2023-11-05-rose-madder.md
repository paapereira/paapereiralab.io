---
title: "Rose Madder by Stephen King (1995)"
slug: "rose-madder"
author: "Paulo Pereira"
date: 2023-11-05T17:00:00+00:00
lastmod: 2023-11-05T17:00:00+00:00
cover: "/posts/books/rose-madder.jpg"
description: "Finished “Rose Madder” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - Fantasy
  - 7/10 Books
  - Audiobook
---

Finished “Rose Madder”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [January 1, 1995](/book-publication-year/1990s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/60318838-rose-madder)
>
> Roused by a single drop of blood, Rosie Daniels wakes up to the chilling realisation that her husband is going to kill her. And she takes flight – with his credit card.
> 
> Alone in a strange city, Rosie begins to build a new life: she meets Bill Steiner and she finds an old junk shop painting, "Rose Madder," which strangely seems to want her as much as she wants it.
> 
> But it’s hard for Rosie not to keep looking over her shoulder. Rose-maddened and on the rampage, Norman is a corrupt cop with a dog’s instinct for tracking people. And he’s getting close. Rosie can feel just how close he is getting…