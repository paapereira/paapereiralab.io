---
title: "The B-Team (The Human Division Book 1) by John Scalzi (2013)"
slug: "the-b-team-the-human-division-book-1"
author: "Paulo Pereira"
date: 2019-01-11T22:39:00+00:00
lastmod: 2019-01-11T22:39:00+00:00
cover: "/posts/books/the-b-team-the-human-division-book-1.jpg"
description: "Finished “The B-Team (The Human Division Book 1)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The B-Team”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [January 15th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 1
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17839531-the-b-team)
>
> The opening episode of The Human Division, John Scalzi's new thirteen-episode novel in the world of his bestselling Old Man's War.
>
> Colonial Union Ambassador Ode Abumwe and her team are used to life on the lower end of the diplomatic ladder. But when a high-profile diplomat goes missing, Abumwe and her team are last minute replacements on a mission critical to the Colonial Unions future. As the team works to pull off their task, CDF Lieutenant Harry Wilson discovers theres more to the story of the missing diplomats than anyone expected...a secret that could spell war for humanity.