---
title: "Twain's Feast by Nick Offerman (2018)"
slug: "twains-feast"
author: "Paulo Pereira"
date: 2019-07-10T23:06:00+01:00
lastmod: 2019-07-10T23:06:00+01:00
cover: "/posts/books/twains-feast.jpg"
description: "Finished “Twain's Feast: Searching for America's Lost Foods in the Footsteps of Samuel Clemens” by Nick Offerman."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Nick Offerman
book series:
  - 
book genres:
  - Nonfiction
  - History
book scores:
  - 4/10 Books
tags:
  - Book Log
  - Nick Offerman
  - Nonfiction
  - History
  - 4/10 Books
  - Audiobook
---

Finished “Twain's Feast: Searching for America's Lost Foods in the Footsteps of Samuel Clemens”.
* Author: [Nick Offerman](/book-authors/nick-offerman)
* First Published: [2018](/book-publication-year/2018)
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53121048-twain-s-feast)
>
> Mark Twain, beloved American writer, performer, and humorist, was a self-proclaimed glutton. With the help of a chef and some friends, Nick Offerman presents the story of Twain’s life through the lens of eight of Mark Twain’s favorite foods. As we explore these foods’ role in Samuel Clemens’ life, we also discover a surprising culinary and ecological history of America. The biggest celebrity of his time, Twain was a witness to a transforming country, and with historian and writer Andy Beahrs as a guide, Beahrs and Offerman take documentary excursions across America, illuminating each dish and bringing to life a broad sampling of Twain’s writing. Twain’s Feast is a rollicking information-packed journey into the rich culinary history of America, with the sharp eye and unmistakable wit of Mark Twain himself.