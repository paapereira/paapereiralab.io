---
title: "Scripting sending messages with SimpleX Chat CLI"
slug: "simplex-chat-cli"
author: "Paulo Pereira"
date: 2023-04-25T09:30:00+01:00
lastmod: 2023-04-25T09:30:00+01:00
description: ""
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - SimpleX Chat
---

[SimpleX Chat](https://simplex.chat/) as a CLI version. You can read more about it in my last [post](/posts/simplex-chat).

You can pass commands to simplex-chat, so... I finally can send myself messages from my shell scripts in my server when something goes wrong.

Something like: `simplex-chat -e "@username A message"`

It supports markdown so I can send it in red: `simplex-chat -e "@username !1 A message!"`

Inside simplex-chat you can list the options for markdown:

```txt
/markdown
Markdown:
        *bold*          - bold text
        _italic_        - italic text (shown as underlined)
        ~strikethrough~ - strikethrough text (shown as inverse)
        `code snippet`  - `a + b // no *markdown* here`
        !1 text!        - red text (1-6: red, green, blue, yellow, cyan, magenta)
        #secret#        - #secret text# (can be copy-pasted)
```
