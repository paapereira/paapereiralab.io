---
title: "Children of Ruin (Children of Time Book 2) by Adrian Tchaikovsky (2019)"
slug: "children-of-ruin-book-2"
author: "Paulo Pereira"
date: 2020-03-17T22:49:00+00:00
lastmod: 2020-03-17T22:49:00+00:00
cover: "/posts/books/children-of-ruin-book-2.png"
description: "Finished “Children of Ruin (Children of Time Book 2)” by Adrian Tchaikovsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Adrian Tchaikovsky
book series:
  - Children of Time Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Children of Time Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Children of Ruin”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
* First Published: [May 14th 2019](/book-publication-year/2019)
* Series: [Children of Time](/book-series/children-of-time-series) Book 2
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40376072-children-of-ruin)
>
> The astonishing sequel to Children of Time, the award-winning novel of humanity's battle for survival on a terraformed planet.
>
> Long ago, Earth's terraforming program sent ships out to build new homes for humanity among the stars and made an unexpected discovery: a planet with life. But the scientists were unaware that the alien ecosystem was more developed than the primitive life forms originally discovered.
>
> Now, thousands of years later, the Portiids and their humans have sent an exploration vessel following fragmentary radio signals. They discover a system in crisis, warring factions trying to recover from an apocalyptic catastrophe arising from what the early terraformers awoke all those years before.
>