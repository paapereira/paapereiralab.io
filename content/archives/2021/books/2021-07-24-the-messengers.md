---
title: "The Messengers by Lindsay Joelle (2020)"
slug: "the-messengers"
author: "Paulo Pereira"
date: 2021-07-24T21:00:00+01:00
lastmod: 2021-07-24T21:00:00+01:00
cover: "/posts/books/the-messengers.jpg"
description: "Finished “The Messengers” by Lindsay Joelle."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Lindsay Joelle
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Lindsay Joelle
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Messengers”.
* Author: [Lindsay Joelle](/book-authors/lindsay-joelle)
* First Published: [March 5th 2020](/book-publication-year/2020)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/52167587-the-messengers)
>
> A mysterious plague ushers in an intergalactic war that ravages the galaxy for decades. A soldier and a pilot are tasked to deliver a package. A messenger and a refugee decide to work together on a dying alien planet. A love letter is lost that could be the key to a new future. A dark comedy about the messages we carry in our bones.