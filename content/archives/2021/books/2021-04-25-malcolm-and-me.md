---
title: "Malcolm and Me by Ishmael Reed (2020)"
slug: "malcolm-and-me"
author: "Paulo Pereira"
date: 2021-04-25T17:00:00+01:00
lastmod: 2021-04-25T17:00:00+01:00
cover: "/posts/books/malcolm-and-me.jpg"
description: "Finished “Malcolm and Me” by Ishmael Reed."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Ishmael Reed
book series:
  - 
book genres:
  - Nonfiction
book scores:
  - 3/10 Books
tags:
  - Book Log
  - Ishmael Reed
  - Nonfiction
  - 3/10 Books
  - Audiobook
---

Finished “Malcolm and Me”.
* Author: [Ishmael Reed](/book-authors/ishmael-reed)
* First Published: [January 30th 2020](/book-publication-year/2020)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/52485069-malcolm-and-me)
>
> In 1960, Ishmael Reed, then an aspiring young writer, interviewed Malcolm X for a local radio station in Buffalo, New York. The encounter cost Reed his job and changed his life. In Malcolm and Me, Reed, acclaimed author of such classic novels as Mumbo Jumbo and winner of a MacArthur "Genius" Fellowship, reveals a side of Malcolm X the public has never seen before, while exploring how the civil rights firebrand influenced his own views on working, living, speaking out, and left a mark on generations of artists and activists.
>
> Malcolm X was one of the most influential human rights activists in history and his views on race, religion, and fighting back changed America and the world. Reed gives us a clear-eyed view of what the man was really like—beyond the headlines and the myth-making. Malcolm and Me is a personal look at the development of an artist and a testament to how chance encounters we have in our youth can transform who we are and the world we live in.