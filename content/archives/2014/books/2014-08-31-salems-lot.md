---
title: "'Salem's Lot by Stephen King (1975)"
slug: "salems-lot"
author: "Paulo Pereira"
date: 2014-08-31T23:57:00+01:00
lastmod: 2014-08-31T23:57:00+01:00
cover: "/posts/books/salems-lot.jpg"
description: "Finished “'Salem's Lot” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Horror
  - Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Horror
  - Fiction
  - 8/10 Books
  - Audiobook
---

Finished “'Salem's Lot”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [October 17th 1975](/book-publication-year/1970s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13584298-salem-s-lot)
>
> Ben Mears has returned to Jerusalem's Lot in the hopes that living in an old mansion, long the subject of town lore, will help him cast out his own devils and provide inspiration for his new book. But when two young boys venture into the woods and only one comes out alive, Mears begins to realize that there may be something sinister at work and that his hometown is under siege by forces of darkness far beyond his control.