---
title: "New Kindle Paperwhite"
slug: "kindle-paperwhite"
author: "Paulo Pereira"
date: 2020-10-10T15:15:54+01:00
lastmod: 2020-10-10T15:15:54+01:00
description: "This week my new [Kindle Paperwhite](https://www.amazon.com/dp/B075MWNNJG/ref=twister_B07J2FGZSM) with [Audible](https://www.audible.com/) support arrived."
draft: false
toc: false
categories:
  - Books
tags:
  - Books
  - Kindle Paperwhite
aliases:
  - /posts/kindle-paperwhite/
---

This week my new [Kindle Paperwhite](https://www.amazon.com/dp/B075MWNNJG/ref=twister_B07J2FGZSM) with [Audible](https://www.audible.com/) support arrived.

![Kindle Paperwhite](/posts/2020/2020-10-10-kindle-paperwhite/kindle.jpg)