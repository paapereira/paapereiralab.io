---
title: "Crystal World (Undying Mercenaries Book 20) by B.V. Larson (2023)"
slug: "crystal-world-undying-mercenaries-book-20"
author: "Paulo Pereira"
date: 2023-12-17T14:30:00+00:00
lastmod: 2023-12-17T14:30:00+00:00
cover: "/posts/books/crystal-world-undying-mercenaries-book-20.jpg"
description: "Finished “Crystal World (Undying Mercenaries Book 20)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Crystal World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [October 6, 2023](/book-publication-year/2023)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 20
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/198338629-crystal-world)
>
> The Silicoids are a growing threat from beyond the frontier. Launching waves of crystalline drones, they ravage colonies all along the border of the Empire. Earth has been forced to ally with her enemies against this mysterious alien menace.Scouts have located a forward base of the freakish creatures, who plot to annihilate all meat-based lifeforms. Earth assembles a joint task force with legions of Rigellian bears, Mogwa battlecruisers, and Saurian troops. Together, they strike as one against Crystal World.After annoying every superior in his chain of command, James McGill is the first to be deployed on the unexplored planet. His to establish a beachhead on this strange, hostile ground. Surrounded and cut off, his troops fight to the bitter end against creatures made of earth, stone, and jewels.