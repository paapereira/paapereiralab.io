---
title: "The One Thing by Gary Keller and Jay Papasan (2012)"
slug: "the-one-thing"
author: "Paulo Pereira"
date: 2022-10-30T16:00:00+00:00
lastmod: 2022-10-30T16:00:00+00:00
cover: "/posts/books/the-one-thing.jpg"
description: "Finished “The One Thing” by Gary Keller and Jay Papasan."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - Gary Keller
  - Jay Papasan
book series:
  - 
book genres:
  - Nonfiction
  - Self Help
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Gary Keller
  - Jay Papasan
  - Nonfiction
  - Self Help
  - 7/10 Books
  - Audiobook
---

Finished “The One Thing: The Surprisingly Simple Truth Behind Extraordinary Results”.
* Author: [Gary Keller](/book-authors/gary-keller) and [Jay Papasan](/book-authors/jay-papasan)
* First Published: [January 1, 2012](/book-publication-year/2012)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44550143-the-one-thing)
>
> "The One Thing" explains the success habit to overcome the six lies that block our success, beat the seven thieves that steal time, and leverage the laws of purpose, priority, and productivity.