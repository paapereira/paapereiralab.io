---
title: "Rogue World (Undying Mercenaries Book 7) by B.V. Larson (2017)"
slug: "rogue-world-undying-mercenaries-book-7"
author: "Paulo Pereira"
date: 2018-05-04T22:51:00+01:00
lastmod: 2018-05-04T22:51:00+01:00
cover: "/posts/books/rogue-world-undying-mercenaries-book-7.jpg"
description: "Finished “Rogue World (Undying Mercenaries Book 7)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Rogue World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [June 13th 2017](/book-publication-year/2017)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 7
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35506822-rogue-world)
>
> The Galactics arrived with their Battle Fleet in 2052. Rather than being exterminated under a barrage of hell-burners, Earth joined a vast Empire that spanned the Milky Way.
>
> Today, Battle Fleet 921 is returning to Earth. It hasn’t been seen by human eyes since our blissful day of Annexation. But what should be a joyful occasion, a chance to grovel at the feet of superior lifeforms, is rapidly becoming a nightmare.
>
> Over the last century, Humanity has engaged in many activities that our Overlords find… questionable. A panic ensues, and Legion Varus is deployed to erase certain “mistakes” our government has made. Projects must be purged to stop Imperial military action.
>
> Among the thousands marching to war is one man no politician has ever enjoyed dealing with. One man who’s destined to follow his own unique path through Galactic Law, Morality and the Stars themselves.
>
> James McGill is about to make history.
>
> ROGUE WORLD is the seventh book of Undying Mercenaries Series, a novel of military science fiction by bestselling author B. V. Larson. 