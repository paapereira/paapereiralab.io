---
title: "Battle Station (Star Force Book 5) by B.V. Larson (2012)"
slug: "battle-station-star-force-book-5"
author: "Paulo Pereira"
date: 2016-05-25T22:58:00+01:00
lastmod: 2016-05-25T22:58:00+01:00
cover: "/posts/books/battle-station-star-force-book-5.jpg"
description: "Finished “Battle Station (Star Force Book 5)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - B.V. Larson
book series:
  - Star Force Series
book genres:
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Force Series
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Battle Station”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [May 2nd 2012](/book-publication-year/2012)
* Series: [Star Force](/book-series/star-force-series) Book 5
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13634835-battle-station)
>
> In BATTLE STATION Kyle Riggs faces new challenges, new alien fleets and learns the secrets behind the war he has been fighting for years.
>
> In the fifth book of the Star Force Series, the Eden system is in humanity's grasp, but can they keep it? Star Force is weak after a long war, and many yearn to go home. Knowing the machines will return with a new armada eventually, Riggs seeks a more permanent solution. Along the way, worlds are won and lost, millions perish, and great truths are revealed.