---
title: "Ice World (Undying Mercenaries Book 16) by B.V. Larson (2021)"
slug: "ice-world-undying-mercenaries-book-16"
author: "Paulo Pereira"
date: 2022-01-01T22:00:00+00:00
lastmod: 2022-01-01T22:00:00+00:00
cover: "/posts/books/ice-world-undying-mercenaries-book-16.jpg"
description: "Finished “Ice World (Undying Mercenaries Book 16)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Ice World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [September 24th 2021](/book-publication-year/2021)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 16
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/58976304-ice-world)
>
> Long before Earth began conquering her neighboring stars, Legion Varus was deployed on Tau Ceti. Valuable and highly illegal treasures were stolen during the campaign, but McGill never gave it a second thought.
> 
> The Tau haven’t forgotten him, however. For decades they’ve hunted and plotted, and at last they’ve found the pirate who robbed them. Tau agents invade Earth seeking revenge and profit. Caught up in violence and intrigue, McGill is given the task of finding out where the loot has gone and who was responsible for the theft in the first place. The hunt takes Legion Varus to ICE WORLD, a distant planet with an inhospitable climate.