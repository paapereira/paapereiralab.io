---
title: "Sharing Files with Windows"
slug: "sharing-files-with-windows"
author: "Paulo Pereira"
date: 2008-03-21T15:15:00+00:00
lastmod: 2008-03-21T15:15:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Samba
aliases:
  - /posts/sharing-files-with-windows/
---

For sharing files with Windows you just have to follow this steps:

Got to System > Administration > Shared Folders

- in the Shared Folders tab add the folders you wish to share
- in the General Properties tab type the Workgroup of your Network (as in Windows)

In the Terminal add yourself as a Samba user:

```bash
sudo smbpasswd -a yourusername
```

That's it. You can now access your shared folder using you Linux user.