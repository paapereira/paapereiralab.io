---
title: "Dexter Is Dead (Dexter Book 8) by Jeff Lindsay (2015)"
slug: "dexter-is-dead-dexter-book-8"
author: "Paulo Pereira"
date: 2015-09-14T22:58:00+01:00
lastmod: 2015-09-14T22:58:00+01:00
cover: "/posts/books/dexter-is-dead-dexter-book-8.jpg"
description: "Finished “Dexter Is Dead (Dexter Book 8)” by Jeff Lindsay."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - Jeff Lindsay
book series:
  - Dexter Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Jeff Lindsay
  - Dexter Series
  - Fiction
  - Mystery
  - 6/10 Books
  - Audiobook
---

Finished “Dexter Is Dead”.
* Author: [Jeff Lindsay](/book-authors/jeff-lindsay)
* First Published: [April 9th 2015](/book-publication-year/2015)
* Series: [Dexter](/book-series/dexter-series) Book 8
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/26152217-dexter-is-dead)
>
> After seven national best sellers and eight seasons as one of the most successful shows on television, New York Times best-selling author Jeff Lindsay bids a thrilling farewell to his uniquely twisted and beloved serial killer, Dexter Morgan. Dexter Is Dead is the definitive conclusion of the character who has become a global icon.
>
> Dexter Morgan has burned the candle at both ends for many years: blood spatter analyst...husband...father...serial killer. And now, for the first time, his world has truly collapsed. Dexter is arrested on charges of murder. He has lost everything - including his wife, his kids, and the loyalty of his sister. Now completely alone, Dexter faces a murder charge (for a crime, ironically, he did not actually commit). His only chance for freedom lies with his brother, Brian, who has a dark plan to prove Dexter's innocence. But the stakes are deadly, and the epic showdown that lies in Dexter's path may lead, once and for all, to his demise.
>
> Jeff Lindsay's trademark devilish wit and cutting satire have never been sharper. Dexter Is Dead marks the end of a beloved series, but is also Dexter's most satisfying and suspenseful outing yet.