---
title: "B3 Server 2.5.1.2 Upgrade"
slug: "b3-server-2512-upgrade"
author: "Paulo Pereira"
date: 2012-11-18T22:39:00+00:00
lastmod: 2012-11-18T22:39:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
---

New B3 Server version is out: [2.5.1.2](http://forum.excito.net/viewtopic.php?f=22&t=4303).

## Upgrade to 2.5.1.2

- Connect to your b3 server
```bash
ssh user@b3server
```
- Become root
```bash
su
```
- Upgrade instructions
```bash
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade
```

You can confirm the software version going into b3 web administration page, then Settings and Software update.

![Settings](/posts/2012/2012-11-18-b3-server-2512-upgrade/b3server.png)

## Official Excito Forum Notes (2.5.1.2)

*Fix a check for easyfind reading config file to detect correctly if easyfind is enabled or not.*