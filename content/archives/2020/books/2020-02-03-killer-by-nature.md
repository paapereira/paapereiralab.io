---
title: "Killer by Nature by Jan Smith (2017)"
slug: "killer-by-nature"
author: "Paulo Pereira"
date: 2020-02-03T22:16:00+00:00
lastmod: 2020-02-03T22:16:00+00:00
cover: "/posts/books/killer-by-nature.jpg"
description: "Finished “Killer by Nature” by Jan Smith."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Jan Smith
book series:
  -
book genres:
  - Nonfiction
  - Thriller
  - Mystery
book scores:
  - 4/10 Books
tags:
  - Book Log
  - Jan Smith
  - Nonfiction
  - Thriller
  - Mystery
  - 4/10 Books
  - Audiobook
---

Finished “Killer by Nature”.
* Author: [Jan Smith](/book-authors/jan-smith)
* First Published: [November 30th 2017](/book-publication-year/2017)
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/37415696-killer-by-nature)
>
> Are we born evil, or do we have evil thrust upon us? This is the eternal question of nature vs nurture at the centre of Killer by Nature, a brand-new psychological thriller from Audible Originals.
>
> Dr. Diane Buckley, a talented freelance forensic psychologist, is drafted in to examine a grisly murder - a body found in a children's playground. The murder carries all the hallmarks of one of her most famous incarcerated clients, 'The Playground Killer' (aka Alfred Dinklage). In a series of intimate 1:1 sessions, Buckley has to race against time to unpick the facts and delve into Dinklage's often manipulative, complicated mind to understand his past whilst striving to prevent further murders...as someone out there will stop at nothing to complete Dinklage's work.
>