---
title: "Fool Moon (The Dresden Files Book 2) by Jim Butcher (2001)"
slug: "fool-moon-the-dresden-files-book-2"
author: "Paulo Pereira"
date: 2022-08-28T21:00:00+01:00
lastmod: 2022-08-28T21:00:00+01:00
cover: "/posts/books/fool-moon-the-dresden-files-book-2.jpg"
description: "Finished “Fool Moon (The Dresden Files Book 2)” by Jim Butcher."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2001
book authors:
  - Jim Butcher
book series:
  - The Dresden Files Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jim Butcher
  - The Dresden Files Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Fool Moon”.
* Author: [Jim Butcher](/book-authors/jim-butcher)
* First Published: [January 1, 2001](/book-publication-year/2001)
* Series: [The Dresden Files](/book-series/the-dresden-files-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6488124-fool-moon)
>
> Harry Dresden — Wizard
> 
> Lost Items Found. Paranormal Investigations. Consulting. Advice. Reasonable Rates. No Love Potions, Endless Purses, or Other Entertainment.
> 
> Business has been slow. Okay, business has been dead. And not even of the undead variety. You would think Chicago would have a little more action for the only professional wizard in the phone book. But lately, Harry Dresden hasn't been able to dredge up any kind of work — magical or mundane.
> 
> But just when it looks like he can't afford his next meal, a murder comes along that requires his particular brand of supernatural expertise.
> 
> A brutally mutilated corpse. Strange-looking paw prints. A full moon. Take three guesses--and the first two don't count...