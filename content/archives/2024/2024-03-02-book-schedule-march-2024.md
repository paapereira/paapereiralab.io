---
title: "Books Schedule update"
slug: "book-schedule-march-2024"
author: "Paulo Pereira"
date: 2024-03-02T21:30:00+00:00
lastmod: 2024-03-02T21:30:00+00:00
description: "March Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my March TBR (To Be Read) update for the next months.

* **[Read]** Proven Guilty (The Dresden Files #8) by Jim Butcher
* **[Read]** Tau Zero by Poul Anderson
* **[Read]** The Night Boat by Robert R. McCammon
* **[Reading]** Micro by Michael Crichton
* Supernova Era by Cixin Liu
* Cycle of the Werewolf by Stephen King
* Galactic North by Alastair Reynolds
* Made Things by Adrian Tchaikovsky
* Shift (Silo #2) by Hugh Howey
* Warbreaker by Brandon Sanderson
* Nightflyers & Other Stories by George R. R. Martin
* The False Mirror (The Damned #2) by Alan Dean Foster
* Star Wars: Episode V: The Empire Strikes Back by Donald F. Glut
* Harry Potter and the Sorcerer's Stone (Harry Potter #1) by J. K. Rowling
* Stranger in a Strange Land by Robert A. Heinlein
* White Night (The Dresden Files #9) by Jim Butcher
* Dragon Teeth by Michael Crichton
* They Thirst by Robert R. McCammon
* Snow Crash by Neal Stephenson

I also have always an audiobook going too:

* **[Read]** Red Company: Discovery (Red Company #2) by B. V. Larson
* **[Read]** The Sandman (Book #1) by Neil Gaiman
* **[Reading]** The Universe in a Nutshell by Stephen Hawking
* The Green Mile by Stephen King
* Total Recall: My Unbelievably True Life Story by Arnold Schwarzenegger
* Minority Report and Other Stories by Philip K. Dick
* The Compound Effect by Darren Hardy
* Red Company: Contact (Red Company #3) by B. V. Larson
* Super Mario: How Nintendo Conquered America by Jeff Ryan