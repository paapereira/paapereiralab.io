---
title: "Clone World (Undying Mercenaries Book 12) by B.V. Larson (2019)"
slug: "clone-world-undying-mercenaries-book-12"
author: "Paulo Pereira"
date: 2019-12-20T22:48:00+00:00
lastmod: 2019-12-20T22:48:00+00:00
cover: "/posts/books/clone-world-undying-mercenaries-book-12.png"
description: "Finished “Clone World (Undying Mercenaries Book 12)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Clone World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [2019](/book-publication-year/2019)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 12
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/48817406-clone-world)
>
> James McGill is called upon for a sabotage mission, one that only a man of his unique talents can handle. Unfortunately, he performs too well, and our enemies from Rigel are enraged. They come for us, and war is declared.
>
> Both sides call for aid, and it comes from the Core Worlds in the form of two great fleets. The Civil War for the Empire's Throne spills out into the provinces. In the center of Galaxy, fantastic fleets crush one another and burn thousands of inhabited worlds. When this conflict between the great powers spreads, humanity finds itself swept up in the storm.
>
> Can McGill escape this disaster he's created? Find out in Clone World, the 12th book of the Undying Mercenaries series. With over three million copies sold, author B. V. Larson is the king of modern military science fiction.