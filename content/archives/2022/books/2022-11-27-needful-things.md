---
title: "Needful Things by Stephen King (1991)"
slug: "needful-things"
author: "Paulo Pereira"
date: 2022-11-27T18:00:00+00:00
lastmod: 2022-11-27T18:00:00+00:00
cover: "/posts/books/needful-things.jpg"
description: "Finished “Needful Things” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - 8/10 Books
  - Audiobook
---

Finished “Needful Things”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [October 1, 1991](/book-publication-year/1990s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/29214177-needful-things)
>
> Leland Gaunt opens a new shop in Castle Rock called Needful Things. Anyone who enters his store finds the object of his or her lifelong dreams and desires: a prized baseball card, a healing amulet. In addition to a token payment, Gaunt requests that each person perform a little "deed", usually a seemingly innocent prank played on someone else from town. These practical jokes cascade out of control, and soon the entire town is doing battle with itself. Only Sheriff Alan Pangborn suspects that Gaunt is behind the population's increasingly violent behavior.