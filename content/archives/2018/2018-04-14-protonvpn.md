---
title: "Connecting to ProtonVPN in Arch Linux"
slug: "protonvpn"
author: "Paulo Pereira"
date: 2018-04-14T18:35:36+01:00
lastmod: 2018-04-14T18:35:36+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - ProtonVPN
---

I recently signed up with [ProtonVPN](https://protonvpn.com/support/linux-vpn-setup/).

Here’s how I connect in Arch Linux.

```bash
sudo pacman -S openvpn

sudo wget -O protonvpn-cli.sh https://raw.githubusercontent.com/ProtonVPN/protonvpn-cli/master/protonvpn-cli.sh

sudo chmod +x protonvpn-cli.sh
sudo ./protonvpn-cli.sh --install
sudo protonvpn-cli -init
sudo protonvpn-cli -connect
sudo protonvpn-cli -disconnect
```