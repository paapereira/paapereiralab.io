---
title: "The New Girl (Fear Street Book 1) by R.L. Stine (1989)"
slug: "the-new-girl-fear-street-book-1"
author: "Paulo Pereira"
date: 2024-10-04T21:00:00+01:00
lastmod: 2024-10-04T21:00:00+01:00
cover: "/posts/books/the-new-girl-fear-street-book-1.jpg"
description: "Finished “The New Girl (Fear Street Book 1)” by R.L. Stine."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - R.L. Stine
book series:
  - Fear Street Series
book genres:
  - Fiction
  - Horror
book scores:
  - 6/10 Books
tags:
  - Book Log
  - R.L. Stine
  - Fear Street Series
  - Fiction
  - Horror
  - 6/10 Books
  - Ebook
---

Finished “The New Girl”.
* Author: [R.L. Stine](/book-authors/r.l.-stine)
* First Published: [June 1, 1989](/book-publication-year/1980s)
* Series: [Fear Street](/book-series/fear-street-series) Book 1
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6663102-the-new-girl)
>
> The new girl is as pale as a ghost, blond, and eerily beautiful -- and she seems to need him as much as he wants her. Cory Brooks hungers for Anna Corwin's kisses, drowns in her light blue eyes. He can't get her out of his mind. He has been loosing sleep, ditching his friends...and everyone has noticed. Then as suddenly as she came to Shadyside High, Anna disappears. To find a cure for his obsession, Cory must go to Anna's house on Fear Street -- no matter what the consequences. Anna may be the love of his life...but finding out her secret might mean his death.