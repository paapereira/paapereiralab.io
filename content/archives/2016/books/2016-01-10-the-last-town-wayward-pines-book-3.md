---
title: "The Last Town (Wayward Pines Book 3) by Blake Crouch (2014)"
slug: "the-last-town-wayward-pines-book-3"
author: "Paulo Pereira"
date: 2016-01-10T23:31:00+00:00
lastmod: 2016-01-10T23:31:00+00:00
cover: "/posts/books/the-last-town-wayward-pines-book-3.jpg"
description: "Finished “The Last Town (Wayward Pines Book 3)” by Blake Crouch."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - Blake Crouch
book series:
  - Wayward Pines Series
book genres:
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Blake Crouch
  - Wayward Pines Series
  - Science Fiction
  - 8/10 Books
  - Audiobook
---

Finished “The Last Town”.
* Author: [Blake Crouch](/book-authors/blake-crouch)
* First Published: [July 15th 2014](/book-publication-year/2014)
* Series: [Wayward Pines](/book-series/wayward-pines-series) Book 3
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/22711770-the-last-town)
>
> Welcome to Wayward Pines, the last town.
>
> Secret Service agent Ethan Burke arrived in Wayward Pines, Idaho, three weeks ago. In this town, people are told who to marry, where to live, where to work. Their children are taught that David Pilcher, the town's creator, is god. No one is allowed to leave; even asking questions can get you killed.
>
> But Ethan has discovered the astonishing secret of what lies beyond the electrified fence that surrounds Wayward Pines and protects it from the terrifying world beyond. It is a secret that has the entire population completely under the control of a madman and his army of followers, a secret that is about to come storming through the fence to wipe out this last, fragile remnant of humanity.