---
title: "Say Cheese and Die! (Goosebumps Book 4) by R.L. Stine (1992)"
slug: "say-cheese-and-die-goosebumps-book-4"
author: "Paulo Pereira"
date: 2023-10-29T21:00:00+00:00
lastmod: 2023-10-29T21:00:00+00:00
cover: "/posts/books/say-cheese-and-die-goosebumps-book-4.jpg"
description: "Finished “Say Cheese and Die! (Goosebumps Book 4)” by R.L. Stine."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - R.L. Stine
book series:
  - Goosebumps Series
book genres:
  - Fiction
  - Horror
book scores:
  - 6/10 Books
tags:
  - Book Log
  - R.L. Stine
  - Goosebumps Series
  - Fiction
  - Horror
  - 6/10 Books
  - Ebook
---

Finished “Say Cheese and Die!”.
* Author: [R.L. Stine](/book-authors/r.l.-stine)
* First Published: [November 1, 1992](/book-publication-year/1990s)
* Series: [Goosebumps](/book-series/goosebumps-series) Book 4
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12309792-say-cheese-and-die)
>
> Greg thinks there is something wrong with the old camera he found. The photos keep turning out . . . different.When Greg takes a picture of his father's brand-new car, it's wrecked in the photo. And then his dad crashes the car.It's like the camera can tell the future--or worse. Maybe it makes the future!Who is going to take the fall next for the evil camera?