---
title: "Paradox Bound by Peter Clines (2017)"
slug: "paradox-bound"
author: "Paulo Pereira"
date: 2018-12-15T22:36:00+00:00
lastmod: 2018-12-15T22:36:00+00:00
cover: "/posts/books/paradox-bound.jpg"
description: "Finished “Paradox Bound” by Peter Clines."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Peter Clines
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Peter Clines
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/paradox-bound
---

Finished “Paradox Bound”.
* Author: [Peter Clines](/book-authors/peter-clines)
* First Published: [September 26th 2017](/book-publication-year/2017)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36314087-paradox-bound)
>
> Nothing ever changes in Sanders. The town's still got a video store, for God's sake.
>
> So why doesn't Eli Teague want to leave?
>
> Not that he'd ever admit it, but maybe he's been waiting - waiting for the traveler to come back. The one who's roared into his life twice before, pausing just long enough to drop tantalizing clues before disappearing in a cloud of gunfire and a squeal of tires. The one who's a walking anachronism, with her tricorne hat, flintlock rifle, and steampunked Model A Ford.
>
> The one who's being pursued by...something.
>
> So when the mysterious traveler finally reappears, Eli's determined that this time, he's going to get some answers. But his hunt soon yields far more than he bargained for, plunging him headlong into a dizzying world full of competing factions and figures straight out of legend.
>
> To make sense of the mystery at its heart, he must embark on a breakneck chase across the country and through two centuries of history - with nothing less than America's past, present, and future at stake.