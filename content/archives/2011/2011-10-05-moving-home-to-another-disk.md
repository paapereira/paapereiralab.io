---
title: "Moving /home to another disk"
slug: "moving-home-to-another-disk"
author: "Paulo Pereira"
date: 2011-10-05T12:48:00+01:00
lastmod: 2011-10-05T12:48:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
aliases:
  - /posts/moving-home-to-another-disk/
---

I have two hard drives in my computer: one with the / partition and the swap partition, and another with the /home partition.

The hard drive with the /home partition was getting full, so I bought another one.

Here's what I did to move the /home partition to the new hard drive:

- Replace the old drive for the new one.
- Connect the old drive to another sata port. This way the new drive  is in the right place and after moving the /home partition I just have  to remove the old drive.
- Boot computer.
- Create a new Extended Partition in the new drive (I used [GParted](http://gparted.sourceforge.net/)) with a primary partition inside. At this point there is no mount point  to this new partition. The new partition in this case is */dev/sdb5*.

![B3 Server](/posts/2011/2011-10-05-moving-home-to-another-disk/gparted.png)

- Get the UUID for the new partition:

```bash
sudo blkid
```

- It will be something like:

```text
/dev/sdb5: UUID="**aa111aaa-1122-aaaa-9999-cccc44447777**" TYPE="ext4"
```

-  Backup your */etc/fstab* file:

```bash
sudo cp /etc/fstab/etc/fstab.$(date +%Y-%m-%d)
```

- Create a new folder to mount the new partition:

```bash
sudo mkdir /media/home
```

- Edit you */etc/fstab* file in order to mount the new partition in the new folder:

```bash
gksu gedit /etc/fstab
```

- Add the following, using your UUID

```text
UUID=**aa111aaa-1122-aaaa-9999-cccc44447777** /media/home    ext4          nodev,nosuid       0       2
```

- Mount the new partition:

```bash
sudo mount -a
```

- Copy your */home* partition to the new */media/home* partition. At this point I disconnected the computer from my LAN so nothing changes:

```bash
sudo rsync -axS --exclude='/*/.gvfs' /home/. /media/home/.
```

- Compare the two partitions:

```bash
sudo diff -r /home /media/home > ~/diff.txt
```

-  Analyze the differences to see if you have to copy something else:

```bash
gedit ~/diff.txt
```

- Edit you */etc/fstab* file again in order to mount the new partition in the */home* folder:

```text
UUID=**aa111aaa-1122-aaaa-9999-cccc44447777** /home ext4 nodev,nosuid 0 2
```

- Move the current */home* to a */old_home* folder. I had to go to a terminal session in order to do this (Ctrl+Alt+F5 to go to the terminal, Ctrl+Alt+F7 to get back):

```bash
cd / && sudo mv /home /old_home && cd / && sudo mkdir -p /home
```

- Reboot.
- After checking everything is allright, remove the */old_home* folder:

```bash
cd /
sudo rm -r /old_home
```

- You can now halt your computer and remove the old hard drive.

More information about this subject, look up [Ubuntu Documentation](https://help.ubuntu.com/community/Partitioning/Home/Moving).