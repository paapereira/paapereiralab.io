---
title: "Permanent Record by Edward Snowden (2019)"
slug: "permanent-record"
author: "Paulo Pereira"
date: 2020-02-12T23:19:00+00:00
lastmod: 2020-02-12T23:19:00+00:00
cover: "/posts/books/permanent-record.png"
description: "Finished “Permanent Record” by Edward Snowden."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Edward Snowden
book series:
  -
book genres:
  - Nonfiction
  - Biography
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Edward Snowden
  - Nonfiction
  - Biography
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/permanent-record
---

Finished “Permanent Record”.
* Author: [Edward Snowden](/book-authors/edward-snowden)
* First Published: [August 17th 2019](/book-publication-year/2019)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/48191180-permanent-record)
>
> Edward Snowden, the man who risked everything to expose the US government’s system of mass surveillance, reveals for the first time the story of his life, including how he helped to build that system and what motivated him to try to bring it down.
>
> In 2013, twenty-nine-year-old Edward Snowden shocked the world when he broke with the American intelligence establishment and revealed that the United States government was secretly pursuing the means to collect every single phone call, text message, and email. The result would be an unprecedented system of mass surveillance with the ability to pry into the private lives of every person on earth. Six years later, Snowden reveals for the very first time how he helped to build this system and why he was moved to expose it.
>
> Spanning the bucolic Beltway suburbs of his childhood and the clandestine CIA and NSA postings of his adulthood, Permanent Record is the extraordinary account of a bright young man who grew up online—a man who became a spy, a whistleblower, and, in exile, the Internet’s conscience. Written with wit, grace, passion, and an unflinching candor, Permanent Record is a crucial memoir of our digital age and destined to be a classic.
>