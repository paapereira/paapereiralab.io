---
title: "The Great Train Robbery by Michael Crichton (1975)"
slug: "the-great-train-robbery"
author: "Paulo Pereira"
date: 2021-04-24T21:00:00+01:00
lastmod: 2021-04-24T21:00:00+01:00
cover: "/posts/books/the-great-train-robbery.jpg"
description: "Finished “The Great Train Robbery” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Michael Crichton
book series:
  - 
book genres:
  - Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Great Train Robbery”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [May 12th 1975](/book-publication-year/1970s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/33957071-the-great-train-robbery)
>
> Lavish wealth and appalling poverty live side by side in Victorian London -- and Edward Pierce easily navigates both worlds. Rich, handsome, and ingenious, he charms the city's most prominent citizens even as he plots the crime of his century, the daring theft of a fortune in gold. But even Pierce could not predict the consequences of an extraordinary robbery that targets the pride of England's industrial era: the mighty steam locomotive.
>
> Based on remarkable fact, and alive with the gripping suspense, surprise, and authenticity that are his trademarks, Michael Crichton's classic adventure is a breathtaking thrill-ride that races along tracks of steel at breakneck speed.