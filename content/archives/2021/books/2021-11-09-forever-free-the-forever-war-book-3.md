---
title: "Forever Free (The Forever War Book 3) by Joe Haldeman (1999)"
slug: "forever-free-the-forever-war-book-3"
author: "Paulo Pereira"
date: 2021-11-09T22:00:00+00:00
lastmod: 2021-11-09T22:00:00+00:00
cover: "/posts/books/forever-free-the-forever-war-book-3.jpg"
description: "Finished “Forever Free (The Forever War Book 3)” by Joe Haldeman."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Joe Haldeman
book series:
  - The Forever War Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Joe Haldeman
  - The Forever War Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
---

Finished “Forever Free”.
* Author: [Joe Haldeman](/book-authors/joe-haldeman)
* First Published: [December 1999](/book-publication-year/1990s)
* Series: [The Forever War](/book-series/the-forever-war-series) Book 3
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31428142-forever-free)
>
> Veterans of the intergalactic Forever War must brave an alien universe to escape mind-control slavery in this thrilling far-future sci-fi adventure.
> 
> On virtually every list of the greatest military science fiction adventures ever written, Joe Haldeman’s Hugo and Nebula Award–winning classic, The Forever War, is ranked at the very top. In Forever Free, the Science Fiction Writers of America Grand Master and author of the acclaimed Worlds series returns to that same volatile universe where human space marines once engaged the alien Taurans in never-ending battle.
> 
> While loyal soldier William Mandella was fighting for the survival of the human race in a distant galaxy, thousands of years were passing on his home planet, Earth. Then, with the end of the hostilities came the shocking realization that humanity had evolved into something he did not recognize.
> 
> Offered the choice of retaining his individuality or becoming part of the genetically modified shared Human hive-mind, Mandella chose exile, joining other veterans of the Forever War seeking a new life on a wasteland world they called Middle Finger.
> 
> Making a home for themselves in this half-frozen hell, Mandella and his life partner, Marygay, have survived into middle age, raising a son and a daughter in the process. Now, the dark truth about the colonists’ ultimate role in the continuation of the Human group mind will force Mandella and Marygay to take desperate action as they hijack an interstellar vessel and set off on a frantic escape across space and time.
> 
> But what awaits them upon their return is a mystery far beyond all human—or Human—comprehension . . .
> 
> In Forever Free, Joe Haldeman’s stunning vision of humankind’s far future reaches its enthralling conclusion in a masterwork of speculation from the mind and heart of one of the undisputed champions of hard science fiction.