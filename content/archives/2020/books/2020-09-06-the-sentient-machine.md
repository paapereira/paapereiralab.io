---
title: "The Sentient Machine: The Coming Age of Artificial Intelligence by Amir Husain (2017)"
slug: "the-sentient-machine"
author: "Paulo Pereira"
date: 2020-09-06T21:59:00+01:00
lastmod: 2020-09-06T21:59:00+01:00
cover: "/posts/books/the-sentient-machine.png"
description: "Finished “The Sentient Machine: The Coming Age of Artificial Intelligence” by Amir Husain."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Amir Husain
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Amir Husain
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “The Sentient Machine: The Coming Age of Artificial Intelligence”.
* Author: [Amir Husain](/book-authors/amir-husain)
* First Published: [November 21st 2017](/book-publication-year/2017)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36082764-the-sentient-machine)
>
> The future is now. Acclaimed technologist and inventor Amir Husain explains how we can live amidst the coming age of sentient machines and artificial intelligence—and not only survive, but thrive.
>
> Artificial “machine” intelligence is playing an ever-greater role in our society. We are already using cruise control in our cars, automatic checkout at the drugstore, and are unable to live without our smartphones. The discussion around AI is polarized; people think either machines will solve all problems for everyone, or they will lead us down a dark, dystopian path into total human irrelevance. Regardless of what you believe, the idea that we might bring forth intelligent creation can be intrinsically frightening. But what if our greatest role as humans so far is that of creators?
>
> Amir Husain, a brilliant inventor and computer scientist, argues that we are on the cusp of writing our next, and greatest, creation myth. It is the dawn of a new form of intellectual diversity, one that we need to embrace in order to advance the state of the art in many critical fields, including security, resource management, finance, and energy. “In The Sentient Machine, Husain prepares us for a brighter future; not with hyperbole about right and wrong, but with serious arguments about risk and potential” (Dr. Greg Hyslop, Chief Technology Officer, The Boeing Company). He addresses broad existential questions surrounding the coming of AI: Why are we valuable? What can we create in this world? How are we intelligent? What constitutes progress for us? And how might we fail to progress? Husain boils down complex computer science and AI concepts into clear, plainspoken language and draws from a wide variety of cultural and historical references to illustrate his points. Ultimately, Husain challenges many of our societal norms and upends assumptions we hold about “the good life.”