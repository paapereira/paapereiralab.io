---
title: "Annihilation (Star Force Book 7) by B.V. Larson (2013)"
slug: "annihilation-star-force-book-7"
author: "Paulo Pereira"
date: 2016-07-29T22:48:00+01:00
lastmod: 2016-07-29T22:48:00+01:00
cover: "/posts/books/annihilation-star-force-book-7.jpg"
description: "Finished “Annihilation (Star Force Book 7)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - B.V. Larson
book series:
  - Star Force Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Force Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Annihilation”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [February 15th 2013](/book-publication-year/2013)
* Series: [Star Force](/book-series/star-force-series) Book 7
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17981909-annihilation)
>
> Kyle Riggs is in for a rough ride.
>
> In Annihilation, the seventh book of the Star Force Series, nothing goes as planned. The three fledgling human colonies in the Eden System are beginning to take hold, but the forces threatening to root them out are many. The Crustaceans are calling for help, despite the fact they are the sworn enemies of Star Force. Are they potential allies, or vicious tricksters? And why are the oceans of their world heating up?
>
> Even as Riggs flies out to investigate the situation, more mysteries arise. The Blues are making vague threats, and Earth is suing for peace. Who is to be trusted, and who holds a dagger behind their backs?
>
> Annihilation is a military science fiction novel by best-selling author B. V. Larson. To find the first book in the series, search for Swarm. Annihilation is the longest book in the series so far....