---
title: "Web searching and bookmarks using Rofi"
slug: "websearch-and-bookmarks-rofi"
author: "Paulo Pereira"
date: 2021-04-02T15:58:21+01:00
lastmod: 2021-04-02T15:58:21+01:00
description: "I use Rofi to search and open bookmarks."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - i3-gaps
  - Rofi
aliases:
  - /posts/websearch-and-bookmarks-rofi/
---

Inspired in a recent [DT](https://www.youtube.com/channel/UCVls1GmFKf6WlTraIb_IaJg) video and his [Gitlab](https://gitlab.com/dwt1/dmscripts) repository, I revamped my websearch shell script I've been using with [Rofi](/tags/rofi).

I already had something very similar to this, but I took this opportunity to review the code and to separate my old websearch script into two scripts:
* websearch: web searching in an handful of sites
* bookmarks: quickly open bookmarks

## websearch

This script allows for searching in a number of sites.

You can add your own sites by adding a new 'options' line.

```bash
cat ~/.local/bin/websearch

  #!/usr/bin/env bash

  DMBROWSER="$BROWSER"

  declare -A options
  options[searx]="https://searx.tuxcloud.net/?q="
  options[amazon]="https://www.amazon.com/s?k="
  options[kindlebooks]="https://www.amazon.com/s?rh=n%3A154606011&ref=nb_sb_noss&k="
  options[youtube]="https://www.youtube.com/results?search_query="
  options[goodreads]="https://www.goodreads.com/search?q="
  options[archaur]="https://aur.archlinux.org/packages/?O=0&K="
  options[archpkg]="https://archlinux.org/packages/?sort=&q="
  options[archwiki]="https://wiki.archlinux.org/index.php?search="
  options[audible]="https://audible.com/search?keywords="
  options[duckduckgo]="https://duckduckgo.com/lite/?q="
  options[github]="https://github.com/search?q="
  options[gitlab]="https://gitlab.com/search?search="
  options[google]="https://www.google.com/search?q="
  options[reddit]="https://www.reddit.com/search/?q="

  while [ -z "$engine" ]; do
    engine=$(printf '%s\n' "${!options[@]}" | sort | rofi -dmenu -p 'Choose search engine:') || exit
    url="${options["${engine}"]}" || exit
  done

  while [ -z "$query" ]; do
    query=$(echo "$engine" | rofi -dmenu -p 'Enter search query:') || exit
  done

  $DMBROWSER "$url""$query"
```

I then add to my `~/.config/i3/config` file:

```text
bindsym  $mod+f  exec --no-startup-id websearch
```

![websearch](/posts/2021/2021-04-02-websearch-and-bookmarks-rofi/websearch.png)

## bookmarks

UPDATED: I have a reviewed script [here](/posts/reviewed-bookmarks-script/).

I use this script to quickly open my bookmarks.

I keep the bookmarks in 2 files:
* quickmarks.txt: with a short list of my most used bookmarks
* bookmarks.txt: all of my bookmarks

```bash
cat ~/.local/bin/bookmarks

  #!/usr/bin/env bash
  default_browser="$BROWSER"

  cat $HOME/.local/bin/quickmarks.txt | grep -v "^#" > /tmp/quickmarks.txt_tmp
  cat $HOME/.local/bin/bookmarks.txt | grep -v "^#" > /tmp/bookmarks.txt_tmp
  quickmarks_file="/tmp/quickmarks.txt_tmp"
  bookmarks_file="/tmp/bookmarks.txt_tmp"

  readarray -t qmarks < "${quickmarks_file}"
  readarray -t bmarks < "${bookmarks_file}"

  qmlist=$(printf '%s\n' "${qmarks[@]}" | awk '{print "["$1"] - "$NF}' | sort)
  bmlist=$(printf '%s\n' "${bmarks[@]}" | awk '{print "["$1"] - "$NF}' | sort)

  separator="================================================================================"
  choice=$(printf '%s\n' ">> Quickmarks ${separator}" "$qmlist" "" ">> Bookmarks ${separator}" "$bmlist" | rofi -dmenu -i -l 20 -theme ~/.config/rofi/themes/gruvbox-dark.rasi -p 'open url:' "$@" ) || exit

  if [[ `echo "$choice" | grep -c "^>>"` -eq 0 && "$choice" ]]; then
    url=$(echo "${choice}" | awk '{print $NF}') || exit
    nohup ${default_browser} "$url" >/dev/null 2>&1 &
  fi
```

```bash
cat ~/.local/bin/quickmarks.txt

  #-------------------------------------------------------------------------------
  📚books/Goodreads       https://www.goodreads.com/
  📚books/Audible         https://www.audible.com/
  📚books/Kindle_Store    https://www.amazon.com/Kindle-eBooks/b/?ie=UTF8&node=1286228011&ref_=sv_kstore_1
  #-------------------------------------------------------------------------------
  headspace               https://www.headspace.com/login
  reddit                  https://www.reddit.com/
  youtube                 https://www.youtube.com/
  paapereira.xyz          https://paapereira.xyz/
```

```bash
cat ~/.local/bin/bookmarks.txt

  #-------------------------------------------------------------------------------
  📚books/Deadhouse_Gates_Guide  https://docs.google.com/presentation/d/1-Rqhn-lK66YQKPwZQNbHmuCLBc0d7zwvRLUJFkM3s50/edit#slide=id.g3fd7d324ec_0_0
  #-------------------------------------------------------------------------------
  🏦banking/cgd.pt        https://www.cgd.pt/
  🏦banking/big.pt        https://big.pt/
  🏦banking/sodexo        https://www.sodexobeneficios.pt/
  #-------------------------------------------------------------------------------
```

I then add to my `~/.config/i3/config` file:

```text
bindsym  $mod+Shift+f exec --no-startup-id bookmarks
```

![bookmarks1](/posts/2021/2021-04-02-websearch-and-bookmarks-rofi/bookmarks1.png)

![bookmarks2](/posts/2021/2021-04-02-websearch-and-bookmarks-rofi/bookmarks2.png)