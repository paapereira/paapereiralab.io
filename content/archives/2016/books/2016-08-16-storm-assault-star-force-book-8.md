---
title: "Storm Assault (Star Force Book 8) by B.V. Larson (2013)"
slug: "storm-assault-star-force-book-8"
author: "Paulo Pereira"
date: 2016-08-16T22:37:00+01:00
lastmod: 2016-08-16T22:37:00+01:00
cover: "/posts/books/storm-assault-star-force-book-8.jpg"
description: "Finished “Storm Assault (Star Force Book 8)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - B.V. Larson
book series:
  - Star Force Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Force Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Storm Assault”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [July 10th 2013](/book-publication-year/2013)
* Series: [Star Force](/book-series/star-force-series) Book 8
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/18629043-storm-assault)
>
> Kyle Riggs has been away from Earth for a long time…but that’s about to change.
>
> In STORM ASSAULT, the eighth book of the Star Force Series, the story moves in a new direction. Massive fleets are built by both Star Force and the Imperials—but there is a third player in the game: the Blues.
>
> A three-way battle for the known star systems breaks out, with the fates of many worlds and species hanging in the balance. Riggs realizes he must destroy his enemies before they destroy him. Seeking justice and revenge, he gathers his strength and ventures out on a mission of conquest from the Eden System.
>
> Humanity must fight to survive, and Riggs is the right man for the job.