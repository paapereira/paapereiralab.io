---
title: "Roadside Picnic by Arkady Strugatsky and Boris Strugatsky (1972)"
slug: "roadside-picnic"
author: "Paulo Pereira"
date: 2022-12-26T20:00:00+00:00
lastmod: 2022-12-26T20:00:00+00:00
cover: "/posts/books/roadside-picnic.jpg"
description: "Finished “Roadside Picnic” by Arkady Strugatsky and Boris Strugatsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Arkady Strugatsky
  - Boris Strugatsky
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Arkady Strugatsky
  - Boris Strugatsky
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “Roadside Picnic”.
* Author: [Arkady Strugatsky](/book-authors/arkady-strugatsky) and [Boris Strugatsky](/book-authors/boris-strugatsky)
* First Published: [January 1, 1972](/book-publication-year/1970s)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17158490-roadside-picnic)
>
> Red Schuhart is a stalker, one of those young rebels who are compelled, in spite of extreme danger, to venture illegally into the Zone to collect the mysterious artifacts that the alien visitors left scattered around. His life is dominated by the place and the thriving black market in the alien products. But when he and his friend Kirill go into the Zone together to pick up a “full empty,” something goes wrong. And the news he gets from his girlfriend upon his return makes it inevitable that he’ll keep going back to the Zone, again and again, until he finds the answer to all his problems. First published in 1972, Roadside Picnic is still widely regarded as one of the greatest science fiction novels, despite the fact that it has been out of print in the United States for almost thirty years. This authoritative new translation corrects many errors and omissions and has been supplemented with a foreword by Ursula K. Le Guin and a new afterword by Boris Strugatsky explaining the strange history of the novel’s publication in Russia.