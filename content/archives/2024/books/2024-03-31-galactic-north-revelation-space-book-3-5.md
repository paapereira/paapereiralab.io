---
title: "Galactic North (Revelation Space Book 3.5) by Alastair Reynolds (2006)"
slug: "galactic-north-revelation-space-book-3-5"
author: "Paulo Pereira"
date: 2024-03-31T07:00:00+01:00
lastmod: 2024-03-31T07:00:00+01:00
cover: "/posts/books/galactic-north-revelation-space-book-3-5.jpg"
description: "Finished “Galactic North (Revelation Space Book 3.5)” by Alastair Reynolds."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2006
book authors:
  - Alastair Reynolds
book series:
  - Revelation Space Series
  - Revelation Space Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Alastair Reynolds
  - Revelation Space Series
  - Revelation Space Universe
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
aliases:
  - /posts/books/galactic-north-revelation-space-book-3-5
---

Finished “Galactic North”.
* Author: [Alastair Reynolds](/book-authors/alastair-reynolds)
* First Published: [July 1, 2006](/book-publication-year/2006)
* Series: [Revelation Space](/book-series/revelation-space-series) Book 3.5
* [Revelation Space Universe](/book-series/revelation-space-universe)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/48981331-galactic-north)
>
> A collection of eight short stories and novellas in the dark and turbulent world of Alastair Reynolds' Revelation Space universe.
>
> Centuries from now, solidarity stretches thin as humanity spreads past the solar system and to the nearest stars. Technology has produced powerful new tools-but lethal risk will always accompany great advancement. And without foresight, opposing groups may fracture multiple worlds. Between the Demarchists and the Conjoiners, the basic right to expand human intelligence-beyond its natural limits-has become a war-worthy cause. Only vast lighthugger starships bind these squabbling colonies together, manned by the panicky and paranoid Ultras. And the hyperpigs just try to keep their heads down. The rich get richer. And everyone tries not to think about the worrying number of extinct alien civilizations turning up on the outer reaches of settled space...because who's to say that humanity won't be next?