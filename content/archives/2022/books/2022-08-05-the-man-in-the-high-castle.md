---
title: "The Man in the High Castle by Philip K. Dick (1962)"
slug: "the-man-in-the-high-castle"
author: "Paulo Pereira"
date: 2022-08-05T17:00:00+01:00
lastmod: 2022-08-05T17:00:00+01:00
cover: "/posts/books/the-man-in-the-high-castle.jpg"
description: "Finished “The Man in the High Castle” by Philip K. Dick."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Philip K. Dick
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Philip K. Dick
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
aliases:
  - /posts/books/the-man-in-the-high-castle
---

Finished “The Man in the High Castle”.
* Author: [Philip K. Dick](/book-authors/philip-k.-dick)
* First Published: [October 1962](/book-publication-year/1960s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36687102-the-man-in-the-high-castle)
>
> It’s America in 1962. Slavery is legal once again. The few Jews who still survive hide under assumed names. In San Francisco, the I Ching is as common as the Yellow Pages. All because some twenty years earlier the United States lost a war—and is now occupied by Nazi Germany and Japan.