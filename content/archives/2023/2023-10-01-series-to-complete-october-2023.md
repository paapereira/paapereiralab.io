---
title: "Books Series to complete buying"
slug: "series-to-complete-october-2023"
author: "Paulo Pereira"
date: 2023-10-01T13:10:00+01:00
lastmod: 2023-10-01T13:10:00+01:00
description: "Short list of series I'm yet to complete buying."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Wishlist
---

October update for my short list of series to complete buying.

* Mars Trilogy by Kim Stanley Robinson
* The Sun Eater by Christopher Ruocchio
* Culture by Iain M. Banks
* New Crobuzon by China Mieville
* The Last Kingdom by Bernard Cornwell
* The Legend of Drizzt by R. A. Salvatore
* Sookie Stackhouse by Charlaine Harris
* Star Wars: Republic Commando
* Other Star Wars Universe books
* Doctor Who