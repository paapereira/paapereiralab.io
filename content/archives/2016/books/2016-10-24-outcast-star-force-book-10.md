---
title: "Outcast (Star Force Book 10) by B.V. Larson and David VanDyke (2014)"
slug: "outcast-star-force-book-10"
author: "Paulo Pereira"
date: 2016-10-24T22:15:00+01:00
lastmod: 2016-10-24T22:15:00+01:00
cover: "/posts/books/outcast-star-force-book-10.jpg"
description: "Finished “Outcast (Star Force Book 10)” by B.V. Larson and David VanDyke."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - B.V. Larson
  - David VanDyke
book series:
  - Star Force Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - David VanDyke
  - Star Force Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Outcast”.
* Author: [B.V. Larson](/book-authors/b.v.-larson) and [David VanDyke](/book-authors/david-vandyke)
* First Published: [May 1st 2014](/book-publication-year/2014)
* Series: [Star Force](/book-series/star-force-series) Book 10
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28242313-outcast)
>
> Many peaceful years have passed since the Macro Wars ended. Most of those who fought against the heartless machines have aged—but not the ingenious artificial construct known as Marvin. His insatiable curiosity is as strong as ever, and he’s brought home fresh perils to humanity’s doorstep in OUTCAST.
>
> This new Saga in the Star Force universe begins with Cody Riggs, the sole surviving child of Kyle Riggs. He’s fresh out of the Star Force Academy and ready to explore the galaxy on his own. Young but fearless, he finds himself lost in uncharted space, seeking a way home. At every step he’s hunted by hostile aliens he doesn’t dare lead back to Earth.
>
> The longest book in the series, OUTCAST is a full length novel of science fiction by bestselling authors B. V. Larson and David VanDyke.