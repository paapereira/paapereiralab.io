---
title: "Planet of Exile (Hainish Cycle) by Ursula K. Le Guin (1966)"
slug: "planet-of-exile-hainish-cycle"
author: "Paulo Pereira"
date: 2025-01-25T16:00:00+00:00
lastmod: 2025-01-25T16:00:00+00:00
cover: "/posts/books/planet-of-exile-hainish-cycle.jpg"
description: "Finished “Planet of Exile (Hainish Cycle)” by Ursula K. Le Guin."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Ursula K. Le Guin
book series:
  - Hainish Cycle Series
book genres:
  - Fiction
  - Science Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Ursula K. Le Guin
  - Hainish Cycle Series
  - Fiction
  - Science Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Planet of Exile”.
* Author: [Ursula K. Le Guin](/book-authors/ursula-k.-le-guin)
* First Published: [January 1, 1966](/book-publication-year/1960s)
* Series: [Hainish Cycle](/book-series/hainish-cycle-series)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/201882.Planet_of_Exile)
>
> The Earth colony of Landin has been stranded on Werel for ten years. But ten of Werel's years are over 600 terrestrial years.
> 
> The lonely & dwindling human settlement is beginning to feel the strain. Every winter --a season that lasts for 15 years-- the Earthmen have neighbors: the humanoid hilfs, a nomadic people who only settle down for the cruel cold spell. The hilfs fear the Earthmen, whom they think of as witches & call the farborns.
> 
> But hilfs & farborns have common enemies: the hordes of ravaging barbarians called gaals & eerie preying snow ghouls. Will they join forces or be annihilated?