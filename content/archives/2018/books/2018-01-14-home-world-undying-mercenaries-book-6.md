---
title: "Home World (Undying Mercenaries Book 6) by B.V. Larson (2016)"
slug: "home-world-undying-mercenaries-book-6"
author: "Paulo Pereira"
date: 2018-01-14T22:39:00+01:00
lastmod: 2018-01-14T22:39:00+01:00
cover: "/posts/books/home-world-undying-mercenaries-book-6.jpg"
description: "Finished “Home World (Undying Mercenaries Book 6)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Home World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [May 24th 2016](/book-publication-year/2016)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 6
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/30322311-home-world)
>
> The Galactics arrived with their Battle Fleet in 2052. Rather than being exterminated under a barrage of hell-burners, Earth joined a vast Empire that spanned the Milky Way.
>
> When the Earth is invaded by a rival empire, James McGill’s legion must defend the Home World. The top brass has complex plans, but none of that matters much to McGill, who chooses his own unique path. Traveling to star systems no human has ever visited, he searches for a technological edge to beat the enemy before it’s too late. Along the way he unleashes new terrors, triggering the biggest battles in human history.
>
> HOME WORLD is the sixth book of Undying Mercenaries Series, a novel of military science fiction by bestselling author B. V. Larson. 