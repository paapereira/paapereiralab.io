---
title: "Reviewing my book ratings"
slug: "book-ratings-reviewed"
author: "Paulo Pereira"
date: 2022-03-01T09:42:24+00:00
lastmod: 2022-03-27T13:44:21+01:00
description: "Reviewing my book ratings."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Rating
aliases:
  - /posts/book-ratings-reviewed/
---

Inspired by TreeBeard's latest [video](https://www.youtube.com/watch?v=ET_kglIam0I&t=615s) I reviewed my rating system.

I'll review this year read book posts and eventually all the past ones.

In [Goodreads](https://www.goodreads.com/paapereira) I'll continue to use stars, matching 5 starts with A+ and so on.

Here's my new logic for rating books:

- A+: A top rated book with a place in my favorite books ever. A book to re-read.
- A : Top rated book, really enjoyed reading it. I can see myself enjoying a re-read.
- B : Good book, good read. Recommended.
- C : I liked it, it was ok. Probably not my first choice to re-read or recommend.
- D : Didn't really liked it.

![book rating diagram](/posts/2022/2022-02-28-book-ratings-reviewed/book_rating_diagram.png)