---
title: "Just Six Numbers by Martin J. Rees (1999)"
slug: "just-six-numbers"
author: "Paulo Pereira"
date: 2019-03-31T22:37:00+01:00
lastmod: 2019-03-31T22:37:00+01:00
cover: "/posts/books/just-six-numbers.jpg"
description: "Finished “Just Six Numbers: The Deep Forces That Shape the Universe” by Martin J. Rees."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Martin J. Rees
book series:
  - The Science Masters Series
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Martin J. Rees
  - The Science Masters Series
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “Just Six Numbers: The Deep Forces That Shape the Universe”.
* Author: [Martin J. Rees](/book-authors/martin-j.-rees)
* First Published: [1999](/book-publication-year/1990s)
* Series: [The Science Masters Series](/book-series/the-science-masters-series)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25958403-just-six-numbers)
>
> How did a single genesis event create billions of galaxies, black holes, stars and planets? How did atoms assemble—here on earth, and perhaps on other worlds—into living beings intricate enough to ponder their origins? What fundamental laws govern our universe?
>
> This book describes new discoveries and offers remarkable insights into these fundamental questions. There are deep connections between stars and atoms, between the cosmos and the microworld. Just six numbers, imprinted in the big bang, determine the essential features of our entire physical world. Moreover, cosmic evolution is astonishingly sensitive to the values of these numbers. If any one of them were untuned, there could be no stars and no life. This realization offers a radically new perspective on our universe, our place in it, and the nature of physical laws.