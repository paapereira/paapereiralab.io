---
title: "Edge World (Undying Mercenaries Book 14) by B.V. Larson (2020)"
slug: "edge-world-undying-mercenaries-book-14"
author: "Paulo Pereira"
date: 2020-12-31T18:20:00+00:00
lastmod: 2020-12-31T18:20:00+00:00
cover: "/posts/books/edge-world-undying-mercenaries-book-14.jpg"
description: "Finished “Edge World (Undying Mercenaries Book 14)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Edge World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [December 18th 2020](/book-publication-year/2020)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 14
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/56280172-edge-world)
>
> A lonely planet circles a star on the very border of Province 921. Critical resources produced there are claimed by both the Mogwa and the Skay. War between the Galactic giants becomes more likely every day.
>
> James McGill and Legion Varus are deployed to protect Edge World, a planet that rotates at a walking pace. Each day is as long as a year back on Earth. The sun-side of the world is baked with endless sunshine, the night-side is freezing and full of strange creatures. Living in an inhabitable zone on the edge of their world, a shadow-line between night and day, nomadic peoples roam the planet. It is these inhabitants Earth’s forces must protect.
>
> Three fleets converge: the Mogwa, the Skay, and Earth’s growing armada. Peace talks are held, but then McGill opens his big mouth, and things go badly…