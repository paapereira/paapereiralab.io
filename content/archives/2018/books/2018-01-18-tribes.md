---
title: "Tribes: We Need You to Lead Us by Seth Godin (2008)"
slug: "tribes"
author: "Paulo Pereira"
date: 2018-01-18T22:26:00+00:00
lastmod: 2018-01-18T22:26:00+00:00
cover: "/posts/books/tribes.jpg"
description: "Finished “Tribes: We Need You to Lead Us” by Seth Godin."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2008
book authors:
  - Seth Godin
book series:
  - 
book genres:
  - Nonfiction
  - Business
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Seth Godin
  - Nonfiction
  - Business
  - 5/10 Books
  - Audiobook
---

Finished “Tribes: We Need You to Lead Us”.
* Author: [Seth Godin](/book-authors/seth-godin)
* First Published: [October 16th 2008](/book-publication-year/2008)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/24860919-tribes)
>
> A tribe is any group of people, large or small, who are connected to one another, a leader, and an idea. For millions of years, humans have been seeking out tribes, be they religious, ethnic, economic, political, or even musical (think of the Deadheads). It's our nature.
>
> Now the Internet has eliminated the barriers of geography, cost, and time. All those blogs and social networking sites are helping existing tribes get bigger. But more important, they're enabling countless new tribes to be born - groups of ten or ten thousand or ten million who care about their iPhones, or a political campaign, or a new way to fight global warming.
>
> And so the key question: Who is going to lead us?
>
> The Web can do amazing things, but it can't provide leadership. That still has to come from individuals - people just like you who have a passion about something. The explosion in tribes means that anyone who wants to make a difference now has the tools at her fingertips.
>
> If you think leadership is for other people, think again - leaders come in surprising packages. Consider Joel Spolsky and his international tribe of scary-smart software engineers. Or Gary Vaynerhuck, a wine expert with a devoted following of enthusiasts. Chris Sharma leads a tribe of rock climbers up impossible cliff faces, while Mich Mathews, a VP at Microsoft, runs her internal tribe of marketers from her cube in Seattle. All they have in common is the desire to change things, the ability to connect a tribe, and the willingness to lead.
>
> If you ignore this opportunity, you risk turning into a "sheepwalker" - someone who fights to protect the status quo at all costs, never asking if obedience is doing you (or your organization) any good. Sheepwalkers don't do very well these days.
>
> Tribes will make you think (really think) about the opportunities in leading your fellow employees, customers, investors, believers, hobbyists, or readers....It's not easy, but it's easier than you think.