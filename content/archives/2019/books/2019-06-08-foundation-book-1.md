---
title: "Foundation (Foundation Book 1) by Isaac Asimov (1951)"
slug: "foundation-book-1"
author: "Paulo Pereira"
date: 2019-06-08T22:19:00+01:00
lastmod: 2019-06-08T22:19:00+01:00
cover: "/posts/books/foundation-book-1.jpg"
description: "Finished “Foundation (Foundation Book 1)” by Isaac Asimov."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1950s
book authors:
  - Isaac Asimov
book series:
  - Foundation Series
book genres:
  - Fiction
  - Science Fiction
  - Classics
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Isaac Asimov
  - Foundation Series
  - Fiction
  - Science Fiction
  - Classics
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/foundation-book-1
---

Finished “Foundation”.
* Author: [Isaac Asimov](/book-authors/isaac-asimov)
* First Published: [1951](/book-publication-year/1950s)
* Series: [Foundation](/book-series/foundation-series) Book 1
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13514670-foundation)
>
> For twelve thousand years the Galactic Empire has ruled supreme. Now it is dying. But only Hari Seldon, creator of the revolutionary science of psychohistory, can see into the future -- to a dark age of ignorance, barbarism, and warfare that will last thirty thousand years. To preserve knowledge and save mankind, Seldon gathers the best minds in the Empire -- both scientists and scholars -- and brings them to a bleak planet at the edge of the Galaxy to serve as a beacon of hope for a future generations. He calls his sanctuary the Foundation.
>
> But soon the fledgling Foundation finds itself at the mercy of corrupt warlords rising in the wake of the receding Empire. Mankind's last best hope is faced with an agonizing choice: submit to the barbarians and be overrun -- or fight them and be destroyed.