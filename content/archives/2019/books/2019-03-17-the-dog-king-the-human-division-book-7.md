---
title: "The Dog King (The Human Division Book 7) by John Scalzi (2013)"
slug: "the-dog-king-the-human-division-book-7"
author: "Paulo Pereira"
date: 2019-03-17T18:05:00+00:00
lastmod: 2019-03-17T18:05:00+00:00
cover: "/posts/books/the-dog-king-the-human-division-book-7.jpg"
description: "Finished “The Dog King (The Human Division Book 7)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Dog King”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [February 26th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 7
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17447080-the-dog-king)
>
> CDF Lieutenant Harry Wilson has one simple task: Watch an ambassador’s dog while the diplomat is conducting sensitive negotiations with an alien race. But you know dogs - always getting into something. And when this dog gets into something that could launch an alien civil war, Wilson has to find a way to solve the conflict, fast, or be the one in the Colonial Union’s doghouse.