---
title: "Reviewed bookmarks script"
slug: "reviewed-bookmarks-script"
author: "Paulo Pereira"
date: 2021-04-10T15:32:24+01:00
lastmod: 2021-04-10T15:32:24+01:00
cover: "/posts/2021/2021-04-10-reviewed-bookmarks-script/bookmarks.png"
description: "Following my [previous post](/posts/websearch-and-bookmarks-rofi/) I kept improving my `bookmarks` script."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - i3-gaps
  - Rofi
aliases:
  - /posts/reviewed-bookmarks-script/
---

Following my [previous post](/posts/websearch-and-bookmarks-rofi/) I kept improving my `bookmarks` script.

I simplified the script to use only one bookmark file and defined a structure to help me while searching.

## The bookmarks.txt file

It has two sections (just for organization sake), a Quickmarks section for common urls and a Bookmarks section for everything else.

Each line is divided in three parts. A category/name, tags and the url.

```bash
cat ~/.local/bin/bookmarks.txt

>> Quickmarks
#
#---
📚books/Goodreads ...... (reviews) ......... https://www.goodreads.com/
📚books/Audible ........ (audiobook) ....... https://www.audible.com/
📚books/Kindle_Deals ... (kindle,ebooks) ... https://www.amazon.com/b/?ie=UTF8&node=11552285011&ref_=sv_kstore_5
#---
🟠headspace ............ (meditation) ...... https://www.headspace.com/login
📶reddit ............... (news,forum) ...... https://www.reddit.com/
📺youtube .............. (video,stream) .... https://www.youtube.com/
⭐paapereira.xyz ....... (blog) ............ https://paapereira.xyz/
#
-------------------------------------------------------------------------------------------------------------------
#
>> Bookmarks
#
🏦banking/cgd.pt ..................... (bank) ............ https://www.cgd.pt/
🏦banking/big.pt ..................... (bank) ............ https://big.pt/
🏦banking/sodexo ..................... (bank) ............ https://www.sodexobeneficios.pt/
#---
#eof
```

## The bookmarks script

With this reviewed script you can also quickly edit the bookmarks.txt file.

```bash
cat ~/.local/bin/bookmarks

#!/usr/bin/env bash

default_browser="$BROWSER"

cat $HOME/.local/bin/bookmarks.txt | grep -v "^#" > /tmp/bookmarks.txt_tmp
bookmarks_file="/tmp/bookmarks.txt_tmp"

readarray -t bmarks < "${bookmarks_file}"

edit="Edit bookmarks.txt"

choice=$(printf '%s\n' "${edit}" "${bmarks[@]}" | \
         rofi -dmenu -i -l 20 -theme ~/.config/rofi/themes/gruvbox-dark.rasi \
           -matching normal -font "Noto Sans Mono 10" -p 'open url:' "$@" ) || exit

if [ "${choice}" = "${edit}" ]; then
  $TERMINAL -e $EDITOR $HOME/.local/bin/bookmarks.txt
elif [[ `echo "$choice" | grep -c "^>>\|^--"` -eq 0 && "$choice" ]]; then
  url=$(echo "${choice}" | awk '{print $NF}') || exit
  nohup ${default_browser} "$url" >/dev/null 2>&1 &
fi

#eof
```