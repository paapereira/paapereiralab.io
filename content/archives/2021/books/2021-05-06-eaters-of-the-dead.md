---
title: "Eaters of the Dead by Michael Crichton (1976)"
slug: "eaters-of-the-dead"
author: "Paulo Pereira"
date: 2021-05-06T21:00:00+01:00
lastmod: 2021-05-06T21:00:00+01:00
cover: "/posts/books/eaters-of-the-dead.jpg"
description: "Finished “Eaters of the Dead” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Michael Crichton
book series:
  - 
book genres:
  - Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Eaters of the Dead”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [March 12th 1976](/book-publication-year/1970s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44294523-eaters-of-the-dead)
>
> The year is A.D. 922. A refined Arab courtier, representative of the powerful Caliph of Bagdad, encounters a party of Viking warriors who are journeying to the barbaric North. He is appalled by their Viking customs -- the wanton sexuality of their pale, angular women, their disregard for cleanliness , their cold-blooded human sacrifices. But it is not until they reach the depths of the Northland that the courtier learns the horrifying and inescapable truth: He has been enlisted by these savage, inscrutable warriors to help combat a terror that plagues them -- a monstrosity that emerges under cover of night to slaughter the Vikings and devour their flesh . . .