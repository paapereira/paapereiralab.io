---
title: "Cosmos by Carl Sagan (1980)"
slug: "cosmos"
author: "Paulo Pereira"
date: 2018-09-29T21:50:00+01:00
lastmod: 2018-09-29T21:50:00+01:00
cover: "/posts/books/cosmos.jpg"
description: "Finished “Cosmos” by Carl Sagan."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Carl Sagan
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Carl Sagan
  - Nonfiction
  - Science
  - 9/10 Books
  - Audiobook
---

Finished “Cosmos”.
* Author: [Carl Sagan](/book-authors/carl-sagan)
* First Published: [1980](/book-publication-year/1980s)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35416916-cosmos)
>
> Cosmos is one of the bestselling science books of all time. In clear-eyed prose, Sagan reveals a jewel-like blue world inhabited by a life form that is just beginning to discover its own identity and to venture into the vast ocean of space. Featuring a new Introduction by Sagan's collaborator, Ann Druyan, and a new Foreword by astrophysicist Neil deGrasse Tyson, Cosmos retraces the fourteen billion years of cosmic evolution that have transformed matter into consciousness, exploring such topics as the origin of life, the human brain, Egyptian hieroglyphics, spacecraft missions, the death of the Sun, the evolution of galaxies, and the forces and individuals who helped to shape modern science.