---
title: "A World Without Email by Cal Newport (2021)"
slug: "a-world-without-email"
author: "Paulo Pereira"
date: 2023-09-10T15:00:00+01:00
lastmod: 2023-09-10T15:00:00+01:00
cover: "/posts/books/a-world-without-email.jpg"
description: "Finished “A World Without Email” by Cal Newport."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - Cal Newport
book series:
  - 
book genres:
  - Nonfiction
  - Self Help
  - Productivity
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Cal Newport
  - Nonfiction
  - Self Help
  - Productivity
  - 8/10 Books
  - Audiobook
---

Finished “A World Without Email: Reimagining Work in an Age of Communication Overload”.
* Author: [Cal Newport](/book-authors/cal-newport)
* First Published: [March 2, 2021](/book-publication-year/2021)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/57610414-a-world-without-email)
>
> Modern knowledge workers communicate constantly: their days are defined by a relentless barrage of incoming messages and back-and-forth digital conversations--a state of constant, anxious chatter in which nobody can disconnect, and so nobody has the cognitive bandwidth to perform substantive work. There was a time when these tools felt cutting edge, but current evidence reveals that the "hyperactive hive mind" workflow they helped create has become a productivity disaster, reducing profitability and perhaps even slowing overall economic growth. Equally worrisome, it makes us miserable. Humans are simply not wired for constant digital communication.
> 
> We have become so used to an inbox-driven workday that it's hard to imagine an alternative. Drawing on case studies from innovative contemporary companies as well as those that thrived in the age before email, author and computer science professor Cal Newport lays out a series of principles for overhauling how you or your organization operate--providing concrete instruction for shifting your efforts away from constant communication and toward more structured approaches to producing valuable output. The knowledge sector's evolution beyond the hyperactive hive mind is inevitable. The question is not whether a world without email is coming (it is), but whether you'll be ahead of this trend.
> 
> If you're a CEO seeking a competitive edge, an entrepreneur convinced your productivity could be higher, or an employee exhausted by your inbox, A World Without Email will convince you that the time has come for bold changes, and walk you through exactly how to make them happen.