---
title: "Bye Bye Banks? by James Haycock (2015)"
slug: "bye-bye-banks"
author: "Paulo Pereira"
date: 2016-11-29T22:32:00+00:00
lastmod: 2016-11-29T22:32:00+00:00
cover: "/posts/books/bye-bye-banks.jpg"
description: "Finished “Bye Bye Banks?: How Retail Banks are Being Displaced, Diminished and Disintermediated by Tech Startups and What They Can Do to Survive” by James Haycock."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - James Haycock
book series:
  -
book genres:
  - Nonfiction
  - Finance
book scores:
  - 5/10 Books
tags:
  - Book Log
  - James Haycock
  - Nonfiction
  - Finance
  - 5/10 Books
  - Book
---

Finished “Bye Bye Banks?: How Retail Banks are Being Displaced, Diminished and Disintermediated by Tech Startups and What They Can Do to Survive”.
* Author: [James Haycock](/book-authors/james-haycock)
* First Published: [June 29th 2015](/book-publication-year/2015)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25866826-bye-bye-banks)
>
> Tech companies have disrupted retailing, media, transit and travel. Now the retail banking business model looks set to be transformed too.
>
> In Bye Bye Banks? James Haycock and Shane Richmond describe these startups, and to which areas of the banking industry they are laying siege. It shows that this assault is already well underway and that many incumbents are poised to be displaced, diminished and disintermediated. It draws on extensive research and on-and-off the record interviews with senior executives in some of the biggest banks.
>
> Haycock and Richmond conclude with the recommendation that traditional banks need to reinvent themselves by launching a ‘Beta Bank’: a lean, stand-alone organisation fit for the future for which they provide a ten-point operating model.
>
> This short book is a bold, urgent and timely analysis of the forces shaping the future of financial services. Its message to industry leaders in the sector could not be more simple: adapt or prepare to be disrupted.