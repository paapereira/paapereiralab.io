---
title: "Installing Ubuntu 9.10 Karmic Koala 64 bits (HP Pavilion dv5-1020ep)"
slug: "installing-ubuntu-910-karmic-koala-64-bits-hp-pavilion-dv5-1"
author: "Paulo Pereira"
date: 2009-11-05T20:49:00+00:00
lastmod: 2009-11-05T20:49:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 9.10
  - Karmic Koala
  - HP dv5-1020ep
aliases:
  - /posts/installing-ubuntu-910-karmic-koala-64-bits-hp-pavilion-dv5-1/
---

Last week Ubuntu 9.10, codename Karmic Koala, was [released](http://www.ubuntu.com/products/whatisubuntu/910features).

I'm doing a clean install because I want to use ext4 as my filesystem. It's possible to [convert ext3 to ext4](http://www.google.pt/search?q=ext3+to+ext4), but I prefer a clean install.

First my laptop. I will be the using the 64 bit desktop cd (*ubuntu-9.10-desktop-amd64.iso*).

## Before you start

- Backup your data
- Make a  list of software you want to recover after the clean install and all the preferences/options you use. This will make you post install easier and you can start working faster.
- Make a list of the extra Software Sources you use
- Check the [Release Notes](http://www.ubuntu.com/getubuntu/releasenotes/910)
- [Download](http://www.ubuntu.com/getubuntu/download) Ubuntu
- Check the file hash (example: `md5sum ubuntu-9.10-desktop-amd64.iso`) and compare it to [UbuntuHashes](https://help.ubuntu.com/community/UbuntuHashes)
- Burn a cd (you can use [Brasero](http://www.gnome.org/projects/brasero/), for example, if you already use Ubuntu)

## Running the live cd

- Boot from the cd.
- After booting it's important to check the integrity of the cd. Burning cds also fails.

## Hardware to check (during the live cd session)

- Graphics (check)
- Sound (check)
- Media buttons (check)
- Cable network (check)
- Wireless network (not tested)
- HP media command (it mutes the sound, more tests needed)
- Integrated Webcam (checked using Cheese)

## Hard Drive Partition Layout

I will keep my storage area as an ext3 filesystem.

```text
/dev/sda
 /dev/sda1      ntfs      Vista                  36002 MB
 /dev/sda3      ext4     /                        10240 MB          -- primary
 /dev/sda5      ext4     /home               10240 MB          -- extended
 /dev/sda6      swap                               2048 MB          -- extended
 /dev/sda7      ext3     /storage            93056 MB          -- extended
 /dev/sda2      ntfs      Vista Recover      8450 MB
```

## Post-install: Wired Network

I had to manually configure my IP because I don't have DHCP activated in my router.

Internet is on!

## Post-install: Graphics Card

I had a warning about an available proprietary driver. Just had to  activate it. Ubuntu downloaded and installed the driver. Pretty simple.

Restart needed.

## Post-install: Updates

Update Manager warned me about 36 available updates. Just letting it go...

## Post-install: File System

Besides the */* and */home* partitions, only root have permissions to write in every other partitions. In my case */storage*.

So, we have to add permissions to our user:

```bash
sudo chown -R youruser:youruser /storage
sudo chmod -R 755 /storage
```

Click [here](http://www.psychocats.net/ubuntu/mountlinux) for more info.

## Post-install: Multimedia

References: [Complete Streaming, Multimedia & Video How-to](http://ubuntuforums.org/showthread.php?t=766683) and [Medibuntu](https://help.ubuntu.com/community/Medibuntu)

- Adding Medibuntu Repositories

```bash
sudo wget http://www.medibuntu.org/sources.list.d/hardy.list -O /etc/apt/sources.list.d/medibuntu.list
wget -q http://packages.medibuntu.org/medibuntu-key.gpg -O- | sudo apt-key add - && sudo apt-get update
```

- Flash (Firefox installed flash, so I skipped this one)

```bash
sudo apt-get purge flashplugin-nonfree && \
sudo mkdir /usr/lib/flashplugin-nonfree && \
sudo cp -f ~/Desktop/libflashplayer.so \
 /usr/lib/flashplugin-nonfree/ && \
sudo nspluginwrapper -i \
 /usr/lib/flashplugin-nonfree/libflashplayer.so && \
sudo ln -sf /usr/lib/nspluginwrapper/plugins/npwrapper.libflashplayer.so \
 /etc/alternatives/firefox-flashplugin && \
sudo ln -sf /etc/alternatives/firefox-flashplugin \
 /usr/lib/firefox-addons/plugins/flashplayer-alternative.so && \
sudo rm -f /usr/lib/firefox/plugins/npwrapper.libflashplayer.so
```

- Codecs and plugins (it will take quite some time)

```bash
sudo apt-get remove gnash gnash-common libflash-mozplugin \
 libflashsupport mozilla-plugin-gnash swfdec-mozilla && \
sudo apt-get install alsa-oss faac faad flashplugin-nonfree \
 gstreamer0.10-ffmpeg gstreamer0.10-plugins-bad \
 gstreamer0.10-plugins-bad-multiverse gstreamer0.10-plugins-ugly \
 gstreamer0.10-plugins-ugly-multiverse ia32-libs icedtea-gcjwebplugin \
 liblame0 non-free-codecs openjdk-6-jre unrar
```

- Audio and Video Streaming

```bash
sudo apt-get remove kaffeine-mozilla mozilla-helix-player \
 mozilla-mplayer mozilla-plugin-vlc totem-mozilla xine-plugin && \
sudo apt-get install gnome-mplayer gecko-mediaplayer
```

- Playing Encrypted DVDs

```bash
sudo apt-get install libdvdcss2 libdvdread3 libdvdnav4 build-essential debhelper fakeroot regionset
```

- Changing your [DVD Region Code](http://en.wikipedia.org/wiki/DVD_region_codes) (be sure there’s a DVD in the drive)

```bash
sudo regionset
```

Check the [reference](http://ubuntuforums.org/showthread.php?t=766683) link, because it as many other things you’ll want.

This time I only add the Medibuntu repositorie. Ubuntu as been installing codecs on the go as I need them.

## Post-install: Preferences and Look and Feel

Here's some things I changed right away:

- 4 workspaces
- Background
- NTP Time Server (*System > Administration > Time and Date*)
  - ntp02.oal.ul.pt
  - ntp04.oal.ul.pt
- Weather on Clock Preferences

## Post-install: SAMBA Server

- Go to *Applications > Ubuntu Software Center* and install Samba.
- Go to *System > Administration > Samba* to add shares and define samba users.

Also check this [post](/posts/sharing-files-with-windows/).

## Post-install: LAMP Server

To install Apache, MySQL and PHP, check [here](/posts/apache-php-and-mysql/).

## Post-install: Software Sources

```text
deb http://packages.medibuntu.org/ karmic free non-free
deb http://ppa.launchpad.net/do-core/ppa/ubuntu karmic main
deb http://linux.dropbox.com/ubuntu karmic main
```

## Post-install: extra software

List of extra installation software installed:

- [Xmarks](http://beta.foxmarks.com/) add-on (can’t live without it)
- Firefox IMDB search-engine
- [GnomeDo](/posts/gnome-do-081/) (after adding the software source just go to Ubuntu Software Source and look for gnome do)
- VLC
- Dropbox (`sudo apt-get install nautilus-dropbox`)
- [Conky](/posts/conky/)
- [Synergy](/posts/synergy/)
- GVim
- lm-sensors
- Gaupol Subtitle Editor
- More Firefox add-ons (Portuguese Dictionary)
- Powertop (`sudo apt-get install powertop`)
- Gnucash
- Aspell pt-pt (`sudo apt-get install aspell-pt-pt`)
- Gnome Partition Editor
- [NTFS support to Gnome Partition Editor](/posts/formating-a-partition-in-ntfs/) (`sudo apt-get install ntfsprogs`)
- Mail Notification
- RAR Support (`sudo apt-get install rar unrar`)

## Post-install: enable compositing

I need this to be able to use the Docky theme in GnomeDo without having any Visual Effects enabled in the system.

```bash
gconftool-2 -s --type bool /apps/metacity/general/compositing_manager true
```