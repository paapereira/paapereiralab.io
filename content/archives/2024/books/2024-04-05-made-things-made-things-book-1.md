---
title: "Made Things (Made Things Book 1) by Adrian Tchaikovsky (2019)"
slug: "made-things-made-things-book-1"
author: "Paulo Pereira"
date: 2024-04-05T08:00:00+01:00
lastmod: 2024-04-05T08:00:00+01:00
cover: "/posts/books/made-things-made-things-book-1.jpg"
description: "Finished “Made Things (Made Things Book 1)” by Adrian Tchaikovsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Adrian Tchaikovsky
book series:
  - Made Things Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Made Things Series
  - Fiction
  - Fantasy
  - 6/10 Books
  - Ebook
---

Finished “Made Things”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
* First Published: [November 5, 2019](/book-publication-year/2019)
* Series: [Made Things](/book-series/made-things-series) Book 1
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44805943-made-things)
>
> Making friends has never been so important.
> Welcome to Fountains Parish--a cesspit of trade and crime, where ambition curls up to die and desperation grows on its cobbled streets like mold on week-old bread. Coppelia is a street thief, a trickster, a low-level con artist. But she has something other thieves don't... tiny puppet-like some made of wood, some of metal. They don't entirely trust her, and she doesn't entirely understand them, but their partnership mostly works.
> After a surprising discovery shakes their world to the core, Coppelia and her friends must re-examine everything they thought they knew about their world, while attempting to save their city from a seemingly impossible new threat.