---
title: "Anatomy of the Linux kernel"
slug: "anatomy-of-linux-kernel"
author: "Paulo Pereira"
date: 2008-08-20T20:55:00+01:00
lastmod: 2008-08-20T20:55:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
---

If you want to know more about the [Anatomy of the Linux kernel](http://www.ibm.com/developerworks/linux/library/l-linux-kernel/index.html?S_TACT=105AGX03&S_CMP=ART), here’s a nice article.