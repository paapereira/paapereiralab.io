---
title: "Exit Strategy (The Murderbot Diaries Book 4) by Martha Wells (2018)"
slug: "exit-strategy-the-murderbot-diaries-book-4"
author: "Paulo Pereira"
date: 2021-01-21T22:00:00+00:00
lastmod: 2021-01-21T22:00:00+00:00
cover: "/posts/books/exit-strategy-the-murderbot-diaries-book-4.jpg"
description: "Finished “Exit Strategy (The Murderbot Diaries Book 4)” by Martha Wells."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Martha Wells
book series:
  - The Murderbot Diaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Martha Wells
  - The Murderbot Diaries Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Exit Strategy”.
* Author: [Martha Wells](/book-authors/martha-wells)
* First Published: [October 2nd 2018](/book-publication-year/2018)
* Series: [The Murderbot Diaries](/book-series/the-murderbot-diaries-series) Book 4
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/41221854-exit-strategy)
>
> Murderbot wasn’t programmed to care. So, its decision to help the only human who ever showed it respect must be a system glitch, right?
>
> Having traveled the width of the galaxy to unearth details of its own murderous transgressions, as well as those of the GrayCris Corporation, Murderbot is heading home to help Dr. Mensah—its former owner (protector? friend?)—submit evidence that could prevent GrayCris from destroying more colonists in its never-ending quest for profit.
>
> But who’s going to believe a SecUnit gone rogue?
>
> And what will become of it when it’s caught?