---
title: "Books Schedule update"
slug: "book-schedule-september-2022"
author: "Paulo Pereira"
date: 2022-09-05T19:00:00+01:00
lastmod: 2022-09-05T19:00:00+01:00
description: "September Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my September TBR (To Be Read) update for the next months.

* **[Read]** Chapterhouse: Dune (Dune 6) by Frank Herbert
* **[Read]** Fool Moon (The Dresden Files 2) by Jim Butcher
* **[Read]** Airframe by Michael Crichton
* **[Reading]** NOS4A2 by Joe Hill
* Cage of Souls by Adrian Tchaikovsky
* The Testaments (The Handmaid's Tale 2) by Margaret Atwood
* Grave Peril (The Dresden Files 3) by Jim Butcher
* Timeline by Michael Crichton
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* Revelation Space (Revelation Space 1) by Alastair Reynolds
* Boy's Life by Robert R. McCammon

(...)

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson
* Redemption Ark (Revelation Space 2) by Alastair Reynolds

I also have always an audiobook going too:

* **[Read]** Rich Dad Poor Dad by Robert T. Kiyosaki
* **[Reading]** Four Past Midnight by Stephen King
* Hive War (Galactic Liberation 4) by David Vandyke & B. V. Larson
* All the Names by José Saramago
* Born a Crime: Stories From a South African Childhood by Trevor Noah
* The One Thing by Gary Keller & Jay Papasan
* Straker's Breakers (Galactic Liberation 5) by David Vandyke & B. V. Larson