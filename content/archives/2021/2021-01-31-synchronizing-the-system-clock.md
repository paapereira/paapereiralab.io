---
title: "Synchronizing the system clock across the network"
slug: "synchronizing-the-system-clock"
author: "Paulo Pereira"
date: 2021-01-31T14:41:57+00:00
lastmod: 2021-01-31T14:41:57+00:00
description: "*systemd-timesyncd is a daemon that has been added for synchronizing the system clock across the network. (...)*\n

Basically it makes sure your system clock is in sync in you network. In my home network I set this up to make sure my desktop, server and laptop are in sync."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - NTP
---

*systemd-timesyncd is a daemon that has been added for synchronizing the system clock across the network. (...)*

Basically it makes sure your system clock is in sync in you network. In my home network I set this up to make sure my desktop, server and laptop are in sync.

I added NTP servers in the `timesyncd` configuration file, before enabling and starting the service:

```bash
sudo vi /etc/systemd/timesyncd.conf
```
```text
[Time]
NTP=0.arch.pool.ntp.org 1.arch.pool.ntp.org 2.arch.pool.ntp.org 3.arch.pool.ntp.org
FallbackNTP=0.pt.pool.ntp.org 1.europe.pool.ntp.org 2.europe.pool.ntp.org 
```
```bash
sudo systemctl enable systemd-timesyncd
sudo systemctl start systemd-timesyncd
sudo systemctl status systemd-timesyncd
timedatectl status
timedatectl timesync-status
```