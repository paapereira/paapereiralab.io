---
title: "The Sandman: Act I by Neil Gaiman and Dirk Maggs (2020)"
slug: "the-sandman-act-1-book-1"
author: "Paulo Pereira"
date: 2024-02-25T16:00:00+00:00
lastmod: 2024-02-25T16:00:00+00:00
cover: "/posts/books/the-sandman-act-1-book-1.jpg"
description: "Finished “The Sandman: Act I” by Neil Gaiman and Dirk Maggs."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Neil Gaiman
  - Dirk Maggs
book series:
  - Sandman Audible Original Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Neil Gaiman
  - Dirk Maggs
  - Sandman Audible Original Series
  - Fiction
  - Fantasy
  - 6/10 Books
  - Audiobook
---

Finished “The Sandman: Act I”.
* Author: [Neil Gaiman](/book-authors/neil-gaiman) and [Dirk Maggs](/book-authors/dirk-maggs)
* First Published: [July 15, 2020](/book-publication-year/2020)
* Series: [Sandman Audible Original](/book-series/sandman-audible-original-series) Book 1
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53406893-the-sandman)
>
> When The Sandman, also known as Lord Morpheus - the immortal king of dreams, stories and the imagination - is pulled from his realm and imprisoned on Earth by a nefarious cult, he languishes for decades before finally escaping. Once free, he must retrieve the three “tools” that will restore his power and help him to rebuild his dominion, which has deteriorated in his absence. As the multi-threaded story unspools, The Sandman descends into Hell to confront Lucifer (Michael Sheen), chases rogue nightmares who have escaped his realm, and crosses paths with an array of characters from DC comic books, ancient myths, and real-world history, including: Inmates of Gotham City's Arkham Asylum, Doctor Destiny, the muse Calliope, the three Fates, William Shakespeare (Arthur Darvill), and many more. 