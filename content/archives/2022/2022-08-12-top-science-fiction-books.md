---
title: "Updating my Science Fiction Top 50 Books and Series"
slug: "top-science-fiction-books-2022-08-12"
author: "Paulo Pereira"
date: 2022-08-12T14:46:45+01:00
lastmod: 2022-08-12T14:46:45+01:00
description: "This is my Science Fiction Top 50 Books and Series.\n

1. Dune Series by Frank Herbert (5 books read so far, 1965-1987)\n
2. Ender's Game (Ender's Saga Book 1) by Orson Scott Card (1985)\n
3. Red Rising Saga by Pierce Brown (5 books read so far, 2014-2019)"
draft: false
toc: false
categories:
  - Books
tags:
  - Top Science Fiction
---

Here's some note about this list:
* It has single books, all series or one or more books in a series
* It's based on my "now that I'm doing this" feeling and I'm sure it will change over time
* I considered the books I read since I started to organized them, so it's probably lacking
* I add a **(*)** in front of the new entries
* I add a **(#.)** with the previous position

## Science Fiction Top 50

1. Dune Series by Frank Herbert (5 books read so far, 1965-1987) [Read more](/book-series/dune-series)
2. Ender's Game (Ender's Saga Book 1) by Orson Scott Card (1985) [Read more](/posts/books/enders-game-enders-saga-book-1)
3. **(4.)** Red Rising Saga by Pierce Brown (5 books read so far, 2014-2019) [Read more](/book-series/red-rising-saga-series)
4. **(3.)** The Time Machine by H.G. Wells (1895) [Read more](/posts/books/the-time-machine)
5. The Forever War by Joe Haldeman (3 books, 1974-1999) [Read more](/book-series/the-forever-war-series)
6. Childhood's End by Arthur C. Clarke (1953) [Read more](/posts/books/childhoods-end)
7. 2001: A Space Odyssey (Space Odyssey Book 1) by Arthur C. Clarke (1968) [Read more](/posts/books/2001-a-space-odyssey-book-1)
8. 1984 by George Orwell (1949) [Read more](/posts/books/1984)
9. Contact by Carl Sagan (1985) [Read more](/posts/books/contact)
10. Children of Time (Children of Time Book 1) by Adrian Tchaikovsky (2015) [Read more](/posts/books/children-of-time-children-of-time-book-1)
11. The Three-Body Problem (Remembrance of Earth's Past Book 1) by Liu Cixin (2008) [Read more](/posts/books/the-three-body-problem-remembrance-of-earths-past-book-1)
12. Hitchhiker's Guide to the Galaxy (Hitchhiker's Guide to the Galaxy Book 1) by Douglas Adams (1979) [Read more](/posts/books/hitchhikers-guide-to-the-galaxy-hitchhikers-guide-to-the-galaxy-book-1)
13. Jurassic Park Series (2 books, 1990-1995) by Michael Crichton [Read more](/book-series/jurassic-park-series)
14. **(*)** The Handmaid's Tale (The Handmaid's Tale Book 1) by Margaret Atwood (1985) [Read more](/posts/books/the-handmaids-tale-the-handmaids-tale-book-1)
15. **(14.)** Star Force Series by B.V. Larson and David VanDyke (12 books, 2010-2015) [Read more](/book-series/star-force-series)
16. **(15.)** Undying Mercenaries Series by B.V. Larson (17 books, 2013-2022) [Read more](/book-series/undying-mercenaries-series)
17. **(16.)** Ready Player One (Ready Player One Book 1) by Ernest Cline (2011) [Read more](/posts/books/ready-player-one-ready-player-one-book-1)
18. **(17.)** Bobiverse Series by Dennis E. Taylor (4 books, 2016-2020) [Read more](/book-series/bobiverse-series)
19. **(18.)** The Hunger Games Series by Suzanne Collins (3 books, 2008-2010) [Read more](/book-series/the-hunger-games-series)
20. **(21.)** Sphere by Michael Crichton (1987) [Read more](/posts/books/sphere)
21. **(19.)** Project Hail Mary by Andy Weir (2021) [Read more](/posts/books/project-hail-mary)
22. **(20.)** Threshold Series by Peter Clines (book 1, 2012 and book 4, 2020) [Read more](/book-series/threshold-series)
23. **(22.)** Remembrance of Earth's Past Series by Liu Cixin (book 2, 2008 and book 3, 2010) [Read more](/book-series/remembrance-of-earths-past-series)
24. **(23.)** The Martian (The Martian Book 1) by Andy Weir (2012) [Read more](/posts/books/the-martian-book-1)
25. **(24.)** Wayward Pines Series by Blake Crouch (3 books, 2012-2014) [Read more](/book-series/wayward-pines-series)
26. **(25.)** Dark Matter by Blake Crouch (2016) [Read more](/posts/books/dark-matter)
27. **(26.)** Recursion by Blake Crouch (2019) [Read more](/posts/books/recursion)
28. **(*)** Upgrade by Blake Crouch (2022) [Read more](/posts/books/upgrade)
29. **(27.)** Foundation (Foundation Book 1) by Isaac Asimov (1951) [Read more](/posts/books/foundation-book-1)
30. **(28.)** The Andromeda Strain (Andromeda Book 1) by Michael Crichton (1969) [Read more](/posts/books/the-andromeda-strain-andromeda-book-1)
31. **(29.)** Fahrenheit 451 by Ray Bradbury (1953) [Read more](/posts/books/fahrenheit-451)
32. **(30.)** Blade Runner (Blade Runner Book 1) by Philip K. Dick (1968) [Read more](/posts/books/blade-runner-blade-runner-book-1)
33. **(31.)** I Am Legend by Richard Matheson (1954) [Read more](/posts/books/i-am-legend)
34. **(*)** The Man in the High Castle by Philip K. Dick (1962) [Read more](/posts/books/the-man-in-the-high-castle)
35. **(32.)** Altered Carbon (Takeshi Kovacs Book 1) by Richard K. Morgan (2002) [Read more](/posts/books/altered-carbon-takeshi-kovacs-book-1)
36. **(33.)** Lock In Series by John Scalzi (2 books, 2014-2018)  [Read more](/book-series/lock-in-series)
37. **(34.)** Level Five (Killday Book 1) by William Ledbetter (2018) [Read more](/posts/books/level-five-killday-book-1)
38. **(35.)** Paradox Bound by Peter Clines (2017) [Read more](/posts/books/paradox-bound)
39. **(36.)** The Circle by Dave Eggers (2013) [Read more](/posts/books/the-circle)
40. **(37.)** Artemis by Andy Weir (2017) [Read more](/posts/books/artemis)
41. **(38.)** Armada by Ernest Cline (2015) [Read more](/posts/books/armada)
42. **(39.)** I, Robot (Robot Series Book 0.1) by Isaac Asimov (1950) [Read more](/posts/books/i-robot-robot-series-book-01)
43. **(40.)** The War of the Worlds by H.G. Wells (1898) [Read more](/posts/books/the-war-of-the-worlds)
44. **(41.)** We by Yevgeny Zamyatin (1920) [Read more](/posts/books/we)
45. **(42.)** The Terminal Man by Michael Crichton (1972) [Read more](/posts/books/the-terminal-man)
46. **(43.)** Starship Troopers by Robert A. Heinlein (1959) [Read more](/posts/books/starship-troopers)
47. **(44.)** Brave New World by Aldous Huxley (1932) [Read more](/posts/books/brave-new-world)
48. **(45.)** Threshold Series by Peter Clines (book 2, 2015 and book 3, 2019) [Read more](/book-series/threshold-series)
49. **(46.)** Alien (1979) and Aliens (1986) by Alan Dean Foster [Read more](/book-series/alien-series)
50. **(47.)** Alien: The Cold Forge by Alex White (2018) [Read more](/posts/books/alien-the-cold-forge)