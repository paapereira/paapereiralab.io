---
title: "The Art of Invisibility by Kevin Mitnick (2017)"
slug: "the-art-of-invisibility"
author: "Paulo Pereira"
date: 2019-01-18T18:22:00+00:00
lastmod: 2019-01-18T18:22:00+00:00
cover: "/posts/books/the-art-of-invisibility.jpg"
description: "Finished “The Art of Invisibility: The World's Most Famous Hacker Teaches You How to Be Safe in the Age of Big Brother and Big Data” by Kevin Mitnick."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Kevin Mitnick
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Kevin Mitnick
  - Nonfiction
  - Science
  - 7/10 Books
  - Audiobook
---

Finished “The Art of Invisibility: The World's Most Famous Hacker Teaches You How to Be Safe in the Age of Big Brother and Big Data”.
* Author: [Kevin Mitnick](/book-authors/kevin-mitnick)
* First Published: [February 14th 2017](/book-publication-year/2017)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35153958-the-art-of-invisibility)
>
> The world's most famous hacker reveals the secrets on how citizens and consumers can stay connected and on the grid safely and securely by using cloaking and counter-measures in today's age of Big Brother and big data.