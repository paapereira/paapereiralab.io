---
title: "Altered Carbon (Takeshi Kovacs Book 1) by Richard K. Morgan (2002)"
slug: "altered-carbon-takeshi-kovacs-book-1"
author: "Paulo Pereira"
date: 2019-02-24T22:30:00+00:00
lastmod: 2019-02-24T22:30:00+00:00
cover: "/posts/books/altered-carbon-takeshi-kovacs-book-1.jpg"
description: "Finished “Altered Carbon (Takeshi Kovacs Book 1)” by Richard K. Morgan."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2002
book authors:
  - Richard K. Morgan
book series:
  - Takeshi Kovacs Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Richard K. Morgan
  - Takeshi Kovacs Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/altered-carbon-takeshi-kovacs-book-1
---

Finished “Altered Carbon”.
* Author: [Richard K. Morgan](/book-authors/richard-k.-morgan)
* First Published: [February 28th 2002](/book-publication-year/2002)
* Series: [Takeshi Kovacs](/book-series/takeshi-kovacs-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40792913-altered-carbon)
>
> Four hundred years from now mankind is strung out across a region of interstellar space inherited from an ancient civilization discovered on Mars. The colonies are linked together by the occasional sublight colony ship voyages and hyperspatial data-casting. Human consciousness is digitally freighted between the stars and downloaded into bodies as a matter of course.
>
> But some things never change. So when ex-envoy, now-convict Takeshi Kovacs has his consciousness and skills downloaded into the body of a nicotine-addicted ex-thug and presented with a catch-22 offer, he really shouldn't be surprised. Contracted by a billionaire to discover who murdered his last body, Kovacs is drawn into a terrifying conspiracy that stretches across known space and to the very top of society.