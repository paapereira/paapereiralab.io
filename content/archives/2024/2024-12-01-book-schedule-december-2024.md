---
title: "Books Schedule update"
slug: "book-schedule-december-2024"
author: "Paulo Pereira"
date: 2024-12-01T08:45:00+00:00
lastmod: 2024-12-01T08:45:00+00:00
description: "December Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my December TBR (To Be Read) update for the next months.

* **[Read]** The Spoils of War (The Damned #3) by Alan Dean Foster 
* **[Read]** Harry Potter and the Chamber of Secrets (Harry Potter #2) by J. K. Rowling
* **[Read]** Hawksbill Station by Robert Silverberg
* **[Read]** Small Favor (The Dresden Files #10) by Jim Butcher
* **[Reading]** Star Wars: Episode VI: Return of the Jedi by James Kahn
* The Subtle Art of Not Giving a F*ck by Mark Manson
* Way Station by Clifford D. Simak
* Mystery Walk by Robert R. McCammon
* Aurora Rising (Prefect Dreyfus Emergency #1) by Alastair Reynolds
* The Sicilian (The Godfather #2) by Mario Puzo
* The Andromeda Evolution (Andromeda #2) by Daniel H. Wilson & Michael Crichton
* Planet of Exile (Hainish Cycle) by Ursula K. le Guin
* We Can Build You by Philip K. Dick
* A Coming of Age by Timothy Zahn
* The Expert System's Champion (The Expert System's Brother #2) by Adrian Tchaikovsky
* The Way of Kings (The Stormlight Archive #1) by Brandon Sanderson
* Dead Med (Prescription: Murder #1) by Freida McFadden
* The Bullet Journal Method by Ryder Carroll
* Harry Potter and the Prisoner of Azkaban (Harry Potter #3) by J. K. Rowling
* Turn Coat (The Dresden Files #11) by Jim Butcher
* Resurrection, Inc. by Kevin J. Anderson

I also have always an audiobook going too:

* **[Read]** Not Till We Are Lost (Bobiverse #5) by Dennis E. Taylor
* **[Reading]** Nexus by Yuval Noah Harari
* Breakfast at Tiffany's by Truman Capote
* Red Company: Invasion (Red Company #4) by B. V. Larson
* The Richest Man in Babylon by George S. Clason
* Impact Winter #1 by Travis Beacham
* A History of Video Games by Jeremy Parish
* The Sandman: Act III by Neil Gaiman
* Bag of Bones by Stephen King
