---
title: "Ender's Game (Ender's Saga Book 1) by Orson Scott Card (1985)"
slug: "enders-game-enders-saga-book-1"
author: "Paulo Pereira"
date: 2019-03-28T22:47:00+00:00
lastmod: 2019-03-28T22:47:00+00:00
cover: "/posts/books/enders-game-enders-saga-book-1.jpg"
description: "Finished “Ender's Game (Ender's Saga Book 1)” by Orson Scott Card."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Orson Scott Card
book series:
  - Ender's Saga Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Orson Scott Card
  - Ender's Saga Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Audiobook
aliases:
  - /posts/books/enders-game-enders-saga-book-1
---

Finished “Ender's Game”.
* Author: [Orson Scott Card](/book-authors/orson-scott-card)
* First Published: [1985](/book-publication-year/1980s)
* Series: [Ender's Saga](/book-series/enders-saga-series) Book 1
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/18192762-ender-s-game)
>
> Andrew "Ender" Wiggin thinks he is playing computer simulated war games; he is, in fact, engaged in something far more desperate. The result of genetic experimentation, Ender may be the military genius Earth desperately needs in a war against an alien enemy seeking to destroy all human life. The only way to find out is to throw Ender into ever harsher training, to chip away and find the diamond inside, or destroy him utterly. Ender Wiggin is six years old when it begins. He will grow up fast.
>
> But Ender is not the only result of the experiment. The war with the Buggers has been raging for a hundred years, and the quest for the perfect general has been underway almost as long. Ender's two older siblings, Peter and Valentine, are every bit as unusual as he is, but in very different ways. While Peter was too uncontrollably violent, Valentine very nearly lacks the capability for violence altogether. Neither was found suitable for the military's purpose. But they are driven by their jealousy of Ender, and by their inbred drive for power. Peter seeks to control the political process, to become a ruler. Valentine's abilities turn more toward the subtle control of the beliefs of commoner and elite alike, through powerfully convincing essays. Hiding their youth and identities behind the anonymity of the computer networks, these two begin working together to shape the destiny of Earth-an Earth that has no future at all if their brother Ender fails.