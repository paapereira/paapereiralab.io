---
title: "Alien: Out of the Shadows by Tim Lebbon (2013)"
slug: "alien-out-of-the-shadows"
author: "Paulo Pereira"
date: 2018-09-07T21:49:00+01:00
lastmod: 2018-09-07T21:49:00+01:00
cover: "/posts/books/alien-out-of-the-shadows.jpg"
description: "Finished “Alien: Out of the Shadows” by Tim Lebbon."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - Tim Lebbon
book series:
  - Alien Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Tim Lebbon
  - Alien Series  
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Alien: Out of the Shadows”.
* Author: [Tim Lebbon](/book-authors/tim-lebbon)
* First Published: [December 17th 2013](/book-publication-year/2013)
* Series: [Alien Series](/book-series/alien-series)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/29972711-alien)
>
> As a child, Chris Hooper dreamed of monsters. But in deep space, he found only darkness and isolation. Then, on planet LV178, he and his fellow miners discovered a storm-scoured, sand-blasted hell - and trimonite, the hardest material known to man.
>
> When a shuttle crashes into the mining ship Marion, the miners learn that there was more than trimonite deep in the caverns. There was evil, hibernating and waiting for suitable prey. Hoop and his associates uncover a nest of Xenomorphs, and hell takes on a new meaning. Quickly they discover that their only hope lies with the unlikeliest of saviors....
>
> Ellen Ripley, the last human survivor of the salvage ship Nostromo.