---
title: "In the Plex by Steven Levy (2011)"
slug: "in-the-plex"
author: "Paulo Pereira"
date: 2016-09-09T23:12:00+01:00
lastmod: 2016-09-09T23:12:00+01:00
cover: "/posts/books/in-the-plex.jpg"
description: "Finished “In the Plex: How Google Thinks, Works, and Shapes Our Lives” by Steven Levy."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Steven Levy
book series:
  - 
book genres:
  - Nonfiction
  - Business
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Steven Levy
  - Nonfiction
  - Business
  - 5/10 Books
  - Audiobook
---

Finished “In the Plex: How Google Thinks, Works, and Shapes Our Lives”.
* Author: [Steven Levy](/book-authors/steven-levy)
* First Published: [April 12th 2011](/book-publication-year/2011)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13635503-in-the-plex)
>
> Few companies in history have ever been as successful and as admired as Google, the company that has transformed the Internet and become an indispensable part of our lives. How has Google done it? Veteran technology reporter Steven Levy was granted unprecedented access to the company, and in this revelatory book he takes listeners inside Google headquarters - the Googleplex - to explain how Google works.
>
> While they were still students at Stanford, Google co-founders Larry Page and Sergey Brin revolutionized Internet search. They followed this brilliant innovation with another, as two of Google's earliest employees found a way to do what no one else had: make billions of dollars from Internet advertising. With this cash cow (until Google's IPO, nobody other than Google management had any idea how lucrative the company's ad business was), Google was able to expand dramatically and take on other transformative projects: more efficient data centers, open-source cell phones, free Internet video (YouTube), cloud computing, digitizing books, and much more.
>
> The key to Google's success in all these businesses, Levy reveals, is its engineering mind-set and adoption of such Internet values as speed, openness, experimentation, and risk taking. After it's unapologetically elitist approach to hiring, Google pampers its engineers with free food and dry cleaning, on-site doctors and masseuses, and gives them all the resources they need to succeed. Even today, with a workforce of more than 23,000, Larry Page signs off on every hire.
>
> But has Google lost its innovative edge? It stumbled badly in China. And now, with its newest initiative, social networking, Google is chasing a successful competitor for the first time. Some employees are leaving the company for smaller, nimbler start-ups. Can the company that famously decided not to be "evil" still compete?
>
> No other book has turned Google inside out as Levy does with In the Plex.