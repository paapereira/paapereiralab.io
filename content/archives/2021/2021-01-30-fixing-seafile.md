---
title: "Fixing Seafile"
slug: "fixing-seafile"
author: "Paulo Pereira"
date: 2021-01-30T16:28:27+00:00
lastmod: 2021-01-30T16:28:27+00:00
description: "Yesterday I woke up without access to my Seafile instance.\n

Here's my notes/steps/fixes for the issue."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Seafile
aliases:
  - /posts/fixing-seafile
---

Yesterday I woke up without access to my Seafile instance.

Here's my notes/steps/fixes for the issue.

Restart Seafile and check for errors:

```bash
sudo systemctl restart seafile
sudo systemctl status seafile
sudo systemctl restart seahub
sudo systemctl status seahub
```

No errors here.

Check Seafile logs:

```bash
sudo -u seafile -s /bin/sh
cd /srv/seafile/logs
```

I found the following in `elasticsearch.log`:

```text
max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
```

Maybe this is the problem? Some searching in seafile forum, and I found [this](https://forum.seafile.com/t/elasticsearch-not-running-after-upgrade-to-7-0-8-from-6-2-4/9819).

I then tried to updating outdated python packages, rebuild `elasticsearch` index and changed the `max_map_count` value:

```bash
sudo systemctl stop seahub
sudo systemctl stop seafile

sudo pip list --outdated
sudo pip3 install -U --timeout=3600 chardet Django feedparser idna jaraco.classes josepy mock Pillow pip urllib3

cd /srv/seafile/seafile-server-latest
pro/pro.py search --clear
pro/pro.py search --update

sudo sysctl -w vm.max_map_count=262144
```

Rebooted, but still no Seafile. The elasticsearch "too low" warning disappeared though.

Then I thought. What about apache?

```bash
sudo systemctl status httpd
```

Yep. This is the problem. I had updated php to version 8, and had models loaded for php7 in `httpd.conf`. Just replaced php7 to php, restarted apache and Seafile is back.

```bash
sudo vi /etc/httpd/conf/httpd.conf
sudo systemctl restart httpd
sudo systemctl status httpd
```