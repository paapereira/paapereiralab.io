---
title: "The Man on the Mountaintop by Susan Trott (2017)"
slug: "the-man-on-the-mountaintop"
author: "Paulo Pereira"
date: 2020-01-26T21:34:00+00:00
lastmod: 2020-01-26T21:34:00+00:00
cover: "/posts/books/the-man-on-the-mountaintop.jpg"
description: "Finished “The Man on the Mountaintop” by Susan Trott."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Susan Trott
book series:
  -
book genres:
  - Fiction
book scores:
  - 4/10 Books
tags:
  - Book Log
  - Susan Trott
  - Fiction
  - 4/10 Books
  - Audiobook
---

Finished “The Man on the Mountaintop”.
* Author: [Susan Trott](/book-authors/susan-trott)
* First Published: [November 9th 2017](/book-publication-year/2017)
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43658721-the-man-on-the-mountaintop)
>
> The Man on the Mountaintop tells the story of Holy Man Joe, an ageing and unassuming man who lives in a hermitage on top of a mountain. During the summer months, thousands of hopefuls line the single-file path leading to his door, seeking his wisdom. From bombastic, wealthy nobles intent on cheating their way to the top to drunkards who gradually build the physical and mental strength they need to quit their addiction, The Man on the Mountaintop is a rousing tale full of humour, wit and life lessons.