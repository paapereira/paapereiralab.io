---
title: "The Secret Life of Walter Mitty by James Thurber (1939)"
slug: "the-secret-life-of-walter-mitty"
author: "Paulo Pereira"
date: 2023-02-26T18:30:00+00:00
lastmod: 2023-02-26T18:30:00+00:00
cover: "/posts/books/the-secret-life-of-walter-mitty.jpg"
description: "Finished “The Secret Life of Walter Mitty” by James Thurber."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1930s
book authors:
  - James Thurber
book series:
  - 
book genres:
  - Fiction
  - Classics
  - Short Stories
book scores:
  - 5/10 Books
tags:
  - Book Log
  - James Thurber
  - Fiction
  - Classics
  - Short Stories
  - 5/10 Books
  - Audiobook
---

Finished “The Secret Life of Walter Mitty”.
* Author: [James Thurber](/book-authors/james-thurber)
* First Published: [January 1, 1939](/book-publication-year/1930s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/19183736-the-secret-life-of-walter-mitty)
>
> The mild-mannered Mitty escapes his extremely humdrum and ineffectual existence by leaping into a myriad of fantasies - imaginative daydreams that range from piloting a Navy plane to performing as a brilliant surgeon to coolly leaning against the wall of a firing squad, all while escorting his wife on their regular shopping trip to Danbury, Connecticut.