---
title: "0"
slug: "alone-with-the-stars"
author: "Paulo Pereira"
date: 2021-06-06T18:00:00+01:00
lastmod: 2021-06-06T18:00:00+01:00
cover: "/posts/books/alone-with-the-stars.jpg"
description: "Finished “Alone with the Stars” by David R. Gillham."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - David R. Gillham
book series:
  - 
book genres:
  - Fiction
book scores:
  - 4/10 Books
tags:
  - Book Log
  - David R. Gillham
  - Fiction
  - 4/10 Books
  - Audiobook
---

Finished “Alone with the Stars”.
* Author: [David R. Gillham](/book-authors/david-r.-gillham)
* First Published: [January 30th 2020](/book-publication-year/2020)
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/52174298-alone-with-the-stars)
>
> In the summer of 1937, Amelia Earhart is the most famous woman in the world - a record-breaking pilot, a best-selling author, and a modern woman shattering the glass ceiling in the early days of aviation.
>
> And then she vanishes.
>
> In Tampa, Florida, 15-year-old Lizzie Friedlander spends her afternoons glued to her father's radio, tapping into the enormity of a world she longs to travel. Lizzie can hardly believe her ears when she picks up a radio signal from a faraway source that sets her heart racing: "Amelia Earhart calling SOS!"
>
> As Lizzie copies down the transmissions, it's clear that the Amelia Earhart is not lost at sea, as the newspapers are dreading, but alive and calling for help. In a race against time, Lizzie must convince the local Coast Guard that the radio transmissions were real and that Earhart's life hangs in the balance. But will anyone believe her? 