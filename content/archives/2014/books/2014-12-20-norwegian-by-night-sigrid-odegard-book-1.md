---
title: "Norwegian by Night (Sigrid Ødegård Book 1) by Derek B. Miller (2012)"
slug: "norwegian-by-night-sigrid-odegard-book-1"
author: "Paulo Pereira"
date: 2014-12-20T23:56:00+00:00
lastmod: 2014-12-20T23:56:00+00:00
cover: "/posts/books/norwegian-by-night-sigrid-odegard-book-1.jpg"
description: "Finished “Norwegian by Night (Sigrid Ødegård Book 1)” by Derek B. Miller."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - Derek B. Miller
book series:
  - Sigrid Odegard Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Derek B. Miller
  - Sigrid Odegard Series
  - Fiction
  - Mystery
  - 5/10 Books
  - Audiobook
---

Finished “Norwegian by Night”.
* Author: [Derek B. Miller](/book-authors/derek-b.-miller)
* First Published: [2012](/book-publication-year/2012)
* Series: [Sigrid Ødegård](/book-series/sigrid-odegard-series) Book 1
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/15814497-norwegian-by-night)
>
> He will not admit it to Rhea and Lars - never, of course not - but Sheldon can't help but wonder what it is he's doing here..
>
> Eighty-two years old, and recently widowed, Sheldon Horowitz has grudgingly moved to Oslo, with his grand-daughter and her Norwegian husband. An ex-Marine, he talks often to the ghosts of his past - the friends he lost in the Pacific and the son who followed him into the US Army, and to his death in Vietnam.
>
> When Sheldon witnesses the murder of a woman in his apartment complex, he rescues her six-year-old son and decides to run. Pursued by both the Balkan gang responsible for the murder, and the Norwegian police, he has to rely on training from over half a century before to try and keep the boy safe. Against a strange and foreign landscape, this unlikely couple, who can't speak the same language, start to form a bond that may just save them both.
>
> An extraordinary debut, featuring a memorable hero, Norwegian by Night is the last adventure of a man still trying to come to terms with the tragedies of his life. Compelling and sophisticated, it is both a chase through the woods thriller and an emotionally haunting novel about ageing and regret.