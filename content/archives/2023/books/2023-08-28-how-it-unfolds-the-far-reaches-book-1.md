---
title: "How It Unfolds (The Far Reaches Book 1) by James S.A. Corey (2023)"
slug: "how-it-unfolds-the-far-reaches-book-1"
author: "Paulo Pereira"
date: 2023-08-28T21:00:00+01:00
lastmod: 2023-08-28T21:00:00+01:00
cover: "/posts/books/how-it-unfolds-the-far-reaches-book-1.jpg"
description: "Finished “How It Unfolds (The Far Reaches Book 1)” by James S.A. Corey."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - James S.A. Corey
book series:
  - The Far Reaches Series
book genres:
  - Fiction
  - Science Fiction
  - Short Stories
book scores:
  - 7/10 Books
tags:
  - Book Log
  - James S.A. Corey
  - The Far Reaches Series
  - Fiction
  - Science Fiction
  - Short Stories
  - 7/10 Books
  - Ebook
---

Finished “How It Unfolds”.
* Author: [James S.A. Corey](/book-authors/james-s.a.-corey)
* First Published: [June 27, 2023](/book-publication-year/2023)
* Series: [The Far Reaches](/book-series/the-far-reaches-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/151908910-how-it-unfolds)
>
> An astronaut’s interstellar mission is a personal journey of a thousand second chances in an exhilarating short story by James S. A. Corey, the New York Times bestselling author of The Expanse series. Roy Court and his crew are taking the trip of a lifetime—several lifetimes in fact—duplicated and dispatched across the galaxies searching for Earthlike planets. Many possibilities for the future. Yet for Roy, no matter how many of him there are, there’s still just one painful, unchangeable past. In what world can a broken relationship be reborn? The universe is so vast, there’s always room for hope.