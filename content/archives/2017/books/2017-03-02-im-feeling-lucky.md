---
title: "I'm Feeling Lucky by Douglas Edwards (2011)"
slug: "im-feeling-lucky"
author: "Paulo Pereira"
date: 2017-03-02T22:00:00+00:00
lastmod: 2017-03-02T22:00:00+00:00
cover: "/posts/books/im-feeling-lucky.jpg"
description: "Finished “I'm Feeling Lucky: The Confessions of Google Employee Number 59” by Douglas Edwards."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Douglas Edwards
book series:
  - 
book genres:
  - Nonfiction
  - Biography
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Douglas Edwards
  - Nonfiction
  - Biography
  - 5/10 Books
  - Audiobook
---

Finished “I'm Feeling Lucky: The Confessions of Google Employee Number 59”.
* Author: [Douglas Edwards](/book-authors/douglas-edwards)
* First Published: [January 1st 2011](/book-publication-year/2011)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/15741948-i-m-feeling-lucky)
>
> Comparing Google to an ordinary business is like comparing a rocket to an Edsel. No academic analysis or bystander's account can capture it. Now Doug Edwards, Employee Number 59, offers the first inside view of Google, giving listeners a chance to fully experience the bizarre mix of camaraderie and competition at this phenomenal company.
>
> Edwards, Google's first director of marketing and brand management, describes it as it happened. We see the first, pioneering steps of Larry Page and Sergey Brin, the company's young, idiosyncratic partners; the evolution of the company's famously nonhierarchical structure (where every employee finds a problem to tackle or a feature to create and works independently); the development of brand identity; the races to develop and implement each new feature; and the many ideas that never came to pass. Above all, Edwards - a former journalist who knows how to write - captures the Google Experience, the rollercoaster ride of being part of a company creating itself in a whole new universe.
>
> I'm Feeling Lucky captures for the first time the unique, self-invented, yet profoundly important culture of the world's most transformative corporation.