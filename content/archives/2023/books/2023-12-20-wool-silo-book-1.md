---
title: "Wool (Silo Book 1) by Hugh Howey (2012)"
slug: "wool-silo-book-1"
author: "Paulo Pereira"
date: 2023-12-20T11:30:00+00:00
lastmod: 2023-12-20T11:30:00+00:00
cover: "/posts/books/wool-silo-book-1.jpg"
description: "Finished “Wool (Silo Book 1)” by Hugh Howey."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - Hugh Howey
book series:
  - Silo Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Hugh Howey
  - Silo Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “Wool”.
* Author: [Hugh Howey](/book-authors/hugh-howey)
* First Published: [January 27, 2012](/book-publication-year/2012)
* Series: [Silo](/book-series/silo-series) Book 1
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/157608295-wool)
>
> Wool is the story of mankind clawing for survival. The world outside has grown toxic, the view of it limited, talk of it forbidden. The remnants of humanity live underground in a single silo.
> 
> But there are always those who hope, who dream. These are the dangerous people, the residents who infect others with their optimism. Their punishment is simple. They are given the very thing they want: They are allowed to go outside.
> 
> After the previous sheriff leaves the silo in a terrifying ritual, Juliette, a mechanic from the down deep, is suddenly and inexplicably promoted to the head of law enforcement. With newfound power and with little regard for the customs she is supposed to abide, Juliette uncovers hints of a sinister conspiracy. Tugging this thread may uncover the truth . . . or it could kill every last human alive.