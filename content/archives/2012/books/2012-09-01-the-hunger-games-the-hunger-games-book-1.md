---
title: "The Hunger Games (The Hunger Games Book 1) by Suzanne Collins (2008)"
slug: "the-hunger-games-the-hunger-games-book-1"
author: "Paulo Pereira"
date: 2012-09-01T23:00:00+01:00
lastmod: 2012-09-01T23:00:00+01:00
cover: "/posts/books/the-hunger-games-the-hunger-games-book-1.jpg"
description: "Finished “The Hunger Games (The Hunger Games Book 1)” by Suzanne Collins."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2008
book authors:
  - Suzanne Collins
book series:
  - The Hunger Games Series
book genres:
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Suzanne Collins
  - The Hunger Games Series
  - Science Fiction
  - 9/10 Books
  - Audiobook
---

Finished “The Hunger Games”.
* Author: [Suzanne Collins](/book-authors/suzanne-collins)
* First Published: [September 14th 2008](/book-publication-year/2008)
* Series: [The Hunger Games](/book-series/the-hunger-games-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/9171146-the-hunger-games)
>
> Could you survive on your own, in the wild, with everyone out to make sure you don't live to see the morning?
>
> In the ruins of a place once known as North America lies the nation of Panem, a shining Capitol surrounded by 12 outlying districts. The Capitol is harsh and cruel and keeps the districts in line by forcing them all to send one boy and one girl between the ages of 12 and 18 to participate in the annual Hunger Games, a fight to the death on live TV.
>
> Sixteen-year-old Katniss Everdeen, who lives alone with her mother and younger sister, regards it as a death sentence when she steps forward to take her sister's place in the Games. But Katniss has been close to dead before - and survival, for her, is second nature. Without really meaning to, she becomes a contender. But if she is to win, she will have to start making choices that weigh survival against humanity and life against love.