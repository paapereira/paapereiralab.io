---
title: "Level Six (Killday Book 2) by William Ledbetter (2020)"
slug: "level-six-killday-book-2"
author: "Paulo Pereira"
date: 2023-09-17T18:00:00+01:00
lastmod: 2023-09-17T18:00:00+01:00
cover: "/posts/books/level-six-killday-book-2.jpg"
description: "Finished “Level Six (Killday Book 2)” by William Ledbetter."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - William Ledbetter
book series:
  - Killday Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - William Ledbetter
  - Killday Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Level Six”.
* Author: [William Ledbetter](/book-authors/william-ledbetter)
* First Published: [December 3, 2020](/book-publication-year/2020)
* Series: [Killday](/book-series/killday-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/56226188-level-six)
>
> Fifteen years after warring artificial intelligences nearly destroyed Earth, a young woman finds an artifact from that struggle, upsetting a delicate balance of power and dragging her into the middle of a new fight for humanity's survival.
> 
> With a significant portion of the planet covered in heat-absorbing black dust, Earth's biosphere teeters on the edge of collapse. One AI faction is attempting to fix the damage, with huge machines trundling across the countryside sucking up dust and planting grass, while another faction cares only about elevating itself to a higher plane of intelligence and will destroy anyone who gets in the way.
> 
> As the only humans still not controlled by AIs race to build huge orbital habitats so some humans will survive should temperatures continue to rise, a more secretive organization grows in the shadows and idolizes the man who triggered the nano-replicator attacks that nearly destroyed the world. They, too, believe the only way to stop the AIs is to annihilate everything.
> 
> In Nebula Award winner William Ledbetter’s Level Six, one woman has the power to save humanity - if she can survive long enough to use it.