---
title: "The Science of Sci-Fi: From Warp Speed to Interstellar Travel by Erin Macdonald (2019)"
slug: "the-science-of-sci-fi"
author: "Paulo Pereira"
date: 2021-05-01T21:00:00+01:00
lastmod: 2021-05-01T21:00:00+01:00
cover: "/posts/books/the-science-of-sci-fi.jpg"
description: "Finished “The Science of Sci-Fi: From Warp Speed to Interstellar Travel” by Erin Macdonald."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Erin Macdonald
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Erin Macdonald
  - Nonfiction
  - Science
  - B Rated Books
  - Audiobook
---

Finished “The Science of Sci-Fi: From Warp Speed to Interstellar Travel”.
* Author: [Erin Macdonald](/book-authors/erin-macdonald)
* First Published: [November 19th 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/48982218-the-science-of-sci-fi)
>
> Science fiction allows listeners to go places they can only dream of seeing—other worlds, distant stars, entirely different galaxies. While not every story is concerned with the hard science behind space travel and other futuristic ventures, fiction can give listeners an amazing insight into what people could be capable of and what people dream of doing.
>
> In the 10 lectures of The Science of Sci-Fi: From Warp Speed to Interstellar Travel, Professor Erin Macdonald interweaves real science and the achievements of the imagination to reveal the truth that underlies favorite stories and sheds light on what the future may hold. From faster-than-light travel to journeys through time itself, science fiction makes humanity seem limitless. So, what scientific boundaries are people pushing against while seeking to fly among the stars?