---
title: "The Private Life of Elder Things by Adrian Tchaikovsky, Adam Gauntlett and Keris McDonald (2016)"
slug: "the-private-life-of-elder-things"
author: "Paulo Pereira"
date: 2023-09-05T21:00:00+01:00
lastmod: 2023-09-05T21:00:00+01:00
cover: "/posts/books/the-private-life-of-elder-things.jpg"
description: "Finished “The Private Life of Elder Things” by Adrian Tchaikovsky, Adam Gauntlett and Keris McDonald."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - Adrian Tchaikovsky
  - Adam Gauntlett
  - Keris McDonald
book series:
  - 
book genres:
  - Fiction
  - Horror
  - Fantasy
  - Short Stories
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Adam Gauntlett
  - Keris McDonald
  - Fiction
  - Horror
  - Fantasy
  - Short Stories
  - 6/10 Books
  - Ebook
---

Finished “The Private Life of Elder Things”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky), [Adam Gauntlett](/book-authors/adam-gauntlett) and [Keris McDonald](/book-authors/keris-mcdonald)
* First Published: [January 1, 2016](/book-publication-year/2016)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31888668-the-private-life-of-elder-things)
>
> From the wastes of the sea to the shadows of our own cities, we are not alone. But what happens where the human world touches the domain of races ancient and alien? Museum curators, surveyors, police officers, archaeologists, mathematicians; from derelict buildings to country houses to the London Underground, another world is just a breath away, around the corner, watching and waiting for you to step into its power. The Private Life of Elder Things is a collection of new Lovecraftian fiction about confronting, discovering and living alongside the creatures of the Mythos.