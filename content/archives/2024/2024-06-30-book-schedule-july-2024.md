---
title: "Books Schedule update"
slug: "book-schedule-july-2024"
author: "Paulo Pereira"
date: 2024-06-30T14:30:00+01:00
lastmod: 2024-06-30T14:30:00+01:00
description: "July Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my July TBR (To Be Read) update for the next months.

* **[Read]** Harry Potter and the Sorcerer's Stone (Harry Potter #1) by J. K. Rowling
* **[Read]** Star Wars: Episode V: The Empire Strikes Back by Donald F. Glut
* **[Read]** White Night (The Dresden Files #9) by Jim Butcher
* **[Read]** Stranger in a Strange Land by Robert A. Heinlein
* **[Reading]** Dragon Teeth by Michael Crichton
* They Thirst by Robert R. McCammon
* Dust (Silo #3) by Hugh Howey
* Lord of the Flies by William Golding
* Inhibitor Phase (Revelation Space #4) by Alastair Reynolds
* Rocannon's World (Hainish Cycle) by Ursula K. le Guin
* The Expert System's Brother (The Expert System's Brother #1) by Adrian Tchaikovsky
* The Godfather (The Godfather #1) by Mario Puzo
* Snow Crash by Neal Stephenson
* Tress of the Emerald Sea by Brandon Sanderson
* The Spoils of War (The Damned #3) by Alan Dean Foster
* Hawksbill Station by Robert Silverberg
* Harry Potter and the Chamber of Secrets (Harry Potter #2) by J. K. Rowling
* Star Wars: Episode VI: Return of the Jedi by James Kahn
* Small Favor (The Dresden Files #10) by Jim Butcher
* The Subtle Art of Not Giving a F*ck by Mark Manson

I also have always an audiobook going too:

* **[Read]** The Showman by Simon Shuster
* **[Reading]** The Sandman: Act II by Neil Gaiman
* Desperation by Stephen King
* Breakfast at Tiffany's by Truman Capote
* Red Company: Invasion (Red Company #4) by B. V. Larson
* The Richest Man in Babylon by George S. Clason
* Impact Winter #1 by Travis Beacham
* A History of Video Games by Jeremy Parish
