---
title: "The Dark Forest (Remembrance of Earth's Past Book 2) by Liu Cixin (2008)"
slug: "the-dark-forest-remembrance-of-earths-past-book-2"
author: "Paulo Pereira"
date: 2019-12-11T22:11:00+00:00
lastmod: 2019-12-11T22:11:00+00:00
cover: "/posts/books/the-dark-forest-remembrance-of-earths-past-book-2.png"
description: "Finished “The Dark Forest (Remembrance of Earth's Past Book 2)” by Liu Cixin."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2008
book authors:
  - Liu Cixin
book series:
  - Remembrance of Earth's Past Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Liu Cixin
  - Remembrance of Earth's Past Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Audiobook
---

Finished “The Dark Forest”.
* Author: [Liu Cixin](/book-authors/liu-cixin)
* First Published: [2008](/book-publication-year/2008)
* Series: [Remembrance of Earth's Past](/book-series/remembrance-of-earths-past-series) Book 2
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/30283483-the-dark-forest)
>
> This is the second novel in the "Three Body" near-future trilogy, written by China's multiple-award-winning science fiction author, Cixin Liu.
>
> In Dark Forest, Earth is reeling from the revelation of a coming alien invasion—four centuries in the future. The aliens' human collaborators have been defeated, but the presence of the sophons, the subatomic particles that allow Trisolaris instant access to all human information, means that Earth's defense plans are exposed to the enemy. Only the human mind remains a secret.
>
> This is the motivation for the Wallfacer Project, a daring plan that grants four men enormous resources to design secret strategies, hidden through deceit and misdirection from Earth and Trisolaris alike. Three of the Wallfacers are influential statesmen and scientists, but the fourth is a total unknown. Luo Ji, an unambitious Chinese astronomer and sociologist, is baffled by his new status. All he knows is that he's the one Wallfacer that Trisolaris wants dead.