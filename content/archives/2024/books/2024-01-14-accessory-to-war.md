---
title: "Accessory to War by Neil deGrasse Tyson and Avis Lang (2018)"
slug: "accessory-to-war"
author: "Paulo Pereira"
date: 2024-01-14T16:00:00+00:00
lastmod: 2024-01-14T16:00:00+00:00
cover: "/posts/books/accessory-to-war.jpg"
description: "Finished “Accessory to War” by Neil deGrasse Tyson and Avis Lang."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Neil deGrasse Tyson
  - Avis Lang
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Neil deGrasse Tyson
  - Avis Lang
  - Nonfiction
  - Science
  - 7/10 Books
  - Audiobook
---

Finished “Accessory to War: The Unspoken Alliance Between Astrophysics and the Military”.
* Author: [Neil deGrasse Tyson](/book-authors/neil-degrasse-tyson) and [Avis Lang](/book-authors/avis-lang)
* First Published: [September 11, 2018](/book-publication-year/2018)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/52199912-accessory-to-war)
>
> In this fascinating foray into the millennia-long relationship between science and military power, acclaimed astrophysicist Neil deGrasse Tyson and writer Avis Lang examine how the methods and tools of astrophysics have been enlisted in the service of war. “The overlap is strong, and it’s a two- way street,” say the authors, because the astrophysicists and military planners care about many of the same things: multi- spectral detection, ranging, tracking, imaging, high ground, nuclear fusion, and access to space. Tyson and Lang call it a “curiously complicit” alliance.
> 
> Spanning early celestial navigation to satellite-enabled warfare, 'ACCESSORY TO WAR' is a richly researched and provocative examination of the intersection of science, technology, industry, and power that will introduce Tyson’s millions of fans to yet another dimension of how the universe has shaped our lives and our world.