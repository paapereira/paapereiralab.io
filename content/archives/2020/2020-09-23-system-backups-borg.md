---
title: "System backups with BorgBackup and BorgBase"
slug: "system-backups-borg"
author: "Paulo Pereira"
date: 2020-09-23T17:46:20+01:00
lastmod: 2020-09-23T17:46:20+01:00
description: "Since my last [post](/posts/borg/) on [BorgBackup](https://www.borgbackup.org/) and [BorgBase](https://www.borgbase.com/), I have subscribed to a 1 TB annual plan and have my daily and weekly /home backups running.\n

The next thing to setup is my system backups. For this (for now) I’m using a more weekly manually approach.\n

`sudo pacman -S borgsudo pacman -S borg`"
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - BorgBackup
  - BorgBase
---

Since my last [post](/posts/borg/) on [BorgBackup](https://www.borgbackup.org/) and [BorgBase](https://www.borgbase.com/), I have subscribed to a 1 TB annual plan and have my daily and weekly /home backups running.

The next thing to setup is my system backups. For this (for now) I’m using a more weekly manually approach.

## Installation

```bash
sudo pacman -S borg
```

## Setup

- Create a new key to use in your backups (*leave the password blank, I couldn't get it to work witch a password*)

```bash
sudo ssh-keygen -o -a 100 -t ed25519
```

- Add your key to BorgBase (Account > SSH Keys)

```bash
sudo cat /root/.ssh/id_ed25519.pub
```

- Create a new ‘desktop-system’ Repository in BorgBase and associate your SSH Key

- Initialize the Repository (you can see your repository address in BorgBase)

Here you must choose a strong password.

```bash
sudo borg init --encryption=repokey-blake2 XXXXX@XXXXX.repo.borgbase.com:repo
```

## Backup

Every time you want to create a new backup just execute:

```bash
sudo borg create --list --progress --info --log-json --json --filter=AM -C lz4 --exclude '/root/.ssh/' XXXXX@XXXXX.repo.borgbase.com:repo::desktop-system-{now:%Y-%m-%dT%H:%M:%S} /etc /var /root /opt
```

## Checking your archives

You can list the archives or files you have and mount them to explore the backed up files.

* List the archives

```bash
sudo borg list XXXXX@XXXXX.repo.borgbase.com:repo
```

* List the files in an archive

```bash
sudo borg list XXXXX@XXXXX.repo.borgbase.com:repo::desktop-system-2020-09-23T17:38:52
```

* Mount an archive and access your files

```bash
mkdir /tmp/mount
sudo borg mount XXXXX@XXXXX.repo.borgbase.com:repo::desktop-system-2020-09-23T17:38:52 /tmp/mount
sudo ls /tmp/mount
sudo ls -l /tmp/mount/etc/fstab
sudo borg umount /tmp/mount
```

## Cleaning your archives

It’s also a good idea to prune your repository to clean out older files.

```bash
sudo borg prune -v --list --stats --keep-within=10d --keep-weekly=4 --keep-monthly=6 --keep-yearly=2 XXXXX@XXXXX.repo.borgbase.com:repo
```