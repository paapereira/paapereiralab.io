---
title: "Dexter Is Delicious (Dexter Book 5) by Jeff Lindsay (2010)"
slug: "dexter-is-delicious-dexter-book-5"
author: "Paulo Pereira"
date: 2015-03-02T23:45:00+00:00
lastmod: 2015-03-02T23:45:00+00:00
cover: "/posts/books/dexter-is-delicious-dexter-book-5.jpg"
description: "Finished “Dexter Is Delicious (Dexter Book 5)” by Jeff Lindsay."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2010
book authors:
  - Jeff Lindsay
book series:
  - Dexter Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jeff Lindsay
  - Dexter Series
  - Fiction
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “Dexter Is Delicious”.
* Author: [Jeff Lindsay](/book-authors/jeff-lindsay)
* First Published: [September 7th 2010](/book-publication-year/2010)
* Series: [Dexter](/book-series/dexter-series) Book 5
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13381423-dexter-is-delicious)
>
> Dexter Morgan's happy homicidal life is undergoing some major changes. He's always live by a single golden rule - he kills only people who deserve it. But the Miami blood-spatter analyst has recently become a daddy - to an eight-pound curiosity named Lily Anne - and strangely, Dex's dark urges seem to have left him. Is he ready to become an overprotective father? To pick up soft teddy bears instead of his trusty knife, duct tape, and fishing wire? What's a serial killer to do?
>
> Then Dexter is summoned to investigate the disappearance of an eighteen-year-old girl who seems to have been abducted by a bizarre group...who just may be vampires...and - possibly - cannibals. Nothing like the familiar hum of his day job to get Dexter's creative dark juices flowing again. Assisting his bull-in-a-china-shop detective sister, Deborah, Dex wades into an investigation that gets more disturbing by the moment. And to compound the complication of Dexter's ever-more-complicated life, a person from his past suddenly reappears...moving dangerously close to his home turf and threatening to destroy the one thing tat has maintained Dexter's pretend human cover and kept him out of the electric chair: his new family.
>
> From an uncharacteristically racy encounter in the Florida Everglades to the most bizarre fringe nightclub in the anything-goes Miami scene, Dexter Is Delicious is an ingenious journey through the dark recesses of Dexter's lovably cold soul. Jeff Lindsay is once again at the top of his game, with this new novel that will thrill fans of his bestselling series.