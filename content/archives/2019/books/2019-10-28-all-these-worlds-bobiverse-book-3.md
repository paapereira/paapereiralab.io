---
title: "All These Worlds (Bobiverse Book 3) by Dennis E. Taylor (2017)"
slug: "all-these-worlds-bobiverse-book-3"
author: "Paulo Pereira"
date: 2019-10-28T22:28:00+00:00
lastmod: 2019-10-28T22:28:00+00:00
cover: "/posts/books/all-these-worlds-bobiverse-book-3.png"
description: "Finished “All These Worlds (Bobiverse Book 3)” by Dennis E. Taylor."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Dennis E. Taylor
book series:
  - Bobiverse Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Dennis E. Taylor
  - Bobiverse Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “All These Worlds”.
* Author: [Dennis E. Taylor](/book-authors/dennis-e.-taylor)
* First Published: [2017](/book-publication-year/2017)
* Series: [Bobiverse](/book-series/bobiverse-series) Book 3
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35663086-all-these-worlds)
>
> Being a sentient spaceship really should be more fun. But after spreading out through space for almost a century, Bob and his clones just can't stay out of trouble.
>
> They've created enough colonies so humanity shouldn't go extinct. But political squabbles have a bad habit of dying hard, and the Brazilian probes are still trying to take out the competition. And the Bobs have picked a fight with an older, more powerful species with a large appetite and a short temper.
>
> Still stinging from getting their collective butts kicked in their first encounter with the Others, the Bobs now face the prospect of a decisive final battle to defend Earth and its colonies. But the Bobs are less disciplined than a herd of cats, and some of the younger copies are more concerned with their own local problems than defeating the Others.
>
> Yet salvation may come from an unlikely source. A couple of eighth-generation Bobs have found something out in deep space. All it will take to save the Earth and perhaps all of humanity is for them to get it to Sol — unless the Others arrive first.