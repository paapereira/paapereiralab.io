---
title: "Catching Fire (The Hunger Games Book 2) by Suzanne Collins (2009)"
slug: "catching-fire-the-hunger-games-book-2"
author: "Paulo Pereira"
date: 2014-04-06T23:36:00+01:00
lastmod: 2014-04-06T23:36:00+01:00
cover: "/posts/books/catching-fire-the-hunger-games-book-2.jpg"
description: "Finished “Catching Fire (The Hunger Games Book 2)” by Suzanne Collins."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2009
book authors:
  - Suzanne Collins
book series:
  - The Hunger Games Series
book genres:
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Suzanne Collins
  - The Hunger Games Series
  - Science Fiction
  - 8/10 Books
  - Audiobook
---

Finished “Catching Fire”.
* Author: [Suzanne Collins](/book-authors/suzanne-collins)
* First Published: [September 1st 2009](/book-publication-year/2009)
* Series: [The Hunger Games](/book-series/the-hunger-games-series) Book 2
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13484101-catching-fire)
>
> Against all odds, Katniss Everdeen has won the annual Hunger Games with fellow district tribute Peeta Mellark. But it was a victory won by defiance of the Capitol and their harsh rules. Katniss and Peeta should be happy. After all, they have just won for themselves and their families a life of safety and plenty. But there are rumors of rebellion among the subjects, and Katniss and Peeta, to their horror, are the faces of that rebellion. The Capitol is angry. The Capitol wants revenge!