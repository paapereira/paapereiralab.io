---
title: "The Lost Symbol (Robert Langdon Book 3) by Dan Brown (2009)"
slug: "the-lost-symbol-robert-langdon-book-3"
author: "Paulo Pereira"
date: 2013-10-28T23:48:00+00:00
lastmod: 2013-10-28T23:48:00+00:00
cover: "/posts/books/the-lost-symbol-robert-langdon-book-3.jpg"
description: "Finished “The Lost Symbol (Robert Langdon Book 3)” by Dan Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2009
book authors:
  - Dan Brown
book series:
  - Robert Langdon Series
book genres:
  - Fiction
  - Thriller
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Dan Brown
  - Robert Langdon Series
  - Fiction
  - Thriller
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “The Lost Symbol”.
* Author: [Dan Brown](/book-authors/dan-brown)
* First Published: [2009](/book-publication-year/2009)
* Series: [Robert Langdon](/book-series/robert-langdon-series) Book 3
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6880946-the-lost-symbol)
>
> The Lost Symbol, the stunning follow-up to The Da Vinci Code, is a masterstroke of storytelling - a deadly race through a real-world labyrinth of codes and unseen truths...all under the watchful eye of a terrifying villain. Set within the unseen tunnels and temples of Washington, D.C., The Lost Symbol accelerates through a startling landscape toward an unthinkable finale.
>
> Harvard symbologist Robert Langdon is summoned to deliver an evening lecture in the U.S. Capitol. Within minutes of his arrival, the night takes a bizarre turn. A disturbing object - artfully encoded with five ancient symbols - is discovered in the Capitol Building. The object is an ancient invitation, meant to usher its recipient into a long-lost world of hidden esoteric wisdom. And when Langdon's mentor Peter Solomon - prominent Mason and philanthropist - is kidnapped, Langdon's only hope of saving Peter is to accept this invitation and follow wherever it leads him. Langdon finds himself plunged into a clandestine world of Masonic secrets, hidden history, and never-before-seen locations...all of which seem to be dragging him toward a single, inconceivable truth.
>
> The Lost Symbol is exactly what Dan Brown's fans have been waiting for...his most thrilling novel yet.