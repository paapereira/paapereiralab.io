---
title: "Murder by Other Means (The Dispatcher Book 2) by John Scalzi (2020)"
slug: "murder-by-other-means-the-dispatcher-book-2"
author: "Paulo Pereira"
date: 2021-10-11T21:00:00+01:00
lastmod: 2021-10-11T21:00:00+01:00
cover: "/posts/books/murder-by-other-means-the-dispatcher-book-2.jpg"
description: "Finished “Murder by Other Means (The Dispatcher Book 2)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - John Scalzi
book series:
  - The Dispatcher Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Dispatcher Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Murder by Other Means”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [September 10th 2020](/book-publication-year/2020)
* Series: [The Dispatcher](/book-series/the-dispatcher-series) Book 2
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/55291869-murder-by-other-means)
>
> In the world of the Dispatchers, a natural or accidental death is an endpoint; a murder pushes the do-over button and 99.99% of the time the victim comes back to life. Tony Valdez is a Dispatcher who’s been taking shadier and shadier gigs in financial tough times, and after witnessing a crime gone wrong, he finds people around him permanently dying in a way that implicates him. He has to solve the mystery of these deaths to save the lives of others–and keep himself out of trouble with the law.