---
title: "Dead Moon (Threshold Book 3) by Peter Clines (2019)"
slug: "dead-moon-threshold-book-3"
author: "Paulo Pereira"
date: 2020-08-09T18:08:00+01:00
lastmod: 2020-08-09T18:08:00+01:00
cover: "/posts/books/dead-moon-threshold-book-3.png"
description: "Finished “Dead Moon (Threshold Book 3)” by Peter Clines."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Peter Clines
book series:
  - Threshold Series
book genres:
  - Fiction
  - Science Fiction
  - Horror
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Peter Clines
  - Threshold Series
  - Fiction
  - Science Fiction
  - Horror
  - 6/10 Books
  - Audiobook
---

Finished “Dead Moon”.
* Author: [Peter Clines](/book-authors/peter-clines)
* First Published: [February 14th 2019](/book-publication-year/2019)
* Series: [Threshold](/book-series/threshold-series) Book 3
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43542011-dead-moon)
>
> In the year 2243, the Moon belongs to the dead.
>
> The largest graveyard in the solar system, it was the perfect solution to the overcrowding and environmental problems that had plagued mankind for centuries. And the perfect place for Cali Washington to run away from her past.
>
> But when a mysterious meteor crashes into one of the Moon’s cemeteries, Cali and her fellow Caretakers find themselves surrounded by a terrifying enemy force that outnumbers them more than a thousand to one. An enemy not hindered by the lack of air or warmth or sustenance.
>
> An enemy that is already dead.
>
> Now Cali and her compatriots must fight to survive. Because if they don’t, everyone on the Moon may be joining the dead.
>
> And maybe everyone on Earth, too.