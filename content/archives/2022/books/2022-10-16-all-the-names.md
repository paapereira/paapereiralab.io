---
title: "All the Names by José Saramago (1997)"
slug: "all-the-names"
author: "Paulo Pereira"
date: 2022-10-16T11:00:00+01:00
lastmod: 2022-10-16T11:00:00+01:00
cover: "/posts/books/all-the-names.jpg"
description: "Finished “All the Names” by José Saramago."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - José Saramago
book series:
  - 
book genres:
  - Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - José Saramago
  - Fiction
  - 7/10 Books
  - Audiobook
---

Finished “All the Names”.
* Author: [José Saramago](/book-authors/josé-saramago)
* First Published: [1997](/book-publication-year/1990s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/2528.All_the_Names)
>
> Senhor José is a low-grade clerk in the city's Central Registry, where the living and the dead share the same shelf space. A middle-aged bachelor, he has no interest in anything beyond the certificates of birth, marriage, divorce, and death, that are his daily routine. But one day, when he comes across the records of an anonymous young woman, something happens to him. Obsessed, Senhor José sets off to follow the thread that may lead him to the woman-but as he gets closer, he discovers more about her, and about himself, than he would ever have wished.
> 
> The loneliness of people's lives, the effects of chance, the discovery of love-all coalesce in this extraordinary novel that displays the power and art of José Saramago in brilliant form.