---
title: "Books Schedule update"
slug: "book-schedule-april-2023"
author: "Paulo Pereira"
date: 2023-04-02T08:30:00+01:00
lastmod: 2023-04-02T08:30:00+01:00
description: "April Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my April TBR (To Be Read) update for the next months.

* **[Read]** Prey by Michael Crichton
* **[Reading]** Absolution Gap (Revelation Space 3) by Alastair Reynolds
* Life of Pi by Yann Martel
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* Elder Race by Adrian Tchaikovsky
* Baal by Robert R. McCammon
* Blood Rites (The Dresden Files 6) by Jim Butcher
* The Tooth Fairy by Graham Joyce
* Chasm City by Alastair Reynolds

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson

I also have always an audiobook going too:

* **[Read]** Gerald's Game by Stephen King
* **[Read]** Until the End of Time by Brian Greene
* **[Reading]** Star Runner by B. V. Larson
* Dolores Claiborne by Stephen King
* Dragons of Eden: Speculations on the Evolution of Human Intelligence by Carl Sagan
* State of Fear by Michael Crichton
* Fire Fight (Star Runner 2) by B. V. Larson
* Insomnia by Stephen King
* Digital Minimalism by Cal Newport