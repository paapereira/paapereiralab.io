---
title: "Steve Jobs by Walter Isaacson (2011)"
slug: "steve-jobs"
author: "Paulo Pereira"
date: 2017-04-02T22:48:00+01:00
lastmod: 2017-04-02T22:48:00+01:00
cover: "/posts/books/steve-jobs.jpg"
description: "Finished “Steve Jobs” by Walter Isaacson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Walter Isaacson
book series:
  - 
book genres:
  - Nonfiction
  - Biography
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Walter Isaacson
  - Nonfiction
  - Biography
  - 7/10 Books
  - Audiobook
---

Finished “Steve Jobs”.
* Author: [Walter Isaacson](/book-authors/walter-isaacson)
* First Published: [October 1st 2011](/book-publication-year/2011)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/24856343-steve-jobs)
>
> From the author of the best-selling biographies of Benjamin Franklin and Albert Einstein, this is the exclusive biography of Steve Jobs.
>
> Based on more than 40 interviews with Jobs conducted over two years - as well as interviews with more than a hundred family members, friends, adversaries, competitors, and colleagues - Walter Isaacson has written a riveting story of the roller-coaster life and searingly intense personality of a creative entrepreneur whose passion for perfection and ferocious drive revolutionized six industries: personal computers, animated movies, music, phones, tablet computing, and digital publishing.
>
> At a time when America is seeking ways to sustain its innovative edge, and when societies around the world are trying to build digital-age economies, Jobs stands as the ultimate icon of inventiveness and applied imagination. He knew that the best way to create value in the 21st century was to connect creativity with technology. He built a company where leaps of the imagination were combined with remarkable feats of engineering.
>
> Although Jobs cooperated with this book, he asked for no control over what was written. He put nothing off-limits. He encouraged the people he knew to speak honestly. And Jobs speaks candidly, sometimes brutally so, about the people he worked with and competed against. His friends, foes, and colleagues provide an unvarnished view of the passions, perfectionism, obsessions, artistry, devilry, and compulsion for control that shaped his approach to business and the innovative products that resulted.
>
> Driven by demons, Jobs could drive those around him to fury and despair. But his personality and products were interrelated, just as Apple's hardware and software tended to be, as if part of an integrated system. His tale is instructive and cautionary, filled with lessons about innovation, character, leadership, and values.