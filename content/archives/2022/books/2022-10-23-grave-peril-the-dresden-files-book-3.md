---
title: "Grave Peril (The Dresden Files Book 3) by Jim Butcher (2001)"
slug: "grave-peril-the-dresden-files-book-3"
author: "Paulo Pereira"
date: 2022-10-23T12:00:00+01:00
lastmod: 2022-10-23T12:00:00+01:00
cover: "/posts/books/grave-peril-the-dresden-files-book-3.jpg"
description: "Finished “Grave Peril (The Dresden Files Book 3)” by Jim Butcher."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2001
book authors:
  - Jim Butcher
book series:
  - The Dresden Files Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jim Butcher
  - The Dresden Files Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Grave Peril”.
* Author: [Jim Butcher](/book-authors/jim-butcher)
* First Published: [September 1, 2001](/book-publication-year/2001)
* Series: [The Dresden Files](/book-series/the-dresden-files-series) Book 3
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/7021986-grave-peril)
>
> In all his years of supernatural sleuthing, Harry Dresden has never faced anything like this: the spirit world's gone postal. These ghosts are tormented, violent, and deadly. Someone-or something-is purposely stirring them up to wreak unearthly havoc. But why? If Harry doesn't figure it out soon, he could wind up a ghost himself.