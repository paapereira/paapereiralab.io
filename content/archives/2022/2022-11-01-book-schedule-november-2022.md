---
title: "Books Schedule update"
slug: "book-schedule-november-2022"
author: "Paulo Pereira"
date: 2022-11-01T09:35:00+00:00
lastmod: 2022-11-01T09:35:00+00:00
description: "November Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my November TBR (To Be Read) update for the next months.

* **[Read]** Welcome to Dead House (Goosebumps 1) by R. L. Stine
* **[Read]** Let the Right One In by John Ajvide Lindqvist
* **[Read]** Grave Peril (The Dresden Files 3) by Jim Butcher
* **[Reading]** Revelation Space (Revelation Space 1) by Alastair Reynolds
* The Testaments (The Handmaid's Tale 2) by Margaret Atwood
* The First Fifteen Lives of Harry August by Claire North
* Timeline by Michael Crichton
* Summer Knight (The Dresden Files 4) by Jim Butcher
* Boy's Life by Robert R. McCammon
* Redemption Ark (Revelation Space 2) by Alastair Reynolds
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson

I also have always an audiobook going too:

* **[Read]** Hive War (Galactic Liberation 4) by David Vandyke & B. V. Larson
* **[Read]** All the Names by José Saramago
* **[Read]** Born a Crime: Stories From a South African Childhood by Trevor Noah
* **[Read]** The One Thing by Gary Keller & Jay Papasan
* **[Reading]** Needful Things by Stephen King
* Straker's Breakers (Galactic Liberation 5) by David Vandyke & B. V. Larson
* Death With Interruptions by José Saramago
* The Power of Now by Eckhart Tolle
* Hell's Reach (Galactic Liberation 6) by David Vandyke & B. V. Larson