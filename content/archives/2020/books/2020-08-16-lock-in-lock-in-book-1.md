---
title: "Lock In (Lock In Book 1) by John Scalzi (2014)"
slug: "lock-in-lock-in-book-1"
author: "Paulo Pereira"
date: 2020-08-16T21:01:00+01:00
lastmod: 2020-08-16T21:01:00+01:00
cover: "/posts/books/lock-in-lock-in-book-1.png"
description: "Finished “Lock In (Lock In Book 1)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - John Scalzi
book series:
  - Lock In Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - John Scalzi
  - Lock In Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Lock In”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [August 26th 2014](/book-publication-year/2014)
* Series: [Lock In](/book-series/lock-in-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/22751752-lock-in)
>
> A blazingly inventive near-future thriller from the best-selling, Hugo Award-winning John Scalzi.
>
> Not too long from today, a new, highly contagious virus makes its way across the globe. Most who get sick experience nothing worse than flu, fever, and headaches. But for the unlucky one percent - and nearly five million souls in the United States alone - the disease causes "Lock In": Victims fully awake and aware, but unable to move or respond to stimulus. The disease affects young, old, rich, poor, people of every color and creed. The world changes to meet the challenge.
>
> A quarter of a century later, in a world shaped by what’s now known as "Haden’s syndrome", rookie FBI agent Chris Shane is paired with veteran agent Leslie Vann. The two of them are assigned what appears to be a Haden-related murder at the Watergate Hotel, with a suspect who is an "integrator" - someone who can let the locked in borrow their bodies for a time. If the Integrator was carrying a Haden client, then naming the suspect for the murder becomes that much more complicated.
>
> But "complicated" doesn’t begin to describe it. As Shane and Vann began to unravel the threads of the murder, it becomes clear that the real mystery - and the real crime - is bigger than anyone could have imagined.