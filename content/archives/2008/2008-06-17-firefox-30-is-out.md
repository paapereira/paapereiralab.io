---
title: "Firefox 3.0 is out"
slug: "firefox-30-is-out"
author: "Paulo Pereira"
date: 2008-06-17T22:17:00+01:00
lastmod: 2008-06-17T22:17:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Firefox
---

Firefox 3.0 is out !!! :)

[Download now](http://www.mozilla.com/en-US/firefox/?p=downloadday).