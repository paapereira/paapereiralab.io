---
title: "The Talisman by Stephen King and Peter Straub (1984)"
slug: "the-talisman"
author: "Paulo Pereira"
date: 2019-08-01T22:36:00+01:00
lastmod: 2019-08-01T22:36:00+01:00
cover: "/posts/books/the-talisman.jpg"
description: "Finished “The Talisman” by Stephen King and Peter Straub."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
  - Peter Straub
book series:
  - The Talisman Series
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Stephen King
  - Peter Straub
  - The Talisman Series
  - Stephen King Novels
  - Fiction
  - Horror
  - Fantasy
  - 9/10 Books
  - Audiobook
---

Finished “The Talisman”.
* Author: [Stephen King](/book-authors/stephen-king) and [Peter Straub](/book-authors/peter-straub)
* First Published: [1984](/book-publication-year/1980s)
* Series: [The Talisman](/book-series/the-talisman-series) Book 1 and a [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/20754433-the-talisman)
>
> On a brisk autumn day, a twelve-year-old boy stands on the shores of the gray Atlantic, near a silent amusement park and a fading ocean resort called the Alhambra. The past has driven Jack Sawyer here: his father is gone, his mother is dying, and the world no longer makes sense. But for Jack everything is about to change. For he has been chosen to make a journey back across America--and into another realm.
>
> One of the most influential and heralded works of fantasy ever written, The Talisman is an extraordinary novel of loyalty, awakening, terror, and mystery. Jack Sawyer, on a desperate quest to save his mother's life, must search for a prize across an epic landscape of innocents and monsters, of incredible dangers and even more incredible truths. The prize is essential, but the journey means even more. Let the quest begin. . . .