---
title: "Until the End of Time by Brian Greene (2020)"
slug: "until-the-end-of-time"
author: "Paulo Pereira"
date: 2023-03-26T14:30:00+01:00
lastmod: 2023-03-26T14:30:00+01:00
cover: "/posts/books/until-the-end-of-time.jpg"
description: "Finished “Until the End of Time” by Brian Greene."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Brian Greene
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Brian Greene
  - Nonfiction
  - Science
  - 7/10 Books
  - Audiobook
---

Finished “Until the End of Time: Mind, Matter, and Our Search for Meaning in an Evolving Universe”.
* Author: [Brian Greene](/book-authors/brian-greene)
* First Published: [February 18, 2020](/book-publication-year/2020)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/49119222-until-the-end-of-time)
>
> Brian Greene takes readers on a breathtaking journey from the big bang to the end of time and invites us to ponder meaning in the face of this unimaginable expanse. He shows us how, despite the universe's tendency toward entropy, remarkable structures form: planets, stars, and galaxies provide islands in a sea of disorder; biochemical mechanisms animate life; neurons, information, and thought give rise to complex consciousness, which in turn creates cultures and their timeless myths and creativity. Through a series of nested stories Greene provides us with a clearer sense of how we came to be, a finer picture of where we are now, and a firmer understanding of where we are headed. Science has proven to be modern society's greatest source for meaning, and Brian Greene takes us on a grand tour of the universe while always keeping his eye on one central question: What does it all mean?