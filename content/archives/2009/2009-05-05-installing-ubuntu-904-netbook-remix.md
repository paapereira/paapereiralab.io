---
title: "Installing Ubuntu 9.04 Netbook Remix (Asus Eee 1000HE)"
slug: "installing-ubuntu-904-netbook-remix"
author: "Paulo Pereira"
date: 2009-05-05T20:24:00+01:00
lastmod: 2009-05-05T20:24:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 9.04
  - Jaunty Jackalope
  - Netbook Remix
  - Asus Eee 1000HE
---

I have a new [Asus Eee 1000HE](/posts/new-asus-eee-1000he/). It came with Windows XP, but I want to try out [Ubuntu Netbook Remix](http://www.ubuntu.com/getubuntu/download-netbook).

The eee don't have an optical drive, so we need to install it using an usb drive.

The goal is dual booting Windows XP and Ubuntu. I want Eee to become my  main Windows machine; this will allow me to format my Desktop machine to have only have Ubuntu (next release with default ext4 filesystem). I  use Windows only to sync my iPod Touch so, this is a great solution.

## Download and "Burn" a USB Drive

- Go to Ubuntu Netbook Remix [Download Page](http://www.ubuntu.com/getubuntu/download-netbook) and download Ubuntu. 

- - Check the downloaded file hash. The output must be the same as you see in the [Ubuntu Hashes Page](https://help.ubuntu.com/community/UbuntuHashes).

```bash
md5sum ubuntu-9.04-netbook-remix-i386.img
```

- [Create a bootable usb drive from the downloaded image](https://help.ubuntu.com/community/Installation/FromImgFiles). I tried the Linux version using ImageWriter but it didn't work. I ended up using the Disk Imager in the pre installed Windows in Eee. Note that this will erase the usb drive contents.

## Install from usb

It's easy. Boot from the usb drive (check the BIOS) and install like any other Ubuntu release.

This is a [tested machine](https://wiki.ubuntu.com/HardwareSupport/Machines/Netbooks) so I didn't play that much with the Live Session.

## Hard Drive Partition Layout

I kept Windows installed and I will need a NTFS shared partition to keep all my tunes.

There was a hidden partition, basically used to restore the system. Basically is a pre-installation of Windows. I didn't kept it.

I don't have the hard drive all partitioned yet. Will do soon.

```text
/dev/sda
  /dev/sda1    ntfs    WinXP     20 GB    -- primary
  /dev/sda2    ext3   /        9.54 GB    -- primary
  /dev/sda3   swap          1.91 GB    -- primary
  /dev/sda4                            -- extended
  /dev/sda5   ext3     /home      9.54 GB
  /dev/sda6    ntfs    tunes      40 GB
  /dev/sda7   ext3   /storage   9.54 GB
```

I created the sda7 partition after installing Ubuntu, so I needed to do the following to mount the new partition at boot time:

- Find out the UUID (Unique unit ID) of the device

```bash
sudo vol_id -u /dev/sda7
```

- Create a folder for your new mount point

```bash
sudo mkdir /storage
```

- Open /etc/fstab

```bash
sudo gedit /etc/fstab
```

- Add the mount point

```text
UUID=ad02cb4e-f9cf-491a-b661-d96ed2c456be /storage        ext3    relatime        0       2
```

- Reboot

- - Add permissions to the new folder

```bash
sudo chown -R *youruser*:*youruser* /storage
sudo chmod -R 755 /storage
```

## Post-install: Wireless

The wireless wasn't working. I could connect, but no Internet connection.

Here's what I did, after a [quick look](http://www.jpierre.com/2009/04/installing-ubuntu-netbook-remix-904-jaunty-jackelope-on-eee-pc-the-howto-guide/) around:

- Download [this driver](http://www.array.org/ubuntu/dists/intrepid/eeepc/binary-i386/rt2860-dkms_1.7.1.1_all.deb)

- Rename the actual drivers

```bash
cd /lib/modules/2.6.28-11-generic/kernel/drivers/staging/rt2860/
sudo mv rt2860sta.ko rt2860sta.bak
cd /lib/modules/2.6.28-11-generic/kernel/drivers/staging/rt2870/
sudo mv rt2870sta.ko rt2870sta.bak
```

- Install the package dkms and the new driver

```bash
sudo apt-get install dkms
chmod +x rt2860-dkms_1.7.1.1_all.deb
./rt2860-dkms_1.7.1.1_all.deb
```

- I also installed linux-backports-modules-jaunty

```bash
sudo apt-get install linux-backports-modules-jaunty
```

- Reboot

- I have wireless :-)

## Post-Install: Software Sources

Added the following:

```text
deb http://ppa.launchpad.net/do-core/ppa/ubuntu jaunty main
deb http://packages.medibuntu.org/ jaunty free non-free
deb http://linux.getdropbox.com/ubuntu jaunty main
deb http://ppa.launchpad.net/openoffice-pkgs/ubuntu/ jaunty main
```

## Post-install: Extra Software

- Dropbox (`sudo apt-get install nautilus-dropbox`)
- Gnome-Do (`sudo apt-get install gnome-do`)
- [Eee-Control](http://greg.geekmind.org/eee-control/)
- Powertop (`sudo apt-get install powertop`)
- GParted (`sudo apt-get install gparted`)
- [Firefox Xmarks add-on](http://www.xmarks.com/)
- [Multimedia](http://ubuntuforums.org/showthread.php?t=766683) (including flash)
- [Adobe Air](http://get.adobe.com/air/)
- [TweetDeck](http://www.tweetdeck.com/beta/)
- [QuickSynergy](/posts/synergy/)
- Gnucash (`sudo apt-get install gnucash`)
- Aspell pt-pt (`sudo apt-get install aspell-pt-pt`)
- [NTFS support to GParted](/posts/formating-a-partition-in-ntfs/)

If you have more tips about using or optimizing the eee please contact me or post a comment :-)