---
title: "rsync.net mysqlbackup.sh"
slug: "rsyncnet-mysqlbackupsh"
author: "Paulo Pereira"
date: 2012-08-16T23:39:00+01:00
lastmod: 2012-08-16T23:39:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - duplicity
  - GitHub
  - rsync.net
---

Following up on my [post](/posts/backing-up-mysql-in-b3-server/) about backing up mysql using [rsync.net](http://rsync.net/), I created a [github](https://github.com/paapereira/rsync.net-mysqlbackup) repository to keep and improve the script.

It uses mysqlhotcopy and duplicity to backup mysql databases to rsync.net.

Stay tuned for the first milestone release.

![rsync.net-mysqlbackup](/posts/2012/2012-08-16-rsyncnet-mysqlbackupsh/github.png)

