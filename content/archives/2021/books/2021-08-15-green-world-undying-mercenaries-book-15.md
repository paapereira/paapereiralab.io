---
title: "Green World (Undying Mercenaries Book 15) by B.V. Larson (2021)"
slug: "green-world-undying-mercenaries-book-15"
author: "Paulo Pereira"
date: 2021-08-15T21:00:00+01:00
lastmod: 2021-08-15T21:00:00+01:00
cover: "/posts/books/green-world-undying-mercenaries-book-15.jpg"
description: "Finished “Green World (Undying Mercenaries Book 15)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Green World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [August 3rd 2021](/book-publication-year/2021)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 15
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/58754574-green-world)
>
> Rebels build a secret base on Green World. Their plan is to attack Earth and retake all the planets the Humans have conquered.
>
> Hegemony starships gather to strike the Rebels first, but where is their base? As the fleets search, Earth warships trespass into Skay space igniting a fresh border conflict between rival Galactics.
>
> When James McGill stumbles onto the rebel camp, they’re forced to step up their plans. The world goes up in flames. Friends are permed and cities are destroyed as everything spins out of control.
>
> Can Earth survive? Find out in GREEN WORLD, book #15 of the Undying Mercenaries series. With over three million copies sold, USA Today Bestselling author B. V. Larson is the king of modern military science fiction.