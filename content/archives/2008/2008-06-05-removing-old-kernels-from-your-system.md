---
title: "Removing old kernels from your system"
slug: "removing-old-kernels-from-your-system"
author: "Paulo Pereira"
date: 2008-06-05T16:20:00+01:00
lastmod: 2008-06-05T16:20:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
aliases:
  - /posts/removing-old-kernels-from-your-system/
---

After a while you're starting to collect kernel versions on your system.

Since Hardy came out, with kernel 2.6.24-16, we had two kernel updates, so, in grub we have now entries to kernels 2.6.24-16, 2.6.24-17 and 2.6.24-18.

We just need one to boot in, and we and the most recent one of course. Still, when deciding to remove an old kernel be aware of possible problems.

[Virtualbox](/posts/windows-under-linux/) for instance, needs a module to the specific kernel you're running. Every time a new kernel is released your Virtualbox setup will not work until your upgrade the said module.
So, if you need to use Virtualbox and you don't yet have the new module, you could just restart into the old kernel.

So, think before you remove old kernels.

## Removing old kernels

- Check what versions you have installed

```bash
sudo dpkg --list | grep linux-image
```

- Output example

```text
ii  linux-image-2.6.24-16-generic                  2.6.24-16.30                                       Linux kernel image for version 2.6.24 on  x86
ii  linux-image-2.6.24-17-generic                  2.6.24-17.31                                       Linux kernel image for version  2.6.24 on x86
ii  linux-image-2.6.24-18-generic                   2.6.24-18.32                                       Linux kernel image  for version 2.6.24 on x86
ii  linux-image-generic                            2.6.24.18.20                                       Generic  Linux kernel image
ii   virtualbox-ose-guest-modules-2.6.24-18-generic 24.0.3                                             virtualbox-ose-guest module for linux-image-
ii  virtualbox-ose-modules-2.6.24-18-generic       24.0.3                                             virtualbox-ose module for  linux-image-2.6.24
```

- In this example, only the first 3 rows are candidates to go. Make sure you don't remove the kernel you're using.

```bash
uname -r
```

- To remove, for instance 2.6.24-16

```bash
sudo apt-get remove linux-image-2.6.24-16-generic
```

- Update your grub list

```bash
sudo update-grub
```

And that is that.