---
title: "Storm Front (The Dresden Files Book 1) by Jim Butcher (2000)"
slug: "storm-front-the-dresden-files-book-1"
author: "Paulo Pereira"
date: 2022-08-01T21:00:00+01:00
lastmod: 2022-08-01T21:00:00+01:00
cover: "/posts/books/storm-front-the-dresden-files-book-1.jpg"
description: "Finished “Storm Front (The Dresden Files Book 1)” by Jim Butcher."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2000
book authors:
  - Jim Butcher
book series:
  - The Dresden Files Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jim Butcher
  - The Dresden Files Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Storm Front”.
* Author: [Jim Butcher](/book-authors/jim-butcher)
* First Published: [April 1st 2000](/book-publication-year/2000)
* Series: [The Dresden Files](/book-series/the-dresden-files-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6219313-storm-front)
>
> Harry Dresden is the best at what he does. Well, technically, he's the only at what he does. So when the Chicago P.D. has a case that transcends mortal creativity or capability, they come to him for answers. For the "everyday" world is actually full of strange and magical things—and most don't play well with humans. That's where Harry comes in. Takes a wizard to catch a—well, whatever. There's just one problem. Business, to put it mildly, stinks.
> 
> So when the police bring him in to consult on a grisly double murder committed with black magic, Harry's seeing dollar signs. But where there's black magic, there's a black mage behind it. And now that mage knows Harry's name. And that's when things start to get interesting.
> 
> Magic - it can get a guy killed.