---
title: "Androids and Aliens (Star Runner Book 3) by B.V. Larson (2022)"
slug: "androids-and-aliens-star-runner-book-3"
author: "Paulo Pereira"
date: 2023-09-03T10:00:00+01:00
lastmod: 2023-09-03T10:00:00+01:00
cover: "/posts/books/androids-and-aliens-star-runner-book-3.jpg"
description: "Finished “Androids and Aliens (Star Runner Book 3)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2022
book authors:
  - B.V. Larson
book series:
  - Star Runner Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Runner Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Androids and Aliens”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [October 4, 2022](/book-publication-year/2022)
* Series: [Star Runner](/book-series/star-runner-series) Book 3
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/75351153-androids-and-aliens)
>
> It’s time for the humans to strike back! Vile, ant-like aliens have already consumed the colonies of the Faustian Chain. Known as the Skaintz, these creatures destroy all living things and carry them back to their nests to be devoured. Now, they’ve begun to invade the stars of the Conclave.
> 
> Captain Bill Gorman, a smuggler from the Fringe, is called upon by the Conclave rulers to scout the enemy. Fleets and armies are launched in a desperate attempt to annihilate the Skaintz before they consume Humanity.