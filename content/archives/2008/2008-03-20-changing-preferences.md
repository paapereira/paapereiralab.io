---
title: "Changing preferences"
slug: "changing-preferences"
author: "Paulo Pereira"
date: 2008-03-20T16:41:00+00:00
lastmod: 2008-03-20T16:41:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

So far I've changed:

System > Preferences >

- Appearance: I don't really like the default Ultimate Edition theme, so I chose the Human theme and changed the Background
- Keyboard: I had the EuroSign to the E key
- Keyboard Shortcuts: Now the "Windows key" shows the panel menu (it's the Start Menu in Windows) and the "Menu key" activates the window menu (right mouse button)
- Mouse: I never liked the "Tap to Click" option
- Power Management: Changed some timeout options and had the Notification icon always on
- Screensaver
- Sessions: I disable the Bluetooth Manager
- Sound: I like it quiet