---
title: "The Snow Queen by Hans Christian Andersen (1844)"
slug: "the-snow-queen"
author: "Paulo Pereira"
date: 2015-12-11T23:42:00+00:00
lastmod: 2015-12-11T23:42:00+00:00
cover: "/posts/books/the-snow-queen.jpg"
description: "Finished “The Snow Queen” by Hans Christian Andersen."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - Hans Christian Andersen
book series:
  - 
book genres:
  - Fantasy
  - Classics
book scores:
  - 3/10 Books
tags:
  - Book Log
  - Hans Christian Andersen
  - Fantasy
  - Classics
  - 3/10 Books
  - Audiobook
---

Finished “The Snow Queen”.
* Author: [Hans Christian Andersen](/book-authors/hans-christian-andersen)
* First Published: [1844](/book-publication-year/1800s)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/23811731-the-snow-queen)
>
> Audible's 2014 Narrator of the Year Julia Whelan performs one of Hans Christian Andersen's most beloved fairy tales, The Snow Queen. This classic tale is a fantastical fable of two dear friends - one of whom goes astray and is literally lost to the north woods, while the other undertakes an epic journey to rescue him. This charming, strange, and wonderful story is a timeless allegory about growing up and the challenges of staying true to one's self, and it served as the wintry inspiration for the blockbuster hit Frozen.