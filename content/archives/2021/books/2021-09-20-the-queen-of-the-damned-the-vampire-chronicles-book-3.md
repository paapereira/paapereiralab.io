---
title: "The Queen of the Damned (The Vampire Chronicles Book 3) by Anne Rice (1988)"
slug: "the-queen-of-the-damned-the-vampire-chronicles-book-3"
author: "Paulo Pereira"
date: 2021-09-20T21:00:00+01:00
lastmod: 2021-09-20T21:00:00+01:00
cover: "/posts/books/the-queen-of-the-damned-the-vampire-chronicles-book-3.jpg"
description: "Finished “The Queen of the Damned (The Vampire Chronicles Book 3)” by Anne Rice."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Anne Rice
book series:
  - The Vampire Chronicles Series
book genres:
  - Fiction
  - Horror
  - Fantasy
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Anne Rice
  - The Vampire Chronicles Series
  - Fiction
  - Horror
  - Fantasy
  - 6/10 Books
  - Audiobook
---

Finished “The Queen of the Damned”.
* Author: [Anne Rice](/book-authors/anne-rice)
* First Published: [1988](/book-publication-year/1980s)
* Series: [The Vampire Chronicles](/book-series/the-vampire-chronicles-series) Book 3
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34861488-the-queen-of-the-damned)
>
> In 1976, a uniquely seductive world of vampires was unveiled in the now-classic Interview with the Vampire . . . in 1985, a wild and voluptous voice spoke to us, telling the story of The Vampire Lestat. In The Queen of the Damned, Anne Rice continues her extraordinary "Vampire Chronicles" in a feat of mesmeric storytelling, a chillingly hypnotic entertainment in which the oldest and most powerful forces of the night are unleashed on an unsuspecting world.