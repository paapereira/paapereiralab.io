---
title: "Mystery Walk by Robert R. McCammon (1983)"
slug: "mystery-walk"
author: "Paulo Pereira"
date: 2024-12-23T22:00:00+00:00
lastmod: 2024-12-23T22:00:00+00:00
cover: "/posts/books/mystery-walk.jpg"
description: "Finished “Mystery Walk” by Robert R. McCammon."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Robert R. McCammon
book series:
  - 
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Robert R. McCammon
  - Fiction
  - Horror
  - 7/10 Books
  - Ebook
---

Finished “Mystery Walk”.
* Author: [Robert R. McCammon](/book-authors/robert-r.-mccammon)
* First Published: [January 1, 1983](/book-publication-year/1980s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/39338942-mystery-walk)
>
> Born and raised in rural Alabama, Billy Creekmore was destined to be a psychic. His mother, a Choctaw Indian schooled in her tribe’s ancient mysticism, understands the permeable barrier between life and death—and can cross it. She taught the power to Billy and now he helps the dead rest in peace.
> Wayne Falconer, son of one of the most fervent tent evangelists in the South, travels the country serving his father’s healing ministry. Using his unique powers to cure the flock, Little Wayne is on his way to becoming one of the popular and successful miracle workers in the country. He helps the living survive.
> Billy and Wayne share more than a gift. They share a dream—and a common enemy. They are on separate journeys, mystery walks that will lead them toward a crossroad where the evil of their dreams has taken shape. One of them will reject the dark. The other will be consumed by it. But neither imagined just how monstrous and far-reaching the dark was, or that mankind’s fate would rest in their hands during an epic showdown of good versus evil.