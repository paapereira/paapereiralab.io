---
title: "A Coming of Age by Timothy Zahn (1984)"
slug: "a-coming-of-age"
author: "Paulo Pereira"
date: 2025-02-08T18:00:00+00:00
lastmod: 2025-02-08T18:00:00+00:00
cover: "/posts/books/a-coming-of-age.jpg"
description: "Finished “A Coming of Age” by Timothy Zahn."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Timothy Zahn
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Timothy Zahn
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “A Coming of Age”.
* Author: [Timothy Zahn](/book-authors/timothy-zahn)
* First Published: [January 1, 1984](/book-publication-year/1980s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/19100189-a-coming-of-age)
>
> The children of Tigris have extraordinary telekinetic gifts—but are these special powers a blessing or a curse?On Tigris, children develop telekinesis beginning at the age of five. By the time they’re pre-teens, though, their special abilities peak, then slip away as they reach maturity. Being able to “teek” gives them power—even over most adults—until they gradually become regular teenagers, no longer special, no longer with authority and status. Some handle the Transition better than others.  Lisa Duncan always thought she’d mature gracefully, but at age fourteen, and close to losing her abilities, she’s confused and uncertain about what the future will bring. That is, until she gets drawn into the experimental plan of Dr. Matthew Jarvis, whose scientific discovery may alter Tigrin society forever. . . .