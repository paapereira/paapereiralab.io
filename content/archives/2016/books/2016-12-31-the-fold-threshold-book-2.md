---
title: "The Fold (Threshold Book 2) by Peter Clines (2015)"
slug: "the-fold-threshold-book-2"
author: "Paulo Pereira"
date: 2016-12-31T23:15:00+00:00
lastmod: 2016-12-31T23:15:00+00:00
cover: "/posts/books/the-fold-threshold-book-2.jpg"
description: "Finished “The Fold (Threshold Book 2)” by Peter Clines."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - Peter Clines
book series:
  - Threshold Series
book genres:
  - Fiction
  - Science Fiction
  - Horror
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Peter Clines
  - Threshold Series
  - Fiction
  - Science Fiction
  - Horror
  - 6/10 Books
  - Audiobook
---

Finished “The Fold”.
* Author: [Peter Clines](/book-authors/peter-clines)
* First Published: [June 2nd 2015](/book-publication-year/2015)
* Series: [Threshold](/book-series/threshold-series) Book 2
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25670118-the-fold)
>
> Step into the fold. It's perfectly safe.
>
> The folks in Mike Erikson's small New England town would say he's just your average, everyday guy. And that's exactly how Mike likes it. Sure, the life he's chosen isn't much of a challenge to someone with his unique gifts, but he's content with his quiet and peaceful existence. That is until an old friend presents him with an irresistible mystery, one that Mike is uniquely qualified to solve.
>
> Far out in the California desert, a team of DARPA scientists has invented a device they affectionately call the Albuquerque Door. Using a cryptic computer equation and magnetic fields to "fold" dimensions, it shrinks distances so a traveler can travel hundreds of feet with a single step. The invention promises to make mankind's dreams of teleportation a reality. And, the scientists insist, traveling through the door is completely safe. Yet evidence is mounting that this miraculous machine isn't quite what it seems - and that its creators are harboring a dangerous secret.
>
> As his investigations draw him deeper into the puzzle, Mike begins to fear there's only one answer that makes sense. And if he's right, it may be only a matter of time before the project destroys...everything. A cunningly inventive mystery featuring a hero worthy of Sherlock Holmes and a terrifying final twist you'll never see coming, The Fold is that rarest of things: a genuine pause-resister science-fiction thriller. Step inside its audio and learn why author Peter Clines has already won legions of loyal fans.