---
title: "The Space Race by Colin Brake, Patrick Chapman, Richard Hollingham, Richard Kurti, Sue Nelson, Helen Quigley and Andrew Mark Sewell (2019)"
slug: "the-space-race"
author: "Paulo Pereira"
date: 2021-08-02T21:00:00+01:00
lastmod: 2021-08-02T21:00:00+01:00
cover: "/posts/books/the-space-race.jpg"
description: "Finished “The Space Race” by Colin Brake, Patrick Chapman, Richard Hollingham, Richard Kurti, Sue Nelson, Helen Quigley and Andrew Mark Sewell."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Colin Brake
  - Patrick Chapman
  - Richard Hollingham
  - Richard Kurti
  - Sue Nelson
  - Helen Quigley
  - Andrew Mark Sewell
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Colin Brake
  - Patrick Chapman
  - Richard Hollingham
  - Richard Kurti
  - Sue Nelson
  - Helen Quigley
  - Andrew Mark Sewell
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “The Space Race”.
* Author: [Colin Brake](/book-authors/colin-brake), [Patrick Chapman](/book-authors/patrick-chapman), [Richard Hollingham](/book-authors/richard-hollingham), [Richard Kurti](/book-authors/richard-kurti), [Sue Nelson](/book-authors/sue-nelson), [Helen Quigley](/book-authors/helen-quigley), [Andrew Mark Sewell](/book-authors/andrew-mark-sewell)
* First Published: [July 12th 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/54444004-the-space-race)
>
> The 1960s space race captured our imaginations and our dreams. Today’s efforts to revitalize and expand space travel is being driven not just by government agencies such as NASA, but also by visionaries such as Richard Branson (Virgin Galactic), Elon Musk (SpaceX), and Jeff Bezos (Blue Origin).
> 
> To celebrate the 50th anniversary of the 1969 moon landing, this major documentary-drama series brings to life the past, present, and future of man’s > exploration of space. Between 1969 and 1972, twelve Americans walked on the moon. You’ll get to experience the thrill of that era and much, much more.
> 
> Narrated by Kate Mulgrew (Emmy Award and Golden Globe nominee for Orange Is the New Black; Obie Award winner for Iphigenia 2.0); TV: Star Trek: > Voyager, film: Star Trek: Nemesis), The Space Race uses actual audio, original interviews, dramatic reconstructions, and first-hand accounts to tell > the story of mankind’s first amazing steps off our world and onto the lunar surface.
> 
> With unprecedented access, The Space Race takes listeners to Virgin Galactic’s space program in the Mojave Desert, features conversations with Buzz > Aldrin, Gene Cernan, Sergei Krikalev, Tim Peake, and numerous key players at mission control. This Audible Original and takes you behind the scenes to see how these exciting adventures in outer space came to be.