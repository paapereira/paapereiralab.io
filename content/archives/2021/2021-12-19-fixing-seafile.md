---
title: "Fixing Seafile"
slug: "2021-12-19-fixing-seafile"
author: "Paulo Pereira"
date: 2021-12-19T14:00:00+00:00
lastmod: 2021-12-19T14:00:00+00:00
description: "Similarly to the beginning of the year, I had a problem with Seafile. It was crashing and I couldn't start it with systemd.\n

Here's my notes/steps/fixes for the issue."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Seafile
aliases:
  - /posts/2021-12-19-fixing-seafile
---

Similarly to the [beginning](/posts/fixing-seafile) of the year, I had a problem with Seafile. It was crashing and I couldn't start it with systemd.

Here's my notes/steps/fixes for the issue.

I stopped the services:

```bash
sudo systemctl stop seahub
sudo systemctl stop seafile
```

Sudo into the seafile user:

```bash
sudo -u seafileuser -s /bin/sh
```

Disabled the daemon mode and manually started seafile to see the output errors:

```bash
vi $HOME/seafile/conf/gunicorn.conf.py
```

```text
(...)
daemon = False
(...)
```

```bash
seafile-server-latest/seafile.sh start
seafile-server-latest/seahub.sh start
```

I had errors loading some python libraries, so I updated them:

```bash
sudo pip3 install --timeout=3600 django-pylibmc django-simple-captcha
sudo pip3 install --timeout=3600 future mysqlclient
```

Enabled the daemon mode:

```bash
vi $HOME/seafile/conf/gunicorn.conf.py
```

```text
(...)
daemon = True
(...)
```

Started seafile with success:

```bash
sudo systemctl start seafile
sudo systemctl start seahub
```