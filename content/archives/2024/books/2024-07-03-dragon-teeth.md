---
title: "Dragon Teeth by Michael Crichton (2017)"
slug: "dragon-teeth"
author: "Paulo Pereira"
date: 2024-07-03T21:00:00+01:00
lastmod: 2024-07-03T21:00:00+01:00
cover: "/posts/books/dragon-teeth.jpg"
description: "Finished “Dragon Teeth” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Michael Crichton
book series:
  - 
book genres:
  - Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Fiction
  - 7/10 Books
  - Ebook
---

Finished “Dragon Teeth”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [May 22, 2017](/book-publication-year/2017)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34810395-dragon-teeth)
>
> The year is 1876. Warring Indian tribes still populate America’s western territories even as lawless gold-rush towns begin to mark the landscape. In much of the country it is still illegal to espouse evolution. Against this backdrop two monomaniacal paleontologists pillage the Wild West, hunting for dinosaur fossils, while surveilling, deceiving and sabotaging each other in a rivalry that will come to be known as the Bone Wars.
> 
> Into this treacherous territory plunges the arrogant and entitled William Johnson, a Yale student with more privilege than sense. Determined to survive a summer in the west to win a bet against his arch-rival, William has joined world-renowned paleontologist Othniel Charles Marsh on his latest expedition.  But when the paranoid and secretive Marsh becomes convinced that William is spying for his nemesis, Edwin Drinker Cope, he abandons him in Cheyenne, Wyoming, a locus of crime and vice. William is forced to join forces with Cope and soon stumbles upon a discovery of historic proportions.  With this extraordinary treasure, however, comes exceptional danger, and William’s newfound resilience will be tested in his struggle to protect his cache, which pits him against some of the West’s most notorious characters.
> 
> A page-turner that draws on both meticulously researched history and an exuberant imagination, Dragon Teeth is based on the rivalry between real-life paleontologists Cope and Marsh; in William Johnson readers will find an inspiring hero only Michael Crichton could have imagined. Perfectly paced and brilliantly plotted, this enormously winning adventure is destined to become another Crichton classic. 