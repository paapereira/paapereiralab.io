---
title: "Moving back to Blogger"
slug: "moving-back-to-blogger"
author: "Paulo Pereira"
date: 2011-04-24T22:58:00+01:00
lastmod: 2011-04-24T22:58:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
---

I'm moving back to Blogger after a long time without new posts.

Next time you point to [lofspot.net](http://lofspot.net/), this is the blog you'll see. My old blog is located at http://paapereira.squarespace.com/, but will be discontinued rapidly.

The goal for this blog is to document my incursions to Linux ([Ubuntu](http://www.ubuntu.com/)), Android and my new [B3 Server](http://www.excito.com/).

I have to manually import my blog from Squarespace :( and this will take some time.

I've decided to import only some posts, the ones I think are more interesting and that make sense.

## What have I been up to?

Since my last post (almost a year ago) my Ubuntu eXperience have been very light. My desktop environment is very stable and it's very easy to  maintain Ubuntu, so my post frequency have been very much reduced.

Meanwhile I've bought an Android device (HTC Desire) and have been playing with it. I will include some posts about it.

Recently I've bought a B3 Server (Debian based) and will also include posts about this.

See you in a bit...