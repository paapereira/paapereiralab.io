---
title: "Walk the Plank (The Human Division Book 2) by John Scalzi (2013)"
slug: "walk-the-plank-the-human-division-book-2"
author: "Paulo Pereira"
date: 2019-01-18T22:39:00+00:00
lastmod: 2019-01-18T22:39:00+00:00
cover: "/posts/books/walk-the-plank-the-human-division-book-2.jpg"
description: "Finished “Walk the Plank (The Human Division Book 2)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Walk the Plank”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [January 22nd 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 2
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17278806-walk-the-plank)
>
> Wildcat colonies are illegal, unauthorized and secret so when an injured stranger shows up at the wildcat colony New Seattle, the colony leaders are understandably suspicious of who he is and what he represents. His story of how he came to their colony is shocking, surprising, and might have bigger consequences than anyone could have expected.
>
> The second episode of The Human Division, John Scalzi's new thirteen-episode novel in the world of his bestselling Old Man's War.