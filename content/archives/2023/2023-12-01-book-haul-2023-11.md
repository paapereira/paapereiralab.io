---
title: "Book Haul for November 2023"
slug: "book-haul-2023-11"
author: "Paulo Pereira"
date: 2023-12-01T13:00:00+00:00
lastmod: 2023-12-01T13:00:00+00:00
cover: "/posts/book-hauls/2023-11-book-haul.png"
description: "November book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

November book haul.