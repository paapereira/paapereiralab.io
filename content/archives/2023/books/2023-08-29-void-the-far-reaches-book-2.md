---
title: "Void (The Far Reaches Book 2) by Veronica Roth (2023)"
slug: "void-the-far-reaches-book-2"
author: "Paulo Pereira"
date: 2023-08-29T21:00:00+01:00
lastmod: 2023-08-29T21:00:00+01:00
cover: "/posts/books/void-the-far-reaches-book-2.jpg"
description: "Finished “Void (The Far Reaches Book 2)” by Veronica Roth."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - Veronica Roth
book series:
  - The Far Reaches Series
book genres:
  - Fiction
  - Science Fiction
  - Short Stories
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Veronica Roth
  - The Far Reaches Series
  - Fiction
  - Science Fiction
  - Short Stories
  - 7/10 Books
  - Ebook
---

Finished “Void”.
* Author: [Veronica Roth](/book-authors/veronica-roth)
* First Published: [June 27, 2023](/book-publication-year/2023)
* Series: [The Far Reaches](/book-series/the-far-reaches-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/151909273-void)
>
> An intergalactic luxury cruise to a distant port is a world unto itself in this piercing short mystery by #1 New York Times bestselling author Veronica Roth. Traveling faster than light, the transport ship Redundancy is cut off from communication as effectively as an ancient ocean liner. The isolation suits crew member Ace Vance just fine—she’s got nowhere else to be. But when a wealthy passenger turns up dead during a routine voyage, Ace will have to connect with the passengers and crew to uncover the truth. Tragedy will strike again—it’s only a matter of time. Veronica Roth’s Void is part of The Far Reaches , a collection of science-fiction stories that stretch the imagination and open the heart. They can be read or listened to in one sitting.