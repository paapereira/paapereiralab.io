---
title: "Setting up Hugo with GitLab"
slug: "setting-hugo-with-gitlab"
author: "Paulo Pereira"
date: 2020-04-13T20:19:55+01:00
lastmod: 2020-04-13T20:19:55+01:00
description: "Setting up Hugo with GitLab."
draft: false
toc: true
categories:
  - General
tags:
  - Hugo
  - GitLab
---

## What will you need

* A [GitLab](https://gitlab.com/) account
* [Hugo](https://gohugo.io/)

## Creating a GitLab Page

It is as easy as clone an example GitLab Page Hugo example as the base for your own blog or homepage.

* Go to https://gitlab.com/pages/hugo and fork the project to your GitLab account

	![fork](/posts/2020/2020-04-13-setting-hugo-with-gitlab/fork.png)

* In your new repository, remove the fork relationship (Settings > General > Advanced > Remove fork relationship)

	![remove fork](/posts/2020/2020-04-13-setting-hugo-with-gitlab/remove-fork.png)

* Make sure your have the Shared Runners enabled (Settings > CI/CD > Runners)

	![shared runners](/posts/2020/2020-04-13-setting-hugo-with-gitlab/shared-runners.png)

* Trigger a build to check if everything is OK (Repositories > Files > README.md > Edit, change something in the file and Commit changes)

	![trigger build](/posts/2020/2020-04-13-setting-hugo-with-gitlab/edit-readme.png)

* Check if a job executed with success (CI / CD > Jobs)

	![build](/posts/2020/2020-04-13-setting-hugo-with-gitlab/job-ok.png)

* Check if the page is up and running using the link in Settings > Pages > Access pages

* Rename the project name (Settings > General > Naming, topics, avatar > Project name)

* Rename the repository (Settings > General > Advanced > Change path)

	![rename](/posts/2020/2020-04-13-setting-hugo-with-gitlab/change-path.png)

* Check if the page url changed and it's still working (Settings > Pages > Access pages)

## Installing Hugo

In my case I'm using Arch Linux and it was as easy as:

```bash
yay -S hugo
```

## Setup up your local environment

* Clone your page

```bash
mkdir ~/www/mypage
cd ~/www/mypage
git clone https://gitlab.com/youruser/youruser.gitlab.io
```

* Add the public directorie to your gitignore

```bash
echo "/public" >> .gitignore
```

* Choose a [theme](https://themes.gohugo.io/)

```bash
git submodule add https://github.com/rhazdon/hugo-theme-hello-friend-ng.git themes/hello-friend-ng
```

* Configure your site

```bash
vim config.toml
```
```toml
baseurl = "https://youruser.gitlab.io/"
title = "My homepage"

DefaultContentLanguage = "en"

theme = "hello-friend-ng"

PygmentsCodeFences = true
PygmentsStyle = "monokai"

copyright = '<a href="https://creativecommons.org/licenses/by-nc/4.0/" target="_blank" rel="noopener">CC BY-NC 4.0</a>'

enableRobotsTXT = false
enableGitInfo = false
enableEmoji     = true
enableMissingTranslationPlaceholders = false
disableRSS     = false
disableSitemap = false
disable404     = false
disableHugoGeneratorInject = false

[permalinks]
  posts = "/posts/:year/:month/:title/"

[author]
  name = "my name"
  avatar = "static/my_avatar.png"

[taxonomies]
  tag      = "tags"
  category = "categories"

[params]
  dateform        = "January 2, 2006"
  dateformShort   = "Jan 2"
  dateformNum     = "2006-01-02"
  dateformNumTime = "2006-01-02 15:04 +0000"

  lastModDisplay = "Modified:"
  disableReadOtherPosts = false

  description = "my description"
  keywords = "homepage, blog"
  images = [""]

  contentTypeName = "posts"

  defaultTheme = "dark"

  showReadingTime = false

  [params.logo]
    logoText     = "$ cd ~"
    logoHomeLink = "/"
    # logoCursorDisabled = true
    logoCursorColor    = "#5e81ac"
    logoCursorAnimate  = "1s"

  [[params.social]]
    name = "email"
    url  = "mailto:me@mymail.com"

  [[params.social]]
    name = "twitter"
    url  = "https://twitter.com/myuser"

[menu]
  [[menu.main]]
    identifier = "whoami"
    name       = "whoami"
    url        = "/page/about"
  [[menu.main]]
    identifier = "posts"
    name       = "posts"
    url        = "posts/"
```

* Run your new site in http://localhost:1313/

```bash
hugo server -D
```

## Push your changes to GitLab

```bash
cd ~/www/mypage
git add .
git commit
git push
```

## How to create new posts

* Create a new post using Hugo

```bash
cd ~/www/mypage
hugo new posts/my-first-post.md
```

* Add your text, and don't forget to change to 'draft: false' after finising

```bash
vim content/posts/my-first-post.md
```
```markdown
---
title: "My first post"
date: 2020-04-13T21:19:55+01:00
draft: false
toc: false
images:
tags:
  - tag1
  - tag2
---
## A header

text
```


## How to create other pages

* Create markdown files in content/page

```bash
cd ~/www/mypage
mkdir content/page/
vim content/page/about.md
```

* In you config.toml you can add the page (no need for the .md extension)

```toml
  [[menu.main]]
    identifier = "about"
    name       = "about"
    url        = "/page/about"
```

## Using your own domain

* Add you domain name (Settings > Pages > New Domain)

	![domain](/posts/2020/2020-04-13-setting-hugo-with-gitlab/new-domain.png)

* Follow the given instructions to add the DNS and Verification records in your domain registrar

## References

* https://gohugo.io/getting-started/quick-start/
* https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/
* https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/
* https://gitlab.com/help/user/project/pages/custom_domains_ssl_tls_certification/index.md
* https://www.youtube.com/watch?v=TWqh9MtT4Bg
