---
title: "The Three-Body Problem (Remembrance of Earth's Past Book 1) by Liu Cixin (2008)"
slug: "the-three-body-problem-remembrance-of-earths-past-book-1"
author: "Paulo Pereira"
date: 2019-10-04T22:51:00+01:00
lastmod: 2019-10-04T22:51:00+01:00
cover: "/posts/books/the-three-body-problem-remembrance-of-earths-past-book-1.jpg"
description: "Finished “The Three-Body Problem (Remembrance of Earth's Past Book 1)” by Liu Cixin."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2008
book authors:
  - Liu Cixin
book series:
  - Remembrance of Earth's Past Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Liu Cixin
  - Remembrance of Earth's Past Series
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Audiobook
aliases:
  - /posts/books/the-three-body-problem-remembrance-of-earths-past-book-1
---

Finished “The Three-Body Problem”.
* Author: [Liu Cixin](/book-authors/liu-cixin)
* First Published: [2008](/book-publication-year/2008)
* Series: [Remembrance of Earth's Past](/book-series/remembrance-of-earths-past-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/29498609-the-three-body-problem)
>
> ThreeBody Problem is the first chance for English-speaking listeners to experience this multiple award-winning phenomenon from China’s most beloved science fiction author, Liu Cixin.
>
> Set against the backdrop of China’s Cultural Revolution, a secret military project sends signals into space to establish contact with aliens. An alien civilization on the brink of destruction captures the signal and plans to invade Earth. Meanwhile, on Earth, different camps start forming, planning to either welcome the superior beings and help them take over a world seen as corrupt, or to fight against the invasion. The result is a science fiction masterpiece of enormous scope and vision.