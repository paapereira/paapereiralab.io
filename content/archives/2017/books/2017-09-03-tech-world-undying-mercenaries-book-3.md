---
title: "Tech World (Undying Mercenaries Book 3) by B.V. Larson (2014)"
slug: "tech-world-undying-mercenaries-book-3"
author: "Paulo Pereira"
date: 2017-09-03T23:04:00+01:00
lastmod: 2017-09-03T23:04:00+01:00
cover: "/posts/books/tech-world-undying-mercenaries-book-3.jpg"
description: "Finished “Tech World (Undying Mercenaries Book 3)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Tech World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [August 1st 2014](/book-publication-year/2014)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 3
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/23472641-tech-world)
>
> The Galactics arrived with their Battle fleet in 2052. Rather than being exterminated under a barrage of hell-burners, Earth joined a vast Empire that spanned the Milky Way. Our only worthwhile trade goods are our infamous mercenary legions, elite troops we sell to the highest alien bidder. In the third book in the series, James McGill is deployed on another alien world. His third interstellar tour is different in every way. Rather than meeting up with a primitive society, this time he’s headed to an advanced world. Tau Ceti, better known as Tech World, is the central trading capital of Frontier 921. McGill figures he’s lucked out. The assignment looks dull but luxurious. Tau Ceti boasts a planet-wide city with a trillion inhabitants, all of whom are only interested in making a few credits. But all is not well on Tech World. The Empire is crumbling, an invasion is coming, and McGill’s easy ride through life and death has come to an end.