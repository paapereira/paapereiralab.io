---
title: "Ubuntu in my desktop (status)"
slug: "ubuntu-in-my-desktop-status"
author: "Paulo Pereira"
date: 2008-04-29T21:45:00+01:00
lastmod: 2008-04-29T21:45:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
aliases:
  - /posts/ubuntu-in-my-desktop-status/
---

I'm preparing to install [Ubuntu 8.04 "Hardy Heron"](http://www.ubuntu.com/) in my desktop machine.

## Before starting

- Try, using the Ubuntu live-cd, at least graphics, sound and cable network access (done)
- Backup important data (done)
- Decide the partition layout for all my hard drives ([example](/posts/partition-map/) (done)
- Rethink my home network (IPs, Windows workgroup names...)
- Decide between Ubuntu 32 bits and Ubuntu 64 bits for my AMD Athlon 64BIT X2 DUAL-CORE 4200+ SKAM2 2.20 GB
  - I've decided to go with Ubuntu 32 bits (done)
  - Check if both cores are beeing used (done)

I will be dual-booting (for now) with Windows XP, and in order to release myself from Windows completely, here's a "to achieve" list.

This list doesn't have a significant order of importance.

**To achieve** (check the installation eXPerience [here](/posts/ubuntu-in-my-desktop-installation/)

- Internet access using my home network (cable) (achieved)
- Internet access using my home network (wireless)
- Read and write support from Linux to my Windows partitions (just in case) (achieved)
- Read and write support from Windows to my Linux partitions (just in case)
- Divx and Xvid playback with subtitle files support (achieved)
- Full browser experience (Java, flash...) (achieved)
- Microsoft Office 100% support (achieved using a virtual machine)
  - it isn't enough .doc support
  - as an example, the Excel formats, graphs, etc. should be exactly the same
  - it could be done using a virtual machine
- Windows XP in a virtual machine (objective - legacy support) (achieved)
- Streaming media content (xvid, divx) to my Xbox 360
  - the the content must be converted to the right format in real time
  - it must add subtitles to the media (for instance .srt separated files)
  - in Windows I use [TVersity](http://tversity.com/home)
- Print to PDF (achieved)
- Have a subtitle editor (achieved)
  - a needed feature is to fix the subtitles in order to keep a fixed number of characters for column
  - in Windows I use [Subtitle Workshop](http://www.softpedia.com/get/Multimedia/Video/Other-VIDEO-Tools/Subtitle-Workshop.shtml)
- Software to mount cd image files (like .iso) (achieved)
- Recording software (CD and DVD support) (achieved)
- Google Earth and Google Notifier (achieved)
- Full iPod Touch support (it could be done using a virtual machine)
- Full Calendar and Contacts sync support for Qtek 9100 (it could be done using a virtual machine) (achieved)
- Apache, PHP and MySQL solution for my home database solution, currently working in Windows (achieved)
- Remote Desktop Connection to other machines, including Windows (it could be done using a virtual machine)
- Home network (at least shared folders) (achieved)

## Other nice stuff I'd like

- BIOS upgrades (it could be done using a virtual machine)
- System monitor (hard drive free space, cpu, memory, network and temperature)