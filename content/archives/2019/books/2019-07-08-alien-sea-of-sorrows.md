---
title: "Alien: Sea of Sorrows by James A. Moore (2014)"
slug: "alien-sea-of-sorrows"
author: "Paulo Pereira"
date: 2019-07-08T21:40:00+01:00
lastmod: 2019-07-08T21:40:00+01:00
cover: "/posts/books/alien-sea-of-sorrows.jpg"
description: "Finished “Alien: Sea of Sorrows” by James A. Moore."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - James A. Moore
book series:
  - Alien Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - James A. Moore
  - Alien Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Alien: Sea of Sorrows”.
* Author: [James A. Moore](/book-authors/james-a.-moore)
* First Published: [2014](/book-publication-year/2014)
* Series: [Alien Series](/book-series/alien-series)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/39879903-alien)
>
> The Alien film franchise has been embraced by sci-fi fans around the world. The series stars Lieutenant Ellen Ripley and her battles with the deadly Xenomorph commonly referred to as the Alien. Continuing the groundbreaking story from ALIEN: OUT OF THE SHADOWS by Tim Lebbon, this novel will reveal Ripley's legacy, as her descendants continue to be harried by the Weyland-Yutani Corporation in their unceasing effort to weaponize the aliens.
>
> Produced by Twentieth Century Fox, the franchise launched with the release of the 1979 film, Alien. The film led to three very successful movie sequels, numerous books, comics and video game spinoffs. This second novel and its two tightly plotted tie-ins will directly relate to the first two films of the franchise, expanding the canon like never before.