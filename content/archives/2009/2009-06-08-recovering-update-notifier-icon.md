---
title: "Recovering the update-notifier icon in Ubuntu 9.04"
slug: "recovering-update-notifier-icon"
author: "Paulo Pereira"
date: 2009-06-08T17:03:00+01:00
lastmod: 2009-06-08T17:03:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 9.04
  - Jaunty Jackalope
---

If you have installed Ubuntu 9.04 you probably noticed by now that the *update-notifier* icon no longer appears when there are new updates available. Instead the Update Manager window opens.

This may be a problem if you don't notice that window and shutdown your system. After that you have to wait for new updates (and notice the  Update Manager window that time) or manually look for updates.

To get the update-notifier icon just run the following:

```bash
gconftool -s --type bool /apps/update-notifier/auto_launch false
```

Now, restart your system or restart the update-notifier:

```bash
killall update-notifier
update-notifier &
disown
```

If you want to go back to the original state run the following and restart your system ou update-notifier:

```bash
gconftool -s --type bool /apps/update-notifier/auto_launch true
```