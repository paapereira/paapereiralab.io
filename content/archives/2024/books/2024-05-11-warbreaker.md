---
title: "Warbreaker by Brandon Sanderson (2009)"
slug: "warbreaker"
author: "Paulo Pereira"
date: 2024-05-11T15:20:00+01:00
lastmod: 2024-05-11T15:20:00+01:00
cover: "/posts/books/warbreaker.jpg"
description: "Finished “Warbreaker” by Brandon Sanderson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2009
book authors:
  - Brandon Sanderson
book series:
  - 
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Brandon Sanderson
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/warbreaker
---

Finished “Warbreaker”.
* Author: [Brandon Sanderson](/book-authors/brandon-sanderson)
* First Published: [June 9, 2009](/book-publication-year/2009)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8120570-warbreaker)
>
> Warbreaker is the story of two sisters, who happen to be princesses, the God King one of them has to marry, the lesser god who doesn't like his job, and the immortal who's still trying to undo the mistakes he made hundreds of years ago.
> 
> Their world is one in which those who die in glory return as gods to live confined to a pantheon in Hallandren's capital city and where a power known as BioChromatic magic is based on an essence known as breath that can only be collected one unit at a time from individual people.
> 
> By using breath and drawing upon the color in everyday objects, all manner of miracles and mischief can be accomplished. It will take considerable quantities of each to resolve all the challenges facing Vivenna and Siri, princesses of Idris; Susebron the God King; Lightsong, reluctant god of bravery, and mysterious Vasher, the Warbreaker.