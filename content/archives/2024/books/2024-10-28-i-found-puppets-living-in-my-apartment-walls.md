---
title: "I Found Puppets Living In My Apartment Walls (I Found Horror Series) by Ben Farthing (2023)"
slug: "i-found-puppets-living-in-my-apartment-walls"
author: "Paulo Pereira"
date: 2024-10-28T22:00:00+00:00
lastmod: 2024-10-28T22:00:00+00:00
cover: "/posts/books/i-found-puppets-living-in-my-apartment-walls.jpg"
description: "Finished “I Found Puppets Living In My Apartment Walls (I Found Horror Series)” by Ben Farthing."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - Ben Farthing
book series:
  - I Found Horror Series
book genres:
  - Fiction
  - Horror
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Ben Farthing
  - I Found Horror Series
  - Fiction
  - Horror
  - 6/10 Books
  - Ebook
---

Finished “I Found Puppets Living In My Apartment Walls”.
* Author: [Ben Farthing](/book-authors/ben-farthing)
* First Published: [August 1, 2023](/book-publication-year/2023)
* Series: [I Found Horror](/book-series/i-found-horror-series)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/177119189-i-found-puppets-living-in-my-apartment-walls)
>
> Johnny awakes. A puppet looms over his bed.
> 
> He recognizes the furry Grandpa was its puppeteer on the children’s television show R-City Street. But Grandpa went missing a year ago. He disappeared from this very apartment building, which was converted from the old R-City Street studio.
> 
> Desperate to see Grandpa again, Johnny follows the puppet inside the building’s walls, ever deeper into a puppet-infested labyrinth...