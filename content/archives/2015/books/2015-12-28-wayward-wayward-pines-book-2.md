---
title: "Wayward (Wayward Pines Book 2) by Blake Crouch (2013)"
slug: "wayward-wayward-pines-book-2"
author: "Paulo Pereira"
date: 2015-12-28T23:55:00+00:00
lastmod: 2015-12-28T23:55:00+00:00
cover: "/posts/books/wayward-wayward-pines-book-2.jpg"
description: "Finished “Wayward (Wayward Pines Book 2)” by Blake Crouch."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - Blake Crouch
book series:
  - Wayward Pines Series
book genres:
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Blake Crouch
  - Wayward Pines Series
  - Science Fiction
  - 8/10 Books
  - Audiobook
---

Finished “Wayward”.
* Author: [Blake Crouch](/book-authors/blake-crouch)
* First Published: [September 17th 2013](/book-publication-year/2013)
* Series: [Wayward Pines](/book-series/wayward-pines-series) Book 2
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17920175-wayward)
>
> Welcome to Wayward Pines, population 461. Nestled amidst picture-perfect mountains, the idyllic town is a modern-day Eden...except for the electrified fence and razor wire, snipers scoping everything 24/7, and the relentless surveillance tracking each word and gesture.
>
> None of the residents know how they got here. They are told where to work, how to live, and who to marry. Some believe they are dead. Others think they’re trapped in an unfathomable experiment. Everyone secretly dreams of leaving, but those who dare face a terrifying surprise.
>
> Ethan Burke has seen the world beyond. He’s sheriff, and one of the few who knows the truth—Wayward Pines isn’t just a town. And what lies on the other side of the fence is a nightmare beyond anyone’s imagining.