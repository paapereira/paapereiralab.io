---
title: "Biography of Resistance: The Epic Battle Between People and Pathogens by Muhammad H. Zaman (2020)"
slug: "biography-of-resistance"
author: "Paulo Pereira"
date: 2021-10-29T21:00:00+01:00
lastmod: 2021-10-29T21:00:00+01:00
cover: "/posts/books/biography-of-resistance.jpg"
description: "Finished “Biography of Resistance: The Epic Battle Between People and Pathogens” by Muhammad H. Zaman."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Muhammad H. Zaman
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Muhammad H. Zaman
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “Biography of Resistance: The Epic Battle Between People and Pathogens”.
* Author: [Muhammad H. Zaman](/book-authors/muhammad-h.-zaman)
* First Published: [April 21st 2020](/book-publication-year/2020)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/51090646-biography-of-resistance)
>
> Award-winning Boston University educator and researcher Muhammad H. Zaman provides a chilling look at the rise of antibiotic-resistant superbugs, explaining how we got here and what we must do to address this growing global health crisis.
> 
> In September 2016, a woman in Nevada became the first known case in the U.S. of a person who died of an infection resistant to every antibiotic available. Her death is the worst nightmare of infectious disease doctors and public health professionals. While bacteria live within us and are essential for our health, some strains can kill us. As bacteria continue to mutate, becoming increasingly resistant to known antibiotics, we are likely to face a public health crisis of unimaginable proportions. “It will be like the great plague of the middle ages, the influenza pandemic of 1918, the AIDS crisis of the 1990s, and the Ebola epidemic of 2014 all combined into a single threat,” Muhammad H. Zaman warns.
> 
> 'BIOGRAPHY OF RESISTANCE' is Zaman’s riveting and timely look at why and how microbes are becoming superbugs. It is a story of science and evolution that looks to history, culture, attitudes and our own individual choices and collective human behavior. Following the trail of resistant bacteria from previously un-contacted tribes in the Amazon to the isolated islands in the Arctic, from the urban slums of Karachi to the wilderness of the Australian outback, Zaman examines the myriad factors contributing to this unfolding health crisis—including war, greed, natural disasters, and germophobia—to the culprits driving it: pharmaceutical companies, farmers, industrialists, doctors, governments, and ordinary people, all whose choices are pushing us closer to catastrophe.