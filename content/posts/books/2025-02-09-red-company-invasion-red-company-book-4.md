---
title: "Red Company: Invasion (Red Company Book 4) by B.V. Larson (2024)"
slug: "red-company-invasion-red-company-book-4"
author: "Paulo Pereira"
date: 2025-02-09T14:00:00+00:00
lastmod: 2025-02-09T14:00:00+00:00
cover: "/posts/books/red-company-invasion-red-company-book-4.jpg"
description: "Finished “Red Company: Invasion (Red Company Book 4)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2024
book authors:
  - B.V. Larson
book series:
  - Red Company Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Red Company Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Red Company: Invasion”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [January 21, 2024](/book-publication-year/2024)
* Series: [Red Company](/book-series/red-company-series) Book 4
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/203955182-red-company)
>
> An alien ship invades the Solar System for the first time in recorded history. The Earth Cruiser Agamemnon is destroyed in combat, and the search for answers begins.
> 
> The veteran crew of Borag gets the assignment, and the race is on. Sgt. Starn of Red Company must visit the disaster site, perform a search and rescue mission, and return to Mars alive.
> 
> What kind of technology could destroy an Earth warship so easily? Why is a single life-pod found floating, transponder pinging away—with no one aboard? Most important of all, what is the prowling alien super-ship going to do next?