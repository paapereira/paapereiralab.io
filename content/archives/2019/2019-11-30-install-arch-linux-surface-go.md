---
title: "🖥️ 🐧 Installing Arch Linux in my new Surface Go"
slug: "install-arch-linux-surface-go"
author: "Paulo Pereira"
date: 2019-11-30T18:01:16+00:00
lastmod: 2019-11-30T18:01:16+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Surface Go
aliases:
  - /posts/install-arch-linux-surface-go
---

## Surface Go

* Surface Go (10'' - Intel Pentium Gold 4415Y - RAM: 8 GB - 128 GB SSD - Intel HD 615)
* Surface Go Black Type Cover

![Surface Go](/posts/2019/2019-11-30-install-arch-linux-surface-go/surface-go.png)

## Final setup

* Run only Linux
* Arch Linux
* Gnome

## Preparation

### Needed hardware

* Surface Go
* Surface Go Keyboard or USB keyboard/mouse
* USB pen drive
* USB Ethernet adapter
* USB-C to USB adapter + USB Hub or USB-C hub

### Burn ISO to USB pen drive

1. Download the Arch Linux ISO.
2. Burn the iso in the pen drive.

```bash
sudo dd bs=4M if=archlinux-2019.10.01-x86_64.iso of=/dev/sdc status=progress oflag=sync
```

### Connect the dongles

Depending on your setup make sure you have the USB pen drive with the Arch Linux ISO burned and a Ehternet cable connected.

### Disable Secure Boot

1. Shut down your Surface.
2. Press and hold the Volume Up button on your Surface, then press and release the Power button.
3. When you see the Surface logo screen appear, release the Volume Up button. The UEFI settings menu will appear in a few seconds.
4. Click on Boot Configuration and move USB Storage up the list.
5. Click on Security and set Secure Boot to disabled.
6. Click on Exit,
7. Save the settings and reboot.

## Installation from the USB pen drive live environment

### First steps and testing Internet connection

Make sure to use the correct keyboard layout (e.g.: for a portuguese layout; use the ? key for the - character).

```bash
loadkeys pt-latin9
```

Test the Internet connection.

```bash
ip link
dhcpcd
ping archlinux.org
```

Active ntp service.

```bash
timedatectl set-ntp true
```

Make sure the en_US.UTF-8 is uncommented.

```bash
nano /etc/locale.gen
locale-gen
```

### Create partitions

List the drives and use cgdisk to create the partitions.

```bash
lsblk
cgdisk /dev/nvme0n1
```

Partitions to create.

|#|Size|Hexcode|Label   |
|-|----|-------|--------|
|1|1G  |ef00   |boot_efi|
|2|1G  |ef02   |boot    |
|3|109G|8304   |root    |
|4|8.2G|8200   |swap    |

Format the partitions.
    
```bash
mkfs.vfat /dev/nvme0n1p1
mkfs.ext4 /dev/nvme0n1p2
mkfs.ext4 /dev/nvme0n1p3
mkswap /dev/nvme0n1p4
swapon /dev/nvme0n1p4
```

### Mounting the partitions

```bash
mount /dev/nvme0n1p3 /mnt
mkdir -p /mnt/boot
mount /dev/nvme0n1p2 /mnt/boot
mkdir -p /mnt/boot/efi
mount /dev/nvme0n1p1 /mnt/boot/efi
```

### Install the base system

Choose a mirror near you.

```bash
nano /etc/pacman.d/mirrorlist
```

Copy your preferred mirror to the first line.

Tips:

* Alt+6 to copy a Server line.
* PageUp key to scroll up.
* Ctrl+U to paste it at the top of the list.
* Ctrl+X to exit, and when prompted to save changes, press Y and Enter.

Refresh the packages list and install the base system.

```bash
pacman -Syy
pacstrap /mnt base base-devel linux linux-firmware
```

Generate the fstab file.

```bash
genfstab -U /mnt >> /mnt/etc/fstab
```

### Configurations, drivers and boot manager

Change context to the base install mounted partitions.

```bash
arch-chroot /mnt
```

Install nano to edit files.

```bash
pacman -S nano
```

Define your local timezone.

```bash
ln -s /usr/share/zoneinfo/Europe/Lisbon /etc/localtime
```

Hardware clock.

```bash
hwclock --systohc --utc
```

Uncomment en_US.UTF-8 UTF-8 and generate the locale.

```bash
nano /etc/locale.gen
locale-gen
```

LANG and KEYMAP configurantion.

```bash
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=pt-latin9" >> /etc/vconsole.conf
```

Hosts and hostname configuration.

```bash
echo "myhostname" >> /etc/hostname
echo "127.0.0.1  localhost" >> /etc/hosts
echo "::1        localhost" >> /etc/hosts
echo "127.0.1.1  myhostname.localdomain  myhostname" >> /etc/hosts
```

Install and enable network services.

```bash
pacman -S networkmanager
systemctl enable NetworkManager.service
```

Useful to have sooner than later.

```bash
pacman -S git wget unzip openssh
systemctl enable sshd
```

mkinitcpio.

```bash
mkinitcpio -P
```

Change your root user password.

```bash
passwd
```

Install grub.

```bash
pacman -S grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
```

Install intel-ucode.

```bash
pacman -S intel-ucode
```

Exit and reboot.

```bash
exit
umount /mnt/{boot,home,boot/efi,}
reboot
```

## Next steps before a window environment

Log in as root and create your user.

```bash
useradd -m -g users -s /bin/bash youruser
chfn youruser
passwd youruser
```

Create a new group to match your user.

```bash
groupadd yourgroup
gpasswd -a youruser yourgroup
```

Add your user as a sudoer.

```bash
pacman -S sudo vi
visudo /etc/sudoers
```
Add:
`youruser   ALL=(ALL) ALL`

Video and audio.

```bash
pacman -S xorg-server xorg-xinit xorg-apps xorg-twm xorg-xclock xterm
lspci | grep -e VGA -e 3D
pacman -S xf86-video-intel xf86-input-libinput
pacman -S pulseaudio pavucontrol alsa-utils
startx
```

Gnome and gdm.

```bash
pacman -S gnome gnome-shell gdm
systemctl enable gdm.service
```

Reboot.

## Surface kernel and drivers

```bash
wget -qO - https://raw.githubusercontent.com/qzed/linux-surface/master/keys/qzed.asc \
    | sudo pacman-key --add -
sudo pacman-key --finger luzmaximilian@gmail.com
sudo pacman-key --lsign-key luzmaximilian@gmail.com
```

```bash
sudo vi /etc/pacman.conf
```

```txt
[linux-surface]
Server = https://tmsp.io/fs/repos/arch/$repo/
```

```bash
sudo pacman -Syy
sudo pacman linux-surface linux-surface-headers linux-surface-docs
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

Reboot and log in into Gnome with Xorg.

## Other configurations and software

### Configure and test wifi

Connect to your wireless network.

If it's not working:

```bash
cd Downloads
wget http://www.killernetworking.com/support/K1535_Debian/board.bin
sudo rm /lib/firmware/ath10k/QCA6174/hw2.1/board.bin
sudo cp ~/Downloads/board.bin /lib/firmware/ath10k/QCA6174/hw2.1
sudo rm /lib/firmware/ath10k/QCA6174/hw3.0/board.bin
sudo cp ~/Downloads/board.bin /lib/firmware/ath10k/QCA6174/hw3.0
reboot
```

### AUR

```bash
cd Downloads
wget https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz
cd yay
makepkg -sri
```

### Firefox

```bash
sudo pacman -S firefox
```

#### touchscreen support

```bash
sudo vi /usr/share/applications/firefox.desktop
```

Look for the three 'Exec=' options and add 'env MOZ_USE_XINPUT2=1 ' in front of the firefox binary.
`Exec=env MOZ_USE_XINPUT2=1 /usr/lib/firefox/firefox`

Go to about:config.
`dom.w3c.touch_events.enabled; 1
browser.urlbar.clickSelectsAll; true
browser.urlbar.doubleClickSelectsAll; false
layout.word_select.stop_at_punctuation; true (default)
layout.css.devPixelsPerPx; 2.0`

```bash
vi /etc/security/pam_env.conf
```
`MOZ_USE_XINPUT2 DEFAULT=1`

#### profile-sync-daemon

https://wiki.archlinux.org/index.php/Profile-sync-daemon

```bash
yay -S profile-sync-daemon
systemctl --user enable psd.service
```

```bash
vi .config/psd/psd.conf
```
`BROWSERS="firefox"`

```bash
systemctl --user start psd.service
psd p
```

### Screen rotation

```bash
sudo pacman iio-sensor-proxy
yay -S screenrotator-git
```

Use Gnome with Xorg, not Wayland.

### Touchpad gestures

https://wiki.archlinux.org/index.php/Libinput#libinput-gestures
https://github.com/bulletmark/libinput-gestures/blob/master/README.md

```bash
sudo gpasswd -a $USER input
yay -S libinput-gestures xdotool wmctrl
libinput-gestures-setup autostart
```

Reboot.

### ssh from another machine

```bash
ssh-copy-id surfacehostname
```

### Pacman/aur Package Manager

```bash
yay -S pamac-aur
```

### fish shell

```bash
yay -S fish
chsh -s /usr/bin/fish
```

### Thunar

```bash
yay -S thunar thunar-volman thunar-archive-plugin thunar-media-tags-plugin thunar-thumbnailers
```

#### Customized Thumbnailer for folders

https://docs.xfce.org/xfce/thunar/tumbler

```bash
sudo vi /usr/share/thumbnailers/folder.thumbnailer
```
```text
[Thumbnailer Entry]
Version=1.0
Encoding=UTF-8
Type=X-Thumbnailer
Name=Folder Thumbnailer
MimeType=inode/directory;
Exec=/usr/bin/folder-thumbnailer %s %i %o %u
```

```bash
sudo vi /usr/bin/folder-thumbnailer
```
```text
#!/bin/bash
 
convert -thumbnail "$1" "$2/folder.jpg" "$3" 1>/dev/null 2>&1 ||\
convert -thumbnail "$1" "$2/.folder.jpg" "$3" 1>/dev/null 2>&1 ||\
convert -thumbnail "$1" "$2/folder.png" "$3" 1>/dev/null 2>&1 ||\
convert -thumbnail "$1" "$2/.folder.png" "$3" 1>/dev/null 2>&1 ||\
convert -thumbnail "$1" "$2/cover.jpg" "$3" 1>/dev/null 2>&1 ||\
rm -f "$HOME/.cache/thumbnails/normal/$(echo -n "$4" | md5sum | cut -d " " -f1).png" ||\
rm -f "$HOME/.thumbnails/normal/$(echo -n "$4" | md5sum | cut -d " " -f1).png" ||\
rm -f "$HOME/.cache/thumbnails/large/$(echo -n "$4" | md5sum | cut -d " " -f1).png" ||\
rm -f "$HOME/.thumbnails/large/$(echo -n "$4" | md5sum | cut -d " " -f1).png" ||\
exit 1
```
```bash
sudo chmod a+x /usr/bin/folder-thumbnailer
```


### pass

...

### reflector

https://wiki.archlinux.org/index.php/Reflector

### Seafile

```bash
yay -S seafile-client
```

### Hibernate

Add resume the the HOOKS list.

```bash
sudo vi /etc/mkinitcpio.conf
```
`HOOKS=(base udev autodetect modconf block filesystems keyboard resume fsck)`
```bash
sudo mkinitcpio
sudo mkinitcpio -P
```

Add the UUID for your swap partition to the grub default command line options.

```bash
sudo cat /etc/fstab
sudo vi /etc/default/grub
```
`
GRUB_TIMEOUT=2
GRUB_CMDLINE_LINUX_DEFAULT="resume=UUID=7c24eb35-0c39-4c85-892a-361d7dcad795 loglevel=3 quiet"
`
```bash
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

Reduce the swappiness value.

```bash
sudo vi /etc/sysctl.d/99-swappiness.conf
```
`vm.swappiness=10`
To check after reboot.
```bash
cat /sys/fs/cgroup/memory/memory.swappiness
```

### Power management

```bash
sudo pacman -S acpid
sudo systemctl enable acpid.service
```

Set in Settings > Power the power button to hibernate.

Reboot.

### Gnome Shell extensions

List of my favorite extensions. In my surface some off them are disabled.

* [Applications Menu](https://extensions.gnome.org/extension/6/applications-menu/) (disabled)
* [Auto Move Windows](https://extensions.gnome.org/extension/16/auto-move-windows/)
* [Dash to Dock](https://extensions.gnome.org/extension/307/dash-to-dock/)
* [Dynamic Panel Transparency](https://extensions.gnome.org/extension/1011/dynamic-panel-transparency/)
* [Gajim IM integration](https://extensions.gnome.org/extension/565/gajim-im-integration/)
* [GSConnect](https://extensions.gnome.org/extension/1319/gsconnect/)
* [Icon Hider](https://extensions.gnome.org/extension/351/icon-hider/)
* [KStatusNotifierItem/AppIndicator Support](https://extensions.gnome.org/extension/615/appindicator-support/)
* [Launch new instance](https://extensions.gnome.org/extension/600/launch-new-instance/)
* [Native Window Placement](https://extensions.gnome.org/extension/18/native-window-placement/)
* [No Symbolic Icons](https://extensions.gnome.org/extension/1304/no-symbolic-icons/)
* Pamac Updates Indicator
* [Places Status Indicator](https://extensions.gnome.org/extension/8/places-status-indicator/) (disabled)
* [Removable Drive Menu](https://extensions.gnome.org/extension/7/removable-drive-menu/) (disabled)
* [Screenshot Window Sizer](https://extensions.gnome.org/extension/881/screenshot-window-sizer/)
* [Tray Icons](https://extensions.gnome.org/extension/1503/tray-icons/) (disabled)
* [Unite](https://extensions.gnome.org/extension/1287/unite/)
* [User Themes](https://extensions.gnome.org/extension/19/user-themes/)
* [windowNavigator](https://extensions.gnome.org/extension/10/windownavigator/)
* [Workspace Indicator](https://extensions.gnome.org/extension/21/workspace-indicator/) (disabled)

### Libreoffice

```bash
yay -S libreoffice-fresh libreoffice-fresh-pt
```

### ulauncher

```bash
yay -S ulauncher
```

#### extensions

* https://github.com/dcervenkov/ulauncher-z-search
* https://github.com/dalanicolai/gnome-tracker-extension
* https://github.com/rkarami/ulauncher-password-generator

#### shortcuts

* DuckDuckGo | d | https://duckduckgo.com/?q=%s

### anbox

#### via AUR

https://wiki.archlinux.org/index.php/Anbox

```bash
yay -S android-tools anbox-image anbox-modules-dkms-git anbox-bridge
yay -S anbox-git
sudo systemctl enable anbox-container-manager.service
sudo systemctl start anbox-container-manager.service
sudo modprobe ashmem_linux
sudo modprobe binder_linux
```

You must execute `anbox-bridge` every time before starting `anbox` in order to get network working in anbox. 

#### via snap

```bash
yay -S snapd
sudo systemctl enable snapd.service
sudo systemctl start snapd.service
sudo snap install --devmode --beta anbox
sudo modprobe ashmem_linux
sudo modprobe binder_linux
```

To update in the future:
```bash
sudo snap refresh --beta --devmode anbox
```

You must execute `anbox-bridge` every time before starting `anbox` in order to get network working in anbox. 

### sound issues

https://github.com/anbox/anbox/issues/904#issuecomment-420904038

```bash
sudo vi /var/lib/anbox/rootfs-overlay/system/etc/media_codecs.xml
```

Paste content from: https://github.com/anbox/anbox/files/2378029/media_codecs.xml.txt

### GRUB

```bash
yay -S arch-silence-grub-theme
sudo vi /etc/default/grub
```
`GRUB_THEME="/boot/grub/themes/arch-silence/theme.txt"`
```bash
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

### protonvpn-cli

https://wiki.archlinux.org/index.php/ProtonVPN

```bash
yay -S protonvpn-cli protonvpn-applet
```

## Some links

https://www.reddit.com/r/SurfaceLinux/comments/94hjxv/surface_go_first_impressions/
https://www.reddit.com/r/SurfaceLinux/comments/dlfain/guide_installing_arch_linux_on_your_surface_device/
https://wiki.archlinux.org/index.php/Tablet_PC
https://bytewelder.com/posts/2019/08/13/surface-go-linux-mint.html
https://gist.github.com/johnramsden/f873723150209ccc4533f43ef100e9da
https://ramsdenj.com/2016/08/29/arch-linux-on-the-surface-pro-4.html
https://github.com/jakeday/linux-surface
https://github.com/dmhacker/arch-linux-surface
https://github.com/qzed/linux-surface/wiki/Package-Repositories
https://tmsp.io/fs/repos/arch/linux-surface/
https://wiki.archlinux.org/index.php/Installation_guide
https://wiki.archlinux.org/index.php/General_recommendations
https://dasgeekcommunity.com/extras.html
https://wiki.archlinux.org/index.php/Touchscreen
https://medium.com/@TheBitStick/fedora-31-on-the-surface-go-f81dbb9fa91b