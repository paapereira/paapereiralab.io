---
title: "Conquest (Star Force Book 4) by B.V. Larson (2011)"
slug: "conquest-star-force-book-4"
author: "Paulo Pereira"
date: 2016-04-30T23:46:00+01:00
lastmod: 2016-04-30T23:46:00+01:00
cover: "/posts/books/conquest-star-force-book-4.jpg"
description: "Finished “Conquest (Star Force Book 4)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - B.V. Larson
book series:
  - Star Force Series
book genres:
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Force Series
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Conquest”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [December 13th 2011](/book-publication-year/2011)
* Series: [Star Force](/book-series/star-force-series) Book 4
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/16136849-conquest)
>
> Conquest is the next chapter in the great interstellar war between all living creatures and the machines. Star Force must stop the machine invaders once again - but how?
> In the fourth book of the Star Force series, Kyle Riggs has freed Earth from the chains of the Macros - but at what cost? The Macros no longer trust him. He is a mad dog that must be put down - and all Star Force must be stamped out with him. The war expands in this story, and mankind is once again faced with annihilation.