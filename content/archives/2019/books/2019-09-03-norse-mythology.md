---
title: "Norse Mythology by Neil Gaiman (2017)"
slug: "norse-mythology"
author: "Paulo Pereira"
date: 2019-09-03T22:07:00+01:00
lastmod: 2019-09-03T22:07:00+01:00
cover: "/posts/books/norse-mythology.jpg"
description: "Finished “Norse Mythology” by Neil Gaiman."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Neil Gaiman
book series:
  - 
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Neil Gaiman
  - Fiction
  - Fantasy
  - 7/10 Books
  - Audiobook
---

Finished “Norse Mythology”.
* Author: [Neil Gaiman](/book-authors/neil-gaiman)
* First Published: [2017](/book-publication-year/2017)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34097209-norse-mythology)
>
> Neil Gaiman has long been inspired by ancient mythology in creating the fantastical realms of his fiction. Now he turns his attention back to the source, presenting a bravura rendition of the great northern tales. In Norse Mythology, Gaiman fashions primeval stories into a novelistic arc that begins with the genesis of the legendary nine worlds; delves into the exploits of the deities, dwarves, and giants; and culminates in Ragnarok, the twilight of the gods and the rebirth of a new time and people. Gaiman stays true to the myths while vividly reincarnating Odin, the highest of the high, wise, daring, and cunning; Thor, Odin’s son, incredibly strong yet not the wisest of gods; and Loki, the son of giants, a trickster and unsurpassable manipulator. From Gaiman’s deft and witty prose emerges the gods with their fiercely competitive natures, their susceptibility to being duped and to dupe others, and their tendency to let passion ignite their actions, making these long-ago myths breathe pungent life again.