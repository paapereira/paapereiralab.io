---
title: "Book Haul for August 2021"
slug: "book-haul-2021-08"
author: "Paulo Pereira"
date: 2021-09-04T14:45:33+01:00
lastmod: 2021-09-04T14:45:33+01:00
cover: "/posts/book-hauls/2021-08-book-haul.png"
description: "August book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

August book haul.