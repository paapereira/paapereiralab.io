---
title: "The Demon Next Door by Bryan Burrough (2019)"
slug: "the-demon-next-door"
author: "Paulo Pereira"
date: 2019-12-23T22:52:00+00:00
lastmod: 2019-12-23T22:52:00+00:00
cover: "/posts/books/the-demon-next-door.jpg"
description: "Finished “The Demon Next Door” by Bryan Burrough."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Bryan Burrough
book series:
  -
book genres:
  - Nonfiction
  - Crime
book scores:
  - 4/10 Books
tags:
  - Book Log
  - Bryan Burrough
  - Nonfiction
  - Crime
  - 4/10 Books
  - Audiobook
---

Finished “The Demon Next Door”.
* Author: [Bryan Burrough](/book-authors/bryan-burrough)
* First Published: [2019](/book-publication-year/2019)
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44164596-the-demon-next-door)
>
> Best-selling author Bryan Burrough (Barbarians at the Gate, Public Enemies, Big Rich) recently made a shocking discovery: The small town of Temple, Texas, where he had grown up, had harbored a dark secret. One of his high school classmates, Danny Corwin, was a vicious serial killer who had raped and mutilated six women, murdering three of them. Yet the town had denied all early signs of the radical evil that was growing within Corwin. What had led the local media to ignore his early rapes? Why had the local Presbyterian Church tried to shield him from prison? Why had local law enforcement been unable to solve and prosecute his murders as they continued?
>
> Burrough is widely admired as a master storyteller, and this chilling tale raises important questions of whether serial killers can be recognized before they kill or rehabilitated after they do. It is also a story of Texas politics and power that led the good citizens of the town of Temple to enable a demon who was their worst nightmare.
>

