---
title: "Low RAM problems after AOKP Build 3 update"
slug: "low-ram-problems-after-aokp-build-3-update"
author: "Paulo Pereira"
date: 2013-02-23T17:33:00+00:00
lastmod: 2013-02-23T17:33:00+00:00
draft: false
toc: false
categories:
  - Android
tags:
  - Android
  - Galaxy Nexus
  - AOKP
aliases:
  - /posts/low-ram-problems-after-aokp-build-3-update/
---

After updating AOKP to the latest [build 3](/posts/updating-aokp-in-galaxy-nexus/), my phone started to get really laggy. Digging around in Google I [found](http://rootzwiki.com/topic/39029-low-ram-sluggish-phone/) others having the same problem.

It seems that the SystemUI has a memory leak, and starts consuming an  abnormal quantity of RAM. This means that apps will start to be killed  and relaunched, including the UI. The consequence is the sluggishness of the phone. Rebooting solves the problem, but it's only temporary.

One suggestion given in the [RootzWiki](http://rootzwiki.com/topic/39029-low-ram-sluggish-phone/) forum was to disable the camera widget in the lockscreen. This seems to not work for some people, but it worked for me.

Just download the [Lockscreen Policy](https://play.google.com/store/apps/details?id=com.wordpress.chislonchow.deviceadminkeyguard) app, and disable the camera widget (or all the widgets completely) from the lockscreen.

![Lockscreen Policy](/posts/2013/2013-02-23-low-ram-problems-after-aokp-build-3/lockscreen-policy.png)

Other widgets you may may have included in the lockscreen may also be the reason for this low RAM problems.

In the [RootzWiki](http://rootzwiki.com/topic/39029-low-ram-sluggish-phone/page__st__100) forum, another option emerged: a unofficial AOKP v4.2.2 based version.

I've been running the [Feb-20](http://erebos.xfer.in/marclandis/maguro/) version for some days with no problems.

To install I used the same method for updating to the official [build 3](/posts/updating-aokp-in-galaxy-nexus/), but the [franco.Kernel](/posts/favapps-francokernel-updater/) version used was r365 (first version supporting Android 4.2.2).