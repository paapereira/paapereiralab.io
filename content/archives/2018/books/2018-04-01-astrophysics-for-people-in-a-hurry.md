---
title: "Astrophysics for People in a Hurry by Neil deGrasse Tyson (2017)"
slug: "astrophysics-for-people-in-a-hurry"
author: "Paulo Pereira"
date: 2018-04-01T20:08:00+01:00
lastmod: 2018-04-01T20:08:00+01:00
cover: "/posts/books/astrophysics-for-people-in-a-hurry.jpg"
description: "Finished “Astrophysics for People in a Hurry” by Neil deGrasse Tyson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Neil deGrasse Tyson
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Neil deGrasse Tyson
  - Nonfiction
  - Science
  - 7/10 Books
  - Audiobook
---

Finished “Astrophysics for People in a Hurry”.
* Author: [Neil deGrasse Tyson](/book-authors/neil-degrasse-tyson)
* First Published: [May 2nd 2017](/book-publication-year/2017)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36540348)
>
> What is the nature of space and time? How do we fit within the universe? How does the universe fit within us? There's no better guide through these mind-expanding questions than acclaimed astrophysicist and best-selling author Neil deGrasse Tyson.
>
> But today, few of us have time to contemplate the cosmos. So Tyson brings the universe down to Earth succinctly and clearly, with sparkling wit, in digestible chapters consumable anytime and anywhere in your busy day. While waiting for your morning coffee to brew, or while waiting for the bus, the train, or the plane to arrive, Astrophysics for People in a Hurry will reveal just what you need to be fluent and ready for the next cosmic headlines: from the big bang to black holes, from quarks to quantum mechanics, and from the search for planets to the search for life in the universe.