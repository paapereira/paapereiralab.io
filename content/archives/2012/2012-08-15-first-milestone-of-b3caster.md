---
title: "First Milestone of b3caster"
slug: "first-milestone-of-b3caster"
author: "Paulo Pereira"
date: 2012-08-15T17:23:00+01:00
lastmod: 2012-08-15T17:23:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - B3 Server
  - b3caster
  - GitHub
---

You can grab the first Milestone of b3caster [here](https://github.com/paapereira/b3caster/tags).

Also check the [wiki](https://github.com/paapereira/b3caster/wiki) for install instructions.

![b3caster](/posts/2012/2012-08-15-first-milestone-of-b3caster/github.png)

