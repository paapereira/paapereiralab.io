---
title: "The Art of Deception by Kevin Mitnick (2001)"
slug: "the-art-of-deception"
author: "Paulo Pereira"
date: 2014-06-08T23:56:00+01:00
lastmod: 2014-06-08T23:56:00+01:00
cover: "/posts/books/the-art-of-deception.jpg"
description: "Finished “The Art of Deception: Controlling the Human Element of Security” by Kevin Mitnick."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2001
book authors:
  - Kevin Mitnick
book series:
  - 
book genres:
  - Nonfiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Kevin Mitnick
  - Nonfiction
  - 6/10 Books
  - Audiobook
---

Finished “The Art of Deception: Controlling the Human Element of Security”.
* Author: [Kevin Mitnick](/book-authors/kevin-mitnick)
* First Published: [January 1st 2001](/book-publication-year/2001)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17663588-the-art-of-deception)
>
> The world's most infamous hacker offers an insider's view of the low-tech threats to high-tech security Kevin Mitnick's exploits as a cyber-desperado and fugitive form one of the most exhaustive FBI manhunts in history and have spawned dozens of articles, books, films, and documentaries. Since his release from federal prison, in 1998, Mitnick has turned his life around and established himself as one of the most sought-after computer security experts worldwide. Now, in The Art of Deception, the world's most notorious hacker gives new meaning to the old adage, "It takes a thief to catch a thief."
>
> Focusing on the human factors involved with information security, Mitnick explains why all the firewalls and encryption protocols in the world will never be enough to stop a savvy grifter intent on rifling a corporate database or an irate employee determined to crash a system. With the help of many fascinating true stories of successful attacks on business and government, he illustrates just how susceptible even the most locked-down information systems are to a slick con artist impersonating an IRS agent. Narrating from the points of view of both the attacker and the victims, he explains why each attack was so successful and how it could have been prevented in an engaging and highly readable style reminiscent of a true-crime novel. And, perhaps most importantly, Mitnick offers advice for preventing these types of social engineering hacks through security protocols, training programs, and manuals that address the human element of security.