---
title: "Star Runner (Star Runner Book 1) by B.V. Larson (2020)"
slug: "star-runner-star-runner-book-1"
author: "Paulo Pereira"
date: 2023-04-02T18:30:00+01:00
lastmod: 2023-04-02T18:30:00+01:00
cover: "/posts/books/star-runner-star-runner-book-1.jpg"
description: "Finished “Star Runner (Star Runner Book 1)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - B.V. Larson
book series:
  - Star Runner Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Runner Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Star Runner”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [January 1, 2020](/book-publication-year/2020)
* Series: [Star Runner](/book-series/star-runner-series) Book 1
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/55343114-star-runner)
>
> Two centuries after humanity colonized the stars, new dangers emerge. The peaceful inhabitants of the Conclave are threatened by expanding alien powers. Invaders threaten the star cluster, attacking our fringe settlements.
> 
> Captain Bill Gorman has mysteriously disappeared. His clone, set aside for a dark day like this, awakens and begins to put together the pieces. What’s gone wrong out on the frontier? Why are our colonies being attacked by aliens while the Conclave worlds dream of better days? And what happened to the original Captain Gorman? 