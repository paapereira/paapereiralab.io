---
title: "Joy at Work: Organizing Your Professional Life (Magic Cleaning Book 3) by Marie Kondō and Scott Sonenshein (2020)"
slug: "joy-at-work"
author: "Paulo Pereira"
date: 2021-11-21T18:00:00+00:00
lastmod: 2021-11-21T18:00:00+00:00
cover: "/posts/books/joy-at-work.jpg"
description: "Finished “Joy at Work: Organizing Your Professional Life (Magic Cleaning Book 3)” by Marie Kondō and Scott Sonenshein."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Marie Kondo
  - Scott Sonenshein
book series:
  - Magic Cleaning Series
book genres:
  - Nonfiction
  - Self Help
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Marie Kondo
  - Scott Sonenshein
  - Magic Cleaning Series
  - Nonfiction
  - Self Help
  - 5/10 Books
  - Audiobook
---

Finished “Joy at Work: Organizing Your Professional Life”.
* Author: [Marie Kondō](/book-authors/marie-kondo) and [Scott Sonenshein](/book-authors/scott-sonenshein)
* First Published: [April 7th 2020](/book-publication-year/2020)
* Series: [Magic Cleaning](/book-series/magic-cleaning-series) Book 3
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/51526691-joy-at-work)
>
> The workplace is a magnet for clutter and mess. Who hasn't felt drained by wasteful meetings, disorganized papers, endless emails, and unnecessary tasks? These are the modern-day hazards of working, and they can slowly drain the joy from work, limit our chances of career progress, and undermine our well-being. The authors offer stories, studies, and strategies to help you eliminate clutter and make space for work that really matters. They will help you overcome the challenges of workplace mess and enjoy the productivity, success, and happiness that comes with a tidy desk and mind.