---
title: "Origin (Robert Langdon Book 5) by Dan Brown (2017)"
slug: "origin-robert-langdon-book-5"
author: "Paulo Pereira"
date: 2018-03-17T22:59:00+00:00
lastmod: 2018-03-17T22:59:00+00:00
cover: "/posts/books/origin-robert-langdon-book-5.jpg"
description: "Finished “Origin (Robert Langdon Book 5)” by Dan Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Dan Brown
book series:
  - Robert Langdon Series
book genres:
  - Fiction
  - Thriller
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Dan Brown
  - Robert Langdon Series
  - Fiction
  - Thriller
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “Origin”.
* Author: [Dan Brown](/book-authors/dan-brown)
* First Published: [October 3rd 2017](/book-publication-year/2017)
* Series: [Robert Langdon](/book-series/robert-langdon-series) Book 5
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36301683-origin)
>
> Robert Langdon, Harvard professor of symbology and religious iconology, arrives at the ultramodern Guggenheim Museum Bilbao to attend a major announcement—the unveiling of a discovery that “will change the face of science forever.” The evening’s host is Edmond Kirsch, a forty-year-old billionaire and futurist whose dazzling high-tech inventions and audacious predictions have made him a renowned global figure. Kirsch, who was one of Langdon’s first students at Harvard two decades earlier, is about to reveal an astonishing breakthrough . . . one that will answer two of the fundamental questions of human existence.
>
> As the event begins, Langdon and several hundred guests find themselves captivated by an utterly original presentation, which Langdon realizes will be far more controversial than he ever imagined. But the meticulously orchestrated evening suddenly erupts into chaos, and Kirsch’s precious discovery teeters on the brink of being lost forever. Reeling and facing an imminent threat, Langdon is forced into a desperate bid to escape Bilbao. With him is Ambra Vidal, the elegant museum director who worked with Kirsch to stage the provocative event. Together they flee to Barcelona on a perilous quest to locate a cryptic password that will unlock Kirsch’s secret.
>
> Navigating the dark corridors of hidden history and extreme religion, Langdon and Vidal must evade a tormented enemy whose all-knowing power seems to emanate from Spain’s Royal Palace itself . . . and who will stop at nothing to silence Edmond Kirsch. On a trail marked by modern art and enigmatic symbols, Langdon and Vidal uncover clues that ultimately bring them face-to-face with Kirsch’s shocking discovery . . . and the breathtaking truth that has long eluded us.