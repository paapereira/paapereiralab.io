---
title: "Mockingjay (The Hunger Games Book 3) by Suzanne Collins (2010)"
slug: "mockingjay-the-hunger-games-book-3"
author: "Paulo Pereira"
date: 2014-05-03T22:54:00+01:00
lastmod: 2014-05-03T22:54:00+01:00
cover: "/posts/books/mockingjay-the-hunger-games-book-3.jpg"
description: "Finished “Mockingjay (The Hunger Games Book 3)” by Suzanne Collins."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2010
book authors:
  - Suzanne Collins
book series:
  - The Hunger Games Series
book genres:
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Suzanne Collins
  - The Hunger Games Series
  - Science Fiction
  - 8/10 Books
  - Audiobook
---

Finished “Mockingjay”.
* Author: [Suzanne Collins](/book-authors/suzanne-collins)
* First Published: [August 2010](/book-publication-year/2010)
* Series: [The Hunger Games](/book-series/the-hunger-games-series) Book 3
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13484097-mockingjay)
>
> Against all odds, Katniss Everdeen has survived the Hunger Games twice. But now that she's made it out of the bloody arena live, she's still not safe. The Capitol is angry. The Capitol wants revenge....