---
title: "The Hero of Ages (The Mistborn Saga Book 3) by Brandon Sanderson (2008)"
slug: "the-hero-of-ages-the-mistborn-saga-book-3"
author: "Paulo Pereira"
date: 2024-01-10T22:00:00+00:00
lastmod: 2024-01-10T22:00:00+00:00
cover: "/posts/books/the-hero-of-ages-the-mistborn-saga-book-3.jpg"
description: "Finished “The Hero of Ages (The Mistborn Saga Book 3)” by Brandon Sanderson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2008
book authors:
  - Brandon Sanderson
book series:
  - The Mistborn Saga Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Brandon Sanderson
  - The Mistborn Saga Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/the-hero-of-ages-the-mistborn-saga-book-3
---

Finished “The Hero of Ages”.
* Author: [Brandon Sanderson](/book-authors/brandon-sanderson)
* First Published: [October 14, 2008](/book-publication-year/2008)
* Series: [The Mistborn Saga](/book-series/the-mistborn-saga-series) Book 3
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/2767793-the-hero-of-ages)
>
> Who is the Hero of Ages?
> 
> To end the Final Empire and restore freedom, Vin killed the Lord Ruler. But as a result, the Deepness—the lethal form of the ubiquitous mists—is back, along with increasingly heavy ashfalls and ever more powerful earthquakes. Humanity appears to be doomed.
> 
> Having escaped death at the climax of The Well of Ascension only by becoming a Mistborn himself, Emperor Elend Venture hopes to find clues left behind by the Lord Ruler that will allow him to save the world. Vin is consumed with guilt at having been tricked into releasing the mystic force known as Ruin from the Well. Ruin wants to end the world, and its near omniscience and ability to warp reality make stopping it seem impossible. Vin can't even discuss it with Elend lest Ruin learn their plans!