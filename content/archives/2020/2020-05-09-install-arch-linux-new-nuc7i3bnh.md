---
title: "🖥️ 🐧 Installing Arch Linux in my new NUC7i3BNH"
slug: "install-arch-linux-new-nuc7i3bnh"
author: "Paulo Pereira"
date: 2020-05-09T10:40:10+01:00
lastmod: 2020-05-09T10:40:10+01:00
cover: "/posts/2020/2020-05-09-install-arch-linux-new-nuc7i3bnh/arch-linux.png"
description: "Time to setup my new [NUC7i3BNH](/posts/2020/05/new-nuc7i3bnh/).\n

I will install Arch Linux with no window manager and it will act as my home server with Seafile, Plex, Sickchill, sabnzbd, etc.."
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - NUC
  - Seafile
  - sabnzbd
  - sickchill
  - plex
---

Time to setup my new [NUC7i3BNH](/posts/2020/05/new-nuc7i3bnh/).

I will install Arch Linux with no window manager and it will act as my home server with Seafile, Plex, Sickchill, sabnzbd, etc..

## Preparation

### Upgrade BIOS Firmware

1. Download the latest [BIOS](https://downloadcenter.intel.com/product/95066/Intel-NUC-Kit-NUC7i3BNH)
2. Copy the .bios file to a USB drive
3. Boot and enter the upgrade page with F7

### Burn ISO to USB pen drive

1. Download the [Arch Linux ISO](https://www.archlinux.org/download/)
2. Burn the iso in the usb pen drive.
```bash
lsblk
cd Downloads
sudo dd bs=4M if=archlinux-2020.05.01-x86_64.iso of=/dev/sdc status=progress oflag=sync
```

## Installation

Boot from the usb pen drive (setup the boot sequence entering the BIOS with F2).

### First steps and testing Internet connection

Make sure to use the correct keyboard layout (e.g.: for a portuguese layout; use the ? key for the - character).

```bash
loadkeys pt-latin9
```

Test the Internet connection.

```bash
ip link
dhcpcd
ping archlinux.org
```

Activate the ntp service.

```bash
timedatectl set-ntp true
```

Make sure the en_US.UTF-8 is uncommented.

```bash
nano /etc/locale.gen
locale-gen
```

### Create partitions

List the drives and use cgdisk to create the partitions.

```bash
lsblk
cgdisk /dev/nvme0n1
```

Partitions to create.

| # | Size  | Hexcode | Label    |
| - | ----- |-------- | -------- |
| 1 | 1G    | ef00    | boot_efi |
| 2 | 1G    | ef02    | boot     |
| 3 | 222G  | 8304    | root     |
| 4 | 8.9G  | 8200    | swap     |

Format the partitions.

```bash
mkfs.vfat /dev/nvme0n1p1
mkfs.ext4 /dev/nvme0n1p2
mkfs.ext4 /dev/nvme0n1p3
mkswap /dev/nvme0n1p4
swapon /dev/nvme0n1p4
```

### Mounting the partitions

```bash
mount /dev/nvme0n1p3 /mnt
mkdir -p /mnt/boot
mount /dev/nvme0n1p2 /mnt/boot
mkdir -p /mnt/boot/efi
mount /dev/nvme0n1p1 /mnt/boot/efi
```

### Install the base system

Choose a mirror near you.

```bash
nano /etc/pacman.d/mirrorlist
```

Copy your preferred mirror to the first line.

Tips:

* Alt+6 to copy a Server line.
* PageUp key to scroll up.
* Ctrl+U to paste it at the top of the list.
* Ctrl+X to exit, and when prompted to save changes, press Y and Enter.

Refresh the packages list and install the base system.

```bash
pacman -Syy
pacstrap /mnt base base-devel linux linux-firmware
```

Generate the fstab file.

```bash
genfstab -U /mnt >> /mnt/etc/fstab
```

### Configurations and boot manager

Change context to the base install mounted partitions.

```bash
arch-chroot /mnt
```

Install nano to edit files.

```bash
pacman -S nano
```

Define your local timezone.

```bash
ln -s /usr/share/zoneinfo/Europe/Lisbon /etc/localtime
```

Hardware clock.

```bash
hwclock --systohc --utc
```

Uncomment en_US.UTF-8 UTF-8 and generate the locale.

```bash
nano /etc/locale.gen
locale-gen
```

LANG and KEYMAP configurantion.

```bash
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=pt-latin9" >> /etc/vconsole.conf
```

Hosts and hostname configuration.

```bash
echo "myhostname" >> /etc/hostname
echo "127.0.0.1  localhost" >> /etc/hosts
echo "::1        localhost" >> /etc/hosts
echo "127.0.1.1  myhostname.localdomain  myhostname" >> /etc/hosts
```

Install and enable network services.

```bash
pacman -S networkmanager
systemctl enable NetworkManager.service
```

Useful to have sooner than later.

```bash
pacman -S git wget unzip openssh
systemctl enable sshd
```

Create an initial ramdisk environment.

```bash
mkinitcpio -P
```

Change your root password.

```bash
passwd
```

Install grub.

```bash
pacman -S grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
```

Install intel-ucode.

```bash
pacman -S intel-ucode
```

Unmount and reboot.

```bash
exit
umount /mnt/boot/efi
umount /mnt/boot
umount /mnt
reboot
```

Log in as root and create your user.

```bash
useradd -m -g users -s /bin/bash myuser
chfn myuser
passwd myuser
```

Create a new group to match your user.

```bash
groupadd mygroup
gpasswd -a myuser mygroup
```

Add your user as a sudoer.

```bash
pacman -S sudo vi
visudo /etc/sudoers
```
Add:
`myuser   ALL=(ALL) ALL`


## After installation

### Fix the IP in the router DHCPCD table

I like to always have the same IP address and tipically I do that at the router level.

### Authorized key to access from my desktop

On the server:

```bash
mkdir -p .ssh
```

On the desktop:

```bash
cat $HOME/.ssh/id_rsa.pub | ssh myuser@myhostname "cat >> .ssh/authorized_keys"
```

### Install yay (Yet another Yogurt)

```bash
mkdir -p downloads
cd downloads
wget https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz
tar xvf yay.tar.gz
cd yay
makepkg -sri
```

### fstab and my NAS

In my server I have NFS mounted directories to my NAS.

```bash
yay -S nfs-utils

sudo systemctl enable rpcbind.service
sudo systemctl start rpcbind.service
sudo systemctl status rpcbind.service

sudo systemctl enable nfs-client.target
sudo systemctl start nfs-client.target
sudo systemctl status nfs-client.target

sudo systemctl enable NetworkManager-wait-online.service
sudo systemctl start NetworkManager-wait-online.service
sudo systemctl status NetworkManager-wait-online.service

sudo mkdir -p /home/nas
sudo mkdir -p /home/nas/media
sudo mkdir -p /home/nas/downloads
```

I reviewed the mount options, mounted /tmp in memory and added the NFS mountpoints.

```text
UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX       /               ext4            noatime,discard,data=ordered,errors=remount-ro     0 1
UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX       /boot           ext4            noatime,discard,data=ordered,errors=remount-ro     0 2
UUID=XXXX-XXXX          /boot/efi       vfat            rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro  0 2
tmpfs   /tmp   tmpfs   nodev,nosuid,noatime,mode=1777   0 0
/swapfile none swap defaults 0 0
mynasserver:/nfs/media     /home/nas/media     nfs  users,noauto,x-systemd.automount,x-systemd.device-timeout=10,timeo=14,soft,intr,noatime 0 0
```

### rsync

yay -S rsync

### Apache

https://wiki.archlinux.org/index.php/Apache_HTTP_Server

I will tranfer all my configuration from the old server.

On the new server:

```bash
yay -S apache php-apache
```

On the old server:

```bash
sudo systemctl stop httpd
sudo systemctl disable httpd
sudo systemctl status httpd
sudo tar --xattrs -czpvf /home/nas/downloads/httpd.tar.gz /etc/httpd
```

On the new server:

```bash
sudo systemctl stop httpd

cd /
sudo tar -xvf /home/nas/downloads/httpd.tar.gz

sudo systemctl start httpd
sudo systemctl status httpd
sudo systemctl enable httpd
```

### Sabnzbd

https://wiki.archlinux.org/index.php/SABnzbd

I will tranfer all my configuration from the old server.

On the new server:

```bash
yay -S sabnzbd python2-pyopenssl p7zip unzip
```

On the old server:

```bash
sudo systemctl stop sabnzbd
sudo systemctl disable sabnzbd
sudo systemctl status sabnzbd
sudo tar --xattrs -czpvf /home/nas/downloads/sabnzbd.tar.gz /opt/sabnzbd
```

On the new server:

```bash
sudo systemctl stop sabnzbd

cd /
sudo tar -xvf /home/nas/downloads/sabnzbd.tar.gz

# find and replace the old server hostname for the new one
sudo vi /opt/sabnzbd/sabnzbd.ini

sudo systemctl start sabnzbd
sudo systemctl status sabnzbd
sudo systemctl enable sabnzbd
```

Check if everything is working: https://myhostname:9090/sabnzbd/

### Sickchill

I will tranfer all my configuration from the old server.

On the new server:

```bash
yay -S sickchill-git
```

On the old server:

```bash
sudo systemctl stop sickchill
sudo systemctl disable sickchill
sudo systemctl status sickchill
sudo tar --xattrs -czpvf /opt/sickchill.tar.gz /opt/sickchill
sudo scp /opt/sickchill.tar.gz root@myhostname:/home/myuser
sudo rm /opt/sickchill.tar.gz
```

On the new server:

```bash
sudo systemctl stop sickchill

cd /
sudo cp /home/myuser/sickchill.tar.gz .
sudo tar -xvf sickchill.tar.gz
sudo rm /home/myuser/sickchill.tar.gz
sudo rm sickchill.tar.gz

# find and replace the old server hostname for the new one
sudo vi /opt/sickchill/data/config.ini

sudo systemctl start sickchill
sudo systemctl status sickchill
sudo systemctl enable sickchill
```

Check if everything is working: http://myhostname:8081/home/

### Plex

I will tranfer all my configuration from the old server.

On the new server:

```bash
yay -S plex-media-server-plexpass
```

On the old server:

```bash
sudo systemctl stop plexmediaserver
sudo systemctl disable plexmediaserver
sudo systemctl status plexmediaserver
sudo tar --xattrs -czpvf /home/nas/downloads/plexmediaserver.tar.gz /usr/lib/plexmediaserver
sudo tar --xattrs -czpvf /home/nas/downloads/plex.tar.gz /var/lib/plex
```

On the new server:

```bash
sudo systemctl stop plexmediaserver

cd /
sudo tar -xvf /home/nas/downloads/plexmediaserver.tar.gz
sudo tar -xvf /home/nas/downloads/plex.tar.gz

sudo systemctl start plexmediaserver
sudo systemctl status plexmediaserver
sudo systemctl enable plexmediaserver
```

Reviewed the port forwarding in my router:

|                        |       |       |         |      |
| ---------------------- | ----- | ----- | ------- | ---- |
| Plex                   | 32400 | 32400 | x.x.x.x | BOTH |
| Plex DLNA Server       | 1900  | 1900  | x.x.x.x | UDP	|
| Plex DLNA Server (TCP) | 32469 | 32469 | x.x.x.x | TCP	|
| Plex GDM _ 1	         | 32410 | 32410 | x.x.x.x | UDP	|
| Plex GDM _ 2           | 32412 | 32412 | x.x.x.x | UDP	|
| Plex GDM _ 3	         | 32413 | 32413 | x.x.x.x | UDP	|
| Plex GDM _ 4	         | 32414 | 32414 | x.x.x.x | UDP  |

Check if everything is working: https://app.plex.tv/

### Seafile

https://www.seafile.com/en/download/

On the new server:

```bash
yay -S certbot-apache
```

On the old server:

```bash
sudo systemctl stop seahub
sudo systemctl stop seafile
sudo systemctl stop mysqld
sudo systemctl disable seahub
sudo systemctl disable seafile
sudo systemctl disable mysqld

sudo tar --xattrs -czpvf /home/nas/downloads/letsencrypt.tar.gz /etc/letsencrypt
sudo tar --xattrs -czpvf /home/nas/downloads/mysql.tar.gz /var/lib/mysql
sudo tar --xattrs -czpvf /home/nas/downloads/ccnet.tar.gz /srv/seafile/ccnet
sudo tar --xattrs -czpvf /home/nas/downloads/conf.tar.gz /srv/seafile/conf
#sudo tar --xattrs -czpvf /home/nas/downloads/pro-data.tar.gz /srv/seafile/pro-data
sudo tar --xattrs -czpvf /srv/seafile/seahub-data.tar.gz /srv/seafile/seahub-data

sudo scp -p /etc/systemd/system/seafile.service myuser@myhostname:/home/myuser/seafile.service
sudo scp -p /etc/systemd/system/seahub.service myuser@myhostname:/home/myuser/seahub.service
```

On the new server:

```bash
cd /
sudo tar -xvf /home/nas/downloads/letsencrypt.tar.gz
sudo certbot certificates

sudo useradd -m -r -d /srv/seafile -s /usr/bin/nologin seafile
sudo groupadd nogroup

sudo mkdir -p /home/nas/seafile-data
sudo chmod 750 /home/nas/seafile-data

sudo vi /etc/fstab
# add an entry for nas:/nfs/seafile-data
sudo shutdown -r now

sudo ls -lah /home/nas/seafile-data

yay -S python-imaging mysql-python libsearpc libsepol libselinux mariadb jdk8-openjdk python-pdftotext

sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

sudo systemctl stop mysqld

cd /
sudo tar -xvf /home/nas/downloads/mysql.tar.gz

sudo systemctl start mysqld
sudo systemctl status mysqld
sudo systemctl enable mysqld

mysql -u root -p

sudo -u seafile -s /bin/sh
cd /srv/seafile
mkdir -p seafile-data
wget "https://download.seafile.com/d/XXXXXXX/files/?p=/pro/seafile-pro-server_7.0.17_x86-64.tar.gz&dl=1" -O seafile-pro-server_7.0.17_x86-64.tar.gz
tar -xzf seafile-pro-server_*
mkdir installed
mv seafile-pro-server_*tar.gz installed
exit

cd /
sudo tar -xvf /home/nas/downloads/ccnet.tar.gz
sudo tar -xvf /home/nas/downloads/conf.tar.gz
#sudo tar -xvf /home/nas/downloads/pro-data.tar.gz
sudo tar -xvf /home/nas/downloads/seahub-data.tar.gz
sudo cp /home/myuser/seafile.service /etc/systemd/system/seafile.service
sudo cp /home/myuser/seahub.service /etc/systemd/system/seahub.service
sudo rm /home/myuser/seafile.service
sudo rm /home/myuser/seahub.service
sudo systemctl daemon-reload

sudo -u seafile -s /bin/sh
ln -s /home/nas/seafile-data /srv/seafile/seafile-data
ls ...
cd /srv/seafile
cd seafile-pro-server-7.0.17
upgrade/minor-upgrade.sh
ls ..
exit

sudo find /srv/seafile ! -user seafile -exec chown seafile:seafile {} \;
sudo find /home/nas/seafile-data ! -user seafile -exec chown seafile:seafile {} \;

sudo -u seafile -s /bin/sh
cd /srv/seafile
rm -rf pro-data/search
exit

sudo systemctl start seafile
sudo systemctl status seafile
sudo systemctl enable seafile
sudo systemctl start seahub
sudo systemctl status seahub
sudo systemctl enable seahub

sudo -u seafile -s /bin/sh
cd /srv/seafile/seafile-server-latest
./pro/pro.py search --update
```

Reviewed the port forwarding in my router:

|                   |      |      |         |      |
| ----------------- | ---- | ---- | ------- | ---- |
| seafile           | 8082 | 8082 | x.x.x.x | BOTH |
| seahub            | 8000 | 8000 | x.x.x.x | BOTH |
| seafile (Web)     | 80   | 80   | x.x.x.x | BOTH |
| seafile (Web SSL)	| 443  | 443  | x.x.x.x | BOTH |
| seafile (webdav)  | 8080 | 8080 | x.x.x.x | BOTH |
| seafdav	          | 8083 | 8083 | x.x.x.x | BOTH |

Check if everything is working: https://myseafileurl/
