---
title: "Summer Knight (The Dresden Files Book 4) by Jim Butcher (2002)"
slug: "summer-knight-the-dresden-files-book-4"
author: "Paulo Pereira"
date: 2022-12-23T18:00:00+00:00
lastmod: 2022-12-23T18:00:00+00:00
cover: "/posts/books/summer-knight-the-dresden-files-book-4.jpg"
description: "Finished “Summer Knight (The Dresden Files Book 4)” by Jim Butcher."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2002
book authors:
  - Jim Butcher
book series:
  - The Dresden Files Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jim Butcher
  - The Dresden Files Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Summer Knight”.
* Author: [Jim Butcher](/book-authors/jim-butcher)
* First Published: [September 3, 2002](/book-publication-year/2002)
* Series: [The Dresden Files](/book-series/the-dresden-files-series) Book 4
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6527661-summer-knight)
>
> Ever since his girlfriend left town to deal with her newly acquired taste for blood, Harry Dresden has been down and out in Chicago. He can’t pay his rent. He’s alienating his friends. He can’t even recall the last time he took a shower.
> 
> The only professional wizard in the phone book has become a desperate man.
> 
> And just when it seems things can’t get any worse, in saunters the Winter Queen of Faerie. She has an offer Harry can’t refuse if he wants to free himself of the supernatural hold his faerie godmother has over him — and hopefully end his run of bad luck. All he has to do is find out who murdered the Summer Queen’s right-hand man, the Summer Knight, and clear the Winter Queen’s name.
> 
> It seems simple enough, but Harry knows better than to get caught in the middle of faerie politics. Until he finds out that the fate of the entire world rests on his solving this case.