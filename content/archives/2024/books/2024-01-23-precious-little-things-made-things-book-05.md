---
title: "Precious Little Things (Made Things Book 0.5) by Adrian Tchaikovsky (2017)"
slug: "precious-little-things-made-things-book-05"
author: "Paulo Pereira"
date: 2024-01-23T22:30:00+00:00
lastmod: 2024-01-23T22:30:00+00:00
cover: "/posts/books/precious-little-things-made-things-book-05.jpg"
description: "Finished “Precious Little Things (Made Things Book 0.5)” by Adrian Tchaikovsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Adrian Tchaikovsky
book series:
  - Made Things Series
book genres:
  - Fiction
  - Fantasy
  - Short Stories
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Made Things Series
  - Fiction
  - Fantasy
  - Short Stories
  - 7/10 Books
  - Ebook
---

Finished “Precious Little Things”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
* First Published: [November 10, 2017](/book-publication-year/2017)
* Series: [Made Things](/book-series/made-things-series) Book 0.5
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/48994844-precious-little-things)
>
> A prequel to the magical novella Made Things.