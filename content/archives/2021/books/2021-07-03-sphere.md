---
title: "Sphere by Michael Crichton (1987)"
slug: "sphere"
author: "Paulo Pereira"
date: 2021-07-03T21:00:00+01:00
lastmod: 2021-07-03T21:00:00+01:00
cover: "/posts/books/sphere.jpg"
description: "Finished “Sphere” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Michael Crichton
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/sphere
---

Finished “Sphere”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [May 12th 1987](/book-publication-year/1980s)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/18739754-sphere)
>
> From the author of Jurassic Park, Timeline, and Congo comes a psychological thriller about a group of scientists who investigate a spaceship discovered on the ocean floor.
>
> In the middle of the South Pacific, a thousand feet below the surface, a huge vessel is unearthed. Rushed to the scene is a team of American scientists who descend together into the depths to investigate the astonishing discovery. What they find defies their imaginations and mocks their attempts at logical explanation. It is a spaceship, but apparently it is undamaged by its fall from the sky. And, most startling, it appears to be at least three hundred years old, containing a terrifying and destructive force that must be controlled at all costs.