---
title: "Originals: How Non-Conformists Move the World by Adam M. Grant (2016)"
slug: "originals"
author: "Paulo Pereira"
date: 2017-09-19T22:06:00+01:00
lastmod: 2017-09-19T22:06:00+01:00
cover: "/posts/books/originals.jpg"
description: "Finished “Originals: How Non-Conformists Move the World” by Adam M. Grant."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - Adam M. Grant
book series:
  - 
book genres:
  - Nonfiction
  - Business
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Adam M. Grant
  - Nonfiction
  - Business
  - 7/10 Books
  - Audiobook
---

Finished “Originals: How Non-Conformists Move the World”.
* Author: [Adam M. Grant](/book-authors/adam-m.-grant)
* First Published: [February 2nd 2016](/book-publication-year/2016)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34660083-originals)
>
> The #1 national bestseller and New York Times bestseller that examines how people can champion new ideas—and how leaders can fight groupthink
>
> With Give and Take, Adam Grant not only introduced a landmark new paradigm for success but also established himself as one of his generation’s most compelling and provocative thought leaders. In Originals he again addresses the challenge of improving the world, but now from the perspective of becoming original: choosing to champion novel ideas and values that go against the grain, battle conformity, and buck outdated traditions. How can we originate new ideas, policies, and practices without risking it all?
>
> Using surprising studies and stories spanning business, politics, sports, and entertainment, Grant explores how to recognize a good idea, speak up without getting silenced, build a coalition of allies, choose the right time to act, and manage fear and doubt; how parents and teachers can nurture originality in children; and how leaders can build cultures that welcome dissent. Learn from an entrepreneur who pitches his start-ups by highlighting the reasons not to invest, a woman at Apple who challenged Steve Jobs from three levels below, an analyst who overturned the rule of secrecy at the CIA, a billionaire financial wizard who fires employees for failing to criticize him, and a TV executive who didn’t even work in comedy but saved Seinfeld from the cutting-room floor. The payoff is a set of groundbreaking insights about rejecting conformity and improving the status quo.
>