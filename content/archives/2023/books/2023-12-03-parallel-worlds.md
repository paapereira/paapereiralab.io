---
title: "Parallel Worlds by Michio Kaku (2013)"
slug: "parallel-worlds"
author: "Paulo Pereira"
date: 2023-12-03T14:00:00+00:00
lastmod: 2023-12-03T14:00:00+00:00
cover: "/posts/books/parallel-worlds.jpg"
description: "Finished “Parallel Worlds” by Michio Kaku."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - Michio Kaku
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Michio Kaku
  - Nonfiction
  - Science
  - 7/10 Books
  - Audiobook
---

Finished “Parallel Worlds: A Journey Through Creation, Higher Dimensions, and the Future of the Cosmos”.
* Author: [Michio Kaku](/book-authors/michio-kaku)
* First Published: [June 18, 2013](/book-publication-year/2013)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/24852521-parallel-worlds)
>
> In this thrilling journey into the mysteries of our cosmos, best-selling author Michio Kaku takes us on a dizzying ride to explore black holes and time machines, multidimensional space, and most tantalizing of all, the possibility that parallel universes may exist alongside our own.
> 
> Kaku skillfully guides us through the latest innovations in string theory and its latest iteration, M-theory, which posits that our universe may be just one in an endless multiverse, a singular bubble floating in a sea of infinite bubble universes. If M-theory is proven correct, we may perhaps finally find an answer to the question, "What happened before the big bang?"
> 
> This is an exciting and unforgettable introduction into the new cutting-edge theories of physics and cosmology from one of the pre-eminent voices in the field.