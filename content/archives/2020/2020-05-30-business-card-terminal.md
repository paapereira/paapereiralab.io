---
title: "curl-based business card"
slug: "business_card_terminal"
author: "Paulo Pereira"
date: 2020-05-30T15:32:37+01:00
lastmod: 2020-05-30T15:32:37+01:00
cover: "/posts/card.png"
description: "No need to explain the why. It's your business card... in the terminal... served via curl..."
draft: false
toc: false
categories:
  - General
tags:
  - Terminal
aliases:
  - /posts/card/
---

No need to explain the why. It's your business card... in the terminal... served via curl...

Inspiration here: https://www.youtube.com/watch?v=KlDVXHAjJSg

What I did:

1. Created a snippet in [GitLab](https://gitlab.com/dashboard/snippets)
2. Made it Public
3. Tried it in the terminal
```bash
curl -sL https://gitlab.com/snippets/1981845/raw
```
4. Created a custom redirection in my domain for the snippet
```bash
curl -sL card.paapereira.xyz
```
5. Done