---
title: "Books Schedule update"
slug: "book-schedule-october-2024"
author: "Paulo Pereira"
date: 2024-10-04T20:30:00+01:00
lastmod: 2024-10-04T20:30:00+01:00
description: "October Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my October TBR (To Be Read) update for the next months.

* **[Read]** The Expert System's Brother (The Expert System's Brother #1) by Adrian Tchaikovsky
* **[Read]** Snow Crash by Neal Stephenson
* **[Read]** Tress of the Emerald Sea by Brandon Sanderson
* **[Reading]** The New Girl (Fear Street #1) by R. L. Stine
* The Surrogate Mother by Freida McFadden
* My Best Friend's Exorcism by Grady Hendrix
* The Curse of the Mummy's Tomb (Goosebumps #5) by R. L. Stine
* Horns by Joe Hill
* The Spoils of War (The Damned #3) by Alan Dean Foster 
* Hawksbill Station by Robert Silverberg
* Harry Potter and the Chamber of Secrets (Harry Potter #2) by J. K. Rowling
* Star Wars: Episode VI: Return of the Jedi by James Kahn
* Small Favor (The Dresden Files #10) by Jim Butcher
* The Subtle Art of Not Giving a F*ck by Mark Manson
* The Andromeda Evolution (Andromeda #2) by Daniel H. Wilson & Michael Crichton
* Mystery Walk by Robert R. McCammon
* Way Station by Clifford D. Simak
* Aurora Rising (Prefect Dreyfus Emergency #1) by Alastair Reynolds
* The Sicilian (The Godfather #2) by Mario Puzo
* Planet of Exile (Hainish Cycle) by Ursula K. le Guin
* We Can Build You by Philip K. Dick
* A Coming of Age by Timothy Zahn

I also have always an audiobook going too:

* **[Reading]** Desperation by Stephen King
* Not Till We Are Lost (Bobiverse #5) by Dennis E. Taylor
* Nexus by Yuval Noah Harari
* Breakfast at Tiffany's by Truman Capote
* Red Company: Invasion (Red Company #4) by B. V. Larson
* The Richest Man in Babylon by George S. Clason
* Impact Winter #1 by Travis Beacham
* A History of Video Games by Jeremy Parish
* The Sandman: Act III by Neil Gaiman
