---
title: "Book Haul for October 2024"
slug: "book-haul-2024-10"
author: "Paulo Pereira"
date: 2024-11-01T16:00:00+00:00
lastmod: 2024-11-01T16:00:00+00:00
cover: "/posts/book-hauls/2024-10-book-haul.png"
description: "October book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

October book haul.