---
title: "B3 online again"
slug: "b3-online-again"
author: "Paulo Pereira"
date: 2011-05-08T11:59:00+01:00
lastmod: 2011-05-08T11:59:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
---

So, my B3 is online again. After is [breakdown](/posts/create-usb-recovery-disk-for-booting-b3) last week I can finally rest (and reinstall/reconfigure everything again).

The problem was the hard drive. I never seen a hard drive so  damaged. Luckily I managed to backup all my data. Check my previous post on how to [create an usb recovery disk for booting b3 server](/posts/create-usb-recovery-disk-for-booting-b3) so you can move your files elsewhere.