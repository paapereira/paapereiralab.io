---
title: "The Lost World (Jurassic Park Book 2) by Michael Crichton (1995)"
slug: "the-lost-world-jurassic-park-book-2"
author: "Paulo Pereira"
date: 2022-07-16T21:00:00+01:00
lastmod: 2022-07-16T21:00:00+01:00
cover: "/posts/books/the-lost-world-jurassic-park-book-2.jpg"
description: "Finished “The Lost World (Jurassic Park Book 2)” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Michael Crichton
book series:
  - Jurassic Park Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Jurassic Park Series
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
---

Finished “The Lost World”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [September 17th 1995](/book-publication-year/1990s)
* Series: [Jurassic Park](/book-series/jurassic-park-series) Book 2
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6321283-the-lost-world)
>
> It is now six years since the secret disaster at Jurassic Park, six years since the extraordinary dream of science and imagination came to a crashing end—the dinosaurs destroyed, the park dismantled, and the island indefinitely closed to the public.
> 
> There are rumors that something has survived. . .