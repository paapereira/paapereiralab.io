---
title: "Star Wars Episode V: The Empire Strikes Back by Donald F. Glut (1980)"
slug: "star-wars-episode-v-the-empire-strikes-back"
author: "Paulo Pereira"
date: 2024-06-06T21:00:00+01:00
lastmod: 2024-06-06T21:00:00+01:00
cover: "/posts/books/star-wars-episode-v-the-empire-strikes-back.jpg"
description: "Finished “Star Wars Episode V: The Empire Strikes Back” by Donald F. Glut."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Donald F. Glut
book series:
  - Star Wars Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Donald F. Glut
  - Star Wars Universe
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “Star Wars Episode V: The Empire Strikes Back”.
* Author: [Donald F. Glut](/book-authors/donald-f.-glut)
* First Published: [May 1, 1980](/book-publication-year/1980s)
* Series: [Star Wars Universe](/book-series/star-wars-universe)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12171101-star-wars-episode-v)
>
> Although they had won a significant battle, the war between the Rebels and the Empire had really just begun. Soon, Luke, Han, the princess and their faithful companions were forced to flee, scattering in all directions…the Dark Lord's minions in fevered pursuit…