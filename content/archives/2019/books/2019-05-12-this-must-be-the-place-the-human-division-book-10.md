---
title: "This Must Be the Place (The Human Division Book 10) by John Scalzi (2013)"
slug: "this-must-be-the-place-the-human-division-book-10"
author: "Paulo Pereira"
date: 2019-05-12T22:09:00+01:00
lastmod: 2019-05-12T22:09:00+01:00
cover: "/posts/books/this-must-be-the-place-the-human-division-book-10.png"
description: "Finished “This Must Be the Place (The Human Division Book 10)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “This Must Be the Place”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [March 19th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 10
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17727169-this-must-be-the-place)
>
> Colonial Union diplomat Hart Schmidt is back home for Harvest Day celebrations - to a family whose members wonder whether its youngest son isn’t wasting his life clinging to the lowest rung of the CU’s diplomatic ladder. When his father, a legendarily powerful politician, presents him with a compelling offer, Schmidt has to take stock of his life and career.