---
title: "The Darkwater Bride by Marty Ross (2018)"
slug: "the-darkwater-bride"
author: "Paulo Pereira"
date: 2020-06-09T21:50:00+01:00
lastmod: 2020-06-09T21:50:00+01:00
cover: "/posts/books/the-darkwater-bride.jpg"
description: "Finished “The Darkwater Bride” by Marty Ross."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Marty Ross
book series:
  - 
book genres:
  - Fiction
  - Mystery
  - Horror
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Marty Ross
  - Fiction
  - Mystery
  - Horror
  - 5/10 Books
  - Audiobook
---

Finished “The Darkwater Bride”.
* Author: [Marty Ross](/book-authors/marty-ross)
* First Published: [September 29th 2018](/book-publication-year/2018)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/42603122-the-darkwater-bride)
>
> Late Victorian London. When the most respectable of Scottish businessmen is pulled, dead, from the Thames, his daughter is drawn into an investigation which reveals a whole world of secrets and corruption and leads all the way to the tragic truth behind the ghostly legend of The Darkwater Bride.
>
> An epic drama combining the genres of the Victorian mystery thriller with the equally classic Victorian mode of the ghostly tale.