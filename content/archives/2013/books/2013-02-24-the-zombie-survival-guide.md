---
title: "The Zombie Survival Guide by Max Brooks (2003)"
slug: "the-zombie-survival-guide"
author: "Paulo Pereira"
date: 2013-02-24T23:57:00+00:00
lastmod: 2013-02-24T23:57:00+00:00
cover: "/posts/books/the-zombie-survival-guide.jpg"
description: "Finished “The Zombie Survival Guide: Complete Protection from the Living Dead” by Max Brooks."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2003
book authors:
  - Max Brooks
book series:
  - 
book genres:
  - Horror
  - Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Max Brooks
  - Horror
  - Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Zombie Survival Guide: Complete Protection from the Living Dead”.
* Author: [Max Brooks](/book-authors/max-brooks)
* First Published: [September 1st 2003](/book-publication-year/2003)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/535441.The_Zombie_Survival_Guide)
>
> The Zombie Survival Guide is your key to survival against the hordes of undead who may be stalking you right now. Fully illustrated and exhaustively comprehensive, this book covers everything you need to know, including how to understand zombie physiology and behavior, the most effective defense tactics and weaponry, ways to outfit your home for a long siege, and how to survive and adapt in any territory or terrain.
>
> Top 10 Lessons for Surviving a Zombie Attack
>
> 1. Organize before they rise!
> 2. They feel no fear, why should you?
> 3. Use your head: cut off theirs.
> 4. Blades don’t need reloading.
> 5. Ideal protection = tight clothes, short hair.
> 6. Get up the staircase, then destroy it.
> 7. Get out of the car, get onto the bike.
> 8. Keep moving, keep low, keep quiet, keep alert!
> 9. No place is safe, only safer.
> 10. The zombie may be gone, but the threat lives on.
>
> Don’t be carefree and foolish with your most precious asset—life. This book is your key to survival against the hordes of undead who may be stalking you right now without your even knowing it. The Zombie Survival Guide offers complete protection through trusted, proven tips for safeguarding yourself and your loved ones against the living dead. It is a book that can save your life.