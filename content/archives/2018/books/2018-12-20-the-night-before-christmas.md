---
title: "The Night Before Christmas by Clement C. Moore (1823)"
slug: "the-night-before-christmas"
author: "Paulo Pereira"
date: 2018-12-20T19:22:00+00:00
lastmod: 2018-12-20T19:22:00+00:00
cover: "/posts/books/the-night-before-christmas.jpg"
description: "Finished “The Night Before Christmas” by Clement C. Moore."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - Clement C. Moore
book series:
  - 
book genres:
  - Fiction
  - Classics
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Clement C. Moore
  - Fiction
  - Classics
  - 5/10 Books
  - Audiobook
---

Finished “The Night Before Christmas”.
* Author: [Clement C. Moore](/book-authors/clement-c.-moore)
* First Published: [1823](/book-publication-year/1800s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43267491-the-night-before-christmas)
>
> Rascal Flatts performs a famous Christmas poem for patients at a Nashville children's hospital - and the result is pure magic.
>
> For over a decade, country music superstars Rascal Flatts have built a special relationship with the kids at Monroe Carell Jr. Children's Hospital at Vanderbilt through annual performances and fund-raisers.
>
> In 2018 they poured their hearts into a reading of Clement C. Moore's iconic holiday classic "A Visit from St. Nicholas" for their most cherished audience. The Night Before Christmas audiobook builds on that performance to spread holiday cheer beyond Nashville, as an accompaniment to yuletide festivities anywhere or as the perfect story to encourage "visions of sugar-plums" (a quote from the poem!) at bedtime.
>
> If you've ever quizzed your family and friends about the names of Santa's reindeer, you were quoting from "A Visit from St. Nicholas", and you haven't heard the phrase "bowlful of jelly" until you've heard it from Gary, Joe Don, and Jay! Their funny and tender voices will inspire people to believe in the healing power of art at Christmas and throughout the year.