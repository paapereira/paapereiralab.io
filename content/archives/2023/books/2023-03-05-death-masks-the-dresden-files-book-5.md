---
title: "Death Masks (The Dresden Files Book 5) by Jim Butcher (2003)"
slug: "death-masks-the-dresden-files-book-5"
author: "Paulo Pereira"
date: 2023-03-05T21:00:00+00:00
lastmod: 2023-03-05T21:00:00+00:00
cover: "/posts/books/death-masks-the-dresden-files-book-5.jpg"
description: "Finished “Death Masks (The Dresden Files Book 5)” by Jim Butcher."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2003
book authors:
  - Jim Butcher
book series:
  - The Dresden Files Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jim Butcher
  - The Dresden Files Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Death Masks”.
* Author: [Jim Butcher](/book-authors/jim-butcher)
* First Published: [August 1, 2003](/book-publication-year/2003)
* Series: [The Dresden Files](/book-series/the-dresden-files-series) Book 5
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8680045-death-masks)
>
> Harry Dresden, Chicago's only practicing professional wizard, should be happy that business is pretty good for a change. But now he's getting more than he bargained for:
> 
> A duel with the Red Court of Vampires' champion, who must kill Harry to end the war between vampires and wizards...
> 
> Professional hit men using Harry for target practice...
> 
> The missing Shroud of Turin...
> 
> A handless and headless corpse the Chicago police need identified...
> 
> Not to mention the return of Harry's ex-girlfriend Susan, who's still struggling with her semi-vampiric nature. And who seems to have a new man in her life.
> 
> Some days, it just doesn't pay to get out of bed. No matter how much you're charging.