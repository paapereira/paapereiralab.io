---
title: "The Tooth Fairy by Graham Joyce (1996)"
slug: "the-tooth-fairy"
author: "Paulo Pereira"
date: 2023-06-27T21:00:00+01:00
lastmod: 2023-06-27T21:00:00+01:00
cover: "/posts/books/the-tooth-fairy.jpg"
description: "Finished “The Tooth Fairy” by Graham Joyce."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Graham Joyce
book series:
  - 
book genres:
  - Fiction
  - Fantasy
  - Horror
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Graham Joyce
  - Fiction
  - Fantasy
  - Horror
  - 8/10 Books
  - Ebook
aliases:
  - /posts/books/the-tooth-fairy
---

Finished “The Tooth Fairy”.
* Author: [Graham Joyce](/book-authors/graham-joyce)
* First Published: [January 1, 1996](/book-publication-year/1990s)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/16110395-the-tooth-fairy)
>
> Sam and his friends are like any normal gang of normal young boys—roaming wild around the outskirts of their car-factory town, daring adults to challenge their freedom. Then one day Sam wakes to find the tooth fairy sitting on the edge of his bed—but this is not the benign figure of childhood myth. This is an enigmatic presence that both torments and seduces him, changing his life forever.