---
title: "Ready Player Two (Ready Player One Book 2) by Ernest Cline (2020)"
slug: "ready-player-two-ready-player-one-book-2"
author: "Paulo Pereira"
date: 2020-12-20T22:04:00+00:00
lastmod: 2020-12-20T22:04:00+00:00
cover: "/posts/books/ready-player-two-ready-player-one-book-2.jpg"
description: "Finished “Ready Player Two (Ready Player One Book 2)” by Ernest Cline."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Ernest Cline
book series:
  - Ready Player One Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Ernest Cline
  - Ready Player One Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
aliases:
  - /posts/books/ready-player-two-ready-player-one-book-2
---

Finished “Ready Player Two”.
* Author: [Ernest Cline](/book-authors/ernest-cline)
* First Published: [November 24th 2020](/book-publication-year/2020)
* Series: [Ready Player One](/book-series/ready-player-one-series) Book 2
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/54447940-ready-player-two)
>
> An unexpected quest. Two worlds at stake. Are you ready?
>
> Days after Oasis founder James Halliday's contest, Wade Watts makes a discovery that changes everything. Hidden within Halliday's vault, waiting for his heir to find, lies a technological advancement that will once again change the world and make the Oasis thousand times more wondrous, and addictive, than even Wade dreamed possible. With it comes a new riddle and a new quest. The last Easter egg from Halliday, hinting at a mysterious prize. And an unexpected, impossibly powerful, and dangerous new rival awaits, one who will kill millions to get what he wants. Wade's life and the future of the Oasis are again at stake, but this time the fate of humanity also hangs in the balance.