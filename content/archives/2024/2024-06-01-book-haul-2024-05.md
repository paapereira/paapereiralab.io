---
title: "Book Haul for May 2024"
slug: "book-haul-2024-05"
author: "Paulo Pereira"
date: 2024-06-01T15:00:00+01:00
lastmod: 2024-06-01T15:00:00+01:00
cover: "/posts/book-hauls/2024-05-book-haul.png"
description: "May book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

May book haul (with early birthday haul).