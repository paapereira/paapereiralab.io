---
title: "Pet Sematary by Stephen King (1983)"
slug: "pet-sematary"
author: "Paulo Pereira"
date: 2018-11-17T22:59:00+00:00
lastmod: 2018-11-17T22:59:00+00:00
cover: "/posts/books/pet-sematary.jpg"
description: "Finished “Pet Sematary” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - 9/10 Books
  - Audiobook
---

Finished “Pet Sematary”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [November 14th 1983](/book-publication-year/1980s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/39651575-pet-sematary)
>
> Never before available on unabridged audio! Dexter's Michael C. Hall reads this classic horror story described by Publishers Weekly as the "most frightening novel Stephen King has ever written".
>
> When Dr. Louis Creed takes a new job and moves his family to the idyllic, rural town of Ludlow, Maine, this new beginning seems too good to be true. Yet despite Ludlow's tranquility, there's an undercurrent of danger that lingers...like the graveyard in the woods near the Creeds' home, where generations of children have buried their beloved pets.
>
> Behind the "pet sematary", there is another burial ground, one that lures people to it with seductive promises...and ungodly temptations. A blood-chilling truth is hidden there - one more terrifying than death itself and hideously more powerful. An ominous fate befalls anyone who dares tamper with this forbidden place, as Louis is about to discover for himself....