---
title: "LibreWolf - A fork of Firefox"
slug: "librewolf"
author: "Paulo Pereira"
date: 2021-01-16T18:33:53+00:00
lastmod: 2021-02-13T16:23:43+00:00
cover: "/posts/2021/2021-01-16-librewolf/librewolf.png"
description: "*[LibreWolf](https://librewolf-community.gitlab.io/) is a fork of Firefox, focused on privacy, security and freedom.*\n

`paru -S librewolf-bin`"
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - LibreWolf
aliases:
  - /posts/librewolf/
---

*[LibreWolf](https://librewolf-community.gitlab.io/) is a fork of Firefox, focused on privacy, security and freedom.*

I've been trying it out and so far so good. It's basically Firefox, but with out of the box enhanced security and without Mozilla telemetries, Pockets and stuffs.

## Installing

```bash
paru -S librewolf-bin
```

## Set LibreWolf as the default browser

I'm using i3-gaps. 

I need to change my `.profile`.

```bash
vi .profile
```
```text
export BROWSER="/usr/bin/librewolf"
```

And replace `firefox` with `librewolf` in the `mimeapps.list` file.

```bash
vi .config/mimeapps.list
```

## Using Profile-sync-daemon

*[profile-sync-daemon](https://wiki.archlinux.org/index.php/profile-sync-daemon) (psd) is a tiny pseudo-daemon designed to manage browser profile(s) in tmpfs and to periodically sync back to the physical disc.*

__>>>> UPDATE: Check an updated setup of psd [here](/posts/profile-sync-daemon-librewolf/).__

I've been using psd so improve Firefox performance. It doesn't come with out of the box support for LibreWolf, but you can use the Firefox config as a dropin to LibreWolf.

Just replace `.mozilla/firefox` with `.librewolf` in `/usr/share/psd/browsers/firefox`.

```bash
sudo cp /usr/share/psd/browsers/firefox /usr/share/psd/browsers/firefox_bck
sudo vi /usr/share/psd/browsers/firefox
```

And restart `psd.service`.

```bash
systemctl --user restart psd.service
```

You can check if it's pointing to the right direction with:

```bash
psd p
```

Check the [Arch Linux Wiki](https://wiki.archlinux.org/index.php/profile-sync-daemon) for more about psd.