---
title: "The Gospel According to Jesus Christ by José Saramago (1991)"
slug: "the-gospel-according-to-jesus-christ"
author: "Paulo Pereira"
date: 2022-02-01T22:00:00+00:00
lastmod: 2022-02-01T22:00:00+00:00
cover: "/posts/books/the-gospel-according-to-jesus-christ.jpg"
description: "Finished “The Gospel According to Jesus Christ” by José Saramago."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - José Saramago
book series:
  - 
book genres:
  - Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - José Saramago
  - Fiction
  - 6/10 Books
  - Audiobook
---

Finished “The Gospel According to Jesus Christ”.
* Author: [José Saramago](/book-authors/josé-saramago)
* First Published: [1991](/book-publication-year/1990s)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25115580-the-gospel-according-to-jesus-christ)
>
> This is a skeptic' s journey into the meaning of God and of human existence. At once an ironic rendering of the life of Christ and a beautiful novel, Saramago' s tale has sparked intense discussion about the meaning of Christianity and the Church as an institution.