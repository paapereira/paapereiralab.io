---
title: "Demon Star (Star Force Book 12) by B.V. Larson and David VanDyke (2015)"
slug: "demon-star-star-force-book-12"
author: "Paulo Pereira"
date: 2016-12-13T22:52:00+00:00
lastmod: 2016-12-13T22:52:00+00:00
cover: "/posts/books/demon-star-star-force-book-12.jpg"
description: "Finished “Demon Star (Star Force Book 12)” by B.V. Larson and David VanDyke."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - B.V. Larson
  - David VanDyke
book series:
  - Star Force Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - David VanDyke
  - Star Force Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Demon Star”.
* Author: [B.V. Larson](/book-authors/b.v.-larson) and [David VanDyke](/book-authors/david-vandyke)
* First Published: [August 3rd 2015](/book-publication-year/2015)
* Series: [Star Force](/book-series/star-force-series) Book 12
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28242354-demon-star)
>
> Three stars, three worlds, three civilizations…one massive war.
>
> In a triple star system, not all planets are equal. When the DEMON STAR swings closest in her orbit, the Insectoids always invade. They raid the inner worlds with grim regularity, riding hordes of stealthed ships in ever growing waves. Their mission is to destroy all opposition—alien, human or otherwise.
>
> Cody Riggs, lost among an endless chain of interstellar rings, flies his task force of starships into the middle of an eternal struggle for survival between three civilizations. He takes it upon himself to help the more peaceful worlds, but only manages to bring disaster to all humanity.
>
> DEMON STAR is the twelfth book of the Star Force series, a novel of military science fiction by bestselling authors B. V. Larson and David VanDyke. 