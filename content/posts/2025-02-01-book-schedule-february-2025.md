---
title: "Books Schedule update"
slug: "book-schedule-february-2025"
author: "Paulo Pereira"
date: 2025-02-01T15:30:00+00:00
lastmod: 2025-02-01T15:30:00+00:00
description: "February Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my February TBR (To Be Read) update for the next months.

* **[Read]** Aurora Rising (Prefect Dreyfus Emergency #1) by Alastair Reynolds
* **[Read]** The Sicilian (The Godfather #2) by Mario Puzo
* **[Read]** The Andromeda Evolution (Andromeda #2) by Daniel H. Wilson & Michael Crichton
* **[Read]** Planet of Exile (Hainish Cycle) by Ursula K. le Guin
* **[Read]** We Can Build You by Philip K. Dick
* **[Reading]** A Coming of Age by Timothy Zahn
* Dead Med (Prescription: Murder #1) by Freida McFadden
* The Expert System's Champion (The Expert System's Brother #2) by Adrian Tchaikovsky
* The Way of Kings (The Stormlight Archive #1) by Brandon Sanderson
* The Bullet Journal Method by Ryder Carroll
* Harry Potter and the Prisoner of Azkaban (Harry Potter #3) by J. K. Rowling
* Turn Coat (The Dresden Files #11) by Jim Butcher
* Resurrection, Inc. by Kevin J. Anderson
* Revan (Star Wars: The Old Republic #1) by Drew Karpyshyn
* Usher's Passing by Robert R. McCammon
* Memorial do Convento by José Saramago
* Elysium Fire (Prefect Dreyfus Emergency #2) by Alastair Reynolds
* A Case of Need by Michael Crichton & Jeffery Hudson
* City of Illusions (Hainish Cycle) by Ursula K. le Guin
* The Man in the Maze by Robert Silverberg

I also have always an audiobook going too:

* **[Read]** Nexus by Yuval Noah Harari
* **[Reading]** Red Company: Invasion (Red Company #4) by B. V. Larson
* The Richest Man in Babylon by George S. Clason
* Bag of Bones by Stephen King
* Impact Winter #1 by Travis Beacham
* A History of Video Games by Jeremy Parish
* The Sandman: Act III by Neil Gaiman

