---
title: "Inferno (Robert Langdon Book 4) by Dan Brown (2013)"
slug: "inferno-robert-langdon-book-4"
author: "Paulo Pereira"
date: 2013-12-09T23:38:00+00:00
lastmod: 2013-12-09T23:38:00+00:00
cover: "/posts/books/inferno-robert-langdon-book-4.jpg"
description: "Finished “Inferno (Robert Langdon Book 4)” by Dan Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - Dan Brown
book series:
  - Robert Langdon Series
book genres:
  - Fiction
  - Thriller
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Dan Brown
  - Robert Langdon Series
  - Fiction
  - Thriller
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “Inferno”.
* Author: [Dan Brown](/book-authors/dan-brown)
* First Published: [May 14th 2013](/book-publication-year/2013)
* Series: [Robert Langdon](/book-series/robert-langdon-series) Book 4
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17935625-inferno)
>
> Harvard professor of symbology Robert Langdon awakens in an Italian hospital, disoriented and with no recollection of the past thirty-six hours, including the origin of the macabre object hidden in his belongings. With a relentless female assassin trailing them through Florence, he and his resourceful doctor, Sienna Brooks, are forced to flee. Embarking on a harrowing journey, they must unravel a series of codes, which are the work of a brilliant scientist whose obsession with the end of the world is matched only by his passion for one of the most influential masterpieces ever written, Dante Alighieri's The Inferno.