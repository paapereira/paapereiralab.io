---
title: "Books Schedule update"
slug: "book-schedule-august-2023"
author: "Paulo Pereira"
date: 2023-07-30T14:00:00+01:00
lastmod: 2023-07-30T14:00:00+01:00
description: "August Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my August TBR (To Be Read) update for the next months.

* **[Read]** Chasm City by Alastair Reynolds
* **[Reading]** Next by Michael Crichton
* Building a Second Brain by Tiago Forte
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson
* How It Unfolds (Far Reaches 1) by James S. A. Corey
* Void (Far Reaches 2) by Veronica Roth
* The Private Life of Elder Things by Adrian Tchaikovsky & Keris McDonald & Adam Gauntlett
* Lost Stars by Claudia Gray
* Falling Bodies (Far Reaches 3) by Rebecca Roanhorse
* The Long Game (Far Reaches 4) by Ann Leckie
* Light Bringer (Red Rising Saga 6) by Pierce Brown
* Just Out of Jupiter's Reach (Far Reaches 5) by Nnedi Okorafor
* Slow Time Between the Stars (Far Reaches 6) by John Scalzi
* Bethany's Sin by Robert R. McCammon
* Dead Beat (The Dresden Files 7) by Jim Butcher
* Wool (Silo 1) by Hugh Howey

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson

I also have always an audiobook going too:

* **[Read]** Insomnia by Stephen King
* **[Read]** Fire Fight (Star Runner 2) by B. V. Larson
* **[Read]** How to Avoid a Climate Disaster by Bill Gates
* **[Reading]** Hearts in Atlantis by Stephen King
* Androids and Aliens (Star Runner 3) by B. V. Larson
* A World Without Email by Cal Newport
* Level Six (Killday 2) by William Ledbetter
* The Ocean at the End of the Lane by Neil Gaiman