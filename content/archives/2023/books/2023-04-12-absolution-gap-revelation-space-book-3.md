---
title: "Absolution Gap (Revelation Space Book 3) by Alastair Reynolds (2020)"
slug: "absolution-gap-revelation-space-book-3"
author: "Paulo Pereira"
date: 2023-04-12T21:00:00+01:00
lastmod: 2023-04-12T21:00:00+01:00
cover: "/posts/books/absolution-gap-revelation-space-book-3.jpg"
description: "Finished “Absolution Gap (Revelation Space Book 3)” by Alastair Reynolds."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Alastair Reynolds
book series:
  - Revelation Space Series
  - Revelation Space Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Alastair Reynolds
  - Revelation Space Series
  - Revelation Space Universe
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
---

Finished “Absolution Gap”.
* Author: [Alastair Reynolds](/book-authors/alastair-reynolds)
* First Published: [April 21, 2020](/book-publication-year/2020)
* Series: [Revelation Space](/book-series/revelation-space-series) Book 3
* [Revelation Space Universe](/book-series/revelation-space-universe)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/50521624-absolution-gap)
>
> The Inhibitors were designed to eliminate any life form reaching a certain level of intelligence -- and they've targeted Humanity. War veteran Clavain and a ragtag group of refugees have fled into hiding. Their leadership is faltering, and their situation is growing more desperate. But their little colony has just received an unexpected visitor: an avenging angel with the power to lead mankind to safety -- or draw down its darkest enemy.
> 
> And as she leads them to an apparently insignificant moon light-years away, it begins to dawn on Clavain and his companions that to beat one enemy, it may be necessary to forge an alliance with something much worse . . .