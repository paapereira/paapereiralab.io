---
title: "Annihilation (Southern Reach Book 1) by Jeff VanderMeer (2014)"
slug: "annihilation-southern-reach-book-1"
author: "Paulo Pereira"
date: 2020-01-31T23:33:00+00:00
lastmod: 2020-01-31T23:33:00+00:00
cover: "/posts/books/annihilation-southern-reach-book-1.png"
description: "Finished “Annihilation (Southern Reach Book 1)” by Jeff VanderMeer."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - Jeff VanderMeer
book series:
  - Southern Reach Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Jeff VanderMeer
  - Southern Reach Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
aliases:
  - /posts/books/annihilation-southern-reach-book-1
---

Finished “Annihilation”.
* Author: [Jeff VanderMeer](/book-authors/jeff-vandermeer)
* First Published: [February 4th 2014](/book-publication-year/2014)
* Series: [Southern Reach](/book-series/southern-reach-series) Book 1
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28573689-annihilation)
>
> Area X has been cut off from the rest of the continent for decades. Nature has reclaimed the last vestiges of human civilization. The first expedition returned with reports of a pristine, Edenic landscape; all the members of the second expedition committed suicide; the third expedition died in a hail of gunfire as its members turned on one another; the members of the eleventh expedition returned as shadows of their former selves, and within months of their return, all had died of aggressive cancer.
>
> This is the twelfth expedition.
>
> Their group is made up of four women: an anthropologist; a surveyor; a psychologist, the de facto leader; and our narrator, a biologist. Their mission is to map the terrain and collect specimens; to record all their observations, scientific and otherwise, of their surroundings and of one another; and, above all, to avoid being contaminated by Area X itself.
>
> They arrive expecting the unexpected, and Area X delivers—they discover a massive topographic anomaly and life forms that surpass understanding—but it’s the surprises that came across the border with them, and the secrets the expedition members are keeping from one another, that change everything.
>