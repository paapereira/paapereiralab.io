---
title: "Firefox Tip: Opening Search results in a new tab"
slug: "firefox-tip-opening-search-results"
author: "Paulo Pereira"
date: 2008-04-21T19:11:00+01:00
lastmod: 2008-04-21T19:11:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Firefox
---

Here's a useful tip to always open your Firefox search results in a new tab:

- In the address bar type *about:config*
- Look for *browser.search.openintab*
- Double click the *False* value (in the Value column) to change it to *True*
- Now try to search something in your favorite search engine

*PS:* this works in every OS (at least Linux and Windows).