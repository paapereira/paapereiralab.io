---
title: "Stock Investing For Beginners by John Roberts (2017)"
slug: "stock-investing-for-beginners"
author: "Paulo Pereira"
date: 2022-08-16T10:00:00+01:00
lastmod: 2022-08-16T10:00:00+01:00
cover: "/posts/books/stock-investing-for-beginners.jpg"
description: "Finished “Stock Investing For Beginners” by John Roberts."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - John Roberts
book series:
  - 
book genres:
  - Nonfiction
  - Self Help
book scores:
  - 7/10 Books
tags:
  - Book Log
  - John Roberts
  - Nonfiction
  - Self Help
  - 7/10 Books
  - Ebook
---

Finished “Stock Investing For Beginners: How To Buy Your First Stock And Grow Your Money”.
* Author: [John Roberts](/book-authors/john-roberts)
* First Published: [March 18, 2017](/book-publication-year/2017)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34683888-stock-investing-for-beginners)
>
> A former janitor and gas station attendant in Vermont, who died a few years ago, surprised everyone by leaving an $8 million fortune to his local library and hospital.
> 
> What was his secret, everyone wondered? And the answer turned out to be pretty basic. Because, besides being industrious and frugal, which you may have guessed, he had invested in the stock market throughout the years.
> 
> This is actually not as surprising as it may sound. According to a recent World Wealth Report, the wealthy invest the largest part of their money into stocks and businesses. Our wise janitor had simply done what the wealthy do. So he got a similar result. That is, he grew his money into considerable wealth.
> 
> And you can do this too. Now, we aren't saying you will make $8 million. After all, this is a beginners book and the janitor had an extraordinary result. But stock market investing is one of the best tools you can use to build a more secure financial future for you and your family.
> 
> So are you someone who wants to make money in the stock market? And does that story make you feel excited? Have you tried to understand the stock market, only to be discouraged by how complicated it all seems? And aren't you just a little bit encouraged that an ordinary person, like our janitor from Vermont, could invest in stocks and succeed?
> 
> If you answered yes to any of those questions, then this book just might be the solution you've been looking for. Because it will show you just what you need to know, and no more, to start investing in the stock market.
> 
> And it will describe all of this for you in simple terms you already understand. Not complicated theory. Not a mind-numbing blitz of technical buzzwords. Just what you need to know and no more. And the few specific steps you can take to get started. 