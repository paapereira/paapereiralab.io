---
title: "Upgrading to Ubuntu 9.04 Jaunty Jackalope (on the Desktop)"
slug: "upgrading-to-ubuntu-904-jaunty"
author: "Paulo Pereira"
date: 2009-04-27T22:01:00+01:00
lastmod: 2009-04-27T22:01:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 9.04
  - Jaunty Jackalope
---

After my [laptop](/posts/upgrading-to-ubuntu-904-jaunty-jackalope/), is time to upgrade my main machine whith [Ubuntu 9.04](http://www.ubuntu.com/products/whatisubuntu/904features/), codename Jaunty Jackalope.

I will be the using the 32 bit alternate cd to upgrade. This means that  the upgrade will have a cd image as his source, and not the Internet.  This way it's much faster, I think.

I shouldn't do this during the week, but... oh well...

## Before you start

- Check for updates using Update Manager (be sure to have all the available upgrades done)
- Backup your data
- Check the [Release Notes](http://www.ubuntu.com/getubuntu/releasenotes/904)
- [Download](http://www.ubuntu.com/getubuntu/download) Ubuntu (I used the [Text based “alternate installer” installation disk](http://www.ubuntu.com/getubuntu/downloadmirrors#alternate) link to get the alternate image)
- Check the file hash (example: `md5sum ubuntu-9.04-alternate-i386.iso`) and compare it to [UbuntuHashes](https://help.ubuntu.com/community/UbuntuHashes)
- Be sure to upgrade from Ubuntu 8.10.
- Have a burned Live CD laying around, just in case.
- Use [Gmount-iso](http://www.ubuntugeek.com/easy-way-of-mountunmount-iso-images-in-ubuntu.html) to mount the iso file. This way cd could be mounted without having to burn a cd.

## Install

In the Terminal, go to the cd mount point (example: /media/cdrom) and type:

```bash
cd /media/cdrom
gksu "sh /cdrom/cdromupgrade"
```

And now we wait...

When is asked if I want to include the latest updates during the upgrade I always say no. At this times the servers are very slow - everyone want their Ubuntu :-)

During the upgrade there may be some questions about replacing some configuration files. I replaced them all with the new versions.

After restarting there may be more updates to do.

## Issues / Post-Install

- **Issue 1:** Upgrade failed after many errors. When I rebooted it wouldn't boot into the desktop :(

The upgrade failed so... No big surprise there. Oh well, I had a  Live CD burned, so I booted using that cd. I only needed to configure my network, Firefox to get to [this post](http://ubuntuforums.org/showthread.php?t=1136740) and a Terminal.

The resolution was low and the screen wasn't centered, but that's ok for what I need.

```bash
sudo mount /dev/sda6
cd /media/disk/
sudo cp {/,}etc/resolv.conf
sudo mount --bind {/,}proc
sudo mount --bind {/,}sys
sudo mount --bind {/,}dev
sudo chroot /media/disk/
apt-get update
apt-get -f dist-upgrade --fix-missing
```

That was basically it. The upgrade ended and after the restart everything was (almost) ok.

- **Issue 2:** My NVIDIA card wasn't working that well. The resolution was low and the screen wasn't centered.

It was easy though:

1. Alt + F2
2. /usr/bin/jockey-gtk
3. Selected the recommend proprietary driver for my NVIDIA card
4. Rebooted and that was that

- **Issue 3:** The Tomboy applet is aborting at restart (**to solve...**)

- **Issue 4:** Updated Software Sources

```text
deb http://ppa.launchpad.net/do-core/ppa/ubuntu jaunty main
deb http://packages.medibuntu.org/ jaunty free non-free
deb http://linux.getdropbox.com/ubuntu jaunty main
deb http://ppa.launchpad.net/openoffice-pkgs/ubuntu/ jaunty main
deb http://download.virtualbox.org/virtualbox/debian jaunty non-free
```

- **Issue 5:** Missing software... recovered

- gVIM
- VLC Player
- GNOME Do (`sudo apt-get install gnome-do`)

- **Issue 6:** Virtualbox isn't working. Easy as this:

```bash
sudo /etc/init.d/vboxdrv setup
```

- **Issue 7:** Partial Upgrade. When checking for new updates the Update Manager tries (and fails) to do a Partial Upgrade. I don't know why, but I just went  to Synaptic Package Manager, marked the packages showed in Update  Manager to Update and Apply. Easy!

- **Post-install 1:** I always run *System > Administration > System Testing*. This will help me and Ubuntu to test my system.

- **Post-install 2:** I also run *System > Administration > Computer Janitor*. It's a great idea to clean your system after an upgrade. In case of doubt let it be.

- **Post-install 3:** Some additional cleanup [here](/posts/cleaning-your-system-from-outdated-software/) (a "Computer Janitor" in the Terminal) and [here](/posts/removing-old-kernels-from-your-system/).