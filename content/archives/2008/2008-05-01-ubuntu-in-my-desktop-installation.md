---
title: "Ubuntu in my desktop (installation)"
slug: "ubuntu-in-my-desktop-installation"
author: "Paulo Pereira"
date: 2008-05-01T14:35:00+01:00
lastmod: 2008-05-01T14:35:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
aliases:
  - /posts/ubuntu-in-my-desktop-installation/
---

Tomorrow (01/05) is an holiday in Portugal and today it was a nice day to start installing Ubuntu in my Desktop :-)

The first reference for my eXPerience is my own [post](/posts/ubuntu-in-my-desktop-status/) where I address several important points to check before installing and to achieve.

## Running the live cd

So, the first thing was downloading [Ubuntu](http://www.ubuntu.com/) 32 bits, checking the downloaded file hash (look for more info [here](/posts/upgrading-to-hardy/) and burning the cd.

After booting from the cd is important to check the integrity of the cd. Burning cds also fails.

## Hardware to check (during the live cd session)

- Sound (check)
- Cable network (check) -- been writing in the blog in the live cd session
- Graphics (not so good)

I have a 800x600 screen resolution in my 19'' monitor, and the image isn't centered...

I installed [EnvyNG](http://albertomilone.com/index.html) using the Synaptic Package Manager (look for `envyng-gtk`) and just had to run it (Applications > System Tools > EnvyNG),  after closing Synaptic (important), choose to "Install he NVIDIA driver (Automatic Hardware Detection)" option.

**Note:** before installing EnvyNG, in Synaptic, go to Settings > Repositories and in the "Ubuntu Software" tab choose every option. Also, in the "Third-Party Software" tab choose the first option.

Because I was in a live cd session I didn't restart to see the effect, but everything looks fine in the log so...

Starting the installation now :-)

## Hard Drive Partition Layout

```text
/dev/sdc
  /dev/sdc1   ntfs                   21476 MB (20 GB)
  /dev/sdc2   ext3   /               15360 MB (15 GB)
  /dev/sdc5   ext3   /home           15360 MB (15 GB)
  /dev/sdc6   swap                   2048 MB (2 GB)
  /dev/sdc7   ext3   /storage/pool   27719 MB (27 GB)
```

## Post-install: graphics card

After the Ubuntu installation, I restart the computer and choose to boot from Ubuntu in the Grub menu.

I still don't have the graphic card installed, but the image is centered now.

I installed [EnvyNG](http://albertomilone.com/index.html) (like in the live cd session) and restarted.

While restarting a NVIDIA splash screen will be displayed, proofing a successful installation.
I now have a normal 1280x1024 screen resolution and some desktop effects are already active.

Also check the System > Administration > NVIDIA X Server Settings. Pretty awesome. Even my monitor is correctly recognized.

## Post-install: wireless network card

A warning in the Notification Area told I had a Proprietary Driver to install. I just had to check Enable and the wireless card was installed.

Ridiculous simple...

## To Do

Try the wireless network...

## Post-install: file system

Besides the */* and */home* partitions, only root have permissions to write in every other partitions. In my case */storage/pool*.

So, we have to add permissions to our user:

```bash
sudo chown -R youruser:youruser /storage/pool
sudo chmod -R 755 /storage/pool
```

Click [here](http://www.psychocats.net/ubuntu/mountlinux) for more info.

## Post-install: Multimedia

References: [Complete Streaming, Multimedia & Video How-to](http://ubuntuforums.org/showthread.php?t=661833) and [Medibuntu](https://help.ubuntu.com/community/Medibuntu)

- Repositories

```bash
sudo wget http://www.medibuntu.org/sources.list.d/hardy.list \
 -O /etc/apt/sources.list.d/medibuntu.list
wget -q http://packages.medibuntu.org/medibuntu-key.gpg -O- | sudo apt-key add \
 \- && sudo apt-get update
```

- Flash

```bash
sudo apt-get purge flashplugin-nonfree gnash gnash-common && \
 sudo apt-get install flashplugin-nonfree
```

- Codecs and plugins (it will take quite some time)

```bash
sudo apt-get remove icedtea-gcjwebplugin openjdk-6-jre && \
 sudo apt-get install alsa-oss \
 compizconfig-settings-manager faad gstreamer0.10-ffmpeg gstreamer0.10-plugins-bad \
 gstreamer0.10-plugins-bad-multiverse gstreamer0.10-plugins-ugly \
 gstreamer0.10-plugins-ugly-multiverse gstreamer0.10-pitfdll libflashsupport liblame0 \
 sun-java6-fonts sun-java6-jre sun-java6-plugin unrar w32codecs
```

- Playing Encrypted DVDs

```bash
sudo apt-get install libdvdcss2 libdvdread3 libdvdnav4 build-essential \
 debhelper fakeroot regionset
```

- Changing your [DVD Region Code](http://en.wikipedia.org/wiki/DVD_region_codes) (be sure there's a DVD in the drive)

```bash
sudo regionset
```

Check the [reference](http://ubuntuforums.org/showthread.php?t=661833) link, because it as many other things you'll want.

## Post-install: System > Preferences

List of changed Preferences:

- Advanced Desktop Effects Settings (Compiz Settings Manager)
- Keyboard (Layout > Layout Options)

## Post-install: LAMP Server

To install Apache, MySQL and PHP, check [here](/posts/apache-php-and-mysql/).

## Post-install: Windows Legacy

To install VirtualBox to run Windows, check [here](/posts/windows-under-linux/).

## Post-install: NFS file sharing

To configure two Ubuntu machines to share files using NFS, check [here](/posts/nfs-file-sharing/).

## Post-install: Syncing Windows Mobile 5/6 with Synce

To sync your phone with Ubuntu, check [here](/posts/syncing-windows-mobile-56-with-synce-update/).

## Post-install: extra software

List of extra installation software installed:

- GVim
- [Firefox ](http://beta.foxmarks.com/)[Foxmarks](http://beta.foxmarks.com/) add-on (can't live without it)
- More Firefox add-ons (BugMeNot, Forecastfox)
- Firefox IMDB search-engine
- VLC
- Gmount-iso
- Gmail Notifier
- Gnucash
- Amarok
- Istanbul Desktop Session Recorder
- Sysinfo
- [Conky](/posts/conky/)
- Gaupol Subtitle Editor
- VirtualBox
- [Acrobat Reader](http://www.adobe.com/products/acrobat/readstep2_allversions.html) (choose the .deb installer and double click after the download)
- [Google Earth](http://earth.google.com/download-earth.html) (it will download a .bin installation file, run it in the Terminal)
- gPHPEdit
- Screem HTML/XML Editor
- Bluefish Editor
- Gnome Partition Editor (gParted)
- aspell-pt-pt
- Simple Backup
- mdf2iso (`sudo apt-get install mdf2iso`)
- nrg2iso (`sudo apt-get install nrg2iso`)
- gFTP