---
title: "FavApps: FileZilla"
slug: "favapps-filezilla"
author: "Paulo Pereira"
date: 2013-03-02T21:02:00+00:00
lastmod: 2013-03-02T21:02:00+00:00
draft: false
toc: true
categories:
  - Apps
tags:
  - Linux
  - Ubuntu
  - FileZilla
---

*FileZilla is a full-featured FTP client with an easy-to-use GUI. It is written in C++ and uses the wxWidgets library. FileZilla includes the following features:*

- *Supports FTP, FTP over SSL/TLS (FTPS) and SSH File Transfer Protocol (SFTP)* 
- *IPv6 support* 
- *Available in more than 40 languages* 
- *Supports resume and transfer of large files >4GB* 
- *Easy to use Site Manager and transfer queue* 
- *Bookmarks* 
- *Drag & drop support* 
- *Speed limits* 
- *Filename filters* 
- *Directory comparison* 
- *Network configuration wizard* 
- *Remote file editing* 
- *Keep-alive* 
- *HTTP/1.1, SOCKS5 and FTP Proxy support* 
- *Logging to file* 
- *Synchronized directory browsing* 
- *Remote file search* 
- *Tabbed interface to connect to multiple servers*

FileZilla is my favorite FTP client. I copy files to my Android devices via FTP and FileZilla makes it very easy and fast.

## Installing FileZilla

![FileZilla](/posts/2013/2013-03-02-favapps-filezilla/filezilla.png)

You can install FileZilla via the [Ubuntu Software Center](https://apps.ubuntu.com/cat/applications/filezilla/). You can also use the button at the beginning of this post.

To install via terminal:

```bash
sudo apt-get install filezilla
```

## Using FileZilla to copy files to an Android device

I use the [WellFTP Server](https://play.google.com/store/apps/details?id=com.gsapp.wifitransport) app in my Android device as a FTP server.

In FileZilla go to the Site Manager and create a new site. Enter the IP  address showed in WellFTP Server and the FTP user and password.

![New site](/posts/2013/2013-03-02-favapps-filezilla/new.png)

In the Advanced tab you can define a default local directory and a default remote directory. 
In this case use /sdcard as the default.

![Default directories](/posts/2013/2013-03-02-favapps-filezilla/default.png)