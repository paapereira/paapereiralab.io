---
title: "We Can Build You by Philip K. Dick (1972)"
slug: "we-can-build-you"
author: "Paulo Pereira"
date: 2025-01-31T22:00:00+00:00
lastmod: 2025-01-31T22:00:00+00:00
cover: "/posts/books/we-can-build-you.jpg"
description: "Finished “We Can Build You” by Philip K. Dick."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Philip K. Dick
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Philip K. Dick
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Ebook
---

Finished “We Can Build You”.
* Author: [Philip K. Dick](/book-authors/philip-k.-dick)
* First Published: [July 1, 1972](/book-publication-year/1970s)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/19914823-we-can-build-you)
>
> In this lyrical and moving novel, Philip K. Dick tells a story of toxic love and compassionate robots. When Louis Rosen's electronic organ company builds a pitch-perfect robotic replica of Abraham Lincoln, they are pulled into the orbit of a shady businessman, who is looking to use Lincoln for his own profit. Meanwhile, Rosen seeks Lincoln's advice as he woos a woman incapable of understanding human emotions--someone who may be even more robotic than Lincoln's replica.