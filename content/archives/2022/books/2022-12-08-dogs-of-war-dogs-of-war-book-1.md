---
title: "Dogs of War (Dogs of War Book 1) by Adrian Tchaikovsky (2017)"
slug: "dogs-of-war-dogs-of-war-book-1"
author: "Paulo Pereira"
date: 2022-12-08T15:00:00+00:00
lastmod: 2022-12-08T15:00:00+00:00
cover: "/posts/books/dogs-of-war-dogs-of-war-book-1.jpg"
description: "Finished “Dogs of War (Dogs of War Book 1)” by Adrian Tchaikovsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Adrian Tchaikovsky
book series:
  - Dogs of War Series
book genres:
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Dogs of War Series
  - Science Fiction
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/dogs-of-war-dogs-of-war-book-1
---

Finished “Dogs of War”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
* First Published: [November 2, 2017](/book-publication-year/2017)
* Series: [Dogs of War](/book-series/dogs-of-war-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/57668692-dogs-of-war)
>
> My name is Rex. I am a good dog.
> 
> Rex is also seven foot tall at the shoulder, bulletproof, bristling with heavy calibre weaponry and his voice resonates with subsonics especially designed to instil fear. With Dragon, Honey and Bees, he's part of a Multiform Assault Pack operating in the lawless anarchy of Campeche, south-eastern Mexico.
> 
> Rex is a genetically engineered Bioform, a deadly weapon in a dirty war. He has the intelligence to carry out his orders and feedback implants to reward him when he does. All he wants to be is a Good Dog. And to do that he must do exactly what Master says and Master says he's got to kill a lot of enemies.
> 
> But who, exactly, are the enemies? What happens when Master is tried as a war criminal? What rights does the Geneva Convention grant weapons? Do Rex and his fellow Bioforms even have a right to exist? And what happens when Rex slips his leash? 