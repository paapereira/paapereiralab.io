---
title: "Installing Ubuntu 10.04 Lucid Lynx (on the Desktop)"
slug: "installing-ubuntu-1004-lucid-lynx-on_5260"
author: "Paulo Pereira"
date: 2010-05-06T09:12:00+01:00
lastmod: 2010-05-06T09:12:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 10.04
  - Lucid Lynx
---

Last week [Ubuntu 10.04](http://www.ubuntu.com/products/whatisubuntu/1004features), codename Lucid Lynx, was released.This is a Long Time Support (LTS) release and will be supported until 2013.

After installing Lucid in my desktop my motherboard died, so I had to do a fresh install in a new computer.

I will be the using the 64 bit desktop cd (ubuntu-10.04-desktop-amd64.iso).

## Before you start

- Backup your data 
- Make a list of software you want to recover after the clean install  and all the preferences/options you use. This will make you post install easier and you can start working faster.
- Make a list of the extra Software Sources you use
- Check the [Release Notes](http://www.ubuntu.com/getubuntu/releasenotes/1004)
- [Download](http://www.ubuntu.com/getubuntu/download) Ubuntu
- Check the file hash (example: *md5sum ubuntu-10.04-desktop-amd64.iso*) and compare it to [UbuntuHashes](https://help.ubuntu.com/community/UbuntuHashes)
- Burn a cd (you can use [Brasero](http://www.gnome.org/projects/brasero/), for example, if you already use Ubuntu).

## Running the live cd

Boot from the cd. 

After booting it's important to check the integrity of the cd. Burning cds also fails.

## Hardware to check (during the live cd session)

- Graphics (check)
- Sound (check)
- Cable network (check)
- Wireless network (not tested)

## Hard Drive Partition Layout

I changed my layout in order to have my home folder in another physical hard drive.

```text
/dev/sda
  /dev/sda1   ext4   /         	        15 GB     -- primary
  /dev/sda5    swap                       2 GB     -- extended
  /dev/sda6    ext4   /storage/pool        60 GB     --  extended
/dev/sdb
  /dev/sdb5    ext4   /home           250 GB     --  extended
```

## Post-install: Wired Network

I had to manually configure my IP because I don't have DHCP activated  in my router.

## Post-install: Graphics Card

I had a warning about an available proprietary driver. Just had to   activate it. Ubuntu downloaded and installed the driver. Pretty simple.

## Post-install: Multimedia

References:[Complete  Streaming, Multimedia & Video How-to](http://ubuntuforums.org/showthread.php?t=766683) and [Medibuntu](https://help.ubuntu.com/community/Medibuntu)

- Adding Medibuntu Repositories

```bash
sudo wget --output-document=/etc/apt/sources.list.d/medibuntu.list  http://www.medibuntu.org/sources.list.d/$(lsb_release -cs).list  && sudo apt-get --quiet update && sudo apt-get --yes  --quiet --allow-unauthenticated install medibuntu-keyring &&  sudo apt-get --quiet update
```

- Install Multimedia packages

```bash
sudo apt-get install ubuntu-restricted-extras w64codecs libdvdcss2
```

## Post-install:  Preferences and Look and Feel

Here's some things I changed right away:

- Background 
- NTP Time Server (System > Administration > Time and Date)
  - ntp02.oal.ul.pt
  - ntp04.oal.ul.pt
- Weather on Clock Preferences

## Post-install: SAMBA Server

Click [here](apt:samba).

## Post-install: LAMP Server

To install Apache, MySQL and PHP, check [here](/posts/apache-php-and-mysql/).

## Post-install: Software Sources

- deb http://download.virtualbox.org/virtualbox/debian karmic non-free
- deb http://ppa.launchpad.net/medibuntu-maintainers/ppa/ubuntu intrepid main
- deb http://packages.medibuntu.org/ lucid free non-free
- deb http://ppa.launchpad.net/banshee-team/ppa/ubuntu lucid main
- deb http://linux.dropbox.com/ubuntu lucid main
- deb http://ppa.launchpad.net/do-core/ppa/ubuntu karmic main
- deb http://ppa.launchpad.net/openoffice-pkgs/ppa/ubuntu karmic main
- deb http://ppa.launchpad.net/gcstar/gcstar-dev/ubuntu lucid main
- deb http://ppa.launchpad.net/pcf/miro-releases/ubuntu lucid main
- deb http://ppa.launchpad.net/docky-core/ppa/ubuntu lucid main

## Post-install: extra software

List of extra installation software installed:

- [Xmarks](http://beta.foxmarks.com/) add-on (can’t live  without it) 
- Firefox IMDB search-engine 
- [GnomeDo](/posts/gnome-do-081/) (after adding the software source just go to Ubuntu Software Source and  look for *gnome do*)
- Docky
- VLC 
- Dropbox

```bash
sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 5044912E
sudo apt-get update
sudo apt-get install nautilus-dropbox
```

- [Synergy](/posts/synergy/) 
- GVim 
- Gaupol Subtitle Editor 
- More Firefox add-ons (Portuguese Dictionary, GMail Manager) 
- Powertop (*sudo apt-get install powertop*)
- Gnucash 
- Aspell pt-pt (*sudo apt-get install aspell-pt-pt*)
- Gnome Partition Editor
- [Virtualbox 3.1](/posts/virtualbox-31/)
- Miro
- GCStar
- RAR Support (*sudo apt-get install rar unrar*)
- Graphical Uncomplicated Firewall (click [here](apt:gufw) to install)
- KeepassX
- Banshee
- Banshee extensions (Indicator Applet, Lyrics, Ubuntu One Music Store)

```bash
sudo apt-get install libappindicator0.0-cil  banshee-extension-appindicator banshee-extension-lyrics  banshee-extension-ubuntuonemusicstore
```

- aMule
- screenlets
- Java

```bash
sudo apt-get install sun-java6-jre sun-java6-plugin sun-java6-fonts
```