---
title: "The Complete Stories of Sherlock Holmes, Volume 2 by Arthur Conan Doyle (2010)"
slug: "the-complete-stories-of-sherlock-holmes-2"
author: "Paulo Pereira"
date: 2017-04-23T22:34:00+01:00
lastmod: 2017-04-23T22:34:00+01:00
cover: "/posts/books/the-complete-stories-of-sherlock-holmes-2.jpg"
description: "Finished “The Complete Stories of Sherlock Holmes, Volume 2” by Arthur Conan Doyle."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2010
book authors:
  - Arthur Conan Doyle
book series:
  - The Complete Stories of Sherlock Holmes
book genres:
  - Fiction
  - Mystery
  - Classics
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Arthur Conan Doyle
  - The Complete Stories of Sherlock Holmes
  - Fiction
  - Mystery
  - Classics
  - 7/10 Books
  - Audiobook
---

Finished “The Complete Stories of Sherlock Holmes Book 2”.
* Author: [Arthur Conan Doyle](/book-authors/arthur-conan-doyle)
* First Published: [2010](/book-publication-year/2010)
* Series: [The Complete Stories of Sherlock Holmes](/book-series/the-complete-stories-of-sherlock-holmes) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/11254485-the-complete-stories-of-sherlock-holmes-volume-2)
>
> Volume two in this series consists of one novel, The Hound of the Baskervilles, and two collections of short stories, which include "Memoirs of Sherlock Holmes" and "The Return of Sherlock Holmes" (a total of 23 stories).
>
> These creations by Doyle represent the finest work of his Holmes series, and certainly the most famous. They are reproduced here (and in all volumes) in the order in which they were first published.