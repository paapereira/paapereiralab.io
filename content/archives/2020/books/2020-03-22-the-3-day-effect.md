---
title: "The 3-Day Effect by Florence Williams (2018)"
slug: "the-3-day-effect"
author: "Paulo Pereira"
date: 2020-03-22T22:42:00+00:00
lastmod: 2020-03-22T22:42:00+00:00
cover: "/posts/books/the-3-day-effect.png"
description: "Finished “The 3-Day Effect” by Florence Williams."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Florence Williams
book series:
  - 
book genres:
  - Nonfiction
book scores:
  - 4/10 Books
tags:
  - Book Log
  - Florence Williams
  - Nonfiction
  - 4/10 Books
  - Audiobook
---

Finished “The 3-Day Effect”.
* Author: [Florence Williams](/book-authors/florence-williams)
* First Published: [September 27th 2018](/book-publication-year/2018)
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/41970691-the-3-day-effect)
>
> The 3-Day Effect is a look at the science behind why being in the wild can make us happier, healthier and more creative. Whether it’s rafting down Utah’s Green River, backpacking in Arizona’s wilderness or walking through Rock Creek Park in Washington, D.C., scientists are finding that the more exposure humans have to nature, the more they will benefit from reduced anxiety, enhanced creativity and overall well-being. Trek with science journalist Florence Williams as she guides former Iraqi war veterans, sex trafficking survivors, and even a nature hater, on three-day nature excursions to see how the outdoors offers something like a miracle cure for an array of extreme and everyday ailments.