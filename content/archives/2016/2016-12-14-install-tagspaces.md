---
title: "Install TagSpaces on Linux"
slug: "install-tagspaces"
author: "Paulo Pereira"
date: 2016-12-14T21:02:24+00:00
lastmod: 2016-12-14T21:02:24+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - TagSpaces
---

I’ve been using [TagSpaces](https://www.tagspaces.org/) from some time now, and I ended up to buy a Pro version.

You get the Pro version by receiving a download link by email. I’m on Arch Linux and the pro versions provided are not for Arch, but it has a .deb version.

Here's how to install it on Arch. Updating is basically the same.

```bash
yaourt -S xarchiver electron libgcrypt15

cd ~/Downloads
mv tagspaces-pro-2.5.0-amd64.deb tagspaces-pro-2.5.0-amd64.ar
xarchiver -x ./tagspaces tagspaces-pro-2.5.0-amd64.ar
xarchiver -x . data.tar.xz
rm -f control.tar.gz
rm -f debian-binary
rm -f data.tar.xz

sudo cp -R opt/ /
rm -rf opt
sudo cp -R usr/ /
rm -rf usr/
rm -f ~/Downloads/tagspaces-pro-2.5.0-amd64.ar

vi ~/.local/share/applications/tagspaces.desktop
```

```text
[Desktop Entry]
Name=tagspaces
Comment=Offline document organizer and browser for your local files, supporting easy file tagging and document editing.
Exec=/opt/tagspaces/tagspaces
Terminal=false
Type=Application
Icon=/usr/share/icons/hicolor/512x512/apps/tagspaces.png
```