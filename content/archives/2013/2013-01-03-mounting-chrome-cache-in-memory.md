---
title: "Mounting Chrome cache in memory"
slug: "mounting-chrome-cache-in-memory"
author: "Paulo Pereira"
date: 2013-01-03T09:00:00+00:00
lastmod: 2013-01-03T09:00:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Chrome
aliases:
  - /posts/mounting-chrome-cache-in-memory/
---

As seen in my [Ubuntu 12.04 installation post](/posts/installing-ubuntu-1204-lts/), I use a script to keep the Chrome cache in RAM.

I found this tip somewhere (don't recall where) and have been using it for some time now.

Mounting Chrome cache in memory will give you the speed of RAM vs. the speed of an hard drive.

Do you have any more tips like this?

## Script

- Create the script:
```bash
vi ~/bin/chrome_tmp_cache.sh
```
- Add the following to the script:
```bash
#!/bin/bash
if test -d /tmp/chrome; then
  exit 0
else
  rm -r ~/.cache/google-chrome
  mkdir /tmp/chrome
  ln -s /tmp/chrome ~/.cache/google-chrome
fi
```
- Change the script permissions:
```bash
chmod +x bin/chrome_tmp_cache.sh
```
- Add the script to the Startup Applications:
```text
Name: Chrome Cache
Command: /home/<user>/bin/chrome_tmp_cache.sh
Comment: Move Chrome cache to /tmp
```

![Play Store](/posts/2013/2013-01-03-mounting-chrome-cache-in-memory/edit-startup-program.png)

## Mounting /tmp in RAM

For this script to work, /tmp must be mounted in memory. Just add the following to your /etc/fstab file:

- Open /etc/fstab as root
```bash
gksu gedit /etc/fstab
```
- Add the following line
```text
tmpfs /tmp tmpfs nodev,nosuid,noatime,mode=1777 0 0
```