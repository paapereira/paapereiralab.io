---
title: "The Green Mile by Stephen King (1996)"
slug: "the-green-mile"
author: "Paulo Pereira"
date: 2024-03-17T18:00:00+00:00
lastmod: 2024-03-17T18:00:00+00:00
cover: "/posts/books/the-green-mile.jpg"
description: "Finished “The Green Mile” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Fantasy
  - Horror
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Fantasy
  - Horror
  - 10/10 Books
  - Audiobook
aliases:
  - /posts/books/the-green-mile
---

Finished “The Green Mile”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [January 1, 1996](/book-publication-year/1990s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/42738827-the-green-mile)
>
> Welcome to Cold Mountain Penitentiary, home to the Depression-worn men of E Block. Convicted killers all, each awaits his turn to walk the Green Mile, keeping a date with "Old Sparky," Cold Mountain's electric chair. Prison guard Paul Edgecombe has seen his share of oddities in his years working the Mile. But he's never seen anyone like John Coffey, a man with the body of a giant and the mind of a child, condemned for a crime terrifying in its violence and shocking in its depravity. In this place of ultimate retribution, Edgecombe is about to discover the terrible, wondrous truth about Coffey, a truth that will challenge his most cherished beliefes... and yours.