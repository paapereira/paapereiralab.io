---
title: "Disclosure by Michael Crichton (1994)"
slug: "disclosure"
author: "Paulo Pereira"
date: 2022-03-20T22:00:00+00:00
lastmod: 2022-03-20T22:00:00+00:00
cover: "/posts/books/disclosure.jpg"
description: "Finished “Disclosure” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Michael Crichton
book series:
  - 
book genres:
  -  Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Fiction
  - 7/10 Books
  - Ebook
---

Finished “Disclosure”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [January 1994](/book-publication-year/1990s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6130403-disclosure)
>
> An up-and-coming executive at the computer firm DigiCom, Tom Sanders is a man whose corporate future is certain. But after a closed-door meeting with his new boss—a woman who is his former lover and has been promoted to the position he expected to have—Sanders finds himself caught in a nightmarish web of deceit in which he is branded the villain.
> 
> As Sanders scrambles to defend himself, he uncovers an electronic trail into the company’s secrets—and begins to grasp that a cynical and manipulative scheme has been devised to bring him down.