---
title: "World War Z by Max Brooks (2006)"
slug: "world-war-z"
author: "Paulo Pereira"
date: 2013-04-23T19:10:00+01:00
lastmod: 2013-04-23T19:10:00+01:00
cover: "/posts/books/world-war-z.jpg"
description: "Finished “World War Z: An Oral History of the Zombie War” by Max Brooks."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2006
book authors:
  - Max Brooks
book series:
  - 
book genres:
  - Horror
  - Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Max Brooks
  - Horror
  - Fiction
  - 5/10 Books
  - Audiobook
---

Finished “World War Z: An Oral History of the Zombie War”.
* Author: [Max Brooks](/book-authors/max-brooks)
* First Published: [September 12th 2006](/book-publication-year/2006)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/16169990-world-war-z)
>
> "The end was near." Voices from the Zombie War.
>
> The Zombie War came unthinkably close to eradicating humanity. Max Brooks, driven by the urgency of preserving the acid-etched firsthand experiences of the survivors from those apocalyptic years, traveled across the United States of America and throughout the world, from decimated cities that once teemed with upwards of thirty million souls to the most remote and inhospitable areas of the planet.
>
> He recorded the testimony of men, women, and sometimes children who came face-to-face with the living - or at least the undead - hell of that dreadful time. World War Z is the result. Never before have we had access to a document that so powerfully conveys the depth of fear and horror, and also the ineradicable spirit of resistance, that gripped human society through the plague years.