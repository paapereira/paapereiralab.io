---
title: "Forward: Stories of Tomorrow by Blake Crouch, Veronica Roth, N.K. Jemisin, Amor Towles, Paul Tremblay and Andy Weir (2019)"
slug: "forward-stories-of-tomorrow"
author: "Paulo Pereira"
date: 2021-07-23T21:00:00+01:00
lastmod: 2021-07-23T21:00:00+01:00
cover: "/posts/books/forward-stories-of-tomorrow.jpg"
description: "Finished “Forward: Stories of Tomorrow” by Blake Crouch, Veronica Roth, N.K. Jemisin, Amor Towles, Paul Tremblay and Andy Weir."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Blake Crouch
  - Veronica Roth
  - N.K. Jemisin
  - Amor Towles
  - Paul Tremblay
  - Andy Weir
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
  - Short Stories
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Blake Crouch
  - Veronica Roth
  - N.K. Jemisin
  - Amor Towles
  - Paul Tremblay
  - Andy Weir
  - Fiction
  - Science Fiction
  - Short Stories
  - 5/10 Books
  - Audiobook
---

Finished “Forward: Stories of Tomorrow”.
* Authors: [Blake Crouch](/book-authors/blake-crouch), [Veronica Roth](/book-authors/veronica-roth), [N.K. Jemisin](/book-authors/n.k.-jemisin), [Amor Towles](/book-authors/amor-towles), [Paul Tremblay](/book-authors/paul-tremblay) and [Andy Weir](/book-authors/andy-weir)
* First Published: [September 17th 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/52839918-forward)
>
> For some, it’s the end of the world. For others, it’s just the beginning. With brilliant imagination, today’s most visionary writers point to the future in a collection curated by bestselling author Blake Crouch. These stories range from darkly comic to deeply chilling, but they all look forward. Featuring stories by Andy Weir, Amor Towles, Veronica Roth, N. K. Jemisin, Paul Tremblay, and Blake Crouch; with narration by Evan Rachel Wood, David Harbour, Jason Isaacs, Rosa Salazar, Steven Strait, and Janina Gavankar.
> 
> ARK, by Veronica Roth, read by Evan Rachel Wood (Westworld)
> 
> On the eve of Earth’s destruction, a young scientist discovers something too precious to lose, in a story of cataclysm and hope by the #1 New York > Times bestselling author of the Divergent trilogy.
> 
> It’s only two weeks before an asteroid turns home to dust. Though most of Earth has already been evacuated, it’s Samantha’s job to catalog plant > samples for the survivors’ unknowable journey beyond. Preparing to stay behind and watch the world end, she makes a final human connection.
> 
> SUMMER FROST, by Blake Crouch, read by Rosa Salazar (Alita: Battle Angel)
> 
> A video game developer becomes obsessed with a willful character in her new project, in a mind-bending exploration of what it means to be human by the > New York Times bestselling author of Recursion.
> 
> Maxine was made to do one thing: die. Except the minor non-player character in the world Riley is building makes her own impossible decision - veering > wildly off course and exploring the boundaries of the map. Soon Riley has all new plans for her spontaneous AI, including bringing Max into the real > world. But what if Max has real-world plans of her own?
> 
> EMERGENCY SKIN, by N. K. Jemisin, read by Jason Isaacs (Star Trek: Discovery)
> 
> What will become of our self-destructed planet? The answer shatters all expectations in this subversive speculation from the Hugo Award-winning author > of the Broken Earth trilogy.
> 
> An explorer returns to gather information from a climate-ravaged Earth that his ancestors, and others among the planet’s finest, fled centuries ago. > The mission comes with a warning: a graveyard world awaits him. But so do those left behind - hopeless and unbeautiful wastes of humanity who should > have died out ages ago. Get in. Get out. And try not to stare.
> 
> YOU HAVE ARRIVED AT YOUR DESTINATION, by Amor Towles, read by David Harbour (Stranger Things)
> 
> Nature or nurture? Neither. Discover a bold new way to raise a child in this unsettling story of the near future by the New York Times bestselling > author of A Gentleman in Moscow.
> 
> When Sam’s wife first tells him about Vitek, a twenty-first-century fertility lab, he sees it as the natural next step in trying to help their future > child get a “leg up” in a competitive world. But the more Sam considers the lives that his child could lead, the more he begins to question the > choices he has made in his life.
> 
> THE LAST CONVERSATION, by Paul Tremblay, read by Steven Strait (The Expanse)
> 
> What’s more frightening: Not knowing who you are? Or finding out? A Bram Stoker Award-winning author explores the answer in a chilling story about > human consciousness.
> 
> Imagine you’ve woken up in an unfamiliar room with no memory of who you are, how you got there, or where you were before. All you have is the > disconnected voice of an attentive caretaker. Dr. Kuhn is there to help you - physically, emotionally, and psychologically. She’ll make sure you > reclaim your lost identity. Now answer one question: Are you sure you want to?
> 
> RANDOMIZE, by Andy Weir, read by Janina Gavankar (True Blood)
> 
> In the near future, if Vegas games are ingeniously scam-proof, then the heists have to be too, in this imaginative and whip-smart story by the New > York Times bestselling author of The Martian.
> 
> An IT whiz at the Babylon Casino is enlisted to upgrade security for the game of keno and its random-number generator. The new quantum computer system > is foolproof. But someone on the inside is no fool. For once the odds may not favor the house.