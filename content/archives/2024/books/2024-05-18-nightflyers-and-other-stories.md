---
title: "Nightflyers & Other Stories by George R.R. Martin (1985)"
slug: "nightflyers-and-other-stories"
author: "Paulo Pereira"
date: 2024-05-18T16:00:00+01:00
lastmod: 2024-05-18T16:00:00+01:00
cover: "/posts/books/nightflyers-and-other-stories.jpg"
description: "Finished “Nightflyers and Other Stories” by George R.R. Martin."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - George R.R. Martin
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
  - Short Stories
book scores:
  - 8/10 Books
tags:
  - Book Log
  - George R.R. Martin
  - Fiction
  - Science Fiction
  - Short Stories
  - 8/10 Books
  - Ebook
---

Finished “Nightflyers & Other Stories”.
* Author: [George R.R. Martin](/book-authors/george-r.r.-martin)
* First Published: [January 1, 1985](/book-publication-year/1980s)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/37635527-nightflyers-other-stories)
>
> On a voyage toward the boundaries of the known universe, nine misfit academics seek out first contact with a shadowy alien race.
> 
> But another enigma is the Nightflyer itself, a cybernetic wonder with an elusive captain no one has ever seen in the flesh. Soon, however, the crew discovers that their greatest mystery – and most dangerous threat – is an unexpected force wielding a thirst for blood and terror….
> 
> Also included are five additional classic tales of science fiction that explore the breadth of technology and the dark corners of the human mind.
>
> Nightflyers - 8/10
> 
> Override - 7/10
> 
> Weekend in a War Zone - 7/10
> 
> And Seven Times Never Kill Man - 6/10
> 
> Nor the Many-Colored Fires of a Star Ring - 7/10
> 
> A Song for Lya - 10/10