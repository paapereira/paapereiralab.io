---
title: "Super Mario: How Nintendo Conquered America by Jeff Ryan (2011)"
slug: "super-mario"
author: "Paulo Pereira"
date: 2024-05-19T15:30:00+01:00
lastmod: 2024-05-19T15:30:00+01:00
cover: "/posts/books/super-mario.jpg"
description: "Finished “Super Mario: How Nintendo Conquered America” by Jeff Ryan."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Jeff Ryan
book series:
  - 
book genres:
  - Nonfiction
  - Business
  - History
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Jeff Ryan
  - Nonfiction
  - Business
  - History
  - 6/10 Books
  - Audiobook
---

Finished “Super Mario: How Nintendo Conquered America”.
* Author: [Jeff Ryan](/book-authors/jeff-ryan)
* First Published: [August 1, 2011](/book-publication-year/2011)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/24856300-super-mario)
>
> The story of Nintendo’s rise and the beloved icon who made it possible
> 
> Nintendo has continually set the standard for video game innovation in America, starting in 1981 with a plucky hero who jumped over barrels to save a girl from an ape.
> 
> The saga of Mario, the portly plumber who became the most successful franchise in the history of gaming, has plot twists worthy of a video game. Jeff Ryan shares the story of how this quintessentially Japanese company found success in the American market. Lawsuits, Hollywood, die-hard fans, and face-offs with Sony and Microsoft are all part of the drama. Find out about: Mario’s eccentric yet brilliant creator, Shigeru Miyamoto, who was tapped for the job because he was considered expendable; Minoru Arakawa, the son-in-law of Nintendo’s imperious president, who bumbled his way to success; and the unexpected approach that allowed Nintendo to reinvent itself as the gaming system for the nongamer, especially now with the Wii.
> 
> Even those who can’t tell a Koopa from a Goomba will find this a fascinating story of striving, comeuppance, and redemption.