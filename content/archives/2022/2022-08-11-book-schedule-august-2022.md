---
title: "Books Schedule update"
slug: "book-schedule-august-2022"
author: "Paulo Pereira"
date: 2022-08-11T19:30:05+01:00
lastmod: 2022-08-11T19:30:05+01:00
description: "August Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my August TBR (To Be Read) update for the next months.

* **[Read]** The Handmaid's Tale by Margaret Atwood
* **[Read]** Storm Front (The Dresden Files 1) by Jim Butcher
* **[Read]** The Man in the High Castle by Philip K. Dick
* **[Reading]** Chapterhouse: Dune (Dune 6) by Frank Herbert
* Fool Moon (The Dresden Files 2) by Jim Butcher
* NOS4A2 by Joe Hill
* Airframe by Michael Crichton
* The Testaments (The Handmaid's Tale 2) by Margaret Atwood
* Grave Peril (The Dresden Files 3) by Jim Butcher
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* Revelation Space (Revelation Space 1) by Alastair Reynolds
* Boy's Life by Robert R. McCammon

(...)

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson
* Redemption Ark (Revelation Space 2) by Alastair Reynolds

I also have always an audiobook going too:

* **[Read]** Flagship Victory (Galactic Liberation 3) by David Vandyke & B. V. Larson
* **[Read]** Upgrade by Blake Crouch
* **[Reading]** Rich Dad Poor Dad by Robert T. Kiyosaki
* Four Past Midnight by Stephen King
* Hive War (Galactic Liberation 4) by David Vandyke & B. V. Larson
* All the Names by José Saramago
* Born a Crime: Stories From a South African Childhood by Trevor Noah
