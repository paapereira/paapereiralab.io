---
title: "Ready Player One (Ready Player One Book 1) by Ernest Cline (2011)"
slug: "ready-player-one-ready-player-one-book-1"
author: "Paulo Pereira"
date: 2015-01-26T23:31:00+00:00
lastmod: 2015-01-26T23:31:00+00:00
cover: "/posts/books/ready-player-one-ready-player-one-book-1.jpg"
description: "Finished “Ready Player One (Ready Player One Book 1)” by Ernest Cline."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Ernest Cline
book series:
  - Ready Player One Series
book genres:
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Ernest Cline
  - Ready Player One Series
  - Science Fiction
  - 10/10 Books
  - Audiobook
aliases:
  - /posts/books/ready-player-one-ready-player-one-book-1
---

Finished “Ready Player One”.
* Author: [Ernest Cline](/book-authors/ernest-cline)
* First Published: [August 16th 2011](/book-publication-year/2011)
* Series: [Ready Player One](/book-series/ready-player-one-series) Book 1
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12382340-ready-player-one)
>
> It's the year 2044, and the real world is an ugly place.
>
> Like most of humanity, Wade Watts escapes his grim surroundings by spending his waking hours jacked into the OASIS, a sprawling virtual utopia that lets you be anything you want to be, a place where you can live and play and fall in love on any of ten thousand planets.
>
> And like most of humanity, Wade dreams of being the one to discover the ultimate lottery ticket that lies concealed within this virtual world. For somewhere inside this giant networked playground, OASIS creator James Halliday has hidden a series of fiendish puzzles that will yield massive fortune--and remarkable power--to whoever can unlock them.
>
> For years, millions have struggled fruitlessly to attain this prize, knowing only that Halliday's riddles are based in the pop culture he loved--that of the late twentieth century. And for years, millions have found in this quest another means of escape, retreating into happy, obsessive study of Halliday's icons. Like many of his contemporaries, Wade is as comfortable debating the finer points of John Hughes's oeuvre, playing Pac-Man, or reciting Devo lyrics as he is scrounging power to run his OASIS rig.
>
> And then Wade stumbles upon the first puzzle.
>
> Suddenly the whole world is watching, and thousands of competitors join the hunt--among them certain powerful players who are willing to commit very real murder to beat Wade to this prize. Now the only way for Wade to survive and preserve everything he knows is to win. But to do so, he may have to leave behind his oh-so-perfect virtual existence and face up to life--and love--in the real world he's always been so desperate to escape.
>
> A world at stake.
> A quest for the ultimate prize.
> Are you ready?