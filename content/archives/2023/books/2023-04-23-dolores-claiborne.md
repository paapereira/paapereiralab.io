---
title: "Dolores Claiborne by Stephen King (1992)"
slug: "dolores-claiborne"
author: "Paulo Pereira"
date: 2023-04-23T11:00:00+01:00
lastmod: 2023-04-23T11:00:00+01:00
cover: "/posts/books/dolores-claiborne.jpg"
description: "Finished “Dolores Claiborne” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Thriller
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Thriller
  - 7/10 Books
  - Audiobook
---

Finished “Dolores Claiborne”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [November 1, 1992](/book-publication-year/1990s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/26805724-dolores-claiborne)
>
> For thirty years, folks on Little Tall Island have been waiting to find out just what happened on the eerie dark day Dolores Claiborne's husband died --- the day of the total eclipse. Now, the police want to know what happened yesterday when her rich, bedridden employer died suddenly in her care. With no choice but to talk, Dolores gives her compelling confession ... of the strange and terrible links forged by hidden intimacies ... of the fierceness of a mother's love and its dreadful consequences ... of the silent rage that can turn a woman's heart to hate. When Dolores Claiborne is accused of murder, it's only the beginning of the bad news. For what comes after that is something only Stephen King could imagine ... as he rips open the darkest secrets and the most damning sins of men and women in an ingrown. Maine town and takes you on a trip below its straitlaced surface.