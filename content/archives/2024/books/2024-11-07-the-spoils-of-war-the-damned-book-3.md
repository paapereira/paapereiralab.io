---
title: "The Spoils of War (The Damned Book 3) by Alan Dean Foster (1993)"
slug: "the-spoils-of-war-the-damned-book-3"
author: "Paulo Pereira"
date: 2024-11-07T21:00:00+00:00
lastmod: 2024-11-07T21:00:00+00:00
cover: "/posts/books/the-spoils-of-war-the-damned-book-3.jpg"
description: "Finished “The Spoils of War (The Damned Book 3)” by Alan Dean Foster."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Alan Dean Foster
book series:
  - The Damned Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Alan Dean Foster
  - The Damned Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “The Spoils of War”.
* Author: [Alan Dean Foster](/book-authors/alan-dean-foster)
* First Published: [March 16, 1993](/book-publication-year/1990s)
* Series: [The Damned](/book-series/the-damned-series) Book 3
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/357763.The_Spoils_of_War)
>
> The Weave was on the verge of winning a decisive victory after a milennia of war, thanks to their new allies from earth. But then the birdlike Wais scholar Lalelang found evidence that Humans might not adapt well to peace. Researching further, she uncovered a secret group of telepathic Humans called the Core, who were on the verge of starting another war, and then eliminating Lalelang. At the last moment, she was saved by a lone Core commander. He took a chance on her intelligence and compassion, and gambled the fate of Humanity on the possibility that together, they could find an alternative to a galaxy-wide bloodbath....