---
title: "Darkly Dreaming Dexter (Dexter Book 1) by Jeff Lindsay (2004)"
slug: "darkly-dreaming-dexter-dexter-book-1"
author: "Paulo Pereira"
date: 2014-09-15T23:53:00+01:00
lastmod: 2014-09-15T23:53:00+01:00
cover: "/posts/books/darkly-dreaming-dexter-dexter-book-1.jpg"
description: "Finished “Darkly Dreaming Dexter (Dexter Book 1)” by Jeff Lindsay."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2004
book authors:
  - Jeff Lindsay
book series:
  - Dexter Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Jeff Lindsay
  - Dexter Series
  - Fiction
  - Mystery
  - 9/10 Books
  - Audiobook
---

Finished “Darkly Dreaming Dexter”.
* Author: [Jeff Lindsay](/book-authors/jeff-lindsay)
* First Published: [July 10th 2004](/book-publication-year/2004)
* Series: [Dexter](/book-series/dexter-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13155707-darkly-dreaming-dexter)
>
> Meet Dexter Morgan, a polite wolf in sheep's clothing. He's handsome and charming, but something in his past has made him abide by a different set of rules. He's a serial killer whose one golden rule makes him immensely likeable: he only kills bad people. And his job as a blood splatter expert for the Miami police department puts him in the perfect position to identify his victims. But when a series of brutal murders bearing a striking similarity to his own style start turning up, Dexter is caught between being flattered and being frightened—of himself or some other fiend.