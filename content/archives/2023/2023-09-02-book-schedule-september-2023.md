---
title: "Books Schedule update"
slug: "book-schedule-september-2023"
author: "Paulo Pereira"
date: 2023-09-02T14:45:00+01:00
lastmod: 2023-09-02T14:45:00+01:00
description: "September Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my September TBR (To Be Read) update for the next months.

* **[Read]** Next by Michael Crichton
* **[Read]** Building a Second Brain by Tiago Forte
* **[Read]** The PARA Method by Tiago Forte
* **[Read]** The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson
* **[Read]** How It Unfolds (Far Reaches 1) by James S. A. Corey
* **[Read]** Void (Far Reaches 2) by Veronica Roth
* **[Reading]** The Private Life of Elder Things by Adrian Tchaikovsky & Keris McDonald & Adam Gauntlett
* Lost Stars by Claudia Gray
* Falling Bodies (Far Reaches 3) by Rebecca Roanhorse
* The Long Game (Far Reaches 4) by Ann Leckie
* Light Bringer (Red Rising Saga 6) by Pierce Brown
* Just Out of Jupiter's Reach (Far Reaches 5) by Nnedi Okorafor
* Slow Time Between the Stars (Far Reaches 6) by John Scalzi
* Bethany's Sin by Robert R. McCammon
* Dead Beat (The Dresden Files 7) by Jim Butcher
* Diamond Dogs, Turquoise Days by Alastair Reynolds
* Wool (Silo 1) by Hugh Howey
* Pirate Latitudes by Michael Crichton
* The Hero of Ages (The Mistborn Saga 3) by Brandon Sanderson
* A Call to Arms (The Damned 1) by Alan Dean Foster

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson

I also have always an audiobook going too:

* **[Read]** Hearts in Atlantis by Stephen King
* **[Reading]** Androids and Aliens (Star Runner 3) by B. V. Larson
* A World Without Email by Cal Newport
* Level Six (Killday 2) by William Ledbetter
* The Ocean at the End of the Lane by Neil Gaiman
* Rose Madder by Stephen King
* Parallel Worlds by Michio Kaku