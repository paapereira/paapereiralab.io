---
title: "New entry in my Science Fiction Top 50 Books and Series"
slug: "top-science-fiction-books-2021-11-22"
author: "Paulo Pereira"
date: 2021-11-22T11:59:08+00:00
lastmod: 2021-11-22T11:59:08+00:00
description: "This is my Science Fiction Top 50 Books and Series.\n

1. Dune Series by Frank Herbert (2 books read so far, 1965-1969)\n
2. Ender's Game (Ender's Saga Book 1) by Orson Scott Card (1985)\n
3. The Time Machine by H.G. Wells (1895)"
draft: false
toc: false
categories:
  - Books
tags:
  - Top Science Fiction
---

Adding Childhood's End, following my [recent](/posts/top-science-fiction-books-2021-11-15) list update.

Here's some note about this list:
* It has single books, all series or one or more books in a series
* It's based on my "now that I'm doing this" feeling and I'm sure it will change over time
* I considered the books I read since I started to organized them, so it's probably lacking
* I add a **(*)** in front of the new entries

## Science Fiction Top 50

1. Dune Series by Frank Herbert (2 books read so far, 1965-1969) [Read more](/book-series/dune-series)
2. Ender's Game (Ender's Saga Book 1) by Orson Scott Card (1985) [Read more](/posts/books/enders-game-enders-saga-book-1)
3. The Time Machine by H.G. Wells (1895) [Read more](/posts/books/the-time-machine)
4. Red Rising Saga by Pierce Brown (3 books read so far, 2014-2016) [Read more](/book-series/red-rising-saga-series)
5. The Forever War by Joe Haldeman (3 books, 1974-1999) [Read more](/book-series/the-forever-war-series)
6. **(*)** Childhood's End by Arthur C. Clarke (1953) [Read more](/posts/books/childhoods-end)
7. 2001: A Space Odyssey (Space Odyssey Book 1) by Arthur C. Clarke (1968) [Read more](/posts/books/2001-a-space-odyssey-book-1)
8. 1984 by George Orwell (1949) [Read more](/posts/books/1984)
9. Contact by Carl Sagan (1985) [Read more](/posts/books/contact)
10. Children of Time (Children of Time Book 1) by Adrian Tchaikovsky (2015) [Read more](/posts/books/children-of-time-children-of-time-book-1)
11. The Three-Body Problem (Remembrance of Earth's Past Book 1) by Liu Cixin (2008) [Read more](/posts/books/the-three-body-problem-remembrance-of-earths-past-book-1)
12. Hitchhiker's Guide to the Galaxy (Hitchhiker's Guide to the Galaxy Book 1) by Douglas Adams (1979) [Read more](/posts/books/hitchhikers-guide-to-the-galaxy-hitchhikers-guide-to-the-galaxy-book-1)
13. Jurassic Park (Jurassic Park Book 1) by Michael Crichton (1990) [Read more](/posts/books/jurassic-park-jurassic-park-book-1)
14. Star Force Series by B.V. Larson and David VanDyke (12 books, 2010-2015) [Read more](/book-series/star-force-series)
15. Undying Mercenaries Series by B.V. Larson (15 books, 2013-2021) [Read more](/book-series/undying-mercenaries-series)
16. Ready Player One (Ready Player One Book 1) by Ernest Cline (2011) [Read more](/posts/books/ready-player-one-ready-player-one-book-1)
17. Bobiverse Series by Dennis E. Taylor (4 books, 2016-2020) [Read more](/book-series/bobiverse-series)
18. The Hunger Games Series by Suzanne Collins (3 books, 2008-2010) [Read more](/book-series/the-hunger-games-series)
19. Project Hail Mary by Andy Weir (2021) [Read more](/posts/books/project-hail-mary)
20. Threshold Series by Peter Clines (book 1, 2012 and book 4, 2020) [Read more](/book-series/threshold-series)
21. Sphere by Michael Crichton (1987) [Read more](/posts/books/sphere)
22. Remembrance of Earth's Past Series by Liu Cixin (book 2, 2008 and book 3, 2010) [Read more](/book-series/remembrance-of-earths-past-series)
23. The Martian (The Martian Book 1) by Andy Weir (2012) [Read more](/posts/books/the-martian-book-1)
24. Wayward Pines Series by Blake Crouch (3 books, 2012-2014) [Read more](/book-series/wayward-pines-series)
25. Dark Matter by Blake Crouch (2016) [Read more](/posts/books/dark-matter)
26. Recursion by Blake Crouch (2019) [Read more](/posts/books/recursion) 
27. Foundation (Foundation Book 1) by Isaac Asimov (1951) [Read more](/posts/books/foundation-book-1)
28. The Andromeda Strain (Andromeda Book 1) by Michael Crichton (1969) [Read more](/posts/books/the-andromeda-strain-andromeda-book-1)
29. Fahrenheit 451 by Ray Bradbury (1953) [Read more](/posts/books/fahrenheit-451)
30. Blade Runner (Blade Runner Book 1) by Philip K. Dick (1968) [Read more](/posts/books/blade-runner-blade-runner-book-1)
31. I Am Legend by Richard Matheson (1954) [Read more](/posts/books/i-am-legend)
32. Altered Carbon (Takeshi Kovacs Book 1) by Richard K. Morgan (2002) [Read more](/posts/books/altered-carbon-takeshi-kovacs-book-1)
33. Lock In Series by John Scalzi (2 books, 2014-2018)  [Read more](/book-series/lock-in-series)
34. Level Five (Killday Book 1) by William Ledbetter (2018) [Read more](/posts/books/level-five-killday-book-1)
35. Paradox Bound by Peter Clines (2017) [Read more](/posts/books/paradox-bound)
36. The Circle by Dave Eggers (2013) [Read more](/posts/books/the-circle)
37. Artemis by Andy Weir (2017) [Read more](/posts/books/artemis)
38. Armada by Ernest Cline (2015) [Read more](/posts/books/armada)
39. I, Robot (Robot Series Book 0.1) by Isaac Asimov (1950) [Read more](/posts/books/i-robot-robot-series-book-01)
40. The War of the Worlds by H.G. Wells (1898) [Read more](/posts/books/the-war-of-the-worlds)
41. We by Yevgeny Zamyatin (1920) [Read more](/posts/books/we)
42. The Terminal Man by Michael Crichton (1972) [Read more](/posts/books/the-terminal-man)
43. Starship Troopers by Robert A. Heinlein (1959) [Read more](/posts/books/starship-troopers)
44. Brave New World by Aldous Huxley (1932) [Read more](/posts/books/brave-new-world)
45. Threshold Series by Peter Clines (book 2, 2015 and book 3, 2019) [Read more](/book-series/threshold-series)
46. Alien (1979) and Aliens (1986) by Alan Dean Foster [Read more](/book-series/alien-series)
47. Alien: The Cold Forge by Alex White (2018) [Read more](/posts/books/alien-the-cold-forge)
48. The Stars My Destination by Alfred Bester (1955) [Read more](/posts/books/the-stars-my-destination)
49. Damocles by S.G. Redling (2013) [Read more](/posts/books/damocles)
50. Flatland: A Romance of Many Dimensions by Edwin A. Abbott (1884) [Read more](/posts/books/flatland)