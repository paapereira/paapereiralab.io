---
title: "The Burnout Generation by Anne Helen Petersen (2019)"
slug: "the-burnout-generation"
author: "Paulo Pereira"
date: 2020-08-09T19:59:00+01:00
lastmod: 2020-08-09T19:59:00+01:00
cover: "/posts/books/the-burnout-generation.png"
description: "Finished “The Burnout Generation” by Anne Helen Petersen."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Anne Helen Petersen
book series:
  - 
book genres:
  - Nonfiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Anne Helen Petersen
  - Nonfiction
  - 5/10 Books
  - Audiobook
---

Finished “The Burnout Generation”.
* Author: [Anne Helen Petersen](/book-authors/anne-helen-petersen)
* First Published: [March 10th 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/49744485-the-burnout-generation)
>
> In January 2019, culture writer Anne Helen Petersen set the Internet on fire with her viral BuzzFeed essay diagnosing "millennial burnout"—a chronic state of stress and exhaustion that’s become a "base temperature" for young people today. Now, she continues this generation-defining conversation in a brand-new format, interviewing millennials around the country about their own deeply personal experiences with burnout and the culture that creates it. Listeners will hear about how this issue has affected Petersen’s own life as well as the lives of five very different subjects: Kevin, a musician and Marine veteran; Kate, a first-generation college graduate working to repay her formidable student debt; Haley and Evette, young writers at different career stages in the digital media industry; and John, a pastor and co-founder of a new Baptist church in North Carolina.
>
> The conversations that comprise The Burnout Generation cover everything from debt to social media to the blurred boundaries between our professional and personal lives. In this illuminating and intimate audio project, listeners learn how and why this generation has been conditioned to "optimize" every aspect of our lives (Meal prep for the week! Find a side hustle! But practice self-care! And answer emails in bed!), and most importantly, how the consequences of this phenomenon play out in day-to-day life. 