---
title: "Blood Rites (The Dresden Files Book 6) by Jim Butcher (2004)"
slug: "blood-rites-the-dresden-files-book-6"
author: "Paulo Pereira"
date: 2023-06-19T21:00:00+01:00
lastmod: 2023-06-19T21:00:00+01:00
cover: "/posts/books/blood-rites-the-dresden-files-book-6.jpg"
description: "Finished “Blood Rites (The Dresden Files Book 6)” by Jim Butcher."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2004
book authors:
  - Jim Butcher
book series:
  - The Dresden Files Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jim Butcher
  - The Dresden Files Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Blood Rites”.
* Author: [Jim Butcher](/book-authors/jim-butcher)
* First Published: [August 1, 2004](/book-publication-year/2004)
* Series: [The Dresden Files](/book-series/the-dresden-files-series) Book 6
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8176978-blood-rites)
>
> Dresden Files, Chicago's only professional wizard takes on a case for a vampire and becomes the prime suspect in a series of ghastly murders.
> 
> Harry Dresden has had worse assignments than going undercover on the set of an adult film. Like fleeing a burning building full of enraged demon-monkeys, for instance. Or going toe-to-leaf with a walking plant monster. Still, there’s something more troubling than usual about his newest case. The film’s producer believes he’s the target of a sinister curse—but it’s the women around him who are dying, in increasingly spectacular ways.
> 
> Harry’s doubly frustrated because he only got involved with this bizarre mystery as a favor to Thomas—his flirtatious, self-absorbed vampire acquaintance of dubious integrity. Thomas has a personal stake in the case Harry can’t quite figure out, until his investigation leads him straight to the vampire’s oversexed, bite-happy family. Now, Harry’s about to discover that Thomas’ family tree has been hiding a shocking a revelation that will change Harry’s life forever.