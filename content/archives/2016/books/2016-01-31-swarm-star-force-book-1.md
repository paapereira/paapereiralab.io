---
title: "Swarm (Star Force Book 1) by B.V. Larson (2010)"
slug: "swarm-star-force-book-1"
author: "Paulo Pereira"
date: 2016-01-31T23:23:00+00:00
lastmod: 2016-01-31T23:23:00+00:00
cover: "/posts/books/swarm-star-force-book-1.jpg"
description: "Finished “Swarm (Star Force Book 1)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2010
book authors:
  - B.V. Larson
book series:
  - Star Force Series
book genres:
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Force Series
  - Science Fiction
  - 9/10 Books
  - Audiobook
---

Finished “Swarm”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [December 23rd 2010](/book-publication-year/2010)
* Series: [Star Force](/book-series/star-force-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/16041462-swarm)
>
> Earth arms marines with alien technology and builds its first battle fleet! Kyle Riggs is snatched by an alien spacecraft sometime after midnight. The ship is testing everyone it catches and murdering the weak. The good news is that Kyle keeps passing tests and staying alive. The bad news is the aliens who sent this ship are the nicest ones out there.
>
> A novel of military science fiction by best-selling author B. V. Larson, Swarm is the story of Earth's annexation by an alien empire. Long considered a primitive people on a backwater planet, humanity finds itself in the middle of a war - and faced with extinction.