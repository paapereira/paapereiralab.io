---
title: "SimpleX Chat"
slug: "simplex-chat"
author: "Paulo Pereira"
date: 2023-04-23T14:00:00+01:00
lastmod: 2023-04-23T14:00:00+01:00
description: ""
draft: false
toc: true
categories:
  - General
tags:
  - Linux
  - Arch Linux
  - Android
  - SimpleX Chat
aliases:
  - /posts/simplex-chat
---

[SimpleX Chat](https://simplex.chat/) is 'The first messaging platform that has no user identifiers of any kind - 100% private by design'.

I've been using it for a week or so but I can already tell that this is the one you if you minimally care about your data and your privacy.

In the website you can read more about how does it work and the main features by scrolling through the [webpage](https://simplex.chat/), read the [whitepaper](https://github.com/simplex-chat/simplexmq/blob/stable/protocol/overview-tjr.md) and read the [user guide](https://github.com/simplex-chat/simplex-chat/blob/stable/docs/guide/README.md).

Here I just want to list a little bit of how to set it up and how I look at it for start using it.

## Installation

You can get SimpleX Chat for [Android](https://play.google.com/store/apps/details?id=chat.simplex.app), [iOS](https://apps.apple.com/us/app/simplex-chat/id1605771084) or as a [CLI app for Linux](https://github.com/simplex-chat/simplex-chat#zap-quick-installation-of-a-terminal-app).

In my case I'm using it on Android and on my Arch Linux machines.

```bash
paru -S simplex-chat-bin
```

## No user ID or registration necessary

The main privacy feature of SimpleX Chat is to not need a registration *per se*. You otherwise establish a connection from your device to your contact device.

This means you can't just look for a contact and try to connect to it. There is no central server that keeps that information. Instead you establish the connection in person or via another (trusted) mean.

This also means that your identity in SimpleX Chat in your phone is different from your desktop or your other phone.

## Establish a connection to a contact

Simplifying there are 2 ways.

By **generating a one-time invitation link** (in the form of a link and QR code) and let your contact connect via the link or by scanning the QR Code. This can be done right next to your contact or via a video call for example, or you can send the invite link to another channel you already have with your contact. You can always at a later date verify the contact at the app.

Another way is to **share your contact address**. Useful for post in social media for example.

These invites are just to establish the connection. Your contact address can be deleted and recreated and your already established contacts will not disappear.

## Chat profiles and incognito mode

You can create several profiles with different user names.

There is also an incognito mode that will generate a random user name. If you enable incognito mode before entering a group or accept a user connection your profile user name will not be shared.

If you then turn off incognito mode the connections with the random user will stay as is.

## Talk to me if you have questions

[Invite link](https://simplex.chat/contact/#/?v=1-2&smp=smp%3A%2F%2F0YuTwO05YJWS8rkjn9eLJDjQhFKvIYd8d4xG8X1blIU%3D%40smp8.simplex.im%2FS5AjypkVk6ynYfnrtynDUNFT_qd_eGRd%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEA7Vbm8UJlN_imOm2u-7CWb4XLCQ6mBhA8irTrY7bxeXo%253D%26srv%3Dbeccx4yfxxbvyhqypaavemqurytl6hozr47wfc7uuecacjqdvwpw2xid.onion)

![QR Code](/social/simplex-chat-invite.png)
