---
title: "Books Schedule update"
slug: "book-schedule-october-2023"
author: "Paulo Pereira"
date: 2023-10-01T13:00:00+01:00
lastmod: 2023-10-01T13:00:00+01:00
description: "October Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my October TBR (To Be Read) update for the next months.

* **[Read]** The Private Life of Elder Things by Adrian Tchaikovsky & Keris McDonald & Adam Gauntlett
* **[Read]** Lost Stars by Claudia Gray
* **[Read]** Falling Bodies (Far Reaches 3) by Rebecca Roanhorse
* **[Read]** The Long Game (Far Reaches 4) by Ann Leckie
* **[Reading]** Light Bringer (Red Rising Saga 6) by Pierce Brown
* Stay Out of the Basement (Goosebumps 2) by R. L. Stine
* Heart-Shaped Box by Joe Hill
* Just Out of Jupiter's Reach (Far Reaches 5) by Nnedi Okorafor
* Slow Time Between the Stars (Far Reaches 6) by John Scalzi
* Bethany's Sin by Robert R. McCammon
* Dead Beat (The Dresden Files 7) by Jim Butcher
* Diamond Dogs, Turquoise Days by Alastair Reynolds
* Wool (Silo 1) by Hugh Howey
* Pirate Latitudes by Michael Crichton
* The Hero of Ages (The Mistborn Saga 3) by Brandon Sanderson
* A Call to Arms (The Damned 1) by Alan Dean Foster
* Precious Little Things by Adrian Tchaikovsky
* Star Wars Episode IV: A New Hope by George Lucas

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson

I also have always an audiobook going too:

* **[Read]** Androids and Aliens (Star Runner 3) by B. V. Larson
* **[Read]** A World Without Email by Cal Newport
* **[Read]** Level Six (Killday 2) by William Ledbetter
* **[Reading]** Elon Musk by Walter Isaacson
* The Ocean at the End of the Lane by Neil Gaiman
* Rose Madder by Stephen King
* Red Company: First Strike! by B. V. Larson
* Parallel Worlds by Michio Kaku
* Who Goes There? by John W. Campbell Jr.
* Accessory to War by Neil Degrasse Tyson