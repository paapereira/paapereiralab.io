---
title: "Books Series to complete buying"
slug: "series-to-complete-december-2023"
author: "Paulo Pereira"
date: 2023-12-28T14:00:00+00:00
lastmod: 2023-12-28T14:00:00+00:00
description: "Short list of series I'm yet to complete buying."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Wishlist
---

December update for my short list of series to complete buying.

* Mars Trilogy by Kim Stanley Robinson
* The Sun Eater by Christopher Ruocchio
* Culture by Iain M. Banks
* New Crobuzon by China Mieville
* The Salvation Sequence by Peter F. Hamilton
* The Long Earth by Terry Pratchett and Stephen Baxter
* The Last Kingdom by Bernard Cornwell
* The Legend of Drizzt by R. A. Salvatore
* Sookie Stackhouse by Charlaine Harris
* The Matthew Corbett Novels by Robert McCammon
* Marsbound by Joe Haldeman
* The Rampart Trilogy by M. R. Carey
* The Southern Reach Trilogy by Jeff VanderMeer
* Wanderers by Chuck Wendig
* Zones of Thought (Qeng Ho) by Vernor Vinge
* Star Wars Universe books
* Doctor Who