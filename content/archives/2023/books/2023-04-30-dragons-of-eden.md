---
title: "Dragons of Eden by Carl Sagan (1977)"
slug: "dragons-of-eden"
author: "Paulo Pereira"
date: 2023-04-30T10:00:00+01:00
lastmod: 2023-04-30T10:00:00+01:00
cover: "/posts/books/dragons-of-eden.jpg"
description: "Finished “Dragons of Eden” by Carl Sagan."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Carl Sagan
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Carl Sagan
  - Nonfiction
  - Science
  - 7/10 Books
  - Audiobook
---

Finished “Dragons of Eden: Speculations on the Evolution of Human Intelligence”.
* Author: [Carl Sagan](/book-authors/carl-sagan)
* First Published: [April 12, 1977](/book-publication-year/1970s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36228124-dragons-of-eden)
>
> Dr Carl Sagan takes us on a great reading adventure, offering his vivid & startling insights into the brains of humans & beasts, the origin of human intelligence, the function of our most haunting legends & their amazing links to recent discoveries.