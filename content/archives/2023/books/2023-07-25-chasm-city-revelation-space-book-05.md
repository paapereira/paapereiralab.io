---
title: "Chasm City (Revelation Space Book 0.5) by Alastair Reynolds (2001)"
slug: "chasm-city-revelation-space-book-05"
author: "Paulo Pereira"
date: 2023-07-25T21:00:00+01:00
lastmod: 2023-07-25T21:00:00+01:00
cover: "/posts/books/chasm-city-revelation-space-book-05.jpg"
description: "Finished “Chasm City (Revelation Space Book 0.5)” by Alastair Reynolds."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2001
book authors:
  - Alastair Reynolds
book series:
  - Revelation Space Series
  - Revelation Space Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Alastair Reynolds
  - Revelation Space Series
  - Revelation Space Universe
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
---

Finished “Chasm City”.
* Author: [Alastair Reynolds](/book-authors/alastair-reynolds)
* First Published: [May 1, 2001](/book-publication-year/2001)
* Series: [Revelation Space](/book-series/revelation-space-series) Book 0.5
* [Revelation Space Universe](/book-series/revelation-space-universe)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/50191771-chasm-city)
>
> The once-utopian Chasm City -- a domed human settlement on an otherwise inhospitable planet -- has been overrun by a virus known as the Melding Plague, capable of infecting any body, organic or computerized. Now, with the entire city corrupted -- from the people to the very buildings they inhabit -- only the most wretched sort of existence remains. For security operative Tanner Mirabel, it is the landscape of nightmares through which he searches for a lowlife postmortal killer. But the stakes are raised when his search brings him face to face with a centuries-old atrocity that history would rather forget.