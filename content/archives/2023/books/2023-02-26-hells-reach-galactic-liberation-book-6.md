---
title: "Hell's Reach (Galactic Liberation Book 6) by David VanDyke and B.V. Larson (2019)"
slug: "hells-reach-galactic-liberation-book-6"
author: "Paulo Pereira"
date: 2023-02-26T16:00:00+00:00
lastmod: 2023-02-26T16:00:00+00:00
cover: "/posts/books/hells-reach-galactic-liberation-book-6.jpg"
description: "Finished “Hell's Reach (Galactic Liberation Book 6)” by David VanDyke and B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - David VanDyke
  - B.V. Larson
book series:
  - Galactic Liberation Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - David VanDyke
  - B.V. Larson
  - Galactic Liberation Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Hell's Reach”.
* Author: [David VanDyke](/book-authors/david-vandyke) and [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [November 4, 2019](/book-publication-year/2019)
* Series: [Galactic Liberation](/book-series/galactic-liberation-series) Book 6
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/48712083-hell-s-reach)
>
> The Breakers find a new home among the stars: Utopia. An engineered planet that is vast beyond comprehension with plenty of room to grow. They set up trade routes and work as mercenaries for their supplies… but trouble lurks.
> 
> Pirates strike their merchant lanes. Key personnel are captured and enslaved. Straker is called upon to do something, and thus a new conflict is set in motion. 