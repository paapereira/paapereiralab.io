---
title: "The Girl in the Spider's Web (Millennium Book 4) by David Lagercrantz (2015)"
slug: "the-girl-in-the-spiders-web-millennium-book-4"
author: "Paulo Pereira"
date: 2018-02-11T21:50:00+01:00
lastmod: 2018-02-11T21:50:00+01:00
cover: "/posts/books/the-girl-in-the-spiders-web-millennium-book-4.jpg"
description: "Finished “The Girl in the Spider's Web (Millennium Book 4)” by David Lagercrantz."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - David Lagercrantz
book series:
  - Millennium Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - David Lagercrantz
  - Millennium Series
  - Fiction
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “The Girl in the Spider's Web”.
* Author: [David Lagercrantz](/book-authors/david-lagercrantz)
* First Published: [August 27th 2015](/book-publication-year/2015)
* Series: [Millennium](/book-series/millennium-series) Book 4
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/26869258-the-girl-in-the-spider-s-web)
>
> Mikael Blomkvist is contacted by renowned scientist Professor Balder. Fearing for his life, but more concerned for his son's well-being, Balder wants Millennium to publish his story.
>
> More interesting to Blomkvist than Balder's advances in Artificial Intelligence, is his connection with a certain female superhacker.
>
> It seems that Lisbeth Salander, like Balder, is a target of ruthless cyber gangsters that will soon bring terror to the streets of Stockholm, the Millennium team, and Blomkvist and Salander themselves. 