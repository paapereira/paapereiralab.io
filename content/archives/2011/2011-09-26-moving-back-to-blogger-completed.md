---
title: "Moving back to blogger completed"
slug: "moving-back-to-blogger-completed"
author: "Paulo Pereira"
date: 2011-09-26T14:11:00+01:00
lastmod: 2011-09-26T14:11:00+01:00
draft: false
toc: true
categories:
  - General
tags:
  - Blogger
---

Last week I finished the migration of the blog from Squarespace back to Blogger.

The blog url is still the same (lofspot.net), but any reference to squarespace will soon stop working.

There are some wrong links in past posts, so if you see some leave me a comment.