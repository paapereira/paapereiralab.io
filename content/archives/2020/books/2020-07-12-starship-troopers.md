---
title: "Starship Troopers by Robert A. Heinlein (1959)"
slug: "starship-troopers"
author: "Paulo Pereira"
date: 2020-07-12T22:30:00+01:00
lastmod: 2020-07-12T22:30:00+01:00
cover: "/posts/books/starship-troopers.png"
description: "Finished “Starship Troopers” by Robert A. Heinlein."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1950s
book authors:
  - Robert A. Heinlein
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Robert A. Heinlein
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
aliases:
  - /posts/books/starship-troopers
---

Finished “Starship Troopers”.
* Author: [Robert A. Heinlein](/book-authors/robert-a.-heinlein)
* First Published: [December 1959](/book-publication-year/1950s)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25352833-starship-troopers)
>
> The historians can’t seem to settle whether to call this one "The Third Space War" (or the fourth), or whether "The First Interstellar War" fits it better. We just call it “The Bug War." Everything up to then and still later were "incidents," "patrols," or "police actions." However, you are just as dead if you buy the farm in an "incident" as you are if you buy it in a declared war...
>
> In one of Robert A. Heinlein’s most controversial bestsellers, a recruit of the future goes through the toughest boot camp in the Universe—and into battle with the Terran Mobile Infantry against mankind’s most alarming enemy.