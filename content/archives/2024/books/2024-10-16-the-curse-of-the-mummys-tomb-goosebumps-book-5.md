---
title: "The Curse of the Mummy's Tomb (Goosebumps Book 5) by R.L. Stine (1993)"
slug: "the-curse-of-the-mummys-tomb-goosebumps-book-5"
author: "Paulo Pereira"
date: 2024-10-16T21:00:00+01:00
lastmod: 2024-10-16T21:00:00+01:00
cover: "/posts/books/the-curse-of-the-mummys-tomb-goosebumps-book-5.jpg"
description: "Finished “The Curse of the Mummy's Tomb (Goosebumps Book 5)” by R.L. Stine."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - R.L. Stine
book series:
  - Goosebumps Series
book genres:
  - Fiction
  - Horror
book scores:
  - 6/10 Books
tags:
  - Book Log
  - R.L. Stine
  - Goosebumps Series
  - Fiction
  - Horror
  - 6/10 Books
  - Ebook
---

Finished “The Curse of the Mummy's Tomb”.
* Author: [R.L. Stine](/book-authors/r.l.-stine)
* First Published: [January 1, 1993](/book-publication-year/1990s)
* Series: [Goosebumps](/book-series/goosebumps-series) Book 5
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12474462-the-curse-of-the-mummy-s-tomb)
>
> Gabe just got lost--in a pyramid. One minute, his crazy cousin Sari was right ahead of him in the pyramid tunnel. The next minute, she'd disappeared.But Gabe isn't alone. Someone else is in the pyramid, too. Someone. Or some thing.Gabe doesn't believe in the curse of the mummy's tomb. But that doesn't mean the curse isn't real. Does it?Now with all-new bonus materials including a Q&A with the author and more!