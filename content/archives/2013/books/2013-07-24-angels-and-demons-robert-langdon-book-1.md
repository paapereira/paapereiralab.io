---
title: "Angels & Demons (Robert Langdon Book 1) by Dan Brown (2000)"
slug: "angels-and-demons-robert-langdon-book-1"
author: "Paulo Pereira"
date: 2013-07-24T23:41:00+01:00
lastmod: 2013-07-24T23:41:00+01:00
cover: "/posts/books/angels-and-demons-robert-langdon-book-1.jpg"
description: "Finished “Angels and Demons (Robert Langdon Book 1)” by Dan Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2000
book authors:
  - Dan Brown
book series:
  - Robert Langdon Series
book genres:
  - Fiction
  - Thriller
  - Mystery
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Dan Brown
  - Robert Langdon Series
  - Fiction
  - Thriller
  - Mystery
  - 9/10 Books
  - Audiobook
---

Finished “Angels & Demons”.
* Author: [Dan Brown](/book-authors/dan-brown)
* First Published: [May 2000](/book-publication-year/2000)
* Series: [Robert Langdon](/book-series/robert-langdon-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/20887941-angels-demons)
>
> World-renowned Harvard symbologist Robert Langdon is summoned to a Swiss research facility to analyze a cryptic symbol seared into the chest of a murdered physicist. What he discovers is unimaginable: a deadly vendetta against the Catholic Church by a centuries-old underground organization, the Illuminati. Desperate to save the Vatican from a powerful time bomb, Langdon joins forces in Rome with the beautiful and mysterious scientist Vittoria Vetra. Together they embark on a frantic hunt through sealed crypts, dangerous catacombs, deserted cathedrals, and the most secretive vault on earth, the long-forgotten Illuminati lair.