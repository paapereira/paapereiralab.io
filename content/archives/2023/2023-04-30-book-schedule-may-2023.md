---
title: "Books Schedule update"
slug: "book-schedule-may-2023"
author: "Paulo Pereira"
date: 2023-04-30T11:00:00+01:00
lastmod: 2023-04-30T11:00:00+01:00
description: "May Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my May TBR (To Be Read) update for the next months.

* **[Read]** Absolution Gap (Revelation Space 3) by Alastair Reynolds
* **[Read]** Life of Pi by Yann Martel
* **[Reading]** The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* Elder Race by Adrian Tchaikovsky
* Baal by Robert R. McCammon
* Blood Rites (The Dresden Files 6) by Jim Butcher
* The Tooth Fairy by Graham Joyce
* Chasm City by Alastair Reynolds
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson

I also have always an audiobook going too:

* **[Read]** Star Runner by B. V. Larson
* **[Read]** Dolores Claiborne by Stephen King
* **[Read]** Dragons of Eden: Speculations on the Evolution of Human Intelligence by Carl Sagan
* **[Reading]** State of Fear by Michael Crichton
* Fire Fight (Star Runner 2) by B. V. Larson
* Insomnia by Stephen King
* Digital Minimalism by Cal Newport
* How to Avoid a Climate Disaster by Bill Gates
* Androids and Aliens (Star Runner 3) by B. V. Larson