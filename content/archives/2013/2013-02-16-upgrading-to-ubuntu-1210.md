---
title: "Upgrading to Ubuntu 12.10"
slug: "upgrading-to-ubuntu-1210"
author: "Paulo Pereira"
date: 2013-02-16T21:41:00+00:00
lastmod: 2013-02-16T21:41:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 12.10
  - Quantal Quetzal
---

The current Ubuntu version, [12.10](http://www.ubuntu.com/download/desktop) Quantal Quetzal, was released passed October, and I've been running it since that time.

![Ubuntu 12.10](/posts/2013/2013-02-16-upgrading-to-ubuntu-1210/ubuntu.png)

I hadn't made a direct upgrade at quite some time. Normally I use the alternate iso (see my 11.10 upgrade [post](/posts/upgrading-to-ubuntu-1110/), but this time I chose to upgrade directly through Updater Manager from my [12.04 installation](/posts/installing-ubuntu-1204-lts/).

## Backup

Your first step should always be backing up your files.

You can use an external drive, CDs or DVD, offline locations (like [rsync.net](http://rsync.net/)), Dropbox... Take your pick, but do it. Really, you should.

## Upgrading

Before starting the upgrade, be sure you have have all your current version updates installed in the Update Manager.

Then, go to Software Sources, and into the "Updates" tab:

![Software Sources](/posts/2013/2013-02-16-upgrading-to-ubuntu-1210/software-sources.jpg)

In the "Notify me of a new Ubuntu version" option select the option "For any new version":

![Select "For any new version" option](/posts/2013/2013-02-16-upgrading-to-ubuntu-1210/new-version.jpg)

Go back to the Update Manager and start the upgrade:

![Upgrade to Ubuntu 12.10](/posts/2013/2013-02-16-upgrading-to-ubuntu-1210/upgrade.jpg)

That's it.

## Unity's online search results

Ubuntu 12.10 presented a new Amazon lens to Unity that presents shopping results based on user queries.

If you want to deactivate that option go to System Settings > Privacy and disable online search results.

![Disabling online search results](/posts/2013/2013-02-16-upgrading-to-ubuntu-1210/online-search.png)

You can also remove the Amazon lens completely:

```bash
sudo apt-get remove unity-lens-shopping
```

## Extra software installed

- Phatch (photo batch processor)
```bash
sudo apt-get install phatch
```
- Nemo file manager
```bash
sudo add-apt-repository ppa:gwendal-lebihan-dev/cinnamon-nightly
sudo apt-get update
sudo apt-get install nemo nemo-data nemo-fileroller nemo-dropbox nemo-share
```
- System Load Indicator
```bash
sudo add-apt-repository ppa:indicator-multiload/stable-daily
sudo apt-get update
sudo apt-get install indicator-multiload
```
- Ubuntu One Indicator
```bash
sudo apt-add-repository ppa:rye/ubuntuone-extras
sudo apt-get update
sudo apt-get install indicator-ubuntuone
```
- Woof
```bash
sudo apt-get install woof
```
- Nethogs
```bash
sudo apt-get install nethogs
```
- Preload
```bash
sudo apt-get install preload
```