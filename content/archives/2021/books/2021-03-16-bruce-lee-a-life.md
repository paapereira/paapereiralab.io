---
title: "Bruce Lee: A Life by Matthew Polly (2018)"
slug: "bruce-lee-a-life"
author: "Paulo Pereira"
date: 2021-03-16T22:00:00+00:00
lastmod: 2021-03-16T22:00:00+00:00
cover: "/posts/books/bruce-lee-a-life.jpg"
description: "Finished “Bruce Lee: A Life” by Matthew Polly."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Matthew Polly
book series:
  - 
book genres:
  - Nonfiction
  - Biography
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Matthew Polly
  - Nonfiction
  - Biography
  - 7/10 Books
  - Audiobook
---

Finished “Bruce Lee: A Life”.
* Author: [Matthew Polly](/book-authors/matthew-polly)
* First Published: [January 1st 2018](/book-publication-year/2018)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/55660212-bruce-lee)
>
> The first authoritative biography—featuring dozens of rarely seen photographs—of film legend Bruce Lee, who made martial arts a global phenomenon, bridged the divide between Eastern and Western cultures, and smashed long-held stereotypes of Asians and Asian-Americans.
>
> Forty-five years after Bruce Lee’s sudden death at age thirty-two, journalist and bestselling author Matthew Polly has written the definitive account of Lee’s life. It’s also one of the only accounts; incredibly, there has never been an authoritative biography of Lee. Following a decade of research that included conducting more than one hundred interviews with Lee’s family, friends, business associates, and even the actress in whose bed Lee died, Polly has constructed a complex, humane portrait of the icon.
>
> Polly explores Lee’s early years as a child star in Hong Kong cinema; his actor father’s struggles with opium addiction and how that turned Bruce into a troublemaking teenager who was kicked out of high school and eventually sent to America to shape up; his beginnings as a martial arts teacher, eventually becoming personal instructor to movie stars like James Coburn and Steve McQueen; his struggles as an Asian-American actor in Hollywood and frustration seeing role after role he auditioned for go to a white actors in eye makeup; his eventual triumph as a leading man; his challenges juggling a sky-rocketing career with his duties as a father and husband; and his shocking end that to this day is still shrouded in mystery.
>
> Polly breaks down the myths surrounding Bruce Lee and argues that, contrary to popular belief, he was an ambitious actor who was obsessed with the martial arts—not a kung-fu guru who just so happened to make a couple of movies. This is an honest, revealing look at an impressive yet imperfect man whose personal story was even more entertaining and inspiring than any fictional role he played onscreen.