---
title: "B3 Server 2.3 Upgrade"
slug: "b3-server-23-upgrade"
author: "Paulo Pereira"
date: 2011-09-26T14:06:00+01:00
lastmod: 2011-09-26T14:06:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
---

[B3 Server](http://www.excito.com/) [2.3 version](http://update.excito.net/install/latest/b3/) come out a couple of months ago.

Normally the upgrade is easy and done via the web interface, but since I changed the mysql root default password, I need to upgrade manually.

## Upgrade to 2.3

- Connect to your b3 server

```bash
ssh user@b3server
```

- Become root

```bash
su
```

- Upgrade instructions

```bash
change_distribution elvin
apt-get -c /etc/apt/bubba-apt.conf -y update
apt-get -c /etc/apt/bubba-apt.conf -y dist-upgrade
```

*Keep in mind the configuration files you want to keep during the upgrade. It will ask you.*
*Example: samba.conf*

- Reboot after the upgrade

```bash
reboot
```