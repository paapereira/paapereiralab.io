---
title: "God Emperor of Dune (Dune Book 4) by Frank Herbert (1981)"
slug: "god-emperor-of-dune-dune-book-4"
author: "Paulo Pereira"
date: 2022-03-29T21:00:00+01:00
lastmod: 2022-03-29T21:00:00+01:00
cover: "/posts/books/god-emperor-of-dune-dune-book-4.jpg"
description: "Finished God Emperor of Dune (Dune Book 4)” by Frank Herbert."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Frank Herbert
book series:
  - Dune Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Frank Herbert
  - Dune Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “God Emperor of Dune”.
* Author: [Frank Herbert](/book-authors/frank-herbert)
* First Published: [May 6th 1981](/book-publication-year/1980s)
* Series: [Dune](/book-series/dune-series) Book 4
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/55712106-god-emperor-of-dune)
>
> Millennia have passed on Arrakis, and the once-desert planet is green with life. Leto Atreides, the son of the world’s savior, the Emperor Paul Muad’Dib, is still alive but far from human. To preserve humanity’s future, he sacrificed his own by merging with a sandworm, granting him near immortality as God Emperor of Dune for the past thirty-five hundred years.
> 
> Leto’s rule is not a benevolent one. His transformation has made not only his appearance but his morality inhuman. A rebellion, led by Siona, a member of the Atreides family, has risen to oppose the despot’s rule. But Siona is unaware that Leto’s vision of a Golden Path for humanity requires her to fulfill a destiny she never wanted—or could possibly conceive....