---
title: "Body of Proof by Darrell Brown and Sophie Ellis (2019)"
slug: "body-of-proof"
author: "Paulo Pereira"
date: 2020-07-30T21:59:00+01:00
lastmod: 2020-07-30T21:59:00+01:00
cover: "/posts/books/body-of-proof.png"
description: "Finished “Body of Proof” by Darrell Brown and Sophie Ellis."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Darrell Brown
  - Sophie Ellis
book series:
  - 
book genres:
  - Nonfiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Darrell Brown
  - Sophie Ellis
  - Nonfiction
  - 5/10 Books
  - Audiobook
---

Finished “Body of Proof”.
* Author: [Darrell Brown](/book-authors/darrell-brown) and [Sophie Ellis](/book-authors/sophie-ellis)
* First Published: [September 5th 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/48063227-body-of-proof)
>
> Body of Proof, a true crime podcast, examines the many unanswered questions surrounding the disappearance and death of Suzanne Pilley in Edinburgh in 2010 and the subsequent conviction of David Gilroy. Journalists and TV producers Darrell Brown and Sophie Ellis spent two years investigating the case and spoke exclusively to David Gilroy, who was convicted of murdering Suzanne Pilley and disposing of her body. Sentenced to life in a Scottish prison, Gilroy maintains his innocence. Although police believe they have the right man, key components of the prosecution’s case are missing: there is no body of the victim, no witnesses to the crime, and no physical evidence (no DNA, CCTV video, or murder weapon).
>
> In this gripping, step-by-step investigation, Darrell Brown and Sophie Ellis uncover startling information not heard in court that might have changed the minds of the jurors. And they shine a light on aspects of the Scottish criminal justice system that might be keeping an innocent man behind bars.