---
title: "Hardware: first impression"
slug: "hardware-first-impression"
author: "Paulo Pereira"
date: 2008-03-20T16:28:00+00:00
lastmod: 2008-03-20T16:28:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

Uppon first boot all my hardware seems to be working, including my graphics card, the mute sound button and even the SD Card Reader (this one doesn't work out of the box in Windows).

So far I only had to manually config my wired connection (I have DHCP turned off) and restart the network service.

```bash
sudo /etc/init.d/networking restart
```