---
title: "Bad Blood: Secrets and Lies in a Silicon Valley Startup by John Carreyrou (2018)"
slug: "bad-blood"
author: "Paulo Pereira"
date: 2019-01-27T18:15:00+00:00
lastmod: 2019-01-27T18:15:00+00:00
cover: "/posts/books/bad-blood.jpg"
description: "Finished “Bad Blood: Secrets and Lies in a Silicon Valley Startup” by John Carreyrou."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - John Carreyrou
book series:
  - 
book genres:
  - Nonfiction
  - Business
book scores:
  - 7/10 Books
tags:
  - Book Log
  - John Carreyrou
  - Nonfiction
  - Business
  - 7/10 Books
  - Audiobook
---

Finished “Bad Blood: Secrets and Lies in a Silicon Valley Startup”.
* Author: [John Carreyrou](/book-authors/john-carreyrou)
* First Published: [May 21st 2018](/book-publication-year/2018)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40217960-bad-blood)
>
> The full inside story of the breathtaking rise and shocking collapse of a multibillion-dollar startup, by the prize-winning journalist who first broke the story and pursued it to the end in the face of pressure and threats from the CEO and her lawyers.
>
> In 2014, Theranos founder and CEO Elizabeth Holmes was widely seen as the female Steve Jobs: a brilliant Stanford dropout whose startup "unicorn" promised to revolutionize the medical industry with a machine that would make blood tests significantly faster and easier. Backed by investors such as Larry Ellison and Tim Draper, Theranos sold shares in a fundraising round that valued the company at $9 billion, putting Holmes's worth at an estimated $4.7 billion. There was just one problem: The technology didn't work.
>
> For years, Holmes had been misleading investors, FDA officials, and her own employees. When Carreyrou, working at The Wall Street Journal, got a tip from a former Theranos employee and started asking questions, both Carreyrou and the Journal were threatened with lawsuits. Undaunted, the newspaper ran the first of dozens of Theranos articles in late 2015. By early 2017, the company's value was zero and Holmes faced potential legal action from the government and her investors. Here is the riveting story of the biggest corporate fraud since Enron, a disturbing cautionary tale set amid the bold promises and gold-rush frenzy of Silicon Valley.