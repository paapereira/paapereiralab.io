---
title: "Book Haul for January 2021"
slug: "book-haul-2021-01"
author: "Paulo Pereira"
date: 2021-02-06T14:10:23+00:00
lastmod: 2021-02-06T14:10:23+00:00
cover: "/posts/book-hauls/2021-01-book-haul.png"
description: "Here’s my book haul for January.\n

A shout to [standardebooks.org](https://standardebooks.org/)."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

Here’s my book haul for January.

A shout to [standardebooks.org](https://standardebooks.org/).