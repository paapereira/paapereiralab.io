---
title: "Books Schedule update"
slug: "book-schedule-may-2022"
author: "Paulo Pereira"
date: 2022-05-27T16:12:56+01:00
lastmod: 2022-05-27T16:12:56+01:00
description: "May Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my May update for my books to read schedule for the next months:

* **[Reading]** To Green Angel Tower (Memory, Sorrow, and Thorn 3) by Tad Williams
* Heretics of Dune (Dune 5) by Frank Herbert
* The Lost World by Michael Crichton
* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* Chapterhouse: Dune (Dune 6) by Frank Herbert
* The Handmaid's Tale by Margaret Atwood
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* Revelation Space (Revelation Space 1) by Alastair Reynolds

I also have always an audiobook going too:

* **[Reading]** Andrea Vernon and the Corporation for UltraHuman Protection by Alexander C. Kane
* Battleship Indomitable (Galactic Liberation 2) by David Vandyke & B. V. Larson
* Cain by José Saramago
* The Dark Half by Stephen King