---
title: "Harry Potter and the Sorcerer's Stone (Harry Potter Book 1) by J.K. Rowling (1997)"
slug: "harry-potter-and-the-sorcerers-stone-harry-potter-book-1"
author: "Paulo Pereira"
date: 2024-06-01T16:30:00+01:00
lastmod: 2024-06-01T16:30:00+01:00
cover: "/posts/books/harry-potter-and-the-sorcerers-stone-harry-potter-book-1.jpg"
description: "Finished “Harry Potter and the Sorcerer's Stone (Harry Potter Book 1)” by J.K. Rowling."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - J.K. Rowling
book series:
  - Harry Potter Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 8/10 Books
tags:
  - Book Log
  - J.K. Rowling
  - Harry Potter Series
  - Fiction
  - Fantasy
  - 8/10 Books
  - Ebook
---

Finished “Harry Potter and the Sorcerer's Stone”.
* Author: [J.K. Rowling](/book-authors/j.k.-rowling)
* First Published: [June 26, 1997](/book-publication-year/1990s)
* Series: [Harry Potter](/book-series/harry-potter-series) Book 1
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/61209488-harry-potter-and-the-sorcerer-s-stone)
>
> Harry Potter has never even heard of Hogwarts when the letters start dropping on the doormat at number four, Privet Drive. Addressed in green ink on yellowish parchment with a purple seal, they are swiftly confiscated by his grisly aunt and uncle. Then, on Harry's eleventh birthday, a great beetle-eyed giant of a man called Rubeus Hagrid bursts in with some astonishing news: Harry Potter is a wizard, and he has a place at Hogwarts School of Witchcraft and Wizardry. An incredible adventure is about to begin!