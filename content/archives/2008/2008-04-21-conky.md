---
title: "Conky"
slug: "conky"
author: "Paulo Pereira"
date: 2008-04-21T18:31:00+01:00
lastmod: 2008-04-21T18:31:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Conky
aliases:
  - /posts/conky/
---

[Conky](http://conky.sourceforge.net/) (1.5.1) is an application that posts system monitoring info onto your Desktop. It is very light-weighted and very cool to play with.

## Steps to install

In the Terminal:

- install conky

```bash
sudo apt-get install conky
```

- get a configuration example file (check a nicer one bellow)

```bash
zcat /usr/share/doc/conky/examples/conkyrc.sample.gz > ~/.conkyrc 
```

## Run \*conky\* in the Terminal

To have *conky* automatically start when you boot Ubuntu, do the following:

- create a script to run *conky* (`vi conky_start.sh`), with the following

```bash
#!/bin/bash
sleep 20 && conky
```

-  make sure the script is executable

```bash
chmod a+x conky_start.sh
```

-  try it

```bash
conky_start.sh
```

- Go to System > Preferences > Sessions and add a new Startup Program with:

```text
Name: conky
Command: /home/your_user/your_script_folder/conky_start.sh
Comment: Start conky
```

To change the way *conky* displays information for you, you need to change the *.conkyrc* file in your home folder (Ctrl+H in Nautilus if you don't see it).

Here's a good configuration file to start, shared in [Ubuntu Forums](http://ubuntuforums.org/showthread.php?t=205865):

```text
# UBUNTU-CONKY# A comprehensive conky script, configured for use on# Ubuntu / Debian Gnome, without the need for any external scripts.## Based on conky-jc and the default .conkyrc.# INCLUDES:# - tail of /var/log/messages# - netstat connections to your computer## -- Pengo (conky@pengo.us)#
# Create own window instead of using desktop (required in nautilus)own_window yesown_window_type overrideown_window_transparent yesown_window_hints undecorated,below,sticky,skip_taskbar,skip_pager
# Use double buffering (reduces flicker, may not work for everyone)double_buffer yes
# fiddle with windowuse_spacer yesuse_xft no
# Update interval in secondsupdate_interval 3.0
# Minimum size of text area# minimum_size 250 5
# Draw shades?draw_shades no
# Text stuffdraw_outline no # amplifies text if yesdraw_borders nofont arialuppercase no # set to yes if you want all text to be in uppercase
# Stippled borders?stippled_borders 3
# border marginsborder_margin 9
# border widthborder_width 10
# Default colors and also border colors, grey90 == #e5e5e5default_color grey
own_window_colour brownown_window_transparent yes
# Text alignment, other possible values are commented#alignment top_leftalignment top_right#alignment bottom_left#alignment bottom_right
# Gap between borders of screen and textgap_x 10gap_y 10
# stuff after 'TEXT' will be formatted on screen
TEXT$color${color orange}SYSTEM ${hr 2}$color$nodename $sysname $kernel on $machine
${color orange}CPU ${hr 2}$color${freq}MHz   Load: ${loadavg}   Temp: ${acpitemp}$cpubar${cpugraph 000000 ffffff}NAME             PID       CPU%      MEM%${top name 1} ${top pid 1}   ${top cpu 1}    ${top mem 1}${top name 2} ${top pid 2}   ${top cpu 2}    ${top mem 2}${top name 3} ${top pid 3}   ${top cpu 3}    ${top mem 3}${top name 4} ${top pid 4}   ${top cpu 4}    ${top mem 4}
${color orange}MEMORY / DISK ${hr 2}$colorRAM:   $memperc%   ${membar 6}$colorSwap:  $swapperc%   ${swapbar 6}$color
Root:  ${fs_free_perc /}%   ${fs_bar 6 /}$colorhda1:  ${fs_free_perc /media/hda1}%   ${fs_bar 6 /media/hda1}$colorhdb3:  ${fs_free_perc /media/hdb3}%   ${fs_bar 6 /media/hdb3}
${color orange}NETWORK (${addr eth0}) ${hr 2}$colorDown: $color${downspeed eth0} k/s ${alignr}Up: ${upspeed eth0} k/s${downspeedgraph eth0 25,140 000000 ff0000} ${alignr}${upspeedgraph eth025,140 000000 00ff00}$colorTotal: ${totaldown eth0} ${alignr}Total: ${totalup eth0}Inbound: ${tcp_portmon 1 32767 count} Outbound: ${tcp_portmon 3276861000 count}${alignr}Total: ${tcp_portmon 1 65535 count}
${color orange}LOGGING ${hr 2}$color${execi 30 tail -n3 /var/log/messages | fold -w50}
${color orange}FORTUNE ${hr 2}$color${execi 120 fortune -s | fold -w50}
```

## Tips

- Look for the free space file systems conf and change it to display your own filesystems

> home:  ${fs_free_perc /home}%   ${fs_bar 6 /home}$color

- Check to see if your using *eth0* as your network interface

> ${color orange}NETWORK (${addr eth0}) ${hr 2}$color

- The comments in the configuration file are very useful to get you started in your own *conky* setup

- Check the variables and config file settings available
  - [variables](http://conky.sourceforge.net/variables.html)
  - [config file settings](http://conky.sourceforge.net/config_settings.html) 

## Flickering problem

If your have a flickering problem with conky add *Load "dbe"*, to your X11 configuration file. It should look like this:

```bash
sudo vi /etc/X11/xorg.conf
```

```text
Section "Module"
  Load    "dbe"
EndSection
```