---
title: "Morning Star (Red Rising Saga Book 3) by Pierce Brown (2016)"
slug: "morning-star-red-rising-saga-book-3"
author: "Paulo Pereira"
date: 2021-11-02T22:00:00+00:00
lastmod: 2021-11-02T22:00:00+00:00
cover: "/posts/books/morning-star-red-rising-saga-book-3.jpg"
description: "Finished “Morning Star (Red Rising Saga Book 3)” by Pierce Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - Pierce Brown
book series:
  - Red Rising Saga Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Pierce Brown
  - Red Rising Saga Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
---

Finished “Morning Star”.
* Author: [Pierce Brown](/book-authors/pierce-brown)
* First Published: [February 9th 2016](/book-publication-year/2016)
* Series: [Red Rising Saga](/book-series/red-rising-saga-series) Book 3
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/24685115-morning-star)
>
> Red Rising thrilled readers and announced the presence of a talented new author. Golden Son changed the game and took the story of Darrow to the next level. Now comes the exhilarating conclusion to the Red Rising Trilogy: Morning Star.
> 
> Darrow would have lived in peace, but his enemies brought him war. The Gold overlords demanded his obedience, hanged his wife, and enslaved his people. But Darrow is determined to fight back. Risking everything to transform himself and breach Gold society, Darrow has battled to survive the cutthroat rivalries that breed Society’s mightiest warriors, climbed the ranks, and waited patiently to unleash the revolution that will tear the hierarchy apart from within.
> 
> Finally, the time has come.
> 
> But devotion to honor and hunger for vengeance run deep on both sides. Darrow and his comrades-in-arms face powerful enemies without scruple or mercy. Among them are some Darrow once considered friends. To win, Darrow will need to inspire those shackled in darkness to break their chains, unmake the world their cruel masters have built, and claim a destiny too long denied—and too glorious to surrender.