---
title: "Flagship Victory (Galactic Liberation Book 3) by David VanDyke and B.V. Larson (2018)"
slug: "flagship-victory-galactic-liberation-book-3"
author: "Paulo Pereira"
date: 2022-07-31T15:00:00+01:00
lastmod: 2022-07-31T15:00:00+01:00
cover: "/posts/books/flagship-victory-galactic-liberation-book-3.jpg"
description: "Finished “Flagship Victory (Galactic Liberation Book 3)” by David VanDyke and B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - David VanDyke
  - B.V. Larson
book series:
  - Galactic Liberation Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - David VanDyke
  - B.V. Larson
  - Galactic Liberation Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Flagship Victory”.
* Author: [David VanDyke](/book-authors/david-vandyke) and [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [March 20th 2018](/book-publication-year/2018)
* Series: [Galactic Liberation](/book-series/galactic-liberation-series) Book 3
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/39853093-flagship-victory)
>
> Commodore Straker faces new enemies as neighboring interstellar empires decide his New Republic is a growing threat. A two-front war develops, and the Liberator must face both the Mutuality and the Hundred Worlds. Even as the fighting rages, the mysterious Opters are on the move. They make a diplomatic effort to reach out to Straker--but can he trust the Hive Masters? Will they help, or stab him in the back?