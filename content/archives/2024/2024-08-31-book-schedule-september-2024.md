---
title: "Books Schedule update"
slug: "book-schedule-september-2024"
author: "Paulo Pereira"
date: 2024-08-31T20:30:00+01:00
lastmod: 2024-08-31T20:30:00+01:00
description: "September Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my September TBR (To Be Read) update for the next months.

* **[Read]** Lord of the Flies by William Golding
* **[Read]** Silo Stories by Hugh Howey
* **[Read]** The Perfect Son by Freida McFadden
* **[Read]** Inhibitor Phase (Revelation Space #4) by Alastair Reynolds
* **[Read]** The Godfather (The Godfather #1) by Mario Puzo
* **[Read]** Rocannon's World (Hainish Cycle) by Ursula K. le Guin
* **[Reading]** The Expert System's Brother (The Expert System's Brother #1) by Adrian Tchaikovsky
* Snow Crash by Neal Stephenson
* Tress of the Emerald Sea by Brandon Sanderson
* The Spoils of War (The Damned #3) by Alan Dean Foster
* Hawksbill Station by Robert Silverberg
* Harry Potter and the Chamber of Secrets (Harry Potter #2) by J. K. Rowling
* Star Wars: Episode VI: Return of the Jedi by James Kahn
* Small Favor (The Dresden Files #10) by Jim Butcher
* The Subtle Art of Not Giving a F*ck by Mark Manson
* The Andromeda Evolution (Andromeda #2) by Daniel H. Wilson & Michael Crichton
* Mystery Walk by Robert R. McCammon
* Way Station by Clifford D. Simak
* Aurora Rising (Prefect Dreyfus Emergency #1) by Alastair Reynolds
* The Sicilian (The Godfather #2) by Mario Puzo
* Planet of Exile (Hainish Cycle) by Ursula K. le Guin
* We Can Build You by Philip K. Dick

I also have always an audiobook going too:

* **[Read]** Throne World (Undying Mercenaries #21) by B. V. Larson
* **[Reading]** Desperation by Stephen King
* Breakfast at Tiffany's by Truman Capote
* Red Company: Invasion (Red Company #4) by B. V. Larson
* The Richest Man in Babylon by George S. Clason
* Impact Winter #1 by Travis Beacham
* A History of Video Games by Jeremy Parish
* The Sandman: Act III by Neil Gaiman

Spooky Halloween month draft selection for this year:

* The Curse of the Mummy's Tomb (Goosebumps #5) by R. L. Stine
* Let's Get Invisible (Goosebumps #6) by R. L. Stine
* Horns by Joe Hill
* My Best Friend's Exorcism by Grady Hendrix
* The Surrogate Mother by Freida McFadden
