---
title: "Disable the touchpad while typing"
slug: "touchpad-synaptics"
author: "Paulo Pereira"
date: 2017-11-07T11:45:22+00:00
lastmod: 2017-11-07T11:45:22+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Synaptics
---

Here's how I set things up to automatically disable the touchpad while typing in my laptop, using the driver's automatic palm detection.

These are just my notes. Check the entire Arch Wiki entry on [Touchpad Synaptics](https://wiki.archlinux.org/index.php/Touchpad_Synaptics).

First of all you should test if it works properly for your touchpad and if the settings are accurate. Enable palm detection with `synclient PalmDetect=1`.

Then test the typing. You can tweak the detection by setting the minimum width for the touch to be considered a palm, for example `synclient PalmMinWidth=8`.

And you can tweak the minimum pressure required for the touch to be considered a palm, for example `synclient PalmMinZ=100`.

Tip: To help find the best values for palm detection, you can use [evtest](https://www.archlinux.org/packages/?name=evtest) to see the width and Z values reported during touchpad use.

Once you have found the correct settings, you can add them to your config file:

```bash
sudo vi /etc/X11/xorg.conf.d/70-synaptics.conf
```
```text
Option "PalmDetect" "1"
Option "PalmMinWidth" "8"
Option "PalmMinZ" "100"
```

Warning: For some touchpads, an [issue](https://bugzilla.kernel.org/show_bug.cgi?id=77161) with the kernel can cause the palm width to always be reported as 0. This breaks palm detection in a majority of cases. Pending an actual fix, you can [patch](https://gist.github.com/silverhammermba/a231c8156ecaa63c86f1) the synaptics package to use only Z for palm detection.

Tip: If you experience problems with consistent palm detection for your hardware, an alternative to try is [libinput](https://wiki.archlinux.org/index.php/Libinput).
