---
title: "The Good Girl by Mary Kubica (2014)"
slug: "the-good-girl"
author: "Paulo Pereira"
date: 2018-07-08T21:53:00+01:00
lastmod: 2018-07-08T21:53:00+01:00
cover: "/posts/books/the-good-girl.jpg"
description: "Finished “The Good Girl” by Mary Kubica."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - Mary Kubica
book series:
  - 
book genres:
  - Fiction
  - Mystery
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Mary Kubica
  - Fiction
  - Mystery
  - 5/10 Books
  - Audiobook
---

Finished “The Good Girl”.
* Author: [Mary Kubica](/book-authors/mary-kubica)
* First Published: [July 29th 2014](/book-publication-year/2014)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/37929849-the-good-girl)
>
> I've been following her for the past few days. I know where she buys her groceries, where she works. I don't know the color of her eyes or what they look like when she's scared. But I will.
>
> One night, Mia Dennett enters a bar to meet her on-again, off-again boyfriend. But when he doesn't show, she unwisely leaves with an enigmatic stranger. At first Colin Thatcher seems like a safe one-night stand. But following Colin home will turn out to be the worst mistake of Mia's life.
>
> When Colin decides to hide Mia in a secluded cabin in rural Minnesota instead of delivering her to his employers, Mia's mother, Eve, and detective Gabe Hoffman will stop at nothing to find them. But no one could have predicted the emotional entanglements that eventually cause this family's world to shatter.