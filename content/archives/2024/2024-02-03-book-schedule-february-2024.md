---
title: "Books Schedule update"
slug: "book-schedule-february-2024"
author: "Paulo Pereira"
date: 2024-02-03T13:00:00+00:00
lastmod: 2024-02-03T13:00:00+00:00
description: "February Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my February TBR (To Be Read) update for the next months.

* **[Read]** The Hero of Ages (The Mistborn Saga #3) by Brandon Sanderson
* **[Read]** A Call to Arms (The Damned #1) by Alan Dean Foster
* **[Read]** Precious Little Things by Adrian Tchaikovsky
* **[Read]** Star Wars Episode IV: A New Hope by George Lucas
* **[Reading]** Proven Guilty (The Dresden Files #8) by Jim Butcher
* Tau Zero by Poul Anderson
* The Night Boat by Robert R. McCammon
* Micro by Michael Crichton
* Supernova Era by Cixin Liu
* Cycle of the Werewolf by Stephen King
* Galactic North by Alastair Reynolds
* Made Things by Adrian Tchaikovsky
* Shift (Silo #2) by Hugh Howey
* Elantris by Brandon Sanderson
* Nightflyers & Other Stories by George R. R. Martin
* The False Mirror (The Damned #2) by Alan Dean Foster
* Star Wars: Episode V: The Empire Strikes Back by Donald F. Glut
* Harry Potter and the Sorcerer's Stone (Harry Potter #1) by J. K. Rowling
* Stranger in a Strange Land by Robert A. Heinlein

I also have always an audiobook going too:

* **[Read]** Accessory to War by Neil Degrasse Tyson
* **[Read]** The Regulators by Stephen King/Richard Bachman
* **[Reading]** Red Company: Discovery (Red Company #2) by B. V. Larson
* The Sandman (Book #1) by Neil Gaiman
* The Universe in a Nutshell by Stephen Hawking
* The Green Mile by Stephen King
* Total Recall: My Unbelievably True Life Story by Arnold Schwarzenegger
* Minority Report and Other Stories by Philip K. Dick
* The Compound Effect by Darren Hardy