---
title: "Podcast Server"
slug: "podcast-server"
author: "Paulo Pereira"
date: 2012-08-05T22:31:00+01:00
lastmod: 2012-08-05T22:31:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
  - b3caster
  - GitHub
---

Inspired by [Farhad's](http://slexy.org/view/s2MxMjFIYN) approach of [bashpodder](http://lincgeek.org/bashpodder), as featured in a [Linux Action Show](http://www.jupiterbroadcasting.com/) episode about bash scripts, I decided to create a Podcast Server of my own.

b3caster, as I'm calling it is a podcast server based on [dircaster](http://www.dircaster.org/) and [bashpodder](http://lincgeek.org/bashpodder).

The idea is to have a podcast server that downloads and feeds my favorite podcasts.

![b3caster](/posts/2012/2012-08-05-podcast-server/github.png)

## Components

b3caster needs the following components to work:

- Linux
- Web Server
- PHP
- cron

I'm using my B3 Server for this.

## github

For this project I decided to create a [github](https://github.com/) account. You can follow the evolution of [b3caster there](https://github.com/paapereira/b3caster).

## The Setup

- Copy the DirCaster version I provide in github, to your web server, for example:

> /www/dircaster

- Copy all the scripts and config files to a folder in your server, for example:

> /home/b3caster

- Go through all the scripts and change the necessary configs to adapt to your setup. I'm setting up a wiki in github to explain this better.  In the meantime, if you look at the code it's not hard to understand  what to change.
- Have cron run the b3caster.sh script with the frequency you prefer, for example:

> 00 06 * * * /home/b3caster/b3caster.sh

And that's it. Just point your favorite podcast client to http://yoursite.com/DirCaster/dircaster.php and your good to go.

## Available scripts

- caster_zones.sh

A caster zone is what you consider being *your* podcast server. You can  have many caster zones and a caster zone can have one or more podcasts.

All you need to do is change the caster_zones.conf file and add as many as you want.

- downloader.sh

This is the script that downloads all the podcasts. Add your favorite ones in the urls.conf file.

- subscriptions.sh

Here you configure what podcasts your caster zone have. Edit the subscriptions.conf file.

- b3caster.sh

This is the main script that runs in cron and creates the caster zones and copy the podcasts to each zone.

## Next steps

This scripts may still have bugs and you may need to change some things to get things going. Follow me at [github](https://github.com/paapereira/b3caster) for changes in the scripts and feel free to comment, send ideas and open issues :)