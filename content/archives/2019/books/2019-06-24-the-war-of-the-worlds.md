---
title: "The War of the Worlds by H.G. Wells (1898)"
slug: "the-war-of-the-worlds"
author: "Paulo Pereira"
date: 2019-06-24T22:11:00+01:00
lastmod: 2019-06-24T22:11:00+01:00
cover: "/posts/books/the-war-of-the-worlds.jpg"
description: "Finished “The War of the Worlds” by H.G. Wells."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - H.G. Wells
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
  - Classics
book scores:
  - 7/10 Books
tags:
  - Book Log
  - H.G. Wells
  - Fiction
  - Science Fiction
  - Classics
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/the-war-of-the-worlds
---

Finished “The War of the Worlds”.
* Author: [H.G. Wells](/book-authors/h.g.-wells)
* First Published: [May 1898](/book-publication-year/1800s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17838874-the-war-of-the-worlds)
>
> First published by H. G. Wells in 1898, The War of the Worlds is the granddaddy of all alien invasion stories. The novel begins ominously, as the lone voice of a narrator intones, "No one would have believed in the last years of the 19th century that this world was being watched keenly and closely by intelligences greater than man's."
>
> Things then progress from a series of seemingly mundane reports about odd atmospheric disturbances taking place on Mars to the arrival of Martians just outside of London. At first, the Martians seem laughable, hardly able to move in Earth's comparatively heavy gravity, even enough to raise themselves out of the pit created when their spaceship landed. But soon the Martians reveal their true nature as death machines 100 feet tall rise up from the pit and begin laying waste to the surrounding land. Wells quickly moves the story from the countryside to the evacuation of London itself and the loss of all hope as England's military suffers defeat after defeat.
>
> With horror, the narrator describes how the Martians suck the blood from living humans for sustenance and how it's clear that man is not being conquered so much as corralled.