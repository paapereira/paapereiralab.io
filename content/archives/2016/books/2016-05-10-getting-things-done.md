---
title: "Getting Things Done by David Allen (2001)"
slug: "getting-things-done"
author: "Paulo Pereira"
date: 2016-05-10T22:49:00+01:00
lastmod: 2016-05-10T22:49:00+01:00
cover: "/posts/books/getting-things-done.jpg"
description: "Finished “Getting Things Done: The Art of Stress-Free Productivity” by David Allen."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2001
book authors:
  - David Allen
book series:
  - 
book genres:
  - Nonfiction
  - Business
  - Productivity
book scores:
  - 10/10 Books
tags:
  - Book Log
  - David Allen
  - Nonfiction
  - Business
  - Productivity
  - 10/10 Books
  - Audiobook
---

Finished “Getting Things Done: The Art of Stress-Free Productivity”.
* Author: [David Allen](/book-authors/david-allen)
* First Published: [2001](/book-publication-year/2001)
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25834292-getting-things-done)
>
> With first-chapter allusions to martial arts, "flow," "mind like water," and other concepts borrowed from the East (and usually mangled), you'd almost think this self-helper from David Allen should have been called Zen and the Art of Schedule Maintenance.
>
> Not quite. Yes, Getting Things Done offers a complete system for downloading all those free-floating gotta-do's clogging your brain into a sophisticated framework of files and action lists--all purportedly to free your mind to focus on whatever you're working on. However, it still operates from the decidedly Western notion that if we could just get really, really organized, we could turn ourselves into 24/7 productivity machines. (To wit, Allen, whom the New Economy bible Fast Company has dubbed "the personal productivity guru," suggests that instead of meditating on crouching tigers and hidden dragons while you wait for a plane, you should unsheathe that high-tech saber known as the cell phone and attack that list of calls you need to return.)
>
> As whole-life-organizing systems go, Allen's is pretty good, even fun and therapeutic. It starts with the exhortation to take every unaccounted-for scrap of paper in your workstation that you can't junk, The next step is to write down every unaccounted-for gotta-do cramming your head onto its own scrap of paper. Finally, throw the whole stew into a giant "in-basket"
>
> That's where the processing and prioritizing begin; in Allen's system, it get a little convoluted at times, rife as it is with fancy terms, subterms, and sub-subterms for even the simplest concepts. Thank goodness the spine of his system is captured on a straightforward, one-page flowchart that you can pin over your desk and repeatedly consult without having to refer back to the book. That alone is worth the purchase price. Also of value is Allen's ingenious Two-Minute Rule: if there's anything you absolutely must do that you can do right now in two minutes or less, then do it now, thus freeing up your time and mind tenfold over the long term. It's commonsense advice so obvious that most of us completely overlook it, much to our detriment; Allen excels at dispensing such wisdom in this useful, if somewhat belabored, self-improver aimed at everyone from CEOs to soccer moms (who we all know are more organized than most CEOs to start with).