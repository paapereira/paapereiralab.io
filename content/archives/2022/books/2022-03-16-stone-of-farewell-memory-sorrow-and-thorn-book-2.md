---
title: "Stone of Farewell (Memory, Sorrow, and Thorn Book 2) by Tad Williams (1990)"
slug: "stone-of-farewell-memory-sorrow-and-thorn-book-2"
author: "Paulo Pereira"
date: 2022-03-16T19:30:00+00:00
lastmod: 2022-03-16T19:30:00+00:00
cover: "/posts/books/stone-of-farewell-memory-sorrow-and-thorn-book-2.jpg"
description: "Finished “Stone of Farewell (Memory, Sorrow, and Thorn Book 2)” by Tad Williams."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Tad Williams
book series:
  - Memory, Sorrow, and Thorn Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Tad Williams
  - Memory, Sorrow, and Thorn Series
  - Fiction
  - Fantasy
  - 10/10 Books
  - Ebook
---

Finished “Stone of Farewell”.
* Author: [Tad Williams](/book-authors/tad-williams)
* First Published: [August 7th 1990](/book-publication-year/1990s)
* Series: [Memory, Sorrow, and Thorn](/book-series/memory-sorrow-and-thorn-series) Book 2
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8498843-stone-of-farewell)
>
> It is a time of darkness, dread, and ultimate testing for the realm of Osten Ard, for the wild magic and terrifying minions of the undead Sithi ruler, Ineluki the Storm King, are spreading their seemingly undefeatable evil across the kingdom.
> 
> With the very land blighted by the power of Ineluki’s wrath, the tattered remnants of a once-proud human army flee in search of a last sanctuary and rallying point—the Stone of Farewell, a place shrouded in mystery and ancient sorrow.
> 
> An even as Prince Josua seeks to rally his scattered forces, Simon and the surviving members of the League of the Scroll are desperately struggling to discover the truth behind an almost-forgotten legend, which will take them from the fallen citadels of humans to the secret heartland of the Sithi—where near-immortals must at last decide whether to ally with the race of men in a final war against those of their own blood.
