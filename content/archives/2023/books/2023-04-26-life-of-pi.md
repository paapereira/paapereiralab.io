---
title: "Life of Pi by Yann Martel (2001)"
slug: "life-of-pi"
author: "Paulo Pereira"
date: 2023-04-26T21:00:00+01:00
lastmod: 2023-04-26T21:00:00+01:00
cover: "/posts/books/life-of-pi.jpg"
description: "Finished “Life of Pi” by Yann Martel."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2001
book authors:
  - Yann Martel
book series:
  - 
book genres:
  - Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Yann Martel
  - Fiction
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/life-of-pi
---

Finished “Life of Pi”.
* Author: [Yann Martel](/book-authors/yann-martel)
* First Published: [September 11, 2001](/book-publication-year/2001)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/33120273-life-of-pi)
>
> After the sinking of a cargo ship, a solitary lifeboat remains bobbing on the wild blue Pacific. The only survivors from the wreck are a sixteen-year-old boy named Pi, a hyena, a wounded zebra, an orangutan—and a 450-pound royal bengal tiger. The scene is set for one of the most extraordinary and beloved works of fiction in recent years.