---
title: "Improve pacman performance and add some flare"
slug: "pacman-performance-flare"
author: "Paulo Pereira"
date: 2021-06-05T15:12:45+01:00
lastmod: 2021-06-05T15:12:45+01:00
description: "How to improve pacman performance and add some flare."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - pacman
---

Want to improve pacman performance and add some flare to it?

Add `Color`, `ILoveCandy` and `ParallelDownloads` options in `pacman.conf`.

```bash
sudo vi /etc/pacman.conf
```

Here's my 'Misc options' section:

```text
# Misc options
#UseSyslog
Color
#TotalDownload
CheckSpace
VerbosePkgLists
ILoveCandy
ParallelDownloads = 5
```