---
title: "Welcome to Dead House (Goosebumps Book 1) by R.L. Stine (1992)"
slug: "welcome-to-dead-house-goosebumps-book-1"
author: "Paulo Pereira"
date: 2022-10-02T21:00:00+01:00
lastmod: 2022-10-02T21:00:00+01:00
cover: "/posts/books/welcome-to-dead-house-goosebumps-book-1.jpg"
description: "Finished “Welcome to Dead House (Goosebumps Book 1)” by R.L. Stine."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - R.L. Stine
book series:
  - Goosebumps Series
book genres:
  - Fiction
  - Horror
book scores:
  - 6/10 Books
tags:
  - Book Log
  - R.L. Stine
  - Goosebumps Series
  - Fiction
  - Horror
  - 6/10 Books
  - Ebook
---

Finished “Welcome to Dead House”.
* Author: [R.L. Stine](/book-authors/r.l.-stine)
* First Published: [July 1, 1992](/book-publication-year/1990s)
* Series: [Goosebumps](/book-series/goosebumps-series) Book 1
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12253826-welcome-to-dead-house)
>
> Enter at your own Risk: The first ever Goosebumps. Now with creepy bonus features! 11-year-old Josh and 12-year-old Amanda just moved into the oldest and weirdest house on the block--the two siblings think it might even be haunted! But of course, their parents don't believe them. You'll get used to it, they say. Go out and make some new friends. But the creepy kids are not like anyone Josh and Amanda have ever met before. And when they take a shortcut through the cemetery one night, Josh and Amanda learn why.