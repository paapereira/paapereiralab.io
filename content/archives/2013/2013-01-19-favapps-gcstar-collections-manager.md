---
title: "FavApps: GCstar Collections Manager"
slug: "favapps-gcstar-collections-manager"
author: "Paulo Pereira"
date: 2013-01-19T15:16:00+00:00
lastmod: 2013-01-19T15:16:00+00:00
draft: false
toc: true
categories:
  - Apps
tags:
  - Linux
  - Ubuntu
  - GCStar
---

*GCstar is an application for managing your collections. It supports  many types of collections, including movies, books, games, comics,  stamps, coins, and many more. You can even create your own collection  type for whatever unique thing it is that you collect.* 
*Detailed information on each item can be automatically retrieved from the internet and you can store additional data, such as the location or who you've lent it to. You may also search and filter your collection  by many criteria.* 

I've been using GCstar for years now. I manage my [Tex](http://www.texbr.com/) collection with it.

## Installing GCstar

![GCstar](/posts/2013/2013-01-19-favapps-gcstar-collections-manager/gcstar.png)

You can install GCstar via the [Ubuntu Software Center](https://apps.ubuntu.com/cat/applications/gcstar/). You can also use the button at the beginning of this post.

To install via terminal:

```bash
sudo apt-get install gcstar
```

##  Creating a new Collection

Creating a new collection it's pretty straightforward using the available templates.

![GCstar](/posts/2013/2013-01-19-favapps-gcstar-collections-manager/gcstar1.png)

You can also create your own templates.

![GCstar](/posts/2013/2013-01-19-favapps-gcstar-collections-manager/gcstar2.png)