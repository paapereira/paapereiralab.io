---
title: "City World (Undying Mercenaries Book 17) by B.V. Larson (2022)"
slug: "city-world-undying-mercenaries-book-17"
author: "Paulo Pereira"
date: 2022-05-22T17:00:00+01:00
lastmod: 2022-05-22T17:00:00+01:00
cover: "/posts/books/city-world-undying-mercenaries-book-17.jpg"
description: "Finished “City World (Undying Mercenaries Book 17)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2022
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “City World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [April 26th 2022](/book-publication-year/2022)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 17
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/60885654-city-world)
>
> Earth’s entire fleet and all her legions are conscripted by the Galactics. Centurion McGill of Legion Varus is among countless troops pressed into service as cannon fodder for the Empire.
> 
> Not everyone on Earth is happy with the demands of the Galactics. Earth has built up her military for decades, and now she might lose everything in a foreign war no one understands. As the man who has killed more Mogwa than any other human in history, McGill is approached and given a mission. He has a fateful decision to make. Will he serve Earth’s overlords faithfully, or will he assassinate the arrogant alien leaders?