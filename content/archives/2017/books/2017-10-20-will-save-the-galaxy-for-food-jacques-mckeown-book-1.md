---
title: "Will Save the Galaxy for Food (Jacques McKeown Book 1) by Yahtzee Croshaw (2017)"
slug: "will-save-the-galaxy-for-food-jacques-mckeown-book-1"
author: "Paulo Pereira"
date: 2017-10-20T23:19:00+01:00
lastmod: 2017-10-20T23:19:00+01:00
cover: "/posts/books/will-save-the-galaxy-for-food-jacques-mckeown-book-1.jpg"
description: "Finished “Will Save the Galaxy for Food (Jacques McKeown Book 1)” by Yahtzee Croshaw."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Yahtzee Croshaw
book series:
  - Jacques McKeown Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Yahtzee Croshaw
  - Jacques McKeown Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Will Save the Galaxy for Food”.
* Author: [Yahtzee Croshaw](/book-authors/yahtzee-croshaw)
* First Published: [February 14th 2017](/book-publication-year/2017)
* Series: [Jacques McKeown](/book-series/jacques-mckeown-series) Book 1
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35187730-will-save-the-galaxy-for-food)
>
> A not-quite epic science fiction adventure about a down-on-his luck galactic pilot caught in a cross-galaxy struggle for survival!
>
> Space travel just isn't what it used to be. With the invention of Quantum Teleportation, space heroes aren't needed anymore. When one particularly unlucky ex-adventurer masquerades as famous pilot and hate figure Jacques McKeown, he's sucked into an ever-deepening corporate and political intrigue. Between space pirates, adorable deadly creatures, and a missing fortune in royalties, saving the universe was never this difficult!