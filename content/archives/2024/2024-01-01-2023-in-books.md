---
title: "2023 in Books"
slug: "2023-in-books"
author: "Paulo Pereira"
date: 2024-01-01T12:00:00+00:00
lastmod: 2024-01-01T12:00:00+00:00
cover: "/pages/library-banner.jpg"
description: "This year I read 64 books."
draft: false
toc: true
categories:
  - Books
tags:
  - Year in Books
---

## Books read in 2023

This year I read 64 books:
- 27 were [Science Fiction](/tags/science-fiction)
- 13 were [Fantasy](/tags/fantasy)
- 13 were [Horror](/tags/horror)
- 10 were [Nonfiction](/tags/nonfiction)
- 26 were [audiobooks](/tags/audiobook)
- read my first [Brandon Sanderson](/book-authors/brandon-sanderson) books (first 2 in the The Mistborn Saga)
- 12 books were 5 stars, 2 of them [10/10](/book-scores/10/10-books)

You can check the list in [Goodreads](https://www.goodreads.com/review/list/16853893-paulo-pereira?shelf=2023) and see my [2023 Year in Books](https://www.goodreads.com/user/year_in_books/2023/16853893).

![2023 in Books](/posts/2024/2024-01-01-2023-in-books/2023-in-books.png)

## Top 10 books and series this year

1. ["Boy's Life" by Robert R. McCammon](/posts/books/boys-life)
2. ["Light Bringer (Red Rising Saga #6)" by Pierce Brown](/book-series/red-rising-saga-series)
3. ["Revelation Space" by Alastair Reynolds](/book-series/revelation-space-series) (books 2 and 3, plus "Chasm City")
4. ["The Mistborn Saga" by Brandon Sanderson](/book-series/the-mistborn-saga-series) (read the first 2 books)
5. ["Lost Stars" by Claudia Gray](/posts/books/lost-stars)
6. ["Wool (Silo #1)" by Hugh Howey](/book-series/silo-series)
7. ["Bear Head (Dogs of War #2)" by Adrian Tchaikovsky](/book-series/dogs-of-war-series)
8. ["Life of Pi" by Yann Martel](/posts/books/life-of-pi)
9. ["Building a Second Brain" and "The PARA Method" by Tiago Forte](/book-authors/tiago-forte)
10. ["The Tooth Fairy" by Graham Joyce](/posts/books/the-tooth-fairy)

## Top authors read this year

- [B.V. Larson](/book-authors/b.v.-larson) (7 books read)
- [Stephen King](/book-authors/stephen-king) (6 books read)
- [Michael Crichton](/book-authors/michael-crichton) and [Alastair Reynolds](/book-authors/alastair-reynolds) (4 books read each)
- [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky), [Jim Butcher](/book-authors/jim-butcher), [Robert R. McCammon](/book-authors/robert-r.-mccammon) and [R.L. Stine](/book-authors/r.l.-stine) (3 books read each)

## 2024 TBR

Keep up with my [TBR](/tags/books-schedule/) updates through the year.

Some goals for the schedule:
- Continuing the "[Revelation Space](/book-series/revelation-space-series)" series by [Alastair Reynolds](/book-authors/alastair-reynolds).
- Continuing the "[The Dresden Files](/book-series/the-dresden-files-series)" series by [Jim Butcher](/book-authors/jim-butcher).
- Continuing the "[The Mistborn Saga](/book-series/the-mistborn-saga-series)" series by [Brandon Sanderson](/book-authors/brandon-sanderson).
- Continuing the "[Silo](/book-series/silo-series)" series by [Hugh Howey](/book-authors/hugh-howey).
- Continue with my 'constant' authors:
  - [Stephen King](/book-authors/stephen-king)
  - [Michael Crichton](/book-authors/michael-crichton)
  - [B.V. Larson](/book-authors/b.v.-larson)
- Adding to this 'constant' list:
  - [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
  - [Robert R. McCammon](/book-authors/robert-r.-mccammon)
- More [Star Wars Universe](/book-series/star-wars-universe) books
- Re-read "The Lord of the Rings" by J. R. R. Tolkien ?
- Harry Potter ?