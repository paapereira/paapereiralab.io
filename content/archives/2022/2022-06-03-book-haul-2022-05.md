---
title: "Book Haul for May 2022"
slug: "book-haul-2022-05"
author: "Paulo Pereira"
date: 2022-06-03T15:36:40+01:00
lastmod: 2022-06-03T15:36:40+01:00
cover: "/posts/book-hauls/2022-05-book-haul.png"
description: "May book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

May book haul.