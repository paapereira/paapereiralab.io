---
title: "The Rum Diary by Hunter S. Thompson (1998)"
slug: "the-rum-diary"
author: "Paulo Pereira"
date: 2015-03-15T23:12:00+00:00
lastmod: 2015-03-15T23:12:00+00:00
cover: "/posts/books/the-rum-diary.jpg"
description: "Finished “The Rum Diary” by Hunter S. Thompson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Hunter S. Thompson
book series:
  - 
book genres:
  - Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Hunter S. Thompson
  - Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Rum Diary”.
* Author: [Hunter S. Thompson](/book-authors/hunter-s.-thompson)
* First Published: [1998](/book-publication-year/1990s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/1531349.The_Rum_Diary)
>
> Begun in 1959 by a twenty-two-year-old Hunter S. Thompson, The Rum Diary is a tangled love story of jealousy, treachery, and violent alcoholic lust in the Caribbean boomtown that was San Juan, Puerto Rico, in the late 1950s. The narrator, freelance journalist Paul Kemp, irresistibly drawn to a sexy, mysterious woman, is soon thrust into a world where corruption and get-rich-quick schemes rule and anything (including murder) is permissible.