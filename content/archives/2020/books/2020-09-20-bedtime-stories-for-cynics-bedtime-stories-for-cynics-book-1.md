---
title: "Bedtime Stories for Cynics by Dave Hill (2017)"
slug: "bedtime-stories-for-cynics-bedtime-stories-for-cynics-book-1"
author: "Paulo Pereira"
date: 2020-09-20T17:27:00+01:00
lastmod: 2020-09-20T17:27:00+01:00
cover: "/posts/books/bedtime-stories-for-cynics-bedtime-stories-for-cynics-book-1.png"
description: "Finished “Bedtime Stories for Cynics (Bedtime Stories for Cynics Book 1)” by Dave Hill."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Dave Hill
book series:
  - Bedtime Stories for Cynics Series
book genres:
  - Fiction
  - Humor
book scores:
  - 4/10 Books
tags:
  - Book Log
  - Dave Hill
  - Bedtime Stories for Cynics Series
  - Fiction
  - Humor
  - 4/10 Books
  - Audiobook
---

Finished “Bedtime Stories for Cynics”.
* Author: [Dave Hill](/book-authors/dave-hill)
* First Published: [November 10th 2017](/book-publication-year/2017)
* Series: [Bedtime Stories for Cynics](/book-series/bedtime-stories-for-cynics-series) Book 1
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/45701569-bedtime-stories-for-cynics)
>
> The closest you'll get to having Parks and Recreation's Ron Swanson read you a bedtime story, this sharp and silly series features inappropriate children's stories for adults only – performed by masters of the comedic arts.