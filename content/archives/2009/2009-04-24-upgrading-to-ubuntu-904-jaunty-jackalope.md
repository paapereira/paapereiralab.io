---
title: "Upgrading to Ubuntu 9.04 Jaunty Jackalope"
slug: "upgrading-to-ubuntu-904-jaunty-jackalope"
author: "Paulo Pereira"
date: 2009-04-24T11:29:00+01:00
lastmod: 2009-04-24T11:29:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 9.04
  - Jaunty Jackalope
  - HP dv5-1020ep
aliases:
  - /posts/upgrading-to-ubuntu-904-jaunty-jackalope/
---

Yesterday Ubuntu 9.04, codename Jaunty Jackalope, was [released](http://www.ubuntu.com/products/whatisubuntu/904features/).

Time to upgrade :-) First in my [laptop](/posts/hp-pavilion-dv5-1020ep-ubuntu-64-bits/).

I will be the using the 64 bit alternate cd to upgrade. This means that the upgrade will have a cd image as his source, and not the Internet. This way it's much faster, I think.

I considered a clean install in order to create ext4 partitions, but after reading around I decided not to, because I saw some "your data may be lost" comments.

ext4 is not the default filesystem in Ubuntu 9.04, so you don't need to think about it. Clean intall or upgrade you're good to go.

## Before you start

- Check for updates using Update Manager (be sure to have all the available upgrades done)
- Backup your data
- Check the [Release Notes](http://www.ubuntu.com/getubuntu/releasenotes/904)
- [Download](http://www.ubuntu.com/getubuntu/download) Ubuntu (I used the [Text based “alternate installer” installation disk](http://www.ubuntu.com/getubuntu/downloadmirrors#alternate) link to get the alternate image)
- Check the file hash (example: `md5sum ubuntu-9.04-alternate-amd64.iso`) and compare it to [UbuntuHashes](https://help.ubuntu.com/community/UbuntuHashes)
- Be sure to upgrade from Ubuntu 8.10.
- Use [Gmount-iso](http://www.ubuntugeek.com/easy-way-of-mountunmount-iso-images-in-ubuntu.html) to mount the iso file. This way cd could be mounted without having to burn a cd.

## Install

In the Terminal, go to the cd mount point (example: */media/cdrom*) and type:

```bash
cd /media/cdrom
gksu "sh /cdrom/cdromupgrade"
```

And now we wait...

When is asked if I want to include the latest updates during the upgrade I always say no. At this times the servers are very slow - everyone want their Ubuntu :-)

During the upgrade there may be some questions about replacing some configuration files. I replaced them all with the new versions.

After restarting there may be more updates to do.

## Issues / Post-Install

**Issue 1:** No sound. It seems the problem is with the new 2.6.28-11 kernel. If I  reboot to the previous 2.6... sound works fine. For now that's what I'll do.

Update 1:

I followed [this instructions](http://ubuntuforums.org/showthread.php?t=789578) and I now have sound if I use headphones, but the columns still don't work.

Update 2:

After trying out a lot of things it seems all I had to do was reinstall pulseaudio

```bash
sudo apt-get remove pulseaudio
sudo apt-get install pulseaudio
```

- **Issue 2:** Missing software, installed in *Add/Remove*.

- gVim
- VLC Player
- GNOME Do (`sudo apt-get install gnome-do`)

- **Issue 3:** Needed to add my [NFS Shares](/posts/nfs-file-sharing/) again.

- **Issue 4:** Updated Software Sources

```text
deb http://ppa.launchpad.net/do-core/ppa/ubuntu jaunty main
deb http://packages.medibuntu.org/ jaunty free non-free
deb http://linux.getdropbox.com/ubuntu jaunty main
deb http://ppa.launchpad.net/openoffice-pkgs/ubuntu/ jaunty main
```

That's that :-) For now...