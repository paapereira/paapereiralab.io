---
title: "Different Seasons by Stephen King (1982)"
slug: "different-seasons"
author: "Paulo Pereira"
date: 2021-04-12T21:00:00+01:00
lastmod: 2021-04-12T21:00:00+01:00
cover: "/posts/books/different-seasons.jpg"
description: "Finished “Different Seasons” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
  - Short Stories
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - Short Stories
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/different-seasons
---

Finished “Different Seasons”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [August 27th 1982](/book-publication-year/1980s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31812706-different-seasons)
>
> A "hypnotic" (The New York Times Book Review) collection of four novellas from Stephen King bound together by the changing of seasons, each taking on the theme of a journey with strikingly different tones and characters.
> "The wondrous readability of his work, as well as the instant sense of communication with his characters, are what make Stephen King the consummate storyteller that he is," hailed the Houston Chronicle about Different Seasons.
>
> This gripping collection begins with "Rita Hayworth and the Shawshank Redemption," in which an unjustly imprisoned convict seeks a strange and startling revenge—the basis for the Best Picture Academy Award-nominee The Shawshank Redemption. Next is "Apt Pupil," the inspiration for the film of the same name about top high school student Todd Bowden and his obsession with the dark and deadly past of an older man in town. In "The Body," four rambunctious young boys plunge through the façade of a small town and come face-to-face with life, death, and intimations of their own mortality. This novella became the movie Stand By Me. Finally, a disgraced woman is determined to triumph over death in "The Breathing Method."