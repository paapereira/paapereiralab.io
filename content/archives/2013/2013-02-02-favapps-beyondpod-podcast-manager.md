---
title: "FavApps: BeyondPod Podcast Manager"
slug: "favapps-beyondpod-podcast-manager"
author: "Paulo Pereira"
date: 2013-02-02T21:35:00+00:00
lastmod: 2013-02-02T21:35:00+00:00
draft: false
toc: false
categories:
  - Apps
tags:
  - Android
  - BeyondPod Podcast Manager
---

*Full featured Podcast Manager (podcatcher) and RSS feed reader.
Subscribe to RSS/Atom feeds, download, listen or watch podcasts directly on your device. Google Reader integration, flexible download scheduler and many more.*

![Google Play](/posts/2013/2013-02-02-favapps-beyondpod-podcast-manager/googleplay.png)

After using Google Listen for my podcast needs from the start, after Google discontinued it BeyondPod as been my daily podcast manager.

![Beyondpod](/posts/2013/2013-02-02-favapps-beyondpod-podcast-manager/beyondpod.png)

I specially like is scheduler options to keep my playlist fresh.

I'm also using my [b3caster](/tags/b3caster) podcast server to keep all my feeds in one place.