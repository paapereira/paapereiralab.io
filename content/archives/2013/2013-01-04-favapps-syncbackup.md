---
title: "FavApps: syncBackup"
slug: "favapps-syncbackup"
author: "Paulo Pereira"
date: 2013-01-04T09:00:00+00:00
lastmod: 2013-01-04T09:00:00+00:00
draft: false
toc: true
categories:
  - Apps
tags:
  - Linux
  - Ubuntu
  - syncBackup
---

[Ubuntu Software Center](https://apps.ubuntu.com/cat/applications/syncbackup/)

*It takes advantage of the delta-transfer algorithm, which reduces the amount of data sent over the network by sending only the differences  between the source files and the existing files in the destination.  Create custom profiles and save each configuration independently. Simple control of source and destination rules, register directories or files  to be skipped, access remote location using SSH protocol and more.*

I've been using syncBackup basically since it was the app pick in [The Linux Action Show](http://www.jupiterbroadcasting.com/25276/pc-bsd-9-1-review-las-s23e09/).

I use it for backing up my home partition in a spare hard drive I have.

## Installing syncBackup

You can install syncBackup via the [Ubuntu Software Center](https://apps.ubuntu.com/cat/applications/syncbackup/). You can also use the button at the beginning of this post.

To install via terminal:

```bash
sudo apt-get install syncbackup
```

## Setup

The syncBackup screen has two tabs: Configuration and Running.

![syncBackup](/posts/2013/2013-01-04-favapps-syncbackup/syncbackup1.png)

In the Configuration tab you can setup your source and destination folders and change the settings for your configuration.

Here is my setup:

- In the SOURCE I have my home directory (/home/\<user>)
- In the DESTINATION I have the location of my spare HDD (/BACKUP/\<user>)
- Review the Settings to your desire. For example, in my case, I want  to remove the files in the DESTINATION that don't exist in the SOURCE.
- Review the Exclude/Include files. In my case I assume I want a  replica of my home folder and add in the Excluded files the directories  or files I don't need to backup.  Examples: /.local/share/Trash, /.thumbnails, /.cache. In my case I also  don't backup the Videos folder because it would take all the space, so  be aware of the space you have in your DESTINATION folder.
- Don't forget to Save your configuration (e.g. /home/\<user>/syncBackup/syncBackup.conf).

After you review and save your configuration, just click Run in the  Running tab. You can see what is being updated in the log presented in  that tab.

I only have one configuration file, but I imagine that if I created two  or more, all I had to to would be loading the different files to run  different backups.

For example, have backups made in another machine using ssh. Something to try.

The only thing this app lacks is automation. As far as I know, I have to manually Run my configuration. Something involving cron would be more  interesting.

Still is a great app and I use it for my local backups.