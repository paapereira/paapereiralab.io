---
title: "Reviewing my book ratings"
slug: "book-ratings"
author: "Paulo Pereira"
date: 2020-09-27T18:23:20+01:00
lastmod: 2020-09-27T18:23:20+01:00
description: "In the process of migrating my book library to [Calibre](/posts/calibre/) I also reviewed my rating process, all my rating in [Goodreads](https://www.goodreads.com/paapereira) and my top 100 shelf."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Rating
aliases:
  - /posts/book-ratings/
---

UPDATE: check out my [current rating](/posts/book-ratings-reviewed/) system.

In the process of migrating my book library to [Calibre](/posts/calibre/) I also reviewed my rating process, all my rating in [Goodreads](https://www.goodreads.com/paapereira) and my [top 100 shelf](https://www.goodreads.com/review/list/16853893-paulo-pereira?ref=nav_mybooks&shelf=top-100).

This is my new logic for rating books:

★★★★★ - Fantastic book. A classic or a future classic. One of my favorite books.

★★★★☆ - Really enjoyed reading this book. Easy recommendation and a very good book.

★★★☆☆ - I liked it and it is a good, above average, book.

★★☆☆☆ - It was ok, but I would probably read another one.

★☆☆☆☆ - Didn't really like it.