---
title: "Theme fix after upgrading rofi to 1.7.0"
slug: "fix-theme-rofi"
author: "Paulo Pereira"
date: 2021-09-04T15:31:55+01:00
lastmod: 2021-09-04T15:31:55+01:00
description: "Fixing rofi themes."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - rofi
aliases:
  - /posts/fix-theme-rofi/
---

After upgrading to rofi 1.7.0 my themes where broken and weird.

To fix I just add to my theme.rasi files:

```bash
element-text, element-icon {
    background-color: inherit;
    text-color:       inherit;
}
```

Before the fix:

![before](/posts/2021/2021-09-04-fix-theme-rofi/rofi-before.png)

After the fix (all normal again):

![after](/posts/2021/2021-09-04-fix-theme-rofi/rofi-after.png)