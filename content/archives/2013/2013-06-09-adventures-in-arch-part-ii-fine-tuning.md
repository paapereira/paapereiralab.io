---
title: "Adventures in Arch (Part II): Fine Tuning"
slug: "adventures-in-arch-part-ii-fine-tuning"
author: "Paulo Pereira"
date: 2013-06-09T17:31:00+01:00
lastmod: 2013-11-04T17:31:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
---

In this part I will show the fine tuning I made in the system.
I will keep updating this post in the next weeks, in case I have other tips.

![Arch Linux](/posts/2013/2013-06-09-adventure-in-arch-part-i-installing/fine-tuning.png)

## fstab

Since I have the system installed in a [SSD](https://wiki.archlinux.org/index.php/SSD) drive, I changed the mount flags in fstab to trim the drive:

```bash
sudo nano /etc/fstab
```

```text
UUID=aee33023-2254-42fc-94ac-612314eeb332   /   ext4   noatime,discard,data=ordered,errors=remount-ro   0 1
UUID=114aaab1-1cb8-4853-bacc-819237d2d063   /boot   ext4   noatime,discard,data=ordered,errors=remount-ro   0 2
```

I also mounted /tmp in memory:

```text
tmpfs   /tmp   tmpfs   nodev,nosuid,noatime,mode=1777   0 0
```

## I/O Scheduler

Check your scheduler:

```bash
sudo cat /sys/block/sda/queue/scheduler
```
```text
noop deadline [cfq] 
```

Get the SSD disk ID:

```bash
sudo ls /dev/disk/by-id/
```
```text
ata-Corsair_Force_3_SSD_12066504000008910711
```

Change the scheduler to noop at boot time:

```bash
sudo nano /etc/rc.local
```
```text
    #!/bin/sh
    declare -ar SSDS=(
       'ata-Corsair_Force_3_SSD_12066504000008910711'
    )
    for SSD in "${SSDS[@]}" ; do
       BY_ID=/dev/disk/by-id/$SSD
       if [[ -e $BY_ID ]] ; then
          DEV_NAME=`ls -l $BY_ID | awk '{ print $NF }' | sed -e 's/[/\.]//g'`
          SCHED=/sys/block/$DEV_NAME/queue/scheduler
          if [[ -w $SCHED ]] ; then
             echo noop > $SCHED
          fi
       fi
    done
```

Create a rc-local.service to start with system.d (see this [tip](http://superuser.com/questions/278396/systemd-does-not-run-etc-rc-local)):
    
```bash
sudo nano /etc/systemd/system/rc-local.service
```
```text
    #  This file is part of systemd.
    #
    #  systemd is free software; you can redistribute it and/or modify it
    #  under the terms of the GNU General Public License as published by
    #  the Free Software Foundation; either version 2 of the License, or
    #  (at your option) any later version.
    
    [Unit]
    Description=/etc/rc.local Compatibility
    ConditionPathExists=/etc/rc.local 
    
    [Service]
    Type=forking
    ExecStart=/etc/rc.local start
    TimeoutSec=0
    StandardOutput=tty
    RemainAfterExit=yes
    SysVStartPriority=99 
    
    [Install]
    WantedBy=multi-user.target
```

Enable the service in systemd:

```bash
sudo systemctl enable rc-local.service
sudo systemctl start rc-local.service
```

## Swap

I didn't create a swap partition.

```bash
sudo nano /etc/sysctl.conf
```
```text
vm.swappiness = 0
```

## Preloading

More on this topic [here](https://wiki.archlinux.org/index.php/Maximizing_Performance).

```bash
sudo pacman -S preload
sudo systemctl enable preload
```

## Zram

More on this topic [here](https://wiki.archlinux.org/index.php/Maximizing_Performance).

Download from [AUR](https://aur.archlinux.org/packages/zramswap/).

```bash
cd builds
tar -xzf zramswap.tar.gz
cd zramswap
makepkg -s
sudo pacman -U zramswap-1.1-1-any.pkg.tar.xz
sudo systemctl enable zramswap.service
```

## Chrome cache

Having installed Chrome, I adapted this [script](/posts/mounting-chrome-cache-in-memory/) to move the cache into memory.

```bash
mkdir -p bin/google-chrome_cache
nano bin/google-chrome_cache/tmp_cache.sh
```
```text
    #!/bin/bash
    if test -d /tmp/google-chrome; then
      exit 0
    else
      rm -r ~/.cache/google-chrome
      mkdir /tmp/google-chrome
      ln -s /tmp/google-chrome ~/.cache/google-chrome
    fi
```
```bash
chmod +x bin/google-chrome_cache/tmp_cache.sh
nano .config/autostart/google-chrome_cache.desktop
```
```text
    [Desktop Entry]
    Type=Application
    Exec=/home/youruser/bin/google-chrome_cache/tmp_cache.sh
    Hidden=false
    NoDisplay=false
    X-GNOME-Autostart-enabled=true
    Name[en_US]=google-chrome_cache.desktop
    Name=Chrome Cache
    Comment[en_US]=Move Chrome cache to /tmp
    Comment=Move Chrome cache to /tmp
```

## Font Rendering

```bash
sudo nano /etc/pacman.conf
```
```text
    [infinality-bundle]
    SigLevel = Never
    Server = http://bohoomil.cu.cc/infinality-bundle/$arch
    
    [infinality-bundle-multilib]
    SigLevel = Never
    Server = http://bohoomil.cu.cc/infinality-bundle-multilib/$arch
```
```bash
sudo pacman-key -r 962DDE58
sudo pacman-key --lsign-key 962DDE58
sudo pacman -Syu
yaourt -S freetype2-infinality fontconfig-infinality \
	lib32-freetype2-infinality libreoffice-uglyfix-freetype2-infinality
sudo nano /etc/fonts/local.conf
```
```text
    <?xml version='1.0'?>
    <!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
    <fontconfig>
     <match target="font">
    
      <edit mode="assign" name="rgba">
       <const>rgb</const>
      </edit>
      <edit mode="assign" name="hinting">
       <bool>true</bool>
      </edit>
      <edit mode="assign" name="hintstyle">
       <const>hintslight</const>
      </edit>
      <edit mode="assign" name="antialias">
       <bool>true</bool>
      </edit>
      <edit mode="assign" name="lcdfilter">
        <const>lcddefault</const>
      </edit>
    
     </match>
    </fontconfig>
```

Check [this](https://wiki.archlinux.org/index.php/Font_Configuration) for more information.