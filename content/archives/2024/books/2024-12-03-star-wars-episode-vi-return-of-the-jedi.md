---
title: "Star Wars Episode VI: Return of the Jedi by James Kahn (1983)"
slug: "star-wars-episode-vi-return-of-the-jedi"
author: "Paulo Pereira"
date: 2024-12-03T21:00:00+00:00
lastmod: 2024-12-03T21:00:00+00:00
cover: "/posts/books/star-wars-episode-vi-return-of-the-jedi.jpg"
description: "Finished “Star Wars Episode VI: Return of the Jedi” by James Kahn."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - James Kahn
book series:
  - Star Wars Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - James Kahn
  - Star Wars Universe
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “Star Wars Episode VI: Return of the Jedi”.
* Author: [James Kahn](/book-authors/james-kahn)
* First Published: [June 6, 1983](/book-publication-year/1980s)
* Series: [Star Wars Universe](/book-series/star-wars-universe)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12400939-return-of-the-jedi)
>
> It was a dark time for the rebel alliance...Han Solo, frozen in carbonite, had been delivered into the hands of the vile gangster Jabba the Hutt. Determined to rescue him, Luke Skywalker, Princess Leia, and Lando Calrissian launched a hazardous mission against Jabba's Tatooine stronghold.The Rebel commanders gathered all the warships of the Rebel fleet into a single giant armada. And Darth Vader and the Emperor, who had ordered construction to begin on a new and even more powerful Death Star, were making plans to crush the Rebel Alliance once and for all.