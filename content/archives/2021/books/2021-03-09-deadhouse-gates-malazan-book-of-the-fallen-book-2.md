---
title: "Deadhouse Gates (Malazan Book of the Fallen Book 2) by Steven Erikson (2000)"
slug: "deadhouse-gates-malazan-book-of-the-fallen-book-2"
author: "Paulo Pereira"
date: 2021-03-09T22:00:00+00:00
lastmod: 2021-03-09T22:00:00+00:00
cover: "/posts/books/deadhouse-gates-malazan-book-of-the-fallen-book-2.jpg"
description: "Finished “Deadhouse Gates (Malazan Book of the Fallen Book 2)” by Steven Erikson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2000
book authors:
  - Steven Erikson
book series:
  - Malazan Book of the Fallen Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Steven Erikson
  - Malazan Book of the Fallen Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
---

Finished “Deadhouse Gates”.
* Author: [Steven Erikson](/book-authors/steven-erikson)
* First Published: [September 2000](/book-publication-year/2000)
* Series: [Malazan Book of the Fallen](/book-series/malazan-book-of-the-fallen-series) Book 2
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/9754945-deadhouse-gates)
>
> In the vast dominion of Seven Cities, in the Holy Desert Raraku, the seer Sha’ik and her followers prepare for the long-prophesied uprising known as the Whirlwind. Unprecedented in size and savagery, this maelstrom of fanaticism and bloodlust will embroil the Malazan Empire in one of the bloodiest conflicts it has ever known, shaping destinies and giving birth to legends.