---
title: "Public Parts by Jeff Jarvis (2011)"
slug: "public-parts"
author: "Paulo Pereira"
date: 2015-08-26T23:00:00+01:00
lastmod: 2015-08-26T23:00:00+01:00
cover: "/posts/books/public-parts.jpg"
description: "Finished “Public Parts: How Sharing in the Digital Age is Revolutionizing Life, Business, and Society” by Jeff Jarvis."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Jeff Jarvis
book series:
  - 
book genres:
  - Nonfiction
  - Technology
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Jeff Jarvis
  - Nonfiction
  - Technology
  - 5/10 Books
  - Audiobook
---

Finished “Public Parts: How Sharing in the Digital Age is Revolutionizing Life, Business, and Society”.
* Author: [Jeff Jarvis](/book-authors/jeff-jarvis)
* First Published: [January 1st 2011](/book-publication-year/2011)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12736501-public-parts)
>
> A journalist and visionary internet optimist's compelling argument for increased openness online in the digital age.