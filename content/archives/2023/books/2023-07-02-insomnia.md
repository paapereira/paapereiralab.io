---
title: "Insomnia by Stephen King (1994)"
slug: "insomnia"
author: "Paulo Pereira"
date: 2023-07-02T11:00:00+01:00
lastmod: 2023-07-02T11:00:00+01:00
cover: "/posts/books/insomnia.jpg"
description: "Finished “Insomnia” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Fantasy
  - 7/10 Books
  - Audiobook
---

Finished “Insomnia”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [January 1, 1994](/book-publication-year/1990s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/50626260-insomnia)
>
> Since his wife died, Ralph Roberts has been having trouble sleeping. Each night he wakes up a bit earlier, until he’s barely sleeping at all. During his late night walks, he observes some strange things going on in Derry, Maine. He sees colored ribbons streaming from people’s heads, two strange little men wandering around town after dark, and more. He begins to suspect that these visions are something more than hallucinations brought on by lack of sleep.
> 
> There’s a definite mean streak running through this small New England city; underneath its ordinary surface awesome and terrifying forces are at work. The dying has been going on in Derry for a long, long time. Now Ralph is part of it…and lack of sleep is the least of his worries.