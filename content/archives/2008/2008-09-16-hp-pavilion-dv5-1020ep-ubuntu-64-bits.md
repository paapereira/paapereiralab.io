---
title: "HP Pavilion dv5-1020ep (Ubuntu 64 bits)"
slug: "hp-pavilion-dv5-1020ep-ubuntu-64-bits"
author: "Paulo Pereira"
date: 2008-09-16T21:49:00+01:00
lastmod: 2008-09-16T21:49:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - HP Pavilion dv5-1020ep
aliases:
  - /posts/hp-pavilion-dv5-1020ep-ubuntu-64-bits/
---

My old laptop's battery is dying, and I used that as an excuse to buy myself a new on :-)

I had a HP Pavilion zt3240EA, and have been very happy with it. Nice machine. Still, age weights on every one.

I'm gonna be installing Ubuntu 8.04.1 LTS Desktop Edition, 64 bits version, on my new [HP Pavilion dv5-1020ep](http://h10025.www1.hp.com/ewfrf/wc/product?lc=en&dlc=en&cc=sg&product=3770477&lang=en).

## Download and Burn a CD

- Go to Ubuntu [Download Page](http://www.ubuntu.com/getubuntu/download) and download Ubuntu.
- Check the downloaded file hash. The output must be the same as you see in the [Ubuntu Hashes Page](https://help.ubuntu.com/community/UbuntuHashes).

```bash
md5sum ubuntu-8.04.1-desktop-amd64.iso
```

- Burn a cd (you can use [Brasero](http://www.gnome.org/projects/brasero/), for example, if you already use Ubuntu). 

## Running the live cd

- Boot from the cd.
- After booting it's important to check the integrity of the cd. Burning cds also fails.

## Hardware to check (during the live cd session)

- Graphics (only 1024x768, but check)
- Sound (check)
- Media buttons (check)
- Cable network (check)
- Wireless network (the proprietary Atheros wireless card was found, not tested)
- HP media command (not working)
- Integrated Webcam (check) 

## Hard Drive Partition Layout

```text
/dev/sda
  /dev/sda1   ntfs   Vista         		36002 MB
  /dev/sda    ntfs   Vista Recover   8450 MB
  /dev/sda3   ext3   /        	    10240 MB
  /dev/sda5   ext3   /home        	10240 MB
  /dev/sda6   ext3   /storage      	94072 MB
  /dev/sda7   swap                 1028 MB
```

## Post-install: Graphics Card

I installed [EnvyNG](http://albertomilone.com/index.html) using the Synaptic Package Manager (look for `envyng-gtk`) and just had to run it (Applications > System Tools > EnvyNG), after closing Synaptic (important), and choose to “Install the ATI driver (Automatic Hardware Detection)” option.

I now have a normal 1280×800 screen resolution and some desktop effects are already active.

There is an ATI Catalyst Control Center under Applications > Other, where you can tweak things up.

**Problems to Solve:** if I have Compiz on, there's a terrible flickering when playing back movies.

## Post-install: CPU Frequency Scaling

After installing Ubuntu I noticed that the frequency of both cores was always at 2 GHz (the cpu max frequency in my case).

Because this is a laptop I want the cpu to scale that frequencies as needed.

Useful tools:

```bash
sudo apt-get install cpufrequtils
```

You can check this behavior by:

- Adding the "CPU Frequency Scaling Monitor" to your Gnome Panel (add two, one for each core). Frequency should change while you work.
- Check the output of the following (here you can check the driver used - *powernow-k8* - and the current scaling policy)

```bash
cpufreq-info
```

Reference: [How to use CPU frequency scaling (cpufreq)](http://xlife.zuavra.net/index.php/70/)

After some time of research here's a very simple method (read the [Reference](http://xlife.zuavra.net/index.php/70/) for much more details):

- Go to your */etc/rc.local* file

```bash
sudo vi /etc/rc.local
```

- And add the following

```text
modprobe powernow-k8
sudo cpufreq-selector -g userspace
/etc/init.d/powernowd restart
exit 0
```

- Go to your */etc/default/powernowd* file

```bash
sudo vi /etc/default/powernowd
```

- Add the -n option

```text
OPTIONS="-q -n"
```

- Restart

Useful tips:

- Check available frequencies for your cpu (if you have more than one core also check cpu1, cpu2, etc.)

```bash
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies
```

- Check current governor

```bash
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
```

- Check available governors

```bash
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors
```

**Problems to solve:** restarting powernowd always change the governor to ondemand.

## Post-install: Wireless Network card

A warning in the Notification Area showed that a Proprietary Driver is enabled (Atheros wireless card).

Check the driver name:

```bash
lspci | grep Atheros
```

Followed this instructions from [Ubuntu Forums](http://ubuntuforums.org/showthread.php?t=908180), now I can use the wireless card, I found several networks, the signal is good, I connect with my password, but I can't get an IP... This means no Internet.

**Problems to solve:** connect to the Internet with sucess.

## Post-install: Laptop Optimizations

Here's some useful tips if you're running on a laptop:

- Go to your */etc/default/acpi-support* file

```bash
sudo vi /etc/default/acpi-support
```

- Change the ENABLE_LAPTOP_MODE option to true

```text
ENABLE_LAPTOP_MODE=true
```

- Install and run `powertop`. Follow the instructions on-screen (more info [here](http://ubuntu-tutorials.com/2008/06/19/extend-your-battery-life-with-powertop/)).

```bash
sudo apt-get install powertop
sudo powertop 
```

## Post-install: Sound Card

Sound works fine, but the Master mute button doen't mute the Front. This means that the columns still output sound.

To fix this go to System > Preferences > Sound and select the Front device in the list below.

## Post-install: File System

Besides the */* and */home* partitions, only root have permissions to write in every other partitions. In my case */storage*.

So, we have to add permissions to our user:

```bash
sudo chown -R youruser:youruser /storage
sudo chmod -R 755 /storage
```

Click [here](http://www.psychocats.net/ubuntu/mountlinux) for more info.

## Post-install: Updates

Update Manager warned me about 120 available updates. Just letting it go...

## Post-install: Multimedia

References:[Complete Streaming, Multimedia & Video How-to](http://ubuntuforums.org/showthread.php?t=766683) and [Medibuntu](https://help.ubuntu.com/community/Medibuntu)

- Repositories

```bash
sudo wget http://www.medibuntu.org/sources.list.d/hardy.list -O /etc/apt/sources.list.d/medibuntu.list
wget -q http://packages.medibuntu.org/medibuntu-key.gpg -O- | sudo apt-key add - && sudo apt-get update
```

- Flash

```bash
sudo apt-get purge flashplugin-nonfree && \
sudo mkdir /usr/lib/flashplugin-nonfree && \
sudo cp -f ~/Desktop/libflashplayer.so \
/usr/lib/flashplugin-nonfree/ && \
sudo nspluginwrapper -i \
/usr/lib/flashplugin-nonfree/libflashplayer.so && \
sudo ln -sf /usr/lib/nspluginwrapper/plugins/npwrapper.libflashplayer.so \
/etc/alternatives/firefox-flashplugin && \
sudo ln -sf /etc/alternatives/firefox-flashplugin \
/usr/lib/firefox-addons/plugins/flashplayer-alternative.so && \
sudo rm -f /usr/lib/firefox/plugins/npwrapper.libflashplayer.so
```

- Codecs and plugins (it will take quite some time)

```bash
sudo apt-get remove gnash gnash-common libflash-mozplugin \
libflashsupport mozilla-plugin-gnash swfdec-mozilla && \
sudo apt-get install alsa-oss faac faad flashplugin-nonfree \
gstreamer0.10-ffmpeg gstreamer0.10-plugins-bad \
gstreamer0.10-plugins-bad-multiverse gstreamer0.10-plugins-ugly \
gstreamer0.10-plugins-ugly-multiverse ia32-libs icedtea-gcjwebplugin \
liblame0 non-free-codecs openjdk-6-jre unrar
```

- Audio and Video Streaming

```bash
sudo apt-get remove kaffeine-mozilla mozilla-helix-player \
mozilla-mplayer mozilla-plugin-vlc totem-mozilla xine-plugin && \
sudo apt-get install gnome-mplayer gecko-mediaplayer
```

- Playing Encrypted DVDs

```bash
sudo apt-get install libdvdcss2 libdvdread3 libdvdnav4 build-essential debhelper fakeroot regionset
```

- Changing your [DVD Region Code](http://en.wikipedia.org/wiki/DVD_region_codes) (be sure there’s a DVD in the drive)

```bash
sudo regionset
```

Check the [reference](http://ubuntuforums.org/showthread.php?t=766683) link, because it as many other things you’ll want.

## Post-install: Preferences and Look and Feel

Here's some things I changed right away:

- 4 workspaces
- Automatic login (System > Administration > Login Window, Security tab)
- Add Network Manager and Battery Charge Monitor to the Panel
- Background
- NTP Time Server
- Weather on Clock Preferences 

## Post-install: NFS file sharing

To configure two Ubuntu machines to share files using NFS, check [here](/posts/nfs-file-sharing/).

## Post-install: SAMBA Server

Go to Applications > Add/Remove and install Samba.

Go to System > Administration > Samba to add shares and define samba users.

Also check this [post](/posts/apache-php-and-mysql/).

## Post-install: LAMP Server

To install Apache, MySQL and PHP, check [here](/posts/apache-php-and-mysql/).

## Post-install: extra software

List of extra installation software installed:

- [Firefox ](http://beta.foxmarks.com/)[Foxmarks](http://beta.foxmarks.com/) add-on (can’t live without it)
- [GnomeDo](/posts/gnomedo-05_29/)
- VLC
- [Conky](/posts/conky/)
- [Synergy](/posts/synergy/)
- GVim
- lm-sensors
- Network Selector
- [VirtualBox](/posts/virtualbox-20/)
- Gaupol Subtitle Editor
- More Firefox add-ons (BugMeNot, Forecastfox, Twitkit, Portuguese Dictionary)
- Firefox IMDB search-engine
- Gnucash
- [Google Earth](http://earth.google.com/download-earth.html) (it will download a .bin installation file, run it in the Terminal)
- aspell-pt-pt
- Mail Notification