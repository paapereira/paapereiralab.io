---
title: "The False Mirror (The Damned Book 2) by Alan Dean Foster (1992)"
slug: "the-false-mirror-the-damned-book-2"
author: "Paulo Pereira"
date: 2024-05-26T21:00:00+01:00
lastmod: 2024-05-26T21:00:00+01:00
cover: "/posts/books/the-false-mirror-the-damned-book-2.jpg"
description: "Finished “The False Mirror (The Damned Book 2)” by Alan Dean Foster."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Alan Dean Foster
book series:
  - The Damned Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Alan Dean Foster
  - The Damned Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “The False Mirror”.
* Author: [Alan Dean Foster](/book-authors/alan-dean-foster)
* First Published: [January 1, 1992](/book-publication-year/1990s)
* Series: [The Damned](/book-series/the-damned-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35114.The_False_Mirror)
>
> As war rages on between the Amplitur and the union of races known as the Weave, Ranji--an Ashregan warrior trained from birth to battle humans--is captured, and he soon learns of the Amplitur's vile genetic manipulations.