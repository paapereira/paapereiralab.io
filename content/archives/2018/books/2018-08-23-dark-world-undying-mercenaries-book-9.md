---
title: "Dark World (Undying Mercenaries Book 9) by B.V. Larson (2018)"
slug: "dark-world-undying-mercenaries-book-9"
author: "Paulo Pereira"
date: 2018-08-23T22:28:00+01:00
lastmod: 2018-08-23T22:28:00+01:00
cover: "/posts/books/dark-world-undying-mercenaries-book-9.jpg"
description: "Finished “Dark World (Undying Mercenaries Book 9)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Dark World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [2018](/book-publication-year/2018)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 9
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40525752-dark-world)
>
> Two expanding interstellar powers are about to meet in battle.
>
> After the collapse of the Cephalopod Kingdom, Humanity claimed the three hundred rebellious worlds they left behind. But many light years away on the far side of disputed region, a rival power has begun to move. They're stealing our planets, one at a time.
>
> Earth Command decides to invade the center of the frontier to set up an advanced base. The mission to DARK WORLD is highly classified and deadly. Legion Varus spearhead’s the effort, and James McGill journeys to the stars again.
>
> How many ships do they have? How advanced is their tech? No one knows, but the campaign takes an unexpected turn immediately. What was supposed to be a snatch-and-grab turns into a bloodbath. McGill dies over and over again, but some battles must be won, even if it means perma-death.
>
> DARK WORLD is the ninth book in the Undying Mercenaries Series.