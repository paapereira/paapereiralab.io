---
title: "Mars: Our Future on the Red Planet by Leonard David (2016)"
slug: "mars-our-future-on-the-red-planet"
author: "Paulo Pereira"
date: 2021-10-31T22:00:00+00:00
lastmod: 2021-10-31T22:00:00+00:00
cover: "/posts/books/mars-our-future-on-the-red-planet.jpg"
description: "Finished “Mars: Our Future on the Red Planet” by Leonard David."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - Leonard David
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Leonard David
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “Mars: Our Future on the Red Planet”.
* Author: [Leonard David](/book-authors/leonard-david)
* First Published: [October 15th 2016](/book-publication-year/2016)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31743633-mars)
>
> An inspiring exploration of the establishment of humans on Mars tying into the National Geographic television documentary series Mars. The next frontier in space exploration is Mars, the Red Planet and human habitation of Mars isn't much farther off. In October 2015, NASA declared Mars an achievable goal; that same season, Ridley Scott and Matt Damons The Martian drew crowds into theaters, grossing over $200 million. Now the National Geographic Channel fast forwards years ahead with Mars, a six-part series documenting and dramatizing the next twenty-five years as humans land on and learn to live on Mars. Following on the visionary success of Buzz Aldrins Mission to Mars and the visual glory of Marc Kaufmans Mars Up Close, this companion book to the Nat Geo series shows the science behind the mission and the challenges awaiting those brave individuals.The book combines science, technology, photography, art, and storytelling, offering what only National Geographic can create. Clear scientific explanations, gorgeous photography from outer space and the planet itself, and dramatic scenes from the television series featuring exquisitely constructed sets made to replicate Mars, make the Mars experience real and provide amazing visuals to savor and return to again and again.