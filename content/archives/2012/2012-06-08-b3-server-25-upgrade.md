---
title: "B3 Server 2.5 Upgrade"
slug: "b3-server-25-upgrade"
author: "Paulo Pereira"
date: 2012-06-08T22:14:00+01:00
lastmod: 2012-06-08T22:14:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
---

New B3 Server version is out ([2.5](http://forum.excito.net/viewtopic.php?f=1&t=3902) & [2.5.0.1](http://forum.excito.net/viewtopic.php?f=1&t=3913)).

## Upgrade to 2.5

- Connect to your b3 server

```bash
ssh user@b3server
```

- Become root

```bash
su
```

- Upgrade instructions

```bash
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade
```

You can confirm the software version going into b3 web administration page, then Settings and Software update.

![B3 Server](/posts/2012/2012-06-08-b3-server-25-upgrade/b3server.png)

## Official Excito Forum Notes

```text
New features and major changes
* Added support for Apple TimeMachine
* Added support for Apple AirPrint
* Showing HD temperature on b3/admin page
* Allowing special characters in passwords
* Improved translations for German and Swedish
* Added ecryptfs support to kernel
* Ssl security fix
* 10+ minor bug fixes
```