---
title: "Translating SimpleX Chat to Portuguese"
slug: "translating-simplex-chat-pt"
author: "Paulo Pereira"
date: 2023-04-29T09:30:00+01:00
lastmod: 2023-04-29T09:30:00+01:00
description: ""
draft: false
toc: false
categories:
  - General
tags:
  - SimpleX Chat
---

I'm translation [SimpleX Chat](https://simplex.chat/) to Portuguese.

If you want to help check the [translation guide](https://github.com/simplex-chat/simplex-chat/blob/stable/docs/TRANSLATIONS.md) and join my effort [here](https://hosted.weblate.org/projects/simplex-chat/android/pt/).

Talk to me if you have questions:

[Invite link](https://simplex.chat/contact/#/?v=1-2&smp=smp%3A%2F%2F0YuTwO05YJWS8rkjn9eLJDjQhFKvIYd8d4xG8X1blIU%3D%40smp8.simplex.im%2FS5AjypkVk6ynYfnrtynDUNFT_qd_eGRd%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEA7Vbm8UJlN_imOm2u-7CWb4XLCQ6mBhA8irTrY7bxeXo%253D%26srv%3Dbeccx4yfxxbvyhqypaavemqurytl6hozr47wfc7uuecacjqdvwpw2xid.onion)

![QR Code](/social/simplex-chat-invite.png)
