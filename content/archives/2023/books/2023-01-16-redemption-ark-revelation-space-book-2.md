---
title: "Redemption Ark (Revelation Space Book 2) by Alastair Reynolds (2002)"
slug: "redemption-ark-revelation-space-book-2"
author: "Paulo Pereira"
date: 2023-01-16T21:00:00+00:00
lastmod: 2023-01-16T21:00:00+00:00
cover: "/posts/books/redemption-ark-revelation-space-book-2.jpg"
description: "Finished “Redemption Ark (Revelation Space Book 2)” by Alastair Reynolds."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2002
book authors:
  - Alastair Reynolds
book series:
  - Revelation Space Series
  - Revelation Space Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Alastair Reynolds
  - Revelation Space Series
  - Revelation Space Universe
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
---

Finished “Redemption Ark”.
* Author: [Alastair Reynolds](/book-authors/alastair-reynolds)
* First Published: [January 1, 2002](/book-publication-year/2002)
* Series: [Revelation Space](/book-series/revelation-space-series) Book 2
* [Revelation Space Universe](/book-series/revelation-space-universe)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6594613-redemption-ark)
>
> This stunning sequel to Revelation Space begins late in the twenty-sixth century. The human race has advanced enough to accidentally trigger alien machines designed to detect intelligent life--and destroy it.