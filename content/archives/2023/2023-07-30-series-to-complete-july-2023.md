---
title: "Books Series to complete buying"
slug: "series-to-complete-july-2023"
author: "Paulo Pereira"
date: 2023-07-30T13:30:00+01:00
lastmod: 2023-07-30T13:30:00+01:00
description: "Short list of series I'm yet to complete buying."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Wishlist
---

July update for my short list of series to complete buying.

* Humanx Commonwealth by Alan Dean Foster
* The Sun Eater by Christopher Ruocchio
* Culture by Iain M. Banks
* The Godfather by Mario Puzo
* New Crobuzon by China Mieville
* The Wayward Pines Trilogy by Blake Crouch
* The Last Kingdom by Bernard Cornwell
* The Legend of Drizzt by R. A. Salvatore
* Sookie Stackhouse by Charlaine Harris
* Star Wars: Republic Commando
* Other Star Wars Universe books
* Doctor Who