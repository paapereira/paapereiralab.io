---
title: "GCstar Personal Collections Manager"
slug: "gcstar-personal-collections-manager"
author: "Paulo Pereira"
date: 2009-11-29T19:26:00+00:00
lastmod: 2009-11-29T19:26:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 9.10
  - Karmic Koala
  - GCStar
---

I use [GCStar](http://www.gcstar.org/) to keep track of my Movie collection. It's great!

> GCstar is an application for managing your collections. It  supports many types of collections, including movies, books, games,  comics, stamps, coins, and many more. You can even create your own  collection type for whatever unique thing it is that you collect!  Detailed information on each item can be automatically retrieved from  the internet and you can store additional data, such as the location or  who you've lent it to. You may also search and filter your collections  by many criteria.

After installing Karmic the automatic  information retrieval from the Internet wasn't working. To install the  latest version that fixes this problem just do the following:

- Add the GCStar PPA to your Software Sources (*System > Administration > Software Sources*)

```text
deb http://ppa.launchpad.net/gcstar/gcstar-dev/ubuntu karmic main
```

- Install GCStar

```bash
sudo apt-get install gcstar
```