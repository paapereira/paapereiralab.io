---
title: "State of Fear by Michael Crichton (2004)"
slug: "state-of-fear"
author: "Paulo Pereira"
date: 2023-05-14T16:00:00+01:00
lastmod: 2023-05-14T16:00:00+01:00
cover: "/posts/books/state-of-fear.jpg"
description: "Finished “State of Fear” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2004
book authors:
  - Michael Crichton
book series:
  - 
book genres:
  - Fiction
  - Thriller
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Fiction
  - Thriller
  - 7/10 Books
  - Audiobook
---

Finished “State of Fear”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [December 7, 2004](/book-publication-year/2004)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/477270.State_of_Fear)
>
> In Paris, a physicist dies after performing a laboratory experiment for a beautiful visitor.
> 
> In the jungles of Malaysia, a mysterious buyer purchases deadly cavitation technology, built to his specifications.
> 
> In Vancouver, a small research submarine is leased for use in the waters off New Guinea. And in Tokyo, an intelligence agent tries to understand what it all means.
> 
> Thus begins Michael Crichton's exciting and provocative technothriller, State of Fear. Only Michael Crichton's unique ability to blend science fact and pulse-pounding fiction could bring such disparate elements to a heart-stopping conclusion.
> 
> This is Michael Crichton's most wide-ranging thriller. State of Fear takes the reader from the glaciers of Iceland to the volcanoes of Antarctica, from the Arizona desert to the deadly jungles of the Solomon Islands, from the streets of Paris to the beaches of Los Angeles. The novel races forward, taking the reader on a rollercoaster thrill ride, all the while keeping the brain in high gear. Gripping and thought-provoking, State of Fear is Michael Crichton at his very best.