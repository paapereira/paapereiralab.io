---
title: "Midnight Tides (Malazan Book of the Fallen Book 5) by Steven Erikson (2004)"
slug: "midnight-tides-malazan-book-of-the-fallen-book-5"
author: "Paulo Pereira"
date: 2021-08-05T20:00:00+01:00
lastmod: 2021-08-05T20:00:00+01:00
cover: "/posts/books/midnight-tides-malazan-book-of-the-fallen-book-5.jpg"
description: "Finished “Midnight Tides (Malazan Book of the Fallen Book 5)” by Steven Erikson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2004
book authors:
  - Steven Erikson
book series:
  - Malazan Book of the Fallen Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Steven Erikson
  - Malazan Book of the Fallen Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
---

Finished “Midnight Tides”.
* Author: [Steven Erikson](/book-authors/steven-erikson)
* First Published: [March 1st 2004](/book-publication-year/2004)
* Series: [Malazan Book of the Fallen](/book-series/malazan-book-of-the-fallen-series) Book 5
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8119976-midnight-tides)
>
> After decades of warfare, the five tribes of the Tiste Edur are united under the implacable rule of the Warlock King of the Hiroth. But the price of peace is a pact with a hidden power whose motives may be deadly. To the south, the expansionist kingdom of Lether has devoured all lesser neighbors - except the Tiste Edur.