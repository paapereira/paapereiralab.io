---
title: "10 tricks for Linux systems administrators"
slug: "10-tricks-for-linux-systems"
author: "Paulo Pereira"
date: 2008-07-23T20:18:00+01:00
lastmod: 2008-07-23T20:18:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
---

Here's a [10 tricks article](http://www.ibm.com/developerworks/linux/library/l-10sysadtips/index.html?ca=drs-) for Linux systems administrators.

You can also download the [PDF](http://download.boulder.ibm.com/ibmdl/pub/software/dw/linux/l-10sysadtips/l-10sysadtips-pdf.pdf) version.