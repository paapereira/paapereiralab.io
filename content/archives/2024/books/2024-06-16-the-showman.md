---
title: "The Showman by Simon Shuster (2024)"
slug: "the-showman"
author: "Paulo Pereira"
date: 2024-06-16T15:00:00+01:00
lastmod: 2024-06-16T15:00:00+01:00
cover: "/posts/books/the-showman.jpg"
description: "Finished “The Showman” by Simon Shuster."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2024
book authors:
  - Simon Shuster
book series:
  - 
book genres:
  - Nonfiction
  - Biography
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Simon Shuster
  - Nonfiction
  - Biography
  - 7/10 Books
  - Audiobook
---

Finished “The Showman: Inside the Invasion That Shook the World and Made a Leader of Volodymyr Zelensky”.
* Author: [Simon Shuster](/book-authors/simon-shuster)
* First Published: [January 23, 2024](/book-publication-year/2024)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/210161353-the-showman)
>
> Time correspondent Simon Shuster delivers the unmissable account of the Russian invasion of Ukraine, written and reported from inside the presidential compound in Kyiv, based on Shuster's unparalleled access to President Zelensky and his top aides.