---
title: "Carmilla by Joseph Sheridan Le Fanu (1872)"
slug: "carmilla"
author: "Paulo Pereira"
date: 2020-08-18T17:35:00+01:00
lastmod: 2020-08-18T17:35:00+01:00
cover: "/posts/books/carmilla.png"
description: "Finished “Carmilla” by Joseph Sheridan Le Fanu."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - Joseph Sheridan Le Fanu
book series:
  - 
book genres:
  - Fiction
  - Horror
  - Classics
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Joseph Sheridan Le Fanu
  - Fiction
  - Horror
  - Classics
  - 5/10 Books
  - Audiobook
---

Finished “Carmilla”.
* Author: [Joseph Sheridan Le Fanu](/book-authors/joseph-sheridan-le-fanu)
* First Published: [1872](/book-publication-year/1800s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/29606979-carmilla)
>
> One of the very first vampire thrillers, this audio adaptation follows 18-year-old Laura as she recounts the story of her mysterious, intriguing and beautiful house guest Carmilla, who is stranded in the forest after a carriage accident and taken in by Laura's widowed father. The girls develop a friendship which turns into a passionate meeting of souls. A relationship of vampire and prey, the story is told through Laura's eyes as she is drawn further into Carmilla's terrifying world of pleasure and pain.
>
> A masterpiece of erotic Gothic horror, Carmilla encompasses mystery, suspense, forbidden lust, violence...and lots of blood...