---
title: "Windows under Linux"
slug: "windows-under-linux"
author: "Paulo Pereira"
date: 2008-05-01T16:22:00+01:00
lastmod: 2008-05-01T16:22:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - VirtualBox
aliases:
  - /posts/windows-under-linux/
---

Ok, some things I may need to do in Windows (for now).

So I will install a virtual Windows machine using [VirtualBox](http://www.virtualbox.org/).

## Install

- Applications > Add/Remove Applications
- Show: All available applications
- Search: *virtualbox*
- Check *VirtualBox OSE* and Apply Changes
- Run VirtualBox (Applications > System Tools > VirtualBox OSE)

## Creating a new Virtual Machine

- Click New and follow the instructions (a virtual hard disk is needed, but you'll be easily guided through the installer)
- Go to Synaptic Package Manager
- Search for *virtualbox*
- Install virtualbox-ose module for your kernel (like *linux-image-2.6.24-16-generic*)
- Install virtualbox-ose-guest module for your kernel (like *linux-image-2.6.24-16-generic*)
- Go to System > Administration > Users and Groups
- In *Manage Groups* find *vboxusers*, click *Properties* and add your user to this Group
- Logout from Ubuntu
- Back to VirtualBox
- Go to Settings > CD/DVD-ROM and mount your Windows CD or iso Windows file
- Start the Virtual Machine and install Windows
- Tip: Click in the VirtualBox window to get the mouse pointer into Windows and click Alt+Ctrl to get the mouse pointer back to Ubuntu
- Note: I used [Brasero](http://www.gnome.org/projects/brasero/) to create a ISO image of Windows XP

## Share folders with the Virtual Machine

- In the VirtualBox window go to Devices > Install Guest Additions
- This will download an iso image file, mounting it in Windows and install *Guest Additions for Windows*
- Shutdown Windows
- Go to Settings > Shared Folder and add the folders you want to share
- Start Windows and map a Network Drive to:

```text
\\vboxsvr\yoursharedfolder
```

## VirtualBox running modes

- Windowed: in this case Windows will be displayed in a window
- Fullscreen: in this case Windows will be displayed in full screen
- Seemless mode: in this case Windows will be integrated into Gnome