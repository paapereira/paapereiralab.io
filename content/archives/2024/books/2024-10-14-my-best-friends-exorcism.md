---
title: "My Best Friend's Exorcism by Grady Hendrix (2016)"
slug: "my-best-friends-exorcism"
author: "Paulo Pereira"
date: 2024-10-14T21:00:00+01:00
lastmod: 2024-10-14T21:00:00+01:00
cover: "/posts/books/my-best-friends-exorcism.jpg"
description: "Finished “My Best Friend's Exorcism” by Grady Hendrix."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - Grady Hendrix
book series:
  - 
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Grady Hendrix
  - Fiction
  - Horror
  - 7/10 Books
  - Ebook
---

Finished “My Best Friend's Exorcism”.
* Author: [Grady Hendrix](/book-authors/grady-hendrix)
* First Published: [May 17, 2016](/book-publication-year/2016)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/41015038-my-best-friend-s-exorcism)
>
> Abby and Gretchen have been best friends since fifth grade, when they bonded over a shared love of E.T., roller-skating parties, and scratch-and-sniff stickers. But when they arrive at high school, things change. Gretchen begins to act….different. And as the strange coincidences and bizarre behavior start to pile up, Abby realizes there’s only one possible explanation: Gretchen, her favorite person in the world, has a demon living inside her. And Abby is not about to let anyone or anything come between her and her best friend. With help from some unlikely allies, Abby embarks on a quest to save Gretchen. But is their friendship powerful enough to beat the devil?