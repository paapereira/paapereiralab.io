---
title: "A Voice in the Wilderness (The Human Division Book 4) by John Scalzi (2013)"
slug: "a-voice-in-the-wilderness-the-human-division-book-4"
author: "Paulo Pereira"
date: 2019-02-06T22:13:00+00:00
lastmod: 2019-02-06T22:13:00+00:00
cover: "/posts/books/a-voice-in-the-wilderness-the-human-division-book-4.jpg"
description: "Finished “A Voice in the Wilderness (The Human Division Book 4)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “A Voice in the Wilderness”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [February 5th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 4
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17334767-a-voice-in-the-wilderness)
>
> Albert Birnbaum was once one of the biggest political talk show hosts around, but these days hes watching his career enter a death spiral. A stranger offers a solution to his woes, promising to put him back on top. Its everything Birnbaum wants, but is there a catch? And does Birnbaum actually care if there is?