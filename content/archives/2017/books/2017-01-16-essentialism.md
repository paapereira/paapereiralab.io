---
title: "Essentialism by Greg McKeown (2011)"
slug: "essentialism"
author: "Paulo Pereira"
date: 2017-01-16T21:48:00+00:00
lastmod: 2017-01-16T21:48:00+00:00
cover: "/posts/books/essentialism.jpg"
description: "Finished “Essentialism: The Disciplined Pursuit of Less” by Greg McKeown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Greg McKeown
book series:
  - 
book genres:
  - Nonfiction
  - Self Help
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Greg McKeown
  - Nonfiction
  - Self Help
  - 8/10 Books
  - Audiobook
---

Finished “Essentialism: The Disciplined Pursuit of Less”.
* Author: [Greg McKeown](/book-authors/greg-mckeown)
* First Published: [December 31st 2011](/book-publication-year/2011)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28950082-essentialism)
>
> Have you ever found yourself stretched too thin at home or at work?
>
> Do you simultaneously feel both overworked and underutilized?
>
> Do you often feel busy but not productive, like you're always in motion, but never getting anywhere?
>
> Do you feel like your time is constantly being hijacked by other people’s priorities and agendas?
>
> Have you ever said "yes" to something simply to please and then resented it afterwards?
>
> If you answered yes to any of these, the way out is the Way of the Essentialist.
>
> The Way of the Essentialist involves doing less, but better, so you can make the highest possible contribution in every area of your life.
>
> The Way of the Essentialist isn’t about getting more done in less time. It’s not about getting less done. It’s about getting only the right things done. It’s about challenging the core assumption of ‘we can have it all’ and ‘I have to do everything’ and replacing it with the pursuit of ‘the right thing, in the right way, at the right time'. It’s about regaining control of our own choices about where to spend our time and energies instead of giving others implicit permission to choose for us.
>
> In Essentialism, Greg McKeown CEO of a Leadership and Strategy agency in Silicon Valley who has taught at Apple, Google and Facebook draws on experience and insight from working with the leaders of the most innovative companies and organizations in the world to show how to achieve what he calls the disciplined pursuit of less. By applying a more selective criteria for what is essential, the pursuit of less allows us to regain control of our own choices so we can channel our time, energy and effort into making the highest possible contribution toward the goals and activities that matter.
>
> Essentialism isn’t one more thing; it is a different way of doing everything. It is a discipline you apply constantly, effortlessly. Essentialism is a mindset; a way of life. It is an idea whose time has come.