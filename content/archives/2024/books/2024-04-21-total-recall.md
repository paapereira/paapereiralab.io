---
title: "Total Recall by Arnold Schwarzenegger (2012)"
slug: "total-recall"
author: "Paulo Pereira"
date: 2024-04-21T13:00:00+01:00
lastmod: 2024-04-21T13:00:00+01:00
cover: "/posts/books/total-recall.jpg"
description: "Finished “Total Recall” by Arnold Schwarzenegger."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - Arnold Schwarzenegger
book series:
  - 
book genres:
  - Nonfiction
  - Biography
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Arnold Schwarzenegger
  - Nonfiction
  - Biography
  - 7/10 Books
  - Audiobook
---

Finished “Total Recall: My Unbelievably True Life Story”.
* Author: [Arnold Schwarzenegger](/book-authors/arnold-schwarzenegger)
* First Published: [January 1, 2012](/book-publication-year/2012)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/27235122-total-recall)
>
> Born in the small city of Thal, Austria, in 1947, Arnold Schwarzenegger moved to Los Angeles at the age of twenty-one. Within ten years, he was a millionaire businessman. After twenty years, he was the world’s biggest movie star. In 2003, he was elected governor of California and a household name around the world.
> 
> Chronicling his embodiment of the American Dream, Total Recall covers Schwarzenegger’s high-stakes journey to the United States, from creating the international bodybuilding industry out of the sands of Venice Beach, to breathing life into cinema’s most iconic characters, and becoming one of the leading political figures of our time. Proud of his accomplishments and honest about his regrets, Schwarzenegger spares nothing in sharing his amazing story.