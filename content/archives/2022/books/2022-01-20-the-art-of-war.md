---
title: "The Art of War by Sun Tzu (-500)"
slug: "the-art-of-war"
author: "Paulo Pereira"
date: 2022-01-20T22:00:00+00:00
lastmod: 2022-01-20T22:00:00+00:00
cover: "/posts/books/the-art-of-war.jpg"
description: "Finished “The Art of War” by Sun Tzu."
draft: false
toc: false
categories:
  - Books
book publication year:
  - BC
book authors:
  - Sun Tzu
book series:
  - 
book genres:
  - Nonfiction
  - Classics
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Sun Tzu
  - Nonfiction
  - Classics
  - 5/10 Books
  - Audiobook
---

Finished “The Art of War”.
* Author: [Sun Tzu](/book-authors/sun-tzu)
* First Published: [-500](/book-publication-year/bc)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25316185-the-art-of-war)
>
> The 13 chapters of The Art of War, each devoted to one aspect of warfare, were compiled by the high-ranking Chinese military general, strategist, and philosopher Sun-Tzu. In spite of its battlefield specificity, The Art of War has found new life in the modern age with leaders in fields as wide and far-reaching as world politics, human psychology, and corporate strategy finding valuable insight in its timeworn words.