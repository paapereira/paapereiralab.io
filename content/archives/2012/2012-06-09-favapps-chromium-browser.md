---
title: "FavApps: Chromium Browser"
slug: "favapps-chromium-browser"
author: "Paulo Pereira"
date: 2012-06-09T23:10:00+01:00
lastmod: 2012-06-09T23:10:00+01:00
draft: false
toc: true
categories:
  - Apps
tags:
  - Linux
  - Ubuntu
  - Chrome
  - Chromium
---

[Ubuntu Software Center](https://apps.ubuntu.com/cat/applications/chromium-browser/)

*Chromium is an open-source browser project that aims to build a  safer, faster, and more stable way for all Internet users to experience  the web.
Chromium serves as a base for Google Chrome, which is  Chromium rebranded (name and logo) with very few additions such as usage tracking and an auto-updater system.*

I've been using Chrome and Chromium for quite some time. Probably since Google launched Chrome.

It's always one of the first apps I install in a new system.

## Installing Chromium

You can install Chromium via the Ubuntu Software Center. You can also use the button at the beginning of this post.

To install via terminal:

```bash
sudo apt-get install chromium-browser chromium-browser-l10n
```

## Installing Google Chrome

If you prefer Google Chrome go to Google Chrome [website](https://www.google.com/intl/en/chrome/browser/) and download the .deb file.

I always prefer the beta [version](https://www.google.com/chrome/intl/en/eula_beta.html?dl=beta_amd64_deb).

```bash
sudo dpkg -i Downloads/google-chrome/google-chrome-beta_current_amd64.deb
```

Last time I installed Chrome I had the following error:

```text
dpkg: dependency problems prevent configuration of google-chrome-beta:
 google-chrome-beta depends on libxss1; however:
 Package libxss1 is not installed.
 google-chrome-beta depends on libcurl3; however:
 Package libcurl3 is not installed.
dpkg: error processing google-chrome-beta (--install):
 dependency problems - leaving unconfigured
```

I fixed the problem this way:

```bash
sudo apt-get -f install
```

I usually remove Firefox from the system:

```bash
sudo apt-get remove --purge firefox
```