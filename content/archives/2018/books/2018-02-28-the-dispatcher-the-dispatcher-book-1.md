---
title: "The Dispatcher (The Dispatcher Book 1) by John Scalzi (2016)"
slug: "the-dispatcher-the-dispatcher-book-1"
author: "Paulo Pereira"
date: 2018-02-28T22:24:00+00:00
lastmod: 2018-02-28T22:24:00+00:00
cover: "/posts/books/the-dispatcher-the-dispatcher-book-1.jpg"
description: "Finished “The Dispatcher (The Dispatcher Book 1)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - John Scalzi
book series:
  - The Dispatcher Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Dispatcher Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Dispatcher”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [October 4th 2016](/book-publication-year/2016)
* Series: [The Dispatcher](/book-series/the-dispatcher-series) Book 1
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/26082188-the-dispatcher)
>
> One day, not long from now, it becomes almost impossible to murder anyone - 999 times out of a thousand, anyone who is intentionally killed comes back. How? We don't know. But it changes everything: war, crime, daily life.
>
> Tony Valdez is a Dispatcher - a licensed, bonded professional whose job is to humanely dispatch those whose circumstances put them in death's crosshairs, so they can have a second chance to avoid the reaper. But when a fellow Dispatcher and former friend is apparently kidnapped, Tony learns that there are some things that are worse than death and that some people are ready to do almost anything to avenge a supposed wrong.
>
> It's a race against time for Valdez to find his friend before it's too late...before not even a Dispatcher can save him.