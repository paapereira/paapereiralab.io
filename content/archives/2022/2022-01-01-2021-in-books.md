---
title: "2021 in Books"
slug: "2021-in-books"
author: "Paulo Pereira"
date: 2022-01-01T16:40:50+00:00
lastmod: 2022-01-01T16:40:50+00:00
cover: "/pages/library-banner.jpg"
description: "This year I read 65 books."
draft: false
toc: true
categories:
  - Books
tags:
  - Year in Books
---

This year I read 65 books. I also continued the “Complete Collection Of H. P. Lovecraft” (still many stories to go).

It was my [Malazan Book of the Fallen](/book-series/malazan-book-of-the-fallen-series) series [read along](/posts/malazan-read-along/) by [Steven Erikson](/book-authors/steven-erikson) year. 3 books to go :-)

## Books read in 2021

You can check the list in [Goodreads](https://www.goodreads.com/review/list/16853893-paulo-pereira?shelf=2021) and see my [2021 Year in Books](https://www.goodreads.com/user/year_in_books/2021/16853893).

![2021 in Books](/posts/2022/2022-01-01-2021-in-books/2021-in-books.png)

## Top 10 books this year

1. ["Malazan Book of the Fallen" series by Steven Erikson](/book-series/malazan-book-of-the-fallen-series) (read 7 books)
2. ["Dune" series by Frank Herbert](/book-series/dune-series) (read 3 books)
3. ["Red Rising Saga" by Pierce Brown](/book-series/red-rising-saga-series) (read 3 books)
4. ["The Forever War" series by Joe Haldeman](/book-series/the-forever-war-series)
5. [“Childhood's End” by Arthur C. Clarke](/posts/books/childhoods-end)
6. ["Jurassic Park (Jurassic Park Book 1)" by Michael Crichton](/posts/books/jurassic-park-jurassic-park-book-1)
7. ["Project Hail Mary" by Andy Weir](/posts/books/project-hail-mary)
8. ["Sphere" by Michael Crichton](/posts/books/sphere)
9. ["Different Seasons" by Stephen King](/posts/books/different-seasons)
10. ["Kings of the Wyld (The Band Book 1)" by Nicholas Eames](/posts/books/kings-of-the-wyld-the-band-book-1)

## Top 100 updates

This year 15 books entered my top 100. I'm always updating the list, so these may in time be removed from the list. You can see the current top 100 list in [Goodreads](https://www.goodreads.com/review/list/16853893-paulo-pereira?shelf=top-100). If the book belongs to a series, I typically only add the first book in the top 100.

If I had to choose one a winner for 2021, the prize would be shared between "Malazan Book of the Fallen" series by Steven Erikson and "Dune" by Frank Herbert.

The "Red Rising Saga" by Pierce Brown and "The Forever War" by Joe Haldeman were also in my top books of the year.

These are ordered by read date (being the first the most recently read).

* [“Childhood's End” by Arthur C. Clarke](/posts/books/childhoods-end)
* ["Dune (Dune Book 1)" by Frank Herbert](/posts/books/dune-dune-book-1)
* ["Jurassic Park (Jurassic Park Book 1)" by Michael Crichton](/posts/books/jurassic-park-jurassic-park-book-1)
* ["The Forever War (The Forever War Book 1)" by Joe Haldeman](/posts/books/the-forever-war-the-forever-war-book-1)
* ["Red Rising (Red Rising Saga Book 1)" by Pierce Brown](/posts/books/red-rising-red-rising-saga-book-1)
* ["Sphere" by Michael Crichton](/posts/books/sphere)
* ["Kings of the Wyld (The Band Book 1)" by Nicholas Eames](/posts/books/kings-of-the-wyld-the-band-book-1)
* ["Project Hail Mary" by Andy Weir](/posts/books/project-hail-mary)
* ["Congo" by Michael Crichton](/posts/books/congo)
* ["Different Seasons" by Stephen King](/posts/books/different-seasons)
* ["We" by Yevgeny Zamyatin](/posts/books/we)
* ["The Andromeda Strain (Andromeda Book 1)" by Michael Crichton](/posts/books/the-andromeda-strain-andromeda-book-1)
* ["Everybody Lies" by Seth Stephens-Davidowitz](/posts/books/everybody-lies)
* ["The Adventures of Tom Sawyer (Adventures of Tom and Huck Book 1)" by Mark Twain](/posts/books/the-adventures-of-tom-sawyer-adventures-of-tom-and-huck-book-1)
* ["Gardens of the Moon (Malazan Book of the Fallen Book 1)" by Steven Erikson](/posts/books/gardens-of-the-moon-malazan-book-of-the-fallen-book-1)

## 2022 TBR

In 2022 I'll continue (and finish) the [Malazan Book of the Fallen](/posts/malazan-read-along/) series read along by Steven Erikson.

* February-March: Toll the Hounds
* April-May: Dust of Dreams
* June-July: The Crippled God

Other books I'm keeping in my list to read this year:

* Continuing the "Dune" series by Frank Herbert. I read the first 3 books in 2021.
* Continuing the "Red Rising Saga" by Pierce Brown. I read the first 3 books in 2021.
* More [Stephen King](/book-authors/stephen-king).
* More [Michael Crichton](/book-authors/michael-crichton). Read Crichton for the first time in 2021. 8 books. 
* More "[Undying Mercenaries](/book-series/undying-mercenaries-series)" by [B.V. Larson](/book-authors/b.v.-larson) when/if they are published.
* Read the "Memory, Sorrow, and Thorn" series by Tad Williams.
* Read the "Revelation Space" series by Alastair Reynolds.
* Continuing "[The Wheel of Time](/book-series/the-wheel-of-time-series)" series by Robert Jordan. This year with the "Malazan Book of the Fallen" read along I din't get to it. I'd probably start from the beginning.
* Continuing the "Complete Collection of H. P. Lovecraft" I've started in 2020. Didn't read much in 2021.
* Extended optional/desired list:
  * "Hyperion" by Dan Simmons.
  * Continuing "Space Odyssey" by Arthur C. Clarke.
  * Re-read "The Lord of the Rings" by J. R. R. Tolkien.
  * Didn't read nothing by Brandon Sanderson. Maybe this year.
  * Didn't start "The Dresden Files" series. Maybe this year.