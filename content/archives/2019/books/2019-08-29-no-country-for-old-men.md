---
title: "No Country for Old Men by Cormac McCarthy (2005)"
slug: "no-country-for-old-men"
author: "Paulo Pereira"
date: 2019-08-29T22:21:00+01:00
lastmod: 2019-08-29T22:21:00+01:00
cover: "/posts/books/no-country-for-old-men.jpg"
description: "Finished “No Country for Old Men” by Cormac McCarthy."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2005
book authors:
  - Cormac McCarthy
book series:
  - 
book genres:
  - Fiction
  - Thriller
  - Mystery
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Cormac McCarthy
  - Fiction
  - Thriller
  - Mystery
  - 6/10 Books
  - Audiobook
---

Finished “No Country for Old Men”.
* Author: [Cormac McCarthy](/book-authors/cormac-mccarthy)
* First Published: [2005](/book-publication-year/2005)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/538837.No_Country_for_Old_Men)
>
> Llewelyn Moss, hunting antelope near the Rio Grande, instead finds men shot dead, a load of heroin, and more than $2 million in cash. Packing the money out, he knows, will change everything. But only after two more men are murdered does a victim's burning car lead Sheriff Bell to the carnage out in the desert, and he soon realizes how desperately Moss and his young wife need protection. One party in the failed transaction hires an ex-Special Forces officer to defend his interests against a mesmerizing freelancer, while on either side are men accustomed to spectacular violence and mayhem. The pursuit stretches up and down and across the border, each participant seemingly determined to answer what one asks another: how does a man decide in what order to abandon his life? A harrowing story of a war that society is waging on itself, and an enduring meditation on the ties of love and blood and duty that inform lives and shape destinies, No Country for Old Men is a novel of extraordinary resonance and power.