---
title: "Origins: Fourteen Billion Years of Cosmic Evolution by Neil deGrasse Tyson and Donald Goldsmith (2004)"
slug: "origins-fourteen-billion-years-of-cosmic-evolution"
author: "Paulo Pereira"
date: 2022-01-09T18:00:00+00:00
lastmod: 2022-01-09T18:00:00+00:00
cover: "/posts/books/origins-fourteen-billion-years-of-cosmic-evolution.jpg"
description: "Finished “Origins: Fourteen Billion Years of Cosmic Evolution” by Neil deGrasse Tyson and Donald Goldsmith."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2004
book authors:
  - Neil deGrasse Tyson
  - Donald Goldsmith
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Neil deGrasse Tyson
  - Donald Goldsmith
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “Origins: Fourteen Billion Years of Cosmic Evolution”.
* Author: [Neil deGrasse Tyson](/book-authors/neil-degrasse-tyson) and [Donald Goldsmith](/book-authors/donald-goldsmith)
* First Published: [September 28th 2004](/book-publication-year/2004)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/45185635-origins)
>
> Our true origins are not just human, or even terrestrial, but in fact cosmic. Drawing on recent scientific breakthroughs and the current cross-pollination among geology, biology, astrophysics, and cosmology, Origins explains the soul-stirring leaps in our understanding of the cosmos. From the first image of a galaxy birth to Spirit Rover's exploration of Mars, to the discovery of water on one of Jupiter's moons, coauthors Neil deGrasse Tyson and Donald Goldsmith conduct a galvanizing tour of the cosmos with clarity and exuberance.