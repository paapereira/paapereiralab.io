---
title: "The Expert System’s Champion (Expert System Book 2) by Adrian Tchaikovsky (2021)"
slug: "the-expert-systems-champion-expert-system-book-2"
author: "Paulo Pereira"
date: 2025-02-18T22:00:00+00:00
lastmod: 2025-02-18T22:00:00+00:00
cover: "/posts/books/the-expert-systems-champion-expert-system-book-2.jpg"
description: "Finished “The Expert System’s Champion (Expert System Book 2)” by Adrian Tchaikovsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - Adrian Tchaikovsky
book series:
  - Expert System Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Expert System Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Ebook
---

Finished “The Expert System’s Champion”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
* First Published: [January 26, 2021](/book-publication-year/2021)
* Series: [Expert System](/book-series/expert-system-series) Book 2
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/50663061-the-expert-system-s-champion)
>
> It’s been ten years since Handry was wrenched away from his family and friends, forced to wander a world he no longer understood. But with the help of the Ancients, he has cobbled together a life, of sorts, for himself and his fellow outcasts.
> 
> Wandering from village to village, welcoming the folk that the townships abandon, fighting the monsters the villagers cannot—or dare not—his ever-growing band of misfits has become the stuff of legend, a story told by parents to keep unruly children in line.
> 
> But there is something new and dangerous in the world, and the beasts of the land are acting against their nature, destroying the towns they once left in peace.
> 
> And for the first time in memory, the Ancients have no wisdom to offer…