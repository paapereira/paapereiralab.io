---
title: "White Night (The Dresden Files Book 9) by Jim Butcher (2007)"
slug: "white-night-the-dresden-files-book-9"
author: "Paulo Pereira"
date: 2024-06-13T16:00:00+01:00
lastmod: 2024-06-13T16:00:00+01:00
cover: "/posts/books/white-night-the-dresden-files-book-9.jpg"
description: "Finished “White Night (The Dresden Files Book 9)” by Jim Butcher."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2007
book authors:
  - Jim Butcher
book series:
  - The Dresden Files Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jim Butcher
  - The Dresden Files Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “White Night”.
* Author: [Jim Butcher](/book-authors/jim-butcher)
* First Published: [April 3, 2007](/book-publication-year/2007)
* Series: [The Dresden Files](/book-series/the-dresden-files-series) Book 9
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/5973243-white-night)
>
> Wizard Harry Dresden must investigate his own flesh and blood when a series of killings strike Chicago’s magic practitioners in this novel in the #1 New York Times bestselling series.Someone is targeting the members of the city’s supernatural underclass—those who don’t possess enough power to become full-fledged wizards. Some have vanished. Others appear to be victims of suicide. But now the culprit has left a calling card at one of the crime scenes—a message for Harry Dresden.   Harry sets out to find the apparent serial killer, but his investigation turns up evidence pointing to the one suspect he cannot possibly believe his half-brother, Thomas. To clear his brother’s name, Harry rushes into a supernatural power struggle that renders him outnumbered, outclassed, and dangerously susceptible to temptation.   And Harry knows that if he screws this one up, people will die—and one of them will be his brother...