---
title: "Adventures in Arch (Part I): Installing"
slug: "adventure-in-arch-part-i-installing"
author: "Paulo Pereira"
date: 2013-06-09T16:59:00+01:00
lastmod: 2013-11-02T16:59:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
---

Inspired by the recent [Linux Action Show](http://www.jupiterbroadcasting.com/) experience with [Arch](https://www.archlinux.org/), I decided to give it a try.

I have now completely changed from Ubuntu to Arch.

Any tips or advises please let me know.

![Arch Linux](/posts/2013/2013-06-09-adventure-in-arch-part-i-installing/installing.png)

**Resources**

- [Arch](https://www.archlinux.org/) home page
- Read the [Arch Way](https://wiki.archlinux.org/index.php/The_Arch_Way)
- [Arch Wiki](https://wiki.archlinux.org/)
- [Arch Beginners Guide](https://wiki.archlinux.org/index.php/Beginners'_Guide)
- [Arch Forums](https://bbs.archlinux.org/)

**Backups**

Since I will format and install Arch I need to backup my /home partition.

I created a Ubuntu USB Live Disk, rebooted into the live environment and backed up all my files.

```bash
sudo rsync -axS --exclude='/*/.gvfs' --exclude='lost+found' /media/home/. /media/externaldrive/home/.
```

**Download Arch**

I rebooted again into Ubuntu and went to the [Arch Download Page](https://archlinux.org/download/) and downloaded the latest Arch iso.

**Create an Arch USB boot disk**

Check what your usb device is:

```bash
sudo blkid
```

Create the USB Arch boot disk:

```bash
sudo dd bs=4M if=/home/myuser/archlinux-2013.06.01-dual.iso of=/dev/sde
```

**Install Arch**

Before starting read the [Arch Beginners Guide](https://wiki.archlinux.org/index.php/Beginners'_Guide). During the installation you should have this guide handy. I used my Nexus 7.

Reboot into the USB disk.

You will start at a shell prompt.

Changing the keyboard to Portuguese (check your layout [here](https://wiki.archlinux.org/index.php/KEYMAP#Keyboard_layouts)) and the terminal font:

```bash
loadkeys pt-latin9
setfont Lat2-Terminus16
```

If you want to change the language uncomment the UTF-8 entry of that language:

```bash
nano /etc/locale.gen
```

Also:

```bash
locale-gen
export LANG=pt_PT.UTF-8
```

At this point I have Internet. Check if you have too:

```bash
ping -c 3 www.google.com
```

If at some point you don't, try:

```bash
dhcpcd
```

Also check the [Arch Beginners Guide](https://wiki.archlinux.org/index.php/Beginners'_Guide) for more information, especially if you have a wireless connection.

Now it's time to prepare the hard drives.

I have a SSD drive for the system and a HDD drive for my /home partition.

I will use [GPT](https://wiki.archlinux.org/index.php/GPT) partitioning. Check [Arch Beginners Guide](https://wiki.archlinux.org/index.php/Beginners'_Guide#Prepare_the_storage_drive) for the different options.

Check your available drives:

```bash
lsblk
```

I used [cgdisk](http://www.rodsbooks.com/gdisk/walkthrough.html) to partition the SSD and HDD drives:

```bash
cgdisk /dev/sda
```

In this drive I created 3 partitions, one for /boot/efi (1GB), another for /boot (1GB) and another for /.

I used the following options in cgdisk to create the partitions:

* o - this option will create a new partition table, destroying the existent one;
* n - create each one of the partitions, in my case I used this option 3 times. 
	* Partition number: start with 1 and go from there;
	* Start  sector number: can be the default (just press enter);
	* End sector number: you can use +1G for example or enter to choose the remaining space;
	* Hex code: use ef00 for the /boot/efi partition, ef02 for the /boot partition and 8300 for the / partition.
* x - this is the expert mode. The /boot/efi partition needs to have  a legacy BIOS bootable attribute. Use the a option to get to the set  attributes menu and for the partition number 1 (/boot/efi) choose  the legacy BIOS bootable attribute.
* w - write the changes.

For the HDD drive I did the same, but created only one partition.

```bash
cgdisk /dev/sdd
```

Now format the partitions:

```bash
mkfs.vfat /dev/sda1
mkfs.ext4 /dev/sda2
mkfs.ext4 /dev/sda3
mkfs.ext4 /dev/sdd1
```

And mount them:

```bash
mount /dev/sda3 /mnt
mkdir -p /mnt/boot/efi
mount /dev/sda2 /mnt/boot
mount /dev/sda1 /mnt/boot/efi
mkdir /mnt/home
mount /dev/sdd1 /mnt/home
```
Select a mirror for installing the system:

```bash
nano /etc/pacman.d/mirrorlist
```

Copy your preferred mirror to the first line.

Tips:

- Alt+6 to copy a Server line.
- PageUp key to scroll up.
- Ctrl+U to paste it at the top of the list.
- Ctrl+X to exit, and when prompted to save changes, press Y and Enter to use the same filename.

Refresh the packages list:

```bash
pacman -Syy
```

Install the base system:

```bash
pacstrap -i /mnt base
```

Generate an fstab:

```bash
genfstab -U -p /mnt >> /mnt/etc/fstab
```

Check if it's correct:

```bash
nano /mnt/etc/fstab
```

Chroot to the system:

```bash
arch-chroot /mnt
```

Uncomment the languages of the system (choose the UTF-8 entries):

```bash
nano /etc/locale.gen
locale-gen
```

Create the /etc/locale.conf file:

```bash
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
```

Console font and keymap:

```bash
nano /etc/vconsole.conf
```
```text
KEYMAP=pt-latin9
FONT=Lat2-Terminus16
```

Time zone:

```bash
ln -s /usr/share/zoneinfo/Europe/Lisbon /etc/localtime
```

Hardware clock:

```bash
hwclock --systohc --utc
```

Hostname:

```bash
echo myhostname > /etc/hostname
```

List your available network interfaces:

```bash
ip link
```

Enable the interface at boot time and enable the NetworkManager:

```bash
sudo systemctl enable dhcpcd@enp2s0.service
sudo systemctl enable NetworkManager.service
```

Set the root password:

```bash
passwd
```

For the bootloader I used Syslinux. Check the [Arch Beginners Guide](https://wiki.archlinux.org/index.php/Beginners'_Guide) for other options, like GRUB.

```bash
pacman -S syslinux
```

As I used GPT partitions I also need:

```bash
pacman -S gptfdisk
```

Install the bootloader:

```bash
syslinux-install_update -i -a -m
```

Check if the root partition is correct in the bootloader configuration file:

```bash
nano /boot/syslinux/syslinux.cfg
```

As I have a EFI motherboard I need to use EFISTUB.

```bash
mkdir -p /boot/efi/EFI/arch/
cp /boot/vmlinuz-linux /boot/efi/EFI/arch/vmlinuz-arch.efi
cp /boot/initramfs-linux.img /boot/efi/EFI/arch/initramfs-arch.img
cp /boot/initramfs-linux-fallback.img /boot/efi/EFI/arch/initramfs-arch-fallback.img
```
```bash
pacman -S refind-efi efibootmgr
mkdir -p /boot/efi/EFI/refind
```
~~cp /usr/lib/refind/refind_x64.efi /boot/efi/EFI/refind/refind_x64.efi~~

~~cp /usr/lib/refind/config/refind.conf /boot/efi/EFI/refind/refind.conf~~

~~cp -r /usr/share/refind/icons /boot/efi/EFI/refind/icons~~
```bash
cp /usr/share/refind/refind_x64.efi /boot/efi/EFI/refind/refind_x64.efi
cp /usr/share/refind/refind.conf-sample /boot/efi/EFI/refind/refind.conf
cp /usr/share/refind/icons /boot/efi/EFI/refind/icons
cp /usr/share/refind/drivers_x64 /boot/efi/EFI/refind/drivers
```
```bash
nano /boot/efi/EFI/refind/refind.conf
```
```text
"Boot to X"       "root=/dev/sda3 ro rootfstype=ext4 systemd.unit=graphical.target"
"Boot to console"   "root=/dev/sda3 ro rootfstype=ext4 systemd-unit=multi-user.target
```
```bash
nano /boot/efi/EFI/arch/refind_linux.conf
```
```text
"Boot to X"      "root=/dev/sda3 ro rootfstype=ext4 systemd.unit=graphical.target"
"Boot to console" "root=/dev/sda3 ro rootfstype=ext4 systemd.unit=multi-user.target"
```
~~efibootmgr -c -d /dev/sda -p 1 -w -L "rEFInd" -l '\EFI\refind\refind_x64.efi'~~
```bash
efibootmgr -c -d /dev/sda -p Y -1 /EFI/refind/refind_x64.efi -L "rEFInd"
```

Exit from the chroot environment, unmount the partitions and reboot.

```bash
exit
umount /mnt/{boot,home,boot/efi,}
reboot
```

Log in as root.

Now it's time to create a your user (more on user Management [here](https://wiki.archlinux.org/index.php/Users_and_Groups#User_management)):

```bash
useradd -m -g users -s /bin/bash youruser
chfn youruser
passwd youruser
```

Create a new group to match your user:

```bash
groupadd yourgroup
gpasswd -a youruser yourgroup
```

Add your user as a sudoer:

```bash
pacman -S sudo
vi /etc/sudoers
```

Add:

```text
youruser  ALL=(ALL) ALL
```

Install ALSA for sound (more on Sound [here](https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture)):

```bash
pacman -S alsa-utils
```

Unmute the channels:

```bash
alsamixer
```

Install the X Window System:

```bash
pacman -S xorg-server xorg-server-utils xorg-xinit
```

Install mesa for 3D support:

```bash
pacman -S mesa
```

Install a video driver. To know what chipset you have:

```bash
lspci | grep VGA
```

In my case I have a NVIDIA card:

```bash
pacman -S nvidia
```

Test X by installing the default environment:

```bash
pacman -S xorg-twm xorg-xclock xterm
```

Start the test Xorg session:

```bash
startx
```

To exit back into the shell prompt:

```bash
exit
```

Install a set of TrueType fonts:

```bash
pacman -S ttf-dejavu
```

Install a Desktop Environment. Choose [here](https://wiki.archlinux.org/index.php/Desktop_Environment).

I chose [GNOME](https://wiki.archlinux.org/index.php/GNOME):

```bash
pacman -S gnome gnome-extra
```

Install a Display Manager. Choose [here](https://wiki.archlinux.org/index.php/Display_Manager).

I chose [GDM](https://wiki.archlinux.org/index.php/GDM):

```bash
pacman -S gdm
```

Enable GDM at startup:

```bash
systemctl enable gdm.service
```

Reboot and enjoy:

```bash
reboot
```

And that is it. Arch with GNOME is installed.

Next I will create a series of other post with post install tips and tricks.