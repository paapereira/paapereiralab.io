---
title: "Shift (Silo Book 2) by Hugh Howey (2013)"
slug: "shift-silo-book-2"
author: "Paulo Pereira"
date: 2024-04-19T21:00:00+01:00
lastmod: 2024-04-19T21:00:00+01:00
cover: "/posts/books/shift-silo-book-2.jpg"
description: "Finished “Shift (Silo Book 2)” by Hugh Howey."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - Hugh Howey
book series:
  - Silo Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Hugh Howey
  - Silo Series
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
---

Finished “Shift”.
* Author: [Hugh Howey](/book-authors/hugh-howey)
* First Published: [January 28, 2013](/book-publication-year/2013)
* Series: [Silo](/book-series/silo-series) Book 2
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53513162-shift)
>
> Hugh Howey goes back to show the first days of the Silo, and the beginning of the end In 2007, the Center for Automation in Nanobiotech (CAN) outlined the hardware and software platforms that would one day allow robots smaller than human cells to make medical diagnoses, conduct repairs, and even self-propagate. In the same year, the CBS network re-aired a program about the effects of propranolol on sufferers of extreme trauma. A simple pill, it had been discovered, could wipe out the memory of any traumatic event. At almost the same moment in humanity’s broad history, mankind discovered the means for bringing about its utter downfall. And the ability to forget it ever happened.