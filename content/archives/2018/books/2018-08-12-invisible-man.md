---
title: "Invisible Man by Ralph Ellison (1952)"
slug: "invisible-man"
author: "Paulo Pereira"
date: 2018-08-12T19:10:00+01:00
lastmod: 2018-08-12T19:10:00+01:00
cover: "/posts/books/invisible-man.jpg"
description: "Finished “Invisible Man” by Ralph Ellison."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1950s
book authors:
  - Ralph Ellison
book series:
  - 
book genres:
  - Fiction
  - Classics
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Ralph Ellison
  - Fiction
  - Classics
  - 5/10 Books
  - Audiobook
---

Finished “Invisible Man”.
* Author: [Ralph Ellison](/book-authors/ralph-ellison)
* First Published: [1952](/book-publication-year/1950s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/32186668-invisible-man)
>
> Invisible Man is a milestone in American literature, a book that has continued to engage readers since its appearance in 1952. A first novel by an unknown writer, it remained on the bestseller list for sixteen weeks, won the National Book Award for fiction, and established Ralph Ellison as one of the key writers of the century. The nameless narrator of the novel describes growing up in a black community in the South, attending a Negro college from which he is expelled, moving to New York and becoming the chief spokesman of the Harlem branch of "the Brotherhood," and retreating amid violence and confusion to the basement lair of the Invisible Man he imagines himself to be. The book is a passionate and witty tour de force of style, strongly influenced by T.S. Eliot's The Waste Land, Joyce, and Dostoevsky.