---
title: "Cage of Souls by Adrian Tchaikovsky (2019)"
slug: "cage-of-souls"
author: "Paulo Pereira"
date: 2022-09-30T21:00:00+01:00
lastmod: 2022-09-30T21:00:00+01:00
cover: "/posts/books/cage-of-souls.jpg"
description: "Finished “Cage of Souls” by Adrian Tchaikovsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Adrian Tchaikovsky
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/cage-of-souls
---

Finished “Cage of Souls”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
* First Published: [April 4, 2019](/book-publication-year/2019)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53948812-cage-of-souls)
>
> The sun is bloated, diseased, dying perhaps. Beneath its baneful light, Shadrapar, last of all cities, harbours fewer than 100,000 human souls. Built on the ruins of countless civilisations, Shadrapar is a museum, a midden, an asylum, a prison on a world that is ever more alien to humanity.
> 
> Bearing witness to the desperate struggle for existence between life old and new is Stefan Advani: rebel, outlaw, prisoner, survivor. This is his testament, an account of the journey that took him into the blazing desolation of the western deserts; that transported him east down the river and imprisoned him in the verdant hell of the jungle's darkest heart; that led him deep into the labyrinths and caverns of the underworld. He will meet with monsters, madman, mutants.
> 
> The question is, which one of them will inherit this Earth?