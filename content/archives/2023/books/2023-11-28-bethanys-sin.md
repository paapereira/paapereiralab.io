---
title: "Bethany's Sin by Robert R. McCammon (1980)"
slug: "bethanys-sin"
author: "Paulo Pereira"
date: 2023-11-28T22:00:00+00:00
lastmod: 2023-11-28T22:00:00+00:00
cover: "/posts/books/bethanys-sin.jpg"
description: "Finished “Bethany's Sin” by Robert R. McCammon."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Robert R. McCammon
book series:
  - 
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Robert R. McCammon
  - Fiction
  - Horror
  - 7/10 Books
  - Ebook
---

Finished “Bethany's Sin”.
* Author: [Robert R. McCammon](/book-authors/robert-r.-mccammon)
* First Published: [January 1, 1980](/book-publication-year/1980s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/18964843-bethany-s-sin)
>
> A family moves to a small town dominated by a murderous cult Despite its eerie name, Bethany’s Sin is a pleasant place. After a life of grim poverty, this new community seems like heaven to Evan Reid and his family. With its quaint shops, manicured lawns, and fresh summer smell, the town charms the Vietnam veteran, his wife, and their daughter like nowhere else they have ever been. But beneath that cheerful façade lurks something deadly. As soon as they enter their new house, Evan is consumed by fear. He can’t place its source, but there is something about the town’s mayor, Kathryn Drago, which makes him uneasy. By day she is a harmless retired archaeologist. But at night she leads an Amazonian cult whose next ritual calls for a secret the blood of Evan Reid.