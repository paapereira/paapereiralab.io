---
title: "Blindness (Blindness Book 1) by José Saramago (1995)"
slug: "blindness-blindness-book-1"
author: "Paulo Pereira"
date: 2022-02-21T22:00:00+00:00
lastmod: 2022-02-21T22:00:00+00:00
cover: "/posts/books/blindness-blindness-book-1.jpg"
description: "Finished “Blindness (Blindness Book 1)” by José Saramago."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - José Saramago
book series:
  - Blindness Series
book genres:
  - Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - José Saramago
  - Blindness Series
  - Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Blindness”.
* Author: [José Saramago](/book-authors/josé-saramago)
* First Published: [1995](/book-publication-year/1990s)
* Series: [Blindness](/book-series/blindness-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/54901632-blindness)
>
> A city is hit by an epidemic of "white blindness" that spares no one. Authorities confine the blind to an empty mental hospital, but there the criminal element holds everyone captive, stealing food rations and assaulting women. There is one eyewitness to this nightmare who guides her charges—among them a boy with no mother, a girl with dark glasses, a dog of tears—through the barren streets, and their procession becomes as uncanny as the surroundings are harrowing. As Blindness reclaims the age-old story of a plague, it evokes the vivid and trembling horrors of the twentieth century, leaving readers with a powerful vision of the human spirit that's bound both by weakness and exhilarating strength.