---
title: "Google Play Music"
slug: "google-music"
author: "Paulo Pereira"
date: 2013-04-13T17:13:00+01:00
lastmod: 2013-04-13T17:13:00+01:00
draft: false
toc: true
categories:
  - Apps
tags:
  - Linux
  - Ubuntu
  - Ubuntu 12.10
  - Quantal Quetzal
  - Android
  - Google Music Manager
  - Nuvola Player
aliases:
  - /posts/google-music/
---

[Google Play Music](https://play.google.com/music) is finally available in Portugal!

I will show how to set up upload your music in Ubuntu using the Google Music Manager.

![Google Play Music](/posts/2013/2013-04-13-google-music/google-music.png)

## Installing Google Play Music Manager in Ubuntu

```bash
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | \
	sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/musicmanager/deb/ stable \
	main" >> /etc/apt/sources.list.d/google.list'
sudo apt-get update
sudo apt-get install google-musicmanager-beta
```

I had trouble with the version installed via ppa, it was always crashing after opening up. I found a working version [here](http://ubuntuforums.org/showthread.php?t=2130680).

To download and install this version:

```bash
wget -O google-musicmanager-beta_1.0.55.7425-r0_amd64.deb \
	http://ubuntuone.com/63gYDtAAJow4nWizbZqM6U
sudo dkpg -i google-musicmanager-beta_1.0.55.7425-r0_amd64.deb
```

If you want to start Google Music Manager automatically at start-up go to Startup Applications and add the following:

![Startup Applications](/posts/2013/2013-04-13-google-music/startup.png)

## Uploading Music

Just open Google Play Music Manager, select your Music folder and wait. If you have many songs it will be a while :)

## Installing Google Play Music in Android

_The Google Play Music app lets you listen to your music collection anywhere. All your music is stored online, so no need to worry about syncing or storage space._

![Google Play](/posts/2013/2013-04-13-google-music/play-store.png)

Go the the Google Play Store and install Google Play Music. Not only you can play your local music, but also your uploaded music. It is also possible to pin music to your device, so you don't need to be connected to Internet to listen to your favorite tunes.

## Listen to music in the Ubuntu desktop

You can listen to your upload music in the Android app and a browser.

You can also access to your music in Ubuntu by installing [Nuvola Player](http://nuvolaplayer.fenryxo.cz/home.html).

![Nuvola Player](/posts/2013/2013-04-13-google-music/nuvola.jpg)

**Installing**

```bash
sudo add-apt-repository ppa:nuvola-player-builders/stable
sudo apt-get update
sudo apt-get install nuvolaplayer
```

**Enable HTML5 Audio in Google Play Music**

Go to [Google Play Music](https://play.google.com/music), Options (icon in the Google Bar next to your account avatar), Music Labs, and enable "HTML5 Audio".

**Turn On Sound Menu Integration and Notifications in Nuvola**

In the Nuvola Player go to Service > Preferences and in the Extensions tab select "Remote Player Interface" and "Notifications".

**Log in to your Google account in Nuvola**

Log in to Google Play Music in Nuvola. You now have access to your music in the Ubuntu desktop.

In order to declutter the interface, you can hide the Nuvola tool bar and the Google Bar.

In Nuvola go to View and select "Only menubar".
Also go to Service > Preferences and in the Service tab select "Hide Google Bar".

Source: http://www.omgubuntu.co.uk/2012/11/5-nuvola-tweaks-for-an-awesome-google-play-music-experience-in-ubuntu