---
title: "The Girl with the Dragon Tattoo (Millennium Book 1) by Stieg Larsson (2005)"
slug: "the-girl-with-the-dragon-tattoo-millennium-book-1"
author: "Paulo Pereira"
date: 2013-01-01T14:20:00+00:00
lastmod: 2013-01-01T14:20:00+00:00
cover: "/posts/books/the-girl-with-the-dragon-tattoo-millennium-book-1.jpg"
description: "Finished “The Girl with the Dragon Tattoo (Millennium Book 1)” by Stieg Larsson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2005
book authors:
  - Stieg Larsson
book series:
  - Millennium Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Stieg Larsson
  - Millennium Series
  - Fiction
  - Mystery
  - 9/10 Books
  - Audiobook
---

Finished “The Girl with the Dragon Tattoo”.
* Author: [Stieg Larsson](/book-authors/stieg-larsson)
* First Published: [August 2005](/book-publication-year/2005)
* Series: [Millennium](/book-series/millennium-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6912901-the-girl-with-the-dragon-tattoo)
>
> Forty years ago, Harriet Vanger disappeared from a family gathering. Her body was never found, yet her uncle is convinced it was murder - and that the killer is a member of his own family. He employs journalist Mikael Blomkvist and the tattooed, truculent computer hacker Lisbeth Salander to investigate.
>
> When the pair link Harriet's disappearance to a number of grotesque murders from forty years ago, they begin to unravel a dark and appalling family history. But the Vangers are a secretive clan, and Blomkvist and Salander are about to find out just how far they are prepared to go to protect themselves.