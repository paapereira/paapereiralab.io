---
title: "Terminal Card"
slug: "card"
author: "Paulo Pereira"
date: 2020-05-30T13:44:22+01:00
lastmod: 2020-05-30T13:44:22+01:00
cover: "/posts/card.png"
draft: false
toc: false
aliases:
  - /pages/card
---

Get my [card](/posts/card/) from your terminal:

```bash
curl -sL card.paapereira.xyz
```
