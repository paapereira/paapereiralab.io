---
title: "Books Schedule update"
slug: "book-schedule-june-2024"
author: "Paulo Pereira"
date: 2024-06-01T15:30:00+01:00
lastmod: 2024-06-01T15:30:00+01:00
description: "June Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my June TBR (To Be Read) update for the next months.

* **[Read]** Warbreaker by Brandon Sanderson
* **[Read]** Nightflyers & Other Stories by George R. R. Martin
* **[Read]** The False Mirror (The Damned #2) by Alan Dean Foster
* **[Reading]** Harry Potter and the Sorcerer's Stone (Harry Potter #1) by J. K. Rowling
* Star Wars: Episode V: The Empire Strikes Back by Donald F. Glut
* White Night (The Dresden Files #9) by Jim Butcher
* Stranger in a Strange Land by Robert A. Heinlein
* Dragon Teeth by Michael Crichton
* They Thirst by Robert R. McCammon
* Dust (Silo #3) by Hugh Howey
* Lord of the Flies by William Golding
* Inhibitor Phase (Revelation Space #4) by Alastair Reynolds
* Rocannon's World (Hainish Cycle) by Ursula K. le Guin
* The Expert System's Brother (The Expert System's Brother #1) by Adrian Tchaikovsky
* The Godfather (The Godfather #1) by Mario Puzo
* Snow Crash by Neal Stephenson
* Tress of the Emerald Sea by Brandon Sanderson
* The Spoils of War (The Damned #3) by Alan Dean Foster
* Hawksbill Station by Robert Silverberg

I also have always an audiobook going too:

* **[Read]** The Compound Effect by Darren Hardy
* **[Read]** Red Company: Contact (Red Company #3) by B. V. Larson
* **[Read]** Super Mario: How Nintendo Conquered America by Jeff Ryan
* **[Reading]** The Showman by Simon Shuster
* Desperation by Stephen King
* The Sandman: Act II by Neil Gaiman
* Breakfast at Tiffany's by Truman Capote
* Red Company: Invasion (Red Company #4) by B. V. Larson
* The Richest Man in Babylon by George S. Clason
* Impact Winter #1 by Travis Beacham
* A History of Video Games by Jeremy Parish
