---
title: "Dexter's Final Cut (Dexter Book 7) by Jeff Lindsay (2012)"
slug: "dexters-final-cut-dexter-book-7"
author: "Paulo Pereira"
date: 2015-07-21T23:28:00+01:00
lastmod: 2015-07-21T23:28:00+01:00
cover: "/posts/books/dexters-final-cut-dexter-book-7.jpg"
description: "Finished “Dexter's Final Cut (Dexter Book 7)” by Jeff Lindsay."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - Jeff Lindsay
book series:
  - Dexter Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Jeff Lindsay
  - Dexter Series
  - Fiction
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “Dexter's Final Cut”.
* Author: [Jeff Lindsay](/book-authors/jeff-lindsay)
* First Published: [2012](/book-publication-year/2012)
* Series: [Dexter](/book-series/dexter-series) Book 7
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17288870-dexter-s-final-cut)
>
> With 1.7 million copies of the Dexter novels sold, and ever-increasing critical acclaim, Jeff Lindsay returns to his groundbreaking and beloved character with his most entertaining book yet. Get ready for a grisly send-up of Hollywood, and a full dose of dark Dexter wit.
>
> Lights. Camera. Mayhem. You won't find this story on television.
>
> Hollywood gets more than it bargained for when television's hottest star arrives at the Miami Police Department and develops an intense, professional interest in a camera-shy blood spatter analyst named Dexter Morgan.
>
> Mega-star Robert Chase is famous for losing himself in his characters. When he and a group of actors descend on the Miami Police Department for "research," Chase becomes fixated on Dexter Morgan, the blood spatter analyst with a sweet tooth for doughnuts and a seemingly average life. To perfect his role, Chase is obsessed with shadowing Dexter's every move and learning what really makes him tick. There is just one tiny problem . . . Dexter's favorite hobby involves hunting down the worst killers to escape legal justice, and introducing them to his special brand of playtime. It's a secret best kept out of the spotlight and away from the prying eyes of bloated Hollywood egos if Dexter wants to stay out of the electric chair. The last thing he needs is bright lights and the paparazzi. . . but even Dexter isn't immune to the call of fame.
>
> Jeff Lindsay's razor sharp, devilish wit, and immaculate pacing prove that he is in a class of his own, and this new novel is his most masterful creation yet.