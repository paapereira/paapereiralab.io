---
title: "lof Ubuntu eXPerience country stats (July 2008)"
slug: "lof-ubuntu-experience-country-stats"
author: "Paulo Pereira"
date: 2008-07-23T20:38:00+01:00
lastmod: 2008-07-23T20:38:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
---

I always like to check the blog statistics.

Here's an interesting one (80% is using Linux):

![country stats](/posts/2008/2008-07-23-lof-ubuntu-experience-country-stats/country-stats-20080723.png)