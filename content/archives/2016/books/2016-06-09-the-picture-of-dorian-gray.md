---
title: "The Picture of Dorian Gray by Oscar Wilde (1890)"
slug: "the-picture-of-dorian-gray"
author: "Paulo Pereira"
date: 2016-06-09T22:56:00+01:00
lastmod: 2016-06-09T22:56:00+01:00
cover: "/posts/books/the-picture-of-dorian-gray.jpg"
description: "Finished “The Picture of Dorian Gray” by Oscar Wilde."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - Oscar Wilde
book series:
  - 
book genres:
  - Classics
  - Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Oscar Wilde
  - Classics
  - Fiction
  - 7/10 Books
  - Audiobook
---

Finished “The Picture of Dorian Gray”.
* Author: [Oscar Wilde](/book-authors/oscar-wilde)
* First Published: [July 20th 1890](/book-publication-year/1800s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/23637195-the-picture-of-dorian-gray)
>
> Oscar Wilde brings his enormous gifts for astute social observation and sparkling prose to The Picture of Dorian Gray, the dreamlike story of a young man who sells his soul for eternal youth and beauty. This dandy, who remains forever unchanged---petulant, hedonistic, vain, and amoral---while a painting of him ages and grows increasingly hideous with the years, has been horrifying and enchanting readers for more than 100 years.