---
title: "So Good They Can't Ignore You by Cal Newport (2012)"
slug: "so-good-they-cant-ignore-you"
author: "Paulo Pereira"
date: 2018-02-18T18:14:00+00:00
lastmod: 2018-02-18T18:14:00+00:00
cover: "/posts/books/so-good-they-cant-ignore-you.jpg"
description: "Finished “So Good They Can't Ignore You: Why Skills Trump Passion in the Quest for Work You Love” by Cal Newport."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - Cal Newport
book series:
  - 
book genres:
  - Nonfiction
  - Business
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Cal Newport
  - Nonfiction
  - Business
  - 7/10 Books
  - Audiobook
---

Finished “So Good They Can't Ignore You: Why Skills Trump Passion in the Quest for Work You Love”.
* Author: [Cal Newport](/book-authors/cal-newport)
* First Published: [January 1st 2012](/book-publication-year/2012)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/23960946-so-good-they-can-t-ignore-you)
>
> In this eye-opening account, Cal Newport debunks the long-held belief that "follow your passion" is good advice.
>
> Not only is the cliché flawed-preexisting passions are rare and have little to do with how most people end up loving their work-but it can also be dangerous, leading to anxiety and chronic job hopping.
>
> After making his case against passion, Newport sets out on a quest to discover the reality of how people end up loving what they do. Spending time with organic farmers, venture capitalists, screenwriters, freelance computer programmers, and others who admitted to deriving great satisfaction from their work, Newport uncovers the strategies they used and the pitfalls they avoided in developing their compelling careers.
>
> Matching your job to a preexisting passion does not matter, he reveals. Passion comes after you put in the hard work to become excellent at something valuable, not before.
> In other words, what you do for a living is much less important than how you do it.
> With a title taken from the comedian Steve Martin, who once said his advice for aspiring entertainers was to "be so good they can't ignore you," Cal Newport's clearly written manifesto is mandatory reading for anyone fretting about what to do with their life, or frustrated by their current job situation and eager to find a fresh new way to take control of their livelihood. He provides an evidence-based blueprint for creating work you love.
>
> SO GOOD THEY CAN'T IGNORE YOU will change the way we think about our careers, happiness, and the crafting of a remarkable life.