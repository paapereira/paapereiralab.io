---
title: "Homepage"
---

# Welcome to paapereira.xyz

## Some things to see

* 📖 Currently reading ['Bag of Bones' by Stephen King](https://www.goodreads.com/book/show/10589.Bag_of_Bones) and ['The Way of Kings (The Stormlight Archive #1)' by Brandon Sanderson](https://www.goodreads.com/book/show/53463056)
* 📚 Full [reading list](/reading-list) from the 80s until today, [Book Log](/tags/book-log/), [Schedule](/tags/books-schedule) and [more...](/books)
* 🐧 My Linux [history](/linux) and [content](/categories/linux/)
* ⚙️ List of [favorite](/pages/favorites) stuff
* 🚀 [Get started](/posts/simplex-chat) with [SimpleX Chat](/tags/simplex-chat)
* 📬 Contact me via [SimpleX Chat](https://simplex.chat/contact/#/?v=1-2&smp=smp%3A%2F%2F0YuTwO05YJWS8rkjn9eLJDjQhFKvIYd8d4xG8X1blIU%3D%40smp8.simplex.im%2FS5AjypkVk6ynYfnrtynDUNFT_qd_eGRd%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEA7Vbm8UJlN_imOm2u-7CWb4XLCQ6mBhA8irTrY7bxeXo%253D%26srv%3Dbeccx4yfxxbvyhqypaavemqurytl6hozr47wfc7uuecacjqdvwpw2xid.onion) or read [about me](/about)
* ⚡️ Send me [a book](https://www.amazon.com/hz/wishlist/ls/1RVET8NF3QUJ2), some [Ⱥlgos](algorand://GYUPWKJ4E7IER3ITLV5UFC5DORODHXWN7ANNZ4TNVPBN3GGC3AV42ESGLY) or some [₿itcoin](https://strike.me/paapereira/)

## Latest posts