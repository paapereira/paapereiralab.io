---
title: "Gardens of the Moon (Malazan Book of the Fallen Book 1) by Steven Erikson (1999)"
slug: "gardens-of-the-moon-malazan-book-of-the-fallen-book-1"
author: "Paulo Pereira"
date: 2021-01-23T22:00:00+00:00
lastmod: 2021-01-23T22:00:00+00:00
cover: "/posts/books/gardens-of-the-moon-malazan-book-of-the-fallen-book-1.jpg"
description: "Finished “Gardens of the Moon (Malazan Book of the Fallen Book 1)” by Steven Erikson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Steven Erikson
book series:
  - Malazan Book of the Fallen Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Steven Erikson
  - Malazan Book of the Fallen Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/gardens-of-the-moon-malazan-book-of-the-fallen-book-1
---

Finished “Gardens of the Moon”.
* Author: [Steven Erikson](/book-authors/steven-erikson)
* First Published: [April 1999](/book-publication-year/1990s)
* Series: [Malazan Book of the Fallen](/book-series/malazan-book-of-the-fallen-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8134944-gardens-of-the-moon)
>
> The Malazan Empire simmers with discontent, bled dry by interminable warfare, bitter infighting and bloody confrontations with the formidable Anomander Rake and his Tiste Andii, ancient and implacable sorcerers. Even the imperial legions, long inured to the bloodshed, yearn for some respite. Yet Empress Laseen's rule remains absolute, enforced by her dread Claw assassins.
>
> For Sergeant Whiskeyjack and his squad of Bridgeburners, and for Tattersail, surviving cadre mage of the Second Legion, the aftermath of the siege of Pale should have been a time to mourn the many dead. But Darujhistan, last of the Free Cities of Genabackis, yet holds out. It is to this ancient citadel that Laseen turns her predatory gaze.
>
> However, it would appear that the Empire is not alone in this great game. Sinister, shadowbound forces are gathering as the gods themselves prepare to play their hand...
>
> Conceived and written on a panoramic scale, Gardens of the Moon is epic fantasy of the highest order--an enthralling adventure by an outstanding new voice.