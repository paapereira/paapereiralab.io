---
title: "Starship - A cross-shell prompt"
slug: "starship"
author: "Paulo Pereira"
date: 2020-12-06T14:54:55+00:00
lastmod: 2020-12-06T14:54:55+00:00
description: "[Starship](https://starship.rs/) is a minimal, blazing-fast, and infinitely customizable prompt for any shell.\n

`paru starship-bin`"
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Starship
---

[Starship](https://starship.rs/) is a minimal, blazing-fast, and infinitely customizable prompt for any shell.

You can see different options for the installation in the Starship site.

In my case, in Arch Linux:

```bash
paru starship-bin
```

After installing `starship` you need to tell you shell to use it.

In my case I'm using zsh and added the following to my `.zshrc` file:

```text
# starship shell
eval "$(starship init zsh)"
```

You can add customizations by creating a `~/.config/starship.toml` file.

This is what I have until the moment. Check the [configuration](https://starship.rs/config/) guide for more options

```text
# Don't print a new line at the start of the prompt
add_newline = false

# Single line prompt
[line_break]
disabled = true
```