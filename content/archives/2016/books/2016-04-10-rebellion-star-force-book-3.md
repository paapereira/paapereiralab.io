---
title: "Rebellion (Star Force Book 3) by B.V. Larson (2011)"
slug: "rebellion-star-force-book-3"
author: "Paulo Pereira"
date: 2016-04-10T23:31:00+01:00
lastmod: 2016-04-10T23:31:00+01:00
cover: "/posts/books/rebellion-star-force-book-3.jpg"
description: "Finished “Rebellion (Star Force Book 3)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - B.V. Larson
book series:
  - Star Force Series
book genres:
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Force Series
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Rebellion”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [August 22nd 2011](/book-publication-year/2011)
* Series: [Star Force](/book-series/star-force-series) Book 3
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12438044-rebellion)
>
> REBELLION is the turning point in the great interstellar war between all living creatures and the machines. Star Force is on the side of the machines--but for how long?
>
> In the third book of the Star Force series, Kyle Riggs learns just what kind of war Earth is caught up in. At the mercy of the Macros, his marines fight against new alien races, big and small. They battle the innocent and the vile alike, until their situation becomes grim.
>
> REBELLION is a military science fiction novel by bestselling author B. V. Larson.