---
title: "Book Haul for December 2022 + Christmas Haul"
slug: "book-haul-2022-12"
author: "Paulo Pereira"
date: 2022-12-31T19:00:00+00:00
lastmod: 2022-12-31T19:00:00+00:00
cover: "/posts/book-hauls/2022-12-book-haul.png"
description: "December and Christmas book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

December and Christmas book haul.