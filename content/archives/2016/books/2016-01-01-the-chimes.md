---
title: "The Chimes by Charles Dickens (1844)"
slug: "the-chimes"
author: "Paulo Pereira"
date: 2016-01-01T23:50:00+00:00
lastmod: 2016-01-01T23:50:00+00:00
cover: "/posts/books/the-chimes.jpg"
description: "Finished “The Chimes: A Goblin Story or Some Bells That Rang an Old Year Out and a New Year In” by Charles Dickens."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - Charles Dickens
book series:
  - 
book genres:
  - Fiction
  - Classics
book scores:
  - 3/10 Books
tags:
  - Book Log
  - Charles Dickens
  - Fiction
  - Classics
  - 3/10 Books
  - Audiobook
---

Finished “The Chimes: A Goblin Story or Some Bells That Rang an Old Year Out and a New Year In”.
* Author: [Charles Dickens](/book-authors/charles-dickens)
* First Published: [1844](/book-publication-year/1800s)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28184546-the-chimes)
>
> This classic story is the second in a series of five Christmas books Dickens was commissioned to write - beginning with A Christmas Carol. A haunting tale set on New Year's Eve, The Chimes tells the story of a poor porter named Trotty Veck who has become disheartened by the state of the world - until he is shown a series of fantastical visions that convince him of the good of humanity. Though much different from and certainly a bit darker than A Christmas Carol, the moral message of The Chimes is equally poignant - touting the importance of compassion, goodwill, and the love of friends and family.