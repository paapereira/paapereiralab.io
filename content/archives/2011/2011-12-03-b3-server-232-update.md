---
title: "B3 Server 2.3.2 Update"
slug: "b3-server-232-update"
author: "Paulo Pereira"
date: 2011-12-03T14:51:00+00:00
lastmod: 2011-12-03T14:51:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
---

Minor [update](http://forum.excito.net/viewtopic.php?f=1&t=3476).

## Upgrading to 2.3.2

- Become root

```bash
su
```

- Do an upgrade

```bash
apt-get update
apt-get upgrade
```

- Left beyond packages

```bash
apt-get install bubba-album bubba-backend libdbix-class-perl libsql-abstract-perl squeezecenter
```

- A little cleansing

```bash
apt-get autoremove
```

You can confirm the software version going into b3 web administration page, then Settings and Software update.

![B3 Server](/posts/2011/2011-12-03-b3-server-232-update/b3server.png)

## Official Excito forum notes

```text
New features and major changes
* Fixed issue in ftd causing upload via file manager to fail after 256 files
* Fixed memory leak issue in disk manager, causing UI to behave strange and some operations to fail
* Fixed memory leak issue in photo album generation
```