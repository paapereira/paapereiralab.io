---
title: "How to Build Meaningful Relationships through Conversations by Carol Ann Lloyd (2020)"
slug: "how-to-build-meaningful-relationships-through-conversations"
author: "Paulo Pereira"
date: 2021-08-25T21:00:00+01:00
lastmod: 2021-08-25T21:00:00+01:00
cover: "/posts/books/how-to-build-meaningful-relationships-through-conversations.jpg"
description: "Finished “How to Build Meaningful Relationships through Conversations” by Carol Ann Lloyd."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Carol Ann Lloyd
book series:
  - 
book genres:
  - Nonfiction
  - Self Help
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Carol Ann Lloyd
  - Nonfiction
  - Self Help
  - 5/10 Books
  - Audiobook
---

Finished “How to Build Meaningful Relationships through Conversations”.
* Author: [Carol Ann Lloyd](/book-authors/carol-ann-lloyd)
* First Published: [February 18th 2020](/book-publication-year/2020)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53483914-how-to-build-meaningful-relationships-through-conversation)
>
> We have conversations every day, but how many of those can be considered effective? The right conversation can help establish strong relationships, solve problems, create new opportunities, and build communities. The right conversation can change everything.
> 
> But how does one prepare to have a conversation in an effective way?
> 
> In 10 lectures for self-development, professional communications coach and speaker Carol Ann Lloyd teaches the best ways to communicate and listen, including how to focus on understanding, how to overcome barriers and distractions, and how to clarify intentions. When listeners step back to hear what makes conversations successful, they will learn that each component of a conversation is a piece of a larger puzzle, which only fits together when thoughtfully considered and executed.
> 
> Conversations that matter take effort, and every conversation can be R.E.A.L. (Relevant, Effective, Affirming, Legitimate.) Carol Ann Lloyd also shares the three pitfalls in tough conversations and shows how to avoid them. By the end of this course, listeners will have a new understanding of the way people communicate. What’s more, they’ll develop the confidence to live the life they want to live—one conversation at a time.