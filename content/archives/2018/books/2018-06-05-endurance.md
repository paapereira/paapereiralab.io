---
title: "Endurance: Shackleton's Incredible Voyage by Alfred Lansing (1959)"
slug: "endurance"
author: "Paulo Pereira"
date: 2018-06-05T22:54:00+01:00
lastmod: 2018-06-05T22:54:00+01:00
cover: "/posts/books/endurance.jpg"
description: "Finished “Endurance: Shackleton's Incredible Voyage” by Alfred Lansing."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1950s
book authors:
  - Alfred Lansing
book series:
  - 
book genres:
  - Nonfiction
  - History
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Alfred Lansing
  - Nonfiction
  - History
  - 5/10 Books
  - Audiobook
---

Finished “Endurance: Shackleton's Incredible Voyage”.
* Author: [Alfred Lansing](/book-authors/alfred-lansing)
* First Published: [January 1st 1959](/book-publication-year/1950s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36595060-endurance)
>
> This is a new reading of the thrilling account of one of the most astonishing feats of exploration and human courage ever recorded.
>
> In August of 1914, the British ship Endurance set sail for the South Atlantic. In October 1915, still half a continent away from its intended base, the ship was trapped, then crushed in the ice. For five months, Sir Ernest Shackleton and his men, drifting on ice packs, were castaways in one of the most savage regions of the world.
>
> Lansing describes how the men survived a 1,000-mile voyage in an open boat across the stormiest ocean on the globe and an overland trek through forbidding glaciers and mountains. The book recounts a harrowing adventure, but ultimately it is the nobility of these men and their indefatigable will that shines through.