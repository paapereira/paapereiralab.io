---
title: "Bear Head (Dogs of War Book 2) by Adrian Tchaikovsky (2021)"
slug: "bear-head-dogs-of-war-book-2"
author: "Paulo Pereira"
date: 2023-02-22T20:00:00+00:00
lastmod: 2023-02-22T20:00:00+00:00
cover: "/posts/books/bear-head-dogs-of-war-book-2.jpg"
description: "Finished “Bear Head (Dogs of War Book 2)” by Adrian Tchaikovsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - Adrian Tchaikovsky
book series:
  - Dogs of War Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Dogs of War Series
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
---

Finished “Bear Head”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
* First Published: [January 7, 2021](/book-publication-year/2021)
* Series: [Dogs of War](/book-series/dogs-of-war-series) Book 2
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/56082145-bear-head)
>
> Mars. The red planet. A new frontier for humanity, a civilization where humans can live in peace, lord and master of all they survey.
> 
> But this isn't Space City from those old science-fiction books. We live in Hell City, built into and from a huge subcontinent-sized crater. There's a big silk canopy over it, feeding out atmosphere as we generate it, little by little, until we can breathe the air.
> 
> It's a perfect place to live, if you actually want to live on Mars. I guess at some point I had actually wanted to live on Mars, because here I am. The money was supposed to be good, and how else was a working Joe like me supposed to get off-planet exactly? But I remember the videos they showed us – guys, not even in suits, watching robots and bees and Bioforms doing all the work – and they didn't quite get it right...