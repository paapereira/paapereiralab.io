---
title: "Recursion by Blake Crouch (2019)"
slug: "recursion"
author: "Paulo Pereira"
date: 2019-11-21T22:33:00+00:00
lastmod: 2019-11-21T22:33:00+00:00
cover: "/posts/books/recursion.jpg"
description: "Finished “Recursion” by Blake Crouch."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Blake Crouch
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Blake Crouch
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/recursion
---

Finished “Recursion”.
* Author: [Blake Crouch](/book-authors/blake-crouch)
* First Published: [2019](/book-publication-year/2019)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/45447944-recursion)
>
> A mind-bending new thriller from the New York Times bestselling author of Dark Matter and The Wayward Pines trilogy.
>
> Barry Sutton is driving home from another long shift as an NYPD detective when the call comes in. A woman is threatening to commit suicide, and someone’s got to try to talk her down. Only as he stands on the rooftop, mere inches away from her, does he realize that the woman is infected with False Memory Syndrome, a mysterious disease that drives its victims mad with memories of a life they never lived. When Barry is unable to save her, he’s rocked to his core–not only by her death but the fear that he’s been exposed to this devastating illness.
>
> Helena Smith is a brilliant but frustrated neuroscientist. If she could only get the funding, she’s sure she could build the ambitious device she’s long imagined–one that would allow people to preserve their most intense memories and relive them whenever they want. So when a billionaire entrepreneur offers to bankroll her project, she jumps at the opportunity–even if there are some strange conditions attached.
>
> As Helena’s efforts yield stunning results, Barry investigates the mystery behind the woman he failed to save. He finds himself on a journey as astonishing as it is terrifying, ultimately revealing the true danger posed by Helena’s invention–and a plot that could bring about the end of reality as we know it.
>
> Weaving together Barry’s story and Helena’s in ways even the savviest reader will never guess, Recurson is a brilliant science fiction thriller about time, memory, and the illusion of the present, built on our inability to escape the flashbulb moments that define us.