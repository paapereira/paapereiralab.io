---
title: "Four Past Midnight by Stephen King (1990)"
slug: "four-past-midnight"
author: "Paulo Pereira"
date: 2022-09-11T17:00:00+01:00
lastmod: 2022-09-11T17:00:00+01:00
cover: "/posts/books/four-past-midnight.jpg"
description: "Finished “Four Past Midnight” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
  - Short Stories
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - Short Stories
  - 8/10 Books
  - Audiobook
---

Finished “Four Past Midnight”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [January 1, 1990](/book-publication-year/1990s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28440395-four-past-midnight)
>
> One Past Midnight: “The Langoliers” takes a red-eye flight from LA to Boston into a most unfriendly sky. Only eleven passengers survive, but landing in an eerily empty world makes them wish they hadn’t. Something’s waiting for them, you see.
> 
> Two Past Midnight: “Secret Window, Secret Garden” enters the suddenly strange life of writer Mort Rainey, recently divorced, depressed, and alone on the shore of Tashmore Lake. Alone, that is, until a figure named John Shooter arrives, pointing an accusing finger.
> 
> Three Past Midnight: “The Library Policeman” is set in Junction City, Iowa, an unlikely place for evil to be hiding. But for small businessman Sam Peebles, who thinks he may be losing his mind, another enemy is hiding there as well—the truth. If he can find it in time, he might stand a chance.
> 
> Four Past Midnight: “The Sun Dog,” a menacing black dog, appears in every Polaroid picture that fifteen-year-old Kevin Delevan takes with his new camera, beckoning him to the supernatural. Old Pop Merrill, Castle Rock’s sharpest trader, aims to exploit The Sun Dog for profit, but this creature that shouldn’t exist at all, is a very dangerous investment.