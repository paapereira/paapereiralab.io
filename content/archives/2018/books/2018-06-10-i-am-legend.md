---
title: "I Am Legend by Richard Matheson (1954)"
slug: "i-am-legend"
author: "Paulo Pereira"
date: 2018-06-10T21:07:00+01:00
lastmod: 2018-06-10T21:07:00+01:00
cover: "/posts/books/i-am-legend.jpg"
description: "Finished “I Am Legend” by Richard Matheson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1950s
book authors:
  - Richard Matheson
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
  - Horror
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Richard Matheson
  - Fiction
  - Science Fiction
  - Horror
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/i-am-legend
---

Finished “I Am Legend”.
* Author: [Richard Matheson](/book-authors/richard-matheson)
* First Published: [1954](/book-publication-year/1950s)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/2572153-i-am-legend)
>
> Robert Neville is the last living man on Earth... but he is not alone. Every other man, woman and child on the planet has become a vampire, and they are hungry for Neville's blood.
>
> By day he is the hunter, stalking the undead through the ruins of civilisation. By night, he barricades himself in his home and prays for the dawn.
>
> How long can one man survive like this?