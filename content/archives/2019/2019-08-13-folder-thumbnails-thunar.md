---
title: "Folder thumbnails in Thunar"
slug: "folder-thumbnails-thunar"
author: "Paulo Pereira"
date: 2019-08-13T21:12:35+00:00
lastmod: 2019-08-13T21:12:35+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Thunar
---

My favorite file manager is [Thunar](https://docs.xfce.org/xfce/thunar/start), but it hasn’t folder thumbnails by default.

Here’s how to enable it.

* Install tumbler and imagemagick

```bash
yay -S tumbler imagemagick
```

* Create the following a folder.thumbnailer file and add the following

```bash
sudo vi /usr/share/thumbnailers/folder.thumbnailer
```
```text
[Thumbnailer Entry]
Version=1.0
Encoding=UTF-8
Type=X-Thumbnailer
Name=Folder Thumbnailer
MimeType=inode/directory;
Exec=/usr/bin/folder-thumbnailer %s %i %o %u
```

* Create the folder-thumbnailer script

```bash
sudo vi /usr/bin/folder-thumbnailer
```
```text
#!/bin/bash

convert -thumbnail "$1" "$2/folder.jpg" "$3" 1>/dev/null 2>&1 ||\
convert -thumbnail "$1" "$2/.folder.jpg" "$3" 1>/dev/null 2>&1 ||\
convert -thumbnail "$1" "$2/folder.png" "$3" 1>/dev/null 2>&1 ||\
convert -thumbnail "$1" "$2/cover.jpg" "$3" 1>/dev/null 2>&1 ||\
rm -f "$HOME/.cache/thumbnails/normal/$(echo -n "$4" | md5sum | cut -d " " -f1).png" ||\
rm -f "$HOME/.thumbnails/normal/$(echo -n "$4" | md5sum | cut -d " " -f1).png" ||\
rm -f "$HOME/.cache/thumbnails/large/$(echo -n "$4" | md5sum | cut -d " " -f1).png" ||\
rm -f "$HOME/.thumbnails/large/$(echo -n "$4" | md5sum | cut -d " " -f1).png" ||\
exit 1
```
```bash
sudo chmod +x /usr/bin/folder-thumbnailer
```

To add a thumbnail to a folder you just need to add a `.folder.jpg` file and your set. You can decide to use different names in the `/usr/bin/folder-thumbnailer` script.

More information here [https://docs.xfce.org/xfce/thunar/tumbler](https://docs.xfce.org/xfce/thunar/tumbler).