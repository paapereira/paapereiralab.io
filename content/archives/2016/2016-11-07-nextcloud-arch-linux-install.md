---
title: "Nextcloud Arch Linux installation"
slug: "nextcloud-arch-linux-install"
author: "Paulo Pereira"
date: 2016-11-07T22:44:10+00:00
lastmod: 2016-11-07T22:44:10+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Nextcloud
---

There are my notes for installing [Nextcloud in Arch Linux](https://wiki.archlinux.org/index.php/nextcloud) in my home server.

## Server installation

```bash
sudo pacman -Syu apache php php-apache mariadb
cower -d nextcloud
sudo pacman -Syu php-gd php-intl php-mcrypt php-apcu
```

## PHP configuration

```bash
sudo nano /etc/php/php.ini
```

Uncomment the following modules:

* gd.so
* iconv.so
* xmlrpc.so
* zip.so
* extension=pdo_mysql.so
* bz2.so
* curl.so
* intl.so
* mcrypt.so

Add the following to open_basedir:

`/tmp:/usr/share/webapps/nextcloud:/dev/urandom`

## MariaDB setup

```bash
sudo mysql_install_db -–user=mysql -–basedir=/usr -–datadir=/var/lib/mysql
sudo systemctl enable mariadb.service
sudo systemctl start mariadb.service
sudo systemctl status mariadb.service
sudo mysql_secure_installation
```

## Create Nextcloud database

```bash
mysql -u root -p
  CREATE DATABASE IF NOT EXISTS nextcloud;
  CREATE USER ‘username’@’localhost’ IDENTIFIED BY ‘password’;
  GRANT ALL PRIVILEGES ON nextcloud.* TO ‘username’@’localhost’ IDENTIFIED BY ‘password’;
  quit
```

## Apache setup

```bash
sudo cp /etc/webapps/nextcloud/apache.example.conf /etc/httpd/conf/extra/nextcloud.conf
sudo nano /etc/httpd/conf/httpd.conf
```

Comment the line:

	`#LoadModule mpm_event_module modules/mod_mpm_event.so`

Uncomment the line:

	`LoadModule mpm_prefork_module modules/mod_mpm_prefork.so`

After LoadModule dir_module modules/mod_dir.so, place the following module:

	`LoadModule php7_module modules/libphp7.so`

At the end of the Include list place the following line:

	`Include conf/extra/php7_module.conf`

At the end of the LoadModule list add the following line:

	`AddHandler php7-script php`

At the bottom of /etc/httpd/conf/httpd.conf add the following line:

	`Include conf/extra/nextcloud.conf`

Enable the following modules:

* mod_rewrite
* headers
* env
* dir
* mime

```bash
sudo systemctl enable httpd.service
sudo systemctl start httpd.service
sudo systemctl status httpd.service
```

## Switch to Cron from AJAX

```bash
sudo pacman -S cronie
sudo crontab -u http -e
```

	`*/15  *  *  *  * php -f /usr/share/webapps/nextcloud/cron.php`

```bash
sudo crontab -u http -l
```

## Enable memcache

```bash
sudo nano /etc/php/conf.d/apcu.ini
```

	`extension=apcu.so`

## Certificate

```bash
sudo cp /etc/httpd/conf/server.crt /usr/share/ca-certificates/domain.com.crt
sudo nano /etc/ca-certificates.conf
```
```text
domain.com.crt
```
```bash
sudo update-ca-certificates
```

## File location

```bash
sudo mkdir /home/nas/nextcloud
sudo nano /etc/fstab
```
```text
nas:/nfs/nextcloud  /home/nas/nextcloud nfs  users,noauto,x-systemd.automount,x-systemd.device-timeout=10,timeo=14,soft,intr,noatime,file_mode=0640,dir_mode=0750 0 0
```

## Nextcloud config.php

```bash
sudo nano /etc/webapps/nextcloud/config/config.php
```

```text
  <?php
  $CONFIG = array (
	'instanceid' => 'XXXXXXXXXXXXXXXXXXXXX',
	'passwordsalt' => 'XXXXXXXXXXXXXXXX',
	'secret' => 'XXXXXXXXXXXXXXXXXXXXXXXXXX',
	'trusted_domains' =>
	array (
	  0 => 'server',
	  1 => 'my_domain.net',
	),
	'datadirectory' => '/home/nas/nextcloud',
	'overwrite.cli.url' => 'https://server/nextcloud',
	'dbtype' => 'mysql',
	'version' => '9.1.1.5',
	'dbname' => 'nextcloud',
	'dbhost' => 'localhost',
	'dbport' => '',
	'dbtableprefix' => 'oc_',
	'dbuser' => 'nextcloud',
	'dbpassword' => 'XXXXXXXXXXXXXXXXXX',
	'logtimezone' => 'UTC',
	'installed' => true,
	'memcache.local' => '\\OC\\Memcache\\APCu',
	'mail_smtpmode' => 'smtp',
	'mail_from_address' => 'nextcloud',
	'mail_domain' => 'mail.com',
	'mail_smtpauth' => 1,
	'mail_smtphost' => 'smtp.mail.com',
	'mail_smtpport' => '587',
	'mail_smtpname' => 'your_mail@mail.com',
	'mail_smtppassword' => 'XXXXXXXXXXXXXXXXXXXX',
	'mail_smtpauthtype' => 'LOGIN',
	'mail_smtpsecure' => 'tls',
  );
```

## Permissions

```bash
sudo nano /root/nexcloud_strong_permissions.sh
```
```text
  #!/bin/bash
  ocpath='/usr/share/webapps/nextcloud'
  htuser='http'
  htgroup='http'
  rootuser='root'

  printf "Creating possible missing Directories\n"
  mkdir -p $ocpath/data
  mkdir -p $ocpath/assets
  mkdir -p $ocpath/updater

  printf "chmod Files and Directories\n"
  find ${ocpath}/ -type f -print0 | xargs -0 chmod 0640
  find ${ocpath}/ -type d -print0 | xargs -0 chmod 0750

  printf "chown Directories\n"
  chown -R ${rootuser}:${htgroup} ${ocpath}/
  chown -R ${htuser}:${htgroup} ${ocpath}/apps/
  chown -R ${htuser}:${htgroup} ${ocpath}/assets/
  chown -R ${htuser}:${htgroup} ${ocpath}/config/
  chown -R ${htuser}:${htgroup} ${ocpath}/data/
  chown -R ${htuser}:${htgroup} ${ocpath}/themes/
  chown -R ${htuser}:${htgroup} ${ocpath}/updater/

  chmod +x ${ocpath}/occ

  printf "chmod/chown .htaccess\n"
  if [ -f ${ocpath}/.htaccess ]
   then
	chmod 0644 ${ocpath}/.htaccess
	chown ${rootuser}:${htgroup} ${ocpath}/.htaccess
  fi
  if [ -f ${ocpath}/data/.htaccess ]
   then
	chmod 0644 ${ocpath}/data/.htaccess
	chown ${rootuser}:${htgroup} ${ocpath}/data/.htaccess
  fi
```

```bash
$ sudo nano /root/nexcloud_upgrade_permissions.sh
```

```text
  #!/bin/bash

  # Sets permissions of the Nextcloud instance for updating

  ocpath='/usr/share/webapps/nextcloud'
  htuser='http'
  htgroup='http'

  chown -R ${htuser}:${htgroup} ${ocpath}
```

```bash
sudo +x /root/nexcloud_strong_permissions.sh
sudo +x /root/nexcloud_upgrade_permissions.sh

sudo /root/nexcloud_strong_permissions.sh
sudo /root/nexcloud_upgrade_permissions.sh
```

## Desktop installation

```bash
cower -d nextcloud-client
```

## First login

http://server/nextcloud