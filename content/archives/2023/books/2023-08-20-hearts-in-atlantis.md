---
title: "Hearts in Atlantis by Stephen King (1999)"
slug: "hearts-in-atlantis"
author: "Paulo Pereira"
date: 2023-08-20T17:00:00+01:00
lastmod: 2023-08-20T17:00:00+01:00
cover: "/posts/books/hearts-in-atlantis.jpg"
description: "Finished “Hearts in Atlantis” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Fantasy
  - Short Stories
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Fantasy
  - Short Stories
  - 7/10 Books
  - Audiobook
---

Finished “Hearts in Atlantis”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [September 14, 1999](/book-publication-year/1990s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/37533245-hearts-in-atlantis)
>
> Five interconnected, sequential narratives, set in the years from 1960 to 1999. Each story is deeply rooted in the sixties, and each is haunted by the Vietnam War.
> 
> Stephen King, whose first novel, Carrie, was published in 1974, the year before the last U.S. troops withdrew from Vietnam, is the first hugely popular writer of the TV generation. Images from that war -- and the protests against it -- had flooded America's living rooms for a decade. Hearts in Atlantis, King's newest fiction, is composed of five interconnected, sequential narratives, set in the years from 1960 to 1999. Each story is deeply rooted in the sixties, and each is haunted by the Vietnam War.
> 
> In Part One, "Low Men in Yellow Coats," eleven-year-old Bobby Garfield discovers a world of predatory malice in his own neighborhood. He also discovers that adults are sometimes not rescuers but at the heart of the terror.
> 
> In the title story, a bunch of college kids get hooked on a card game, discover the possibility of protest...and confront their own collective heart of darkness, where laughter may be no more than the thinly disguised cry of the beast.
> 
> In "Blind Willie" and "Why We're in Vietnam," two men who grew up with Bobby in suburban Connecticut try to fill the emptiness of the post-Vietnam era in an America which sometimes seems as hollow -- and as haunted -- as their own lives.
> 
> And in "Heavenly Shades of Night Are Falling," this remarkable book's denouement, Bobby returns to his hometown where one final secret, the hope of redemption, and his heart's desire may await him. 