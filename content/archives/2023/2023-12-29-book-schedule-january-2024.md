---
title: "Books Schedule update"
slug: "book-schedule-january-2024"
author: "Paulo Pereira"
date: 2023-12-29T14:30:00+00:00
lastmod: 2023-12-29T14:30:00+00:00
description: "January Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my January TBR (To Be Read) update for the next months.

* **[Read]** Diamond Dogs, Turquoise Days by Alastair Reynolds
* **[Read]** Wool (Silo 1) by Hugh Howey
* **[Read]** Pirate Latitudes by Michael Crichton
* **[Reading]** The Hero of Ages (The Mistborn Saga 3) by Brandon Sanderson
* A Call to Arms (The Damned 1) by Alan Dean Foster
* Precious Little Things by Adrian Tchaikovsky
* Star Wars Episode IV: A New Hope by George Lucas
* Proven Guilty (The Dresden Files 8) by Jim Butcher
* The Night Boat by Robert R. McCammon
* Tau Zero by Poul Anderson
* Supernova Era by Cixin Liu
* Micro by Michael Crichton
* Galactic North by Alastair Reynolds
* Shift (Silo 2) by Hugh Howey
* Elantris by Brandon Sanderson
* Nightflyers & Other Stories by George R. R. Martin

I also have always an audiobook going too:

* **[Read]** Parallel Worlds by Michio Kaku
* **[Read]** Crystal World (Undying Mercenaries 20) by B. V. Larson
* **[Read]** Who Goes There? by John W. Campbell Jr.
* Accessory to War by Neil Degrasse Tyson
* The Regulators by Stephen King/Richard Bachman
* Red Company: Discovery (Red Company 2) by B. V. Larson
* The Sandman (Book 1) by Neil Gaiman
* The Universe in a Nutshell by Stephen Hawking