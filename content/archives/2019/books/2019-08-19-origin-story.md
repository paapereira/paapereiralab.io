---
title: "Origin Story by David Christian (2018)"
slug: "origin-story"
author: "Paulo Pereira"
date: 2019-08-19T21:50:00+01:00
lastmod: 2019-08-19T21:50:00+01:00
cover: "/posts/books/origin-story.jpg"
description: "Finished “Origin Story: A Big History of Everything” by David Christian."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - David Christian
book series:
  - 
book genres:
  - Nonfiction
  - History
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - David Christian
  - Nonfiction
  - History
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “Origin Story: A Big History of Everything”.
* Author: [David Christian](/book-authors/david-christian)
* First Published: [2018](/book-publication-year/2018)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/42834395-origin-story)
>
> How did we get from the Big Bang to today's staggering complexity, in which seven billion humans are connected into networks powerful enough to transform the planet? And why, in comparison, are our closest primate relatives reduced to near-extinction?
>
> Big History creator David Christian gives the answers in a mind-expanding cosmological detective story told on the grandest possible scale. He traces how, during eight key thresholds, the right conditions have allowed new forms of complexity to arise, from stars to galaxies, Earth to homo sapiens, agriculture to fossil fuels. This last mega-innovation gave us an energy bonanza that brought huge benefits to mankind, yet also threatens to shake apart everything we have created.
>
> This global origin story is one that we could only begin to tell recently, thanks to the underlying unity of modern knowledge. Panoramic in scope and thrillingly told, Origin Story reveals what we learn about human existence when we consider it from a universal scale.