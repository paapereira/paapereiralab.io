---
title: "The Observers (The Human Division Book 9) by John Scalzi (2013)"
slug: "the-observers-the-human-division-book-9"
author: "Paulo Pereira"
date: 2019-05-02T22:18:00+01:00
lastmod: 2019-05-02T22:18:00+01:00
cover: "/posts/books/the-observers-the-human-division-book-9.png"
description: "Finished “The Observers (The Human Division Book 9)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Observers”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [March 12th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 9
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17607641-the-observers)
>
> In an effort to improve relations with the Earth, the Colonial Union has invited a contingent of diplomats from that planet to observe Ambassador Abumwe negotiate a trade deal with an alien species. Then something very bad happens to one of the Earthings, and with that, the relationship between humanitys two factions is on the cusp of disruption once more. Its a race to find out what really happened, and who is to blame.