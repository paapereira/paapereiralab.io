---
title: "Dark Age (Red Rising Saga Book 5) by Pierce Brown (2019)"
slug: "dark-age-red-rising-saga-book-5"
author: "Paulo Pereira"
date: 2022-04-28T21:00:00+01:00
lastmod: 2022-04-28T21:00:00+01:00
cover: "/posts/books/dark-age-red-rising-saga-book-5.jpg"
description: "Finished “Dark Age (Red Rising Saga Book 5)” by Pierce Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Pierce Brown
book series:
  - Red Rising Saga Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Pierce Brown
  - Red Rising Saga Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
---

Finished “Dark Age”.
* Author: [Pierce Brown](/book-authors/pierce-brown)
* First Published: [July 30th 2019](/book-publication-year/2019)
* Series: [Red Rising Saga](/book-series/red-rising-saga-series) Book 5
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/37562956-dark-age)
>
> He broke the chains. Then he broke the world….
> 
> A decade ago Darrow led a revolution, and laid the foundations for a new world. Now he’s an outlaw.
> 
> Cast out of the very Republic he founded, with half his fleet destroyed, he wages a rogue war on Mercury, in hopes that he can still salvage the dream of Eo. But as he leaves death and destruction in his wake, is he still the hero who broke the chains? Or will he become the very evil he fought to destroy? Will another legend rise to take his place?
> 
> In his darkening shadow, a new hero rises.
> 
> Lysander au Lune, the heir to the old empire in exile, has returned to bridge the divide between the Golds of the Rim and Core. Determined to bring peace back to mankind at the edge of his sword, he must overcome or unite the treacherous Gold families of the Core and face down Darrow over the skies of war-torn Mercury. If united, their combined might may prove fatal to the fledgling Republic.
> 
> But theirs are not the only fates hanging in the balance.
> 
> On Luna, Mustang, the embattled Sovereign of the Republic, Virginia au Augustus campaigns to unite the Republic behind her husband, fights to preserve her precious demokracy and her exiled husband. Beset by political and criminal enemies, can she outwit her opponents in time to save him? But one may cost her the other, and her son is not yet returned.
> 
> Abducted by a new threat to the Republic, Pax and Electra, the children of Darrow and Sevro, must trust in Ephraim, a Gray thief, for their salvation—and Ephraim must look to them for his chance at redemption.
> 
> Far across the void, young Lyria, Once a Red refugee, now stands accused of treason, and her only hope is a desperate bid for freedom with the help of two unlikely new allies.
> 
> Fear dims the hopes of the Rising as alliances shift, break, and re-form—and power is seized, lost, and reclaimed—every player is at risk in a game of conquest and the worlds spin on and on toward a new Dark Age.