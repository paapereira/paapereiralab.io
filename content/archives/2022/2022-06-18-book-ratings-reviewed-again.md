---
title: "Reviewing my book ratings... again"
slug: "book-ratings-reviewed-again"
author: "Paulo Pereira"
date: 2022-06-18T12:24:05+01:00
lastmod: 2022-06-18T12:24:05+01:00
description: "Another change in my book ratings. I'm changing to a 1-10 scoring system."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Rating
---

Another change in my book ratings. I'm changing to a 1-10 scoring system.


| Score                      | Score | Rating | Stars      |
| -------------------------- | ----- | ------ | ---------- |
| It's a special book for me | 10/10 | A+     | ⭐⭐⭐⭐⭐ |
| One of the best for me     | 9/10  | A+     | ⭐⭐⭐⭐⭐ |
| Worth a re-read            | 8/10  | A      | ⭐⭐⭐⭐   |
| Recommended read           | 7/10  | A      | ⭐⭐⭐⭐   |
| Above the average book     | 6/10  | B      | ⭐⭐⭐     |
| A good book                | 5/10  | B      | ⭐⭐⭐     |
| I liked it                 | 4/10  | C      | ⭐⭐       |
| Not my first choice        | 3/10  | C      | ⭐⭐       |
| Didn't really liked it     | 2/10  | D      | ⭐         |
| Didn't finish it           | 1/10  | D      | ⭐         |
