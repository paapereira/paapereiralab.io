---
title: "For We Are Many (Bobiverse Book 2) by Dennis E. Taylor (2017)"
slug: "for-we-are-many-bobiverse-book-2"
author: "Paulo Pereira"
date: 2019-10-16T22:02:00+01:00
lastmod: 2019-10-16T22:02:00+01:00
cover: "/posts/books/for-we-are-many-bobiverse-book-2.png"
description: "Finished “For We Are Many (Bobiverse Book 2)” by Dennis E. Taylor."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Dennis E. Taylor
book series:
  - Bobiverse Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Dennis E. Taylor
  - Bobiverse Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “For We Are Many”.
* Author: [Dennis E. Taylor](/book-authors/dennis-e.-taylor)
* First Published: [2017](/book-publication-year/2017)
* Series: [Bobiverse](/book-series/bobiverse-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/33395557-for-we-are-many)
>
> Bob Johansson didn’t believe in an afterlife, so waking up after being killed in a car accident was a shock. To add to the surprise, he is now a sentient computer and the controlling intelligence for a Von Neumann probe.
>
> Bob and his copies have been spreading out from Earth for forty years now, looking for habitable planets. But that’s the only part of the plan that’s still in one piece. A system-wide war has killed off 99.9% of the human race; nuclear winter is slowly making the Earth uninhabitable; a radical group wants to finish the job on the remnants of humanity; the Brazilian space probes are still out there, still trying to blow up the competition; And the Bobs have discovered a spacefaring species that considers all other life as food.
>
> Bob left Earth anticipating a life of exploration and blissful solitude. Instead he’s a sky god to a primitive native species, the only hope for getting humanity to a new home, and possibly the only thing that can prevent every species in the local sphere ending up as dinner.