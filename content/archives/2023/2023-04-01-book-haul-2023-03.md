---
title: "Book Haul for March 2023"
slug: "book-haul-2023-03"
author: "Paulo Pereira"
date: 2023-04-01T20:00:00+01:00
lastmod: 2023-04-01T20:00:00+01:00
cover: "/posts/book-hauls/2023-03-book-haul.png"
description: "March book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

March book haul.