---
title: "VirtualBox Kernel driver not installed"
slug: "virtualbox-kernel-driver-not-installed"
author: "Paulo Pereira"
date: 2009-06-19T19:08:00+01:00
lastmod: 2009-06-19T19:08:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - VirtualBox
---

After installing the new 2.6.28-13 kernel yesterday, running a virtual machine in [VirtualBox](http://www.virtualbox.org/) aborts with the following error:

![virtualbox](/posts/2009/2009-06-19-virtualbox-kernel-driver-not-installed/virtualbox-kernel-error.png)

As the message says, the VirtualBox Linux kernel driver for the running Linux kernel isn't installed.

To re-setup the kernel module you just need to:

```bash
sudo /etc/init.d/vboxdrv setup
```