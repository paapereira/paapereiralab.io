---
title: "The Eye of the World (The Wheel of Time Book 1) by Robert Jordan (1990)"
slug: "the-eye-of-the-world-the-wheel-of-time-book-1"
author: "Paulo Pereira"
date: 2020-06-04T22:45:00+01:00
lastmod: 2020-06-04T22:45:00+01:00
cover: "/posts/books/the-eye-of-the-world-the-wheel-of-time-book-1.png"
description: "Finished “The Eye of the World (The Wheel of Time Book 1)” by Robert Jordan."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Robert Jordan
book series:
  - The Wheel of Time Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Robert Jordan
  - The Wheel of Time Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Audiobook
aliases:
  - /posts/books/the-eye-of-the-world-the-wheel-of-time-book-1
---

Finished “The Eye of the World”.
* Author: [Robert Jordan](/book-authors/robert-jordan)
* First Published: [January 15th 1990](/book-publication-year/1990s)
* Series: [The Wheel of Time](/book-series/the-wheel-of-time-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/13412379-the-eye-of-the-world)
>
> When their village is attacked by trollocs, monsters thought to be only legends, three young men, Rand, Matt, and Perrin, flee in the company of the Lady Moiraine, a sinister visitor of unsuspected powers. Thus begins an epic adventure set in a world of wonders and horror, where what was, what will be, and what is, may yet fall under the Shadow.