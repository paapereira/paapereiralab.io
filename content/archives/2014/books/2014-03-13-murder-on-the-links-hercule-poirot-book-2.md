---
title: "Murder on the Links (Hercule Poirot Book 2) by Agatha Christie (1923)"
slug: "murder-on-the-links-hercule-poirot-book-2"
author: "Paulo Pereira"
date: 2014-03-13T23:45:00+00:00
lastmod: 2014-03-13T23:45:00+00:00
cover: "/posts/books/murder-on-the-links-hercule-poirot-book-2.jpg"
description: "Finished “Murder on the Links (Hercule Poirot Book 2)” by Agatha Christie."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1920s
book authors:
  - Agatha Christie
book series:
  - Hercule Poirot Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Agatha Christie
  - Hercule Poirot Series
  - Fiction
  - Mystery
  - 5/10 Books
  - Audiobook
---

Finished “Murder on the Links”.
* Author: [Agatha Christie](/book-authors/agatha-christie)
* First Published: [May 1923](/book-publication-year/1920s)
* Series: [Hercule Poirot](/book-series/hercule-poirot-series) Book 2
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/1059129.Murder_on_the_Links)
>
> When Poirot arrives in France, following an urgent appeal for help, he finds he is too late. His client, a South American millionaire, has been stabbed to death and his body flung into a freshly dug open grave on the golf course adjoining the property. Meanwhile the millionaire's wife is found bound and gagged in her room. Her wrists are badly cut and she seems to be in shock, but she agrees to speak to Poirot. She tells him that they were attacked by thugs who tied her up and forced her husband to leave the house, dressed only in his underwear, before brutally killing him. Poirot is not sure whether to believe her story. Did she, as the sole beneficiary of her millionaire husband's estate, set the whole thing up? And if not, who did kill M. Renauld? The suspects include the mysterious Dulcie Duveen and Renauld's son Jack, who had quarreled violently with his father. As Poirot investigates, the mystery begins to unfold—but not before another murder occurs. . . John Moffatt stars as Poirot, with Jeremy Clyde as Captain Hastings, Madeline Smith as Dulcie Duveen, and Stephen Tompkinson as Jack.