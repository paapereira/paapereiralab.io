---
title: "Battleship Indomitable (Galactic Liberation Book 2) by David VanDyke and B.V. Larson (2017)"
slug: "battleship-indomitable-galactic-liberation-book-2"
author: "Paulo Pereira"
date: 2022-06-19T18:00:00+01:00
lastmod: 2022-06-19T18:00:00+01:00
cover: "/posts/books/battleship-indomitable-galactic-liberation-book-2.jpg"
description: "Finished “Battleship Indomitable (Galactic Liberation Book 2)” by David VanDyke and B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - David VanDyke
  - B.V. Larson
book series:
  - Galactic Liberation Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - David VanDyke
  - B.V. Larson
  - Galactic Liberation Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Battleship Indomitable”.
* Author: [David VanDyke](/book-authors/david-vandyke) and [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [May 16th 2017](/book-publication-year/2017)
* Series: [Galactic Liberation](/book-series/galactic-liberation-series) Book 2
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36184613-battleship-indomitable)
>
> Commodore Straker's rebellion grows in strength--but his enemies are growing even faster. Faced with a dozen rebel planets in their territory, the Mutuality finally takes notice of the upstart known as the Liberator, and they gather a vast fleet to crush him.
> 
> Preparing for a titanic interstellar battle, it's clear Straker has no chance. His tiny enclave of free planets can't survive the weight of a thousand worlds. His own officers realize this, and some of them begin to turn against him...
> 
> In a desperate attempt to halt their inevitable destruction, Straker and his team set out to capture the largest ship ever built. The monstrous vessel is well-defended and contains secrets no one suspects. Unleashing its power might turn the tide of the war--but it may also doom humanity.