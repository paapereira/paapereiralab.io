---
title: "A Grown-Up Guide to Dinosaurs by Ben Garrod (2019)"
slug: "a-grown-up-guide-to-dinosaurs"
author: "Paulo Pereira"
date: 2020-04-21T23:18:00+01:00
lastmod: 2020-04-21T23:18:00+01:00
cover: "/posts/books/a-grown-up-guide-to-dinosaurs.png"
description: "Finished “A Grown-Up Guide to Dinosaurs” by Ben Garrod."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Ben Garrod
book series:
  -
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Ben Garrod
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
aliases:
  - /posts/books/a-grown-up-guide-to-dinosaurs
---

Finished “A Grown-Up Guide to Dinosaurs”.
* Author: [Ben Garrod](/book-authors/ben-garrod)
* First Published: [July 4th 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/46750711-a-grown-up-guide-to-dinosaurs)
>
> Most children go through a dinosaur phase. Learning all the tongue-twisting names, picking favourites based on ferocity, armour, or sheer size. For many kids this love of ‘terrible lizards’ fizzles out at some point between starting and leaving primary school. All those fancy names slowly forgotten, no longer any need for a favourite.
>
> For all those child dino fanatics who didn’t grow up to become paleontologists, dinosaurs seem like something out of mythology. They are dragons, pictures in books, abstract, other, extinct.
>
> They are at the same time familiar and mysterious. And yet we’re in an age of rapid discovery - new dinosaur species and genera are being discovered at an accelerating rate, we’re learning more about what they looked like, how they lived, how they evolved and where they all went.
>
> This series isn’t just a top trumps list of dino facts - we’re interested in the why and the how and like all areas of science there is plenty of controversy and debate.
>