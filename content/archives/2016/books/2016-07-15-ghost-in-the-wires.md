---
title: "Ghost in the Wires by Kevin Mitnick (2011)"
slug: "ghost-in-the-wires"
author: "Paulo Pereira"
date: 2016-07-15T23:04:00+01:00
lastmod: 2016-07-15T23:04:00+01:00
cover: "/posts/books/ghost-in-the-wires.jpg"
description: "Finished “Ghost in the Wires: My Adventures as the World's Most Wanted Hacker” by Kevin Mitnick."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Kevin Mitnick
book series:
  - 
book genres:
  - Nonfiction
  - Biography
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Kevin Mitnick
  - Nonfiction
  - Biography
  - 6/10 Books
  - Audiobook
---

Finished “Ghost in the Wires: My Adventures as the World's Most Wanted Hacker”.
* Author: [Kevin Mitnick](/book-authors/kevin-mitnick)
* First Published: [August 15th 2011](/book-publication-year/2011)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12447340-ghost-in-the-wires)
>
> If they were a hall of fame or shame for computer hackers, a Kevin Mitnick plaque would be mounted the near the entrance. While other nerds were fumbling with password possibilities, this adept break-artist was penetrating the digital secrets of Sun Microsystems, Digital Equipment Corporation, Nokia, Motorola, Pacific Bell, and other mammoth enterprises. His Ghost in the Wires memoir paints an action portrait of a plucky loner motivated by a passion for trickery, not material game. (P.S. Mitnick's capers have already been the subject of two books and a movie. This first-person account is the most comprehensive to date.)