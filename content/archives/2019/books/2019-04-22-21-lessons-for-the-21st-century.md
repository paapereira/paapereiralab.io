---
title: "21 Lessons for the 21st Century by Yuval Noah Harari (2018)"
slug: "21-lessons-for-the-21st-century"
author: "Paulo Pereira"
date: 2019-04-22T20:50:00+01:00
lastmod: 2019-04-22T20:50:00+01:00
cover: "/posts/books/21-lessons-for-the-21st-century.jpg"
description: "Finished “21 Lessons for the 21st Century” by Yuval Noah Harari."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Yuval Noah Harari
book series:
  - 
book genres:
  - Nonfiction
  - History
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Yuval Noah Harari
  - Nonfiction
  - History
  - 7/10 Books
  - Audiobook
---

Finished “21 Lessons for the 21st Century”.
* Author: [Yuval Noah Harari](/book-authors/yuval-noah-harari)
* First Published: [August 30th 2018](/book-publication-year/2018)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/41584641-21-lessons-for-the-21st-century)
>
> From the author of the multimillion copy best seller Sapiens.
>
> Sapiens showed us where we came from. Homo Deus looked to the future. 21 Lessons for the 21st Century explores the present.
>
> How can we protect ourselves from nuclear war, ecological cataclysms and technological disruptions? What can we do about the epidemic of fake news or the threat of terrorism? What should we teach our children?
>
> Yuval Noah Harari takes us on a thrilling journey through today’s most urgent issues. The golden thread running through his exhilarating new book is the challenge of maintaining our collective and individual focus in the face of constant and disorienting change. Are we still capable of understanding the world we have created?