---
title: "Install Xylon in Galaxy Nexus"
slug: "install-xylon-in-galaxy-nexus"
author: "Paulo Pereira"
date: 2013-03-09T22:43:00+00:00
lastmod: 2013-03-09T22:43:00+00:00
draft: false
toc: true
categories:
  - Android
tags:
  - Android
  - Galaxy Nexus
  - Xylon
  - Ak Kernel
aliases:
  - /posts/install-xylon-in-galaxy-nexus/
---

I was having problems in [AOKP](/posts/low-ram-problems-after-aokp-build-3-update/), so I decided to try a different ROM and a different kernel.

I've decided to go with [Xylon](http://forum.xda-developers.com/showthread.php?t=1996726) and [Ak Kernel](http://forum.xda-developers.com/showthread.php?t=1883298).

## What is Xylon?

*"A Generally based of many kinds of features around the Android  Development and fits into minimal customization. It may act up like  AOKP, but totally not at all. Overall, we're still in work in progress  state of changing the base to slightly minimal like FNV before. This ROM is built off with GCC 4.7, O3, Linaro Strings enabled. (Galaxy Nexus  Kernel, fusion with anarkia's horn, O3 and Linaro GCC 4.7.3 enabled.)."* 
*from xda-developers*

![xylon_maguro_2.1.1_20130228](/posts/2013/2013-03-09-install-xylon-in-galaxy-nexus/xylon.png)

I remember when I saw the FNV ROM. I didn't try it back then, but it  looked very good, so when I was looking for a new ROM I went with [Xylon](http://forum.xda-developers.com/showthread.php?t=1996726).

## Ak Kernel

[Ak Kernel](http://forum.xda-developers.com/showthread.php?t=1883298) comes in two flavors:

- Purity edition, the stable version
- Cylon edition, a experimental branch

![ak.716.422.Cylon](/posts/2013/2013-03-09-install-xylon-in-galaxy-nexus/ak.png)

## Installation

As with [AOKP](/posts/install-aokp-in-galaxy-nexus/), I will show how to do a fresh install of Xylon with Ak Kernel.

## Backing up your device

- Backup using [Titanium Backup](https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup)
- Backup [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher) settings
- Backup SMS using [SMS Backup & Restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore)
- Reboot into recovery (I have ClockworkMod Recovery v6.0.2.3)
- Backup (backup and restore > backup)
- Go into "install zip from sdcard > choose zip from sdcard" just  to guarantee that ClockworkMod Recovery is accessing the storage
- Plug the phone into a computer (I'm using Ubuntu 12.10, and have already android-sdk)
- Go into your android-sdk tools folder
  ```bash
  cd Android/android-sdk-linux_86/tools/
  ```
- Go into adb shell and list the directories in your "sdcard" to decide what to backup
  ```bash
  sudo ./adb shell
  ls /data/media/0
  exit
  ```
- Pull all the directories you want to backup
  ```bash
  sudo ./adb pull /data/media/0/examplefolder sdcard/examplefolder
  ```
- The previous example will create a sdcard folder in your tools folder with the data folder pulled from your phone
- Change the owner of the pulled folders
  ```bash
  sudo chown -R youruser:youruser sdcard
  ```
You can also backup using FTP for example. I normally use [WellFTP Server](https://play.google.com/store/apps/details?id=com.gsapp.wifitransport).

Don't forget to backup at least your "clockwork" and "TitaniumBackup" folders!

Note: clockwork backups are located in "/data/media/clockworkmod" and  not in "/data/media/0/clockworkmod", at least at my system.

Check in what folder you have the following folders: "backup" and "blobs". That will be the correct clockwork backup folder.

## Wiping your device

- Format /data and /cache (wipe data/factory reset)
- Format /system (mounts and storage > format /system)
- Format /sdcard (mounts and storage > format /sdcard)
- Wipe Dalvik Cache (advanced > wipe dalvik cache)

After these steps, your device, including the "sdcard" is cleaned without ROM or files.

## Installing Xylon

I will be installing Xylon 2.1.1 (2013-02-28), based on Android 4.2.2,  and the provided Google Apps (gapps-xy-20130220-nops-BAnKs-signed). The  kernel is the Cylon edition, version 716.

- Create a new folder in your "sdcard"
  ```bash
  sudo ./adb shell
  cd /data/media
  mkdir 0/0.ROOT/xylon
  exit
  ```
- Push the ROM and the kernel into your phone
  ```bash
  sudo ./adb push ~/xylon_maguro_2.1.1_20130228.zip /data/media/0/0.ROOT/xylon
  sudo ./adb push ~/gapps-xy-20130220-nops-BAnKs-signed.zip/data/media/0/0.ROOT/xylon
  sudo ./adb push ~/ak.716.422.Cylon.zip /data/media/0/0.ROOT/xylon
  ```
- Install the ROM (install zip from sdcard > choose zip from sdcard > 0 > 0.ROOT > xylon > xylon_maguro_2.1.1_20130228.zip)
- Install the Google Apps (install zip from sdcard > choose zip  from sdcard > 0 > 0.ROOT > xylon  > gapps-xy-20130220-nops-BAnKs-signed.zip)
- Install the kernel (install zip from sdcard > choose zip from sdcard > 0 > 0.ROOT > xylon > ak.716.422.Cylon.zip)
- Reboot (reboot system now)

## Restoring your "sdcard"

- Reboot into Recovery
- Push your backed up folders into your "sdcard"
  ```bash
  sudo ./adb push sdcard/examplefolder /data/media/0
  ```
- Reboot (reboot system now)

When restoring I tend to use [WellFTP Server](https://play.google.com/store/apps/details?id=com.gsapp.wifitransport).

## Restoring your apps

- Restore apps using [Titanium Backup](https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup)
- Restore [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher) settings
- Restore SMS using [SMS Backup & Restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore)

## Ak Kernel settings

To change Ak Kernel setting use [Trickster MOD Kernel Settings](https://play.google.com/store/apps/details?id=com.bigeyes0x0.trickstermod). Don't forget to donate to the developer by purchasing the premium [key](https://play.google.com/store/apps/details?id=com.bigeyes0x0.trickstermod.premium).

Also, read the kernel FAQ [here](http://forum.xda-developers.com/showthread.php?t=2157193).

Enjoy!