---
title: "Broken Angels (Takeshi Kovacs Book 2) by Richard K. Morgan (2003)"
slug: "broken-angels-takeshi-kovacs-book-2"
author: "Paulo Pereira"
date: 2019-05-26T21:10:00+01:00
lastmod: 2019-05-26T21:10:00+01:00
cover: "/posts/books/broken-angels-takeshi-kovacs-book-2.jpg"
description: "Finished “Broken Angels (Takeshi Kovacs Book 2)” by Richard K. Morgan."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2003
book authors:
  - Richard K. Morgan
book series:
  - Takeshi Kovacs Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Richard K. Morgan
  - Takeshi Kovacs Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Broken Angels”.
* Author: [Richard K. Morgan](/book-authors/richard-k.-morgan)
* First Published: [March 20th 2003](/book-publication-year/2003)
* Series: [Takeshi Kovacs](/book-series/takeshi-kovacs-series) Book 2
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/29355316-broken-angels)
>
> Cynical, quick-on-the-trigger Takeshi Kovacs, the ex-U.N. envoy turned private eye, has changed careers, and bodies, once more, trading sleuthing for soldiering as a warrior-for-hire and helping a far-flung planet's government put down a bloody revolution.
>
> But when it comes to taking sides, the only one Kovacs is ever really on is his own. So when a rogue pilot and a sleazy corporate fat cat offer him a lucrative role in a treacherous treasure hunt, he's only too happy to go AWOL with a band of resurrected soldiers of fortune. All that stands between them and the ancient alien spacecraft they mean to salvage are a massacred city bathed in deadly radiation, unleashed nanotechnolgy with a million ways to kill, and whatever surprises the highly advanced Martian race may have in store. But armed with his genetically engineered instincts, and his trusty twin Kalashnikovs, Takeshi is ready to take on anything...and let the devil take whoever's left behind.
>
> Broken Angels is the second Takeshi Kovacs novel. Don't miss the first, Altered Carbon, or the third, Woken Furies.