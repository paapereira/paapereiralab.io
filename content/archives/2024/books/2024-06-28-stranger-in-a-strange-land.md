---
title: "Stranger in a Strange Land by Robert A. Heinlein (1961)"
slug: "stranger-in-a-strange-land"
author: "Paulo Pereira"
date: 2024-06-28T21:00:00+01:00
lastmod: 2024-06-28T21:00:00+01:00
cover: "/posts/books/stranger-in-a-strange-land.jpg"
description: "Finished “Stranger in a Strange Land” by Robert A. Heinlein."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Robert A. Heinlein
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Robert A. Heinlein
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
aliases:
  - /posts/books/stranger-in-a-strange-land
---

Finished “Stranger in a Strange Land”.
* Author: [Robert A. Heinlein](/book-authors/robert-a.-heinlein)
* First Published: [July 1, 1961](/book-publication-year/1960s)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53456915-stranger-in-a-strange-land)
>
> Here at last is the complete, uncut version of Heinlein's all-time masterpiece, the brilliant novel that grew from a cult favorite to a bestseller to a classic in a few short years. It is the story of Valentine Michael Smith, the man from Mars who taught humankind grokking and water-sharing. And love.