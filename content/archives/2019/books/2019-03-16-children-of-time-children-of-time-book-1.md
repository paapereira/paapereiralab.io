---
title: "Children of Time (Children of Time Book 1) by Adrian Tchaikovsky (2015)"
slug: "children-of-time-children-of-time-book-1"
author: "Paulo Pereira"
date: 2019-03-16T23:16:00+00:00
lastmod: 2019-03-16T23:16:00+00:00
cover: "/posts/books/children-of-time-children-of-time-book-1.jpg"
description: "Finished “Children of Time (Children of Time Book 1)” by Adrian Tchaikovsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - Adrian Tchaikovsky
book series:
  - Children of Time Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Children of Time Series
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Audiobook
aliases:
  - /posts/books/children-of-time-children-of-time-book-1
---

Finished “Children of Time”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
* First Published: [June 2015](/book-publication-year/2015)
* Series: [Children of Time](/book-series/children-of-time-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35228265-children-of-time)
>
> A race for survival among the stars... Humanity's last survivors escaped earth's ruins to find a new home. But when they find it, can their desperation overcome it's dangers? WHO WILL INHERIT THIS NEW EARTH? The last remnants of the human race left a dying Earth, desperate to find a new home among the stars. Following in the footsteps of their ancestors, they discover the greatest treasure of the past age - a world terraformed and prepared for human life. But all is not right in this new Eden. In the long years since the planet was abandoned, the work of its architects has borne disastrous fruit. The planet is not waiting for them, pristine and unoccupied. New masters have turned it from a refuge into mankind's worst nightmare. Now two civilizations are on a collision course, both testing the boundaries of what they will do to survive. As the fate of humanity hangs in the balance, who are the true heirs of this new Earth?