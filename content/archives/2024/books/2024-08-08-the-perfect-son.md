---
title: "The Perfect Son by Freida McFadden (2019)"
slug: "the-perfect-son"
author: "Paulo Pereira"
date: 2024-08-08T21:00:00+01:00
lastmod: 2024-08-08T21:00:00+01:00
cover: "/posts/books/the-perfect-son.jpg"
description: "Finished “The Perfect Son” by Freida McFadden."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Freida McFadden
book series:
  - 
book genres:
  - Fiction
  - Thriller
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Freida McFadden
  - Fiction
  - Thriller
  - 8/10 Books
  - Ebook
---

Finished “The Perfect Son”.
* Author: [Freida McFadden](/book-authors/freida-mcfadden)
* First Published: [October 18, 2019](/book-publication-year/2019)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/52616342-the-perfect-son)
>
> "Mrs. Cass, we were hoping your son could answer a few questions about the girl who disappeared last night..."
> 
> Erika Cass has a perfect family and a perfect life. Until the evening when two detectives show up at her front door.
> 
> A high school girl has vanished from Erika's quiet suburban neighborhood. The police suspect the worst--murder. And Erika's teenage son, Liam, was the last person to see the girl alive.
> 
> Erika has always sensed something dark and disturbed in her seemingly perfect older child. She wants to believe he's innocent, but as the evidence mounts, she can't deny the truth--Liam may have done the unthinkable.
> 
> Now she must ask herself:
> 
> How far will she go to protect her son?