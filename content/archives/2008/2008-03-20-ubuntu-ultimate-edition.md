---
title: "Ubuntu Ultimate Edition"
slug: "ubuntu-ultimate-edition"
author: "Paulo Pereira"
date: 2008-03-20T16:06:00+00:00
lastmod: 2008-03-20T16:06:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

I chose [Ubuntu Ultime Edition](http://ultimateedition.info/), based on [Ubuntu 7.10 Gutsy Gibson](http://www.ubuntu.com/getubuntu/releasenotes/710) because it added a lot of useful software and configuration out of the box.

Having tried it on my laptop I saw everything I needed to get me started, from hardware to software, was already there. So I backup all my data and started my eXPerience.

PS: I had Windows XP Professional Edition installed...