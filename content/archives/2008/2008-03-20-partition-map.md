---
title: "Partition Map"
slug: "partition-map"
author: "Paulo Pereira"
date: 2008-03-20T16:13:00+00:00
lastmod: 2008-03-20T16:13:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
aliases:
  - /posts/partition-map/
---

First thing to decide is how will I partition my hard drive.

```text
/dev/sda
  /dev/sda1 ext3 /     10240 MB
  /dev/sda5 ext3 /home 10240 MB
  /dev/sda6 ext3 /data 58547 MB
  /dev/sda7 swap        1028 MB
```

I used some useful resources to decide:

- [Ubuntu Forums Partition Basics](http://ubuntuforums.org/showthread.php?t=282018)
- [Explanation of the Ubuntu / Linux file structure](http://ubuntu-tutorials.com/2006/12/20/explanation-of-the-ubuntu-linux-file-structure-ubuntu-all-versions/)