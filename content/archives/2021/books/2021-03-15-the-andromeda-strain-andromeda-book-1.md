---
title: "The Andromeda Strain (Andromeda Book 1) by Michael Crichton (1969)"
slug: "the-andromeda-strain-andromeda-book-1"
author: "Paulo Pereira"
date: 2021-03-15T22:00:00+00:00
lastmod: 2021-03-15T22:00:00+00:00
cover: "/posts/books/the-andromeda-strain-andromeda-book-1.png"
description: "Finished “The Andromeda Strain (Andromeda Book 1)” by Michael Crichton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Michael Crichton
book series:
  - Andromeda Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Michael Crichton
  - Andromeda Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
aliases:
  - /posts/books/the-andromeda-strain-andromeda-book-1
---

Finished “The Andromeda Strain”.
* Author: [Michael Crichton](/book-authors/michael-crichton)
* First Published: [September 1st 1969](/book-publication-year/1960s)
* Series: [Andromeda](/book-series/andromeda-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53127281-the-andromeda-strain)
>
> From the author of Jurassic Park, Timeline, and Sphere comes a captivating thriller about a deadly extraterrestrial microorganism, which threatens to annihilate human life.
>
> Five prominent biophysicists have warned the United States government that sterilization procedures for returning space probes may be inadequate to guarantee uncontaminated re-entry to the atmosphere. Two years later, a probe satellite falls to the earth and lands in a desolate region of northeastern Arizona. Nearby, in the town of Piedmont, bodies lie heaped and flung across the ground, faces locked in frozen surprise. What could cause such shock and fear? The terror has begun, and there is no telling where it will end.