---
title: "About me"
slug: "about"
toc: true
aliases:
  - /pages/about
---

Hi, my name is Paulo Pereira. 

I'm a coordinator at [SIBS](https://sibs.com), the central utility for payments, running the payment system end-to-end for more than 30 years, serving Banks, the Central Bank, government, etc. 

In 2005 I started working as a consultant in [Novabase](https://www.novabase.pt), where I stayed until 2017 when I joined [SIBS](https://sibs.com).

I'm a Linux and Open Source enthusiast in my spare time and I really enjoy reading. I’ve also been meditating since 2016 with [Headspace](https://www.headspace.com/).

I lived through the 80s and 90s in [Coruche](https://www.google.com/maps/@38.9593738,-8.5317964,1238m/data=!3m1!1e3), my hometown. In the year 2000 I moved to [Lisbon](https://www.google.com/maps/@38.7436214,-9.1952226,14054m/data=!3m1!1e3) to study in the [Faculty of Sciences](https://ciencias.ulisboa.pt/en/informatics) of the University of Lisbon, where I completed my [Licentiate](http://en.wikipedia.org/wiki/Licentiate#Portugal) degree.

I'm also a proud father since 2022.

## Contacts

You may reach me in several ways:

* SimpleX Chat [invite](https://simplex.chat/contact/#/?v=1-2&smp=smp%3A%2F%2F0YuTwO05YJWS8rkjn9eLJDjQhFKvIYd8d4xG8X1blIU%3D%40smp8.simplex.im%2FS5AjypkVk6ynYfnrtynDUNFT_qd_eGRd%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEA7Vbm8UJlN_imOm2u-7CWb4XLCQ6mBhA8irTrY7bxeXo%253D%26srv%3Dbeccx4yfxxbvyhqypaavemqurytl6hozr47wfc7uuecacjqdvwpw2xid.onion) - [Learn more](/posts/simplex-chat)
* (*Not using it at the moment*) XMPP - [paapereira@conversations.im](https://conversations.im/i/paapereira@conversations.im?omemo-sid-1999939473=9df5caa9906214747ff8e3d2bb0d2b4dd6edb46b6f164f69338c876910d11106&omemo-sid-518817183=a0db5e9d1c7b0b7c5dface0f30de6cb36523fb2c142533b53c04067b53b48b36) - [Learn more](/posts/xmpp-2023-update)
* (*Not using it at the moment*) Signal - [if you have my phone number](https://www.signal.org/)
* (*Not using it at the moment*) Matrix - [@paapereira](https://matrix.to/#/@paapereira:matrix.org)
* Email - [paulo@paapereira.xyz](mailto:paulo@paapereira.xyz)
* WhatsApp - [if you have my phone number](https://www.whatsapp.com/)

(*Not using it at the moment*) It’s a good idea to protect your messages in the digital world. If you use an untrusted email service or messaging app, you can easily [encrypt](https://keybase.io/encrypt#paapereira) messages to me and [verify](https://keybase.io/verify) it was me that sent a signed message at [Keybase](https://keybase.io/paapereira).

(*Not using it at the moment*) This is my [public key](https://keybase.io/paapereira/pgp_keys.asc?fingerprint=7f00ef29a2e047eb4e2faf65cfd73bf440ff1d99).

I’m not a big fan of social media, but you can find me at [Keybase](https://keybase.io/paapereira) (*Not using it at the moment*), [Goodreads](https://www.goodreads.com/paapereira), [Mastodon](https://mastodon.social/@paapereira) (*Not using it at the moment*)  and [LinkedIn](https://www.linkedin.com/in/paapereira). I’m **not** on Facebook, Instagram, Twitter and alike.

You can get my card in a terminal [here](/pages/card).

## Professional career

Since 2017: Coordinator at [SIBS](https://sibs.com)

### Past experience

* Jan 2014 - Mar 2017: Associate Specialist at Novabase Business Solutions
* Jan 2011 - Dec 2013: Senior Professional at Novabase Business Solutions
* Jan 2010 - Dec 2010: Senior Professional at Novabase Consulting
* Jan 2009 – Dec 2009: Senior Consultant at Novabase Business Intelligence
* Jan 2007 – Dec 2008: Consultant at Novabase Business Intelligence
* Jun 2005 – Dec 2006: Associate Consultant at Novabase Business Intelligence

## Linux

I’m currently running [Arch Linux](https://www.archlinux.org/) in all my personal computers. A NUC6i5SYH as my desktop, a [NUC7i3BNH](/posts/2020/05/new-nuc7i3bnh/) as my server and a [Surface Go](/posts/install-arch-linux-surface-go) as my laptop.

It all started in the 90s playing around with [Red Hat](https://en.wikipedia.org/wiki/Red_Hat_Linux) 5.2 (if my memory is correct). At that time it was just curiosity and Windows was still my main operating system. Even then I already tended more to the likes of [Netscape](https://en.wikipedia.org/wiki/Netscape), [StarOffice](https://en.wikipedia.org/wiki/StarOffice) and [Firefox](https://en.wikipedia.org/wiki/Firefox).

[Read more](/pages/linux)

## Books

Reading is one my favorite things to do since I was little.

You can find me at [Goodreads](https://www.goodreads.com/paapereira) and follow my [book](/categories/books/) posts.

[Read more](/pages/books)

## Buy me a coffee (or a book)

* Amazon ebook wishlist (books@paapereira.xyz): https://www.amazon.com/hz/wishlist/ls/1RVET8NF3QUJ2
* [Short Books Wishlist](/tags/books-wishlist)
* [Ⱥlgos (ALGO)](algorand://GYUPWKJ4E7IER3ITLV5UFC5DORODHXWN7ANNZ4TNVPBN3GGC3AV42ESGLY)
* ₿itcoin (BTC) [on chain](bitcoin:bc1q7u7veg9hcth9x8mhdlkja6a7gxvtn9psygmtm3) or [on lightning](https://strike.me/paapereira/)
* Paypal: https://paypal.me/paapereira
* Vultr referral: https://www.vultr.com/?ref=8791237
* Vultr (get $100 on signup): https://www.vultr.com/?ref=8791238-6G