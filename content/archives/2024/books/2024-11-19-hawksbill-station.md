---
title: "Hawksbill Station by Robert Silverberg (1967)"
slug: "hawksbill-station"
author: "Paulo Pereira"
date: 2024-11-19T22:00:00+00:00
lastmod: 2024-11-19T22:00:00+00:00
cover: "/posts/books/hawksbill-station.jpg"
description: "Finished “Hawksbill Station” by Robert Silverberg."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Robert Silverberg
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Robert Silverberg
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “Hawksbill Station”.
* Author: [Robert Silverberg](/book-authors/robert-silverberg)
* First Published: [January 1, 1967](/book-publication-year/1960s)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/49951244-hawksbill-station)
>
> In the barren landscape of the late Cambrian period, a penal colony sits high above the ocean on the east coast of what would become the United States. The men—political prisoners—have been sent from the twenty-first century on a one-way ticket to a lifetime of exile. Their lonely existence has taken its toll . . .
> 
> Jim Barrett was once the physically imposing leader of an underground movement dedicated to toppling America’s totalitarian government. Now he is nothing but a crippled old man, the camp’s de facto ruler due to his seniority. His mind is still sharp, having yet to succumb to the psychosis that claims more and more men each day. So when a new prisoner is transported to the colony—a startlingly young and suspiciously apolitical man—Barrett’s instincts go on high alert.
> 
> As Barrett reminisces about his revolutionary past, he uncovers the new prisoner’s secrets—and faces a shocking revelation that thrusts him into a future he never dreamed possible . . .