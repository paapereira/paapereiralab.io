---
title: "Books Schedule update"
slug: "book-schedule-november-2023"
author: "Paulo Pereira"
date: 2023-11-01T13:15:00+00:00
lastmod: 2023-11-01T13:15:00+00:00
description: "November Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my November TBR (To Be Read) update for the next months.

* **[Read]** Light Bringer (Red Rising Saga 6) by Pierce Brown
* **[Read]** Stay Out of the Basement (Goosebumps 2) by R. L. Stine
* **[Read]** Heart-Shaped Box by Joe Hill
* **[Read]** Monster Blood (Goosebumps 3) by R. L. Stine
* **[Read]** Horrorstör by Grady Hendrix
* **[Read]** Say Cheese and Die! (Goosebumps 4) by R. L. Stine
* **[Reading]** The Halloween Tree by Ray Bradbury
* Just Out of Jupiter's Reach (Far Reaches 5) by Nnedi Okorafor
* Slow Time Between the Stars (Far Reaches 6) by John Scalzi
* Dead Beat (The Dresden Files 7) by Jim Butcher
* Bethany's Sin by Robert R. McCammon
* Diamond Dogs, Turquoise Days by Alastair Reynolds
* Wool (Silo 1) by Hugh Howey
* Pirate Latitudes by Michael Crichton
* The Hero of Ages (The Mistborn Saga 3) by Brandon Sanderson
* A Call to Arms (The Damned 1) by Alan Dean Foster
* Precious Little Things by Adrian Tchaikovsky
* Star Wars Episode IV: A New Hope by George Lucas

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson

I also have always an audiobook going too:

* **[Read]** Elon Musk by Walter Isaacson
* **[Read]** The Ocean at the End of the Lane by Neil Gaiman
* **[Reading]** Rose Madder by Stephen King
* Red Company: First Strike! by B. V. Larson
* Parallel Worlds by Michio Kaku
* Who Goes There? by John W. Campbell Jr.
* Accessory to War by Neil Degrasse Tyson