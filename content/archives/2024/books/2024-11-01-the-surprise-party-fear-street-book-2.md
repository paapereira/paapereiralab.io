---
title: "The Surprise Party (Fear Street Book 2) by R.L. Stine (1989)"
slug: "the-surprise-party-fear-street-book-2"
author: "Paulo Pereira"
date: 2024-11-01T08:00:00+00:00
lastmod: 2024-11-01T08:00:00+00:00
cover: "/posts/books/the-surprise-party-fear-street-book-2.jpg"
description: "Finished “The Surprise Party (Fear Street Book 2)” by R.L. Stine."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - R.L. Stine
book series:
  - Fear Street Series
book genres:
  - Fiction
  - Horror
book scores:
  - 6/10 Books
tags:
  - Book Log
  - R.L. Stine
  - Fear Street Series
  - Fiction
  - Horror
  - 6/10 Books
  - Ebook
---

Finished “The Surprise Party”.
* Author: [R.L. Stine](/book-authors/r.l.-stine)
* First Published: [August 1, 1989](/book-publication-year/1980s)
* Series: [Fear Street](/book-series/fear-street-series) Book 2
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53772778-the-surprise-party)
>
> When an old friend returns to town, Meg plans a party to bring the old gang back together, but someone—or something—will do anything to keep it from happening in this chilling tale from Goosebumps author R.L. Stine.
> 
> A year ago, Meg Dalton’s group of friends fractured. Evan died in the Fear Street woods. Ellen moved away. The ones that stayed behind changed. And Meg felt as if she’d lost her best friends. Lately, even her boyfriend Tony has been acting moody and strange. But things may finally be looking up. Ellen is coming to visit! And what better way to bring old friends together than with a surprise party for her arrival?
> 
> That’s when the terror begins—the phone calls, the threats, the acts of violence. “Cancel the party—or else,” whispers the voice on the phone. Meg is terrified. Who would do so many terrible things to stop her party? To find out, she’ll have to venture into the dark Fear Street woods that took Evan’s life last year.