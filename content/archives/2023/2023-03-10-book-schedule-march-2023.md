---
title: "Books Schedule update"
slug: "book-schedule-march-2023"
author: "Paulo Pereira"
date: 2023-03-10T21:00:00+00:00
lastmod: 2023-03-10T21:00:00+00:00
description: "March Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my March TBR (To Be Read) update for the next months.

* **[Read]** Boy's Life by Robert R. McCammon
* **[Read]** Bear Head (Dogs of War 2) by Adrian Tchaikovsky
* **[Read]** Death Masks (The Dresden Files 5) by Jim Butcher
* **[Reading]** Prey by Michael Crichton
* Absolution Gap (Revelation Space 3) by Alastair Reynolds
* Life of Pi by Yann Martel
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* Elder Race by Adrian Tchaikovsky
* Baal by Robert R. McCammon
* Blood Rites (The Dresden Files 6) by Jim Butcher

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson

I also have always an audiobook going too:

* **[Read]** The Signal and the Noise by Nate Silver
* **[Read]** Hell's Reach (Galactic Liberation 6) by David Vandyke & B. V. Larson
* **[Read]** The Secret Life of Walter Mitty by James Thurber
* **[Reading]** Gerald's Game by Stephen King
* Until the End of Time by Brian Greene
* Star Runner by B. V. Larson
* Dolores Claiborne by Stephen King
* Dragons of Eden: Speculations on the Evolution of Human Intelligence by Carl Sagan