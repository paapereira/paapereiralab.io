---
title: "Firefox Download Day is June 17, 2008"
slug: "firefox-download-day-is-june-17-2008"
author: "Paulo Pereira"
date: 2008-06-12T21:25:00+01:00
lastmod: 2008-06-12T21:25:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Firefox
aliases:
  - /posts/firefox-download-day-is-june-17-2008/
---

It's annouced!! Firefox 3.0 will be released in **June 17**, it's a Tuesday.

Firefox is trying to set a [Guinness World Record](/posts/firefox-guinness-world-record/), so don't forget, [pledge](http://www.spreadfirefox.com/en-US/worldrecord) now and download Firefox 3.0 next Tuesday.