---
title: "Heaven's River (Bobiverse Book 4) by Dennis E. Taylor (2020)"
slug: "heavens-river-bobiverse-book-4"
author: "Paulo Pereira"
date: 2020-11-08T17:56:00+00:00
lastmod: 2020-11-08T17:56:00+00:00
cover: "/posts/books/heavens-river-bobiverse-book-4.jpg"
description: "Finished “Heaven's River (Bobiverse Book 4)” by Dennis E. Taylor."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Dennis E. Taylor
book series:
  - Bobiverse Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Dennis E. Taylor
  - Bobiverse Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Heaven's River”.
* Author: [Dennis E. Taylor](/book-authors/dennis-e.-taylor)
* First Published: [September 24th 2020](/book-publication-year/2020)
* Series: [Bobiverse](/book-series/bobiverse-series) Book 4
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/42950440-heaven-s-river)
>
> Civil war looms in the Bobiverse in this brand-new, epic-length adventure by Audible number-one best seller Dennis E. Taylor.
>
> More than a hundred years ago, Bender set out for the stars and was never heard from again. There has been no trace of him despite numerous searches by his clone-mates. Now Bob is determined to organize an expedition to learn Bender’s fate - whatever the cost.
>
> But nothing is ever simple in the Bobiverse. Bob’s descendants are out to the 24th generation now, and replicative drift has produced individuals who can barely be considered Bobs anymore. Some of them oppose Bob’s plan; others have plans of their own. The out-of-control moots are the least of the Bobiverse’s problems.
>
> Undaunted, Bob and his allies follow Bender’s trail. But what they discover out in deep space is so unexpected and so complex that it could either save the universe - or pose an existential threat the likes of which the Bobiverse has never faced.