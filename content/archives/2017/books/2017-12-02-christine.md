---
title: "Christine by Stephen King (1983)"
slug: "christine"
author: "Paulo Pereira"
date: 2017-12-02T22:40:00+00:00
lastmod: 2017-12-02T22:40:00+00:00
cover: "/posts/books/christine.jpg"
description: "Finished “Christine” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - 7/10 Books
  - Audiobook
---

Finished “Christine”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [April 29th 1983](/book-publication-year/1980s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31184972-christine)
>
> Evil is alive in Libertyville. It inhabits a custom-painted red and white 1958 Plymouth Fury and the teenage boy, Arnold Cunningham, who buys it from the strange Roland LeBay.
>
> Helped by Arnold's girlfriend Leigh Cabot, Dennis Guilder embarks to find out the real truth behind Christine and finds more than he bargained for: from murder, to suicide, and a strange feeling that surrounds Christine -- she gets even with anyone that crosses her! Can Dennis save Arnold from the evil that is Christine?