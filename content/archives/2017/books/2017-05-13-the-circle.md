---
title: "The Circle by Dave Eggers (2013)"
slug: "the-circle"
author: "Paulo Pereira"
date: 2017-05-13T23:12:00+01:00
lastmod: 2017-05-13T23:12:00+01:00
cover: "/posts/books/the-circle.jpg"
description: "Finished “The Circle” by Dave Eggers."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - Dave Eggers
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Dave Eggers
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/the-circle
---

Finished “The Circle”.
* Author: [Dave Eggers](/book-authors/dave-eggers)
* First Published: [2013](/book-publication-year/2013)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/29978131-the-circle)
>
> The Circle is the exhilarating new audiobook from Dave Eggers, bestselling author of A Hologram for the King, a finalist for the National Book Award.
>
> When Mae Holland is hired to work for the Circle, the world's most powerful internet company, she feels she's been given the opportunity of a lifetime. The Circle, run out of a sprawling California campus, links users' personal emails, social media, banking, and purchasing with their universal operating system, resulting in one online identity and a new age of civility and transparency.
>
> As Mae tours the open-plan office spaces, the towering glass dining facilities, the cozy dorms for those who spend nights at work, she is thrilled with the company's modernity and activity. There are parties that last through the night, there are famous musicians playing on the lawn, there are athletic activities and clubs and brunches, and even an aquarium of rare fish retrieved from the Marianas Trench by the CEO.
>
> Mae can't believe her luck, her great fortune to work for the most influential company in the world - even as life beyond the campus grows distant, even as a strange encounter with a colleague leaves her shaken, even as her role at the Circle becomes increasingly public. What begins as the captivating story of one woman's ambition and idealism soon becomes a heart-racing novel of suspense, raising questions about memory, history, privacy, democracy, and the limits of human knowledge.