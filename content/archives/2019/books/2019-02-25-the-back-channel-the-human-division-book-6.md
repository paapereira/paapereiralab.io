---
title: "The Back Channel (The Human Division Book 6) by John Scalzi (2013)"
slug: "the-back-channel-the-human-division-book-6"
author: "Paulo Pereira"
date: 2019-02-25T21:17:00+00:00
lastmod: 2019-02-25T21:17:00+00:00
cover: "/posts/books/the-back-channel-the-human-division-book-6.jpg"
description: "Finished “The Back Channel (The Human Division Book 6)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Back Channel”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [February 19th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 6
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17381279-the-back-channel)
>
> The Conclave is a confederation of 400 alien races - many of whom would like to see the Colonial Union - and the humans inside of it - blasted to extinction. To avoid a conflict that neither side can afford, Conclave leader General Tarsem Gau appoints Hafte Sorvalh to resolve an emerging diplomatic crisis with the humans, before the only acceptable solution is war.
>
> "The Back Channel" is a tale from John Scalzi's The Human Division, a series of self-contained but interrelated short stories set in the Old Man's War universe.