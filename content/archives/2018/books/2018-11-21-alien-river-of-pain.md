---
title: "Alien: River of Pain by Christopher Golden (2014)"
slug: "alien-river-of-pain"
author: "Paulo Pereira"
date: 2018-11-21T22:30:00+00:00
lastmod: 2018-11-21T22:30:00+00:00
cover: "/posts/books/alien-river-of-pain.jpg"
description: "Finished “Alien: River of Pain” by Christopher Golden."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - Christopher Golden
book series:
  - Alien Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Christopher Golden
  - Alien Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Alien: River of Pain”.
* Author: [Christopher Golden](/book-authors/christopher-golden)
* First Published: [November 25th 2014](/book-publication-year/2014)
* Series: [Alien Series](/book-series/alien-series)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34743929-alien)
>
> Ellen Ripley finally returns to Earth, only to discover that LV-426 - resting place of the Nostromo and the xenomorph she first encountered - has been renamed Acheron.
>
> Protected by Colonial Marines, the colonists seek to terraform the storm-swept planet against all the odds. But in the face of brutal living conditions and the daily struggles of a new world, there is humanity and hope. Anne and Russell Jorden - two colonists who are seeking a fortune that eluded them on Earth - are expecting their first born child.
>
> The birth of Rebecca Jorden, known to her family as 'Newt', is a cause for celebration. But as the colony grows and expands, so too do the political struggles between a small detachment of Colonial Marines and the Weyland-Yutani scientists posted on Acheron. Willing to overlook their orders in order to serve the Company's interests, these scientists have another far more sinister agenda - to covertly capture a living Alien.
>
> The wildcatters discover a vast, decaying spaceship. The horseshoe-shaped vessel is of particular interest to Weyland-Yutani, and may be the answer to their dreams. But what Anne and Russell find on board proves to be the stuff, not of dreams, but of nightmares.