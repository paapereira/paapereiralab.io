---
title: "Listening to podcast using gpodder, gpodder.net and AntennaPod"
slug: "gpodder-antennapod"
author: "Paulo Pereira"
date: 2020-09-06T11:43:13+01:00
lastmod: 2020-09-06T11:43:13+01:00
description: "I’ve been using [PocketCasts](https://www.pocketcasts.com/) to listen to my podcasts for quite some time. In Android using with the app and in Linux in the website (using [MellowPlayer](https://gitlab.com/ColinDuquesnoy/MellowPlayer)).\n

But I’ve also been looking to a more open source way to listen to my podcasts.\n

I’m using for two weeks now [AntennaPod](https://antennapod.org/) in Android, [gpodder.net](https://gpodder.net/) to manage my subscriptions and [gpodder](https://gpodder.github.io/) in Arch Linux."
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Android
  - gpodder
  - gpodder.net
  - AntennaPod
---

I’ve been using [PocketCasts](https://www.pocketcasts.com/) to listen to my podcasts for quite some time. In Android using with the app and in Linux in the website (using [MellowPlayer](https://gitlab.com/ColinDuquesnoy/MellowPlayer)).

But I’ve also been looking to a more open source way to listen to my podcasts.

I’m using for two weeks now [AntennaPod](https://antennapod.org/) in Android, [gpodder.net](https://gpodder.net/) to manage my subscriptions and [gpodder](https://gpodder.github.io/) in Arch Linux.

## Installation and Setup

* Install [AntennaPod](https://antennapod.org/) on [F-Droid](https://f-droid.org/packages/de.danoeh.antennapod/) or [Google Play](https://play.google.com/store/apps/details?id=de.danoeh.antennapod)
* Install [gpodder](https://gpodder.github.io/) (in my case in Arch Linux `yay -S gpodder`)
* Create an account at [gpodder.net](https://gpodder.net/)
* Configure in AntennaPod and gpodder your new gpodder.net account

## Managing Subscriptions

I exported my subscriptions in PocketCasts.

In https://gpodder.net/devices/ I checked that both my phone and desktop were there and that they synced with each other (there’s an option for that).

Then I imported the subscriptions in AntennaPod.

They synced correctly with gpodder.

The boring part was going to each subscription in AntennaPod and ‘mark as played’ old episodes.

## Syncing and day to day

This setup is not perfect and the synchronization is in part automatic and in part manual.

When I finish listening to a new episode in AntennaPod, it still new in gpodder, but in the Duration column it appears as Finished. I can then delete the episode.

When I finish listening to a new episode in gpodder I have to manually go to AntennaPod and mark it as played.

Not perfect I know, but it works for me. Any suggestion please let me know.

## Listening to episodes in gpodder

gpodder can download your episodes but it isn’t a player. So your have to use another application.

Depending on the application, the episode may start from the beginning every time you start listening.

I configured gpodder to use mpv with the following command, so it always starts from the position I left it.

`mpv --save-position-on-quit --speed=1.4 --player-operation-mode=pseudo-gui -- %U`