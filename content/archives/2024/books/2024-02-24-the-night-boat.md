---
title: "The Night Boat by Robert R. McCammon (1980)"
slug: "the-night-boat"
author: "Paulo Pereira"
date: 2024-02-24T22:00:00+00:00
lastmod: 2024-02-24T22:00:00+00:00
cover: "/posts/books/the-night-boat.jpg"
description: "Finished “The Night Boat” by Robert R. McCammon."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Robert R. McCammon
book series:
  - 
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Robert R. McCammon
  - Fiction
  - Horror
  - 7/10 Books
  - Ebook
---

Finished “The Night Boat”.
* Author: [Robert R. McCammon](/book-authors/robert-r.-mccammon)
* First Published: [August 1, 1980](/book-publication-year/1980s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/39932166-the-night-boat)
>
> Robert Moore had a cushy life in Baltimore. The son of a bank president, he could have had the old man’s job if he’d just waited in line. But Moore isn’t the patient type, and rather than spend his life trapped behind a desk, he decamped for the Caribbean, to pass his days diving beneath the perfect blue sea. One day, diving deeper than usual, he spies a sunken ship. His investigations disrupt an unexploded depth charge, which hurls Robert to the surface with the sunken ship not far behind.
> 
> The U-boat, still seaworthy after all these decades, drifts towards the island and gets caught on the reef. A strange knocking echoes from inside the hull, as though something within is still alive. When Robert opens the long-closed hatch, he’ll learn that some sunken treasure is better left undisturbed.