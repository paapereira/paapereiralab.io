---
title: "New Kindle Paperwhite"
slug: "kindle-paperwhite-signature"
author: "Paulo Pereira"
date: 2024-10-25T21:00:00+01:00
lastmod: 2024-10-25T21:00:00+01:00
description: "Today, 4 years later, I got a new [Kindle Paperwhite Signature Edition](https://www.amazon.com/All-new-Amazon-Kindle-Paperwhite-Signature/dp/B0C8RR4WN3/)."
draft: false
toc: false
categories:
  - Books
tags:
  - Books
  - Kindle Paperwhite
---

Today, 4 years later, I got a new [Kindle Paperwhite Signature Edition](https://www.amazon.com/All-new-Amazon-Kindle-Paperwhite-Signature/dp/B0C8RR4WN3/).

First impressions: bigger and better screen and suuuuper fast comparing to my current one.

![Kindle Paperwhite Signature Edition](/posts/2024/2024-10-25-kindle-paperwhite-signature/kindle-paperwhite-signature.jpg)