---
title: "Star Wars Episode IV: A New Hope by George Lucas (1976)"
slug: "star-wars-episode-iv-a-new-hope"
author: "Paulo Pereira"
date: 2024-01-31T21:00:00+00:00
lastmod: 2024-01-31T21:00:00+00:00
cover: "/posts/books/star-wars-episode-iv-a-new-hope.jpg"
description: "Finished “Star Wars Episode IV: A New Hope” by George Lucas."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - George Lucas
book series:
  - Star Wars Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - George Lucas
  - Star Wars Universe
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “Star Wars Episode IV: A New Hope”.
* Author: [George Lucas](/book-authors/george-lucas)
* First Published: [December 5, 1976](/book-publication-year/1970s)
* Series: [Star Wars Universe](/book-series/star-wars-universe)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/18910932-star-wars)
>
> Luke Skywalker was a twenty-year-old who lived and worked on his uncle's farm on the remote planet of Tatooine...and he was bored beyond belief. He yearned for adventures that would take him beyond the farthest galaxies. But he got much more than he bargained for....