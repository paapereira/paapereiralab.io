---
title: "Firefox Download Day Certification"
slug: "firefox-download-day-certification"
author: "Paulo Pereira"
date: 2008-06-25T20:54:00+01:00
lastmod: 2008-06-25T20:54:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
  - Firefox
---

Have you downloaded Firefox 3.0 at [Download Day](/posts/firefox-download-day-is-june-17-2008/) and helped Firefox to set a new world record?

Yes? Get your [certificate](http://www.spreadfirefox.com/en-US/worldrecord/certificate_form) then :)