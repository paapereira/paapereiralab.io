---
title: "Install Chromium with Lucid looks"
slug: "install-chromium-with-lucid-looks"
author: "Paulo Pereira"
date: 2010-05-29T17:26:00+01:00
lastmod: 2010-05-29T17:26:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 10.04
  - Lucid Lynx
  - Chromium
---

I've been using Firefox for years, but I'm trying out [Chromium](http://www.chromium.org/) and so far it's been great and fast.

## Install

You can just go to Ubuntu Software Center and type Chromium to install, or install via Terminal:

```bash
sudo apt-get install chromium-browser
```

## Daily Builds

If you want to get the latests versions of Chromium add to your Software Sources:

```text
deb http://ppa.launchpad.net/chromium-daily/ppa/ubuntu lucid main
```

## Customize to look like Lucid

You can install the Ambiance theme from [Google Chrome Extensions](http://chrome.google.com/extensions).

[direct link to ambiance theme](https://chrome.google.com/extensions/detail/elnmibmpefhmfgphdphdncoogpbfmlbp)

To use Lucid system title bar and borders right click on the Chromium bar, next to the tab, and choose the appropriate option.

## Extensions

You can install [Google Chrome Extensions](http://chrome.google.com/extensions) in Chromium.