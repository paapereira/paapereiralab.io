---
title: "Gnome Touchpad Issue"
slug: "gnome-touchpad-issue"
author: "Paulo Pereira"
date: 2016-12-04T22:54:32+00:00
lastmod: 2016-12-04T22:54:32+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Gnome
---

In Gnome 3.20 (I think) Gnome switched to use `libinput` over `synaptic` for the touchpad lib.

You need to remove `xf86-input-synaptic` and install `xf86-input-libinput`. You'll also need to remove the synaptic config file.