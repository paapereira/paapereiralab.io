---
title: "Harry Potter and the Chamber of Secrets (Harry Potter Book 2) by J.K. Rowling (1998)"
slug: "harry-potter-and-the-chamber-of-secrets-harry-potter-book-2"
author: "Paulo Pereira"
date: 2024-11-15T22:00:00+00:00
lastmod: 2024-11-15T22:00:00+00:00
cover: "/posts/books/harry-potter-and-the-chamber-of-secrets-harry-potter-book-2.jpg"
description: "Finished “Harry Potter and the Chamber of Secrets (Harry Potter Book 2)” by J.K. Rowling."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - J.K. Rowling
book series:
  - Harry Potter Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 8/10 Books
tags:
  - Book Log
  - J.K. Rowling
  - Harry Potter Series
  - Fiction
  - Fantasy
  - 8/10 Books
  - Ebook
---

Finished “Harry Potter and the Chamber of Secrets”.
* Author: [J.K. Rowling](/book-authors/j.k.-rowling)
* First Published: [July 2, 1998](/book-publication-year/1990s)
* Series: [Harry Potter](/book-series/harry-potter-series) Book 2
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28141183-harry-potter-and-the-chamber-of-secrets)
>
> Harry Potter's summer has included the worst birthday ever, doomy warnings from a house-elf called Dobby, and rescue from the Dursleys by his friend Ron Weasley in a magical flying car! Back at Hogwarts School of Witchcraft and Wizardry for his second year, Harry hears strange whispers echo through empty corridors - and then the attacks start. Students are found as though turned to stone ... Dobby's sinister predictions seem to be coming true.