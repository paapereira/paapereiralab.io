---
title: "Empire (Star Force Book 6) by B.V. Larson (2012)"
slug: "empire-star-force-book-6"
author: "Paulo Pereira"
date: 2016-06-26T23:37:00+01:00
lastmod: 2016-06-26T23:37:00+01:00
cover: "/posts/books/empire-star-force-book-6.jpg"
description: "Finished “Empire (Star Force Book 6)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - B.V. Larson
book series:
  - Star Force Series
book genres:
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Force Series
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Empire”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [October 6th 2012](/book-publication-year/2012)
* Series: [Star Force](/book-series/star-force-series) Book 6
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17240767-empire)
>
> In EMPIRE, the sixth book of the Star Force Series, the story moves in a new direction. Earth falls quiet, and the few reports coming out of the homeworld are increasingly strange. Isolated in the Eden system, Riggs realizes his enemies are forming an alliance against him. Crushed between two monstrous fleets, the last defenders of Star Force must forge their own alliance with the biotic aliens. But which alien race can Riggs truly trust?