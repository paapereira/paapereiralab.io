---
title: "The Surrogate Mother by Freida McFadden (2018)"
slug: "the-surrogate-mother"
author: "Paulo Pereira"
date: 2024-10-07T21:00:00+01:00
lastmod: 2024-10-07T21:00:00+01:00
cover: "/posts/books/the-surrogate-mother.jpg"
description: "Finished “The Surrogate Mother” by Freida McFadden."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Freida McFadden
book series:
  - 
book genres:
  - Fiction
  - Thriller
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Freida McFadden
  - Fiction
  - Thriller
  - 8/10 Books
  - Ebook
---

Finished “The Surrogate Mother”.
* Author: [Freida McFadden](/book-authors/freida-mcfadden)
* First Published: [October 10, 2018](/book-publication-year/2018)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40893132-the-surrogate-mother)
>
> Abby wants a baby more than anything.
> 
> But after years of failed infertility treatments and adoptions that have fallen through, it seems like motherhood is not in her future. That is, until her personal assistant Monica makes a generous offer that will make all of Abby's dreams come true.
> 
> Or all of her nightmares.
> 
> Because it turns out Monica isn't who she says she is. The woman now carrying Abby's child has dark, twisted secrets.
> 
> And she will stop at nothing to get what she wants. 