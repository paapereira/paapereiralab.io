---
title: "Adventures in Arch (Part III): Customizing"
slug: "adventures-in-arch-part-iii-customizing"
author: "Paulo Pereira"
date: 2013-06-09T20:20:00+01:00
lastmod: 2013-06-15T20:20:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
---

In this part I will show the customizations I made in the desktop environment.

I will keep updating this post in the next weeks, in case I have other tips.

![Arch Linux](/posts/2013/2013-06-09-adventure-in-arch-part-i-installing/customizing.png)

## Fonts

I like Ubuntu fonts so:

```bash
sudo pacman -S ttf-ubuntu-font-family
```

## GNOME Shell tweaks

![Tweak Tool](/posts/2013/2013-06-09-adventure-in-arch-part-i-installing/tweak-tool.png)

Install GNOME Tweak Tool:

```bash
sudo pacman -S gnome-tweak-tool
```

Some changes I made:

* Files Menu: activated the option to always use the location entry.
* Fonts: I changed all the fonts to Ubuntu.
* Shell: activated Show Date in clock; chose All in the arrangement of buttons on the titlebar.
* Theme: enabled dark theme.
* Typing: chose Control + Alt + Backspace to kill the X Server.

## GNOME extensions

Using GNOME's Web browser go to: https://extensions.gnome.org/

I'm using:

* Activities Configurator
* Advanced Volume Mixer
* AlternateTab
* Autohide Battery (not working with my UPS)
* Dash to Dock
* Places Status Indicator
* Quit Button
* TopIcons
* User Themes

## Region and Language

Go to Settings > Region and Language and in the Input Sources I add the Portuguese keyboard.
Don't forget to also add it in the Login Screen menu.

## Online Accounts

Go to Settings > Online Accounts and add Google and Facebook, for example.

## Themes

Change the themes with GNOME Tweak Tool

Cursor theme:
```bash
yaourt -S xcursor-human
```

Shell and GTK theme:
```bash
yaourt -S gtk-theme-numix
```