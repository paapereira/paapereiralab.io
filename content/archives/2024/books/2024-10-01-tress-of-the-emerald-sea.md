---
title: "Tress of the Emerald Sea by Brandon Sanderson (2023)"
slug: "tress-of-the-emerald-sea"
author: "Paulo Pereira"
date: 2024-10-01T20:00:00+01:00
lastmod: 2024-10-01T20:00:00+01:00
cover: "/posts/books/tress-of-the-emerald-sea.jpg"
description: "Finished “Tress of the Emerald Sea” by Brandon Sanderson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - Brandon Sanderson
book series:
  - 
book genres:
  - Fiction
  - Fantasy
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Brandon Sanderson
  - Fiction
  - Fantasy
  - 8/10 Books
  - Ebook
---

Finished “Tress of the Emerald Sea”.
* Author: [Brandon Sanderson](/book-authors/brandon-sanderson)
* First Published: [January 10, 2023](/book-publication-year/2023)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/60531406-tress-of-the-emerald-sea)
>
> The only life Tress has known on her island home in an emerald-green ocean has been a simple one, with the simple pleasures of collecting cups brought by sailors from faraway lands and listening to stories told by her friend Charlie. But when his father takes him on a voyage to find a bride and disaster strikes, Tress must stow away on a ship and seek the Sorceress of the deadly Midnight Sea. Amid the spore oceans where pirates abound, can Tress leave her simple life behind and make her own place sailing a sea where a single drop of water can mean instant death?