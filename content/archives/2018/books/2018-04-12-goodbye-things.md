---
title: "Goodbye, Things: The New Japanese Minimalism by Fumio Sasaki (2015)"
slug: "goodbye-things"
author: "Paulo Pereira"
date: 2018-04-12T22:26:00+01:00
lastmod: 2018-04-12T22:26:00+01:00
cover: "/posts/books/goodbye-things.jpg"
description: "Finished “Goodbye, Things: The New Japanese Minimalism” by Fumio Sasaki."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - Fumio Sasaki
book series:
  - 
book genres:
  - Nonfiction
  - Self Help
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Fumio Sasaki
  - Nonfiction
  - Self Help
  - 5/10 Books
  - Audiobook
---

Finished “Goodbye, Things: The New Japanese Minimalism”.
* Author: [Fumio Sasaki](/book-authors/fumio-sasaki)
* First Published: [June 12th 2015](/book-publication-year/2015)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36466191-goodbye-things)
>
> Fumio Sasaki is not an enlightened minimalism expert or organizing guru like Marie Kondo—he’s just a regular guy who was stressed out and constantly comparing himself to others, until one day he decided to change his life by saying goodbye to everything he didn’t absolutely need. The effects were remarkable: Sasaki gained true freedom, new focus, and a real sense of gratitude for everything around him. In Goodbye, Things Sasaki modestly shares his personal minimalist experience, offering specific tips on the minimizing process and revealing how the new minimalist movement can not only transform your space but truly enrich your life. The benefits of a minimalist life can be realized by anyone, and Sasaki’s humble vision of true happiness will open your eyes to minimalism’s potential.