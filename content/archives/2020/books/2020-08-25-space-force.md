---
title: "Space Force by Jeremy Robinson (2018)"
slug: "space-force"
author: "Paulo Pereira"
date: 2020-08-25T22:04:00+01:00
lastmod: 2020-08-25T22:04:00+01:00
cover: "/posts/books/space-force.png"
description: "Finished “Space Force” by Jeremy Robinson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Jeremy Robinson
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Jeremy Robinson
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
aliases:
  - /posts/books/space-force
---

Finished “Space Force”.
* Author: [Jeremy Robinson](/book-authors/jeremy-robinson)
* First Published: [October 27th 2018](/book-publication-year/2018)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44288153-space-force)
>
> Five years ago, the US Space Force became a reality. And while those writing the checks took things very seriously, the other military branches did not. As a result, Space Force was populated by undesirables: men and women who made too many mistakes, didn't follow the rules, or...slept with the wrong general's daughter. Three times. On camera. It was a mistake, okay?
>
> My name is Captain Ethan Stone, a decorated member of SEAL Team Six turned Space Force "recruit". While the powers that be attempted to make Space Force an embarrassment, they also sent some of the very best minds, fighters, and pilots to the program, because sometimes the best of us decide to take a stand against those same powers. Yeah, yeah. Not me. Though I seem to recall I was standing when...sorry. It's a distracting memory.
>
> Flash forward five years and between tenses. President West is in office. Power has shifted. And Space Force is defunded. With just a handful of us still living on base, we find ourselves being evicted. But before all of us can leave, aliens invade. Really ugly ones, too. Super nasty. I don't think I could describe them here without getting the audiobook banned.
>
> Anywho, using their advanced technology, they seal off Space Force Command behind a shrinking force field and kick off a battle royale to the death. Human vs. alien. To the victors goes the Earth. While my boy Frank Taylor and I throw down inside the force field, my main squeeze, First Lieutenant Jennifer Hale, leads an international strike team into freakin' space. It's nuts. Crazy action. Super funny, too, in like a Deadpool way, if that's your thing.
>
> The only way to really know what went down is to snag a copy of this audiobook and listen to it for yourself...before it's too late. Or wait for the movie. There is going to be a movie, right?
>
> Hale and I agreed to let "best-selling"* author Jeremy Robinson tell our tales. He's not my favorite person in the world, but I think he captured our voices and the reality of what happened, printing every word we spoke, no matter how offensive, blunt, disgusting, or violent it was. If you're upset by what you hear in this audiobook, first, lighten up, but then blame me. Jeremy Robinson is a fragile flower. He's only to blame for hackneyed writing.
>
> *Best-selling is in quotes because the only best seller status that matters is New York Times. Doesn't matter if it's not a true reflection of actual sales, popularity, or quality. Everyone knows that, Robinson.