---
title: "Alien III: An Audible Original Drama by William Gibson (2019)"
slug: "alien-iii-an-audible-original-drama"
author: "Paulo Pereira"
date: 2020-11-09T21:39:00+00:00
lastmod: 2020-11-09T21:39:00+00:00
cover: "/posts/books/alien-iii-an-audible-original-drama.jpg"
description: "Finished “Alien III: An Audible Original Drama” by William Gibson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - William Gibson
book series:
  - Alien Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 4/10 Books
tags:
  - Book Log
  - William Gibson
  - Alien Series
  - Fiction
  - Science Fiction
  - 4/10 Books
  - Audiobook
---

Finished “Alien III: An Audible Original Drama”.
* Author: [William Gibson](/book-authors/william-gibson)
* First Published: [May 30th 2019](/book-publication-year/2019)
* Series: [Alien Series](/book-series/alien-series)
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/54711650-alien-iii)
>
> Audible is bringing William Gibson’s lost Alien III script to life in audio for the first time, to mark the 40th Anniversary of the birth of the Alien Franchise.
>
> Alongside a full cast, Michael Biehn and Lance Henriksen reprise their iconic roles as Corporal Hicks and Bishop from the 1986 film Aliens.
>
> Father of cyberpunk, William Gibson’s original script for Alien III, written in 1987 as a sequel to Aliens, never made it to our screens, although it went on to achieve cult status among fans as the third installment that might have been after being leaked online.
>
> This terrifying, cinematic multicast dramatization—directed by the multi award-winning Dirk Maggs, is the chance to experience William Gibson’s untold story and its terrifying, claustrophobic and dark encounters between humans and aliens, as a completely immersive audio experience.
>
> The story begins with the Sulaco on its return journey from LV-426. On board the military ship are the cryogenically frozen skeleton crew of that film’s survivors: Ripley, Hicks, Newt and Bishop.
>
> We travel aboard and hear an alarm blare. Our heroes are no longer alone....