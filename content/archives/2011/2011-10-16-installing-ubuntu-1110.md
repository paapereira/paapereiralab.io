---
title: "Installing Ubuntu 11.10"
slug: "installing-ubuntu-1110"
author: "Paulo Pereira"
date: 2011-10-16T21:34:00+01:00
lastmod: 2011-10-16T21:34:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 11.10
  - Oneiric Ocelot
---

Since [upgrading](/posts/upgrading-to-ubuntu-1110/) didn't work on my machine, I went ahead and did a fresh install.

As I keep my /home partition separated from my / partition (they're in  fact in different drives), I can install without much worries. I have to reinstall a bunch of things, but I was in need of a clean up anyway.

## Installing

- Backup your data!
- Be sure you have the latest updates prior to this new release:

```bash
sudo apt-get update
sudo apt-get upgrade
```

- [Download](http://www.ubuntu.com/download/ubuntu/download) Ubuntu. In my case was the 64bit version (ubuntu-11.10-desktop-amd64.iso).
- Check the [hash](https://help.ubuntu.com/community/UbuntuHashes) of the downloaded file:

```bash
md5sum ubuntu-11.10-alternate-amd64.iso
```

- Compare the output with the Ubuntu [hash list](https://help.ubuntu.com/community/UbuntuHashes):

> 62fb5d750c30a27a26d01c5f3d8df459 ubuntu-11.10-desktop-amd64.iso

-  Create a CD or a bootable USB stick. I used the "Startup Disk Creator" in Ubuntu. More information [here](http://www.ubuntu.com/download/ubuntu/download).
- Reboot into your CD or bootable USB stick.
- Install!

Installing Ubuntu it's pretty straightforward this days, so there's nothing much to say.

I selected the option to install the multimedia codecs and unchecked the option to download available updates (I will do that later).

If you have your /home partition separated from your / partition, be  sure to select the mount point, but leave the format option unchecked.

## Post install

After the install I was having troubles with Unity. I couldn't get to the desktop.

I logged out (Ctrl+Alt+Delete and Log Out), chose Unity 2D and logged in again.

Then I reset Unity to start with the default experience.

```bash
unity --reset
```

Finally I installed the NVIDIA drivers (*System Settings > Additional Drivers*), rebooted and log in in Unity without any problems.

## Extra software installed

- SSH and SSH File System for my [B3 server](/tags/b3-server) mounts

```bash
sudo apt-get install sshfs openssh-server
```

- Chromium

```bash
sudo add-apt-repository ppa:chromium-daily/ppa
sudo apt-get install chromium-browser
```

- Ubuntu Restricted Extras

```bash
sudo apt-get install ubuntu-restricted-extras
```

- Java (no more Sun Java...)

```bash
sudo apt-get install openjdk-7-jre
```

- HP Linux Imaging and Printing GUI

```bash
sudo apt-get install hplip-gui
```

- Dropbox

```bash
rm -r ~/.dropbox-dist
sudo apt-get install nautilus-dropbox
dropbox start -i
```

-  CompizConfig Settings Manager

```bash
sudo apt-get install compizconfig-settings-manager
```

- Gnome Sushi (file previews)

```bash
sudo apt-get install gnome-sushi
```

- [Simple LightDM Manager](http://www.omgubuntu.co.uk/2011/10/simple-lightdm-manager-lets-easily-tweak-ubuntu-11-10-login-screen/)
- GCStar

```bash
sudo apt-get install gcstar
```

- VLC

```bash
sudo apt-get install vlc
```

- Gaupol

```bash
sudo apt-get install gaupol
```

- Portuguese Aspell

```bash
sudo apt-get install aspell-pt-pt
```

- GParted

```bash
sudo apt-get install gparted
```

- RAR Support

```bash
sudo apt-get install rar unrar
```

- Banshee daily PPA and lyrics extension

```bash
sudo apt-add-repository ppa:banshee-team/banshee-daily
sudo apt-get install banshee-extension-lyrics 
```

## Useful Links

- [Ubuntu](http://www.ubuntu.com/)
- [Ubuntu Forums](http://ubuntuforums.org/)
- [OMG! Ubuntu! "10 Things To Do After Installing Ubuntu 11.10"](http://www.omgubuntu.co.uk/2011/10/10-things-to-do-after-installing-ubuntu-11-10/)