---
title: "Forever Peace (The Forever War Book 2) by Joe Haldeman (1997)"
slug: "forever-peace-the-forever-war-book-2"
author: "Paulo Pereira"
date: 2021-09-07T21:00:00+01:00
lastmod: 2021-09-07T21:00:00+01:00
cover: "/posts/books/forever-peace-the-forever-war-book-2.jpg"
description: "Finished “Forever Peace (The Forever War Book 2)” by Joe Haldeman."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Joe Haldeman
book series:
  - The Forever War Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Joe Haldeman
  - The Forever War Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
---

Finished “Forever Peace”.
* Author: [Joe Haldeman](/book-authors/joe-haldeman)
* First Published: [October 1997](/book-publication-year/1990s)
* Series: [The Forever War](/book-series/the-forever-war-series) Book 2
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31346068-forever-peace)
>
> T2043 A.D.: The Ngumi War rages. A burned-out soldier and his scientist lover discover a secret that could put the universe back to square one. And it is not terrifying. It is tempting...