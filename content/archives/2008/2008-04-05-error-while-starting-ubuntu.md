---
title: "Error while starting Ubuntu"
slug: "error-while-starting-ubuntu"
author: "Paulo Pereira"
date: 2008-04-05T16:45:00+01:00
lastmod: 2008-04-05T16:45:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

Yesterday, when I was starting up my Ubuntu machine, the screen went white and I had to manually turn off the laptop.

When I started it again the file system checkup ([fsck](http://en.wikipedia.org/wiki/Fsck)) started. I got a lot of yes or no questions about fixing several problems. Near the end fsck aborted and I was left in the terminal to try and fix the problem.

I rebooted the system, but there was a X server error and it went to the terminal again.

Ok, at this moment I was scared. A lot of data I didn't wanna lose.

I logged to my desktop machine and went to Ubuntu Forums.

Here's what I did, and yes, I'm posting from Ubuntu:

-  I started by unmounting all my file systems:

```bash
sudo umount /dev/sda1
sudo umount /dev/sda5
sudo umount /dev/sda6
sudo umount /dev/sda7
```

- Then I run a filesystem check, like this:

```bash
sudo e2fsck -C0 -p -f -v /dev/hda1
sudo e2fsck -C0 -p -f -v /dev/hda5
sudo e2fsck -C0 -p -f -v /dev/hda6
sudo e2fsck -C0 -p -f -v /dev/hda7
```

And that was it :-)

I rebooted and all was ok.