---
title: "Power Moves by Adam M. Grant (2019)"
slug: "power-moves"
author: "Paulo Pereira"
date: 2019-08-09T22:22:00+01:00
lastmod: 2019-08-09T22:22:00+01:00
cover: "/posts/books/power-moves.jpg"
description: "Finished “Power Moves” by Adam M. Grant."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Adam M. Grant
book series:
  - 
book genres:
  - Nonfiction
  - Business
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Adam M. Grant
  - Nonfiction
  - Business
  - 5/10 Books
  - Audiobook
---

Finished “Power Moves”.
* Author: [Adam M. Grant](/book-authors/adam-m.-grant)
* First Published: [January 3rd 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/53104184-power-moves)
>
> Navigating the new landscape of power with Mary Barra (GM), Stewart Butterfield (Slack), Satya Nadella (Microsoft), Sheryl Sandberg (Facebook), Eric Schmidt (Google/Alphabet), David Solomon (Goldman Sachs), Ellen Stofan (NASA), and two dozen other leaders, thinkers, and luminaries.
>
> Power is changing. Private corner offices and management by decree are out, as is unquestioned trust in the government and media. These former pillars of traditional power have been replaced by networks of informed citizens who collectively wield more power over their personal lives, employers, and worlds than ever before.
>
> So how do you navigate this new landscape and come out on top?