---
title: "Stay Out of the Basement (Goosebumps Book 2) by R.L. Stine (1992)"
slug: "stay-out-of-the-basement-goosebumps-book-2"
author: "Paulo Pereira"
date: 2023-10-10T21:00:00+01:00
lastmod: 2023-10-10T21:00:00+01:00
cover: "/posts/books/stay-out-of-the-basement-goosebumps-book-2.jpg"
description: "Finished “Stay Out of the Basement (Goosebumps Book 2)” by R.L. Stine."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - R.L. Stine
book series:
  - Goosebumps Series
book genres:
  - Fiction
  - Horror
book scores:
  - 6/10 Books
tags:
  - Book Log
  - R.L. Stine
  - Goosebumps Series
  - Fiction
  - Horror
  - 6/10 Books
  - Ebook
---

Finished “Stay Out of the Basement”.
* Author: [R.L. Stine](/book-authors/r.l.-stine)
* First Published: [July 1, 1992](/book-publication-year/1990s)
* Series: [Goosebumps](/book-series/goosebumps-series) Book 2
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/12986878-stay-out-of-the-basement)
>
> Dr. Brewer is doing a little plant-testing in his basement. Nothing to worry about. Harmless, really. But Margaret and Casey Brewer are worried about their father. Especially when they...meet...some of the plants he is growing down there. Then they notice that their father is developing plantlike tendencies. In fact, he is becoming distinctly weedy-and seedy. Is it just part of their father's "harmless" experiment? Or has the basement turned into another little shop of horrors?