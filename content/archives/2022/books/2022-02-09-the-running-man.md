---
title: "The Running Man by Stephen King writing as Richard Bachman (1982)"
slug: "the-running-man"
author: "Paulo Pereira"
date: 2022-02-09T22:00:00+00:00
lastmod: 2022-02-09T22:00:00+00:00
cover: "/posts/books/the-running-man.jpg"
description: "Finished “The Running Man” by Stephen King writing as Richard Bachman."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
  - Richard Bachman
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Richard Bachman
  - Stephen King Novels
  - Fiction
  - Horror
  - 7/10 Books
  - Audiobook
---

Finished “The Running Man”.
* Author: [Stephen King](/book-authors/stephen-king) writing as [Richard Bachman](/book-authors/richard-bachman)
* First Published: [May 1st 1982](/book-publication-year/1980s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44094920-the-running-man)
>
> The Running Man is set within a dystopian future in which the poor are seen more by the government as worrisome rodents than actual human beings. The protagonist of The Running Man, Ben Richards, is quick to realize this as he watches his daughter, Cathy, grow more sick by the day and tread closer and closer to death. Desperate for money to pay Cathy’s medical bills, Ben enlists himself in a true reality style game show where the objective is to merely stay alive.