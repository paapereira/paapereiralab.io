---
title: "Dark Matter by Blake Crouch (2016)"
slug: "dark-matter"
author: "Paulo Pereira"
date: 2017-04-13T22:20:00+01:00
lastmod: 2017-04-13T22:20:00+01:00
cover: "/posts/books/dark-matter.jpg"
description: "Finished “Dark Matter” by Blake Crouch."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2016
book authors:
  - Blake Crouch
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Blake Crouch
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Audiobook
aliases:
  - /posts/books/dark-matter
---

Finished “Dark Matter”.
* Author: [Blake Crouch](/book-authors/blake-crouch)
* First Published: [July 26th 2016](/book-publication-year/2016)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31191008-dark-matter)
>
> “Are you happy with your life?” Those are the last words Jason Dessen hears before the masked abductor knocks him unconscious. Before he awakens to find himself strapped to a gurney, surrounded by strangers in hazmat suits. Before a man Jason’s never met smiles down at him and says, “Welcome back, my friend.”
>
> In this world he’s woken up to, Jason’s life is not the one he knows. His wife is not his wife. His son was never born. And Jason is not an ordinary college physics professor but a celebrated genius who has achieved something remarkable--something impossible.
>
> Is it this world or the other that’s the dream? And even if the home he remembers is real, how can Jason possibly make it back to the family he loves? The answers lie in a journey more wondrous and horrifying than anything he could’ve imagined—one that will force him to confront the darkest parts of himself even as he battles a terrifying, seemingly unbeatable foe.