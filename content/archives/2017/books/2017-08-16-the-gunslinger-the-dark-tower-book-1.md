---
title: "The Gunslinger (The Dark Tower Book 1) by Stephen King (1982)"
slug: "the-gunslinger-the-dark-tower-book-1"
author: "Paulo Pereira"
date: 2017-08-16T22:38:00+01:00
lastmod: 2017-08-16T22:38:00+01:00
cover: "/posts/books/the-gunslinger-the-dark-tower-book-1.jpg"
description: "Finished “The Gunslinger (The Dark Tower Book 1)” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
book series:
  - The Dark Tower Series
  - Stephen King Novels
book genres:
  - Fiction
  - Fantasy
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Stephen King
  - The Dark Tower Series
  - Stephen King Novels
  - Fiction
  - Fantasy
  - 8/10 Books
  - Audiobook
---

Finished “The Gunslinger”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [June 1st 1982](/book-publication-year/1980s)
* Series: [The Dark Tower](/book-series/the-dark-tower-series) Book 1), [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35436985-the-gunslinger)
>
> Beginning with a short story appearing in The Magazine of Fantasy and Science Fiction in 1978, the publication of Stephen King's epic work of fantasy -- what he considers to be a single long novel and his magnum opus -- has spanned a quarter of a century.
>
> Set in a world of extraordinary circumstances, filled with stunning visual imagery and unforgettable characters, The Dark Tower series is King's most visionary feat of storytelling, a magical mix of science fiction, fantasy, and horror that may well be his crowning achievement.
>
> Book I
> In The Gunslinger (originally published in 1982), King introduces his most enigmatic hero, Roland Deschain of Gilead, the Last Gunslinger. He is a haunting, solitary figure at first, on a mysterious quest through a desolate world that eerily mirrors our own. Pursuing the man in black, an evil being who can bring the dead back to life, Roland is a good man who seems to leave nothing but death in his wake.