---
title: "Book Haul for November 2020"
slug: "book-haul-2020-11"
author: "Paulo Pereira"
date: 2020-12-06T14:18:10+00:00
lastmod: 2020-12-06T14:18:10+00:00
cover: "/posts/book-hauls/2020-11-book-haul.png"
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

Here’s my book haul for November.

Those damned Amazon deals...
