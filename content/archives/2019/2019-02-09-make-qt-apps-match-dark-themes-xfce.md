---
title: "How to Make Qt Apps Match Dark Themes on Xfce"
slug: "make-qt-apps-match-dark-themes-xfce"
author: "Paulo Pereira"
date: 2019-02-09T18:40:14+00:00
lastmod: 2019-02-09T18:40:14+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Xfce
---

In summary, I prefer dark themes and my QT based applications weren’t matching this preference. I’m using Xfce.

After some digging around, and this [video](https://www.youtube.com/watch?v=rP4DWu24ff0), here’s what I did.

```sh
$ nano ~/.config/Trolltech.conf
```

> [Qt]
> style=GTK+

```sh
$ sudo pacman -S qt5-styleplugins
```
```sh
$ sudo nano /etc/environment
```
> QT_QPA_PLATFORMTHEME=gtk2
> QT_STYLE_OVERRIDE=gtk2