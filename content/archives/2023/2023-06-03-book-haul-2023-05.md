---
title: "Book Haul for May 2023"
slug: "book-haul-2023-05"
author: "Paulo Pereira"
date: 2023-06-03T17:00:00+01:00
lastmod: 2023-06-03T17:00:00+01:00
cover: "/posts/book-hauls/2023-05-book-haul.png"
description: "May book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

May book haul.