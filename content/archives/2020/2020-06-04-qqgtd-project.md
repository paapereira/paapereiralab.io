---
title: "Started a new qqgtd project in GitLab"
slug: "qqgtd-project"
author: "Paulo Pereira"
date: 2020-06-04T19:03:58+01:00
lastmod: 2020-06-04T19:03:58+01:00
description: "I recently started a [new project](https://gitlab.com/paapereira/qqgtd) at GitLab to host my new text based Getting Things Done approach."
draft: false
toc: false
categories:
  - General
tags:
  - GTD
  - qqgtd
---

I recently started a [new project](https://gitlab.com/paapereira/qqgtd) at GitLab to host my new text based Getting Things Done approach.

It's still at early stages, but I'm using it daily along my current system using [Nozbe](https://nozbe.com/).

I've been a GTD guy since like forever and have used many different applications through the years. The most important thing about GTD is the method and not the tools, so changing my tool set is usually not a problem.

This change was caused by the need of being online to access my system. At my work the Internet access is very controlled, so every time some problem occurred my productivity dropped.

Some requirements I typically use when building and choosing my tools to be used with my GTD system:

- Can I export my tasks to a readable format?
- Can I use it in every platform and operating system?

This list could/is larger, but it's my minimal list.

Right now I'm satisfied with this 0.1 version, but I've not yet been in a "mobile situation" given that I've been working for home.

You can read more about the project in the [README](https://gitlab.com/paapereira/qqgtd/-/blob/master/README.md) file and how to [contribute](https://gitlab.com/paapereira/qqgtd/-/blob/master/CONTRIBUTING.md).

I'll probably write up a more complete post in the future.
