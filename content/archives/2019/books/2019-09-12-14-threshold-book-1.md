---
title: "14 (Threshold Book 1) by Peter Clines (2012)"
slug: "14-threshold-book-1"
author: "Paulo Pereira"
date: 2019-09-12T22:46:00+01:00
lastmod: 2019-09-12T22:46:00+01:00
cover: "/posts/books/14-threshold-book-1.png"
description: "Finished “14 (Threshold Book 1)” by Peter Clines."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - Peter Clines
book series:
  - Threshold Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Peter Clines
  - Threshold Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Audiobook
---

Finished “14”.
* Author: [Peter Clines](/book-authors/peter-clines)
* First Published: [2012](/book-publication-year/2012)
* Series: [Threshold](/book-series/threshold-series) Book 1
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/15737884-14)
>
> Padlocked doors. Strange light fixtures. Mutant cockroaches.
> There are some odd things about Nate’s new apartment.
>
> Of course, he has other things on his mind. He hates his job. He has no money in the bank. No girlfriend. No plans for the future. So while his new home isn’t perfect, it’s livable. The rent is low, the property managers are friendly, and the odd little mysteries don’t nag at him too much.
>
> At least, not until he meets Mandy, his neighbour across the hall, and notices something unusual about her apartment. And Xela’s apartment. And Tim’s. And Veek’s. Because every room in this old Los Angeles brownstone has a mystery or two. Mysteries that stretch back over a hundred years. Some of them are in plain sight. Some are behind locked doors. And all together these mysteries could mean the end of Nate and his friends.
>
> Or the end of everything...