---
title: "My Linux history"
slug: "linux"
draft: false
toc: false
aliases:
  - /pages/linux
---

I’m currently running [Arch Linux](https://www.archlinux.org/) in all my personal computers. A NUC6i5SYH as my desktop, a [NUC7i3BNH](/posts/2020/05/new-nuc7i3bnh/) as my server and a [Surface Go](/posts/install-arch-linux-surface-go) as my laptop.

It all started in the 90s playing around with [Red Hat](https://en.wikipedia.org/wiki/Red_Hat_Linux) 5.2 (if my memory is correct). At that time it was just curiosity and Windows was still my main operating system. Even then I already tended more to the likes of [Netscape](https://en.wikipedia.org/wiki/Netscape), [StarOffice](https://en.wikipedia.org/wiki/StarOffice) and [Firefox](https://en.wikipedia.org/wiki/Firefox).

When I went to university my [Red Hat](https://en.wikipedia.org/wiki/Red_Hat) musing started to be more serious and I started dual booting with Windows and trying several distributions. Between some others I used [Fedora Core](https://en.wikipedia.org/wiki/Fedora_%28operating_system%29), [Corel Linux](https://en.wikipedia.org/wiki/Corel_Linux), Mandrake, a try at [Gentoo](https://en.wikipedia.org/wiki/Gentoo), [SUSE](https://en.wikipedia.org/wiki/SUSE), [CrunchBang](https://en.wikipedia.org/wiki/CrunchBang_Linux) and later [Ubuntu](https://en.wikipedia.org/wiki/Ubuntu).

In 2009 I finally switched fully to Linux and at that time with Ubuntu. Following the [LAS Arch Challenge](https://www.jupiterbroadcasting.com/38086/the-arch-way-aas-s27e03/) in 2013, I switched to [Arch Linux](https://www.archlinux.org/) until today.

All around I used [GNOME](https://en.wikipedia.org/wiki/GNOME) and for some time [Openbox](https://en.wikipedia.org/wiki/Openbox) until [Canonical](https://en.wikipedia.org/wiki/Canonical_(company)) started shipping [Unity](https://en.wikipedia.org/wiki/Unity_(user_interface)). After that I started hopping between [GNOME Shell](https://en.wikipedia.org/wiki/GNOME_Shell), [KDE Plasma](https://en.wikipedia.org/wiki/KDE), [Xfce](https://en.wikipedia.org/wiki/Xfce) and lately [i3-gaps](https://en.wikipedia.org/wiki/I3_(window_manager)).

[Firefox](https://en.wikipedia.org/wiki/Firefox) has always been my browser of choice after [Netscape](https://en.wikipedia.org/wiki/Netscape). I used [Chrome](https://en.wikipedia.org/wiki/Google_Chrome) and [Chromium](https://en.wikipedia.org/wiki/Chromium) for a period, but I’ve been back to Firefox for some time.

You can follow my Linux related posts [here](/categories/linux/) in my blog and check some of my [favorite Linux applications and related things](/pages/favorites).
