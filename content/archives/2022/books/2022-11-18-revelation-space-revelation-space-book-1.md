---
title: "Revelation Space (Revelation Space Book 1) by Alastair Reynolds (2000)"
slug: "revelation-space-revelation-space-book-1"
author: "Paulo Pereira"
date: 2022-11-18T22:00:00+00:00
lastmod: 2022-11-18T22:00:00+00:00
cover: "/posts/books/revelation-space-revelation-space-book-1.jpg"
description: "Finished “Revelation Space (Revelation Space Book 1)” by Alastair Reynolds."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2000
book authors:
  - Alastair Reynolds
book series:
  - Revelation Space Series
  - Revelation Space Universe
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Alastair Reynolds
  - Revelation Space Series
  - Revelation Space Universe
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/revelation-space-revelation-space-book-1
---

Finished “Revelation Space”.
* Author: [Alastair Reynolds](/book-authors/alastair-reynolds)
* First Published: [March 1, 2000](/book-publication-year/2000)
* Series: [Revelation Space](/book-series/revelation-space-series) Book 1
* [Revelation Space Universe](/book-series/revelation-space-universe)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/49015373-revelation-space)
>
> Nine hundred thousand years ago, something annihilated the Amarantin civilization just as it was on the verge of discovering space flight. Now one scientist, Dan Sylveste, will stop at nothing to solve the Amarantin riddle before ancient history repeats itself. With no other resources at his disposal, Sylveste forges a dangerous alliance with the cyborg crew of the starship Nostalgia for Infinity. But as he closes in on the secret, a killer closes in on him. Because the Amarantin were destroyed for a reason, and if that reason is uncovered, the universe and reality itself could be irrevocably altered...