---
title: "The Expert System's Brother (Expert System Book 1) by Adrian Tchaikovsky (2018)"
slug: "the-expert-systems-brother-expert-system-book-1"
author: "Paulo Pereira"
date: 2024-08-31T22:00:00+01:00
lastmod: 2024-08-31T22:00:00+01:00
cover: "/posts/books/the-expert-systems-brother-expert-system-book-1.jpg"
description: "Finished “The Expert System's Brother (Expert System Book 1)” by Adrian Tchaikovsky."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Adrian Tchaikovsky
book series:
  - Expert System Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Adrian Tchaikovsky
  - Expert System Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “The Expert System's Brother”.
* Author: [Adrian Tchaikovsky](/book-authors/adrian-tchaikovsky)
* First Published: [July 17, 2018](/book-publication-year/2018)
* Series: [Expert System](/book-series/expert-system-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/38321266-the-expert-system-s-brother)
>
> After an unfortunate accident, Handry is forced to wander a world he doesn’t understand, searching for meaning. He soon discovers that the life he thought he knew is far stranger than he could even possibly imagine.
> 
> Can an unlikely saviour provide the answers to the questions he barely comprehends?