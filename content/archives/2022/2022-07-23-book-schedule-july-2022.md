---
title: "Books Schedule update"
slug: "book-schedule-july-2022"
author: "Paulo Pereira"
date: 2022-07-23T16:38:48+01:00
lastmod: 2022-07-23T16:38:48+01:00
description: "July Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my July update for my 'books to read' schedule for the next months.

Malazan has been postponed, I did a little shuffling and added new books in.

* **[Read]** Heretics of Dune (Dune 5) by Frank Herbert
* **[Read]** The Lost World by Michael Crichton
* **[Reading]** The Handmaid's Tale by Margaret Atwood
* Storm Front (The Dresden Files 1) by Jim Butcher
* Chapterhouse: Dune (Dune 6) by Frank Herbert
* The Man in the High Castle by Philip K. Dick
* NOS4A2 by Joe Hill
* Airframe by Michael Crichton
* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Testaments (The Handmaid's Tale 2) by Margaret Atwood
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* Revelation Space (Revelation Space 1) by Alastair Reynolds
* Boy's Life by Robert R. McCammon

(...)

* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson
* Redemption Ark (Revelation Space 2) by Alastair Reynolds

I also have always an audiobook going too:

* **[Read]** The Dark Half by Stephen King
* **[Reading]** Flagship Victory (Galactic Liberation 3) by David Vandyke & B. V. Larson
* Upgrade by Blake Crouch
* Rich Dad Poor Dad by Robert T. Kiyosaki
* Four Past Midnight by Stephen King