---
title: "Red Company: First Strike! (Red Company Book 1) by B.V. Larson (2023)"
slug: "red-company-first-strike-red-company-book-1"
author: "Paulo Pereira"
date: 2023-11-19T13:30:00+00:00
lastmod: 2023-11-19T13:30:00+00:00
cover: "/posts/books/red-company-first-strike-red-company-book-1.jpg"
description: "Finished “Red Company: First Strike! (Red Company Book 1)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - B.V. Larson
book series:
  - Red Company Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Red Company Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Red Company: First Strike!”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [May 20, 2023](/book-publication-year/2023)
* Series: [Red Company](/book-series/red-company-series) Book 1
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/157720386-red-company)
>
> For centuries, mankind has searched for nonhuman civilizations in the cosmos. These lost alien societies have finally been discovered—but they were destroyed long ago.
> 
> Devin Starn starts off as a lowly rock-rat aboard Borag , a mining ship working the dangerous end of the asteroid belt. Slowly, he gains prestige and purpose. When an alien base is uncovered, however, he must risk everything to protect his ship and her crew.
> 
> What wiped out all the civilizations that came before humanity?