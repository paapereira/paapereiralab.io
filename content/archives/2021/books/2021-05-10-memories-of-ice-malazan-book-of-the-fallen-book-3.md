---
title: "Memories of Ice (Malazan Book of the Fallen Book 3) by Steven Erikson (2001)"
slug: "memories-of-ice-malazan-book-of-the-fallen-book-3"
author: "Paulo Pereira"
date: 2021-05-10T21:00:00+01:00
lastmod: 2021-05-10T21:00:00+01:00
cover: "/posts/books/memories-of-ice-malazan-book-of-the-fallen-book-3.jpg"
description: "Finished “Memories of Ice (Malazan Book of the Fallen Book 3)” by Steven Erikson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2001
book authors:
  - Steven Erikson
book series:
  - Malazan Book of the Fallen Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Steven Erikson
  - Malazan Book of the Fallen Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
---

Finished “Memories of Ice”.
* Author: [Steven Erikson](/book-authors/steven-erikson)
* First Published: [December 6th 2001](/book-publication-year/2001)
* Series: [Malazan Book of the Fallen](/book-series/malazan-book-of-the-fallen-series) Book 3
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/10377639-memories-of-ice)
>
> The ravaged continent of Genabackis is a terrifying new empire, the Pannion Domin, that devours all. An uneasy allliance resists: Onearm's army, Whiskeyjack's Bridgeburners and former enemies - forces of Warlord Caladan Brood, Anomander Rake and his Tiste Andii mages, and the Rhivi people of the plains. And the Crippled God intends revenge.