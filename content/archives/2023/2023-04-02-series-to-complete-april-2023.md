---
title: "Books Series to complete buying"
slug: "series-to-complete-april-2023"
author: "Paulo Pereira"
date: 2023-04-02T14:20:00+01:00
lastmod: 2023-04-02T14:20:00+01:00
description: "Short list of series I'm yet to complete buying."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Wishlist
---

April update for my short list of series to complete buying.

* Terra Ignota by Ada Palmer
* Red Rising (new books comming) by Pierce Brown
* The Hitchhiker's Guide to the Galaxy by Douglas Adams
* The Final Architecture (new book comming) by Adrian Tchaikovsky
* The Stormlight Archive by Brandon Sanderson
* Mistborn by Brandon Sanderson
* Culture by Iain M. Banks
* The Commonwealth Saga by Peter F. Hamilton
* The Night's Dawn by Peter F. Hamilton
* The Last Kingdom by Bernard Cornwell
* The Legend of Drizzt by R. A. Salvatore
* Sookie Stackhouse by Charlaine Harris