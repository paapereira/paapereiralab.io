---
title: "Starfire by B.V. Larson (2014)"
slug: "starfire"
author: "Paulo Pereira"
date: 2017-02-04T23:03:00+00:00
lastmod: 2017-02-04T23:03:00+00:00
cover: "/posts/books/starfire.jpg"
description: "Finished “Starfire” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - B.V. Larson
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Starfire”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [November 6th 2014](/book-publication-year/2014)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/23514936-starfire)
>
> Two ships launch, but only one can come home with the prize—and no one suspects what they’ll find when they get out there.
>
> On June 30, 1908 an object fell from the sky releasing more energy than a thousand Hiroshima bombs. A Siberian forest was flattened, but the strike left no significant crater. The anomaly came to be known as the Tunguska Event, and scientists have never agreed whether it was the largest meteor strike in recorded history—or something else.
>
> Alien Artifacts have been uncovered since the 1908 event, and a new star drive is discovered. When another, larger Artifact is detected orbiting Jupiter, both NASA and the Russian Federal Space Agency are determined to beat all rivals to the next treasure trove of alien tech.