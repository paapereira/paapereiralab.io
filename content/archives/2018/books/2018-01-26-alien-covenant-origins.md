---
title: "Alien: Covenant Origins by Alan Dean Foster (2017)"
slug: "alien-covenant-origins"
author: "Paulo Pereira"
date: 2018-01-26T22:27:00+01:00
lastmod: 2018-01-26T22:27:00+01:00
cover: "/posts/books/alien-covenant-origins.jpg"
description: "Finished “Alien: Covenant Origins” by Alan Dean Foster."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Alan Dean Foster
book series:
  - Alien Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Alan Dean Foster
  - Alien Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Alien: Covenant Origins”.
* Author: [Alan Dean Foster](/book-authors/alan-dean-foster)
* First Published: [September 26th 2017](/book-publication-year/2017)
* Series: [Alien Series](/book-series/alien-series)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/48495169-alien)
>
> The Covenant mission is the most ambitious endeavor in the history of Weyland-Yutani. A ship bound for Origae-6, carrying two thousand colonists beyond the limits of known space, this is make-or-break investment for the corporation-and for the future of all mankind.Yet there are those who would die to stop the mission. As the colony ship hovers in Earth orbit, several violent events reveal a deadly conspiracy to sabotage the launch. While Captain Jacob Branson and his wife, Daniels, complete their preparations, security chief Daniel Lope recruits the final key member of his team. Together they seek to stop the perpetrators before the ship and its passengers can be destroyed.
>
> An original novel by the acclaimed Alan Dean Foster, author of the groundbreaking Alien novelization, Alien: Covenant Origins is the official chronicle of the events that led up to Alien: Covenant. It also reveals the world the colonists left behind.