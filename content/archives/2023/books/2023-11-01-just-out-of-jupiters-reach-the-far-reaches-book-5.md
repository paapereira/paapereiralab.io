---
title: "Just Out of Jupiter's Reach (The Far Reaches Book 5) by Nnedi Okorafor (2023)"
slug: "just-out-of-jupiters-reach-the-far-reaches-book-5"
author: "Paulo Pereira"
date: 2023-11-01T21:00:00+00:00
lastmod: 2023-11-01T21:00:00+00:00
cover: "/posts/books/just-out-of-jupiters-reach-the-far-reaches-book-5.jpg"
description: "Finished “Just Out of Jupiter's Reach (The Far Reaches Book 5)” by Nnedi Okorafor."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - Nnedi Okorafor
book series:
  - The Far Reaches Series
book genres:
  - Fiction
  - Science Fiction
  - Short Stories
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Nnedi Okorafor
  - The Far Reaches Series
  - Fiction
  - Science Fiction
  - Short Stories
  - 7/10 Books
  - Ebook
---

Finished “Just Out of Jupiter's Reach”.
* Author: [Nnedi Okorafor](/book-authors/nnedi-okorafor)
* First Published: [June 27, 2023](/book-publication-year/2023)
* Series: [The Far Reaches](/book-series/the-far-reaches-series) Book 5
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/151908136-just-out-of-jupiter-s-reach)
>
> A revolutionary experiment in space opens a woman’s eyes to the meaning of solitude in a thought-provoking short story by New York Times bestselling, award-winning author Nnedi Okorafor. Tornado Onwubiko is one of seven people on Earth paired with sentient ships to explore and research the cosmos for twenty million euros. A decade of solitary life for a lifetime of wealth. Five years into the ten-year mission of total isolation comes a a temporary meetup among fellow travelers. A lot can happen in a week. For Tornado, who left a normal life behind, a little company can be life-changing.