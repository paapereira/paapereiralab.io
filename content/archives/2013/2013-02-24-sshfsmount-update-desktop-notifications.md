---
title: "sshfsmount update: Desktop notifications"
slug: "sshfsmount-update-desktop-notifications"
author: "Paulo Pereira"
date: 2013-02-24T10:00:00+00:00
lastmod: 2013-02-24T10:00:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - B3 Server
  - GitHub
  - sshfsmount
---

I've updated [sshfsmount](https://github.com/paapereira/sshfsmount) to include desktop notifications.

![github](/posts/2013/2013-02-24-sshfsmount-update-desktop-notifications/github.png)

You can install it the same way already described in the previous [post](/posts/mount-remote-directories-in-your-local/).

![notify-send](/posts/2013/2013-02-24-sshfsmount-update-desktop-notifications/notify.png)