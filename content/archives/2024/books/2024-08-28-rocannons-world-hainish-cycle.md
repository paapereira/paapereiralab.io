---
title: "Rocannon's World (Hainish Cycle) by Ursula K. Le Guin (1966)"
slug: "rocannons-world-hainish-cycle"
author: "Paulo Pereira"
date: 2024-08-28T20:00:00+01:00
lastmod: 2024-08-28T20:00:00+01:00
cover: "/posts/books/rocannons-world-hainish-cycle.jpg"
description: "Finished “Rocannon's World (Hainish Cycle)” by Ursula K. Le Guin."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Ursula K. Le Guin
book series:
  - Hainish Cycle Series
book genres:
  - Fiction
  - Science Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Ursula K. Le Guin
  - Hainish Cycle Series
  - Fiction
  - Science Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
---

Finished “Rocannon's World”.
* Author: [Ursula K. Le Guin](/book-authors/ursula-k.-le-guin)
* First Published: [January 1, 1966](/book-publication-year/1960s)
* Series: [Hainish Cycle](/book-series/hainish-cycle-series)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/92610.Rocannon_s_World)
>
> A world shared by three native humanoid races - the cavern-dwelling Gdemiar, elvish Fiia, and warrior clan, Liuar - is suddenly invaded and conquered by a fleet of ships from the stars. Earth scientist Rocannon is on that world, and he sees his friends murdered and his spaceship destroyed. Marooned among alien peoples, he leads the battle to free this new world - and finds that legends grow around him even as he fights.