---
title: "The First Fifteen Lives of Harry August by Claire North (2014)"
slug: "the-first-fifteen-lives-of-harry-august"
author: "Paulo Pereira"
date: 2022-11-29T21:00:00+00:00
lastmod: 2022-11-29T21:00:00+00:00
cover: "/posts/books/the-first-fifteen-lives-of-harry-august.jpg"
description: "Finished “The First Fifteen Lives of Harry August” by Claire North."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - Claire North
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Claire North
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
aliases:
  - /posts/books/the-first-fifteen-lives-of-harry-august
---

Finished “The First Fifteen Lives of Harry August”.
* Author: [Claire North](/book-authors/claire-north)
* First Published: [April 8, 2014](/book-publication-year/2014)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35066358-the-first-fifteen-lives-of-harry-august)
>
> Some stories cannot be told in just one lifetime. Harry August is on his deathbed. Again. No matter what he does or the decisions he makes, when death comes, Harry always returns to where he began, a child with all the knowledge of a life he has already lived a dozen times before. Nothing ever changes. Until now. As Harry nears the end of his eleventh life, a little girl appears at his bedside. "I nearly missed you, Doctor August," she says. "I need to send a message." This is the story of what Harry does next, and what he did before, and how he tries to save a past he cannot change and a future he cannot allow.