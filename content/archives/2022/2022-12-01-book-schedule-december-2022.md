---
title: "Books Schedule update"
slug: "book-schedule-december-2022"
author: "Paulo Pereira"
date: 2022-12-01T12:20:00+00:00
lastmod: 2022-12-01T12:20:00+00:00
description: "December Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my December TBR (To Be Read) update for the next months.

* **[Read]** Revelation Space (Revelation Space 1) by Alastair Reynolds
* **[Read]** The First Fifteen Lives of Harry August by Claire North
* **[Reading]** Dogs of War by Adrian Tchaikovsky
* Timeline by Michael Crichton
* Summer Knight (The Dresden Files 4) by Jim Butcher
* Redemption Ark (Revelation Space 2) by Alastair Reynolds
* The Testaments (The Handmaid's Tale 2) by Margaret Atwood
* Boy's Life by Robert R. McCammon
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson

I also have always an audiobook going too:

* **[Read]** Needful Things by Stephen King
* Straker's Breakers (Galactic Liberation 5) by David Vandyke & B. V. Larson
* The Power of Now by Eckhart Tolle
* Skeleton Crew by Stephen King
* The Signal and the Noise by Nate Silver
* Hell's Reach (Galactic Liberation 6) by David Vandyke & B. V. Larson