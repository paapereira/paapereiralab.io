---
title: "Thicker Than Water by Tyler Shultz (2020)"
slug: "thicker-than-water"
author: "Paulo Pereira"
date: 2021-09-25T21:00:00+01:00
lastmod: 2021-09-25T21:00:00+01:00
cover: "/posts/books/thicker-than-water.jpg"
description: "Finished “Thicker Than Water” by Tyler Shultz."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Tyler Shultz
book series:
  - 
book genres:
  - Nonfiction
  - Biography
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Tyler Shultz
  - Nonfiction
  - Biography
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “Thicker Than Water”.
* Author: [Tyler Shultz](/book-authors/tyler-shultz)
* First Published: [August 4th 2020](/book-publication-year/2020)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/54815214-thicker-than-water)
>
> From the hero whistleblower of the infamous Theranos scam, Thicker than Water is a look at never-before-revealed details behind closed doors at the company, revealing a cautionary tale of corporate bullying, gaslighting, ego, and wealth run amok in Silicon Valley.
> 
> Tyler Shultz had been in the workforce for less than a year when he emailed Elizabeth Holmes, his employer and the CEO and founder of Theranos, with concerns that the company’s lab practices were faulty, ignored quality control, and were potentially dangerous to patients. The COO fired back with a dismissive and insulting email, to which Tyler replied: "Consider this my two weeks’ notice."
> 
> From there, his life spun out of control at the hand of Elizabeth, her team of high-powered lawyers, and the patriarch of Tyler’s own family, George Shultz—one of America’s most prominent statesmen, who sat among the top of the Theranos Board of Directors. And yet, Tyler forged on. To protect his own conscience, the honor and reputation of his grandfather, and the health of patients worldwide.
> 
> Thicker than Water is Tyler’s as-told-to story—a harrowing and heartbreaking roller coaster of biomedical drama, family intrigue, and redemption—that will ultimately make you feel as though you are at a dinner party, seated next to a brilliant friend with one hell of a story.