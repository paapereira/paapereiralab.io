---
title: "Fugitive Telemetry (The Murderbot Diaries Book 6) by Martha Wells (2021)"
slug: "fugitive-telemetry-the-murderbot-diaries-book-6"
author: "Paulo Pereira"
date: 2021-08-29T21:00:00+01:00
lastmod: 2021-08-29T21:00:00+01:00
cover: "/posts/books/fugitive-telemetry-the-murderbot-diaries-book-6.jpg"
description: "Finished “Fugitive Telemetry (The Murderbot Diaries Book 6)” by Martha Wells."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2021
book authors:
  - Martha Wells
book series:
  - The Murderbot Diaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Martha Wells
  - The Murderbot Diaries Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Fugitive Telemetry”.
* Author: [Martha Wells](/book-authors/martha-wells)
* First Published: [April 27th 2021](/book-publication-year/2021)
* Series: [The Murderbot Diaries](/book-series/the-murderbot-diaries-series) Book 6
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/58176203-fugitive-telemetry)
>
> No, I didn’t kill the dead human. If I had, I wouldn’t dump the body in the station mall.
> 
> When Murderbot discovers a dead body on Preservation Station, it knows it is going to have to assist station security to determine who the body is (was), how they were killed (that should be relatively straightforward, at least), and why (because apparently that matters to a lot of people—who knew?)
> 
> Yes, the unthinkable is about to happen: Murderbot must voluntarily speak to humans!
> 
> Again!