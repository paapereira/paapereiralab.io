---
title: "The Godfather (The Godfather Book 1) by Mario Puzo (1969)"
slug: "the-godfather-the-godfather-book-1"
author: "Paulo Pereira"
date: 2024-08-24T12:00:00+01:00
lastmod: 2024-08-24T12:00:00+01:00
cover: "/posts/books/the-godfather-the-godfather-book-1.jpg"
description: "Finished “The Godfather (The Godfather Book 1)” by Mario Puzo."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Mario Puzo
book series:
  - The Godfather Series
book genres:
  - Fiction
  - Classics
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Mario Puzo
  - The Godfather Series
  - Fiction
  - Classics
  - 10/10 Books
  - Ebook
aliases:
  - /posts/books/the-godfather-the-godfather-book-1
---

Finished “The Godfather”.
* Author: [Mario Puzo](/book-authors/mario-puzo)
* First Published: [March 10, 1969](/book-publication-year/1960s)
* Series: [The Godfather](/book-series/the-godfather-series) Book 1
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/51876297-the-godfather)
>
> With its brilliant and brutal portrayal of the Corleone family, The Godfather burned its way into our national consciousness. This unforgettable saga of crime and corruption, passion and loyalty continues to stand the test of time, as the definitive novel of the Mafia underworld.
> 
> A #1 New York Times bestseller in 1969, Mario Puzo’s epic was turned into the incomparable film of the same name, directed by Francis Ford Coppola, which won the Academy Award for Best Picture. It is the original classic that has been often imitated, but never matched. A tale of family and society, law and order, obedience and rebellion, it reveals the dark passions of human nature played out against a backdrop of the American dream.