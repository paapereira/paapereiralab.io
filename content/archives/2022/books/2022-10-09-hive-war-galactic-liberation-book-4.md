---
title: "Hive War (Galactic Liberation Book 4) by David VanDyke and B.V. Larson (2018)"
slug: "hive-war-galactic-liberation-book-4"
author: "Paulo Pereira"
date: 2022-10-09T12:00:00+01:00
lastmod: 2022-10-09T12:00:00+01:00
cover: "/posts/books/hive-war-galactic-liberation-book-4.jpg"
description: "Finished “Hive War (Galactic Liberation Book 4)” by David VanDyke and B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - David VanDyke
  - B.V. Larson
book series:
  - Galactic Liberation Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - David VanDyke
  - B.V. Larson
  - Galactic Liberation Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Hive War”.
* Author: [David VanDyke](/book-authors/david-vandyke) and [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [August 26, 2018](/book-publication-year/2018)
* Series: [Galactic Liberation](/book-series/galactic-liberation-series) Book 4
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/41539958-hive-war)
>
> The Republic, the Opters and a strange Crystalline race of unknown origin all meet in a series of titanic battles. Commodore Straker summons thousands of ships, but he soon learns he’s technologically out-classed by the enemy.
> 
> With traditional victory impossible, the race is on for each side to destroy the key worlds of the other. Billions die, entire planets are destroyed.
> 
> Even while the war rages, dark suspicions grow. There are traitors in the midst of the chaos. Straker realizes the worst enemy is the one you believed to be your ally… 