---
title: "betterlockscreen: a simple, minimal lockscreen"
slug: "betterlockscreen"
author: "Paulo Pereira"
date: 2021-03-27T17:13:39+00:00
lastmod: 2021-03-27T17:13:39+00:00
description: "[betterlockscreen](https://github.com/pavanjadhaw/betterlockscreen) is a simple and minimal lockscreen."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - i3-gaps
  - betterlockscreen
---

[betterlockscreen](https://github.com/pavanjadhaw/betterlockscreen) is a simple and minimal lockscreen.

I have been using `i3lock`, witch conflicts with `betterlockscreen`, so it will be uninstalled.

To install:

```bash
paru -S betterlockscreen
cp /usr/share/doc/betterlockscreen/examples/betterlockscreenrc ~/.config
```

In my case, using `i3-gaps`, I added to `~/.config/i3/config`:

```text
exec --no-startup-id betterlockscreen -w dim
exec --no-startup-id xautolock -time 10 -corners 000- -locker "betterlockscreen -l dim"
```

And reviewed my session key bindings:

```text
bindsym $mod+Shift+e mode "$mode_system"
set $mode_system (l)ock, (e)xit, (r)eboot, (Shift+s)hutdown
mode "$mode_system" {
    # session options
    bindsym l       exec --no-startup-id betterlockscreen -l dim, mode "default"
    bindsym e       exec --no-startup-id i3-msg exit, mode "default"
    bindsym r       exec --no-startup-id systemctl reboot, mode "default"
    bindsym Shift+s exec --no-startup-id systemctl poweroff, mode "default"
    # exit system mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
```

Finally:

```bash
systemctl enable betterlockscreen@$USER
systemctl start betterlockscreen@$USER
```