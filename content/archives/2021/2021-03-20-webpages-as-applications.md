---
title: "Using Epiphany to create applications from webpages"
slug: "webpages-as-applications"
author: "Paulo Pereira"
date: 2021-03-20T14:27:08+00:00
lastmod: 2021-03-20T14:27:08+00:00
description: "Using [Gnome Web](https://wiki.gnome.org/Apps/Web) (aka Epiphany), your can create 'applications' from webpages.\n

Using Plex as an example, this means that you can 'install' Plex from https://app.plex.tv/desktop#!/."
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Gnome Web
  - Epiphany
---

Using [Gnome Web](https://wiki.gnome.org/Apps/Web) (aka Epiphany), your can create 'applications' from webpages.

Using Plex as an example, this means that you can 'install' Plex from https://app.plex.tv/desktop#!/.

First, install `epiphany` and enable hardware acceleration.

```bash
paru -S epiphany gstreamer
gsettings set org.gnome.Epiphany.web:/ hardware-acceleration-policy 'always'
```

Open `epiphany` and go to https://app.plex.tv/desktop#!/.

"Install" as an application.

![webpages-as-applications](/posts/2021/2021-03-20-webpages-as-applications/webpages-as-applications.png)

Plex will appear as an app.

![plex](/posts/2021/2021-03-20-webpages-as-applications/plex.png)