---
title: "The Butterfly Effect by Jon Ronson (2017)"
slug: "the-butterfly-effect"
author: "Paulo Pereira"
date: 2017-11-12T22:19:00+00:00
lastmod: 2017-11-12T22:19:00+00:00
cover: "/posts/books/the-butterfly-effect.jpg"
description: "Finished “The Butterfly Effect” by Jon Ronson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Jon Ronson
book series:
  - 
book genres:
  - Nonfiction
book scores:
  - 3/10 Books
tags:
  - Book Log
  - Jon Ronson
  - Nonfiction
  - 3/10 Books
  - Audiobook
---

Finished “The Butterfly Effect”.
* Author: [Jon Ronson](/book-authors/jon-ronson)
* First Published: [July 26th 2017](/book-publication-year/2017)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35841729-the-butterfly-effect)
>
> From best-selling writer Jon Ronson and the executive producer behind the TED Radio Hour and Invisibilia, Audible Originals presents a new seven-episode series, The Butterfly Effect with Jon Ronson.
>
> [Contains explicit content] Free for a limited time, hear the story of what happened when the tech industry gave the world what it wanted: free porn. Lives were mangled. Fortunes were made. All for your pleasure. Follow writer and narrator Jon Ronson as he uncovers our web of desire.