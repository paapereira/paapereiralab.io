---
title: "The Stars My Destination by Alfred Bester (1955)"
slug: "the-stars-my-destination"
author: "Paulo Pereira"
date: 2019-05-01T22:11:00+01:00
lastmod: 2019-05-01T22:11:00+01:00
cover: "/posts/books/the-stars-my-destination.jpg"
description: "Finished “The Stars My Destination” by Alfred Bester."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1950s
book authors:
  - Alfred Bester
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Alfred Bester
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
aliases:
  - /posts/books/the-stars-my-destination
---

Finished “The Stars My Destination”.
* Author: [Alfred Bester](/book-authors/alfred-bester)
* First Published: [1955](/book-publication-year/1950s)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/37660604-the-stars-my-destination)
>
> In this pulse-quickening novel, Alfred Bester imagines a future in which people "jaunte" a thousand miles with a single thought, where the rich barricade themselves in labyrinths and protect themselves with radioactive hit men - and where an inarticulate outcast is the most valuable and dangerous man alive. 
>
> The Stars My Destination is a classic of technological prophecy and timeless narrative enchantment by an acknowledged master of science fiction.