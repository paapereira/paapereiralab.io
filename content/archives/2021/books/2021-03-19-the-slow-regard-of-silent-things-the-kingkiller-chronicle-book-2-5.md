---
title: "The Slow Regard of Silent Things (The Kingkiller Chronicle Book 2.5) by Patrick Rothfuss (2014)"
slug: "the-slow-regard-of-silent-things-the-kingkiller-chronicle-book-2-5"
author: "Paulo Pereira"
date: 2021-03-19T22:00:00+00:00
lastmod: 2021-03-19T22:00:00+00:00
cover: "/posts/books/the-slow-regard-of-silent-things-the-kingkiller-chronicle-book-2-5.jpg"
description: "Finished “The Slow Regard of Silent Things (The Kingkiller Chronicle Book 2.5)” by Patrick Rothfuss."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - Patrick Rothfuss
book series:
  - The Kingkiller Chronicle Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Patrick Rothfuss
  - The Kingkiller Chronicle Series
  - Fiction
  - Fantasy
  - 6/10 Books
  - Ebook
---

Finished “The Slow Regard of Silent Things”.
* Author: [Patrick Rothfuss](/book-authors/patrick-rothfuss)
* First Published: [October 28th 2014](/book-publication-year/2014)
* Series: [The Kingkiller Chronicle](/book-series/the-kingkiller-chronicle-series) Book 2.5
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/21801768-the-slow-regard-of-silent-things)
>
> Deep below the University, there is a dark place. Few people know of it: a broken web of ancient passageways and abandoned rooms. A young woman lives there, tucked among the sprawling tunnels of the Underthing, snug in the heart of this forgotten place.
>
> Her name is Auri, and she is full of mysteries.
>
> The Slow Regard of Silent Things is a brief, bittersweet glimpse of Auri’s life, a small adventure all her own. At once joyous and haunting, this story offers a chance to see the world through Auri’s eyes. And it gives the reader a chance to learn things that only Auri knows...
>
> In this book, Patrick Rothfuss brings us into the world of one of The Kingkiller Chronicle’s most enigmatic characters. Full of secrets and mysteries, The Slow Regard of Silent Things is the story of a broken girl trying to live in a broken world.