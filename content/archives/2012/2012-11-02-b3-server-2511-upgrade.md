---
title: "B3 Server 2.5.1.1 Upgrade"
slug: "b3-server-2511-upgrade"
author: "Paulo Pereira"
date: 2012-11-02T22:49:00+00:00
lastmod: 2012-11-02T22:49:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
---

New B3 Server versions are out, [2.5.1](http://forum.excito.net/viewtopic.php?f=22&t=4291) and [2.5.1.1](http://forum.excito.net/viewtopic.php?f=22&t=4294&sid=7de4f2af9228329656c74ac867930d4f).

## Upgrade to 2.5.1.1

- Connect to your b3 server
```bash
ssh user@b3server
```
- Become root
```bash
su
```
- Upgrade instructions
```bash
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade
```

You can confirm the software version going into b3 web administration page, then Settings and Software update.

![Settings](/posts/2012/2012-11-02-b3-server-2511-upgrade/b3server.png)

## Official Excito Forum Notes (2.5.1)

- *New Easyfind system on B3, now handling IP updates even if behind another router*
- *Print server updated to support even more printers*
- *Updated minidlna server (minidlna) to latest*
- *Some minor bug fixes*

## Official Excito Forum Notes (2.5.1.1)

- *One bug fix regarding the updater*