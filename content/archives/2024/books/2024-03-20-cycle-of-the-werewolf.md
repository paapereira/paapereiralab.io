---
title: "Cycle of the Werewolf by Stephen King (1983)"
slug: "cycle-of-the-werewolf"
author: "Paulo Pereira"
date: 2024-03-20T22:00:00+00:00
lastmod: 2024-03-20T22:00:00+00:00
cover: "/posts/books/cycle-of-the-werewolf.jpg"
description: "Finished “Cycle of the Werewolf” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
    - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - 7/10 Books
  - Ebook
---

Finished “Cycle of the Werewolf”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [November 1, 1983](/book-publication-year/1980s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43614794-cycle-of-the-werewolf)
>
> Terror began in January—by the light of the full moon...
> 
> The first scream came from the snowbound railwayman who felt the werewolf’s fangs ripping at his throat. The next month there was a scream of ecstatic agony from the woman attacked in her cozy bedroom. Now scenes of unbelievable horror unfold each time the full moon shines on the isolated Maine town of Tarker’s Mills. No one knows who will be attacked next. But one thing is sure. When the full moon rises, a paralyzing fear sweeps through Tarker's Mills. For snarls that sound like human words can be heard whining through the wind. And all around are the footprints of a monster whose hunger cannot be sated...