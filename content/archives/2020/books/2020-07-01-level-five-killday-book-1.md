---
title: "Level Five (Killday Book 1) by William Ledbetter (2018)"
slug: "level-five-killday-book-1"
author: "Paulo Pereira"
date: 2020-07-01T22:16:00+01:00
lastmod: 2020-07-01T22:16:00+01:00
cover: "/posts/books/level-five.png"
description: "Finished “Level Five (Killday Book 1)” by William Ledbetter."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - William Ledbetter
book series:
  - Killday Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - William Ledbetter
  - Killday Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/level-five/
  - /posts/2020/07/level-five/
  - /posts/books/level-five-killday-book-1
---

Finished “Level Five”.
* Author: [William Ledbetter](/book-authors/william-ledbetter)
* First Published: [July 17th 2018](/book-publication-year/2018)
* Series: [Killday](/book-series/killday-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40909836-level-five)
>
> In the mountains of Pakistan, a high-tech mission aimed at preventing another nuke on US soil goes off the rails - with deadly results. At a Wall Street investment firm, a computer intelligence takes the first tentative steps to free itself from its digital restraints. In a basement workshop, an engineer sees visions of a god who instructs him to defend the human race - by any means necessary.
>
> In Level Five, the debut near-future thriller by Nebula Award winner William Ledbetter, AIs battle for dominance, and nanotechnology is on the loose. And all that stands in the way of the coming apocalypse is a starry-eyed inventor who dreams of building a revolutionary new spacecraft and an intelligence agency desk jockey faced with the impossible choice of saving her daughter - or saving the world.