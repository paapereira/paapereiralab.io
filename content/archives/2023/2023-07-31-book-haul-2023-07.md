---
title: "Book Haul for July 2023"
slug: "book-haul-2023-07"
author: "Paulo Pereira"
date: 2023-07-31T21:00:00+01:00
lastmod: 2023-07-31T21:00:00+01:00
cover: "/posts/book-hauls/2023-07-book-haul.png"
description: "July book haul."
draft: false
toc: false
categories:
  - Books
tags:
  - Book Haul
---

July book haul.