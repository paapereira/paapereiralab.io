---
title: "Everybody Lies by Seth Stephens-Davidowitz (2017)"
slug: "everybody-lies"
author: "Paulo Pereira"
date: 2021-02-01T22:00:00+00:00
lastmod: 2021-02-01T22:00:00+00:00
cover: "/posts/books/everybody-lies.jpg"
description: "Finished “Everybody Lies: Big Data, New Data, and What the Internet Can Tell Us About Who We Really Are” by Seth Stephens-Davidowitz."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Seth Stephens-Davidowitz
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Seth Stephens-Davidowitz
  - Nonfiction
  - Science
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/everybody-lies
---

Finished “Everybody Lies: Big Data, New Data, and What the Internet Can Tell Us About Who We Really Are”.
* Author: [Seth Stephens-Davidowitz](/book-authors/seth-stephens-davidowitz)
* First Published: [May 9th 2017](/book-publication-year/2017)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/36113656-everybody-lies)
>
> Blending the informed analysis of The Signal and the Noise with the instructive iconoclasm of Think Like a Freak, a fascinating, illuminating, and witty look at what the vast amounts of information now instantly available to us reveals about ourselves and our world—provided we ask the right questions.
>
> By the end of an average day in the early twenty-first century, human beings searching the internet will amass eight trillion gigabytes of data. This staggering amount of information—unprecedented in history—can tell us a great deal about who we are—the fears, desires, and behaviors that drive us, and the conscious and unconscious decisions we make. From the profound to the mundane, we can gain astonishing knowledge about the human psyche that less than twenty years ago, seemed unfathomable.
>
> Everybody Lies offers fascinating, surprising, and sometimes laugh-out-loud insights into everything from economics to ethics to sports to race to sex, gender and more, all drawn from the world of big data. What percentage of white voters didn’t vote for Barack Obama because he’s black? Does where you go to school affect how successful you are in life? Do parents secretly favor boy children over girls? Do violent films affect the crime rate? Can you beat the stock market? How regularly do we lie about our sex lives and who’s more self-conscious about sex, men or women?
>
> Investigating these questions and a host of others, Seth Stephens-Davidowitz offers revelations that can help us understand ourselves and our lives better. Drawing on studies and experiments on how we really live and think, he demonstrates in fascinating and often funny ways the extent to which all the world is indeed a lab. With conclusions ranging from strange-but-true to thought-provoking to disturbing, he explores the power of this digital truth serum and its deeper potential—revealing biases deeply embedded within us, information we can use to change our culture, and the questions we’re afraid to ask that might be essential to our health—both emotional and physical. All of us are touched by big data everyday, and its influence is multiplying. Everybody Lies challenges us to think differently about how we see it and the world.