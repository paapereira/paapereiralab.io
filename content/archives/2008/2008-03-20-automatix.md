---
title: "Automatix"
slug: "automatix"
author: "Paulo Pereira"
date: 2008-03-20T17:51:00+00:00
lastmod: 2008-03-20T17:51:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

Now a great piece of software to help you install a bunch of stuff: [Automatix](http://www.getautomatix.com/)

I started with an update (Automatix already installed) using Update Manager.

I then installed:

- Codecs and Plugins

  - MPlayer Plugin for Firefox
  - Ubuntu Restricted Extras and Multimedia Codecs
  - W32-DVD Codecs

- Email Clients

  - CheckGmail

- Miscellaneous

  - Extra Fonts
  - Nautilus Scripts (open as root option)

- Office

  - Acrobat Reader
  - Google Earth

Check for other useful software yourself :-)