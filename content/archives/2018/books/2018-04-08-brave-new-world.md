---
title: "Brave New World by Aldous Huxley (1932)"
slug: "brave-new-world"
author: "Paulo Pereira"
date: 2018-04-08T17:55:00+01:00
lastmod: 2018-04-08T17:55:00+01:00
cover: "/posts/books/brave-new-world.jpg"
description: "Finished “Brave New World” by Aldous Huxley."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1930s
book authors:
  - Aldous Huxley
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
  - Classics
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Aldous Huxley
  - Fiction
  - Science Fiction
  - Classics
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/brave-new-world
---

Finished “Brave New World”.
* Author: [Aldous Huxley](/book-authors/aldous-huxley)
* First Published: [1932](/book-publication-year/1930s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/27400282-brave-new-world)
>
> Brave New World is a dystopian novel written in 1931 by English author Aldous Huxley, and published in 1932. Largely set in a futuristic World State of genetically modified citizens and an intelligence-based social hierarchy, the novel anticipates huge scientific developments in reproductive technology, sleep-learning, psychological manipulation, and classical conditioning that are combined to make a utopian society that goes challenged only by a single outsider.