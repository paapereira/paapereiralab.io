---
title: "Reduce brightness on HP Pavilion dv5-1020ep (Ubuntu 8.10 64 bits)"
slug: "reduce-brightness-on-hp-pavilion-dv5"
author: "Paulo Pereira"
date: 2009-01-12T22:37:00+00:00
lastmod: 2009-01-12T22:37:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 8.10
  - HP Pavilion dv5-1020ep
---

My brightness function keys always worked, but suddenly they stopped  working as well as the "Reduce backlight brightness" option in Power  Management Preferences :(
Every time I unplugged my power cord the  monitor dimmed automatically. Now not even with the function keys I get  my brightness down.

This happened in my [HP Pavilion dv5-1020ep](/posts/upgrading-to-ubuntu-810/) running Ubuntu 8.10 64 bits.

Here some additional info after searching in the Ubuntu forums and Google:

```bash
grep . -r /var/lib/acpi-support/*-*
```
```text
/var/lib/acpi-support/bios-version:F.20
/var/lib/acpi-support/system-manufacturer:Hewlett-Packard 
/var/lib/acpi-support/system-product-name:HP Pavilion dv5 Notebook PC 
/var/lib/acpi-support/system-version:Rev 1
```
```bash
more /proc/acpi/video/DVGA/LCD/brightness
```
```text
<not supported>
```
```bash
sudo echo -n 5 > /proc/acpi/video/DVGA/LCD/brightness
```
```text
bash: /proc/acpi/video/DVGA/LCD/brightness: Permission denied
```

This happened either after a kernel or hal update...

I found a workaround, that allow the brightness to be changed. This isn't the fix, if I unplug the power cord the brightness keeps it's value. Still, the function keys do their job...

Just add *acpi_osi="Linux"* to your grub menu:

```bash
sudo vi /boot/grub/menu.ls
```
```text
title           Ubuntu 8.10, kernel 2.6.27-11-generic
root            (hd0,2)
kernel          /boot/vmlinuz-2.6.27-11-generic root=UUID=cba317f7-1d97-4cf8-9ac8-a4a6aaf0ea81 ro quiet splash acpi_osi="Linux"
initrd          /boot/initrd.img-2.6.27-11-generic
quiet
```