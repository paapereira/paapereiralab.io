---
title: "ufw - Ucomplicated Firewall"
slug: "ufw-ucomplicated-firewall"
author: "Paulo Pereira"
date: 2008-06-10T15:49:00+01:00
lastmod: 2008-06-10T15:49:00+01:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Ubuntu 10.04
  - Lucid Lynx
aliases:
  - /posts/ufw-ucomplicated-firewall/
---

I'm starting to play around with **ufw** (Ucomplicated Firewall), a firewall for Linux.

It's command line based, but simple enough.

I'll update this post as I play along.

## Enable/disable ufw

```bash
sudo ufw enable
sudo ufw disable 
```

## Default policy

- mostly open ports

```bash
sudo ufw default allow
```

- mostly closed ports

```bash
sudo ufw default deny 
```

## Allow/deny services syntax

```bash
sudo ufw allow|deny <service> 
```

## Add rules syntax

```bash
sudo ufw allow|deny [proto <protocol>] [from <address>  [port <port>]] [to <address> [port <port>]] 
```

## Delete rules syntax

```bash
sudo ufw delete <rule type> from <ip address> to any port <port number> 
```

## Firewall status

```bash
sudo ufw status 
```

```text
Firewall loaded
To                         Action  From
--                         ------  ----
24800:tcp                  ALLOW   100.000.1.1
```

## Examples

```bash
sudo ufw allow proto tcp from 100.000.1.1 to any port 24800
sudo ufw delete allow proto tcp from 100.000.1.1 to any port 24800
sudo ufw allow ssh
sudo ufw delete allow ssh
```