---
title: "The Invisible Man by H.G. Wells (1897)"
slug: "the-invisible-man"
author: "Paulo Pereira"
date: 2019-05-31T22:54:00+01:00
lastmod: 2019-05-31T22:54:00+01:00
cover: "/posts/books/the-invisible-man.jpg"
description: "Finished “The Invisible Man” by H.G. Wells."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - H.G. Wells
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
  - Classics
book scores:
  - 5/10 Books
tags:
  - Book Log
  - H.G. Wells
  - Fiction
  - Science Fiction
  - Classics
  - 5/10 Books
  - Audiobook
---

Finished “The Invisible Man”.
* Author: [H.G. Wells](/book-authors/h.g.-wells)
* First Published: [1897](/book-publication-year/1800s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/23532058-the-invisible-man)
>
> On a freezing February day, a stranger emerges from out of the gray to request a room at a local provincial inn. Who is this out-of-season traveler? More confounding is the thick mask of bandages obscuring his face. Why does he disguise himself in this manner and keep himself hidden away in his room?
>
> Aroused by trepidation and curiosity, the local villagers bring it upon themselves to find the answers. What they discover is a man trapped in a terror of his own creation, and a chilling reflection of the unsolvable mysteries of their own souls.