---
title: "Migration to WordPress"
slug: "migration-to-wordpress"
author: "Paulo Pereira"
date: 2008-06-11T16:10:00+01:00
lastmod: 2008-06-11T16:10:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
---

I'm migration the blog to WordPress.

In the process I'm changing/fixing:

- the overall look of the blog
- the post categories
- some html related problems/improvements in some posts

I'm also redirecting http://lof-ubuntu-xp.lofspot.net/ from Blogspot to WordPress.

All the posts from the old blog are already migrated.

Please, [let me know what you think](https://www.blogger.com/blog/2008/6/11/migration-to-wordpress.html#comments).