---
title: "Army of One (Star Force Book 4.5) by B.V. Larson (2013)"
slug: "army-of-one-star-force-book-4.5"
author: "Paulo Pereira"
date: 2016-05-29T22:29:00+01:00
lastmod: 2016-05-29T22:29:00+01:00
cover: "/posts/books/army-of-one-star-force-book-4.5.jpg"
description: "Finished “Army of One (Star Force Book 4.5)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - B.V. Larson
book series:
  - Star Force Series
book genres:
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Force Series
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Army of One”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [January 1st 2013](/book-publication-year/2013)
* Series: [Star Force](/book-series/star-force-series) Book 4.5
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/18757740-army-of-one)
>
> The Macros are invading from the skies. And the only nanotized man who isn't officially part of Star Force learns how hard it can be to avoid an interstellar war.