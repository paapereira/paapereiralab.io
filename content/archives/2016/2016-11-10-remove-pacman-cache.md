---
title: "Remove pacman cache"
slug: "remove-pacman-cache"
author: "Paulo Pereira"
date: 2016-11-10T21:33:11+00:00
lastmod: 2016-11-10T21:33:11+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - pacman
---

`pacman` keeps a cache of package versions it installs in `/var/cache/pacman/pkg/`.

As you install and upgrade more packages, the more it grows.

So, it's a good ideia to clean it from time to time.

If you want to keep the last 2 versions of each package as an example, just execute the following.

```bash
sudo paccache -rk 2

==> finished: 2670 packages removed (disk space saved: 8.49 GiB)
```