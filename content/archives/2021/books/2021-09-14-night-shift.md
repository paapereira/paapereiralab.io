---
title: "Night Shift by Stephen King (1978)"
slug: "night-shift"
author: "Paulo Pereira"
date: 2021-09-14T21:00:00+01:00
lastmod: 2021-09-14T21:00:00+01:00
cover: "/posts/books/night-shift.jpg"
description: "Finished “Night Shift” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1970s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
  - Short Stories
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - Short Stories
  - 7/10 Books
  - Ebook
---

Finished “Night Shift”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [February 1978](/book-publication-year/1970s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/42955713-night-shift)
>
> Never trust your heart to the New York Times bestselling master of suspense, Stephen King. Especially with an anthology that features the classic stories "Children of the Corn," "The Lawnmower Man," "Graveyard Shift," "The Mangler," and "Sometimes They Come Back"-which were all made into hit horror films.
> 
> From the depths of darkness, where hideous rats defend their empire, to dizzying heights, where a beautiful girl hangs by a hair above a hellish fate, this chilling collection of twenty short stories will plunge readers into the subterranean labyrinth of the most spine-tingling, eerie imagination of our time.