---
title: "The Wealthy Barber by David H. Chilton (1989)"
slug: "the-wealthy-barber"
author: "Paulo Pereira"
date: 2015-02-06T23:36:00+00:00
lastmod: 2015-02-06T23:36:00+00:00
cover: "/posts/books/the-wealthy-barber.jpg"
description: "Finished “The Wealthy Barber: Everyone's Commonsense Guide to Becoming Financially Independent” by David H. Chilton."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - David H. Chilton
book series:
  - 
book genres:
  - Nonfiction
  - Finance
book scores:
  - 6/10 Books
tags:
  - Book Log
  - David H. Chilton
  - Nonfiction
  - Finance
  - 6/10 Books
  - Audiobook
---

Finished “The Wealthy Barber: Everyone's Commonsense Guide to Becoming Financially Independent”.
* Author: [David H. Chilton](/book-authors/david-h.-chilton)
* First Published: [1989](/book-publication-year/1980s)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/1513910.The_Wealthy_Barber)
>
> David Chilton's popular The Wealthy Barber is a good starting point for anyone who wants to construct a personal financial plan. Many people are so scared of dealing with their money that they don't do anything at all--only to suffer for it over the long haul. Chilton shows that planning is simple and you don't have be a whiz kid to set yourself on the route to financial security. "When I finally learned the basics of financial planning, I couldn't believe how straightforward they were. It's just common sense," is the overarching message.
>
> The Wealthy Barber takes the form of a novel, though it wouldn't win many awards for plot, setting, or characterization. The narrator, Dave, a 28-year-old school teacher and expectant father, his 30-year-old sister, Cathy, who runs a small business, and his buddy, Tom, who works in a refinery, sit around a barber shop in Sarnia, Ontario, and listen as Ray Miller, the well-to-do barber, teaches them how to get rich. The friends are at the age when most people start thinking about their future stability; among the three of them, they face almost every broad situation that can influence a financial plan. Ray, the Socrates of personal finance, isn't a pin-striped Bay Street wizard. He is a simple, down-to-earth barber dispensing homespun wisdom while he lops a little off the top. Ray's barbershop isn't the place to learn strategies for trading options and commodities. Instead, his advice covers the basics of RRSPs, mutual funds, real estate, insurance, and the like. His first and most important rule is "pay yourself first." Take 10 per cent off every pay cheque as it comes in and invest it in safe interest-bearing instruments. Through the magic of compound interest, this 10 per cent will turn into a substantial nest egg over time. This book isn't about how to get rich quick. It's about how to get rich slowly and stay that way. 