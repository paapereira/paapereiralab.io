---
title: "The Final Empire (The Mistborn Saga Book 1) by Brandon Sanderson (2006)"
slug: "the-final-empire-the-mistborn-saga-book-1"
author: "Paulo Pereira"
date: 2023-05-22T21:00:00+01:00
lastmod: 2023-05-22T21:00:00+01:00
cover: "/posts/books/the-final-empire-the-mistborn-saga-book-1.jpg"
description: "Finished “The Final Empire (The Mistborn Saga Book 1)” by Brandon Sanderson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2006
book authors:
  - Brandon Sanderson
book series:
  - The Mistborn Saga Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Brandon Sanderson
  - The Mistborn Saga Series
  - Fiction
  - Fantasy
  - 9/10 Books
  - Ebook
---

Finished “The Final Empire”.
* Author: [Brandon Sanderson](/book-authors/brandon-sanderson)
* First Published: [July 17, 2006](/book-publication-year/2006)
* Series: [The Mistborn Saga](/book-series/the-mistborn-saga-series) Book 1
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/68428.The_Final_Empire)
>
> For a thousand years the ash fell and no flowers bloomed. For a thousand years the Skaa slaved in misery and lived in fear. For a thousand years the Lord Ruler, the "Sliver of Infinity," reigned with absolute power and ultimate terror, divinely invincible. Then, when hope was so long lost that not even its memory remained, a terribly scarred, heart-broken half-Skaa rediscovered it in the depths of the Lord Ruler's most hellish prison. Kelsier "snapped" and found in himself the powers of a Mistborn. A brilliant thief and natural leader, he turned his talents to the ultimate caper, with the Lord Ruler himself as the mark.
> 
> Kelsier recruited the underworld's elite, the smartest and most trustworthy allomancers, each of whom shares one of his many powers, and all of whom relish a high-stakes challenge. Then Kelsier reveals his ultimate dream, not just the greatest heist in history, but the downfall of the divine despot.
> 
> But even with the best criminal crew ever assembled, Kel's plan looks more like the ultimate long shot, until luck brings a ragged girl named Vin into his life. Like him, she's a half-Skaa orphan, but she's lived a much harsher life. Vin has learned to expect betrayal from everyone she meets. She will have to learn trust if Kel is to help her master powers of which she never dreamed.