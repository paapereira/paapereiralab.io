---
title: "The Hobbit by J.R.R. Tolkien (1937)"
slug: "the-hobbit"
author: "Paulo Pereira"
date: 2018-09-02T20:54:00+01:00
lastmod: 2018-09-02T20:54:00+01:00
cover: "/posts/books/the-hobbit.jpg"
description: "Finished “The Hobbit, or There and Back Again” by J.R.R. Tolkien."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1930s
book authors:
  - J.R.R. Tolkien
book series:
  - Middle-Earth Universe
book genres:
  - Fiction
  - Fantasy
  - Classics
book scores:
  - 9/10 Books
tags:
  - Book Log
  - J.R.R. Tolkien
  - Middle-Earth Universe
  - Fiction
  - Fantasy
  - Classics
  - 9/10 Books
  - Audiobook
---

Finished “The Hobbit, or There and Back Again”.
* Author: [J.R.R. Tolkien](/book-authors/j.r.r.-tolkien)
* First Published: [September 21st 1937](/book-publication-year/1930s)
* Series: [Middle-Earth Universe](/book-series/middle-earth-universe)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/5907.The_Hobbit_or_There_and_Back_Again)
>
> In a hole in the ground there lived a hobbit. Not a nasty, dirty, wet hole, filled with the ends of worms and an oozy smell, nor yet a dry, bare, sandy hole with nothing in it to sit down on or to eat: it was a hobbit-hole, and that means comfort.
>
> Written for J.R.R. Tolkien’s own children, The Hobbit met with instant critical acclaim when it was first published in 1937. Now recognized as a timeless classic, this introduction to the hobbit Bilbo Baggins, the wizard Gandalf, Gollum, and the spectacular world of Middle-earth recounts of the adventures of a reluctant hero, a powerful and dangerous ring, and the cruel dragon Smaug the Magnificent.