---
title: "Dune Messiah (Dune Book 2) by Frank Herbert (1969)"
slug: "dune-messiah-dune-book-2"
author: "Paulo Pereira"
date: 2021-10-22T21:00:00+01:00
lastmod: 2021-10-22T21:00:00+01:00
cover: "/posts/books/dune-messiah-dune-book-2.jpg"
description: "Finished Dune Messiah (Dune Book 2)” by Frank Herbert."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Frank Herbert
book series:
  - Dune Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Frank Herbert
  - Dune Series
  - Fiction
  - Science Fiction
  - 8/10 Books
  - Ebook
---

Finished “Dune Messiah”.
* Author: [Frank Herbert](/book-authors/frank-herbert)
* First Published: [1969](/book-publication-year/1960s)
* Series: [Dune](/book-series/dune-series) Book 2
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/8117883-dune-messiah)
>
> Dune Messiah continues the story of Paul Atreides, better known—and feared—as the man christened Muad’Dib. As Emperor of the known universe, he possesses more power than a single man was ever meant to wield. Worshipped as a religious icon by the fanatical Fremen, Paul faces the enmity of the political houses he displaced when he assumed the throne—and a conspiracy conducted within his own sphere of influence.
> 
> And even as House Atreides begins to crumble around him from the machinations of his enemies, the true threat to Paul comes to his lover, Chani, and the unborn heir to his family’s dynasty...