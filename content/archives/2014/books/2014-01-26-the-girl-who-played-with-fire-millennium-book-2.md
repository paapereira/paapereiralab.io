---
title: "The Girl Who Played with Fire (Millennium Book 2) by Stieg Larsson (2006)"
slug: "the-girl-who-played-with-fire-millennium-book-2"
author: "Paulo Pereira"
date: 2014-01-26T23:51:00+00:00
lastmod: 2014-01-26T23:51:00+00:00
cover: "/posts/books/the-girl-who-played-with-fire-millennium-book-2.jpg"
description: "Finished “The Girl Who Played with Fire (Millennium Book 2)” by Stieg Larsson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2006
book authors:
  - Stieg Larsson
book series:
  - Millennium Series
book genres:
  - Fiction
  - Mystery
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stieg Larsson
  - Millennium Series
  - Fiction
  - Mystery
  - 7/10 Books
  - Audiobook
---

Finished “The Girl Who Played with Fire”.
* Author: [Stieg Larsson](/book-authors/stieg-larsson)
* First Published: [May 9th 2006](/book-publication-year/2006)
* Series: [Millennium](/book-series/millennium-series) Book 2
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/6679145-the-girl-who-played-with-fire)
>
> Lisbeth Salander is a wanted woman. Two Millennium journalists are murdered and Salander's fingerprints are on the weapon. Her history of unpredictable behaviour makes her the official suspect, but no-one can find her. Salander may be an expert at staying out of sight, but she also has ways of tracking down her most elusive enemies.