---
title: "Light Bringer (Red Rising Saga Book 6) by Pierce Brown (2023)"
slug: "light-bringer-red-rising-saga-book-6"
author: "Paulo Pereira"
date: 2023-10-07T21:00:00+01:00
lastmod: 2023-10-07T21:00:00+01:00
cover: "/posts/books/light-bringer-red-rising-saga-book-6.jpg"
description: "Finished “Light Bringer (Red Rising Saga Book 6)” by Pierce Brown."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2023
book authors:
  - Pierce Brown
book series:
  - Red Rising Saga Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Pierce Brown
  - Red Rising Saga Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
---

Finished “Light Bringer”.
* Author: [Pierce Brown](/book-authors/pierce-brown)
* First Published: [July 25, 2023](/book-publication-year/2023)
* Series: [Red Rising Saga](/book-series/red-rising-saga-series) Book 6
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/61755286-light-bringer)
>
> “The measure of a man is not the fear he sows in his enemies. It is the hope he gives his friends.”—Virginia au Augustus
> 
> The Reaper is a legend, more myth than man: the savior of worlds, the leader of the Rising, the breaker of chains.
> 
> But the Reaper is also Darrow, born of the red soil of Mars: a husband, a father, a friend.
> 
> The worlds once needed the Reaper. But now they need Darrow. Because after the dark age will come a new age: of light, of victory, of hope.