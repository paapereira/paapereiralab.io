---
title: "Dodge & Twist by Tony Lee (2011)"
slug: "dodge-and-twist"
author: "Paulo Pereira"
date: 2019-10-21T22:54:00+01:00
lastmod: 2019-10-21T22:54:00+01:00
cover: "/posts/books/dodge-and-twist.jpg"
description: "Finished “Dodge and Twist” by Tony Lee."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2011
book authors:
  - Tony Lee
book series:
  - 
book genres:
  - Fiction
book scores:
  - 4/10 Books
tags:
  - Book Log
  - Tony Lee
  - Fiction
  - 4/10 Books
  - Audiobook
---

Finished “Dodge & Twist”.
* Author: [Tony Lee](/book-authors/tony-lee)
* First Published: [2011](/book-publication-year/2011)
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44646042-dodge-twist)
>
> It's rumoured that Dickens wanted to return to his classic novel Oliver Twist, to bring him back into a later book as an older character, but he never managed this before he died. However, if Oliver had returned, what would he have been like? Would the scars of his childhood affect the man he would become? And what of 'Dodger', sent to a land halfway around the world, his friend, mentor and master dead because of Oliver?
>
> Dodge & Twist is that story. The tale of two boys, 12 years later, returning into each other's lives - for both good and bad.