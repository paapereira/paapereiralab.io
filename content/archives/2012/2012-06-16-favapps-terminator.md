---
title: "FavApps: Terminator"
slug: "favapps-terminator"
author: "Paulo Pereira"
date: 2012-06-16T19:15:00+00:00
lastmod: 2012-06-16T19:15:00+00:00
draft: false
toc: true
categories:
  - Apps
tags:
  - Linux
  - Ubuntu
  - Terminator
aliases:
  - /posts/favapps-terminator/
---

[Ubuntu Software Center](https://apps.ubuntu.com/cat/applications/terminator/)

*Terminator is a little project to produce an efficient way of filling a large area of screen space with terminals.
The user can have multiple terminals in one window and use key bindings to switch between them.*

Terminator is more than an alternative to the regular default terminal  application. It allows you to split the terminal window in two or more  terminals.

![Terminator](/posts/2012/2012-06-16-favapps-terminator/terminator.png)

## Installing Terminator

You can install Terminator via the Ubuntu Software Center. You can also use the button at the beginning of this post.

To install via terminal:
```bash
sudo apt-get install terminator
```

## Customization

You can customize Terminator in many ways. Use the preferences menu to do so.

I really like [crunchbang](http://crunchbanglinux.org/) Terminator looks, so I used the config file from that distro in my machine.

You can use it to by going into *~/.config/terminator/config* and replace all the text with:

```text
[global_config]
 title_transmit_bg_color = "#000000"
 title_inactive_bg_color = "#000000"
[keybindings]
 hide_window = <Shift><Control>a
[profiles]
 [[default]]
  scrollbar_position = hidden
   palette =  "#000000:#cc0000:#4e9a06:#c4a000:#3465a4:#75507b:#06989a:#d3d7cf:#555753:#ef2929:#8ae234:#fce94f:#729fcf:#ad7fa8:#34e2e2:#eeeeec"
  background_darkness = 0.769574944072
  scrollback_lines = 5000
  use_system_font = False
  cursor_color = "#d8d8d8"
  foreground_color = "#d8d8d8"
  scroll_on_output = False
  show_titlebar = False
  color_scheme = custom
  font = Monospace 9
  background_color = "#252627"
  scrollback_infinite = True
[layouts]
 [[default]]
  [[[child1]]]
   type = Terminal
   parent = window0
  [[[window0]]]
   type = Window
   parent = ""
[plugins]
```