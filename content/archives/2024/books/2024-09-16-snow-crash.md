---
title: "Snow Crash by Neal Stephenson (1992)"
slug: "snow-crash"
author: "Paulo Pereira"
date: 2024-09-16T21:00:00+01:00
lastmod: 2024-09-16T21:00:00+01:00
cover: "/posts/books/snow-crash.jpg"
description: "Finished “Snow Crash” by Neal Stephenson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Neal Stephenson
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Neal Stephenson
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “Snow Crash”.
* Author: [Neal Stephenson](/book-authors/neal-stephenson)
* First Published: [June 1, 1992](/book-publication-year/1990s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/75565875-snow-crash)
>
> In reality, Hiro Protagonist delivers pizza for Uncle Enzo's CosoNostra Pizza Inc., but in the Metaverse he's a warrior prince. Plunging headlong into the enigma of a new computer virus that's striking down hackers everywhere, he races along the neon-lit streets on a search-and-destroy mission for the shadowy virtual villain threatening to bring about infocalypse. Snow Crash is a mind-altering romp through a future America so bizarre, so outrageous… you'll recognize it immediately.