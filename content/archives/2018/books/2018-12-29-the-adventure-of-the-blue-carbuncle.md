---
title: "The Adventure of the Blue Carbuncle by Arthur Conan Doyle (1892)"
slug: "the-adventure-of-the-blue-carbuncle"
author: "Paulo Pereira"
date: 2018-12-29T22:41:00+00:00
lastmod: 2018-12-29T22:41:00+00:00
cover: "/posts/books/the-adventure-of-the-blue-carbuncle.jpg"
description: "Finished “The Adventure of the Blue Carbuncle” by Arthur Conan Doyle."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - Arthur Conan Doyle
book series:
  - 
book genres:
  - Fiction
  - Mystery
  - Classics
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Arthur Conan Doyle
  - Fiction
  - Mystery
  - Classics
  - 5/10 Books
  - Audiobook
---

Finished “The Adventure of the Blue Carbuncle”.
* Author: [Arthur Conan Doyle](/book-authors/arthur-conan-doyle)
* First Published: [January 1892](/book-publication-year/1800s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/24860898-the-adventure-of-the-blue-carbuncle)
>
> Two-time Emmy Award nominee (The Good Wife) and Audie Award winner for best narrator Alan Cumming here narrates Sir Arthur Conan Doyle's first-rate Yuletide whodunit "The Adventure of the Blue Carbuncle". Cumming adds his personal flair to the mystery classic and turns in an outstanding performance of the great detective Sherlock Holmes and his pal, Watson.