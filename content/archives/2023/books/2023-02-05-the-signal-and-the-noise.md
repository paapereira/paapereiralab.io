---
title: "The Signal and the Noise by Nate Silver (2012)"
slug: "the-signal-and-the-noise"
author: "Paulo Pereira"
date: 2023-02-05T15:00:00+00:00
lastmod: 2023-02-05T15:00:00+00:00
cover: "/posts/books/the-signal-and-the-noise.jpg"
description: "Finished “The Signal and the Noise” by Nate Silver."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2012
book authors:
  - Nate Silver
book series:
  - 
book genres:
  - Nonfiction
  - Science
  - Business
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Nate Silver
  - Nonfiction
  - Science
  - Business
  - 7/10 Books
  - Audiobook
---

Finished “The Signal and the Noise: Why So Many Predictions Fail - But Some Don't”.
* Author: [Nate Silver](/book-authors/nate-silver)
* First Published: [September 27, 2012](/book-publication-year/2012)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/16948405-the-signal-and-the-noise)
>
> Nate Silver built an innovative system for predicting baseball performance, predicted the 2008 election within a hair's breadth. He solidified his standing as the nation's foremost political forecaster with his near perfect prediction of the 2012 election. Silver is the founder and editor in chief of FiveThirtyEight.com.
> 
> Drawing on his own groundbreaking work, Silver examines the world of prediction, investigating how we can distinguish a true signal from a universe of noisy data. Most predictions fail, often at great cost to society, because most of us have a poor understanding of probability and uncertainty. Both experts and laypeople mistake more confident predictions for more accurate ones. But overconfidence is often the reason for failure. If our appreciation of uncertainty improves, our predictions can get better too. This is the "prediction paradox": The more humility we have about our ability to make predictions, the more successful we can be in planning for the future.
> 
> In keeping with his own aim to seek truth from data, Silver visits the most successful forecasters in a range of areas, from hurricanes to baseball, from the poker table to the stock market, from Capitol Hill to the NBA. He explains and evaluates how these forecasters think and what bonds they share. What lies behind their success? Are they good-or just lucky? What patterns have they unraveled? And are their forecasts really right? He explores unanticipated commonalities and exposes unexpected juxtapositions. And sometimes, it is not so much how good a prediction is in an absolute sense that matters but how good it is relative to the competition. In other cases, prediction is still a very rudimentary-and dangerous-science.
> 
> Silver observes that the most accurate forecasters tend to have a superior command of probability, and they tend to be both humble and hardworking. They distinguish the predictable from the unpredictable, and they notice a thousand little details that lead them closer to the truth. Because of their appreciation of probability, they can distinguish the signal from the noise.