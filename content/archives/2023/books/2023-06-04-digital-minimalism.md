---
title: "Digital Minimalism by Cal Newport (2019)"
slug: "digital-minimalism"
author: "Paulo Pereira"
date: 2023-06-04T16:00:00+01:00
lastmod: 2023-06-04T16:00:00+01:00
cover: "/posts/books/digital-minimalism.jpg"
description: "Finished “Digital Minimalism” by Cal Newport."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Cal Newport
book series:
  - 
book genres:
  - Nonfiction
  - Self Help
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Cal Newport
  - Nonfiction
  - Self Help
  - 7/10 Books
  - Audiobook
---

Finished “Digital Minimalism: Choosing a Focused Life in a Noisy World”.
* Author: [Cal Newport](/book-authors/cal-newport)
* First Published: [January 1, 2019](/book-publication-year/2019)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/44438880-digital-minimalism)
>
> Most of us know that addiction to digital tools is costing us both productivity and peace. But giving them up completely isn't realistic.
> 
> We're addicted to texting, Instagram, Facebook and Twitter not because we're stupid or shallow, but because they provide real value in the form of connection, community, affirmation, and information. Instagram is how you see new photos of your school friend's baby. Texting is how you let your mum know you're safe in a storm. Twitter is how you hear about breaking news in your industry. But these tools can also disrupt our ability to focus on meaningful work and live fully in the present. Must we choose between one or the other?
> 
> Newport's answer is no. In Digital Minimalism, he outlines a practical philosophy and plan for a mindful, intentional use of technology that maximises its benefits while minimising its drain on our attention, focus and time. Demonstrating how to implement a 30 day digital detox, this book will help you identify which uses of technology are actually helping you reach your goals, and which are holding back.
> 
> If you care about improving your effectiveness but don't want to become a Luddite or a social dropout this book can lead you to increased control over your time, attention, and energy and ultimately, a richer life. Read Digital Minimalism and you'll never again mindlessly sacrifice your productivity to clickbait or lose 40 minutes of your evening to your Instagram feed.