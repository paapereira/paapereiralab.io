---
title: "Apache, PHP and MySQL"
slug: "apache-php-and-mysql"
author: "Paulo Pereira"
date: 2008-03-20T21:31:00+00:00
lastmod: 2008-03-20T21:31:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - Apache
  - PHP
  - MySQL
  - LAMP
aliases:
  - /posts/apache-php-and-mysql/
---

Ok, setting up **Apache**, **PHP** and **MySQL** is super easy. I used this [tip](http://ubuntuforums.org/showthread.php?t=684347&highlight=mysql+database+folder) from Ubuntu Forum.

I also want to import my site and database from Windows.

## Install

- Go to System > Administration > Synaptic Package Manager
- Now go to Edit > Mark Packages by Task
- Choose LAMP Server (Linux/Apache/MySQL/PHP server)

In my case, the Ultimate Edition already had this installed :-)

I just updated Apache, MySQL and PHP using System > Administration > Update Manager.

Now I'm going to install **PHPMyAdmin** (very useful):

- Go to System > Administration > Synaptic Package Manager
- Find by *phpmyadmin*
- Install

## Get database and site from Windows

Ultimate Edition already had this installed as well :-)

I marked it to update though.

Now I need to install my Apache/MySQL/PHP solution I had under Windows.

How difficult (easy) will it be?

- Get from Windows the PHP folder *mysite*
- Get from Windows the MySQL database folder *mydatabase*
- In the Terminal (this will open the default Apache site location)

```bash
sudo nautilus /var/www
```

- Now copy the *mysite* folder here
- In the Terminal (this will open the default MySQL database location)

```bash
sudo nautilus /var/lib/mysql
```

- Now copy the *mydatabase* folder here
- In the Terminal (this will change your admin password)

```bash
sudo mysqladmin -u root password NEWPASSWORD
```

*-- NEWPASSWORD must be the same from your Windows config*

- In your favorite browser type http://localhost/mysite

Still, there's a problem. The database is read-only. But is easy to fix:

- Go to *http://localhost/myphpadmin* and login as *root*
- Select your database in the left pane
- Go to *Export*
- Select the *Add DROP TABLE / DROP VIEW* option
- Click the *Go* button
- Copy the generated script (it includes the insert statements) to your favorite text editor
- Add the following to the script, before the first drop table statement, replacing *mydatabase* with your database name and save the script

```bash
USE mydatabase;
```

- Delete the *mydatabase* folder copied from Windows

```bash
sudo rm -R /var/lib/mysql/mydatabase/
```

- In *myphpadmin* go to the home page (first icon in the left pane)
- Create a New Database with the same name from the previous, *mydatabase*
- In *myphpadmin* go to the *Query Window* (third icon in the left pane)
- In the *Query Window* go to Import and browse to your script file
- Click the *Go* button and that's it, your database is ready to go

## Post Update

After upgrading to [Hardy Heron](/posts/upgrading-to-hardy/) I had to reinstall the LAMP server. After the installation, when I accessed my index.php file, Firefox tried to download it.

I had to link the PHP module files to the *mods-enabled* Apache directory in order to get it going:

- Link the PHP modules do the mods-enabled Apache directory

```bash
cd /etc/apache2/mods-enabled/
sudo ln -s ../mods-available/php5.load .
sudo ln -s ../mods-available/php5.conf .
```

- Restart Apache

```bash
sudo /etc/init.d/apache2 restart
```

- Clean browser cache (Ctrl+Shift+Del in Firefox)

And that was it.