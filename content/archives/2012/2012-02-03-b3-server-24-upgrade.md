---
title: "B3 Server 2.4 Upgrade"
slug: "b3-server-24-upgrade"
author: "Paulo Pereira"
date: 2012-02-03T00:00:00+00:00
lastmod: 2012-02-03T00:00:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - B3 Server
---

New B3 Server version [is out](http://forum.excito.net/viewtopic.php?f=1&t=3569).

## Upgrade to 2.4

- Connect to your b3 server

```bash
ssh user@b3server
```

- Become root

```bash
su
```

- Upgrade instructions

```bash
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade
```

- Turn off B3 Server after the upgrade

- Power cycle by removing the power cord and waiting for 5 seconds before reconnecting and start B3 Server

You can confirm the software version going into b3 web administration page, then Settings and Software update.

![B3 Server](/posts/2012/2012-02-03-b3-server-24-upgrade/b3server.png)

## Official Excito Forum Notes

```text
New features and major changes
* Tor (B3 can now be a Tor node - read more here: [torproject.org](http://www.torproject.org/))
* Logitech Media Server update to 7.7.1
* Added swedish translation on UI and help texts
* Changed DAAP (iTunes) server to solve problems on OsX Lion
* Solved AFP bugs showing on OsX Lion
* HDD Load Cycle Count racing issue solved
* Fixed language support for visually impaired using screen readers
* Gerneral system updates from upstream (kernel etc.)
* Some general bug fixes
```