---
title: "A Call to Arms (The Damned Book 1) by Alan Dean Foster (1991)"
slug: "a-call-to-arms-the-damned-book-1"
author: "Paulo Pereira"
date: 2024-01-23T22:00:00+00:00
lastmod: 2024-01-23T22:00:00+00:00
cover: "/posts/books/a-call-to-arms-the-damned-book-1.jpg"
description: "Finished “A Call to Arms (The Damned Book 1)” by Alan Dean Foster."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Alan Dean Foster
book series:
  - The Damned Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Alan Dean Foster
  - The Damned Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “A Call to Arms”.
* Author: [Alan Dean Foster](/book-authors/alan-dean-foster)
* First Published: [March 27, 1991](/book-publication-year/1990s)
* Series: [The Damned](/book-series/the-damned-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/171535.A_Call_to_Arms)
>
> For eons, the Amplitur had searched space for intelligent species, each of which was joyously welcomed to take part in the fulfillment of the Amplitur Purpose. Whether it wanted to or not. When the Amplitur and their allies stumbled upon the races called the Weave, the Purpose seemed poised for a great leap forward. But the Weave's surprising unity also gave it the ability to fight the Amplitur and their cause. And fight it did, for thousands of years.
> 
> Will Dulac was a New Orleans composer who thought the tiny reef off Belize would be the perfect spot to drop anchor and finish his latest symphony in solitude. What he found instead was a group of alien visitors, a scouting party for the Weave, looking for allies among what they believed to be a uniquely warlike race, Humans.
> 
> Will tried to convince the aliens that Man was fundamentally peaceful, for he understood that Human involvement would destroy the race. But all too soon, it didn't matter. The Amplitur had discovered Earth...