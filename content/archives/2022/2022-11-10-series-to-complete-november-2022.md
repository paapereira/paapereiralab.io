---
title: "Books Series to complete buying"
slug: "series-to-complete-november-2022"
author: "Paulo Pereira"
date: 2022-11-12T11:00:00+00:00
lastmod: 2022-11-12T11:00:00+00:00
description: "Short list of series I'm yet to complete buying."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Wishlist
---

November update for my short list of series to complete buying.

* The Expanse by James S. A. Corey
* Harry Potter by J. K. Rowling
* The Dark Tower by Stephen King
* The Faithful and the Fallen by John Gwynne
* Old Man's War by John Scalzi
* The Hitchhiker's Guide to the Galaxy by Douglas Adams
* Children of Time by Adrian Tchaikovksy
* Remembrance of Earth's Past by Cixin Liu
* Culture by Iain M. Banks
* The Commonwealth Saga by Peter F. Hamilton
* The Night's Dawn by Peter F. Hamilton
* The Legend of Drizzt by R. A. Salvatore