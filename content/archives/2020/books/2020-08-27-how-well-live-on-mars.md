---
title: "How We'll Live on Mars by Stephen Petranek (2014)"
slug: "how-well-live-on-mars"
author: "Paulo Pereira"
date: 2020-08-27T21:47:00+01:00
lastmod: 2020-08-27T21:47:00+01:00
cover: "/posts/books/how-well-live-on-mars.png"
description: "Finished “How We'll Live on Mars” by Stephen Petranek."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - Stephen Petranek
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Stephen Petranek
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “How We'll Live on Mars”.
* Author: [Stephen Petranek](/book-authors/stephen-petranek)
* First Published: [October 7th 2014](/book-publication-year/2014)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/25402583-how-we-ll-live-on-mars)
>
> Award-winning journalist Stephen Petranek says humans will live on Mars by 2027. Now he makes the case that living on Mars is not just plausible, but inevitable.
>
> It sounds like science fiction, but Stephen Petranek considers it fact: Within twenty years, humans will live on Mars. We’ll need to. In this sweeping, provocative book that mixes business, science, and human reporting, Petranek makes the case that living on Mars is an essential back-up plan for humanity and explains in fascinating detail just how it will happen.
>
> The race is on. Private companies, driven by iconoclastic entrepreneurs, such as Elon Musk, Jeff Bezos, Paul Allen, and Sir Richard Branson; Dutch reality show and space mission Mars One; NASA; and the Chinese government are among the many groups competing to plant the first stake on Mars and open the door for human habitation. Why go to Mars? Life on Mars has potential life-saving possibilities for everyone on earth. Depleting water supplies, overwhelming climate change, and a host of other disasters—from terrorist attacks to meteor strikes—all loom large. We must become a space-faring species to survive. We have the technology not only to get humans to Mars, but to convert Mars into another habitable planet. It will likely take 300 years to “terraform” Mars, as the jargon goes, but we can turn it into a veritable second Garden of Eden. And we can live there, in specially designed habitations, within the next twenty years.
>
> In this exciting chronicle, Petranek introduces the circus of lively characters all engaged in a dramatic effort to be the first to settle the Red Planet. How We’ll Live on Mars brings firsthand reporting, interviews with key participants, and extensive research to bear on the question of how we can expect to see life on Mars within the next twenty years.