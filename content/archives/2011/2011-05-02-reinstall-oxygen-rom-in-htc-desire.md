---
title: "Reinstall Oxygen ROM in HTC Desire"
slug: "reinstall-oxygen-rom-in-htc-desire"
author: "Paulo Pereira"
date: 2011-05-02T21:12:00+01:00
lastmod: 2011-05-02T21:12:00+01:00
draft: false
toc: true
categories:
  - Android
tags:
  - Android
  - Oxygen ROM
---

I'm currently using the [Android Oxygen ROM](http://forum.xda-developers.com/showthread.php?t=971904) in my HTC Desire.

As the version 2.1 just came out, I've decided to wipe my device and do a fresh install.
Also I took the chance to post the procedure.

Note: I have root on my device.

## Backup before wipe

- I used [Titanium Backup](https://market.android.com/details?id=com.keramidas.TitaniumBackup) to create a backup of my apps
- I used [SMS Backup](https://market.android.com/details?id=tv.studer.smssync) to backup my sms to GMail
- I backup up my sdcard using rsync in Ubuntu

```bash
rsync -axS --delete /media/sdcard/ /media/backup
```

- I've manually kept a list of all the apps I want to keep. I will installed them all  by hand, as needed, and restore only a list of selected ones

## Download ROM and prepare for installation

- Download [Oxygen ROM v2.1](http://download.oxygen.im/roms/update-oxygen-2.1-signed.zip) from [xda-developers forum](http://forum.xda-developers.com/showthread.php?t=971904)
- I've formated my sdcard (*Settings > Storage > Erase SD card*) so I'll have a really clean install
- Copy the ROM zip file to the sdcard

## Reboot into Recovery and backup entire ROM

- I have the Clockwork Recovery (installed via [ROM Manager](https://market.android.com/details?id=com.koushikdutta.rommanager)), so I rebooted into recovery by long pressing the power button and rebooting into recovery
- In Clockwork Recovery interface:
  - backup and restore > Backup
  - reboot system now
- After rebooting backup from sdcard (*/media/sdcard/clockworkmod/backup/*)

## Reboot into Recovery and install

- Reboot into Recovery
- In Clockwork Recovery interface:
	- install zip from sdcard > choose zip from sdcard  > update-oxygen-2.1-signed.zip > Yes -  Install update-oxygen-2.1-signed.zip
  - press back button
  - wipe data/factory reset > Yes -- delete all user data
  - advanced > Wipe Dalvik Cache > Yes - Wipe Dalvik Cache
  - advanced > Wipe Battery Stats > Yes - Wipe Battery Stats (if your battery is 100% and charging)
  - press back button
  - reboot system now

## App list

- [Superuser](https://market.android.com/details?id=com.noshufou.android.su)
- [LauncherPro](https://market.android.com/details?id=com.fede.launcher)
- [Home Switcher for Froyo](https://market.android.com/details?id=ymst.android.homeswitcherfroyo)
- [Tasker](https://market.android.com/details?id=net.dinglisch.android.taskerm)
- [Gentle Alarm](https://market.android.com/details?id=com.mobitobi.android.gentlealarm)
- [On Air (Wifi Disk)](https://market.android.com/details?id=com.bw.onair)
- [Titanium Backup](https://market.android.com/details?id=com.keramidas.TitaniumBackup)
- [Gmail](https://market.android.com/details?id=com.google.android.gm)
- [Google Reader](https://market.android.com/details?id=com.google.android.apps.reader)
- [Listen](https://market.android.com/details?id=com.google.android.apps.listen)
- [Barcode Scanner](https://market.android.com/details?id=com.google.zxing.client.android)
- [Google Authenticator](https://market.android.com/details?id=com.google.android.apps.authenticator)
- [Google Docs](https://market.android.com/details?id=com.google.android.apps.docs)
- [YouTube](https://market.android.com/details?id=com.google.android.youtube)
- [Google Maps](https://market.android.com/details?id=com.google.android.apps.maps)
- [Street View on Google Maps](https://market.android.com/details?id=com.google.android.street)
- [Voice Search](https://market.android.com/details?id=com.google.android.voicesearch)
- [Google Search](https://market.android.com/details?id=com.google.android.googlequicksearchbox)
- [Google Goggles](https://market.android.com/details?id=com.google.android.apps.unveil)
- Google Talk
- [Google Translate](https://market.android.com/details?id=com.google.android.apps.translate)
- [Google Chrome to Phone](https://market.android.com/details?id=com.google.android.apps.chrometophone)
- [Blogger](https://market.android.com/details?id=com.google.android.apps.blogger)
- [GO Contacts](https://market.android.com/details?id=com.jbapps.contact)
- [GO SMS Pro](https://market.android.com/details?id=com.jb.gosms)
- [APNdroid](https://market.android.com/details?id=com.google.code.apndroid)
- [APN Portugal](https://market.android.com/details?id=com.androidpt.apnportugal)
- [Swype](http://www.swypeinc.com/)
- [SMS Backup](https://market.android.com/details?id=tv.studer.smssync)
- [Lookout](https://market.android.com/details?id=com.lookout)
- [SystemPanel App / Task Manager](https://market.android.com/details?id=nextapp.systempanel.r1)
- [Extended Controls](https://market.android.com/details?id=com.extendedcontrols)
- [Beautiful Widgets](https://market.android.com/details?id=com.levelup.beautifulwidgets)
- [Battery Snap](https://market.android.com/details?id=com.xelacorp.android.batsnaps)
- [Astro](https://market.android.com/details?id=com.metago.astro)
- [Astro SMB Module](https://market.android.com/details?id=com.metago.astro.smb)
- [SiMi Clock Widget](https://market.android.com/details?id=com.th.android.widget.gTabsimiClock)
- [Dolphin Browser Mini](https://market.android.com/details?id=com.dolphin.browser)
- [AdFree](https://market.android.com/details?id=com.bigtincan.android.adfree)
- [Adobe Reader](https://market.android.com/details?id=com.adobe.reader)
- [Flash Player](https://market.android.com/details?id=com.adobe.flashplayer)
- [ROM Manager ](https://market.android.com/details?id=com.koushikdutta.rommanager)
- [ConnectBot](https://market.android.com/details?id=org.connectbot)
- Facebook 
- Twitter 
- Tweetdeck 
- Dropbox 
- Evernote 
- Skype 
- Jorte 
- Smooth Calendar 
- NDrive
- ColorNote
- Our Groceries 
- QuickPic 
- SoundHound 
- ACV 
- air control 
- aldiko 
- amdroid 
- andry birds 
- astrid tasks 
- autowipe 
- battery snap 
- cache cleaner NG 
- camscanner 
- carhome 
- cifs manager
- cinemas 
- clockr evolution 
- currency converter 
- desktop visualizer 
- farmácias 
- folder organizer 
- food spotting 
- imdb 
- launch-x pro 
- layar 
- linkedin 
- metro lx 
- musicbox 
- music junk 
- mysporting 
- network info ii 
- prey 
- pulse 
- resultados das apostas 
- revision3 
- robo defense 
- seepu 
- seepu++ 
- smart sms vibrate 
- sms unread count 
- songbird 
- syncmypix 
- team coco 
- temp+cpu v2 
- ubuntu countdown widget 
- vplayer 
- watchdog lite 
- wheres my droid power 
- wifi status 
- winamp xda 