---
title: "Books Schedule update"
slug: "book-schedule-june-2023"
author: "Paulo Pereira"
date: 2023-06-03T17:30:00+01:00
lastmod: 2023-06-03T17:30:00+01:00
description: "June Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my June TBR (To Be Read) update for the next months.

* **[Read]** The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* **[Read]** Elder Race by Adrian Tchaikovsky
* **[Reading]** Baal by Robert R. McCammon
* Blood Rites (The Dresden Files 6) by Jim Butcher
* The Tooth Fairy by Graham Joyce
* Chasm City by Alastair Reynolds
* Next by Michael Crichton
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson
* Precious Little Things by Adrian Tchaikovsky

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson

I also have always an audiobook going too:

* **[Read]** State of Fear by Michael Crichton
* **[Read]** Jungle World (Undying Mercenaries 19) by B. V. Larson
* **[Reading]** Digital Minimalism by Cal Newport
* Insomnia by Stephen King
* Fire Fight (Star Runner 2) by B. V. Larson
* How to Avoid a Climate Disaster by Bill Gates
* Level Six (Killday 2) by William Ledbetter
* Androids and Aliens (Star Runner 3) by B. V. Larson