---
title: "Glass World (Undying Mercenaries Book 13) by B.V. Larson (2020)"
slug: "glass-world-undying-mercenaries-book-13"
author: "Paulo Pereira"
date: 2020-04-19T23:07:00+01:00
lastmod: 2020-04-19T23:07:00+01:00
cover: "/posts/books/glass-world-undying-mercenaries-book-13.png"
description: "Finished “Glass World (Undying Mercenaries Book 13)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
aliases:
  - /posts/books/glass-world-undying-mercenaries-book-13
---

Finished “Glass World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [January 20th 2020](/book-publication-year/2020)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 13
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/52220847-glass-world)
>
> The Galactics arrived with their Battle Fleet in 2052. Rather than being exterminated under a barrage of hell-burners, Earth joined a vast Empire that spanned the Milky Way.
>
> More than a century has passed since the glorious day of Earth’s annexation. Struggling to hold on to a handful of planets in the frontier provinces, humanity is at war with Rigel. After the destruction of both our fleet and theirs, a time of quiet rebuilding has begun.
>
> James McGill was hoping for a peaceful break in the conflict, but an opportunity to gain a significant technological edge arises. He’s summoned to Central and sent off to the stars, searching for a strange planet dotted with crystalline formations. There, the vicious apex predators from Rigel manufacture their impenetrable body armor.
>
> McGill is sent to the new planet first as an agent, then at the forefront of Legion Varus. He must face death, evil aliens, and his own untrustworthy government. Find out who lives and who gets permed in GLASS WORLD, book 13 of the Undying Mercenaries series.
>
> With over three million copies sold, USA Today Bestselling author B. V. Larson is the king of modern military science fiction.
>