---
title: "The Girl on the Train by Paula Hawkins (2015)"
slug: "the-girl-on-the-train"
author: "Paulo Pereira"
date: 2016-09-28T22:56:00+01:00
lastmod: 2016-09-28T22:56:00+01:00
cover: "/posts/books/the-girl-on-the-train.jpg"
description: "Finished “The Girl on the Train” by Paula Hawkins."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2015
book authors:
  - Paula Hawkins
book series:
  - 
book genres:
  - Fiction
  - Mystery
  - Thriller
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Paula Hawkins
  - Fiction
  - Mystery
  - Thriller
  - 7/10 Books
  - Audiobook
---

Finished “The Girl on the Train”.
* Author: [Paula Hawkins](/book-authors/paula-hawkins)
* First Published: [January 6th 2015](/book-publication-year/2015)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/32054384-the-girl-on-the-train)
>
> Rachel catches the same commuter train every morning. She knows it will wait at the same signal each time, overlooking a row of back gardens. She's even started to feel like she knows the people who live in one of the houses. 'Jess and Jason', she calls them. Their life - as she sees it - is perfect. If only Rachel could be that happy. And then she sees something shocking. It's only a minute until the train moves on, but it's enough. Now everything's changed. Now Rachel has a chance to become a part of the lives she's only watched from afar. Now they'll see; she's much more than just the girl on the train...