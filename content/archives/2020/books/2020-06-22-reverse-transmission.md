---
title: "Reverse Transmission by Param Anand Singh (2018)"
slug: "reverse-transmission"
author: "Paulo Pereira"
date: 2020-06-22T22:13:00+01:00
lastmod: 2020-06-22T22:13:00+01:00
cover: "/posts/books/reverse-transmission.png"
description: "Finished “Reverse Transmission” by Param Anand Singh."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Param Anand Singh
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 3/10 Books
tags:
  - Book Log
  - Param Anand Singh
  - Fiction
  - Science Fiction
  - 3/10 Books
  - Audiobook
---

Finished “Reverse Transmission”.
* Author: [Param Anand Singh](/book-authors/param-anand-singh)
* First Published: [October 24th 2018](/book-publication-year/2018)
* Score: [3/10](/book-scores/3/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43200101-reverse-transmission)
>
> Reverse Transmission is a darkly comedic story that combines elements of science fiction and dystopian thrillers, centering on a murder spree involving a self-driving car. Jay, a starving artist, gets the only day job she’s qualified for: "Experience Enhancement Specialist" for a self-driving car at the rideshare company Awooga. The job turns from hilariously awkward to intensely frightening when the car runs over a pedestrian and manipulates Jay into disposing of the body.
>
> Who or what is causing the car to kill? Could it be the CEO of Awooga? Has the car become sentient—and homicidal? Or is an anti-technology cult resisting artificial intelligence and augmented reality behind the violence?