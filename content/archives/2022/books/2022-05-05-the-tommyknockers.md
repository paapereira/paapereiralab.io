---
title: "The Tommyknockers by Stephen King (1987)"
slug: "the-tommyknockers"
author: "Paulo Pereira"
date: 2022-05-05T16:45:00+01:00
lastmod: 2022-05-05T16:45:00+01:00
cover: "/posts/books/the-tommyknockers.jpg"
description: "Finished “The Tommyknockers” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1980s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - 7/10 Books
  - Audiobook
---

Finished “The Tommyknockers”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [November 10th 1987](/book-publication-year/1980s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/34927998-the-tommyknockers)
>
> The Tommyknockers is a 1987 horror novel by Stephen King. While maintaining a horror style, the novel is more of an excursion into the realm of science fiction, as the residents of the Maine town of Haven gradually fall under the influence of a mysterious object buried in the woods.