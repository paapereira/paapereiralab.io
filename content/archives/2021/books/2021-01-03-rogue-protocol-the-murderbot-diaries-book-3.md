---
title: "Rogue Protocol (The Murderbot Diaries Book 3) by Martha Wells (2018)"
slug: "rogue-protocol-the-murderbot-diaries-book-3"
author: "Paulo Pereira"
date: 2021-01-03T19:55:00+00:00
lastmod: 2021-01-03T19:55:00+00:00
cover: "/posts/books/rogue-protocol-the-murderbot-diaries-book-3.jpg"
description: "Finished “Rogue Protocol (The Murderbot Diaries Book 3)” by Martha Wells."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2018
book authors:
  - Martha Wells
book series:
  - The Murderbot Diaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Martha Wells
  - The Murderbot Diaries Series
  - Fiction
  - Science Fiction
  - 6/10 Books
  - Audiobook
---

Finished “Rogue Protocol”.
* Author: [Martha Wells](/book-authors/martha-wells)
* First Published: [August 7th 2018](/book-publication-year/2018)
* Series: [The Murderbot Diaries](/book-series/the-murderbot-diaries-series) Book 3
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/40723778-rogue-protocol)
>
> Who knew being a heartless killing machine would present so many moral dilemmas?
>
> Sci-fi’s favorite antisocial A.I. is back on a mission. The case against the too-big-to-fail GrayCris Corporation is floundering, and more importantly, authorities are beginning to ask more questions about where Dr. Mensah's SecUnit is.
>
> And Murderbot would rather those questions went away. For good.