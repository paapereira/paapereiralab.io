---
title: "Tales from the Clarke (The Human Division Book 5) by John Scalzi (2013)"
slug: "tales-from-the-clarke-the-human-division-book-5"
author: "Paulo Pereira"
date: 2019-02-13T22:47:00+00:00
lastmod: 2019-02-13T22:47:00+00:00
cover: "/posts/books/tales-from-the-clarke-the-human-division-book-5.jpg"
description: "Finished “Tales from the Clarke (The Human Division Book 5)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Tales from the Clarke”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [February 12th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 5
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17371847-tales-from-the-clarke)
>
> Captain Sophia Coloma of the Clarke has a simple task: Ferry around representatives from Earth in an aging spaceship that the Colonial Union hopes to sell to them. But nothing is as simple as it seems, and Coloma discovers the ship she's showing off holds suprises of its own...and it's not the only one with secrets.