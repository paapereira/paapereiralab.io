---
title: "FavApps: franco.Kernel updater"
slug: "favapps-francokernel-updater"
author: "Paulo Pereira"
date: 2013-01-04T17:35:00+00:00
lastmod: 2013-01-04T17:35:00+00:00
draft: false
toc: true
categories:
  - Apps
tags:
  - Android
  - franco.Kernel
aliases:
  - /posts/favapps-francokernel-updater/
---

*A well established top-100 paid app with one of the highest ratings in that top.
It combines a series of powerful tools to manage your device with your favorite kernel, specially with franco.Kernel.*

[![Play Store](/posts/2013/2013-01-04-favapps-francokernel-updater/playstore.png)](http://play.google.com/store/apps/details?id=com.franco.kernel)

I use franco.Kernel at quite some time. The app to control to change the kernel setting is payed, but it is a great app and you support the  developer.

![franco.Kernel](/posts/2013/2013-01-04-favapps-francokernel-updater/francokernel.png)

Here are my current settings, based on [osm0sis](http://forum.xda-developers.com/showpost.php?p=34951507&postcount=34567).

## Frequencies/Voltages


Maximum CPU Frequency: 1536 MHz

Minimum CPU Frequency: 384 MHz

Current Governor: interactive

Maximum CPU Frequency for screen off: 537 MHz

Governor Control:

	above_hispeed_delay: 60000
	
	go_hispeed_load: 65
	
	hispeed_freq: 1036 MHz
	
	min_sample_time: 45000
	
	timer_rate: 15000
	
	input_boost_freq: 729 MHz


I typically do any changes in Voltages.

## Custom Kernel Settings

IO Scheduler: deadline

I usually check "Turn Logger off" and "Turn Events logging off"

TCP Congestion Avoidance Algorithm: westwood

## Color Control

Color Multipliers: 250 300 350

RGB Gamma: 0 0 12

Disable Content Adaptive Brightness: unchecked

Trinity's Contrast Interface: -24

OMAP4 Gamma: Disabled