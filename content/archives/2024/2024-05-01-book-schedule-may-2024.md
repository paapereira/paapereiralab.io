---
title: "Books Schedule update"
slug: "book-schedule-may-2024"
author: "Paulo Pereira"
date: 2024-05-01T12:40:00+01:00
lastmod: 2024-05-01T12:40:00+01:00
description: "May Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my May TBR (To Be Read) update for the next months.

* **[Read]** Galactic North by Alastair Reynolds
* **[Read]** Made Things by Adrian Tchaikovsky
* **[Read]** Shift (Silo #2) by Hugh Howey
* **[Reading]** Warbreaker by Brandon Sanderson
* Nightflyers & Other Stories by George R. R. Martin
* The False Mirror (The Damned #2) by Alan Dean Foster
* Harry Potter and the Sorcerer's Stone (Harry Potter #1) by J. K. Rowling
* Star Wars: Episode V: The Empire Strikes Back by Donald F. Glut
* White Night (The Dresden Files #9) by Jim Butcher
* Stranger in a Strange Land by Robert A. Heinlein
* Dragon Teeth by Michael Crichton
* They Thirst by Robert R. McCammon
* Snow Crash by Neal Stephenson
* Inhibitor Phase (Revelation Space #4) by Alastair Reynolds
* Lord of the Flies by William Golding
* Rocannon's World (Hainish Cycle) by Ursula K. le Guin
* The Expert System's Brother (The Expert System's Brother #1) by Adrian Tchaikovsky
* Dust (Silo #3) by Hugh Howey
* The Godfather (The Godfather #1) by Mario Puzo

I also have always an audiobook going too:

* **[Read]** Total Recall: My Unbelievably True Life Story by Arnold Schwarzenegger
* **[Read]** Minority Report and Other Stories by Philip K. Dick
* **[Reading]** The Compound Effect by Darren Hardy
* Red Company: Contact (Red Company #3) by B. V. Larson
* Super Mario: How Nintendo Conquered America by Jeff Ryan
* The Showman by Simon Shuster
* Desperation by Stephen King
* The Sandman: Act II by Neil Gaiman
* Breakfast at Tiffany's by Truman Capote
