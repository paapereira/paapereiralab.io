---
title: "New laptop"
slug: "new-laptop"
author: "Paulo Pereira"
date: 2008-09-16T18:50:00+01:00
lastmod: 2008-09-16T18:50:00+01:00
draft: false
toc: false
categories:
  - General
tags:
  - General
  - HP Pavilion dv5-1020ep
---

Just bought a new [HP Pavilion dv5-1020ep](http://h10025.www1.hp.com/ewfrf/wc/product?lc=en&dlc=en&cc=sg&product=3770477&lang=en).

Downloading 64 bits Ubuntu and almost ready to install it. I'll write a post about it.