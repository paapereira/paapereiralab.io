---
title: "Migration to Squarespace"
slug: "migration-to-squarespace"
author: "Paulo Pereira"
date: 2009-01-21T21:15:00+00:00
lastmod: 2009-01-21T21:15:00+00:00
draft: false
toc: false
categories:
  - General
tags:
  - General
---

I just changed my blog to [Squarespace](http://www.squarespace.com/?associateTag=paapereira).

I had already [posted](/posts/squarespace/) about this service, and now, finally I've changed. If you don't know it yet check out the video in the front page and sign for a trial account. You'll only need an email account to do so.

Sign the new [guestbook](http://lof-ubuntu-xp.lofspot.net/guestbook/) and let me now what you think about the new looks.