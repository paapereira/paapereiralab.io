---
title: "Administration tasks"
slug: "administration-tasks"
author: "Paulo Pereira"
date: 2008-03-20T17:02:00+00:00
lastmod: 2008-03-20T17:02:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
---

So far I've pass through:

System > Administration > 

- BootUp-Manager: I disabled  "Common files for NVIDIA video cards" (ATI on my laptop) and "Bluetooth  services". Something to investigate a little more though.
- Login Window: Changed the Theme to "Human" and disabled the login screen sound
- Network: Already manually configured my IP
- Time and Date: I like to keep my OS clock using a NTP server (*ntp02.oal.ul.pt*)