---
title: "Who Goes There? by John W. Campbell Jr. (1938)"
slug: "who-goes-there"
author: "Paulo Pereira"
date: 2023-12-17T18:30:00+00:00
lastmod: 2023-12-17T18:30:00+00:00
cover: "/posts/books/who-goes-there.jpg"
description: "Finished “Who Goes There?” by John W. Campbell Jr.."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1930s
book authors:
  - John W. Campbell Jr
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - John W. Campbell Jr
  - Fiction
  - Science Fiction
  - Horror
  - 7/10 Books
  - Audiobook
---

Finished “Who Goes There?”.
* Author: [John W. Campbell Jr.](/book-authors/john-w.-campbell-jr)
* First Published: [August 1, 1938](/book-publication-year/1930s)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/35000734-who-goes-there)
>
> The novella that formed the basis of "The Thing" is the John W. Campbell classic about an antarctic research camp that discovers and thaws the ancient, frozen body of a crash-landed alien. The creature revives with terrifying results, shape-shifting to assume the exact form of animal and man, alike.
Paranoia ensues as a band of frightened men work to discern friend from foe, and destroy the menace before it challenges all of humanity.
The story, hailed as "one of the finest science fiction novellas ever written" by the SF Writers of America, is best known to fans as THE THING, as it was the basis of Howard Hawks' The Thing From Another World in 1951, and John Carpenter's The Thing in 1982.