---
title: "Books Schedule update"
slug: "book-schedule-january-2023"
author: "Paulo Pereira"
date: 2023-01-01T16:20:00+00:00
lastmod: 2023-01-01T16:20:00+00:00
description: "January Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my January TBR (To Be Read) update for the next months.

* **[Read]** Dogs of War by Adrian Tchaikovsky
* **[Read]** Timeline by Michael Crichton
* **[Read]** Summer Knight (The Dresden Files 4) by Jim Butcher
* **[Read]** Roadside Picnic by Arkady Strugatsky and Boris Strugatsky
* **[Reading]** Redemption Ark (Revelation Space 2) by Alastair Reynolds
* The Testaments (The Handmaid's Tale 2) by Margaret Atwood
* Boy's Life by Robert R. McCammon
* Bear Head (Dogs of War 2) by Adrian Tchaikovsky
* Death Masks (The Dresden Files 5) by Jim Butcher
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* Prey by Michael Crichton
* Life of Pi by Yann Martel

Postponed:

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson

I also have always an audiobook going too:

* **[Read]** Straker's Breakers (Galactic Liberation 5) by David Vandyke & B. V. Larson
* **[DNF]** The Power of Now by Eckhart Tolle
* **[Read]** Sky World (Undying Mercenaries 18) by B. V. Larson
* **[Reading]** Skeleton Crew by Stephen King
* The Signal and the Noise by Nate Silver
* Hell's Reach (Galactic Liberation 6) by David Vandyke & B. V. Larson