---
title: "Death by Black Hole: And Other Cosmic Quandaries by Neil deGrasse Tyson (2006)"
slug: "death-by-black-hole"
author: "Paulo Pereira"
date: 2021-12-04T22:00:00+00:00
lastmod: 2021-12-04T22:00:00+00:00
cover: "/posts/books/death-by-black-hole.jpg"
description: "Finished “Death by Black Hole: And Other Cosmic Quandaries” by Neil deGrasse Tyson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2006
book authors:
  - Neil deGrasse Tyson
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Neil deGrasse Tyson
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “Death by Black Hole: And Other Cosmic Quandaries”.
* Author: [Neil deGrasse Tyson](/book-authors/neil-degrasse-tyson)
* First Published: [November 1st 2006](/book-publication-year/2006)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/24855687-death-by-black-hole)
>
> Neil deGrasse Tyson has a talent for explaining the mysteries of outer space with stunning clarity and almost childlike enthusiasm. This collection of his essays from Natural History magazine explores a myriad of cosmic topics, from astral life at the frontiers of astrobiology to the movie industry's feeble efforts to get its images of night skies right.
> 
> Tyson introduces us to the physics of black holes by explaining what would happen to our bodies if we fell into one; he also examines the needless friction between science and religion, and notes Earth's status as "an insignificantly small speck in the cosmos".
> 
> Renowned for his ability to blend content, accessibility, and humor, Tyson is a natural teacher who simplifies some of the most complex concepts in astrophysics while sharing his infectious excitement for our universe.