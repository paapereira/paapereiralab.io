---
title: "Supernova Era by Liu Cixin (2003)"
slug: "supernova-era"
author: "Paulo Pereira"
date: 2024-03-19T22:00:00+00:00
lastmod: 2024-03-19T22:00:00+00:00
cover: "/posts/books/supernova-era.jpg"
description: "Finished “Supernova Era” by Liu Cixin."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2003
book authors:
  - Liu Cixin
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Liu Cixin
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Ebook
---

Finished “Supernova Era”.
* Author: [Liu Cixin](/book-authors/liu-cixin)
* First Published: [January 1, 2003](/book-publication-year/2003)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43383035-supernova-era)
>
> In those days, Earth was a planet in space.
> In those days, Beijing was a city on Earth.
> On this night, history as known to humanity came to an end.
> 
> Eight light years away, a star has died, creating a supernova event that showers Earth in deadly levels of radiation. Within a year, everyone over the age of thirteen will die.
> 
> And so the countdown begins. Parents apprentice their children and try to pass on the knowledge they'll need to keep the world running.
> 
> But the last generation may not want to carry the legacy of their parents’ world. And though they imagine a better, brighter world, they may bring about a future so dark humanity won't survive.