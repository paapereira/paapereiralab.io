---
title: "Books Schedule update"
slug: "book-schedule-june-2022"
author: "Paulo Pereira"
date: 2022-07-01T21:00:00+01:00
lastmod: 2022-07-01T21:00:00+01:00
description: "June Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my June update for my 'books to read' schedule for the next months:

* **[Read]** To Green Angel Tower (Memory, Sorrow, and Thorn 3) by Tad Williams
* **[Reading]** Heretics of Dune (Dune 5) by Frank Herbert
* The Lost World by Michael Crichton
* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* Chapterhouse: Dune (Dune 6) by Frank Herbert
* The Handmaid's Tale by Margaret Atwood
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* Revelation Space (Revelation Space 1) by Alastair Reynolds
* Storm Front (The Dresden Files 1) by Jim Butcher
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Man in the High Castle by Philip K. Dick
* NOS4A2 by Joe Hill
* Boy's Life by Robert R. McCammon
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson
* Redemption Ark (Revelation Space 2) by Alastair Reynolds
* The Testaments (The Handmaid's Tale 2) by Margaret Atwood

I also have always an audiobook going too:

* **[Read]** Andrea Vernon and the Corporation for UltraHuman Protection by Alexander C. Kane
* **[Read]** Battleship Indomitable (Galactic Liberation 2) by David Vandyke & B. V. Larson
* **[Read]** Cain by José Saramago
* **[Reading]** The Dark Half by Stephen King
* Flagship Victory (Galactic Liberation 3) by David Vandyke & B. V. Larson
* Rich Dad Poor Dad by Robert T. Kiyosaki
* Four Past Midnight by Stephen King