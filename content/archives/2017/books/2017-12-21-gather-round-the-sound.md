---
title: "Gather 'Round the Sound by Paulo Coelho, Yvonne Morrison, Charles Dickens (2017)"
slug: "gather-round-the-sound"
author: "Paulo Pereira"
date: 2017-12-21T17:34:00+00:00
lastmod: 2017-12-21T17:34:00+00:00
cover: "/posts/books/gather-round-the-sound.jpg"
description: "Finished “Gather 'Round the Sound” by Paulo Coelho, Yvonne Morrison, Charles Dickens."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Paulo Coelho
  - Yvonne Morrison
  - Charles Dickens
book series:
  - 
book genres:
  - Fiction
book scores:
  - 4/10 Books
tags:
  - Book Log
  - Paulo Coelho
  - Yvonne Morrison
  - Charles Dickens
  - Fiction
  - 4/10 Books
  - Audiobook
---

Finished “Gather 'Round the Sound”.
* Author: [Paulo Coelho](/book-authors/paulo-coelho), [Yvonne Morrison](/book-authors/yvonne-morrison), [Charles Dickens](/book-authors/charles-dickens)
* First Published: [December 8th 2017](/book-publication-year/2017)
* Score: [4/10](/book-scores/4/10-books)

> [Goodreads](https://www.goodreads.com/book/show/37277043-gather-round-the-sound)
>
> Gather 'Round the Sound: Holiday Stories from Beloved Authors and Great Performers Across the Globe
>
> 2017 has been a monumental year for Audible, having just celebrated our 20th anniversary, a milestone that would have never been possible without our wonderful and loyal listeners. One of our major commitments is bringing new and diverse audio experiences to our members, so this year, as our gift to you, we pulled together a collection that reflects a little bit of everything we've been up to recently: a surprising and heartwarming documentary, a selection of stories from our partners around the world, and even some improvised comedic carols we hope will make you laugh as much as they did us.