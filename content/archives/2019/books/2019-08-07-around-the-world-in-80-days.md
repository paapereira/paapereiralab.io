---
title: "Around the World in 80 Days by Jules Verne (1873)"
slug: "around-the-world-in-80-days"
author: "Paulo Pereira"
date: 2019-08-07T21:41:00+01:00
lastmod: 2019-08-07T21:41:00+01:00
cover: "/posts/books/around-the-world-in-80-days.jpg"
description: "Finished “Around the World in 80 Days” by Jules Verne."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - Jules Verne
book series:
  - Extraordinary Voyages Series
book genres:
  - Fiction
  - Classics
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Jules Verne
  - Extraordinary Voyages Series
  - Fiction
  - Classics
  - 5/10 Books
  - Audiobook
---

Finished “Around the World in 80 Days”.
* Author: [Jules Verne](/book-authors/jules-verne)
* First Published: [1873](/book-publication-year/1800s)
* Series: [Extraordinary Voyages Series](/book-series/extraordinary-voyages-series)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/39853206-around-the-world-in-eighty-days)
>
> "To go around the world...in such a short time and with the means of transport currently available, was not only impossible, it was madness"
>
> One ill-fated evening at the Reform Club, Phileas Fogg rashly bets his companions £20,000 that he can travel around the entire globe in just eighty days - and he is determined not to lose. Breaking the well-establised routine of his daily life, the reserved Englishman immediately sets off for Dover, accompanied by his hot-blooded French manservant Passepartout. Travelling by train, steamship, sailing boat, sledge and even elephant, they must overcome storms, kidnappings, natural disasters, Sioux attacks and the dogged Inspector Fix of Scotland Yard - who believes that Fogg has robbed the Bank of England - to win the extraordinary wager. Around the World in Eighty Days gripped audiences on its publication and remains hugely popular, combining exploration, adventure and a thrilling race against time.
>
> Michael Glencross's lively translation is accompanied by an introduction by Brian Aldiss, which places Jules Verne's work in its literary and historical context. There is also a detailed chronology, notes and further reading.