---
title: "Manage your Arch Linux mirrors with Reflector"
slug: "reflector"
author: "Paulo Pereira"
date: 2020-08-02T15:08:17+01:00
lastmod: 2020-08-02T15:08:17+01:00
description: "*[Reflector](http://xyne.archlinux.ca/projects/reflector/) is a script which can retrieve the latest mirror list from the [MirrorStatus](https://www.archlinux.org/mirrors/status/) page, filter the most up-to-date mirrors, sort them by speed and overwrite the file `/etc/pacman.d/mirrorlist`.* \n

`yay -S reflector`"
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Reflector
---

From the [Arch Wiki](https://wiki.archlinux.org/index.php/Reflector):

*[Reflector](http://xyne.archlinux.ca/projects/reflector/) is a script which can retrieve the latest mirror list from the [MirrorStatus](https://www.archlinux.org/mirrors/status/) page, filter the most up-to-date mirrors, sort them by speed and overwrite the file `/etc/pacman.d/mirrorlist`.* 

## Instalation

- Install reflector

```bash
yay -S reflector
```

- Create the systemd service

```bash
sudo vi /etc/systemd/system/reflector.service
```

```text
[Unit]
Description=Pacman mirrorlist update
Wants=network-online.target
After=network-online.target nss-lookup.target

[Service]
Type=oneshot
ExecStart=/usr/bin/reflector --verbose --protocol https --country 'Portugal' --latest 30 --number 20 --sort rate --save /etc/pacman.d/mirrorlist

[Install]
WantedBy=multi-user.target
```

```bash
sudo systemctl daemon-reload
sudo systemctl start reflector.service
sudo systemctl status reflector.service
```

You can enable the service. I prefer to control when the scripts executes.

## Refresh your mirror list

You can force a refresh by starting the service:

```bash
sudo systemctl start reflector.service
```

Or every time the pacman-mirrorlist package is updated by creating a hook:

```bash
sudo mkdir -p /etc/pacman.d/hooks/
sudo vi /etc/pacman.d/hooks/mirrorupgrade.hook
```

```text
[Trigger]
Operation = Upgrade
Type = Package
Target = pacman-mirrorlist

[Action]
Description = Updating pacman-mirrorlist with reflector and removing pacnew...
When = PostTransaction
Depends = reflector
Exec = /bin/sh -c "reflector --verbose --protocol https --country 'Portugal' --latest 30 --number 20 --s
ort rate --save /etc/pacman.d/mirrorlist; rm -f /etc/pacman.d/mirrorlist.pacnew"
```

## Need more help?

Check the reflector help:

```bash
reflector --help
```

Or check the [Arch Wiki](https://wiki.archlinux.org/index.php/Reflector).