---
title: "Armor World (Undying Mercenaries Book 11) by B.V. Larson (2019)"
slug: "armor-world-undying-mercenaries-book-11"
author: "Paulo Pereira"
date: 2019-09-22T21:57:00+01:00
lastmod: 2019-09-22T21:57:00+01:00
cover: "/posts/books/armor-world-undying-mercenaries-book-11.png"
description: "Finished “Armor World (Undying Mercenaries Book 11)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - B.V. Larson
book series:
  - Undying Mercenaries Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Undying Mercenaries Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “Armor World”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [2019](/book-publication-year/2019)
* Series: [Undying Mercenaries](/book-series/undying-mercenaries-series) Book 11
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/47175608-armor-world)
>
> In a twist of fate that rocks the Galactic Empire, James McGill finds himself negotiating the future of a thousand inhabited worlds.
>
> An artificial object made of compressed stardust is barreling toward Earth. Is it an invasion ship? A doomsday weapon? Perhaps it’s the final response of Squanto, the Warlord of Rigel who McGill has repeatedly humiliated. Or could it be from the Mogwa, sent to avenge McGill’s assassination of Earth’s Imperial Governor?
>
> No one knows the truth of its origins, but the object is huge and unstoppable. Whoever hurled this rock at us isn’t answering our calls. Every weapon bounces off, and the people of Earth begin to go mad as they realize their destruction is only hours away.
>
> ARMOR WORLD is the eleventh book of the Undying Mercenaries Series. With over three million copies sold, author B. V. Larson is the king of modern military science fiction.