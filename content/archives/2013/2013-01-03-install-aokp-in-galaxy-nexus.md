---
title: "Install AOKP in Galaxy Nexus"
slug: "install-aokp-in-galaxy-nexus"
author: "Paulo Pereira"
date: 2013-01-03T19:56:00+00:00
lastmod: 2013-01-03T19:56:00+00:00
draft: false
toc: true
categories:
  - Apps
tags:
  - Linux
  - Ubuntu
  - GCStar
aliases:
  - /posts/install-aokp-in-galaxy-nexus/
---

This is how I install a new ROM in my Galaxy Nexus, backing up all my  apps and settings, and allowing a recover to my current state in case of something goes wrong.

I have root and have been using XenonHD (Android 4.2.1).

![aokp_maguro_jb-mr1_build-1](/posts/2013/2013-01-03-install-aokp-in-galaxy-nexus/aokp.png)

I'm going back to [AOKP](http://aokp.co/), so I decided to post how I prepare my phone for a new ROM.

## Backing up your device

- Backup using [Titanium Backup](https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup)
- Backup [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher) settings
- Backup SMS using [SMS Backup & Restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore)
- Reboot into recovery (I have ClockworkMod Recovery v6.0.2.3)
- Backup (backup and restore > backup)
- Go into "install zip from sdcard > choose zip from sdcard" just  to guarantee that ClockworkMod Recovery is accessing the storage
- Plug the phone into a computer (I'm using Ubuntu 12.10, and have already android-sdk)
- Go into your android-sdk tools folder
```bash
cd Android/android-sdk-linux_86/tools/
```
- Go into adb shell and list the directories in your "sdcard" to decide what to backup
```bash
sudo ./adb shell
ls /data/media/0
exit
```
- Pull all the directories you want to backup
```bash
sudo ./adb pull /data/media/0/data sdcard/data
```
- The previous example will create a sdcard folder in your tools folder with the data folder pulled from your phone
- Change the owner of the pulled folders
```bash
sudo chown -R youruser:youruser sdcard
```

You can also backup using FTP for example. I normally use [WellFTP Server](https://play.google.com/store/apps/details?id=com.gsapp.wifitransport).

Don't forget to backup at least your "clockwork" and "TitaniumBackup" folders!

Note: clockwork backups are located in "/data/media/clockworkmod" and  not in "/data/media/0/clockworkmod", at least at my system.

Check in what folder you have the following folders: "backup" and "blobs". That will be the correct clockwork backup folder.

## Wiping your device

- Format /data and /cache (wipe data/factory reset)
- Format /system (mounts and storage > format /system)
- Format /sdcard (mounts and storage > format /sdcard)
- Wipe Dalvik Cache (advanced > wipe dalvik cache)

After these steps, all your device, including the "sdcard" is cleaned without ROM or files.

## Installing a ROM

I will be installing [AOKP JB-MR1 Build 1](http://aokp.co/release-jb-mr1-build-1/), based on Android 4.2.1.

Some of the steps here may be different from another ROMs, for example, the need for installing Google Apps.

- Create a new folder in your "sdcard"
```bash
sudo ./adb shell
cd /data/media
mkdir 0.ROOT
mkdir 0.ROOT/AOKP
exit
```
- Push the ROM into your phone
```bash
sudo ./adb push ~/aokp_maguro_jb-mr1_build-1.zip /data/media/0.ROOT/AOKP
sudo ./adb push ~/gapps-jb-20121212-signed.zip /data/media/0.ROOT/AOKP
```
- I will also install franco.Kernel
```bash
sudo ./adb push ~/franco.Kernel-r358.zip /data/media/0.ROOT/AOKP
```
- Install the ROM (install zip from sdcard > choose zip from sdcard > 0.ROOT > AOKP > aokp_maguro_jb-mr1_build-1.zip)
- Install the Google Apps (install zip from sdcard > choose zip  from sdcard > 0.ROOT > AOKP > gapps-jb-20121212-signed.zip)
- Reboot (reboot system now)
- Login to your Google account
- Reboot into Recovery
- Wipe Cache (wipe cache partition)
- Wipe Dalvik Cache (advanced > wipe dalvik cache)
- Install franco.Kernel (install zip from sdcard > choose zip from sdcard > 0.ROOT > AOKP > franco.Kernel-r358.zip)
- Reboot (reboot system now)

Android 4.2 creates a 0/ folder in your sdcard, so you might want to move the 0.ROOT folder created before into this 0/ folder.

## Restoring your "sdcard"

- Reboot into Recovery
- Push your backed up folders into your "sdcard"
```bash
sudo ./adb push sdcard/data /data/media/0
```
- Reboot (reboot system now)

When restoring I tend to use [WellFTP Server](https://play.google.com/store/apps/details?id=com.gsapp.wifitransport).

## Restoring your apps

- Restore apps using [Titanium Backup](https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup)
- Restore [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher) settings
- Restore SMS using [SMS Backup & Restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore)

## franco.Kernel settings

I'm currently using [these](http://forum.xda-developers.com/showpost.php?p=34951507&postcount=34567) settings.

Enjoy!