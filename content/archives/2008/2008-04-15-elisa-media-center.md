---
title: "Elisa Media Center"
slug: "elisa-media-center"
author: "Paulo Pereira"
date: 2008-04-15T21:03:00+01:00
lastmod: 2008-04-15T21:03:00+01:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
---

Today I was talking with a friend of mine about Media Centers. A little by change I found a great Open Source solution.

It's called [Elisa](http://elisa.fluendo.com/).

Here's a little demo:

https://youtu.be/n_Hkr5GyrA0

I had to try it. It is super simple to install.

It all happens in the Terminal:

- First import the GPG key

```bash
wget http://elisa.fluendo.com/packages/philn.asc -O - | sudo apt-key add -
```

- Then add the Elisa package source to your sources list

  - open *sources.list* with your favorite text editor

  ```bash
  sudo vi /etc/apt/sources.list
  ```

  - add the source (this one is to Gutsy Gibbon)

  ```text
  deb http://elisa.fluendo.com/packages gutsy main
  ```

- Now all it's left to do is update *apt-get* and install Elisa

```bash
sudo apt-get update
sudo apt-get install elisa
```

Elisa is installed by default under *Applications > Sound & Video > Elisa Media Center*.

Without any configuration Elisa saw all the Music, Pictures and Videos I have in my Home Folder. It also loaded the subtitles file corresponding to an avi file. Just great.

In the Videos folder I add a link to a video folder in another file system and I could navigate in Elisa to that location.

This is Elisa 0.3.5. I can't wait for new updates.