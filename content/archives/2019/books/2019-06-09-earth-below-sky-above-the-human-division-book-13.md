---
title: "Earth Below, Sky Above (The Human Division Book 13) by John Scalzi (2013)"
slug: "earth-below-sky-above-the-human-division-book-13"
author: "Paulo Pereira"
date: 2019-06-09T23:40:00+01:00
lastmod: 2019-06-09T23:40:00+01:00
cover: "/posts/books/earth-below-sky-above-the-human-division-book-13.png"
description: "Finished “Earth Below, Sky Above (The Human Division Book 13)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “Earth Below, Sky Above”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [April 9th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 13
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17787241-earth-below-sky-above)
>
> At last, the Earth and the Colonial Union have begun formal discussions about their relationship in the future chance for the divisions in humanity to be repaired. The diplomats and crew of the Clarke are on hand to help with the process, including Ambassador Ode Abumwe and CDF Lieutenant Harry Wilson, both of whom were born on Earth. But not everyone wants The Human Division to be repaired...and they will go to great length to make sure it isn't.