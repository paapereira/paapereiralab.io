---
title: "Books Schedule update"
slug: "book-schedule-october-2022"
author: "Paulo Pereira"
date: 2022-10-01T12:15:00+01:00
lastmod: 2022-10-01T12:15:00+01:00
description: "October Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my October TBR (To Be Read) update for the next months.

Reviewed for the Halloween season and reordered.

* **[Read]** NOS4A2 by Joe Hill
* **[Read]** Cage of Souls by Adrian Tchaikovsky
* **[Reading]** Welcome to Dead House (Goosebumps 1) by R. L. Stine
* Let the Right One In by John Ajvide Lindqvist
* Grave Peril (The Dresden Files 3) by Jim Butcher
* Revelation Space (Revelation Space 1) by Alastair Reynolds
* The Testaments (The Handmaid's Tale 2) by Margaret Atwood
* Timeline by Michael Crichton
* The Final Empire (The Mistborn Saga 1) by Brandon Sanderson
* Boy's Life by Robert R. McCammon

(...)

* Dust of Dreams (Malazan Book of the Fallen 9) by Steven Erikson
* The Crippled God (Malazan Book of the Fallen 10) by Steven Erikson
* The Well of Ascension (The Mistborn Saga 2) by Brandon Sanderson
* Redemption Ark (Revelation Space 2) by Alastair Reynolds

I also have always an audiobook going too:

* **[Read]** Four Past Midnight by Stephen King
* **[Reading]** Hive War (Galactic Liberation 4) by David Vandyke & B. V. Larson
* All the Names by José Saramago
* Born a Crime: Stories From a South African Childhood by Trevor Noah
* The One Thing by Gary Keller & Jay Papasan
* Needful Things by Stephen King
* Straker's Breakers (Galactic Liberation 5) by David Vandyke & B. V. Larson