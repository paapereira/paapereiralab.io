---
title: "Understanding Complexity by Scott E. Page (2009)"
slug: "understanding-complexity"
author: "Paulo Pereira"
date: 2020-07-16T21:53:00+01:00
lastmod: 2020-07-16T21:53:00+01:00
cover: "/posts/books/understanding-complexity.png"
description: "Finished “Understanding Complexity” by Scott E. Page."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2009
book authors:
  - Scott E. Page
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Scott E. Page
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “Understanding Complexity”.
* Author: [Scott E. Page](/book-authors/scott-e.-page)
* First Published: [2009](/book-publication-year/2009)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/49625163-understanding-complexity)
>
> Recent years have seen the introduction of concepts from the new and exciting field of complexity science that have captivated the attention of economists, sociologists, engineers, businesspeople, and many others. These include tipping points, the sociological term used to describe moments when unique or rare phenomena become more commonplace; the wisdom of crowds, the argument that certain types of groups harness information and make decisions in more effective ways than individuals; six degrees of separation, the idea that it takes no more than six steps to find some form of connection between two random individuals; and emergence, the idea that new properties, processes, and structures can emerge unexpectedly from complex systems.