---
title: "Activate WebDAV in Seafile"
slug: "webdav-seafile"
author: "Paulo Pereira"
date: 2022-12-30T14:00:00+00:00
lastmod: 2022-12-30T14:00:00+00:00
cover: "/posts/2022/2022-12-30-webdav-seafile/seafile.png"
description: "Activating WebDAV in Seafile."
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Seafile
---

I turned off WebDAV in Seafile some time ago because it wasn't working.

Can't remember the issue itself, but I wasn't actively using it so...

I'm now in version 9.0.13. Here's how I enable it.

Make sure you have `pycryptodome` installed.

```bash
sudo pip install pycryptodome==3.12.0
```

Activate WebDAV in `seafdav.conf`

```text
[WEBDAV]
enabled = true
port = 8888
share_name = /seafdav
show_repo_id = true
```

Configure `ProxyPassReverse` in Apache. In my case `/etc/httpd/conf/extra/httpd-vhosts.conf`

```text
<Location /seafdav>
RequestHeader edit Destination ^https: http:
ProxyPass http://127.0.0.1:8888/seafdav
ProxyPassReverse http://127.0.0.1:8888/seafdav
</Location>
```

If you have 2FA configured you can't login via WebDAV. The trick is to activate a different password for WebDAV. In `seahub_settings.py` the following.

```text
ENABLE_WEBDAV_SECRET = True
WEBDAV_SECRET_MIN_LENGTH = 12
```

Restart.

```bash
sudo systemctl restart seafile
sudo systemctl restart seahub
```

Now in Seafile, in Setting you have a new option to add a password for WebDAV.

You can access WebDAV with https://yourdomain/seafdav