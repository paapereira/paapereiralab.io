---
title: "Formating a partition in NTFS"
slug: "formating-a-partition-in-ntfs"
author: "Paulo Pereira"
date: 2009-02-22T19:30:00+00:00
lastmod: 2009-02-22T19:30:00+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Ubuntu
  - GParted
aliases:
  - /posts/formating-a-partition-in-ntfs/
---

If you need to format a partition in NTFS and you are using [GParted](http://gparted.sourceforge.net/), just install `ntfsprogs`.

- Go to *System > Administration > Synaptic Package Manager*
- Search for *ntfsprogs*
- Rigth click and *Mark for Installation*

That's it. You should see only greens in the NTFS line in *GParted > Show Features*.

![gparted_features](/posts/2009/2009-02-22-formating-a-partition-in-ntfs/gparted-features.png)

