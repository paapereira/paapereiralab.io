---
title: "The Island of Doctor Moreau by H.G. Wells (1896)"
slug: "the-island-of-doctor-moreau"
author: "Paulo Pereira"
date: 2019-08-24T23:26:00+01:00
lastmod: 2019-08-24T23:26:00+01:00
cover: "/posts/books/the-island-of-doctor-moreau.jpg"
description: "Finished “The Island of Doctor Moreau” by H.G. Wells."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1800s
book authors:
  - H.G. Wells
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
  - Classics
book scores:
  - 5/10 Books
tags:
  - Book Log
  - H.G. Wells
  - Fiction
  - Science Fiction
  - Classics
  - 5/10 Books
  - Audiobook
aliases:
  - /posts/books/the-island-of-doctor-moreau
---

Finished “The Island of Doctor Moreau”.
* Author: [H.G. Wells](/book-authors/h.g.-wells)
* First Published: [1896](/book-publication-year/1800s)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/37756934-the-island-of-dr-moreau)
>
> Ranked among the classic novels of the English language and the inspiration for several unforgettable movies, this early work of H. G. Wells was greeted in 1896 by howls of protest from reviewers, who found it horrifying and blasphemous. They wanted to know more about the wondrous possibilities of science shown in his first book, The Time Machine, not its potential for misuse and terror. In The Island of Dr. Moreau, a shipwrecked gentleman named Edward Prendick, stranded on a Pacific island lorded over by the notorious Dr. Moreau, confronts dark secrets, strange creatures, and a reason to run for his life.
>
> While this riveting tale was intended to be a commentary on evolution, divine creation, and the tension between human nature and culture, modern readers familiar with genetic engineering will marvel at Wells’s prediction of the ethical issues raised by producing “smarter” human beings or bringing back extinct species. These levels of interpretation add a richness to Prendick’s adventures on Dr. Moreau’s island of lost souls without distracting from what is still a rip-roaring good read.