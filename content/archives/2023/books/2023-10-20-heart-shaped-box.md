---
title: "Heart-Shaped Box by Joe Hill (2007)"
slug: "heart-shaped-box"
author: "Paulo Pereira"
date: 2023-10-20T21:00:00+01:00
lastmod: 2023-10-20T21:00:00+01:00
cover: "/posts/books/heart-shaped-box.jpg"
description: "Finished “Heart-Shaped Box” by Joe Hill."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2007
book authors:
  - Joe Hill
book series:
  - 
book genres:
  - Fiction
  - Horror
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Joe Hill
  - Fiction
  - Horror
  - 7/10 Books
  - Ebook
---

Finished “Heart-Shaped Box”.
* Author: [Joe Hill](/book-authors/joe-hill)
* First Published: [February 13, 2007](/book-publication-year/2007)
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/59054810-heart-shaped-box)
>
> Judas Coyne is a collector of the macabre: a cookbook for cannibals . . . a used hangman's noose . . . a snuff film. An aging death-metal rock god, his taste for the unnatural is as widely known to his legions of fans as the notorious excesses of his youth. But nothing he possesses is as unlikely or as dreadful as his latest discovery, an item for sale on the Internet, a thing so terribly strange, Jude can't help but reach for his wallet.
> 
> I will "sell" my stepfather's ghost to the highest bidder. . . .
> 
> For a thousand dollars, Jude will become the proud owner of a dead man's suit, said to be haunted by a restless spirit. He isn't afraid. He has spent a lifetime coping with ghosts—of an abusive father, of the lovers he callously abandoned, of the bandmates he betrayed. What's one more?
> 
> But what UPS delivers to his door in a black heart-shaped box is no imaginary or metaphorical ghost, no benign conversation piece. It's the real thing.
> 
> And suddenly the suit's previous owner is everywhere: behind the bedroom door . . . seated in Jude's restored vintage Mustang . . . standing outside his window . . . staring out from his widescreen TV. Waiting—with a gleaming razor blade on a chain dangling from one bony hand. . . .