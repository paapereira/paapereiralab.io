---
title: "Galileo and the Science Deniers by Mario Livio (2020)"
slug: "galileo-and-the-science-deniers"
author: "Paulo Pereira"
date: 2022-03-20T18:00:00+00:00
lastmod: 2022-03-20T18:00:00+00:00
cover: "/posts/books/galileo-and-the-science-deniers.jpg"
description: "Finished “Galileo and the Science Deniers” by Mario Livio."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2020
book authors:
  - Mario Livio
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Mario Livio
  - Nonfiction
  - Science
  - 5/10 Books
  - Audiobook
---

Finished “Galileo and the Science Deniers”.
* Author: [Mario Livio](/book-authors/mario-livio)
* First Published: [2020](/book-publication-year/2020)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/55891941-galileo-and-the-science-deniers)
>
> An insightful, accessible, and fresh interpretation of the life of Galileo Galilei, one of history’s greatest and most fascinating scientists, that sheds new light on his discoveries and how he was challenged by science deniers—a cautionary tale for our time.
> 
> Galileo’s story may be more relevant today than ever before. At present, we face enormous crises—such as the minimization of the dangers of climate change—because the science behind these threats is erroneously questioned or ignored. Galileo encountered this problem 400 years ago. His discoveries, based on careful observations and ingenious experiments, contradicted conventional wisdom and the teachings of the church at the time. Consequently, in a blatant assault on freedom of thought, his books were forbidden by church authorities.