---
title: "Childhood's End by Arthur C. Clarke (1953)"
slug: "childhoods-end"
author: "Paulo Pereira"
date: 2021-11-15T22:00:00+00:00
lastmod: 2021-11-15T22:00:00+00:00
cover: "/posts/books/childhoods-end.jpg"
description: "Finished “Childhood's End” by Arthur C. Clarke."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1950s
book authors:
  - Arthur C. Clarke
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 9/10 Books
tags:
  - Book Log
  - Arthur C. Clarke
  - Fiction
  - Science Fiction
  - 9/10 Books
  - Ebook
aliases:
  - /posts/books/childhoods-end
---

Finished “Childhood's End”.
* Author: [Arthur C. Clarke](/book-authors/arthur-c.-clarke)
* First Published: [August 1953](/book-publication-year/1950s)
* Score: [9/10](/book-scores/9/10-books)

> [Goodreads](https://www.goodreads.com/book/show/50783631-childhood-s-end)
>
> In the near future, enormous silver spaceships appear without warning over mankind’s largest cities. They belong to the Overlords, an alien race far superior to humanity in technological development. Their purpose is to dominate Earth. Their demands, however, are surprisingly benevolent: end war, poverty, and cruelty. Their presence, rather than signaling the end of humanity, ushers in a golden age . . . or so it seems.
> 
> Without conflict, human culture and progress stagnate. As the years pass, it becomes clear that the Overlords have a hidden agenda for the evolution of the human race that may not be as benevolent as it seems.