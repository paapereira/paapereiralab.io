---
title: "We Only Need the Heads (The Human Division Book 3) by John Scalzi (2013)"
slug: "we-only-need-the-heads-the-human-division-book-3"
author: "Paulo Pereira"
date: 2019-01-27T22:14:00+00:00
lastmod: 2019-01-27T22:14:00+00:00
cover: "/posts/books/we-only-need-the-heads-the-human-division-book-3.jpg"
description: "Finished “We Only Need the Heads (The Human Division Book 3)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “We Only Need the Heads”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [January 29th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 3
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17334765-we-only-need-the-heads)
>
> CDF Lieutenant Harry Wilson has been loaned out to a CDF platoon tasked with secretly removing an unauthorized colony of humans on an alien world. Colonial Ambassador Abumwe has been ordered to participate in final negotiations with an alien race the Union hopes to make allies. Wilson and Abumwes missions are fated to crossand in doing so, place both missions at risk of failure.