---
title: "Jabber/XMPP Instant Messaging"
slug: "xmpp-2023-update"
author: "Paulo Pereira"
date: 2023-03-26T15:00:00+01:00
lastmod: 2023-03-26T15:00:00+01:00
description: "*XMPP is the Extensible Messaging and Presence Protocol, a set of open technologies for instant messaging, presence, multi-party chat, voice and video calls, collaboration, lightweight middleware, content syndication, and generalized routing of XML data.*"
draft: false
toc: true
categories:
  - General
tags:
  - Linux
  - Android
  - XMPP
  - Jabber
aliases:
  - /posts/xmpp-2023-update
---

## Quick start

1. Install [Conversations](https://play.google.com/store/apps/details?id=eu.siacs.conversations)
2. Create your free `username@conversations.im` account
3. Contact [me](mailto:paulo@paapereira.xyz) if you need help/tips

## What is Jabber (aka XMPP)?

*XMPP is the Extensible Messaging and Presence Protocol, a set of open technologies for instant messaging, presence, multi-party chat, voice and video calls, collaboration, lightweight middleware, content syndication, and generalized routing of XML data.*[^1]

[^1]: https://xmpp.org/about/technology-overview.html

In simple terms *xmpp* it’s like imap and pop3 for email.
A major advantage is that you can create an account in you preferred server (just like email) and talk to another person that have an account in another server.

Remember Google Talk? It used XMPP. Facebook and Whatsapp? Initially used XMPP before locking themselves up.

## How to use

Basically you need to:

1. Choose a server to register your account (just like email);
2. Login to your account from a XMPP client.

## XMPP Servers

There are many servers to choose from, and this could be hard for new users.
Different servers have different XMPP Extension Protocols. These extensions offer functionality.
https://compliance.conversations.im/ is the best place to check if a server is compliant.
https://xmpp-servers.404.city/ provide a list of different servers.

Always look for OMEMO support for encrypted and private messages.

My recommendations:

- https://conversations.im/ (free, `username@conversations.im` account, paid option to use your domain name)
- https://quicksy.im/ (free, registered using your phone number, `+phonenumber@quicksy.im` account)

## XMPP Clients

You can use your account in any XMPP client. Just like an email client your can change it anytime and keep the same account.
https://xmpp.org/software/clients.html  and https://github.com/iNPUTmice/talks/blob/master/2019_08_10_-_state_of_the_xmpp_community.md for client recommendations.

My recommendations:

- https://conversations.im/ for Android, paid app
- https://quicksy.im/ for Android, free app
- https://gajim.org/ for Linux/Windows (install the OMEMO, HTTP Upload and URL image preview plugins)
- https://pidgin.im/ for Linux/Windows (in Arch Linux I install OMEMO (libpurple-lurch) and Message Carbons (libpurple-carbons) plugins)
- https://chatsecure.org/ for iOS (never tried it though)

# More information

Great read about XMPP: https://gultsch.de/xmpp_2016.html.

Conversations FAQ with useful information: https://github.com/siacs/Conversations (check the OMEMO and backup info).

If your are not getting notifications maybe it’s your phone killing the app: https://dontkillmyapp.com/.

Interesting articles:
- https://medium.com/@dreamflasher/jabber-omemo-getting-started-f1e91c15d833
- https://medium.com/hackernoon/encrypted-instant-messaging-recommendations-january-2017-711c03af02cc