---
title: "Stan Lee's Alliances: A Trick of Light by Stan Lee (2019)"
slug: "alliances"
author: "Paulo Pereira"
date: 2020-05-10T22:29:00+01:00
lastmod: 2020-05-10T22:29:00+01:00
cover: "/posts/books/alliances.png"
description: "Finished “Stan Lee's Alliances: A Trick of Light” by Stan Lee."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2019
book authors:
  - Stan Lee
book series:
  - 
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - Stan Lee
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
aliases:
  - /posts/books/alliances
---

Finished “Stan Lee's Alliances: A Trick of Light”.
* Author: [Stan Lee](/book-authors/stan-lee)
* First Published: [June 27th 2019](/book-publication-year/2019)
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/46247857-stan-lee-s-alliances)
>
> Stan Lee’s Alliances: A Trick of Light introduces listeners to a mysterious young woman, Nia, whose fate is intertwined with a seemingly ordinary Midwestern teenager, Cameron Ackerson, whose quest for YouTube stardom takes him to the heart of the Great Lakes Triangle where he is imbued with the power to "see" and "hear" a new reality.
>
> As Nia and Cameron’s relationship grows, they conspire to create a more righteous online universe, but wind up crossing a shadowy outfit called OPTIC. Meanwhile, a threat of galactic proportions emerges, imperiling humanity’s very existence by harnessing our individual desire to connect as the very means to destroy us.
>
> Can Nia, Cameron, and Juaquo (Cameron’s best friend) save us from our own collective ruin?
>
> Join forces with Stan Lee to find out!