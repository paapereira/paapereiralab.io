---
title: "Dune (Dune Book 1) by Frank Herbert (1965)"
slug: "dune-dune-book-1"
author: "Paulo Pereira"
date: 2021-08-23T21:00:00+01:00
lastmod: 2021-08-23T21:00:00+01:00
cover: "/posts/books/dune-dune-book-1.jpg"
description: "Finished “Dune (Dune Book 1)” by Frank Herbert."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1960s
book authors:
  - Frank Herbert
book series:
  - Dune Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 10/10 Books
tags:
  - Book Log
  - Frank Herbert
  - Dune Series
  - Fiction
  - Science Fiction
  - 10/10 Books
  - Ebook
aliases:
  - /posts/books/dune-dune-book-1
---

Finished “Dune”.
* Author: [Frank Herbert](/book-authors/frank-herbert)
* First Published: [June 1965](/book-publication-year/1960s)
* Series: [Dune](/book-series/dune-series) Book 1
* Score: [10/10](/book-scores/10/10-books)

> [Goodreads](https://www.goodreads.com/book/show/43419431-dune)
>
> Set on the desert planet Arrakis, Dune is the story of the boy Paul Atreides, heir to a noble family tasked with ruling an inhospitable world where the only thing of value is the “spice” melange, a drug capable of extending life and enhancing consciousness. Coveted across the known universe, melange is a prize worth killing for....
> 
> When House Atreides is betrayed, the destruction of Paul’s family will set the boy on a journey toward a destiny greater than he could ever have imagined. And as he evolves into the mysterious man known as Muad’Dib, he will bring to fruition humankind’s most ancient and unattainable dream.
> 
> A stunning blend of adventure and mysticism, environmentalism and politics, Dune won the first Nebula Award, shared the Hugo Award, and formed the basis of what is undoubtedly the grandest epic in science fiction.