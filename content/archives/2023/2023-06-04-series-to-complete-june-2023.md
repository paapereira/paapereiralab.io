---
title: "Books Series to complete buying"
slug: "series-to-complete-june-2023"
author: "Paulo Pereira"
date: 2023-06-04T21:00:00+01:00
lastmod: 2023-06-04T21:00:00+01:00
description: "Short list of series I'm yet to complete buying."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Wishlist
---

June update for my short list of series to complete buying.

* Culture by Iain M. Banks
* The Godfather by Mario Puzo
* New Crobuzon by China Mieville
* The Last Kingdom by Bernard Cornwell
* The Legend of Drizzt by R. A. Salvatore
* Sookie Stackhouse by Charlaine Harris
* Star Wars: Republic Commando