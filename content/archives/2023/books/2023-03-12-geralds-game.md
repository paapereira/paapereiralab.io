---
title: "Gerald's Game by Stephen King (1992)"
slug: "geralds-game"
author: "Paulo Pereira"
date: 2023-03-12T14:00:00+00:00
lastmod: 2023-03-12T14:00:00+00:00
cover: "/posts/books/geralds-game.jpg"
description: "Finished “Gerald's Game” by Stephen King."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 1990s
book authors:
  - Stephen King
book series:
  - Stephen King Novels
book genres:
  - Fiction
  - Horror
book scores:
  - 8/10 Books
tags:
  - Book Log
  - Stephen King
  - Stephen King Novels
  - Fiction
  - Horror
  - 8/10 Books
  - Audiobook
---

Finished “Gerald's Game”.
* Author: [Stephen King](/book-authors/stephen-king)
* First Published: [January 1, 1992](/book-publication-year/1990s)
* Series: [Stephen King Novels](/book-series/stephen-king-novels)
* Score: [8/10](/book-scores/8/10-books)

> [Goodreads](https://www.goodreads.com/book/show/28931357-gerald-s-game)
>
> A game. A husband-and-wife game. Gerald's Game.
> 
> But this time Jessie didn't want to play. Lying there, spreadeagled and handcuffed to the bedstead while he'd loomed and drooled over her, she'd felt angry and humiliated.
> 
> So she'd kicked out hard. Aimed to hit him where it hurt.
> 
> And now he was dead - a coronary - on the floor.
> 
> Leaving Jessie alone and helpless in a lakeside holiday cabin. Miles from nowhere. No-one to hear her screams.
> 
> Alone. Except for the voices in her head that begun to chatter and argue and sneer...