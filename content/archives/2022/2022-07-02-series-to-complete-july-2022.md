---
title: "Books Series to complete buying"
slug: "series-to-complete-july-2022"
author: "Paulo Pereira"
date: 2022-07-02T13:54:44+01:00
lastmod: 2022-07-02T13:54:44+01:00
description: "Short list of series I'm yet to complete buying."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Wishlist
---

July update for my short list of series to complete buying.

* The Wheel of Time by Robert Jordan
* Ender's Saga by Orson Scott Card
* The Witcher by Andrzej Sapkowski
* A Song of Ice and Fire by George R. R. Martin
* The Expanse by James S. A. Corey
* Culture by Iain M. Banks
* The First Law Trilogy by Joe Abercrombie
* The Green Bone Saga by Fonda Lee
* Harry Potter by J. K. Rowling
* The Legend of Drizzt by R. A. Salvatore
* Old Man's War by John Scalzi
* The Dark Tower by Stephen King
* The Hitchhiker's Guide to the Galaxy by Douglas Adams
* The Faithful and the Fallen by John Gwynne