---
title: "Superintelligence: Paths, Dangers, Strategies by Nick Bostrom (2014)"
slug: "superintelligence"
author: "Paulo Pereira"
date: 2019-01-09T22:04:00+00:00
lastmod: 2019-01-09T22:04:00+00:00
cover: "/posts/books/superintelligence.jpg"
description: "Finished “Superintelligence: Paths, Dangers, Strategies” by Nick Bostrom."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2014
book authors:
  - Nick Bostrom
book series:
  - 
book genres:
  - Nonfiction
  - Science
book scores:
  - 6/10 Books
tags:
  - Book Log
  - Nick Bostrom
  - Nonfiction
  - Science
  - 6/10 Books
  - Audiobook
---

Finished “Superintelligence: Paths, Dangers, Strategies”.
* Author: [Nick Bostrom](/book-authors/nick-bostrom)
* First Published: [July 3rd 2014](/book-publication-year/2014)
* Score: [6/10](/book-scores/6/10-books)

> [Goodreads](https://www.goodreads.com/book/show/23877508-superintelligence)
>
> Superintelligence asks the questions: What happens when machines surpass humans in general intelligence? Will artificial agents save or destroy us? Nick Bostrom lays the foundation for understanding the future of humanity and intelligent life. The human brain has some capabilities that the brains of other animals lack. It is to these distinctive capabilities that our species owes its dominant position. If machine brains surpassed human brains in general intelligence, then this new superintelligence could become extremely powerful - possibly beyond our control. As the fate of the gorillas now depends more on humans than on the species itself, so would the fate of humankind depend on the actions of the machine superintelligence.
>
> But we have one advantage: We get to make the first move. Will it be possible to construct a seed Artificial Intelligence, to engineer initial conditions so as to make an intelligence explosion survivable? How could one achieve a controlled detonation?
>
> This profoundly ambitious and original book breaks down a vast track of difficult intellectual terrain. After an utterly engrossing journey that takes us to the frontiers of thinking about the human condition and the future of intelligent life, we find in Nick Bostrom's work nothing less than a reconceptualization of the essential task of our time.