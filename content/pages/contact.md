---
title: "Contacts"
slug: "contact"
toc: false
aliases:
  - /pages/contact
---

Contacts in order of preference:

* SimpleX Chat [invite](https://simplex.chat/contact/#/?v=1-2&smp=smp%3A%2F%2F0YuTwO05YJWS8rkjn9eLJDjQhFKvIYd8d4xG8X1blIU%3D%40smp8.simplex.im%2FS5AjypkVk6ynYfnrtynDUNFT_qd_eGRd%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEA7Vbm8UJlN_imOm2u-7CWb4XLCQ6mBhA8irTrY7bxeXo%253D%26srv%3Dbeccx4yfxxbvyhqypaavemqurytl6hozr47wfc7uuecacjqdvwpw2xid.onion) - [Learn more](/posts/simplex-chat)
* (*Not using it at the moment*) XMPP - [paapereira@conversations.im](https://conversations.im/i/paapereira@conversations.im?omemo-sid-1999939473=9df5caa9906214747ff8e3d2bb0d2b4dd6edb46b6f164f69338c876910d11106&omemo-sid-518817183=a0db5e9d1c7b0b7c5dface0f30de6cb36523fb2c142533b53c04067b53b48b36) - [Learn more](/posts/xmpp-2023-update)
* (*Not using it at the moment*) Signal - [if you have my phone number](https://www.signal.org/)
* (*Not using it at the moment*) Matrix - [@paapereira](https://matrix.to/#/@paapereira:matrix.org)
* Email - [paulo@paapereira.xyz](mailto:paulo@paapereira.xyz)
* WhatsApp - [if you have my phone number](https://www.whatsapp.com/)
* [More](/pages/about)...