---
title: "The Sound of Rebellion (The Human Division Book 8) by John Scalzi (2013)"
slug: "the-sound-of-rebellion-the-human-division-book-8"
author: "Paulo Pereira"
date: 2019-03-28T22:52:00+00:00
lastmod: 2019-03-28T22:52:00+00:00
cover: "/posts/books/the-sound-of-rebellion-the-human-division-book-8.jpg"
description: "Finished “The Sound of Rebellion (The Human Division Book 8)” by John Scalzi."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - John Scalzi
book series:
  - The Human Division Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 5/10 Books
tags:
  - Book Log
  - John Scalzi
  - The Human Division Series
  - Fiction
  - Science Fiction
  - 5/10 Books
  - Audiobook
---

Finished “The Sound of Rebellion”.
* Author: [John Scalzi](/book-authors/john-scalzi)
* First Published: [March 5th 2013](/book-publication-year/2013)
* Series: [The Human Division](/book-series/the-human-division-series) Book 8
* Score: [5/10](/book-scores/5/10-books)

> [Goodreads](https://www.goodreads.com/book/show/17557870-the-sound-of-rebellion)
>
> The Colonial Defense Forces usually protect humanity from alien attack, but now the stability of the Colonial Union has been threatened, and Lieutenant Heather Lee and her squad are called to squash a rebellion on a colony world. It seems simple enoughbut theres a second act to the rebellion that finds Lee captive, alone, and armed with only her brains to survive.