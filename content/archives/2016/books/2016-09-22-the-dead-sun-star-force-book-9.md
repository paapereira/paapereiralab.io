---
title: "The Dead Sun (Star Force Book 9) by B.V. Larson (2013)"
slug: "the-dead-sun-star-force-book-9"
author: "Paulo Pereira"
date: 2016-09-22T22:56:00+01:00
lastmod: 2016-09-22T22:56:00+01:00
cover: "/posts/books/the-dead-sun-star-force-book-9.jpg"
description: "Finished “The Dead Sun (Star Force Book 9)” by B.V. Larson."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2013
book authors:
  - B.V. Larson
book series:
  - Star Force Series
book genres:
  - Fiction
  - Science Fiction
book scores:
  - 7/10 Books
tags:
  - Book Log
  - B.V. Larson
  - Star Force Series
  - Fiction
  - Science Fiction
  - 7/10 Books
  - Audiobook
---

Finished “The Dead Sun”.
* Author: [B.V. Larson](/book-authors/b.v.-larson)
* First Published: [January 1st 2013](/book-publication-year/2013)
* Series: [Star Force](/book-series/star-force-series) Book 9
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/20726973-the-dead-sun)
>
> Kyle Riggs sits uneasily upon Earth’s throne. He’s liberated his homeworld from a tyrant, only to replace him. In The Dead Sun, the ninth book of the Star Force Series, the Great War between life and the machines reaches its final chapter. Both sides have new technology and expanded industrial bases. Star Force and the machines attempt to exterminate one another in a final, glorious conflict. Along the way, Riggs finally learns who his real friends and enemies are. The Dead Sun is a military science fiction novel by best-selling author B. V. Larson. To read the first book in this series, look for Swarm, by B. V. Larson.