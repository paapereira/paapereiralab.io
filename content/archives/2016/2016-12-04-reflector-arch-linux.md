---
title: "Use Reflector to update your pacman mirror list"
slug: "reflector-arch-linux"
author: "Paulo Pereira"
date: 2016-12-04T20:22:12+00:00
lastmod: 2016-12-04T20:22:12+00:00
draft: false
toc: false
categories:
  - Linux
tags:
  - Linux
  - Arch Linux
  - Reflector
  - pacman
---

```bash
sudo pacman -S reflector

sudo vi /etc/systemd/system/reflector.service
```

```text
[Unit]
Description=Pacman mirrorlist update

[Service]
Type=oneshot
ExecStart=/usr/bin/reflector --verbose --country 'Portugal' -l 5 -p http --sort rate --save /etc/pacman.d/mirrorlist
```

```bash
sudo systemctl start reflector
```