---
title: "Books Schedule update"
slug: "book-schedule-february-2025"
author: "Paulo Pereira"
date: 2025-03-02T15:15:00+00:00
lastmod: 2025-03-02T15:15:00+00:00
description: "February Books Schedule update."
draft: false
toc: false
categories:
  - Books
tags:
  - Books Schedule
---

Here's my February TBR (To Be Read) update for the next months.

* **[Read]** Aurora Rising (Prefect Dreyfus Emergency #1) by Alastair Reynolds
* **[Read]** The Sicilian (The Godfather #2) by Mario Puzo
* **[Read]** The Andromeda Evolution (Andromeda #2) by Daniel H. Wilson & Michael Crichton
* **[Read]** Planet of Exile (Hainish Cycle) by Ursula K. le Guin
* **[Read]** We Can Build You by Philip K. Dick
* **[Read]** A Coming of Age by Timothy Zahn
* **[Read]** Dead Med (Prescription: Murder #1) by Freida McFadden
* **[Read]** The Expert System's Champion (The Expert System's Brother #2) by Adrian Tchaikovsky
* **[Reading]** The Way of Kings (The Stormlight Archive #1) by Brandon Sanderson
* The Bullet Journal Method by Ryder Carroll
* Harry Potter and the Prisoner of Azkaban (Harry Potter #3) by J. K. Rowling
* Resurrection, Inc. by Kevin J. Anderson
* Turn Coat (The Dresden Files #11) by Jim Butcher
* Revan (Star Wars: The Old Republic #1) by Drew Karpyshyn
* Usher's Passing by Robert R. McCammon
* Memorial do Convento by José Saramago
* Elysium Fire (Prefect Dreyfus Emergency #2) by Alastair Reynolds
* A Case of Need by Michael Crichton & Jeffery Hudson
* City of Illusions (Hainish Cycle) by Ursula K. le Guin
* The Ex by Freida McFadden
* The Doors of Eden by Adrian Tchaikovsky
* Prelude to Space by Arthur C. Clarke

I also have always an audiobook going too:

* **[Read]** Nexus by Yuval Noah Harari
* **[Read]** Red Company: Invasion (Red Company #4) by B. V. Larson
* **[Read]** The Richest Man in Babylon by George S. Clason
* **[Reading]** Bag of Bones by Stephen King
* Impact Winter #1 by Travis Beacham
* A History of Video Games by Jeremy Parish
* Red Company: Steel Rain (Red Company #5) by B. V. Larson
* The Sandman: Act III by Neil Gaiman
