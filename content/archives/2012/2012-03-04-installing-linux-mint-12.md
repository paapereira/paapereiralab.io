---
title: "Installing Linux Mint 12"
slug: "installing-linux-mint-12"
author: "Paulo Pereira"
date: 2012-03-04T00:06:00+00:00
lastmod: 2012-03-04T00:06:00+00:00
draft: false
toc: true
categories:
  - Linux
tags:
  - Linux
  - Linux Mint 12
  - Lisa
---

I've been running [Linux Mint 12](http://linuxmint.com/) for  the past three months (since December). I wasn't really happy with  Unity, and Mint was getting nice reviews so... I decided to jump in and  tried it myself.

Being based on Ubuntu, it was nice to see that it felt faster in my machine than Ubuntu 11.10.

I went with the [gnome 3](http://www.gnome.org/) route, but since then I change to [openbox](http://openbox.org/). Yes, openbox.

I'm leaving the openbox part to another post, so here's the Mint installation part.

## Installing

- Backup your data!
- [Download](http://www.linuxmint.com/download.php) Linux Mint. In my case was the 64bit DVD version (*linuxmint-12-gnome-dvd-64bit.iso*).
- Check the hash of the downloaded file (see the download site for the correct hash):

```text
548f0ac303fea840ef138e5669880a74  linuxmint-12-gnome-dvd-64bit.iso
```

- Create a CD or a bootable USB stick. I used the "Startup Disk Creator" in Ubuntu. Other alternatives [here](http://community.linuxmint.com/tutorial/view/744).
- Reboot into your CD or bootable USB stick.
- Install!

Installing Mint is as straightforward as Ubuntu, so there's nothing much to say.

I had trouble running the live environment. It kept giving me an error:

```text
vesamenu.c32: not a COM32R image
 boot:
```

I just typed `live` in the prompt and that was it. See [here](http://www.howtoforge.com/creating-a-bootable-usb-device-on-linux-mint-11) for more information.

If you have your /home partition separated from your / partition, be  sure to select the mount point, but leave the format option *unchecked*.

I kept my home partition and being based on Ubuntu, all my settings worked fine in Mint.

## Post install - Google Chrome

I installed Google Chrome beta.

- [Download](https://www.google.com/chrome/intl/en/eula_beta.html?dl=beta_amd64_deb)
- Install

```bash
sudo dpkg -i Downloads/google-chrome-beta_current_amd64.deb
```

- Had the following error

```text
google-chrome-beta depends on libnss3-1d (>= 3.12.3); however:
 Package libnss3-1d is not installed.
 google-chrome-beta depends on libcurl3; however:
 Package libcurl3 is not installed.
```

- Fixing the problem and finishing the installation

```bash
sudo apt-get -f install
```

## Post install - extra software

- NVIDIA Drivers (Settings > Additional Drivers)
- SSH and FTP

```bash
sudo apt-get install sshfs openssh-server gftp
```

- Multimedia (codecs, banshee, vlc and gaupol)

```bash
sudo apt-get install ubuntu-restricted-extras
sudo apt-get install ffmpeg

sudo apt-add-repository ppa:banshee-team/ppa
sudo apt-get update
sudo apt-get install banshee
sudo apt-get install banshee-extension-lyrics

sudo add-apt-repository ppa:n-muench/vlc
sudo apt-get update
sudo apt-get install vlc vlc-plugin-pulse vlc-plugin-notify
sudo apt-get install gaupol
```

- Java

```bash
sudo apt-get install openjdk-7-jre
```

- Printing (I have a HP Photosmart B110)

```bash
sudo apt-get install hplip-gui
```

- gperted with ntfs support

```bash
sudo apt-get install gparted
sudo apt-get install ntfs-3g
```

- Dropbox, with fix for gnome 3

```bash
dropbox stop
rm -r ~/.dropbox-dist
sudo apt-get install nautilus-dropbox
dropbox start -i
sudo ln /usr/lib/nautilus/extensions-2.0/*dropbox* /usr/lib/nautilus/extensions-3.0/
killall nautilus
```

- Compiz

```bash
sudo apt-get install compizconfig-settings-manager
```

- [lightdm tweaking](http://www.omgubuntu.co.uk/2011/10/simple-lightdm-manager-lets-easily-tweak-ubuntu-11-10-login-screen/)
- GCStar

```bash
sudo apt-get install gcstar
```

- Portuguese aspell

```bash
sudo apt-get install aspell-pt-pt
```

- RAR support

```bash
sudo apt-get install rar unrar
```

- Ubuntu Software Center

```bash
sudo apt-get install software-center
```

- XChat

```bash
sudo apt-get install xchat xchat-indicator
```

- Window buttons Ubuntu-like

```bash
sudo apt-get install gconf-editor
gconftool-2 -s -t string /desktop/gnome/shell/windows/button_layout "close,maximize,minimize:"
```