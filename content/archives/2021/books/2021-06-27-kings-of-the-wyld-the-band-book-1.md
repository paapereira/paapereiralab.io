---
title: "Kings of the Wyld (The Band Book 1) by Nicholas Eames (2017)"
slug: "kings-of-the-wyld-the-band-book-1"
author: "Paulo Pereira"
date: 2021-06-27T15:30:00+01:00
lastmod: 2021-06-27T15:30:00+01:00
cover: "/posts/books/kings-of-the-wyld-the-band-book-1.jpg"
description: "Finished “Kings of the Wyld (The Band Book 1)” by Nicholas Eames."
draft: false
toc: false
categories:
  - Books
book publication year:
  - 2017
book authors:
  - Nicholas Eames
book series:
  - The Band Series
book genres:
  - Fiction
  - Fantasy
book scores:
  - 7/10 Books
tags:
  - Book Log
  - Nicholas Eames
  - The Band Series
  - Fiction
  - Fantasy
  - 7/10 Books
  - Ebook
aliases:
  - /posts/books/kings-of-the-wyld-the-band-book-1
---

Finished “Kings of the Wyld”.
* Author: [Nicholas Eames](/book-authors/nicholas-eames)
* First Published: [February 21st 2017](/book-publication-year/2017)
* Series: [The Band](/book-series/the-band-series) Book 1
* Score: [7/10](/book-scores/7/10-books)

> [Goodreads](https://www.goodreads.com/book/show/31698361-kings-of-the-wyld)
>
> Clay Cooper and his band were once the best of the best -- the meanest, dirtiest, most feared crew of mercenaries this side of the Heartwyld.
>
> Their glory days long past, the mercs have grown apart and grown old, fat, drunk - or a combination of the three. Then an ex-bandmate turns up at Clay's door with a plea for help. His daughter Rose is trapped in a city besieged by an enemy one hundred thousand strong and hungry for blood. Rescuing Rose is the kind of mission that only the very brave or the very stupid would sign up for.
>
> It's time to get the band back together for one last tour across the Wyld.